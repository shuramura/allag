﻿using System;

namespace Allag.Core
{
    public class Logger : ILogger
    {
        #region Variables
        private readonly ILogger _logger;
        private Action<ILogger> _closeAction;
        #endregion

        #region Constructors
        public Logger(ILogger logger, Action<ILogger> closeAction)
        {
            _logger = logger;
            _closeAction = closeAction;
        }

        ~Logger()
        {
            Dispose(false);
        }
        #endregion

        #region Implementation of IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation of ILogger
        public void Debug(string message, params object[] args)
        {
            _logger?.Debug(message, args);
        }

        public void Error(string format, Exception exception, params object[] args)
        {
            _logger?.Error(format, exception, args);
        }

        public void FatalError(Exception exception)
        {
            _logger?.FatalError(exception);
        }

        public void FatalError(string format, Exception exception, params object[] args)
        {
            _logger?.FatalError(format, exception, args);
        }

        public ILogger GetLogger(Type type)
        {
            throw new NotSupportedException();
        }

        public void Info(string format, params object[] args)
        {
            _logger?.Info(format, args);
        }

        public void Warning(string format, params object[] args)
        {
            _logger?.Warning(format, args);
        }

        public void Warning(string format, Exception exception, params object[] args)
        {
            _logger?.Warning(format, exception, args);
        }
        #endregion

        #region Methods
        private void Dispose(bool disposing)
        {
            if (disposing && _closeAction != null)
            {
                _closeAction(this);
                _logger?.Dispose();
                _closeAction = null;
            }
        }
        #endregion
    }
}
