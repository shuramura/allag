#region Using directives
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Allag.Core.Configuration;
using Allag.Core.Configuration.Reflection;

#endregion

namespace Allag.Core.Reflection
{
    public static class ObjectFactory
    {
        #region Variables
        private static readonly Dictionary<Type, IList<ObjectElement>> _typeToDescriptions;

        private static readonly ConcurrentDictionary<Type, Func<object>> _typeToRuntimeCreaters;
        private static readonly ConcurrentDictionary<Type, ConcurrentDictionary<Type, object>> _typeToInstances;
        #endregion

        #region Constructors
        static ObjectFactory()
        {
            _typeToDescriptions = new Dictionary<Type, IList<ObjectElement>>();
            _typeToRuntimeCreaters = new ConcurrentDictionary<Type, Func<object>>();
            _typeToInstances = new ConcurrentDictionary<Type, ConcurrentDictionary<Type, object>>();

            ObjectsSection section = CoreCfg.Objects;
            foreach (ObjectElement item in section.Items)
            {
                IList<ObjectElement> items;
                if (!_typeToDescriptions.TryGetValue(item.Interface, out items))
                {
                    items = new List<ObjectElement>();
                    _typeToDescriptions.Add(item.Interface, items);
                }
                items.Add(item);
            }
        }
        #endregion

        #region Static methods
        public static TInterface Add<TInterface, TOwner>(TInterface @object)
            where TInterface : class
        {
            return Add(@object, typeof(TOwner));
        }

        public static TInterface Add<TInterface>(TInterface @object, Type ownerType = null)
            where TInterface : class
        {
            return (TInterface)Add(typeof(TInterface), @object, ownerType);
        }

        public static void Add<TInterface>(Func<TInterface> creater)
            where TInterface : class
        {
            _typeToRuntimeCreaters.AddOrUpdate(typeof(TInterface), creater, (type, func) => creater);
        }

        public static object Add(Type interfaceType, object @object, Type ownerType = null)
        {
            ConcurrentDictionary<Type, object> instances = _typeToInstances.GetOrAdd(interfaceType, x => new ConcurrentDictionary<Type, object>());
            return instances.AddOrUpdate(ownerType ?? typeof(ObjectFactory), addType => @object, (updateType, current) => @object);
        }

        public static bool Contains<TInterface>(object owner = null)
        {
            ConcurrentDictionary<Type, object> instances;
            return _typeToInstances.TryGetValue(typeof(TInterface), out instances) && instances.ContainsKey(owner == null ? typeof(ObjectFactory) : owner.GetType()) ||
                   _typeToRuntimeCreaters.ContainsKey(typeof(TInterface));
        }

        public static void Remove<TInterface>(object owner = null)
        {
            ConcurrentDictionary<Type, object> instances;
            if (_typeToInstances.TryGetValue(typeof(TInterface), out instances))
            {
                object @obj;
                instances.TryRemove(owner?.GetType() ?? typeof(ObjectFactory), out @obj);
            }
            Func<object> func;
            _typeToRuntimeCreaters.TryRemove(typeof(TInterface), out func);
        }

        public static TInterface Create<TInterface>(object owner, object args = null) where TInterface : class
        {
            if (owner == null)
            {
                throw new ArgumentNullException(nameof(owner));
            }

            return Create<TInterface>(owner.GetType(), args);
        }

        public static TInterface Create<TInterface>(Type owner, object args = null) where TInterface : class
        {
            return (TInterface)Create(typeof(TInterface), owner, args);
        }

        public static object Create(Type interfaceType, Type owner, object args = null)
        {
            return Create(interfaceType, owner, true, args);
        }

        private static object Create(Type interfaceType, Type owner, bool useDefault, object args = null)
        {
            using(LogBlock.New(typeof(ObjectFactory), new {interfaceType, owner, args}))
            {
                if (interfaceType == null)
                {
                    throw new ArgumentNullException(nameof(interfaceType));
                }

                object ret = null;

                Func<object> creater;
                IList<ObjectElement> elements;
                if (_typeToDescriptions.TryGetValue(interfaceType, out elements))
                {
                    ObjectElement element = null;
                    ObjectElement defaultElement = null;
                    foreach (ObjectElement item in elements.Where(item => interfaceType.IsAssignableFrom(item.Type)))
                    {
                        if (item.Owner == owner)
                        {
                            element = item;
                            break;
                        }
                        if (item.Owner == null && useDefault)
                        {
                            defaultElement = item;
                        }
                    }

                    if (element != null || defaultElement != null)
                    {
                        ret = (element ?? defaultElement).CreateObject(args);
                        LogBlock.Logger.Info("The type '{0}' was created for the interface '{1}'.", ret != null ? ret.GetType().FullName : "null", interfaceType.FullName);
                    }
                }
                else if (_typeToRuntimeCreaters.TryGetValue(interfaceType, out creater) && creater != null)
                {
                    ret = creater();
                    LogBlock.Logger.Info("The type '{0}' was created in runtime.", interfaceType.FullName);
                }
                else if (!interfaceType.IsAbstract && !interfaceType.IsInterface)
                {
                    ret = ObjectElement.CreateObject(owner, interfaceType, new ElementCollection<ParameterElement>(), args.ToDictionary());
                    LogBlock.Logger.Info("The type '{0}' was created.", interfaceType.FullName);
                }
                return ret;
            }
        }

        public static TInterface Get<TInterface>(object owner, object args = null) where TInterface : class
        {
            if (owner == null)
            {
                throw new ArgumentNullException(nameof(owner));
            }
            return Get<TInterface>(owner.GetType(), args);
        }

        public static TInterface Get<TInterface>(Type owner, object args = null) where TInterface : class
        {
            return (TInterface)Get(typeof(TInterface), owner, args);
        }

        public static object Get(Type interfaceType, Type owner, object args = null)
        {
            using(LogBlock.New(typeof(ObjectFactory), new {owner, args}))
            {
                if (interfaceType == null)
                {
                    throw new ArgumentNullException(nameof(interfaceType));
                }

                ConcurrentDictionary<Type, object> instances = _typeToInstances.GetOrAdd(interfaceType, x => new ConcurrentDictionary<Type, object>());

                object ret = null;
                if (owner != null && !instances.TryGetValue(owner, out ret) && (ret = Create(interfaceType, owner, false)) != null)
                {
                    instances.TryAdd(owner, ret);
                }

                return ret ?? (instances.GetOrAdd(typeof(ObjectFactory), x => Create(interfaceType, typeof(ObjectFactory), true, args)));
            }
        }
        #endregion
    }
}