#region Using directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;

#endregion

namespace Allag.Core.Reflection
{
    public static class ReflectionExtension
    {
        #region Constants
        public const int MagicPrimeNumber = 40375;
        #endregion

        #region Variables
        private static readonly MethodInfo _miReferenceEquals;
        private static readonly MethodInfo _miEquals;
        private static readonly MethodInfo _miGetHashCode;

        private static readonly MethodInfo _miGetEnumerator;
        private static readonly MethodInfo _miMoveNext;
        private static readonly MethodInfo _miCurrentGet;

        private static readonly Expression _expNull;
        private static readonly Expression _expZero;
        private static readonly Expression _expOne;
        private static readonly Expression _expFalse;
        private static readonly Expression _expTrue;
        private static readonly Expression _magicPrimeNumberExpression;

        private static readonly ParameterExpression _expEqualityRet;
        private static readonly ParameterExpression _expHashCodeRet;
        private static readonly ParameterExpression _expMoveNext;
        private static readonly ParameterExpression _expFirst;
        private static readonly ParameterExpression _expSecond;

        private static readonly LabelTarget _laEqualityReturn;
        private static readonly LabelTarget _laHashReturn;
        #endregion

        #region Constructors
        static ReflectionExtension()
        {
            Type typeObject = typeof(object);
            _miReferenceEquals = typeObject.GetMethod("ReferenceEquals", BindingFlags.Static | BindingFlags.Public);
            _miEquals = typeObject.GetMethod("Equals", new[] {typeof(object)});
            _miGetHashCode = typeObject.GetMethod("GetHashCode", Type.EmptyTypes);

            _miGetEnumerator = typeof(IEnumerable).GetMethod("GetEnumerator", Type.EmptyTypes);
            _miMoveNext = typeof(IEnumerator).GetMethod("MoveNext", Type.EmptyTypes);
            _miCurrentGet = typeof(IEnumerator).GetProperty("Current").GetGetMethod();

            _expNull = Expression.Constant(null);
            _expZero = Expression.Constant(0);
            _expOne = Expression.Constant(1);
            _expTrue = Expression.Constant(true);
            _expFalse = Expression.Constant(false);
            _magicPrimeNumberExpression = Expression.Constant(MagicPrimeNumber);

            _expEqualityRet = Expression.Variable(typeof(bool), "ret");
            _expHashCodeRet = Expression.Variable(typeof(int), "ret");
            _expMoveNext = Expression.Variable(typeof(bool), "bFirst");
            _expFirst = Expression.Variable(typeof(IEnumerator), "enFirst");
            _expSecond = Expression.Variable(typeof(IEnumerator), "enSecond");

            _laEqualityReturn = Expression.Label(typeof(bool));
            _laHashReturn = Expression.Label(typeof(int));
        }
        #endregion

        #region Static methods
        public static void HandleAttribute<TAttribute>(this ICustomAttributeProvider attributeProvider, Func<TAttribute, bool> handler) where TAttribute : Attribute
        {
            HandleAttribute(attributeProvider, false, handler);
        }

        public static void HandleAttribute<TAttribute>(this ICustomAttributeProvider attributeProvider, bool inherit, Func<TAttribute, bool> handler) where TAttribute : Attribute
        {
            if (attributeProvider != null && handler != null)
            {
                object[] attributes = attributeProvider.GetCustomAttributes(typeof(TAttribute), inherit);
                foreach (object t in attributes)
                {
                    if (!handler((TAttribute)t))
                    {
                        break;
                    }
                }
            }
        }

        public static void HandleMember<TMember>(this TMember[] members, Func<TMember, bool> handler) where TMember : MemberInfo
        {
            if (members != null && handler != null)
            {
                foreach (TMember t in members)
                {
                    if (!handler(t))
                    {
                        break;
                    }
                }
            }
        }

        public static IReadOnlyDictionary<string, object> ToDictionary(this object @obj, BindingFlags flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty)
        {
            Dictionary<string, object> ret = new Dictionary<string, object>();
            if (@obj != null)
            {
                flags &= (BindingFlags.Instance | BindingFlags.Static | BindingFlags.GetField | BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
                IReadOnlyDictionary<string, MemberInfo> members = @obj.GetType().GetMembers(flags).ToDictionary(x => x.Name);
                foreach (KeyValuePair<string, MemberInfo> pair in members)
                {
                    Func<object, object> valueProvider = null;
                    MemberInfo memberInfo = pair.Value;
                    if (pair.Value.MemberType == MemberTypes.Field && flags.HasFlag(BindingFlags.GetField))
                    {
                        if (!memberInfo.Name.EndsWith("BackingField"))
                        {
                            valueProvider = x => ((FieldInfo)memberInfo).GetValue(x);
                        }
                    }
                    else if (pair.Value.MemberType == MemberTypes.Property && flags.HasFlag(BindingFlags.GetProperty))
                    {
                        valueProvider = x => ((PropertyInfo)memberInfo).GetValue(x);
                    }

                    if (valueProvider != null)
                    {
                        ret.Add(pair.Key, valueProvider(@obj));
                    }
                }
            }
            return ret;
        }

        public static ConstructorInfo GetConstructorByParameterNames(this Type type, params string[] parameterNames)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return (ConstructorInfo)GetMethodByParameterNames(type.GetConstructors().Select(x => (MethodBase)x).ToArray(), parameterNames);
        }

        public static MethodBase GetMethodByParameterNames(this MethodBase[] methods, params string[] parameterNames)
        {
            if (methods == null)
            {
                throw new ArgumentNullException(nameof(methods));
            }

            if (parameterNames == null)
            {
                throw new ArgumentNullException(nameof(parameterNames));
            }

            Dictionary<string, object> dictionary = new Dictionary<string, object>(parameterNames.Length * 2);
            foreach (string name in parameterNames)
            {
                dictionary.Add(name, null);
            }

            MethodBase ret = null;
            foreach (MethodBase methodBase in methods)
            {
                ParameterInfo[] parameterInfos = methodBase.GetParameters();
                bool isMatched = true;
                int nDefaultParameters = 0;
                foreach (ParameterInfo parameterInfo in parameterInfos)
                {
                    isMatched = dictionary.ContainsKey(parameterInfo.Name);
                    if (!isMatched && parameterInfo.IsOptional)
                    {
                        nDefaultParameters++;
                        isMatched = true;
                    }
                }

                if (isMatched && parameterInfos.Length == (dictionary.Keys.Count + nDefaultParameters))
                {
                    ret = methodBase;
                    break;
                }
            }
            return ret;
        }

        public static TObject SetProperty<TObject, TValue>(this TObject @object, Expression<Func<TObject, TValue>> propertyExpression, TValue value)
            where TObject : class
        {
            if (@object == null)
            {
                throw new ArgumentNullException(nameof(@object));
            }

            SetProperty(@object, (LambdaExpression)propertyExpression, value);
            return @object;
        }

        public static void SetProperty<TValue>(Expression<Func<TValue>> propertyExpression, TValue value)
        {
            SetProperty(null, propertyExpression, value);
        }

        private static void SetProperty(this object @object, LambdaExpression propertyExpression, object value)
        {
            MemberExpression memberExpression = propertyExpression.GetMember();

            PropertyInfo info = memberExpression.Member as PropertyInfo;
            if (info == null)
            {
                throw new InvalidOperationException("The expression body should provide object property.");
            }

            if (info.CanWrite)
            {
                info.SetValue(@object, value, null);    
            }
            else
            {
                BindingFlags flags = BindingFlags.NonPublic | (@object != null ? BindingFlags.Instance : BindingFlags.Static);
                FieldInfo fieldInfo  = info.DeclaringType.GetField($"<{info.Name}>k__BackingField", flags);
                if (fieldInfo == null)
                {
                    string strFieldName = '_' + info.Name.ToLower().Substring(0, 1) + info.Name.Substring(1);
                    fieldInfo = info.DeclaringType.GetField(strFieldName, flags);
                }
                
                if (fieldInfo == null)
                {
                    throw new InvalidOperationException($"There is no back field for the property '{info.Name}'.");
                }
                fieldInfo.SetValue(@object, value);
            }
        }

        public static Func<object, object> CreateGetHandler<TOwner, TItem, TValue>(this Expression<Func<TItem, TValue>> propertyExpression)
        {
            MemberExpression memberExpression = propertyExpression.GetMember();
            return memberExpression.Member.CreateGetHandler<TOwner>();
        }

        public static Func<object, object> CreateGetHandler<TOwner>(this MemberInfo memberInfo)
        {
            if (memberInfo == null)
            {
                throw new ArgumentNullException(nameof(memberInfo));
            }
            DynamicMethod dynamicGet = new DynamicMethod("Dm" + Guid.NewGuid().ToString("N"), typeof(object), new[] {typeof(object)}, typeof(TOwner), true);
            ILGenerator generator = dynamicGet.GetILGenerator();

            generator.Emit(OpCodes.Ldarg_0);
            switch (memberInfo.MemberType)
            {
                case MemberTypes.Property:
                {
                    MethodInfo methodInfo = ((PropertyInfo)memberInfo).GetGetMethod(true);
                    generator.Emit(OpCodes.Callvirt, methodInfo);
                    Box(methodInfo.ReturnType, generator);
                }
                    break;
                case MemberTypes.Field:
                {
                    FieldInfo fieldInfo = (FieldInfo)memberInfo;
                    generator.Emit(OpCodes.Ldfld, fieldInfo);
                    Box(fieldInfo.FieldType, generator);
                }
                    break;
                default:
                    throw new InvalidOperationException($"The member '{memberInfo}' is not allowed.");
            }
            generator.Emit(OpCodes.Ret);

            return (Func<object, object>)dynamicGet.CreateDelegate(typeof(Func<object, object>));
        }

        public static Action<object, object> CreateSetHandler<TOwner, TItem, TValue>(this Expression<Func<TItem, TValue>> propertyExpression)
        {
            MemberExpression memberExpression = propertyExpression.GetMember();
            return memberExpression.Member.CreateSetHandler<TOwner>();
        }

        public static Action<object, object> CreateSetHandler<TOwner>(this MemberInfo memberInfo)
        {
            if (memberInfo == null)
            {
                throw new ArgumentNullException(nameof(memberInfo));
            }

            DynamicMethod dynamicMethod = new DynamicMethod("Dm" + Guid.NewGuid().ToString("N"), typeof(void), new[] {typeof(object), typeof(object)}, typeof(TOwner), true);
            ILGenerator generator = dynamicMethod.GetILGenerator();

            generator.Emit(OpCodes.Ldarg_0);
            generator.Emit(OpCodes.Ldarg_1);
            switch (memberInfo.MemberType)
            {
                case MemberTypes.Property:
                {
                    MethodInfo methodInfo = ((PropertyInfo)memberInfo).GetSetMethod(true);
                    Unbox(methodInfo.GetParameters()[0].ParameterType, generator);
                    generator.Emit(OpCodes.Callvirt, methodInfo);
                }
                    break;
                case MemberTypes.Field:
                {
                    FieldInfo fieldInfo = (FieldInfo)memberInfo;
                    Unbox(fieldInfo.FieldType, generator);
                    generator.Emit(OpCodes.Stfld, fieldInfo);
                }
                    break;
                default:
                    throw new InvalidOperationException($"The member '{memberInfo}' is not allowed.");
            }

            generator.Emit(OpCodes.Ret);

            return (Action<object, object>)dynamicMethod.CreateDelegate(typeof(Action<object, object>));
        }

        public static void CreateEqualityAndHashCodeHandlers<TItem, TAttrKey>(out Func<TItem, TItem, bool> equalityHandler, out Func<TItem, int> hashCodeHandler)
            where TItem : class
        {
            MemberInfo[] memberInfos = typeof(TItem).GetMembers(BindingFlags.GetField | BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)
                .Where(x => Attribute.IsDefined(x, typeof(TAttrKey), true)).ToArray();

            ParameterExpression paramOne = Expression.Parameter(typeof(TItem), "one");
            ParameterExpression paramTwo = Expression.Parameter(typeof(TItem), "two");
            Expression expEqualityBody = _expTrue;
            Expression expHashCodeBody = _expOne;

            if (memberInfos.Length > 0)
            {
                foreach (MemberInfo memberInfo in memberInfos)
                {
                    Expression first;
                    Expression second;
                    Type parameterType;
                    if (memberInfo.MemberType == MemberTypes.Property)
                    {
                        PropertyInfo propertyInfo = (PropertyInfo)memberInfo;
                        first = Expression.Property(paramOne, propertyInfo);
                        second = Expression.Property(paramTwo, propertyInfo);
                        parameterType = propertyInfo.PropertyType;
                    }
                    else
                    {
                        FieldInfo fieldInfo = (FieldInfo)memberInfo;
                        first = Expression.Field(paramOne, fieldInfo);
                        second = Expression.Field(paramTwo, fieldInfo);
                        parameterType = fieldInfo.FieldType;
                    }

                    Expression expTempHashCodeBody;
                    if (parameterType == typeof(string) || !typeof(IEnumerable).IsAssignableFrom(parameterType))
                    {
                        expEqualityBody = Expression.AndAlso(expEqualityBody, GenerateEqualFor(parameterType, first, second));
                        expTempHashCodeBody = GenerateHashCodeFor(parameterType, first);
                    }
                    else
                    {
                        expEqualityBody = Expression.AndAlso(expEqualityBody,
                            Expression.Block(
                                new ParameterExpression[] {_expFirst, _expSecond, _expEqualityRet, _expMoveNext},
                                Expression.Assign(_expEqualityRet, Expression.Equal(first, second)),
                                Expression.IfThen(
                                    Expression.AndAlso(Expression.IsFalse(_expEqualityRet),
                                        Expression.AndAlso(Expression.NotEqual(first, _expNull), Expression.NotEqual(second, _expNull))),
                                    Expression.Block(
                                        Expression.Assign(_expFirst, Expression.Call(first, _miGetEnumerator)),
                                        Expression.Assign(_expSecond, Expression.Call(second, _miGetEnumerator)),
                                        Expression.Assign(_expEqualityRet, Expression.Constant(true)),
                                        Expression.Assign(_expMoveNext, Expression.Constant(true)),
                                        Expression.Loop(
                                            Expression.Block(
                                                Expression.IfThen(Expression.IsFalse(_expEqualityRet), Expression.Break(_laEqualityReturn, _expEqualityRet)),
                                                Expression.Assign(_expMoveNext, Expression.Call(_expFirst, _miMoveNext)),
                                                Expression.IfThen(Expression.IsFalse(_expMoveNext), Expression.Break(_laEqualityReturn, Expression.Constant(false))),
                                                Expression.IfThen(Expression.IsFalse(Expression.Call(_expSecond, _miMoveNext)), Expression.Break(_laEqualityReturn, _expEqualityRet)),
                                                Expression.Assign(_expEqualityRet, GenerateEqualFor(typeof(object), Expression.Call(_expFirst, _miCurrentGet), Expression.Call(_expSecond, _miCurrentGet)))
                                                ),
                                            _laEqualityReturn),
                                        Expression.AndAlso(_expEqualityRet, Expression.Equal(_expMoveNext, Expression.Call(_expSecond, _miMoveNext)))
                                        )
                                    ),
                                _expEqualityRet
                                )
                            );

                        expTempHashCodeBody = Expression.Block(
                            new ParameterExpression[] {_expFirst, _expHashCodeRet},
                            Expression.IfThenElse(Expression.NotEqual(first, _expNull),
                                Expression.Block
                                    (
                                        Expression.Assign(_expFirst, Expression.Call(first, _miGetEnumerator)),
                                        Expression.Assign(_expHashCodeRet, Expression.Constant(1)),
                                        Expression.Loop(
                                            Expression.Block(
                                                Expression.IfThen(Expression.IsFalse(Expression.Call(_expFirst, _miMoveNext)), Expression.Break(_laHashReturn, _expHashCodeRet)),
                                                Expression.Assign(_expHashCodeRet,
                                                    Expression.ExclusiveOr(
                                                        Expression.Multiply(_magicPrimeNumberExpression, _expHashCodeRet), GenerateHashCodeFor(typeof(object), Expression.Call(_expFirst, _miCurrentGet))))),
                                            _laHashReturn)
                                    ),
                                Expression.Assign(_expHashCodeRet, _expZero)),
                            _expHashCodeRet);
                    }
                    expHashCodeBody = Expression.ExclusiveOr(Expression.Multiply(expHashCodeBody, _magicPrimeNumberExpression), expTempHashCodeBody);
                }

                expEqualityBody = Expression.AndAlso(Expression.NotEqual(paramTwo, _expNull), Expression.OrElse(Expression.Call(null, _miReferenceEquals, paramOne, paramTwo), expEqualityBody));
                expHashCodeBody = Expression.ExclusiveOr(expHashCodeBody, _magicPrimeNumberExpression);

                equalityHandler = Expression.Lambda<Func<TItem, TItem, bool>>(expEqualityBody, paramOne, paramTwo).Compile();
                hashCodeHandler = Expression.Lambda<Func<TItem, int>>(expHashCodeBody, paramOne).Compile();
            }
            else
            {
                equalityHandler = ReferenceEquals;
                hashCodeHandler = x => 0;
            }
        }

        private static Expression GenerateEqualFor(Type parameterType, Expression one, Expression two)
        {
            Expression nullExpression = _expFalse;
            Expression notNullExpression = Expression.Call(one, _miEquals, Expression.Convert(two, typeof(object)));
            if (IsNullable(parameterType))
            {
                Expression left = Expression.Equal(one, _expNull);
                Expression right = Expression.Equal(two, _expNull);
                nullExpression = Expression.AndAlso(left, right);

                left = Expression.NotEqual(one, _expNull);
                right = Expression.NotEqual(two, _expNull);
                notNullExpression = Expression.AndAlso(Expression.AndAlso(left, right), notNullExpression);
            }

            return Expression.OrElse(nullExpression, notNullExpression);
        }

        private static Expression GenerateHashCodeFor(Type parameterType, Expression param)
        {
            Expression expHashCode = Expression.Call(param, _miGetHashCode);
            if (IsNullable(parameterType))
            {
                expHashCode = Expression.Condition(Expression.Equal(param, _expNull), _expZero, expHashCode);
            }
            return expHashCode;
        }

        private static bool IsNullable(Type type)
        {
            return !type.IsValueType || type.FullName.StartsWith("System.Nullable`");
        }

        private static void Box(Type type, ILGenerator generator)
        {
            if (type.IsValueType)
            {
                generator.Emit(OpCodes.Box, type);
            }
        }

        private static void Unbox(Type type, ILGenerator generator)
        {
            if (type.IsValueType)
            {
                generator.Emit(OpCodes.Unbox_Any, type);
            }
        }
        #endregion
    }
}