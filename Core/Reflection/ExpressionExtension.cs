#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Reflection;

#endregion

namespace Allag.Core.Reflection
{
    public static class ExpressionExtension
    {
        #region Methods
        public static bool AreEqual<TExpression>(this Expression<TExpression> expression, Expression<TExpression> expected)
        {
            return (expression?.ToString() ?? string.Empty) == (expected?.ToString() ?? string.Empty);
        }

        public static bool HandleMethodExpression<TObject>(this Expression expression, Action<MethodInfo, IList<Expression>> handler)
        {
            return HandleMethodExpression(expression, typeof(TObject), handler);
        }

        public static bool HandleMethodExpression(this Expression expression, Action<MethodInfo, IList<Expression>> handler)
        {
            return HandleMethodExpression(expression, null, handler);
        }

        public static bool HandleMethodExpression(this Expression expression, Type objectType, Action<MethodInfo, IList<Expression>> handler)
        {
            if (handler == null)
            {
                throw new ArgumentNullException(nameof(handler));
            }

            bool ret = false;
            if (expression != null && expression.NodeType == ExpressionType.Call)
            {
                MethodCallExpression methodCallExpression = (MethodCallExpression)expression;
                if (objectType != null && !methodCallExpression.Method.DeclaringType.IsAssignableFrom(objectType))
                {
                    throw new InvalidOperationException($"The methods of the type '{objectType}' are only allowed.");
                }

                handler(methodCallExpression.Method, methodCallExpression.Arguments);
                ret = true;
            }
            return ret;
        }

        public static Expression<TFunc> JoinExpression<TFunc>(this IEnumerable<Expression<TFunc>> expressions, bool isAndJoin = true)
        {
            if (expressions == null)
            {
                throw new ArgumentNullException(nameof(expressions));
            }

            int nCount = 0;
            Expression expBody = Expression.Constant(isAndJoin);
            List<ParameterExpression> parameters = new List<ParameterExpression>();
            foreach (Expression<TFunc> expression in expressions)
            {
                expBody = isAndJoin ? Expression.AndAlso(expBody, expression.Body) : Expression.Or(expBody, expression.Body);
                if (nCount == 0)
                {
                    parameters.AddRange(expression.Parameters);
                }
                nCount++;
            }

            return nCount == 0 ? null : Expression.Lambda<TFunc>(expBody, parameters);
        }

        public static MemberExpression GetMember(this LambdaExpression property)
        {
            if (property == null)
            {
                throw new ArgumentNullException(nameof(property));
            }

            Expression expression = property.Body;

            while (expression.NodeType == ExpressionType.Convert)
            {
                expression = ((UnaryExpression)expression).Operand;
            }

            MemberExpression ret = expression as MemberExpression;
            if (ret == null)
            {
                throw new InvalidOperationException("The expression body should provide object member.");
            }
            return ret;
        }

        public static MemberInfo GetMemberInfo<TEntity, TValue>(this Expression<Func<TEntity, TValue>> expression)
        {
            if (expression == null)
            {
                throw new ArgumentNullException(nameof(expression));
            }

            MemberInfo memberInfo = null;
            Expression body = expression.Body;
            while (body != null)
            {
                switch (body.NodeType)
                {
                    case ExpressionType.Convert:
                        body = ((UnaryExpression)body).Operand;
                        break;
                    case ExpressionType.MemberAccess:
                        memberInfo = ((MemberExpression)body).Member;
                        body = null;
                        break;
                    default:
                        body = null;
                        break;
                }
            }
            if (memberInfo == null)
            {
                throw new InvalidDataException("The expression does not have member access instruction.");
            }

            return memberInfo;
        }
        #endregion
    }
}