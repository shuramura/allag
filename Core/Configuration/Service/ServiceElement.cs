#region Using directives

using System.Configuration;
using System.ServiceProcess;
using Allag.Core.Configuration.Reflection;

#endregion

namespace Allag.Core.Configuration.Service
{
    public class ServiceElement : ObjectElement
    {
        #region Override property
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        [RegexStringValidator(@"[^/\\\s]{0,256}")]
        public override string Name => base.Name;
        #endregion

        #region Properties
        [ConfigurationProperty("displayName", IsRequired = false)]
        public string DisplayName
        {
            get 
            {
                string ret = (string)this["displayName"];
                if (string.IsNullOrEmpty(ret))
                {
                    ret = Name;
                }
                return ret; 
            }
        }

        [ConfigurationProperty("description", IsRequired = false)]
        public string Description => (string)this["description"];

        [ConfigurationProperty("startType", IsRequired = false, DefaultValue = ServiceStartMode.Automatic)]
        public ServiceStartMode StartType => (ServiceStartMode)base["startType"];
        #endregion
    }
}
