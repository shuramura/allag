#region Using directives

using System.Configuration;

#endregion

namespace Allag.Core.Configuration.Service
{
    public class ServicesSection : Section
    {
        #region Properties

        [ConfigurationProperty("", Options = ConfigurationPropertyOptions.IsDefaultCollection)]
        [ConfigurationCollection(typeof(ElementCollection<ServiceElement>))]
        public ElementCollection<ServiceElement> Items => (ElementCollection<ServiceElement>)base[""];
        #endregion
    }
}
