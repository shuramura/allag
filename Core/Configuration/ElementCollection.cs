#region Using directives
using System.Collections.Generic;
using System.Configuration;

#endregion

namespace Allag.Core.Configuration
{
    public class ElementCollection<TElement, TKey> : ConfigurationElementCollection, IEnumerable<TElement>
        where TElement : BaseElement, new()
    {
        #region Implementation of IEnumerable<TElement>
        IEnumerator<TElement> IEnumerable<TElement>.GetEnumerator()
        {
            foreach (TElement element in this)
            {
                yield return element;
            }
        }
        #endregion

        #region Override properties

        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.AddRemoveClearMap;
        #endregion

        #region Override methods

        protected override ConfigurationElement CreateNewElement()
        {
            return new TElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((TElement)element).Key;
        }

        #endregion

        #region Properties
        public virtual TElement this[TKey keyName] => (TElement)BaseGet(keyName);
        #endregion
    }

    public class ElementCollection<TElement> : ElementCollection<TElement, string>
        where TElement : Element<string>, new()
    {
    }
}
