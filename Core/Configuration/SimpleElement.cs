﻿#region Using directives
using System;

#endregion

namespace Allag.Core.Configuration
{
    public class SimpleElement : BaseElement
    {
        #region Variables
        private readonly Guid _key;
        #endregion

        #region Constructors
        public SimpleElement()
        {
            _key = Guid.NewGuid();
        }
        #endregion

        #region Override properties
        public override object Key => _key;
        #endregion
    }
}