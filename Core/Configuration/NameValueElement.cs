#region Using directives

using System.Configuration;

#endregion

namespace Allag.Core.Configuration
{
    public class NameValueElement : Element
    {
        #region Properties
        [ConfigurationProperty("value", IsRequired = false)]
        public string Value
        {
            get { return (string)base["value"]; }
            set { base["value"] = value; }
        }
        #endregion
    }
}
