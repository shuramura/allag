﻿#region Using directives
using System.Configuration;
using Allag.Core.Configuration.Reflection;

#endregion

namespace Allag.Core.Configuration.Net
{
    public class CustomServerElement : ObjectElement
    {
        #region Properties
        [ConfigurationProperty("schema", IsRequired = true)]
        public string Schema => (string)base["schema"];
        #endregion
    }
}