#region Using directives
using System;
using System.Configuration;
using System.Net;

#endregion

namespace Allag.Core.Configuration.Net
{
    public class FtpElement : ServerElement
    {
        #region Constructors

        public FtpElement() : base(21, 300000) { }

        #endregion

        #region Implementation of ServerElement
        public override string UriScheme => Uri.UriSchemeFtp;
        #endregion

        #region Properties
        [ConfigurationProperty("usePassive", IsRequired = false, DefaultValue = false)]
        public bool UsePassive => (bool)base["usePassive"];

        [ConfigurationProperty("keepAlive", IsRequired = false, DefaultValue = false)]
        public bool KeepAlive => (bool)base["keepAlive"];

        [ConfigurationProperty("useDefaultProxy", IsRequired = false, DefaultValue = false)]
        public bool UseDefaultProxy => (bool)base["useDefaultProxy"];
        #endregion

        #region Methods
        public FtpWebRequest GetRequest(string path)
        {
            FtpWebRequest ftpWebRequest = (FtpWebRequest)WebRequest.Create(new Uri(BaseUri, path));
            ftpWebRequest.EnableSsl = EnableSsl;
            NetworkCredential credential = Credential.Account;
            if (credential != null)
            {
                ftpWebRequest.Credentials = credential;    
            }
            ftpWebRequest.KeepAlive = KeepAlive;
            if (!UseDefaultProxy)
            {
                ftpWebRequest.Proxy = null;
            }
            ftpWebRequest.UsePassive = UsePassive;
            ftpWebRequest.Timeout = Timeout;

            return ftpWebRequest;
        }

        #endregion
    }
}
