﻿#region Using directives
using System;
using System.Configuration;
using System.Globalization;
using System.Net;

#endregion

namespace Allag.Core.Configuration.Net
{
    public class HttpElement : ServerElement
    {
        #region Constructors
        public HttpElement() : base(80, 100000) {}
        #endregion

        #region Implementation of ServerElement
        public override string UriScheme => EnableSsl ? Uri.UriSchemeHttps : Uri.UriSchemeHttp;
        #endregion

        #region Properties
        [ConfigurationProperty("keepAlive", IsRequired = false, DefaultValue = false)]
        public bool KeepAlive => (bool)base["keepAlive"];

        [ConfigurationProperty("acceptLanguage", IsRequired = false, DefaultValue = "")]
        public string AcceptLanguage
        {
            get
            {
                string ret = (string)base["acceptLanguage"];
                if (ret.Length == 0)
                {
                    ret = CultureInfo.CurrentCulture.Name;
                }
                return ret;
            }
        }

        [ConfigurationProperty("useDefaultProxy", IsRequired = false, DefaultValue = true)]
        public bool UseDefaultProxy => (bool)base["useDefaultProxy"];
        #endregion

        #region Methods
        public HttpWebRequest GetRequest(string path)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(new Uri(BaseUri, path));

            httpWebRequest.Headers.Add(HttpRequestHeader.AcceptLanguage, AcceptLanguage);
            NetworkCredential credential = Credential.Account;
            if (credential != null)
            {
                httpWebRequest.Credentials = credential;
            }
            httpWebRequest.KeepAlive = KeepAlive;
            if (!UseDefaultProxy)
            {
                httpWebRequest.Proxy = null;
            }
            httpWebRequest.Timeout = Timeout;
            return httpWebRequest;
        }
        #endregion
    }
}