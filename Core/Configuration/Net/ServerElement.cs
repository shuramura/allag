﻿#region Using directives
using System;
using System.Configuration;

#endregion

namespace Allag.Core.Configuration.Net
{
    public abstract class ServerElement : Element
    {
        #region Variables
        private readonly int _nDefaultPort;
        private readonly int _nDefaultTimeout;
        #endregion

        #region Constructors
        protected ServerElement(int defaultPort, int defaultTimeout)
        {
            if (defaultTimeout < 60000)
            {
                throw new ArgumentOutOfRangeException(nameof(defaultTimeout), defaultTimeout, "The value should be greater or equal to 60000.");
            }

            _nDefaultPort = defaultPort;
            _nDefaultTimeout = defaultTimeout;
        }
        #endregion

        #region Properties
        [ConfigurationProperty("host", IsRequired = true)]
        public string Host => (string)base["host"];

        [ConfigurationProperty("enableSsl", IsRequired = false, DefaultValue = false)]
        public bool EnableSsl => (bool)base["enableSsl"];

        [ConfigurationProperty("port", IsRequired = false, DefaultValue = 0)]
        [IntegerValidator(MinValue = 0)]
        public int Port => GetValue((int)base["port"], 0, _nDefaultPort);

        [ConfigurationProperty("basePath", IsRequired = false)]
        public virtual string BasePath => (string)base["basePath"];

        [ConfigurationProperty("timeout", IsRequired = false, DefaultValue = 59999)]
        [IntegerValidator(MinValue = 59999)]
        public int Timeout => GetValue((int)base["timeout"], 59999, _nDefaultTimeout);

        [ConfigurationProperty("credential", IsRequired = false)]
        public CredentialElement Credential => (CredentialElement)base["credential"];

        public Uri BaseUri
        {
            get
            {
                string strBaseUriFormat = "{0}{1}{2}";
                if (Port > 0)
                {
                    strBaseUriFormat += ":{3}";
                }
                if (!string.IsNullOrWhiteSpace(BasePath))
                {
                    strBaseUriFormat += "{4}";
                }
                return new Uri(string.Format(strBaseUriFormat, UriScheme, Uri.SchemeDelimiter, Host, Port, BasePath));
            }
        }
        #endregion

        #region Abstract methods
        public abstract string UriScheme { get; }
        #endregion

        #region Static methods
        private static TValue GetValue<TValue>(TValue value, TValue unspecifiedValue, TValue defaultValue)
        {
            if (value.Equals(unspecifiedValue))
            {
                value = defaultValue;
            }
            return value;
        }
        #endregion
    }
}