#region Using directives

using System;
using System.ComponentModel;
using System.Configuration;
using Allag.Core.Net;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.Configuration.Net
{
    public class EmailElement : Element
    {
        #region Constructors
        public EmailElement() {}

        public EmailElement(string addresses, string smtpClient = null)
            : this(new MemoryResource(addresses), smtpClient)
        {
        }

        public EmailElement(IResource addresses, string smtpClient = null)
            : this()
        {
            if (addresses == null)
            {
                throw new ArgumentNullException(nameof(addresses));
            }
            if (smtpClient != null)
            {
                base["smtp"] = TypeDescriptor.GetConverter(typeof(SmtpClient)).ConvertFrom(smtpClient);
            }
            Addresses = addresses;
        }
        #endregion

        #region Properties
        [ConfigurationProperty("smtp", DefaultValue = "")]
        public SmtpClient Smtp => (SmtpClient)base["smtp"];

        [ConfigurationProperty("addresses", IsRequired = true)]
        public IResource Addresses
        {
            get { return (IResource)base["addresses"]; }
            private set { base["addresses"] = value; }
        }

        [ConfigurationProperty("subject")]
        public IResource Subject => (IResource)base["subject"] ?? Smtp.Configuration.Subject;

        [ConfigurationProperty("isBodyHtml", DefaultValue = false)]
        public bool IsBodyHtml
        {
            get
            {
                bool ret;
                if (base["message"] == null)
                {
                    ret = Smtp.Configuration.IsBodyHtml;
                }
                else
                {
                    ret = (bool)base["isBodyHtml"];
                }
                return ret;
            }
        }

        [ConfigurationProperty("cc")]
        public IResource Cc => (IResource)base["cc"] ?? Smtp.Configuration.Cc;

        [ConfigurationProperty("bcc")]
        public IResource Bcc => (IResource)base["bcc"] ?? Smtp.Configuration.Bcc;

        [ConfigurationProperty("message")]
        public IResource Message => (IResource)base["message"] ?? Smtp.Configuration.Message;

        [ConfigurationProperty("attachments")]
        [ConfigurationCollection(typeof(ElementCollection<ResourceElement>))]
        public ElementCollection<ResourceElement> Attachments => (ElementCollection<ResourceElement>)base["attachments"];
        #endregion
    }
}