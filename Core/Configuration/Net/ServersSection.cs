#region Using directives
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Allag.Core.Configuration.Reflection;
using Allag.Core.Net;

#endregion

namespace Allag.Core.Configuration.Net
{
    public class ServersSection : Section
    {
        #region Variables
        private static readonly IDictionary<string, Func<ServersSection, string, IFileClient>> _fileClients;
        #endregion

        #region Constructors
        static ServersSection()
        {
            _fileClients = new Dictionary<string, Func<ServersSection, string, IFileClient>>
            {
                {Uri.UriSchemeFtp, (scheme, name) => new FtpClient(scheme.Ftps.FirstOrDefault(x => string.Compare(x.Name, name, StringComparison.CurrentCultureIgnoreCase) == 0))},
                {Uri.UriSchemeFile, (scheme, name) => new FileSystemClient(scheme.FileSystems.FirstOrDefault(x => string.Compare(x.Name, name, StringComparison.CurrentCultureIgnoreCase) == 0))}
            };
        }
        #endregion

        #region Properties
        [ConfigurationProperty("http")]
        [ConfigurationCollection(typeof(ElementCollection<HttpElement>))]
        public ElementCollection<HttpElement> Https => (ElementCollection<HttpElement>)base["http"];

        [ConfigurationProperty("ftp")]
        [ConfigurationCollection(typeof(ElementCollection<FtpElement>))]
        public ElementCollection<FtpElement> Ftps => (ElementCollection<FtpElement>)base["ftp"];

        [ConfigurationProperty("smtp")]
        [ConfigurationCollection(typeof(ElementCollection<SmtpElement>))]
        public ElementCollection<SmtpElement> Smtps => (ElementCollection<SmtpElement>)base["smtp"];

        [ConfigurationProperty("fileSystem")]
        [ConfigurationCollection(typeof(ElementCollection<FileSystemElement>))]
        public ElementCollection<FileSystemElement> FileSystems => (ElementCollection<FileSystemElement>)base["fileSystem"];

        [ConfigurationProperty("custom")]
        [ConfigurationCollection(typeof(ElementCollection<ObjectElement>))]
        public ElementCollection<CustomServerElement> Customs => (ElementCollection<CustomServerElement>)base["custom"];
        #endregion

        #region Methods
        public IFileClient GetFileClient(string scheme, string name)
        {
            if (string.IsNullOrWhiteSpace(scheme))
            {
                throw new ArgumentNullOrEmptyException(nameof(scheme));
            }

            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            Func<ServersSection, string, IFileClient> fileClient;
            IFileClient ret = null;

            if (_fileClients.TryGetValue(scheme, out fileClient))
            {
                ret = fileClient(this, name);
            }
            else
            {
                foreach (CustomServerElement element in Customs)
                {
                    if (string.Compare(element.Name, name, StringComparison.CurrentCultureIgnoreCase) == 0 &&
                        string.Compare(element.Schema, scheme, StringComparison.CurrentCultureIgnoreCase) == 0)
                    {
                        ret = element.CreateObject() as IFileClient;
                    }
                }
            }

            if (ret == null)
            {
                throw new NotSupportedException($"The scheme '{scheme}' is not supported.");
            }
           
            return ret;
        }
        #endregion
    }
}