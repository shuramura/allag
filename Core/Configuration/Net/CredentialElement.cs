﻿#region Using directives
using System.Configuration;
using System.Net;

#endregion

namespace Allag.Core.Configuration.Net
{
    public class CredentialElement : ConfigurationElement
    {
        #region Properties
        [ConfigurationProperty("userName", IsRequired = true)]
        public string UserName => (string) base["userName"];

        [ConfigurationProperty("domain", IsRequired = false)]
        public string Domain => (string) base["domain"];

        [ConfigurationProperty("password", IsRequired = false)]
        public string Password => (string) base["password"];

        public NetworkCredential Account
        {
            get
            {
                NetworkCredential ret = null;
                if (UserName.Length > 0)
                {
                    ret = new NetworkCredential(UserName, Password, Domain);
                }
                return ret;
            }
        }
        #endregion
    }
}