﻿#region Using directives
using System.ComponentModel;
using System.Globalization;
using System.Security.Permissions;
using Allag.Core.Tools;
#endregion

namespace Allag.Core.Configuration.Net
{
    [HostProtection(SecurityAction.LinkDemand, SharedState = true)]
    public class EmailElementConverter : ElementConverter<EmailElement>
    {
        #region Constructors
        public EmailElementConverter()
        {
            AddFromType<IResource>(ConvertFromResource);
            AddToType<IResource>(ConvertToResource);
        }
        #endregion

        #region Overrides of ElementConverter<EmailElement>
        protected override ElementCollection<EmailElement> Elements => CoreCfg.Emails.Items;

        protected override EmailElement CreateElement(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            return new EmailElement(value as string);
        }
        #endregion

        #region Static methods
        private static EmailElement ConvertFromResource(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            return new EmailElement(value as IResource);
        }

        private static object ConvertToResource(ITypeDescriptorContext context, CultureInfo culture, EmailElement value)
        {
            return value.Addresses;
        }
        #endregion
    }
}