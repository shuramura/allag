﻿#region Using directives
using System.Security.Permissions;

#endregion

namespace Allag.Core.Configuration.Net
{
    [HostProtection(SecurityAction.LinkDemand, SharedState = true)]
    public class SmtpElementConverter : ElementConverter<SmtpElement>
    {
        #region Overrides of ElementConverter<EmailElement>
        protected override ElementCollection<SmtpElement> Elements => CoreCfg.Servers.Smtps;
        #endregion
    }
}