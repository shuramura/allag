#region Using directives
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.Configuration.Net
{
    public class SmtpElement : ServerElement
    {
        #region Constructors
        public SmtpElement() : base(25, 100000) {}
        #endregion

        #region Implementation of ServerElement
        public override string UriScheme => Uri.UriSchemeMailto;
        #endregion

        #region Properties
        [ConfigurationProperty("deliveryMethod", IsRequired = false, DefaultValue = SmtpDeliveryMethod.Network)]
        public SmtpDeliveryMethod DeliveryMethod => (SmtpDeliveryMethod)base["deliveryMethod"];

        [ConfigurationProperty("pickupDirectoryLocation", IsRequired = false)]
        public string PickupDirectoryLocation => Path.Combine(Application.Base, (string)base["pickupDirectoryLocation"]);

        [ConfigurationProperty("email", IsRequired = false, DefaultValue = "")]
        public IResource Email => (IResource)base["email"];

        [ConfigurationProperty("displayName", IsRequired = false, DefaultValue = "")]
        public IResource DisplayName => (IResource)base["displayName"];

        [ConfigurationProperty("subject", IsRequired = false, DefaultValue = "")]
        public IResource Subject => (IResource)base["subject"];

        [ConfigurationProperty("isBodyHtml", IsRequired = false, DefaultValue = false)]
        public bool IsBodyHtml => (bool)base["isBodyHtml"];

        [ConfigurationProperty("cc", IsRequired = false, DefaultValue = "")]
        public IResource Cc => (IResource)base["cc"];

        [ConfigurationProperty("bcc", IsRequired = false, DefaultValue = "")]
        public IResource Bcc => (IResource)base["bcc"];

        [ConfigurationProperty("message", IsRequired = false, DefaultValue = "")]
        public IResource Message => (IResource)base["message"];

        [ConfigurationProperty("attachments")]
        [ConfigurationCollection(typeof(ElementCollection<ResourceElement>))]
        public ElementCollection<ResourceElement> Attachments => (ElementCollection<ResourceElement>)base["attachments"];
        #endregion

        #region Methods
        public void InitializeSmtpClient(SmtpClient smtpClient)
        {
            if (smtpClient == null)
            {
                throw new ArgumentNullException(nameof(smtpClient));
            }

            smtpClient.Host = Host;
            smtpClient.Port = Port;
            smtpClient.DeliveryMethod = DeliveryMethod;
            smtpClient.PickupDirectoryLocation = PickupDirectoryLocation;
            smtpClient.EnableSsl = EnableSsl;
            smtpClient.Timeout = Timeout;
            NetworkCredential credential = Credential.Account;
            if (credential != null)
            {
                smtpClient.Credentials = credential;
            }
        }
        #endregion
    }
}