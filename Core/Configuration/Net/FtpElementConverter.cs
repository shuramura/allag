﻿#region Using directives
using System.Security.Permissions;

#endregion

namespace Allag.Core.Configuration.Net
{
    [HostProtection(SecurityAction.LinkDemand, SharedState = true)]
    public class FtpElementConverter : ElementConverter<FtpElement>
    {
        #region Overrides of ElementConverter<FtpElement>
        protected override ElementCollection<FtpElement> Elements => CoreCfg.Servers.Ftps;
        #endregion
    }
}