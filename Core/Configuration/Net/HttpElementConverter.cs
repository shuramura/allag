﻿#region Using directives
using System.Security.Permissions;

#endregion

namespace Allag.Core.Configuration.Net
{
    [HostProtection(SecurityAction.LinkDemand, SharedState = true)]
    public class HttpElementConverter : ElementConverter<HttpElement>
    {
        #region Overrides of ElementConverter<FtpElement>
        protected override ElementCollection<HttpElement> Elements => CoreCfg.Servers.Https;
        #endregion
    }
}