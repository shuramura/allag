#region Using directives
using System.Configuration;

#endregion

namespace Allag.Core.Configuration.Net
{
    public class EmailsSection : Section
    {
        #region Properties
        [ConfigurationProperty("", Options = ConfigurationPropertyOptions.IsDefaultCollection)]
        [ConfigurationCollection(typeof(ElementCollection<EmailElement>))]
        public ElementCollection<EmailElement> Items => (ElementCollection<EmailElement>)base[""];
        #endregion
    }
}
