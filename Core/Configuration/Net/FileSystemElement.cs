﻿#region Using directives
using System;
using System.Configuration;
using System.IO;

#endregion

namespace Allag.Core.Configuration.Net
{
    public class FileSystemElement : ServerElement
    {
        #region Constructors
        public FileSystemElement() : base(-1, 60000) {}
        #endregion

        #region Implementation of ServerElement
        public override string UriScheme => Uri.UriSchemeFile;
        #endregion

        #region Override properties
        public override string BasePath
        {
            get
            {
                string ret = base.BasePath;
                if (string.IsNullOrWhiteSpace(Host))
                {
                    ret = Path.Combine(CoreCfg.General.ApplicationBase, base.BasePath);
                }
                return ret;
            }
        }
        #endregion

        #region Properties
        [ConfigurationProperty("isLocked", DefaultValue = true)]
        public bool IsLocked => (bool)base["isLocked"];
        #endregion

        #region Methods
        public DirectoryInfo GetDirectoryInfo()
        {
           return new DirectoryInfo(BaseUri.LocalPath);
        }
        #endregion

    }
}