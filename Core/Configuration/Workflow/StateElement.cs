﻿using System.Configuration;

namespace Allag.Core.Configuration.Workflow
{
	public class StateElement : Element
	{
		#region Properties
		[ConfigurationProperty("", Options = ConfigurationPropertyOptions.IsDefaultCollection)]
		[ConfigurationCollection(typeof(ElementCollection<TriggerElement>), AddItemName = "trigger", ClearItemsName = "clearTriggers", RemoveItemName = "removeTrigger")]
		public ElementCollection<TriggerElement> Triggers => (ElementCollection<TriggerElement>)base[""];
	    #endregion
	}
}