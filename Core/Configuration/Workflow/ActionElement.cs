﻿using System.Configuration;
using Allag.Core.Configuration.Reflection;

namespace Allag.Core.Configuration.Workflow
{
	public class ActionElement : ObjectElement
	{
		#region Properties
		[ConfigurationProperty("strict", IsRequired = false, DefaultValue = true)]
		public bool IsStrict => (bool)this["strict"];
	    #endregion
	}
}