﻿using System.Configuration;

namespace Allag.Core.Configuration.Workflow
{
	public class TriggerElement : Element
	{
		#region Properties
		[ConfigurationProperty("nextState", IsRequired = false)]
		public string NewState => (string)this["nextState"];

	    [ConfigurationProperty("action", IsRequired = false)]
		public string Action => (string)this["action"];

	    [ConfigurationProperty("actions")]
		[ConfigurationCollection(typeof(ElementCollection<Element>))]
		public ElementCollection<Element> Actions => (ElementCollection<Element>)base["actions"];
	    #endregion
	}
}