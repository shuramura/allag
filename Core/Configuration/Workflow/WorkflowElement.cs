﻿using System.Configuration;

namespace Allag.Core.Configuration.Workflow
{
	public class WorkflowElement : Element
	{
		#region Properties
		[ConfigurationProperty("", Options = ConfigurationPropertyOptions.IsDefaultCollection)]
		[ConfigurationCollection(typeof(ElementCollection<StateElement>), AddItemName = "state", ClearItemsName = "clearStates", RemoveItemName = "removeState")]
		public ElementCollection<StateElement> States => (ElementCollection<StateElement>)base[""];

	    [ConfigurationProperty("actions")]
		[ConfigurationCollection(typeof(ElementCollection<ActionElement>))]
		public ElementCollection<ActionElement> Actions => (ElementCollection<ActionElement>)base["actions"];

	    [ConfigurationProperty("preExecutionActions")]
		[ConfigurationCollection(typeof(ElementCollection<Element>))]
		public ElementCollection<Element> PreExecutedActions => (ElementCollection<Element>)base["preExecutionActions"];

	    [ConfigurationProperty("postExecutionActions")]
		[ConfigurationCollection(typeof(ElementCollection<Element>))]
		public ElementCollection<Element> PostExecutedActions => (ElementCollection<Element>)base["postExecutionActions"];
	    #endregion
	}
}