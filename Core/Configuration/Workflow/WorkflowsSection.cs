﻿using System.Configuration;

namespace Allag.Core.Configuration.Workflow
{
	public class WorkflowsSection : Section
	{
		#region Properties
		[ConfigurationProperty("", Options = ConfigurationPropertyOptions.IsDefaultCollection)]
		[ConfigurationCollection(typeof(ElementCollection<WorkflowElement>), AddItemName = "workflow", ClearItemsName = "clearWorkflows", RemoveItemName = "removeWorkflow")]
		public ElementCollection<WorkflowElement> Items => (ElementCollection<WorkflowElement>)base[""];
	    #endregion
	}
}