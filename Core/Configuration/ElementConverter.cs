﻿#region Using directives
using System.ComponentModel;
using System.Globalization;
using System.Security.Permissions;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.Configuration
{
    [HostProtection(SecurityAction.LinkDemand, SharedState = true)]
    public abstract class ElementConverter<TElement> : BaseTypeConverter<TElement>
        where TElement : Element, new()
    {
        #region Abstract properties
        protected abstract ElementCollection<TElement> Elements { get; }
        #endregion

        #region Override methods
        protected override TElement ConvertFromString(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            return Elements[(string)value] ?? CreateElement(context, culture, value);
        }

        protected override object ConvertToString(ITypeDescriptorContext context, CultureInfo culture, TElement value)
        {
            return value.Name;
        }
        #endregion

        #region Methods
        protected virtual TElement CreateElement(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            return Elements[string.Empty] ?? new TElement();
        }
        #endregion
    }
}