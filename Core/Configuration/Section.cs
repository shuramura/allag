#region Using directives

using System.Configuration;
using System.Xml;
#endregion

namespace Allag.Core.Configuration
{
    public abstract class Section : ConfigurationSection
    {
        #region Properties
        public virtual string DefaultName
        {
            get
            {
                string ret = GetType().Name;
                ret = ret.Substring(0, ret.Length - typeof(Section).Name.Length);
                return ret.Substring(0, 1).ToLower() + ret.Substring(1);
            }
        }
        #endregion

        #region Override methods
        protected override void DeserializeSection(XmlReader reader)
        {
            BaseElement.Deserialize(reader, (xmlReader, isExternal) =>
                {
                    if (isExternal)
                    {
                        DeserializeSection(xmlReader);
                    }
                    else
                    {
                        base.DeserializeSection(xmlReader);
                    }
                });
        }
        #endregion
    }
}
