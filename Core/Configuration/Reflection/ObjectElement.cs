#region Using directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using Allag.Core.Reflection;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.Configuration.Reflection
{
    public class ObjectElement<TKey> : Element<TKey>
    {
        #region Constants
        private const char MODULE_DELIMITER = ':';
        #endregion

        #region Variables
        private Type _interface;
        private Type _owner;
        #endregion

        #region Constructors
        protected ObjectElement()
        {
            _interface = null;
            _owner = null;
        }
        #endregion

        #region Properties
        [ConfigurationProperty("", Options = ConfigurationPropertyOptions.IsDefaultCollection)]
        [ConfigurationCollection(typeof(ElementCollection<ParameterElement>))]
        public ElementCollection<ParameterElement> Parameters => (ElementCollection<ParameterElement>)base[""];

        [ConfigurationProperty("members")]
        [ConfigurationCollection(typeof(ElementCollection<MemberElement>))]
        public ElementCollection<MemberElement> Members => (ElementCollection<MemberElement>)base["members"];

        public Type Interface
        {
            get
            {
                if (_interface == null)
                {
                    string strType = Name.ToString();
                    int nIndex = strType.IndexOf(MODULE_DELIMITER);
                    if (nIndex != -1)
                    {
                        strType = strType.Substring(nIndex + 1);
                    }
                    _interface = Type.GetType(strType, true);
                }
                return _interface;
            }
        }

        public Type Owner
        {
            get
            {
                if (_owner == null)
                {
                    int nIndex = Name.ToString().IndexOf(MODULE_DELIMITER);
                    if (nIndex != -1)
                    {
                        _owner = Type.GetType(Name.ToString().Substring(0, nIndex), true);
                    }
                }
                return _owner;
            }
        }

        [ConfigurationProperty("type", IsRequired = false)]
        [TypeConverter(typeof(TypeTypeConverter))]
        public virtual Type Type => (Type)base["type"];
        #endregion

        #region Methods
        public virtual object CreateObject(object args = null)
        {
            object ret = null;
            if (Type != null)
            {
                IReadOnlyDictionary<string, object> parameters = args.ToDictionary();
                ret = InitializeObject(Owner, Parameters.Count == 0 && parameters.Count == 0 ? Activator.CreateInstance(Type) : CreateObject(Owner, Type, Parameters, parameters), Members);
            }
            return ret;
        }
        #endregion

        #region Static methods
        public static object CreateObject(Type owner, Type type, ElementCollection<ParameterElement> parameters, IReadOnlyDictionary<string, object> constantParameters = null)
        {
            using(LogBlock.New<ObjectElement<TKey>>(new {owner, type}))
            {
                if (type == null)
                {
                    throw new ArgumentNullException(nameof(type));
                }

                if (parameters == null)
                {
                    throw new ArgumentNullException(nameof(parameters));
                }

                if (constantParameters == null)
                {
                    constantParameters = new Dictionary<string, object>();
                }

                return InvokeMethod(owner, type, "Ctor", parameters, constantParameters, type.GetConstructorByParameterNames, (constructorInfo, args) => constructorInfo.Invoke(args));
            }
        }

        public static object InitializeObject(Type owner, object instance, ElementCollection<MemberElement> members)
        {
            using(LogBlock.New<ObjectElement<TKey>>(new {owner, instance}))
            {
                if (instance == null)
                {
                    throw new ArgumentNullException(nameof(instance));
                }

                if (members == null)
                {
                    throw new ArgumentNullException(nameof(members));
                }

                Type type = instance.GetType();
                if (members.Count > 0)
                {
                    ISupportInitialize supportInitialize = instance as ISupportInitialize;
                    supportInitialize?.BeginInit();

                    foreach (ParameterElement parameterElement in members)
                    {
                        MemberInfo[] memberInfos = type.GetMember(parameterElement.Name, MemberTypes.Field | MemberTypes.Method | MemberTypes.Property, BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.IgnoreCase);
                        if (memberInfos.Length == 0)
                        {
                            throw new InvalidOperationException($"There is no method '{parameterElement.Name}' in the type '{type.FullName}.");
                        }
                        switch (memberInfos[0].MemberType)
                        {
                            case MemberTypes.Property:
                                PropertyInfo propertyInfo = ((PropertyInfo)memberInfos[0]);
                                object[] indexes = GetIndexes(owner, propertyInfo, parameterElement.Indexes);
                                if (parameterElement.CreationType == CreationType.Init)
                                {
                                    object value = propertyInfo.GetValue(instance, indexes);
                                    if (value != null)
                                    {
                                        InitializeObject(owner, value, parameterElement.Members);
                                    }
                                }
                                else
                                {
                                    propertyInfo.SetValue(instance, parameterElement.GetValue(owner, ((PropertyInfo)memberInfos[0]).PropertyType), indexes);
                                }
                                break;
                            case MemberTypes.Field:
                                FieldInfo fieldInfo = ((FieldInfo)memberInfos[0]);
                                if (parameterElement.CreationType == CreationType.Init)
                                {
                                    object value = fieldInfo.GetValue(instance);
                                    if (value != null)
                                    {
                                        InitializeObject(owner, value, parameterElement.Members);
                                    }
                                }
                                else
                                {
                                    fieldInfo.SetValue(instance, parameterElement.GetValue(owner, ((FieldInfo)memberInfos[0]).FieldType));
                                }
                                break;
                            case MemberTypes.Method:
                                Type[] types = parameterElement.Members.Select(x => x.Type).ToArray();
                                InvokeMethod(owner, type, memberInfos[0].Name, parameterElement.Parameters, new Dictionary<string, object>(),
                                    parameterList =>
                                    {
                                        MethodInfo methodInfo = (MethodInfo)Array.ConvertAll(memberInfos, input => (MethodBase)input).GetMethodByParameterNames(parameterList);
                                        if (methodInfo.ContainsGenericParameters)
                                        {
                                            methodInfo = methodInfo.MakeGenericMethod(types);
                                        }
                                        return methodInfo;
                                    },
                                    (methodBase, args) => methodBase.Invoke(instance, args));
                                break;
                        }
                    }

                    supportInitialize?.EndInit();
                }
                return instance;
            }
        }

        private static object[] GetIndexes(Type owner, PropertyInfo propertyInfo, ElementCollection<ParameterElement> indexes)
        {
            object[] ret = null;
            ParameterInfo[] parameterInfos = propertyInfo.GetIndexParameters();
            if (parameterInfos.Length > 0)
            {
                ret = new object[parameterInfos.Length];
                for (int i = 0; i < ret.Length; i++)
                {
                    ParameterElement element = indexes[parameterInfos[i].Name];
                    if (element == null)
                    {
                        throw new InvalidDataException($"The index '{parameterInfos[i].Name}' is not specifies.");
                    }
                    ret[i] = element.GetValue(owner, parameterInfos[i].ParameterType);
                }
            }
            return ret;
        }

        private static object InvokeMethod<TMethod>(Type owner, Type type, string methodName, ICollection parameters, IReadOnlyDictionary<string, object> constantParameters,
            Func<string[], TMethod> getMethodHandler, Func<TMethod, object[], object> invokeMethodHandler)
            where TMethod : MethodBase
        {
            Dictionary<string, ParameterElement> parameterElements = new Dictionary<string, ParameterElement>(parameters.Count * 2);
            List<string> parameterList = new List<string>(constantParameters.Keys);
            foreach (ParameterElement parameterElement in parameters)
            {
                if (!constantParameters.ContainsKey(parameterElement.Name))
                {
                    parameterList.Add(parameterElement.Name);
                    parameterElements.Add(parameterElement.Name, parameterElement);
                }
            }

            string[] parameterNames = parameterList.ToArray();
            TMethod methodInfo = getMethodHandler(parameterNames);
            if (methodInfo == null)
            {
                string strParams = parameterNames.Join(", ", "'");
                throw new InvalidOperationException($"There is no method '{methodName}' with parameters {strParams} in the type '{type.FullName}'.");
            }
            ParameterInfo[] parameterInfos = methodInfo.GetParameters();
            object[] args = new object[parameterInfos.Length];
            for (int i = 0; i < parameterInfos.Length; i++)
            {
                ParameterInfo parameterInfo = parameterInfos[i];
                object value;
                if (!constantParameters.TryGetValue(parameterInfo.Name, out value))
                {
                    ParameterElement parameterElement;
                    if (parameterElements.TryGetValue(parameterInfo.Name, out parameterElement))
                    {
                        value = parameterElement.GetValue(owner, parameterInfo.ParameterType);
                    }
                    else
                    {
                        value = parameterInfo.DefaultValue;
                    }
                }
                args[i] = value;
            }

            return invokeMethodHandler(methodInfo, args);
        }
        #endregion
    }

    public class ObjectElement : ObjectElement<string> {}
}