﻿#region Using directives
using System;
using System.ComponentModel;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.Configuration.Reflection
{
    public class InstanceElement : ObjectElement<Type>
    {
        #region Override properties
        [TypeConverter(typeof(TypeTypeConverter))]
        public override Type Name
        {
            get { return base.Name; }
            set { base.Name = value; }
        }

        public override Type Type => base.Name;
        #endregion
    }
}