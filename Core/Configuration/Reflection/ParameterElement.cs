#region Using directives
using System;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using Allag.Core.Reflection;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.Configuration.Reflection
{
    public class ParameterElement : NameValueElement
    {
        #region Properties
        [ConfigurationProperty("", Options = ConfigurationPropertyOptions.IsDefaultCollection)]
        [ConfigurationCollection(typeof(ElementCollection<ParameterElement>))]
        public ElementCollection<ParameterElement> Parameters => (ElementCollection<ParameterElement>)base[""];

        [ConfigurationProperty("members")]
        [ConfigurationCollection(typeof(ElementCollection<MemberElement>))]
        public ElementCollection<MemberElement> Members => (ElementCollection<MemberElement>)base["members"];

        [ConfigurationProperty("indexes")]
        [ConfigurationCollection(typeof(ElementCollection<ParameterElement>))]
        public ElementCollection<ParameterElement> Indexes => (ElementCollection<ParameterElement>)base["indexes"];

        [ConfigurationProperty("type", IsRequired = false)]
        [TypeConverter(typeof(TypeTypeConverter))]
        public Type Type => (Type)this["type"];

        [ConfigurationProperty("isNull", IsRequired = false)]
        public bool IsNull => (bool)this["isNull"];

        [ConfigurationProperty("creationType", IsRequired = false, DefaultValue = CreationType.New)]
        public CreationType CreationType => (CreationType)this["creationType"];

        [ConfigurationProperty("owner", IsRequired = false)]
        public string StringOwner => (string)this["owner"];

        private Type Owner
        {
            get
            {
                Type ret = null;
                if (StringOwner.Length > 0)
                {
                    ret = Type.GetType(StringOwner, true);
                }
                return ret;
            }
        }
        #endregion

        #region Methods
        public object GetValue(Type owner, Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            type = Type ?? type;
            owner = Owner ?? owner;

            object ret = null;
            if (!IsNull)
            {
                if (Parameters.Count == 0)
                {
                    ret = GetValue(owner, type, Value);
                }
                else
                {
                    ret = ObjectElement.CreateObject(owner, type, Parameters);
                }
            }
            if (ret != null)
            {
                ret = ObjectElement.InitializeObject(owner, ret, Members);
            }
            return ret;
        }

        private object GetValue(Type owner, Type type, string value)
        {
            object ret;
            TypeConverter typeConverter = TypeDescriptor.GetConverter(type);
            
            if (typeConverter.CanConvertFrom(typeof(string)))
            {
                ret = typeConverter.ConvertFromString(value);
            }
            else if (type == typeof(Type))
            {
                ret = Type.GetType(value);
            }
            else if (type.IsAbstract)
            {
                if (type.FullName.StartsWith("System.Collections.Generic.IEnumerable`1["))
                {
                    ret = new ArrayTypeConverter(type.GetGenericArguments()[0]).ConvertFrom(null, CultureInfo.InvariantCulture, value);
                }
                else
                {
                    ret = (CreationType == CreationType.New ? ObjectFactory.Create(type, owner) : ObjectFactory.Get(type, owner)) ?? typeConverter.ConvertFromString(value);
                }
            }
            else if (type.IsArray)
            {
                if (Members.Count == 0)
                {
                    ret = new ArrayTypeConverter(type.GetElementType()).ConvertFromString(value);
                }
                else
                {
                    int[] lengths = (int[])new ArrayTypeConverter<int>().ConvertFromString(value);
                    ret = Array.CreateInstance(type.GetElementType(), lengths);
                }
            }
            else
            {
                ret = Activator.CreateInstance(type);
            }
            return ret;
        }
        #endregion
    }
}