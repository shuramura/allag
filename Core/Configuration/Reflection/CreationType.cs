﻿namespace Allag.Core.Configuration.Reflection
{
    public enum CreationType
    {
        New,
        Copy,
        Init
    }
}