#region Using directives
using System;

#endregion

namespace Allag.Core.Configuration.Reflection
{
    public class MemberElement : ParameterElement
    {
        #region Variables
        private readonly Guid _key;
        #endregion

        #region Constructors
        public MemberElement()
        {
            _key = Guid.NewGuid();
        }
        #endregion

        #region Override properties
        public override object Key => _key;
        #endregion
    }
}