#region Using directives

using System.Configuration;

#endregion

namespace Allag.Core.Configuration.Reflection
{
    public class ObjectsSection : Section
    {
        #region Properties

        [ConfigurationProperty("", Options = ConfigurationPropertyOptions.IsDefaultCollection)]
        [ConfigurationCollection(typeof(ElementCollection<ObjectElement>))]
        public ElementCollection<ObjectElement> Items => (ElementCollection<ObjectElement>)base[""];
        #endregion
    }
}
