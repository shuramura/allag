﻿#region Using directives
using Allag.Core.Configuration.Job;
using Allag.Core.Configuration.Net;
using Allag.Core.Configuration.Reflection;
using Allag.Core.Configuration.Service;
using Allag.Core.Configuration.Workflow;
#endregion

namespace Allag.Core.Configuration
{
    public static class CoreCfg
    {
        #region Constructors
        static CoreCfg()
        {
            Root = SectionGroup.Instance.GetRoot("allag.core");
        }
        #endregion

        #region Properties
        public static SectionGroup Root { get; }

        public static ObjectsSection Objects => Root.GetSection<ObjectsSection>();

        public static GeneralSection General => Root.GetSection<GeneralSection>();

        public static ServicesSection Services => Root.GetSection<ServicesSection>();

        public static JobsSection Jobs => Root.GetSection<JobsSection>();

        public static WorkflowsSection Workflows => Root.GetSection<WorkflowsSection>();

        public static ServersSection Servers => Root.GetSection<ServersSection>();

        public static EmailsSection Emails => Root.GetSection<EmailsSection>();
        #endregion
    }
}