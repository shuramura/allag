﻿#region Using directives
using System;
using System.Configuration;

#endregion

namespace Allag.Core.Configuration
{
    public class GeneralSection : Section
    {
        #region Properties
        [ConfigurationProperty("applicationBase", IsRequired = false, DefaultValue = "")]
        public string ApplicationBase
        {
            get
            {
                string ret = (string)base["applicationBase"];
                if (string.IsNullOrWhiteSpace(ret))
                {
                    ret = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
                }
                return ret;
            }
        }
        #endregion
    }
}