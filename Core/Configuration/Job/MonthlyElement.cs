#region Using directives
using System.Configuration;
using Allag.Core.Tools.Job;
using Allag.Core.Tools.Job.IntervalProviders;
using DayOfWeek = System.DayOfWeek;

#endregion

namespace Allag.Core.Configuration.Job
{
    public class MonthlyElement : SimpleElement
    {
        #region Implementation of SimpleElement<Monthly>
        public override IIntervalProvider GetProvider(ScheduleType scheduleType)
        {
            return new Monthly(Day, NumberOfWeek, DayOfWeek, Months, Every, UseMonths)
                {
                    StartTime = Start,
                    RunImmediately = RunImmediately
                };
        }
        #endregion

        #region Properties
        [ConfigurationProperty("day", IsRequired = false, DefaultValue = 1)]
        [IntegerValidator(MinValue = 0, MaxValue = 31)]
        public int Day => (int)this["day"];

        [ConfigurationProperty("numberOfWeek", IsRequired = false, DefaultValue = NumberOfWeek.First)]
        public NumberOfWeek NumberOfWeek => (NumberOfWeek)this["numberOfWeek"];

        [ConfigurationProperty("dayOfWeek", IsRequired = false, DefaultValue = DayOfWeek.Monday)]
        public DayOfWeek DayOfWeek => (DayOfWeek)this["dayOfWeek"];

        [ConfigurationProperty("months", IsRequired = false, DefaultValue = Month.All)]
        public Month Months => (Month)this["months"];

        [ConfigurationProperty("useMonths", IsRequired = false, DefaultValue = true)]
        public bool UseMonths => (bool)this["useMonths"];
        #endregion
    }
}