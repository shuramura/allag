#region Using directives
using System.Configuration;
using Allag.Core.Tools.Job;
using Allag.Core.Tools.Job.IntervalProviders;
using DayOfWeek = System.DayOfWeek;

#endregion

namespace Allag.Core.Configuration.Job
{
    public class WeeklyElement : SimpleElement
    {
        #region Implementation of SimpleElement<Weekly>
        public override IIntervalProvider GetProvider(ScheduleType scheduleType)
        {
            return new Weekly(DayOfWeek, Every)
                {
                    StartTime = Start,
                    RunImmediately = RunImmediately
                };
        }
        #endregion

        #region Properties
        [ConfigurationProperty("dayOfWeek", IsRequired = false, DefaultValue = DayOfWeek.Monday)]
        public DayOfWeek DayOfWeek => (DayOfWeek)this["dayOfWeek"];
        #endregion
    }
}