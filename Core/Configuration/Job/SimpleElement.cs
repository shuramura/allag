#region Using directives
using System;
using System.Configuration;
using Allag.Core.Tools.Job;
using Allag.Core.Tools.Job.IntervalProviders;

#endregion

namespace Allag.Core.Configuration.Job
{
    public class SimpleElement : BaseIntervalElement
    {
        #region Implementation of BaseIntervalElement<Simple>
        public override IIntervalProvider GetProvider(ScheduleType scheduleType)
        {
            return new Simple(scheduleType, Every)
                {
                    StartTime = Start,
                    StopTime = Stop,
                    RunImmediately = RunImmediately
                };
        }
        #endregion

        #region Properties
        [ConfigurationProperty("every", IsRequired = false, DefaultValue = 1.0)]
        [CallbackValidatorAttribute(CallbackMethodName = "ValidateEveryParameter", Type = typeof(SimpleElement))]
        public double Every => (double)this["every"];
        #endregion

        #region Methods
        public static void ValidateEveryParameter(object value)
        {
            using (LogBlock.New<SimpleElement>(new { value }))
            {
                if (((double)value) <= 0)
                {
                    throw new ArgumentException("The parameter should be greater then zero.", nameof(value));
                }
            }
        }
        #endregion
    }
}