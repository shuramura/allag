#region Using directives
using System.Configuration;
using Allag.Core.Configuration.Reflection;
using Allag.Core.Tools.Job;

#endregion

namespace Allag.Core.Configuration.Job
{
    public class JobElement : ObjectElement
    {
        #region Properties
        [ConfigurationProperty("scheduleType", IsRequired = false, DefaultValue = ScheduleType.None)]
        public ScheduleType ScheduleType => (ScheduleType)this["scheduleType"];

        [ConfigurationProperty("scheduleName", IsRequired = false)]
        public string ScheduleName => (string)this["scheduleName"];

        [ConfigurationProperty("dependents", IsRequired = false)]
        [ConfigurationCollection(typeof(ElementCollection<DependentJobElement>))]
        public ElementCollection<DependentJobElement> Dependents => (ElementCollection<DependentJobElement>)base["dependents"];
        #endregion
    }
}