#region Using directives

using System.Configuration;
using Allag.Core.Configuration.Reflection;

#endregion

namespace Allag.Core.Configuration.Job
{
    public class JobsSection : Section
    {
        #region Properties

        [ConfigurationProperty("", Options = ConfigurationPropertyOptions.IsDefaultCollection)]
        [ConfigurationCollection(typeof(ElementCollection<JobElement>))]
        public ElementCollection<JobElement> Items => (ElementCollection<JobElement>)base[""];

        [ConfigurationProperty("groups")]
        [ConfigurationCollection(typeof(ElementCollection<JobElement>))]
        public ElementCollection<GroupElement> Groups => (ElementCollection<GroupElement>)base["groups"];

        [ConfigurationProperty("simpleSchedule")]
        [ConfigurationCollection(typeof(ElementCollection<SimpleElement>))]
        public ElementCollection<SimpleElement> SimpleSchedule => (ElementCollection<SimpleElement>)base["simpleSchedule"];

        [ConfigurationProperty("daily")]
        [ConfigurationCollection(typeof(ElementCollection<DailyElement>))]
        public ElementCollection<DailyElement> Daily => (ElementCollection<DailyElement>)base["daily"];

        [ConfigurationProperty("weekly")]
        [ConfigurationCollection(typeof(ElementCollection<WeeklyElement>))]
        public ElementCollection<WeeklyElement> Weekly => (ElementCollection<WeeklyElement>)base["weekly"];

        [ConfigurationProperty("monthly")]
        [ConfigurationCollection(typeof(ElementCollection<SimpleElement>))]
        public ElementCollection<MonthlyElement> Monthly => (ElementCollection<MonthlyElement>)base["monthly"];

        [ConfigurationProperty("fileSystem")]
        [ConfigurationCollection(typeof(ElementCollection<FileSystemElement>))]
        public ElementCollection<FileSystemElement> FileSystem => (ElementCollection<FileSystemElement>)base["fileSystem"];

        [ConfigurationProperty("custom")]
        [ConfigurationCollection(typeof(ElementCollection<ObjectElement>))]
        public ElementCollection<ObjectElement> Custom => (ElementCollection<ObjectElement>)base["custom"];
        #endregion
    }
}
