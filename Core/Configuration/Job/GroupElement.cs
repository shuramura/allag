﻿#region Using directives
using System.Configuration;

#endregion

namespace Allag.Core.Configuration.Job
{
    public class GroupElement : Element
    {
        #region Properties
        [ConfigurationProperty("", Options = ConfigurationPropertyOptions.IsDefaultCollection)]
        [ConfigurationCollection(typeof(ElementCollection<JobElement>))]
        public ElementCollection<JobElement> Items => (ElementCollection<JobElement>)base[""];
        #endregion
    }
}