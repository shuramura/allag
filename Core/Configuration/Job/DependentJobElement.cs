﻿#region Using directives
using System.Configuration;
using Allag.Core.Configuration.Reflection;

#endregion

namespace Allag.Core.Configuration.Job
{
    public class DependentJobElement : ObjectElement
    {
        #region Properties
        [ConfigurationProperty("synchronized", IsRequired = false, DefaultValue = true)]
        public bool Synchronized => (bool)this["synchronized"];
        #endregion
    }
}