#region Using directives
using System.Configuration;
using Allag.Core.Tools.Job;
using Allag.Core.Tools.Job.IntervalProviders;

#endregion

namespace Allag.Core.Configuration.Job
{
    public class DailyElement : SimpleElement
    {
        #region #region Implementation of BaseIntervalElement<Daily>
        public override IIntervalProvider GetProvider(ScheduleType scheduleType)
        {
            return new Daily(DayOfWeeks, Every, UseDayOfWeeks)
                {
                    StartTime = Start,
                    RunImmediately = RunImmediately
                };
        }
        #endregion

        #region Properties
        [ConfigurationProperty("dayOfWeeks", IsRequired = false, DefaultValue = DayOfWeek.All)]
        public DayOfWeek DayOfWeeks => (DayOfWeek)this["dayOfWeeks"];

        [ConfigurationProperty("useDayOfWeeks", IsRequired = false, DefaultValue = true)]
        public bool UseDayOfWeeks => (bool)this["useDayOfWeeks"];
        #endregion
    }
}