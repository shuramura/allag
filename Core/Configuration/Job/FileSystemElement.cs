﻿#region Using directives
using System.Configuration;
using System.IO;
using Allag.Core.Tools.Job;
using Allag.Core.Tools.Job.IntervalProviders;

#endregion

namespace Allag.Core.Configuration.Job
{
    public class FileSystemElement : BaseIntervalElement
    {
        #region Implementation of BaseIntervalElement
        public override IIntervalProvider GetProvider(ScheduleType scheduleType)
        {
            return new FileSystem(Path, Mask)
                {
                    StartTime = Start,
                    StopTime = Stop,
                    RunImmediately = RunImmediately,
                    Filter = Filter,
                    EventBuffer = EventBuffer,
                    Subdirectories = Subdirectories
                };
        }
        #endregion

        #region Properties
        [ConfigurationProperty("path", IsRequired = true)]
        public string Path => System.IO.Path.Combine(CoreCfg.General.ApplicationBase, (string)base["path"]);

        [ConfigurationProperty("mask", IsRequired = false, DefaultValue = "")]
        public string Mask => (string)this["mask"];

        [ConfigurationProperty("filter", IsRequired = false, DefaultValue = NotifyFilters.LastWrite)]
        public NotifyFilters Filter => (NotifyFilters)this["filter"];

        [ConfigurationProperty("eventBuffer", IsRequired = false, DefaultValue = 4096)]
        public int EventBuffer => (int)this["eventBuffer"];

        [ConfigurationProperty("subdirectories", IsRequired = false, DefaultValue = false)]
        public bool Subdirectories => (bool)this["subdirectories"];
        #endregion

    }
}