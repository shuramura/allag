﻿#region Using directives
using System;
using System.Configuration;
using Allag.Core.Tools.Job;

#endregion

namespace Allag.Core.Configuration.Job
{
    public abstract class BaseIntervalElement : Element
    {
        #region Properties
        [ConfigurationProperty("runImmediately", IsRequired = false, DefaultValue = false)]
        public bool RunImmediately => (bool)this["runImmediately"];

        [ConfigurationProperty("start", IsRequired = false, DefaultValue = "0")]
        public TimeSpan Start => (TimeSpan)this["start"];

        [ConfigurationProperty("stop", IsRequired = false, DefaultValue = "10675199.02:48:05.4775807")]
        public TimeSpan Stop => (TimeSpan)this["stop"];
        #endregion

        #region Abstract methods
        public abstract IIntervalProvider GetProvider(ScheduleType scheduleType);
        #endregion
    }
}