#region Using directives

using System;
using System.Configuration;
using System.IO;

#endregion

namespace Allag.Core.Configuration
{
    public class SectionGroup : ConfigurationSectionGroup
    {
        #region Variables
        private System.Configuration.Configuration _configuration;
        #endregion

        #region Constructors

        static SectionGroup()
        {
            Instance = new SectionGroup(string.Empty);
        }

        public SectionGroup()
        {
            _configuration = null;
        }

        public SectionGroup(string configurationFile)
        {
            if (configurationFile == null)
            {
                throw new ArgumentNullException(nameof(configurationFile));
            }

            if (!File.Exists(configurationFile  + ".config"))
            {
                configurationFile = string.Empty;
            }
            _configuration = ConfigurationManager.OpenExeConfiguration(configurationFile);
        }

        #endregion
        
        #region Static properties
        public static SectionGroup Instance { get; }
        #endregion

        #region Methods
        public SectionGroup GetRoot(string rootName)
        {
            if (string.IsNullOrEmpty(rootName))
            {
                throw new ArgumentException("The parameter cannot be null or empty.", nameof(rootName));
            }

            SectionGroup sectionGroup = (SectionGroup)_configuration.GetSectionGroup(rootName) ?? new SectionGroup();
            sectionGroup._configuration = _configuration;
            return sectionGroup;
        }

        public TSection GetSection<TSection>() where TSection : Section, new()
        {
            return GetSection<TSection>(new TSection().DefaultName);
        }

        public TSection GetSection<TSection>(string sectionName) where TSection : Section, new()
        {
            using(LogBlock.New<SectionGroup>(new {sectionName, sectionType = typeof(TSection)}))
            {
                if (string.IsNullOrEmpty(sectionName))
                {
                    throw new ArgumentException("The parameter cannot be null or empty.", nameof(sectionName));
                }

                TSection section = null;
                try
                {
                    section = (TSection)Sections[sectionName];
                }
                catch (InvalidOperationException exp)
                {
                    LogBlock.Logger.Warning("The section '{0}' is not declared in the configuration file. Exception message is '{1}'.", sectionName, exp.Message);
                }
                return section ?? new TSection();    
            }
        }
        #endregion
    }
}
