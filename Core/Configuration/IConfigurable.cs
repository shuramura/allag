namespace Allag.Core.Configuration
{
    public interface IConfigurable<TElement> where TElement : Element<string>
    {
        TElement Configuration { get; set; }
    }
}