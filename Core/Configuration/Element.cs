#region Using directives
using System;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.Configuration
{
    public abstract class BaseElement : ConfigurationElement
    {
        #region Variables
        private static readonly TypeConverter _typeConverter;
        #endregion

        #region Constructors
        static BaseElement()
        {
            _typeConverter = TypeDescriptor.GetConverter(typeof(IResource));
        }
        #endregion

        #region Abstract properties
        public abstract object Key { get; }
        #endregion

        #region Override methods
        protected override void DeserializeElement(XmlReader reader, bool serializeCollectionKey)
        {
            Deserialize(reader, (xmlReader, isExternal) =>
            {
                if (isExternal)
                {
                    DeserializeElement(xmlReader, serializeCollectionKey);
                }
                else
                {
                    xmlReader.MoveToContent();
                    base.DeserializeElement(xmlReader, serializeCollectionKey);
                }
            });
        }
        #endregion

        #region Static methods
        public static void Deserialize(XmlReader reader, Action<XmlReader, bool> deserializeHandler)
        {
            if (reader == null)
            {
                throw new ArgumentNullException(nameof(reader));
            }

            if (deserializeHandler == null)
            {
                throw new ArgumentNullException(nameof(deserializeHandler));
            }

            reader.MoveToContent();
            string strExternalConfig = reader.GetAttribute("external");
            string strContent = reader.ReadOuterXml();
            bool isExternal = strExternalConfig != null;
            if (isExternal)
            {
                strContent = strExternalConfig;
            }
            IResource resource = (IResource)_typeConverter.ConvertFrom(strContent);
            using(XmlReader internalReader = XmlReader.Create(resource.GetStream(), reader.Settings))
            {
                deserializeHandler(internalReader, isExternal);
            }
        }
        #endregion
    }

    public class Element<TKey> : BaseElement
    {
        #region Properties
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public virtual TKey Name
        {
            get { return (TKey)base["name"]; }
            set { base["name"] = value; }
        }
        #endregion

        #region Override properties
        public override object Key => Name;
        #endregion
    }

    public class Element : Element<string> {}
}