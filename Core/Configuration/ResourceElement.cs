﻿#region Using directives
using System.Configuration;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.Configuration
{
    public class ResourceElement : Element
    {
        #region Properties
        [ConfigurationProperty("value", IsRequired = false, IsKey = true)]
        public IResource Resource => (IResource)base["value"];
        #endregion
    }
}