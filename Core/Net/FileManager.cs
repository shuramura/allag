﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Allag.Core.Configuration;

#endregion

namespace Allag.Core.Net
{
    public class FileManager
    {
        #region Variables
        private readonly Dictionary<string, IFileClient> _fileClients;
        #endregion

        #region Constructors
        public FileManager() : this(null) {}

        public FileManager(string configName)
        {
            _fileClients = new Dictionary<string, IFileClient>();

            DataBuffer = 4096;

            if (!string.IsNullOrWhiteSpace(configName))
            {
                AddFileClient(configName);
            }
        }
        #endregion

        #region Properties
        public uint DataBuffer { get; set; }

        public IDictionary<string, IFileClient> Clients => _fileClients;
        #endregion

        #region Methods
        public IFileClient AddFileClient(string configName)
        {
            using(LogBlock.New<FileManager>(new {configName}))
            {
                if (string.IsNullOrWhiteSpace(configName))
                {
                    throw new ArgumentNullOrEmptyException(nameof(configName));
                }
                Uri uri = new Uri(configName);
                IFileClient client = CoreCfg.Servers.GetFileClient(uri.Scheme, uri.Host);
                _fileClients.Add(configName, client);
                return client;
            }
        }

        public void Download(string sourceFolder, string destinationFolder, string mask, bool isDeleteSource, Func<string, string> filePathTransformer = null)
        {
            LogBlock.Action<FileManager, int>(() => DownloadEnumeration(sourceFolder, destinationFolder, mask, isDeleteSource, filePathTransformer).Count(), new { sourceFolder, destinationFolder, mask, isDeleteSource });

        }

        public IEnumerable<string> DownloadEnumeration(string sourceFolder, string destinationFolder, string mask, bool isDeleteSource, Func<string, string> filePathTransformer = null)
        {
            using (LogBlock.New<FileManager>(new { sourceFolder, destinationFolder, mask, isDeleteSource }))
            {
                destinationFolder = NormalizeFolder(destinationFolder);
                if (!Directory.Exists(destinationFolder))
                {
                    Directory.CreateDirectory(destinationFolder);
                }

                if (filePathTransformer == null)
                {
                    filePathTransformer = filePath => filePath;
                }

                return _fileClients.Values.SelectMany(client => client.Download(sourceFolder, mask, (index, file, inputStream) =>
                {
                    string strFile = filePathTransformer(Path.Combine(destinationFolder, file.FullName));

                    using (FileStream fileStream = File.Create(strFile))
                    {
                        byte[] buffer = new byte[DataBuffer];
                        int nCount;
                        while ((nCount = inputStream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            fileStream.Write(buffer, 0, nCount);
                        }
                    }
                    return strFile;
                }, item => true, item => isDeleteSource));
            }
        }

        public IEnumerable<string> Upload(string sourceFolder, string destinationFolder, string mask)
        {
            using(LogBlock.New<FileManager>(new {sourceFolder, destinationFolder, mask}))
            {
                List<string> ret = new List<string>();
                sourceFolder = NormalizeFolder(sourceFolder);
                string[] files = Directory.GetFiles(sourceFolder, mask);
                foreach (IFileClient client in _fileClients.Values)
                {
                    client.Upload(destinationFolder, index =>
                    {
                        Tuple<string, Stream> item = null;
                        if (index < files.Length)
                        {
                            item = new Tuple<string, Stream>(Path.GetFileName(files[index]), File.OpenRead(files[index]));
                            ret.Add(item.Item1);
                        }
                        return item;
                    }, (i, stream) => stream.Dispose());
                }

                return files.Select(Path.GetFileName);
            }
        }

        public string Upload(Stream sourceStream, string destinationFolder, string fileName)
        {
            return LogBlock.Action<FileManager, string>(() =>
            {
                if (string.IsNullOrEmpty(fileName))
                {
                    throw new ArgumentException("The parameter cannot be null or empty.", nameof(fileName));
                }

                char[] chInvalidFileNameChars = Path.GetInvalidFileNameChars();
                if (fileName.IndexOfAny(chInvalidFileNameChars) != -1)
                {
                    throw new ArgumentException($"The parameter cannot contain following characters: {chInvalidFileNameChars}", nameof(fileName));
                }

                if (sourceStream == null)
                {
                    throw new ArgumentNullException(nameof(sourceStream));
                }

                long lPosition = sourceStream.Position;
                foreach (IFileClient client in _fileClients.Values)
                {
                    sourceStream.Position = lPosition;
                    client.Upload(destinationFolder, index => index == 0 ? new Tuple<string, Stream>(fileName, sourceStream) : null);
                }

                return fileName;
            }, new {destinationFolder, fileName});
        }

        public void DeleteFile(string folder, string fileName)
        {
            using(LogBlock.New<FileManager>(new {folder, fileName}))
            {
                foreach (IFileClient client in _fileClients.Values)
                {
                    client.Delete(folder, fileName);
                }
            }
        }

        public IEnumerable<IItem> GetDirectoryList(string folder, string mask)
        {
            using(LogBlock.New<FileManager>(new {folder, mask}))
            {
                List<IItem> ret = new List<IItem>();
                foreach (IFileClient client in _fileClients.Values)
                {
                    ret.AddRange(client.GetDirectoryList(folder, mask));
                }
                return ret;
            }
        }
        #endregion

        #region Static methods
        public static string NormalizeFolder(string folder)
        {
            if (folder == null)
            {
                throw new ArgumentNullException(nameof(folder));
            }
            return Path.Combine(Application.Base, folder);
        }
        #endregion
    }
}