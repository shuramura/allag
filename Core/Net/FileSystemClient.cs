﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Allag.Core.Configuration;
using Allag.Core.Configuration.Net;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.Net
{
    public class FileSystemClient : IFileClient
    {
        #region Variables
        private DirectoryInfo _rootFolder;
        #endregion

        #region Constructors
        public FileSystemClient() : this(string.Empty) {}
        public FileSystemClient(string configName) : this(CoreCfg.Servers.FileSystems[configName ?? string.Empty]) {}

        public FileSystemClient(FileSystemElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException(nameof(element));
            }

            RootFolder = element.GetDirectoryInfo();
            IsLocked = element.IsLocked;
        }
        #endregion

        #region Implementation of IFileClient
        public IEnumerable<string> Download(string sourceFolder, string mask, Func<int, IItem, Stream, string> saveFileHndler, Func<IItem, bool> isDownloadItemHandler = null, Func<IItem, bool> isDeleteItemHandler = null)
        {
            using(LogBlock.New<FileSystemClient>(new {sourceFolder, mask}))
            {
                if (saveFileHndler == null)
                {
                    throw new ArgumentNullException(nameof(saveFileHndler));
                }

                int nIndex = 0;
                foreach (IItem item in GetDirectoryList(sourceFolder, mask))
                {
                    FileSystemItem fileSystemItem = (FileSystemItem)item;
                    if (item.Type == ItemType.File)
                    {
                        FileInfo fileInfo = (FileInfo)fileSystemItem.Info;
                        string strFile;
                        using (Stream stream = fileInfo.OpenRead())
                        {
                            strFile = saveFileHndler(nIndex++, item, stream);
                            LogBlock.Logger.Info("The file '{0}' was saved. The original name is {1}.", strFile, item.FullName);
                        }
                        if (isDeleteItemHandler != null && isDeleteItemHandler(item))
                        {
                            fileInfo.Delete();
                        }
                        yield return strFile;
                    }
                }
            }
        }

        public void Upload(string destinationFolder, Func<int, Tuple<string, Stream>> getFileSource, Action<int, Stream> releaseResourse)
        {
            using(LogBlock.New<FileSystemClient>(new {destinationFolder}))
            {
                int nIndex = 0;
                DirectoryInfo info = GetFolder(destinationFolder);
                if (!info.Exists)
                {
                    info.Create();
                }
                while (true)
                {
                    Tuple<string, Stream> item = getFileSource(nIndex);
                    if (item != null)
                    {
                        try
                        {
                            using(FileStream stream = File.Create(Path.Combine(info.FullName, item.Item1)))
                            {
                                item.Item2.Seek(0, SeekOrigin.Begin);
                                item.Item2.CopyTo(stream);
                            }
                        }
                        finally
                        {
                            releaseResourse?.Invoke(nIndex, item.Item2);
                            nIndex++;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        public void Delete(string folder, string itemName)
        {
            using(LogBlock.New<FileSystemClient>(new {folder, itemName}))
            {
                DirectoryInfo info = GetFolder(folder);
                info.EnumerateFileSystemInfos(itemName).ForEach(x => x.Delete());
            }
        }

        public IEnumerable<IItem> GetDirectoryList(string folder, string mask)
        {
            using(LogBlock.New<FileSystemClient>(new {folder, mask}))
            {
                if (string.IsNullOrWhiteSpace(mask))
                {
                    throw new ArgumentNullOrEmptyException(nameof(mask));
                }

                try
                {
                    DirectoryInfo info = GetFolder(folder);
                    Regex regex = new Regex(mask, RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.Singleline);
                    return info.EnumerateFileSystemInfos().Where(x => regex.IsMatch(x.Name)).Select(x => new FileSystemItem(_rootFolder, x));
                }
                catch (DirectoryNotFoundException exp)
                {
                    throw new InvalidDataException("The folder does not exist.", exp);
                }
            }
        }
        #endregion

        #region Properties
        public DirectoryInfo RootFolder
        {
            get { return _rootFolder; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }

                _rootFolder = value;
                if (!_rootFolder.Exists)
                {
                    _rootFolder.Create();
                }
            }
        }

        public bool IsLocked { get; set; }
        #endregion

        #region Methods
        private DirectoryInfo GetFolder(string folder)
        {
            if (folder == null)
            {
                throw new ArgumentNullException(nameof(folder));
            }

            DirectoryInfo ret = new DirectoryInfo(Path.Combine(_rootFolder.FullName, ".", folder.TrimStart('/').Replace('/', '\\')));
            if (IsLocked && !ret.FullName.StartsWith(_rootFolder.FullName, true, CultureInfo.CurrentCulture))
            {
                throw new ArgumentOutOfRangeException(nameof(folder), folder, "The folder is out of root folder scope.");
            }
            return ret;
        }
        #endregion
    }
}