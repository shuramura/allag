﻿#region Using directives
using Allag.Core.Configuration;
using Allag.Core.Configuration.Net;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.Net
{
    public class SmtpClientTypeConverter : ConfigurableTypeConverter<SmtpClient, SmtpElement>
    {
        #region Overrides of ConfigurableTypeConverter<SmtpClient,SmtpElement>
        protected override ElementCollection<SmtpElement> Elements => CoreCfg.Servers.Smtps;
        #endregion
    }
}