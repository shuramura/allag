#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Text.RegularExpressions;
using Allag.Core.Configuration;
using Allag.Core.Configuration.Net;

#endregion

namespace Allag.Core.Net
{
    public class FtpClient : IFileClient
    {
        #region Variables
        private readonly FtpElement _ftpElement;
        private RemoteCertificateValidationCallback _remoteCertificateValidationHandler;
        #endregion

        #region Constructors
        public FtpClient() : this(string.Empty) {}

        public FtpClient(string ftpConfigName)
            : this(CoreCfg.Servers.Ftps[ftpConfigName ?? string.Empty]) {}

        public FtpClient(FtpElement ftpElement)
        {
            if (ftpElement == null)
            {
                throw new ArgumentNullException(nameof(ftpElement));
            }

            _ftpElement = ftpElement;
            RemoteCertificateValidationHandler = null;
        }
        #endregion

        #region Implementation of IFileClient
        public IEnumerable<string> Download(string sourceFolder, string mask, Func<int, IItem, Stream, string> saveFileHndler, Func<IItem, bool> isDownloadItemHandler = null, Func<IItem, bool> isDeleteItemHandler = null)
        {
            using(LogBlock.New<FtpClient>(new {sourceFolder, mask}))
            {
                if (saveFileHndler == null)
                {
                    throw new ArgumentNullException(nameof(saveFileHndler));
                }

                int nIndex = 0;
                foreach (IItem item in GetDirectoryList(sourceFolder, mask))
                {
                    if (item.Type == ItemType.File && (isDownloadItemHandler == null || isDownloadItemHandler(item)))
                    {
                        using(FtpWebResponse response = GetResponse(Path.Combine(item.Parent, item.FullName), WebRequestMethods.Ftp.DownloadFile))
                        {
                            using(Stream stream = response.GetResponseStream())
                            {
                                string strFile = saveFileHndler(nIndex++, item, stream);
                                LogBlock.Logger.Info("The file '{0}' was saved. The original name is {1}.", strFile, item.FullName);

                                if (isDeleteItemHandler != null && isDeleteItemHandler(item))
                                {
                                    Delete(item.Parent, item.FullName);
                                }
                                yield return strFile;
                            }
                        }
                    }
                }
            }
        }

        public void Upload(string destinationFolder, Func<int, Tuple<string, Stream>> getFileSource, Action<int, Stream> releaseResourse)
        {
            using(LogBlock.New<FtpClient>(new {destinationFolder}))
            {
                int nIndex = 0;
                while (true)
                {
                    Tuple<string, Stream> item = getFileSource(nIndex);
                    if (item != null)
                    {
                        try
                        {
                            item.Item2.Seek(0, SeekOrigin.Begin);
                            string strPath = GetFtpPath(destinationFolder, item.Item1);
                            using(FtpWebResponse response = GetResponse(strPath, WebRequestMethods.Ftp.UploadFile, delegate(FtpWebRequest request)
                            {
                                using(Stream stream = request.GetRequestStream())
                                {
                                    item.Item2.CopyTo(stream, 1024 * 1024);
                                }
                            }))
                            {
                                LogBlock.Logger.Info("The file '{0}' was uploaded to {1}. The command status is '{2}'.", item.Item1, strPath, response.StatusCode);
                            }
                        }
                        finally
                        {
                            releaseResourse?.Invoke(nIndex, item.Item2);
                            nIndex++;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        public void Delete(string folder, string itemName)
        {
            using(LogBlock.New<FtpClient>(new {folder, fileName = itemName}))
            {
                string strPath = GetFtpPath(folder, itemName);
                using(FtpWebResponse response = GetResponse(strPath, WebRequestMethods.Ftp.DeleteFile))
                {
                    LogBlock.Logger.Info("The file '{0}' was deleted. The command status is '{1}'.", strPath, response.StatusCode);
                }
            }
        }

        public IEnumerable<IItem> GetDirectoryList(string folder, string mask)
        {
            using(LogBlock.New<FtpClient>(new {folder, mask}))
            {
                if (string.IsNullOrWhiteSpace(mask))
                {
                    throw new ArgumentNullOrEmptyException("mask");
                }

                string strPath = GetFtpPath(folder, string.Empty);
                List<IItem> ret = new List<IItem>();

                try
                {
                    using(FtpWebResponse response = GetResponse(strPath, WebRequestMethods.Ftp.ListDirectoryDetails, null, false))
                    {
                        using(StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                        {
                            string itemData = streamReader.ReadLine();
                            LogBlock.Logger.Info("The request's location is {0}.", strPath);
                            Regex regex = new Regex(mask, RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.Singleline);
                            while (itemData != null)
                            {
                                FtpItem item = new FtpItem(this, folder, itemData);
                                if (regex.IsMatch(item.FullName))
                                {
                                    ret.Add(item);
                                    LogBlock.Logger.Info("The item was added (Type: {0}; Name: {1}; Size: {2})", item.Type, item.FullName, item.Size);
                                }

                                itemData = streamReader.ReadLine();
                            }
                        }
                    }
                }
                catch (WebException e)
                {
                    try
                    {
                        FtpStatusCode statusCode = ((FtpWebResponse)e.Response).StatusCode;
                        if (statusCode == FtpStatusCode.ActionNotTakenFileUnavailable || statusCode == FtpStatusCode.Undefined)
                        {
                            using(GetResponse(NormalizeFtpFolder(folder), WebRequestMethods.Ftp.ListDirectoryDetails)) {}
                        }
                    }
                    catch (WebException exp)
                    {
                        throw new InvalidDataException($"The folder '{folder}' does not find.", exp);
                    }
                }

                return ret;
            }
        }
        #endregion

        #region Properties
        public string Host => _ftpElement.Host;

        public int Port => _ftpElement.Port;

        public RemoteCertificateValidationCallback RemoteCertificateValidationHandler
        {
            get { return _remoteCertificateValidationHandler; }
            set
            {
                if (value == null)
                {
                    value = delegate { return true; };
                }
                _remoteCertificateValidationHandler = value;
            }
        }
        #endregion

        #region Methods
        public DateTime GetLastModifiedDateTime(string folder, string item)
        {
            using(LogBlock.New<FtpClient>(new {folder, item}))
            {
                if (string.IsNullOrEmpty(item))
                {
                    throw new ArgumentException("The parameter cannot be null or empty.", nameof(item));
                }

                using(FtpWebResponse response = GetResponse(GetFtpPath(folder, item), WebRequestMethods.Ftp.GetDateTimestamp))
                {
                    return response.LastModified;
                }
            }
        }

        private FtpWebResponse GetResponse(string source, string method, Action<FtpWebRequest> requestHandler = null, bool isLogException = true)
        {
            using(LogBlock.New<FtpClient>(new {source, method}))
            {
                RemoteCertificateValidationCallback callback = null;
                FtpWebRequest ftpWebRequest = null;
                try
                {
                    ftpWebRequest = _ftpElement.GetRequest(source);
                    if (ftpWebRequest.EnableSsl)
                    {
                        callback = ServicePointManager.ServerCertificateValidationCallback;
                        ServicePointManager.ServerCertificateValidationCallback = _remoteCertificateValidationHandler;
                    }

                    ftpWebRequest.Method = method;

                    requestHandler?.Invoke(ftpWebRequest);
                    return (FtpWebResponse)ftpWebRequest.GetResponse();
                }
                catch (Exception e)
                {
                    if (isLogException)
                    {
                        LogBlock.Logger.Error("FtpElement Name: {0}; Host: {1}; Source: {2}; Method: {3}", e, _ftpElement.Name, _ftpElement.Host, source, method);
                    }
                    throw;
                }
                finally
                {
                    if (callback != null && ftpWebRequest.EnableSsl)
                    {
                        ServicePointManager.ServerCertificateValidationCallback = callback;
                    }
                }
            }
        }
        #endregion

        #region Static methods
        public static string GetFtpPath(string folder, string item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            return NormalizeFtpFolder(folder) + item.Trim();
        }

        public static string NormalizeFtpFolder(string folder)
        {
            if (folder == null)
            {
                throw new ArgumentNullException(nameof(folder));
            }

            folder = folder.Trim().Replace('\\', '/');
            if (folder.Length > 0)
            {
                if (folder[0] != '/')
                {
                    folder = '/' + folder;
                }
                if (folder[folder.Length - 1] != '/')
                {
                    folder += '/';
                }
            }

            return folder;
        }
        #endregion
    }
}