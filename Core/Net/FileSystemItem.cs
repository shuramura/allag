﻿#region Using directives
using System;
using System.IO;

#endregion

namespace Allag.Core.Net
{
    public class FileSystemItem : IItem
    {
        #region Variables
        private readonly DirectoryInfo _root;
        #endregion

        #region Constructors
        public FileSystemItem(DirectoryInfo root, FileSystemInfo info)
        {
            if (root == null)
            {
                throw new ArgumentNullException(nameof(root));
            }

            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            Info = info;
            _root = root;
        }
        #endregion

        #region Implementation of IItem
        public string Path => Info.FullName.Substring(_root.FullName.Length).Replace('\\', '/');

        public string Parent
        {
            get
            {
                string strParent = System.IO.Path.GetDirectoryName(Info.FullName);
                if (strParent != null && strParent.Length > _root.FullName.Length)
                {
                    strParent = strParent.Substring(_root.FullName.Length) + '/';
                }
                else
                {
                    strParent = ".";
                }
                return strParent.Replace('\\', '/');
            }
        }

        public ItemType Type => (Info is DirectoryInfo) ? ItemType.Directory : ItemType.File;

        public string FullName => Info.Name;

        public long Size
        {
            get
            {
                long ret = 0;
                FileInfo info = Info as FileInfo;
                if (info != null)
                {
                    ret = info.Length;
                }
                return ret;
            }
        }
        public DateTime LastModified => Info.LastWriteTime;
        #endregion

        #region Properties
        public FileSystemInfo Info { get; }
        #endregion

    }
}