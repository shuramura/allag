#region Using directives
using System;
using System.ComponentModel;
using System.Net.Mail;
using Allag.Core.Configuration;
using Allag.Core.Configuration.Net;

#endregion

namespace Allag.Core.Net
{
    [TypeConverter(typeof(SmtpClientTypeConverter))]
    public class SmtpClient : System.Net.Mail.SmtpClient, IConfigurable<SmtpElement>
    {
        #region Variables
        private SmtpElement _element;
        #endregion

        #region Constructors
        public SmtpClient()
        {
            _element = null;
        }

        public SmtpClient(string smtpServerName) : this(CoreCfg.Servers.Smtps[smtpServerName]) {}

        public SmtpClient(SmtpElement smtpElement) : base(smtpElement.Host)
        {
            Configuration = smtpElement;
        }
        #endregion

        #region Implementation of IConfigurable<SmtpElement>
        public SmtpElement Configuration
        {
            get { return _element; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }

                _element = value;
                _element.InitializeSmtpClient(this);
            }
        }
        #endregion

        #region Methods
        public MailAddress GetMailAddress(object parameters = null)
        {
            return new MailAddress(Configuration.Email.ToString(parameters), Configuration.DisplayName.ToString(parameters));
        }
        #endregion
    }
}