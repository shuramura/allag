#region Using directives
using System;
using System.Text.RegularExpressions;

#endregion

namespace Allag.Core.Net
{
    public class FtpItem : IItem
    {
        #region Constants
        /// <summary>
        ///   List of REGEX formats for different FTP server listing formats
        /// </summary>
        /// <remarks>
        ///   The first three are various UNIX/LINUX formats, fourth is for MS FTP
        ///   in detailed mode and the last for MS FTP in 'DOS' mode.
        /// </remarks>
        private static readonly string[] PARSE_FORMATS = new[]
        {
            @"(?<dir>[\-d])(?<permission>([\-r][\-w][\-xs]){3})\s+\d+\s+\w+\s+\w+\s+(?<size>\d+)\s+(?<timestamp>\w+\s+\d+\s+\d{4})\s+(?<name>.+)",
            @"(?<dir>[\-d])(?<permission>([\-r][\-w][\-xs]){3})\s+\d+\s+\d+\s+(?<size>\d+)\s+(?<timestamp>\w+\s+\d+\s+\d{4})\s+(?<name>.+)",
            @"(?<dir>[\-d])(?<permission>([\-r][\-w][\-xs]){3})\s+\d+\s+\d+\s+(?<size>\d+)\s+(?<timestamp>\w+\s+\d+\s+\d{1,2}:\d{2})\s+(?<name>.+)",
            @"(?<dir>[\-d])(?<permission>([\-r][\-w][\-xs]){3})\s+\d+\s+\w+\s+\w+\s+(?<size>\d+)\s+(?<timestamp>\w+\s+\d+\s+\d{1,2}:\d{2})\s+(?<name>.+)",
            @"(?<dir>[\-d])(?<permission>([\-r][\-w][\-xs]){3})(\s+)(?<size>(\d+))(\s+)(?<ctbit>(\w+\s\w+))(\s+)(?<size2>(\d+))\s+(?<timestamp>\w+\s+\d+\s+\d{2}:\d{2})\s+(?<name>.+)",
            @"(?<timestamp>\d{2}\-\d{2}\-\d{2}\s+\d{2}:\d{2}[Aa|Pp][mM])\s+(?<dir>\<\w+\>){0,1}(?<size>\d+){0,1}\s+(?<name>.+)"
        };
        #endregion

        #region Variables
        private readonly FtpClient _ftpClient;
        private DateTime _dtLastModified;
        #endregion

        #region Constructors
        public FtpItem(FtpClient ftpClient, string parent, string data)
        {
            using(LogBlock.New<FtpItem>())
            {
                LogBlock.Logger.Info("The parameter 'data' is '{0}'.", data);

                if (ftpClient == null)
                {
                    throw new ArgumentNullException(nameof(ftpClient));
                }

                if (parent == null)
                {
                    throw new ArgumentNullException(nameof(parent));
                }

                if (string.IsNullOrEmpty(data))
                {
                    throw new ArgumentException("The parameter cannot be null or empty.", nameof(data));
                }

                Match match = ParseItemInformation(data);
                if (match == null)
                {
                    throw new InvalidOperationException("The data format of item is wrong.");
                }

                _dtLastModified = DateTime.MinValue;

                _ftpClient = ftpClient;
                Parent = FtpClient.NormalizeFtpFolder(parent);
                FullName = match.Groups["name"].Value;

                long lSize;
                long.TryParse(match.Groups["size"].Value, out lSize);
                Size = lSize;

                Type = GetItemType(match.Groups["dir"].Value);
            }
        }
        #endregion

        #region Implementation of IItem
        public string Path => FtpClient.GetFtpPath(Parent, FullName);

        public string Parent { get; }

        public ItemType Type { get; }

        public string FullName { get; }

        public long Size { get; }

        public DateTime LastModified
        {
            get
            {
                if (_dtLastModified == DateTime.MinValue)
                {
                    _dtLastModified = _ftpClient.GetLastModifiedDateTime(Parent, FullName);
                }
                return _dtLastModified;
            }
        }
        #endregion

        #region Methods
        private static Match ParseItemInformation(string data)
        {
            for (int i = 0; i <= PARSE_FORMATS.Length - 1; i++)
            {
                Regex rx = new Regex(PARSE_FORMATS[i]);
                Match m = rx.Match(data);
                if (m.Success)
                {
                    return m;
                }
            }
            return null;
        }

        private static ItemType GetItemType(string data)
        {
            return data != string.Empty && data != "-" ? ItemType.Directory : ItemType.File;
        }
        #endregion
    }
}