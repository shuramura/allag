﻿#region Using directives
using System;

#endregion

namespace Allag.Core.Net
{
    public interface IItem
    {
        string Path { get; }
        string Parent { get; }
        ItemType Type { get; }
        string FullName { get; }
        long Size { get; }
        DateTime LastModified { get; }
    }
}