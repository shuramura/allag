﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;

#endregion

namespace Allag.Core.Net
{
    public interface IFileClient
    {
        IEnumerable<string> Download(string sourceFolder, string mask, Func<int, IItem, Stream, string> saveFileHndler, Func<IItem, bool> isDownloadItemHandler = null, Func<IItem, bool> isDeleteItemHandler = null);
        void Upload(string destinationFolder, Func<int, Tuple<string, Stream>> getFileSource, Action<int, Stream> releaseResourse = null);
        void Delete(string folder, string itemName);
        IEnumerable<IItem> GetDirectoryList(string folder, string mask);
    }
}