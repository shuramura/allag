#region Using directives
using System;
#endregion

namespace Allag.Core.Service
{
    [Flags]
    public enum ServiceAccessType : uint
    {
        /// <summary>
        /// Required to call the QueryServiceConfig and QueryServiceConfig2 
        /// functions to query the service configuration.
        /// </summary>
        QueryConfig = 0x00000001,

        /// <summary>
        /// Required to call the ChangeServiceConfig or ChangeServiceConfig2 
        /// function to change the service configuration. Because this grants 
        /// the caller the right to change the executable file that the system 
        /// runs, it should be granted only to administrators. 
        /// </summary>
        ChangeConfig = 0x00000002,

        /// <summary>
        /// Required to call the QueryServiceStatusEx function to ask 
        /// the service control manager about the status of the service.
        /// </summary>
        QueryStatus = 0x00000004,

        /// <summary>
        /// Required to call the EnumDependentServices function to enumerate 
        /// all the services dependent on the service.
        /// </summary>
        EnumerateDependents = 0x00000008,

        /// <summary>
        /// Required to call the StartService function to start the service.
        /// </summary>
        Start = 0x00000010,

        /// <summary>
        /// Required to call the ControlService function to stop the service.
        /// </summary>
        Stop = 0x00000020,

        /// <summary>
        /// Required to call the ControlService function to pause or continue the service.
        /// </summary>
        PauseContinue = 0x00000040,

        /// <summary>
        /// Required to call the ControlService function to ask 
        /// the service to report its status immediately.
        /// </summary>
        Interrogate = 0x00000080,

        /// <summary>
        /// Required to call the ControlService function to specify 
        /// a user-defined control code.
        /// </summary>
        UserDefinedControl = 0x00000100,

        /// <summary>
        /// Includes STANDARD_RIGHTS_REQUIRED, in addition to all access 
        /// rights in this table.
        /// </summary>
        All = 0x000F0000 | QueryConfig | ChangeConfig | QueryStatus | EnumerateDependents |
                        Start | Stop | PauseContinue | Interrogate | UserDefinedControl,

        /// <summary>
        /// Includes STANDARD_RIGHTS_READ, in addition to all read 
        /// rights in this table.
        /// </summary>
        Read = 0x00020000 | QueryConfig | QueryStatus | Interrogate | EnumerateDependents,

        /// <summary>
        /// Includes STANDARD_RIGHTS_WRITE, in addition to all write 
        /// rights in this table.
        /// </summary>
        Write = 0x00020000 | ChangeConfig,

        /// <summary>
        /// Includes STANDARD_RIGHTS_EXECUTE, in addition to all execute 
        /// rights in this table.
        /// </summary>
        Execute = 0x00020000 | Start | Stop | PauseContinue |UserDefinedControl
    }
}
