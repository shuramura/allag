#region Using directives

using System;
using System.ComponentModel;
using Allag.Core.Native;

#endregion

namespace Allag.Core.Service
{
    public class ServiceControlManager : IDisposable
    {
        #region Variables
        private IntPtr _handle;
        private string _strMachineName;
        #endregion

        #region Constructors

        public ServiceControlManager(SCAccessType accessType) : this(null, accessType) { }

        public ServiceControlManager(string machineName, SCAccessType accessType)
        {
            _handle = SafeNativeMethods.OpenSCManager(machineName, null, (uint)accessType);
            _strMachineName = machineName ?? "Local computer";

            AssertOperation(_handle != IntPtr.Zero, "Can not open service database on the '{0}'.", _strMachineName);
        }

        ~ServiceControlManager()
        {
            Dispose(false);
        }

        #endregion

        #region Implementation of IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region Properties
        public ServiceManager this[string serviceName, ServiceAccessType serviceAccessType]
        {
            get
            {
                CheckDisposed();
                return new ServiceManager(serviceName, _handle, serviceAccessType);
            }
        }

        public string MachineName
        {
            get 
            {
                CheckDisposed();
                return _strMachineName; 
            }
        }

        #endregion

        #region Methods
        public void Close()
        {
            CheckDisposed();

            AssertOperation(SafeNativeMethods.CloseServiceHandle(_handle), "Can not close service database on the '{0}'.", _strMachineName);
            
            _handle = IntPtr.Zero;
        }

        private void Dispose(bool disposing)
        {
            if (_handle != IntPtr.Zero)
            {
                if (disposing)
                {
                    _strMachineName = null;
                }
                Close();
            }
        }

        private void CheckDisposed()
        {
            if (_handle == IntPtr.Zero)
            {
                throw new ObjectDisposedException(typeof(ServiceControlManager).FullName);
            }
        }

        #endregion

        #region Static methods
        private static void AssertOperation(bool isValid, string messageTemplate, params string[] args)
        {
            if (!isValid)
            {
                throw new InvalidOperationException(string.Format(messageTemplate, args), new Win32Exception());
            }
        }
        #endregion
    }
}
