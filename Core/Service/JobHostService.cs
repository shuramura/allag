﻿#region Using directives
using System;
using Allag.Core.Tools.Job;

#endregion

namespace Allag.Core.Service
{
    public class JobHostService : BaseService
    {
        #region Properties
        public TimeSpan StopTimeout { get; set; }
        #endregion

        #region Override methods
        public override void StartAction(params string[] args)
        {
            using (LogBlock.New<JobHostService>())
            {
                base.StartAction(args);
                LogBlock.Logger.Info("Starting the service");

                JobFactory.Start();

                LogBlock.Logger.Info("The service is started");
            }
        }

        public override void StopAction()
        {
            using (LogBlock.New<JobHostService>())
            {
                LogBlock.Logger.Info("Stopping the service");

                JobFactory.Stop(StopTimeout);

                LogBlock.Logger.Info("Service is stopped");
                base.StopAction();
            }
        }
        #endregion
    }
}