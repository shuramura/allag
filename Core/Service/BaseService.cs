#region Using directives

using System.ServiceProcess;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.Service
{
    public class BaseService : ServiceBase
    {
        #region Constructors

        public BaseService()
        {
            IsStarted = false;
            IsConsoleMode = true;
        }

        #endregion

        #region Properties
        public bool IsStarted { get; private set; }

        public bool IsConsoleMode { get; private set; }
        #endregion

        #region Override methods

        protected sealed override void OnStart(string[] args)
        {
            using(LogBlock.New<BaseService>(new {args = args.Join(",", "[", "]")}))
            {
                base.OnStart(args);
                IsConsoleMode = false;

                StartAction(args);    
            }
        }

        protected sealed override void OnStop()
        {
            StopAction();
            IsConsoleMode = true;
            base.OnStop();
        }

        #endregion

        #region Methods
        public virtual void StartAction(params string[] args)
        {
            IsStarted = true;
        }

        public virtual void StopAction()
        {
            IsStarted = false;
        }

        #endregion
    }
}
