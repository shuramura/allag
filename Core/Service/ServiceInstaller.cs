#region Using directives

using System.Collections;
using System.Diagnostics.CodeAnalysis;

#endregion

namespace Allag.Core.Service
{
    [ExcludeFromCodeCoverage]
    public class ServiceInstaller : System.ServiceProcess.ServiceInstaller
    {
        #region Override methods
        public override void Install(IDictionary stateSaver)
        {
            base.Install(stateSaver);

            using (ServiceControlManager controlManager = new ServiceControlManager(SCAccessType.Connect))
            {
                using (ServiceManager serviceManager = controlManager[ServiceName, ServiceAccessType.Read | ServiceAccessType.Write])
                {
                    ServiceInfo info = serviceManager.Info;
                    info.Arguments = ServiceFactory.StartArg + " \"" + ServiceName + '\"';
                    serviceManager.Info = info;
                }
            }
        }
        #endregion
    }
}
