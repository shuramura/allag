#region Using directives
using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using Allag.Core.Native;

#endregion

namespace Allag.Core.Service
{
    public class ServiceManager : IDisposable
    {
        #region Constants
        private const uint NO_CHANGE = 0xFFFFFFFF;
        #endregion

        #region Variables
        private string _strServiceName;
        private IntPtr _handle;
        private ServiceInfo _info;
        #endregion

        #region Constructors
        public ServiceManager(string serviceName, IntPtr databaseHandle, ServiceAccessType accessType)
        {
            if(string.IsNullOrEmpty(serviceName))
            {
                throw new ArgumentException("The parameter must contain a valid service name.", "serviceName");
            }

            if(databaseHandle == IntPtr.Zero)
            {
                throw new ArgumentException("The service database is not opened.");
            }

            _strServiceName = serviceName;
            _handle = SafeNativeMethods.OpenService(databaseHandle, serviceName, (uint) accessType);

            AssertOperation(_handle != IntPtr.Zero, "Can not open the service '{0}'.", _strServiceName);
            
            _info = new ServiceInfo();
        }

        ~ServiceManager()
        {
            Dispose(false);
        }
        #endregion

        #region Implementation of IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Properties
        public string ServiceName
        {
            get
            {
                CheckDisposed();
                return _strServiceName;
            }
        }

        public ServiceInfo Info
        {
            get
            {
                CheckDisposed();

                if(_info.File == null)
                {
                    int nByteCount = 0;
                    IntPtr ptr = Marshal.AllocCoTaskMem(0);
                    int nRetCode = NativeMethods.QueryServiceConfig(_handle, ptr, 0, ref nByteCount);
                    AssertOperation(nRetCode != 0 || nByteCount != 0, "Can not retrieve structure size.");

                    ptr = Marshal.AllocCoTaskMem(nByteCount);
                    nRetCode = NativeMethods.QueryServiceConfig(_handle, ptr, nByteCount, ref nByteCount);

                    AssertOperation(nRetCode != 0, "Can not retrieve configuration of the service '{0}'.", _strServiceName);

                    ServiceConfig serviceConfig = new ServiceConfig
                                                      {
                                                          BinaryPathName = IntPtr.Zero,
                                                          Dependencies = IntPtr.Zero,
                                                          DisplayName = IntPtr.Zero,
                                                          LoadOrderGroup = IntPtr.Zero,
                                                          StartName = IntPtr.Zero
                                                      };
                    
                    serviceConfig = (ServiceConfig) Marshal.PtrToStructure(ptr, new ServiceConfig().GetType());
                    _info = new ServiceInfo(serviceConfig);
                }
                return _info;
            }
            set
            {
                CheckDisposed();
                AssertOperation(NativeMethods.ChangeServiceConfig(_handle, NO_CHANGE, NO_CHANGE, NO_CHANGE,
                                                                  value.BinaryPath, null, IntPtr.Zero, null, null, null, value.DisplayName) != 0,
                                "Can not update configuration of the service '{0}'.", _strServiceName);
                
                _info = new ServiceInfo();
            }
        }
        #endregion

        #region Methods
        public void Close()
        {
            CheckDisposed();

            AssertOperation(SafeNativeMethods.CloseServiceHandle(_handle), "Can not close the service '{0}'.", _strServiceName);
            
            _handle = IntPtr.Zero;
        }

        private void Dispose(bool disposing)
        {
            if(_handle != IntPtr.Zero)
            {
                if(disposing)
                {
                    _strServiceName = null;
                }
                Close();
            }
        }

        private void CheckDisposed()
        {
            if(_handle == IntPtr.Zero)
            {
                throw new ObjectDisposedException(typeof(ServiceManager).FullName);
            }
        }

        #endregion

        #region Static methods
        private static void AssertOperation(bool isValid, string messageTemplate, params string[] args)
        {
            if (!isValid)
            {
                throw new InvalidOperationException(string.Format(messageTemplate, args), new Win32Exception());
            }
        }
        #endregion
    }
}