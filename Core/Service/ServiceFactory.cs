#region Using directives
using System;
using System.Collections.Generic;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using Allag.Core.Configuration;
using Allag.Core.Configuration.Service;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.Service
{
    public static class ServiceFactory
    {
        #region Constants
        public const string ConsoleModeArg = "/console";
        public const string RestartArg = "/restart";
        public const string InstallArg = "/install";
        public const string UninstallArg = "/uninstall";

        public const string StartArg = "/start";
        public const string StopArg = "/stop";
        #endregion

        #region Variables
        private static readonly Dictionary<string, Action<string[]>> _appActions;
        private static readonly List<BaseService> _services;
        #endregion

        #region Constructors
        static ServiceFactory()
        {
            _services = new List<BaseService>();

            foreach(ServiceElement item in CoreCfg.Services.Items)
            {
                BaseService service = (BaseService) item.CreateObject();
                service.ServiceName = item.Name;
                _services.Add(service);
            }


            Func<string[], string[]> serviceNames = args => args != null && args.Length > 0 && !string.IsNullOrWhiteSpace(args.Take(1).Single()) ? args.Take(1).Single().Split(',') : null;
            Func<string[], string[]> arguments = args => args != null && args.Length > 1 ? args.Skip(1).ToArray() : new string[0];

            _appActions = new Dictionary<string, Action<string[]>>
                {
                    {StopArg, args => Stop(serviceNames(args))}, {StartArg, args => Start(serviceNames(args), false, arguments(args))},
                    {
                        ConsoleModeArg, delegate(string[] args)
                            {
                                using(LogBlock.New<BaseService>())
                                {
                                    Start(serviceNames(args), true, arguments(args));
                                    LogBlock.Logger.Info("Host started.");
                                    Console.WriteLine("Press the enter for stopping the host.");
                                    Console.Read();
                                    Stop(serviceNames(args));
                                    LogBlock.Logger.Info("Host stopped.");    
                                }
                            }
                    },
                    {RestartArg, args => Restart(serviceNames(args), arguments(args))}
                };

            if(Application.Root != null)
            {
                string strLocation = Application.Root.Assembly.Location;

                _appActions.Add(InstallArg, delegate { ManagedInstallerClass.InstallHelper(new[] {strLocation}); });
                _appActions.Add(UninstallArg, delegate { ManagedInstallerClass.InstallHelper(new[] {"/u", strLocation}); });
            }
        }
        #endregion

        #region Properties
        public static int Count
        {
            get { return _services.Count; }
        }
        #endregion

        #region Methods
        #endregion

        #region Static methods
        public static void RunApplication(params string[] args)
        {
            using (LogBlock.New(typeof(ServiceFactory), new { args = args.Join(",", "[", "]") }))
            {
                if(args.Length == 0)
                {
                    throw new ArgumentException(string.Format("The count of arguments should be greater than zero.\nThe list of possible arguments: '{0}'.", string.Join("', '", _appActions.Keys)));
                }
                
                Action<string[]> action;
                if(_appActions.TryGetValue(args[0], out action))
                {
                    action(args.Skip(1).ToArray());
                }
                else
                {
                    throw new NotSupportedException(string.Format("The argument '{0}' is not supported.\nThe list of possible arguments: '{1}'.", args[0], string.Join("', '", _appActions.Keys)));
                }
            }
        }

        public static void Start(ICollection<string> serviceNames, bool isConsoleMode, params string[] args)
        {
            using (LogBlock.New(typeof(ServiceFactory), new { serviceNames = serviceNames.Join(",", "[", "]"), isConsoleMode, args = args.Join(",", "[", "]") }))
            {
                lock(_services)
                {
                    List<BaseService> services = GetServices(serviceNames, true);
                    if(services.Count > 0)
                    {
                        if(isConsoleMode)
                        {
                            services.ForEach(service => service.StartAction(args));
                        }
                        else
                        {
                            ServiceBase.Run(services.ToArray());
                        }
                    }
                }
            }
        }

        public static void Stop(ICollection<string> serviceNames)
        {
            using (LogBlock.New(typeof(ServiceFactory), new{serviceNames = serviceNames.Join(",", "[", "]")}))
            {
                lock(_services)
                {
                    List<BaseService> services = GetServices(serviceNames, false);
                    services.ForEach(delegate(BaseService service)
                                         {
                                             if(service.IsConsoleMode)
                                             {
                                                 service.StopAction();
                                             }
                                             else
                                             {
                                                 service.Stop();
                                             }
                                         });
                }
            }
        }

        public static void Restart(ICollection<string> serviceNames, params string[] args)
        {
            using (LogBlock.New(typeof(ServiceFactory), new { serviceNames = serviceNames.Join(",", "[", "]"), args = args.Join(",", "[", "]") }))
            {
                lock(_services)
                {
                    List<BaseService> services = GetServices(serviceNames, false);
                    List<BaseService> toStart = new List<BaseService>();
                    services.ForEach(delegate(BaseService service)
                                         {
                                             if(service.IsConsoleMode)
                                             {
                                                 service.StopAction();
                                                 service.StartAction(args);
                                             }
                                             else
                                             {
                                                 service.Stop();
                                                 toStart.Add(service);
                                             }
                                         });
                    if(toStart.Count > 0)
                    {
                        ServiceBase.Run(toStart.ToArray());
                    }
                }
            }
        }

        public static Installer[] GetInstallers()
        {
            using(LogBlock.New(typeof(ServiceFactory)))
            {
                List<Installer> installers = new List<Installer>();
                ServicesSection section = CoreCfg.Services;
                if(section.Items.Count > 0)
                {
                    ServiceProcessInstaller serviceProcessInstaller = new ServiceProcessInstaller
                                                                          {
                                                                              Account = ServiceAccount.LocalSystem
                                                                          };
                    installers.Add(serviceProcessInstaller);
                    foreach(ServiceElement item in section.Items)
                    {
                        ServiceInstaller serviceInstaller = new ServiceInstaller
                                                                {
                                                                    ServiceName = item.Name,
                                                                    DisplayName = item.DisplayName,
                                                                    Description = item.Description,
                                                                    StartType = item.StartType
                                                                };

                        installers.Add(serviceInstaller);
                        LogBlock.Logger.Info("The service '{0}' was added.", item.Name);
                    }
                }

                return installers.ToArray();
            }
        }

        private static List<BaseService> GetServices(ICollection<string> serviceNames, bool startAction)
        {
            List<BaseService> ret = _services;
            if(serviceNames != null && serviceNames.Count > 0)
            {
                ret = new List<BaseService>(_services.Count);
                foreach(string serviceName in serviceNames)
                {
                    foreach(BaseService t in _services)
                    {
                        if(t.ServiceName == serviceName && t.IsStarted != startAction)
                        {
                            ret.Add(t);
                            break;
                        }
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}