#region Using directives
using System;
#endregion

namespace Allag.Core.Service
{
    [Flags]
    public enum SCAccessType : uint
    {
        /// <summary>
        /// Required to connect to the service control manager.
        /// </summary>
        Connect = 0x00001,

        /// <summary>
        /// Required to call the CreateService function to create a service
        /// object and add it to the database.
        /// </summary>
        CreateService = 0x00002,

        /// <summary>
        /// Required to call the EnumServicesStatusEx function to list the 
        /// services that are in the database.
        /// </summary>
        EnumerateService = 0x00004,

        /// <summary>
        /// Required to call the LockServiceDatabase function to acquire a 
        /// lock on the database.
        /// </summary>
        Lock = 0x00008,

        /// <summary>
        /// Required to call the QueryServiceLockStatus function to retrieve 
        /// the lock status information for the database.
        /// </summary>
        QueryLockStatus = 0x00010,

        /// <summary>
        /// Required to call the NotifyBootConfigStatus function.
        /// </summary>
        ModifyBootConfig = 0x00020,

        /// <summary>
        /// Includes STANDARD_RIGHTS_REQUIRED, in addition to all access 
        /// rights in this table.
        /// </summary>
        All = 0x000F0000 | Connect | CreateService | EnumerateService | Lock | QueryLockStatus | ModifyBootConfig,

        /// <summary>
        /// Includes STANDARD_RIGHTS_READ, in addition to all read 
        /// rights in this table.
        /// </summary>
        Read = 0x00020000 | EnumerateService | QueryLockStatus,

        /// <summary>
        /// Includes STANDARD_RIGHTS_WRITE, in addition to all write 
        /// rights in this table.
        /// </summary>
        Write = 0x00020000 | CreateService | ModifyBootConfig,

        /// <summary>
        /// Includes STANDARD_RIGHTS_EXECUTE, in addition to all execute 
        /// rights in this table.
        /// </summary>
        Execute = 0x00020000 | Connect | Lock
    }
}
