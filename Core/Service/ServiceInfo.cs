#region Using directives

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Allag.Core.Native;

#endregion

namespace Allag.Core.Service
{
    public struct ServiceInfo
    {
        #region Constants
        private static readonly string PATTERN = string.Format("^\"?(?<file>[^\"]+\\{0}[^\\{0}\"]+\\.[^\\{0} \"]+)\"?[ ]*(?<arguments>.*)$", Path.DirectorySeparatorChar);
        #endregion

        #region Variables
        private string _strFile;
        private string _strArguments;
        private readonly string _strAccountName;
        private string _strDisplayName;

        internal uint ServiceType;
        internal uint StartType;
        internal uint ErrorControl;
        internal string LoadOrderGroup;
        internal uint TagId;
        internal string Dependencies;
        #endregion

        #region Constructors
        internal ServiceInfo(ServiceConfig serviceConfig)
        {
            string[] binaryPathName = SplitBinaryPath(Marshal.PtrToStringAuto(serviceConfig.BinaryPathName));
            _strFile = binaryPathName[0];
            _strArguments = binaryPathName[1];

            _strAccountName = Marshal.PtrToStringAuto(serviceConfig.StartName);
            _strDisplayName = Marshal.PtrToStringAuto(serviceConfig.DisplayName);

            ServiceType = serviceConfig.ServiceType;
            StartType = serviceConfig.StartType;
            ErrorControl = serviceConfig.ErrorControl;
            LoadOrderGroup = Marshal.PtrToStringAuto(serviceConfig.LoadOrderGroup);
            TagId = serviceConfig.TagId;
            Dependencies = Marshal.PtrToStringAuto(serviceConfig.Dependencies);
        }

        #endregion

        #region Properties
        public string BinaryPath
        {
            get 
            {
                string ret = File;
                if (ret.Contains(" "))
                {
                    ret = '"' + ret + '"';
                }
                if (Arguments.Length > 0)
                {
                    ret += ' ' + Arguments;
                }
                return ret;
            }
        }
        public string File
        {
            get { return _strFile; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                
                if (Path.GetFileName(value).Length == 0)
                {
                    throw new ArgumentException("The execution file is not specified.", "value");
                }

                _strFile = value;
            }
        }

        public string Arguments
        {
            get { return _strArguments; }
            set 
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                _strArguments = value; 
            }
        }

        public string AccountName
        {
            get { return _strAccountName; }
        }

        public string DisplayName
        {
            get { return _strDisplayName; }
            set 
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                _strDisplayName = value; 
            }
        }
        #endregion

        #region Methods
        private static string[] SplitBinaryPath(string binaryPath)
        {
            string[] ret = new string[] { string.Empty, string.Empty };
            Match match = Regex.Match(binaryPath, PATTERN);
            ret[0] = match.Groups[1].Value;
            if (match.Groups.Count > 2)
            {
                ret[1] = match.Groups[2].Value;
            }

            return ret;
        }

        #endregion
    }
}
