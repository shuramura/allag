﻿#region Using directives
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

#endregion

namespace Allag.Core
{
    public class ArgumentNullOrEmptyException : ArgumentException
    {
        #region Constants
        private const string MESSAGE_FORMAT = "The argument cannot be null, empty or contain only white space.{0}{1}";
        #endregion

        #region Constructors
        public ArgumentNullOrEmptyException() : base(string.Format(MESSAGE_FORMAT, string.Empty, string.Empty))
        {
        }

        public ArgumentNullOrEmptyException(string paramName) : base(string.Format(MESSAGE_FORMAT, string.Empty, string.Empty), paramName)
        {
        }

        public ArgumentNullOrEmptyException(string message, Exception innerException)
            : base(string.Format(MESSAGE_FORMAT, Environment.NewLine, message), innerException)
        {
        }

        public ArgumentNullOrEmptyException(string message, string paramName, Exception innerException)
            : base(string.Format(MESSAGE_FORMAT, Environment.NewLine, message), paramName, innerException)
        {
        }

        public ArgumentNullOrEmptyException(string message, string paramName)
            : base(string.Format(MESSAGE_FORMAT, Environment.NewLine, message), paramName)
        {
        }

        [ExcludeFromCodeCoverage]
        protected ArgumentNullOrEmptyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
        #endregion
    }
}