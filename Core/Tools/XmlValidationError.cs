#region Using directives

using System.Collections.Generic;

#endregion

namespace Allag.Core.Tools
{
    public class XmlValidationError
    {
        #region Variables
        public string Element;
        public List<XmlValidationError> Children;
        #endregion

        #region Constructors

        public XmlValidationError()
        {
            Children = new List<XmlValidationError>();
        }
        #endregion
    }
}
