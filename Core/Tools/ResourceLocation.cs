﻿namespace Allag.Core.Tools
{
    public enum ResourceLocation
    {
        Memory,
        File,
        Assembly
    }
}