#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Xml.XPath;

#endregion

namespace Allag.Core.Tools
{
    public static class XmlSerializerExtension
    {
        #region Variables
        private static readonly Dictionary<Type, XmlSchema> _schemaDictionary;
        #endregion

        #region Constructors
        static XmlSerializerExtension()
        {
            _schemaDictionary = new Dictionary<Type, XmlSchema>();
        }
        #endregion

        #region Methods
        public static TResult Deserialize<TResult>(this object source, Action<XmlReaderSettings> settingInitializer = null) where TResult : new()
        {
            return (TResult)Deserialize(source, typeof(TResult), settingInitializer);
        }

        public static object Deserialize(this object source, Type itemType, Action<XmlReaderSettings> settingInitializer = null)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(itemType);
            using(XmlReader reader = GetXmlReader(source, GetReaderSettings(itemType, settingInitializer)))
            {
                return xmlSerializer.Deserialize(reader);
            }
        }

        public static void Serialize(this object item, object destination, Action<XmlWriterSettings> settingInitializer = null)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            Serialize(item, item.GetType(), destination, settingInitializer);
        }

        public static void Serialize<TItem>(this TItem item, object destination, Action<XmlWriterSettings> settingInitializer = null)
        {
            Serialize(item, typeof(TItem), destination, settingInitializer);
        }

        public static string Serialize<TItem>(this TItem item, Action<XmlWriterSettings> settingInitializer = null)
        {
            StringBuilder destination = new StringBuilder();
            Serialize(item, typeof(TItem), destination, settingInitializer);
            return destination.ToString();
        }

        public static void Serialize(this object item, Type itemType, object destination, Action<XmlWriterSettings> settingInitializer = null)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(itemType);
            XmlWriterSettings settings = new XmlWriterSettings {NewLineHandling = NewLineHandling.None, OmitXmlDeclaration = true};
            if (settingInitializer != null)
            {
                settingInitializer(settings);
            }

            using(XmlWriter writer = GetXmlWriter(destination, settings))
            {
                xmlSerializer.Serialize(writer, item);
            }
        }

        public static XmlReaderSettings GetReaderSettings(Type itemType, Action<XmlReaderSettings> settingInitializer = null)
        {
            XmlReaderSettings settings = new XmlReaderSettings {CheckCharacters = false};
            XmlSchema xmlSchema = GetXmlSchema(itemType);
            if (xmlSchema != null)
            {
                settings.ValidationType = ValidationType.Schema;
                settings.Schemas.Add(xmlSchema);
            }
            else
            {
                settings.ValidationType = ValidationType.None;
            }
            if (settingInitializer != null)
            {
                settingInitializer(settings);
            }
            return settings;
        }

        public static XmlValidationError Validate(string xml, Type itemType)
        {
            if (string.IsNullOrEmpty(xml))
            {
                throw new ArgumentException("The argument cannot be null or empty.", "xml");
            }

            if (itemType == null)
            {
                throw new ArgumentNullException("itemType");
            }

            XmlValidationError ret = new XmlValidationError();
            XmlSchema xmlSchema = GetXmlSchema(itemType);
            if (xmlSchema != null)
            {
                using(StringReader reader = new StringReader(xml))
                {
                    XPathDocument document = new XPathDocument(reader);
                    XPathNavigator navigator = document.CreateNavigator();

                    XmlNamespaceManager manager = new XmlNamespaceManager(navigator.NameTable);
                    foreach (XmlQualifiedName ns in xmlSchema.Namespaces.ToArray())
                    {
                        if (ns.Name.Length == 0)
                        {
                            manager.AddNamespace("ns" + Guid.NewGuid().ToString("N"), ns.Namespace);
                        }
                        else
                        {
                            manager.AddNamespace(ns.Name, ns.Namespace);
                        }
                    }

                    XmlSchemaSet set = new XmlSchemaSet();
                    set.Add(xmlSchema);
                    set.Compile();

                    ret = Validate(ret, "/", navigator, xmlSchema.Items, manager);
                }
            }
            return ret;
        }

        public static XmlSchema GetXmlSchema(Type itemType)
        {
            if (itemType == null)
            {
                throw new ArgumentNullException("itemType");
            }

            XmlSchema xmlSchema;
            if (!_schemaDictionary.TryGetValue(itemType, out xmlSchema))
            {
                lock(_schemaDictionary)
                {
                    if (!_schemaDictionary.TryGetValue(itemType, out xmlSchema))
                    {
                        using(Stream stream = itemType.Assembly.GetManifestResourceStream(itemType, "Schemas." + itemType.Name + ".xsd"))
                        {
                            if (stream != null)
                            {
                                xmlSchema = XmlSchema.Read(stream, null);
                            }
                            _schemaDictionary.Add(itemType, xmlSchema);
                        }
                    }
                }
            }
            return xmlSchema;
        }

        private static XmlReader GetXmlReader(object source, XmlReaderSettings settings)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            XmlReader ret;
            Stream stream;
            TextReader textReader;
            StringBuilder stringBuilder;
            if ((stream = source as Stream) != null)
            {
                ret = XmlReader.Create(stream, settings);
            }
            else if ((textReader = source as TextReader) != null)
            {
                ret = XmlReader.Create(textReader, settings);
            }
            else if ((stringBuilder = source as StringBuilder) != null)
            {
                ret = XmlReader.Create(new StringReader(stringBuilder.ToString()), settings);
            }
            else
            {
                ret = XmlReader.Create(source.ToString(), settings);
            }
            return ret;
        }

        private static XmlWriter GetXmlWriter(object destination, XmlWriterSettings settings)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination");
            }

            XmlWriter ret;
            Stream stream;
            TextWriter textWriter;
            StringBuilder stringBuilder;
            if ((stream = destination as Stream) != null)
            {
                ret = XmlWriter.Create(stream, settings);
            }
            else if ((textWriter = destination as TextWriter) != null)
            {
                ret = XmlWriter.Create(textWriter, settings);
            }
            else if ((stringBuilder = destination as StringBuilder) != null)
            {
                ret = XmlWriter.Create(stringBuilder, settings);
            }
            else
            {
                ret = XmlWriter.Create(destination.ToString(), settings);
            }
            return ret;
        }

        private static XmlValidationError Validate(XmlValidationError ret, string xpathPrefix, XPathNavigator navigator, XmlSchemaObjectCollection items, IXmlNamespaceResolver manager)
        {
            foreach (XmlSchemaObject item in items)
            {
                XmlSchemaElement element = item as XmlSchemaElement;
                if (element != null)
                {
                    string strItem = xpathPrefix + manager.LookupPrefix(element.QualifiedName.Namespace) + ":" + element.Name;
                    XPathNodeIterator iterator = navigator.Select(strItem, manager);

                    if (iterator.Count < element.MinOccurs)
                    {
                        XmlValidationError error = new XmlValidationError {Element = element.Name};
                        ret.Children.Add(error);
                    }
                    else if (iterator.Count > 0)
                    {
                        XmlSchemaComplexType complexType = element.ElementSchemaType as XmlSchemaComplexType;
                        if (complexType != null)
                        {
                            if (complexType.ContentType != XmlSchemaContentType.Empty &&
                                complexType.ContentTypeParticle is XmlSchemaSequence)
                            {
                                XmlSchemaSequence schemaSequence = (XmlSchemaSequence)complexType.ContentTypeParticle;
                                int i = 0;
                                while (iterator.MoveNext())
                                {
                                    string strIndex = iterator.Count == 1 ? string.Empty : "[" + (i + 1) + "]";
                                    XmlValidationError childrenError =
                                        Validate(new XmlValidationError(), strItem + strIndex + "/", navigator,
                                                 schemaSequence.Items, manager);
                                    if (childrenError.Children.Count > 0)
                                    {
                                        XmlValidationError error = new XmlValidationError
                                            {
                                                Element = element.Name + strIndex,
                                                Children = childrenError.Children
                                            };
                                        ret.Children.Add(error);
                                    }
                                    i++;
                                }
                            }
                        }
                        else if (element.ElementSchemaType is XmlSchemaSimpleType)
                        {
                            int i = 0;
                            while (iterator.MoveNext())
                            {
                                string strIndex = iterator.Count == 1 ? string.Empty : "[" + i + "]";
                                if (iterator.Current.Value.Length == 0)
                                {
                                    XmlValidationError error = new XmlValidationError
                                        {Element = element.Name + strIndex};
                                    ret.Children.Add(error);
                                }
                                i++;
                            }
                        }
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}