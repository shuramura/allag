﻿#region Using directives
using System.ComponentModel;
using System.Globalization;

#endregion

namespace Allag.Core.Tools
{
    public class EnumTypeConverter<TEnum> : BaseTypeConverter<TEnum>
        where TEnum : struct
    {
        #region Overrides of BaseTypeConverter<Enum>
        protected override TEnum ConvertFromString(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            return ((string)value).ParseEnum<TEnum>(true);
        }

        protected override object ConvertToString(ITypeDescriptorContext context, CultureInfo culture, TEnum value)
        {
            return value.EnumToString();
        }
        #endregion
    }
}