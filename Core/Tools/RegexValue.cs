﻿#region Using directives
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

#endregion

namespace Allag.Core.Tools
{
    [TypeConverter(typeof(RegexValueTypeConverter))]
    public class RegexValue
    {
        #region Constants
        private const RegexOptions DEFAULT_OPTIONS = RegexOptions.Compiled | RegexOptions.ExplicitCapture | RegexOptions.CultureInvariant | RegexOptions.Singleline | RegexOptions.IgnoreCase;
        #endregion

        #region Variables
        private string _strTemplate;
        private string[] _replacements;
        private RegexOptions _options;
        private Regex _reValue;
        #endregion

        #region Constructors
        public RegexValue() : this(null) {}

        public RegexValue(string template, RegexOptions options = DEFAULT_OPTIONS)
        {
            _options = options;
            Template = template;
            Replacements = null;
        }
        #endregion

        #region Properties
        public RegexOptions Options
        {
            get { return _options; }
            set
            {
                if (_options != value)
                {
                    _options = value;
                    CreateRegex();
                }
            }
        }

        public string Template
        {
            get { return _strTemplate; }
            set
            {
                _strTemplate = value;
                CreateRegex();
            }
        }

        public ReplaceMode ReplaceMode { get; set; }

        public string[] Replacements
        {
            get { return _replacements; }
            set
            {
                if (value == null || value.Length == 0)
                {
                    value = new string[] {string.Empty};
                }
                _replacements = value;
            }
        }
        #endregion

        #region Override methods
        public override string ToString()
        {
            return Template;
        }
        #endregion

        #region Methods
        public bool IsMatch(string data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            return _reValue == null || _reValue.IsMatch(data);
        }

        public string GetValue(string data)
        {
            return GetValues(data, string.Concat).FirstOrDefault();
        }

        public IEnumerable<string> GetValues(string data)
        {
            return GetValues(data, string.Concat);
        }

        public TValue GetValue<TValue>(string data, Func<string[], TValue> converter)
        {
            return GetValues(data, converter).FirstOrDefault();
        }

        public IEnumerable<TValue> GetValues<TValue>(string data, Func<string[], TValue> converter)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            if (converter == null)
            {
                throw new ArgumentNullException("converter");
            }

            if (_reValue != null)
            {
                MatchCollection matchs = _reValue.Matches(data);
                foreach (Match match in matchs)
                {
                    List<string> values = new List<string>();
                    if (match.Success)
                    {
                        for (int i = 1; i < match.Groups.Count; i++)
                        {
                            values.Add(match.Groups[i.ToString(CultureInfo.InvariantCulture)].Value);
                        }
                    }
                    yield return converter(values.ToArray());
                }
            }
        }

        public string Replace(string value)
        {
            string ret;
            if (ReplaceMode == ReplaceMode.Native)
            {
                ret = _reValue.Replace(value, Replacements[0]);
            }
            else
            {
                ret = _reValue.Replace(value, match =>
                {
                    StringBuilder builder = new StringBuilder(value.Length * 2);
                    int nIndex = match.Index;
                    int nCount = 0;
                    for (int i = 1; i < match.Groups.Count; i++)
                    {
                        Group group = match.Groups[i.ToString(CultureInfo.InvariantCulture)];
                        if (group.Success)
                        {
                            nCount++;
                            foreach (Capture capture in group.Captures)
                            {
                                if (capture.Index - nIndex > 0)
                                {
                                    builder.Append(match.Value, nIndex - match.Index, capture.Index - nIndex);
                                }
                                string strReplacement = i - 1 >= _replacements.Length ? _replacements[0] : _replacements[i - 1];
                                int nRepeatCount = 1;
                                if (ReplaceMode == ReplaceMode.Multiply)
                                {
                                    nRepeatCount = capture.Length;
                                }
                                for (int k = 0; k < nRepeatCount; k++)
                                {
                                    builder.Append(strReplacement);
                                }

                                nIndex = capture.Index + capture.Length;
                            }
                        }
                    }
                    if (nCount > 0 && nIndex < (match.Index + match.Length))
                    {
                        builder.Append(match.Value.Substring(nIndex - match.Index));
                    }
                    return builder.ToString();
                });
            }
            return ret;
        }

        private void CreateRegex()
        {
            if (string.IsNullOrEmpty(_strTemplate))
            {
                _reValue = null;
            }
            else
            {
                _reValue = new Regex(_strTemplate, _options);
            }
        }
        #endregion

        #region Operators
        public static implicit operator string(RegexValue value)
        {
            return value != null ? value.Template : null;
        }

        public static implicit operator RegexValue(string value)
        {
            return new RegexValue(value);
        }
        #endregion
    }
}