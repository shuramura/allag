#region Using directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;

#endregion

namespace Allag.Core.Tools
{
    public static class StringExtension
    {
        #region Methods
        public static string Join<TObject>(this IEnumerable<TObject> values, string separator = null, string openDecorater = null, string closeDecorater = null, Func<TObject, string> value = null)
        {
            string ret;
            if (values != null && value == null)
            {
                value = x => !ReferenceEquals(x, null) ? x.ToString() : "";
            }
            if (values == null)
            {
                ret = string.Empty;
            }
            else if (openDecorater == null && closeDecorater == null)
            {
                ret = string.Join(separator, from x in values select value(x));
            }
            else
            {
                if (separator == null)
                {
                    separator = string.Empty;
                }

                openDecorater = openDecorater ?? closeDecorater;
                closeDecorater = closeDecorater ?? openDecorater;

                openDecorater = separator + openDecorater;
                ret = values.Aggregate(string.Empty, (str, next) => str + openDecorater + value(next) + closeDecorater);
                if (ret.Length > 0)
                {
                    ret = ret.Substring(separator.Length);
                }
            }
            return ret;
        }

        #region Enumeration helpers
        public static TEnum ParseEnum<TEnum>(this string value, bool ignoreCase = false, bool throwException = true, string delimiters = null)
            where TEnum : struct
        {
            return LogBlock.Action(typeof(StringExtension), () =>
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }

                TEnum ret;
                string strValue = GetXmlEnumValues(typeof(TEnum), value, ignoreCase, false, delimiters);
                if (throwException)
                {
                    ret = (TEnum)Enum.Parse(typeof(TEnum), strValue, ignoreCase);
                }
                else
                {
                    Enum.TryParse(strValue, ignoreCase, out ret);
                }

                return ret;
            }, new {value, ignoreCase, throwException, delimiters});
        }

        public static string EnumToString<TEnum>(this TEnum value, string delimiters = null)
            where TEnum : struct
        {
            return EnumToString(typeof(TEnum), value, delimiters);
        }

        public static string EnumToString(Type enumType, object value, string delimiters = null)
        {
            return LogBlock.Action(typeof(StringExtension), () =>
            {
                if (enumType == null)
                {
                    throw new ArgumentNullException("enumType");
                }

                if (!enumType.IsEnum)
                {
                    throw new ArgumentException("The parameter should be enumeration.", "value");
                }

                return GetXmlEnumValues(enumType, value.ToString(), false, true, delimiters);
            }, new { value, delimiters });
        }

        private static string GetXmlEnumValues(Type enumType, string value, bool ignoreCase, bool bInvert, string delimiters)
        {
            return LogBlock.Action(typeof(StringExtension), () =>
            {
                FieldInfo[] fields = enumType.GetFields();
                Dictionary<string, string> dictionary = new Dictionary<string, string>(fields.Length * 2);
                foreach (FieldInfo field in fields)
                {
                    XmlEnumAttribute attr = (XmlEnumAttribute)Attribute.GetCustomAttribute(field, typeof(XmlEnumAttribute));
                    if (attr != null && attr.Name != null)
                    {
                        string strKey = bInvert ? field.Name : attr.Name;
                        string strValue = bInvert ? attr.Name : field.Name;
                        if (ignoreCase)
                        {
                            strKey = strKey.ToLower();
                        }
                        if (!dictionary.ContainsKey(strKey))
                        {
                            dictionary.Add(strKey, strValue);
                        }
                    }
                }

                if (string.IsNullOrEmpty(delimiters))
                {
                    delimiters = ",";
                }
                if (dictionary.Count > 0 || delimiters != ",")
                {
                    string[] values = value.Split(delimiters.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    if (values.Length == 0)
                    {
                        values = new string[] { string.Empty };
                    }
                    if (dictionary.Count > 0)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            string str;

                            string strValue = values[i].Trim();
                            if (dictionary.TryGetValue(ignoreCase ? strValue.ToLower() : strValue, out str))
                            {
                                values[i] = str;
                            }
                            else
                            {
                                values[i] = strValue;
                            }
                        }    
                    }
                    value = string.Join(", ", values);
                }
                return value;
            }, new {value, ignoreCase, bInvert, delimiters});
        }
        #endregion

        #endregion
    }
}