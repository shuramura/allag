#region Using directives
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Security.Permissions;
#endregion

namespace Allag.Core.Tools
{
    [HostProtection(SecurityAction.LinkDemand, SharedState = true)]
    public class ArrayTypeConverter : BaseTypeConverter<Array>
    {
        #region Constants
        private const char DELIMITER = ',';
        #endregion

        #region Variables
        private readonly TypeConverter _typeConverter;
        private readonly Type _elementType;
        #endregion

        #region Constructors
        public ArrayTypeConverter(Type elementType)
        {
            if (elementType == null)
            {
                throw new ArgumentNullException("elementType");
            }

            _elementType = elementType;
            _typeConverter = TypeDescriptor.GetConverter(elementType);
        }
        #endregion

        #region Properties
        public Type ElementType
        {
            get { return _elementType; }
        }
        #endregion

        #region Override methods
        protected override Array ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, Array value)
        {
            Array elements = Array.CreateInstance(_elementType, value.Length);
            for (int i = 0; i < value.Length; i++)
            {
                elements.SetValue(_typeConverter.ConvertFrom(context, culture, value.GetValue(i)), i);
            }
            return elements;
        }

        protected override Array ConvertFromString(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            string str = (string) value;
            string[] arr = string.IsNullOrEmpty(str) ? new string[0] : str.Split(DELIMITER);
            return ConvertFrom(context, culture, arr);
        }

        protected override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, Array value,
                                            Type destinationType)
        {
            Type elementType = destinationType.IsArray ? destinationType.GetElementType() : typeof (string);

            Array arr = Array.CreateInstance(elementType, value.Length);
            for (int i = 0; i < arr.Length; i++)
            {
                arr.SetValue(_typeConverter.ConvertTo(context, culture, value.GetValue(i), elementType), i);
            }
            return arr;
        }

        protected override object ConvertToString(ITypeDescriptorContext context, CultureInfo culture, Array value)
        {
            Array arr = (Array) ConvertTo(context, culture, value, typeof (Array));
            return ((string[]) arr).Join("" + DELIMITER);
        }

        protected override bool CanConvert(Type type, ICollection<Type> supportedTypes)
        {
            return _typeConverter != null && (base.CanConvert(type, supportedTypes) || type.IsArray);
        }
        #endregion
    }

    public class ArrayTypeConverter<TElement> : ArrayTypeConverter
    {
        public ArrayTypeConverter() : base(typeof (TElement)) {}
    }
}