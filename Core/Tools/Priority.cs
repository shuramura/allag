﻿#region Using directives
using System;

#endregion

namespace Allag.Core.Tools
{
    [Flags]
    public enum Priority
    {
        None = 0,
        Low = 1,
        Normal = 2,
        High = 4,

        All = Low | Normal | High
    }
}