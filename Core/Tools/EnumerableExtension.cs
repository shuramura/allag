﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace Allag.Core.Tools
{
    public static class EnumerableExtension
    {
        #region Static methods
        public static void ForEach<TItem>(this IEnumerable<TItem> enumerable, Action<TItem> action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            ForEach(enumerable, (i, item) => action(item));
        }

        public static void ForEach<TItem>(this IEnumerable<TItem> enumerable, Action<int, TItem> action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            if (enumerable != null)
            {
                TItem[] items = enumerable.ToArray();
                for (int i = 0; i < items.Length; i++)
                {
                    action(i, items[i]);
                }
            }
        }
        #endregion
    }
}