﻿namespace Allag.Core.Tools.Workflow
{
	public interface IWorkflowAction
	{
		object Execute(object item, object state, object data);
	}

	public interface IWorkflowAction<TItem, in TState, in TData>
		where TItem : IWorkflowItem<TState> 
        where TState : struct
	{
		TItem Execute(TItem item, TState nextState, TData data);
	}
}