﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using Allag.Core.Configuration;
using Allag.Core.Configuration.Workflow;
using Allag.Core.Reflection;

namespace Allag.Core.Tools.Workflow
{
	public class Workflow<TItem, TState, TTrigger, TData>
		where TItem : IWorkflowItem<TState> 
        where TState : struct
	{
		#region Variables
		private readonly Dictionary<string, Tuple<IWorkflowAction<TItem, TState, TData>, IWorkflowAction>> _actions;
		private readonly Dictionary<TState, Dictionary<TTrigger, Tuple<TState, IList<string>>>> _states;
		private readonly List<string> _preActions;
		private readonly List<string> _postActions;

	    private readonly Action<object, object> _setItemState; 
		#endregion

		#region Constructors
		internal Workflow(string name)
		{
			_actions = new Dictionary<string, Tuple<IWorkflowAction<TItem, TState, TData>, IWorkflowAction>>(200);
			_states = new Dictionary<TState, Dictionary<TTrigger, Tuple<TState, IList<string>>>>(40);
			_preActions = new List<string>();
			_postActions = new List<string>();

			Name = name;
		    PropertyInfo propertyInfo = typeof(TItem).GetProperty("State", BindingFlags.Instance | BindingFlags.SetProperty | BindingFlags.Public | BindingFlags.NonPublic);
            if (propertyInfo != null)
            {
                _setItemState = propertyInfo.CreateSetHandler<TItem>();
            }
            else
            {
                _setItemState = null;
            }
       }

		/// <summary>
		/// Initializes a new instance of the <see cref="Workflow{TItem, TState, TTrigger, TData}"/> class.
		/// </summary>
		/// <param name="configuration">The configuration.</param>
		/// <exception cref="System.IO.InvalidDataException"></exception>
		internal Workflow(WorkflowElement configuration)
			: this(configuration.Name)
		{
			foreach (ActionElement element in configuration.Actions)
			{
				Tuple<IWorkflowAction<TItem, TState, TData>, IWorkflowAction> actionInfo = new Tuple<IWorkflowAction<TItem, TState, TData>, IWorkflowAction>
					(
					element.IsStrict ? element.CreateObject() as IWorkflowAction<TItem, TState, TData> : null,
					element.IsStrict ? null : element.CreateObject() as IWorkflowAction
					);

				if (actionInfo.Item1 == null && actionInfo.Item2 == null)
				{
					throw new InvalidDataException(string.Format("The action '{0}' should implement interface '{1}'.", element.Name, element.IsStrict ? typeof(IWorkflowAction<TItem, TState, TData>) : typeof(IWorkflowAction)));
				}

				_actions.Add(element.Name, actionInfo);
			}

			TypeConverter stateConverter = TypeDescriptor.GetConverter(typeof(TState));
			TypeConverter triggerConverter = TypeDescriptor.GetConverter(typeof(TTrigger));
			Dictionary<TTrigger, Tuple<TState, IList<string>>> commonTriggers = new Dictionary<TTrigger, Tuple<TState, IList<string>>>();
			foreach (StateElement stateElement in configuration.States)
			{
				foreach (string value in stateElement.Name.Split(','))
				{
					TState state = default(TState);
					Dictionary<TTrigger, Tuple<TState, IList<string>>> triggers = commonTriggers;
					if (!string.IsNullOrWhiteSpace(value))
					{
						state = (TState)stateConverter.ConvertFrom(value);
						if (!_states.TryGetValue(state, out triggers))
						{
							triggers = new Dictionary<TTrigger, Tuple<TState, IList<string>>>(stateElement.Triggers.Count * 2);
							_states.Add(state, triggers);
						}	
					}
					foreach (TriggerElement triggerElement in stateElement.Triggers)
					{
						IList<string> actions = CreateActionList(new string[] { triggerElement.Action }, GetActionList(triggerElement.Actions));
						TState newState = string.IsNullOrEmpty(triggerElement.NewState) ? state : (TState)stateConverter.ConvertFrom(triggerElement.NewState);
						TTrigger trigger = (TTrigger)triggerConverter.ConvertFrom(triggerElement.Name);
						triggers.Add(trigger, new Tuple<TState, IList<string>>(newState, actions));
					}
				}
			}

			foreach (KeyValuePair<TTrigger, Tuple<TState, IList<string>>> commonTrigger in commonTriggers)
			{
				foreach (KeyValuePair<TState, Dictionary<TTrigger, Tuple<TState, IList<string>>>> state in _states)
				{
					if (!state.Value.ContainsKey(commonTrigger.Key))
					{
						state.Value.Add(commonTrigger.Key, new Tuple<TState, IList<string>>(commonTrigger.Value.Item1.Equals(default(TState)) ? state.Key : commonTrigger.Value.Item1, commonTrigger.Value.Item2));	
					}
				}
			}

			_preActions.AddRange(CreateActionList(GetActionList(configuration.PreExecutedActions)));
			_postActions.AddRange(CreateActionList(GetActionList(configuration.PostExecutedActions)));
		}
		#endregion

		#region Properties
		public string Name { get; private set; }
		#endregion

		#region Methods
		public Workflow<TItem, TState, TTrigger, TData> AddTrigger(TState state, TTrigger trigger, TState newState, params string[] actions)
		{
			List<string> actionNames = new List<string>();
			if (actions != null)
			{
				foreach (string action in actions)
				{
					if (!string.IsNullOrEmpty(action))
					{
						if (!_actions.ContainsKey(action))
						{
							throw new InvalidDataException(string.Format("The action '{0}' does not exist.", action));
						}
						actionNames.Add(action);
					}
				}
			}

			Dictionary<TTrigger, Tuple<TState, IList<string>>> triggers;
			if (!_states.TryGetValue(state, out triggers))
			{
				triggers = new Dictionary<TTrigger, Tuple<TState, IList<string>>>();
				_states.Add(state, triggers);
			}
			triggers.Add(trigger, new Tuple<TState, IList<string>>(newState, actionNames));
			return this;
		}

		public Workflow<TItem, TState, TTrigger, TData> AddAction(string name, IWorkflowAction<TItem, TState, TData> action)
		{
			_actions.Add(name, new Tuple<IWorkflowAction<TItem, TState, TData>, IWorkflowAction>(action, null));
			return this;
		}

		public Workflow<TItem, TState, TTrigger, TData> AddAction(string name, IWorkflowAction action)
		{
			_actions.Add(name, new Tuple<IWorkflowAction<TItem, TState, TData>, IWorkflowAction>(null, action));
			return this;
		}

		public TItem Execute(TItem item, TTrigger trigger, TData data)
		{
		    TState currentState = item.State;
			TState state = item.State;
			foreach (Tuple<IWorkflowAction<TItem, TState, TData>, IWorkflowAction> actionInfo in GetActions(ref state, trigger))
			{
				if (actionInfo.Item1 != null)
				{
                    item = actionInfo.Item1.Execute(item, state, data);
				}
				else
				{
					item = (TItem)actionInfo.Item2.Execute(item, state, data);
				}
			}

            if (_setItemState != null && !currentState.Equals(state))
            {
                _setItemState(item, state);
            }
	
			return item;
		}

		/// <summary>
		/// Get the actions which run for provided trigger and state.
		/// </summary>
		/// <param name="trigger">The trigger.</param>
		/// <param name="state">The state.</param>
		/// <returns>List of actions.</returns>
		public object[] GetActions(TTrigger trigger, ref TState state)
		{
			List<object> ret = new List<object>();
			foreach (Tuple<IWorkflowAction<TItem, TState, TData>, IWorkflowAction> actionInfo in GetActions(ref state, trigger))
			{
				if (actionInfo.Item1 != null)
				{
					ret.Add(actionInfo.Item1);
				}
				else
				{
					ret.Add(actionInfo.Item2);
				}
			}

			return ret.ToArray();
		}
		
		private IList<string> CreateActionList(params string[][] actionList)
		{
			List<string> ret = new List<string>();
			foreach (string[] actions in actionList)
			{
				foreach (string action in actions)
				{
					if (!string.IsNullOrEmpty(action))
					{
						if (!_actions.ContainsKey(action))
						{
							throw new InvalidDataException(string.Format("The action '{0}' does not exist.", action));
						}
						ret.Add(action);
					}
				}
			}
			return ret.ToArray();
		}

		/// <summary>
		/// Return the information of actions which should be ran for appropriate state and trigger.
		/// </summary>
		/// <param name="state"></param>
		/// <param name="trigger"></param>
		/// <returns></returns>
		private IEnumerable<Tuple<IWorkflowAction<TItem, TState, TData>, IWorkflowAction>> GetActions(ref TState state, TTrigger trigger)
		{
			Dictionary<TTrigger, Tuple<TState, IList<string>>> triggers;
			if (!_states.TryGetValue(state, out triggers))
			{
				throw new InvalidDataException(string.Format("The state '{0}' is undefined.", state));
			}

			Tuple<TState, IList<string>> triggerInfos;
			if (!triggers.TryGetValue(trigger, out triggerInfos))
			{
				throw new InvalidDataException(string.Format("The trigger '{0}' is undefined for the state '{1}'.", trigger, state));
			}

			state = triggerInfos.Item1;
			List<Tuple<IWorkflowAction<TItem, TState, TData>, IWorkflowAction>> ret = new List<Tuple<IWorkflowAction<TItem, TState, TData>, IWorkflowAction>>();

			// Get pre-executed actions
            ret.AddRange(_preActions.Select(x => _actions[x]));
			
			// Get trigger's actions
			ret.AddRange(triggerInfos.Item2.Select(x => _actions[x]));

			// Get post-executed actions
            ret.AddRange(_postActions.Select(x => _actions[x]));
			return ret;
		}
		#endregion

		#region Static methods
		private static string[] GetActionList(IEnumerable collection)
		{
            return (from Element element in collection select element.Name).ToArray();
		}
		#endregion
	}

	public static class Workflow
	{
		private static readonly ConcurrentDictionary<string, object> _workflows;

		static Workflow()
		{
			_workflows = new ConcurrentDictionary<string, object>();
		}

		/// <summary>
		/// Get exact workflow by provided name.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static Workflow<TItem, TState, TTrigger, TData> Get<TItem, TState, TTrigger, TData>(object name)
			where TItem : IWorkflowItem<TState> 
            where TState : struct
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}

			return (Workflow<TItem, TState, TTrigger, TData>)_workflows.GetOrAdd(name.ToString(), workflowName =>
			                                                                                      {
				                                                                                      Workflow<TItem, TState, TTrigger, TData> ret;
				                                                                                      WorkflowElement configuration = CoreCfg.Workflows.Items[workflowName];
				                                                                                      if (configuration != null)
				                                                                                      {
					                                                                                      ret = new Workflow<TItem, TState, TTrigger, TData>(CoreCfg.Workflows.Items[workflowName]);
				                                                                                      }
				                                                                                      else
				                                                                                      {
					                                                                                      ret = new Workflow<TItem, TState, TTrigger, TData>(workflowName);
				                                                                                      }
				                                                                                      return ret;
			                                                                                      });
		}
	}
}