﻿namespace Allag.Core.Tools.Workflow
{
	public interface IWorkflowItem<out TState>
        where TState : struct 
	{
		TState State { get; }
	}
}