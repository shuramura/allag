﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;

#endregion

namespace Allag.Core.Tools.Archives
{
    public interface IArchiveFile : IDisposable, IEnumerable<IArchiveItem>
    {
        bool IsReadOnly { get; }
        CompressionLevel CompressionLevel { get; set; }

        IArchiveItem AddFile(string destFile, string sourceFile = null);
        IArchiveItem AddFile(string destFile, Stream data);
        void ExtractAll(string destinationFolder, Action<IArchiveItem> progress = null);
        IEnumerable<IArchiveItem> AddFolder(string sourceRootFolder, SearchOption searchOption, Func<string, string> sourceFileProvider = null);

        IArchiveFile Open(Stream archiveStream);
        IArchiveFile Open(string archiveFile, bool isReadOnly = true);
    }
}