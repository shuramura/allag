﻿#region Using directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

#endregion

namespace Allag.Core.Tools.Archives
{
    public class ZipFile : IArchiveFile
    {
        #region Variables
        private ZipArchive _zipArchive;
        private Stream _fileStream;
        #endregion

        #region Constructors
        public ZipFile()
        {
            IsReadOnly = true;
            CompressionLevel = CompressionLevel.Optimal;
        }

        public ZipFile(Stream zipStream) : this()
        {
            if (zipStream == null)
            {
                throw new ArgumentNullException("zipStream");
            }

            IsReadOnly = !zipStream.CanWrite;
            _zipArchive = new ZipArchive(zipStream, IsReadOnly ? ZipArchiveMode.Read : ZipArchiveMode.Update, true);
        }

        public ZipFile(string zipFile, bool isReadOnly) : this()
        {
            if (string.IsNullOrWhiteSpace(zipFile))
            {
                throw new ArgumentNullOrEmptyException("zipFile");
            }

            ZipArchiveMode zipArchiveMode;
            if (isReadOnly)
            {
                _fileStream = File.OpenRead(zipFile);
                zipArchiveMode = ZipArchiveMode.Read;
            }
            else if (!File.Exists(zipFile))
            {
                _fileStream = File.Create(zipFile);
                zipArchiveMode = ZipArchiveMode.Create;
            }
            else
            {
                _fileStream = File.Open(zipFile, FileMode.OpenOrCreate);
                zipArchiveMode = ZipArchiveMode.Update;
            }

            try
            {
                IsReadOnly = isReadOnly;
                _zipArchive = new ZipArchive(_fileStream, zipArchiveMode, false);
            }
            catch (Exception exp)
            {
                if (_fileStream != null)
                {
                    _fileStream.Close();
                    _fileStream = null;
                }
                throw new InvalidDataException("The file cannot be opened or it is not a zip archive.", exp);
            }
        }

        ~ZipFile()
        {
            Dispose(false);
        }
        #endregion

        #region Implementation of IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation of IEnumerable
        IEnumerator IEnumerable.GetEnumerator()
        {
            CheckDisposed();
            return GetEnumerator();
        }
        #endregion

        #region Implementation of IEnumerable<IArchiveItem>
        public IEnumerator<IArchiveItem> GetEnumerator()
        {
            CheckDisposed();
            return _zipArchive.Entries.Select(x => new ZipItem(x)).GetEnumerator();
        }
        #endregion

        #region Implementation of IArchiveFile
        public bool IsReadOnly { get; private set; }
        public CompressionLevel CompressionLevel { get; set; }

        public IArchiveItem AddFile(string destFile, string sourceFile = null)
        {
            return CreateZipItem(destFile).Pack(sourceFile);
        }

        public IArchiveItem AddFile(string destFile, Stream data)
        {
            return CreateZipItem(destFile).Pack(data);
        }

        public void ExtractAll(string destinationFolder, Action<IArchiveItem> progress = null)
        {
            if (destinationFolder == null)
            {
                throw new ArgumentNullException("destinationFolder");
            }

            foreach (IArchiveItem archiveItem in this)
            {
                if (!archiveItem.IsFolder)
                {
                    string strPath = archiveItem.FullName.Replace('/', '\\');
                    archiveItem.Extract(destinationFolder, strPath);
                }
                if (progress != null)
                {
                    progress(archiveItem);
                }
            }
        }

        public IEnumerable<IArchiveItem> AddFolder(string sourceRootFolder, SearchOption searchOption, Func<string, string> sourceFileProvider = null)
        {
            if (sourceFileProvider == null)
            {
                sourceFileProvider = x => x;
            }

            return Directory.EnumerateFiles(sourceRootFolder, "*.*", searchOption).Select(file => AddFile(file.Substring(sourceRootFolder.Length), sourceFileProvider(file))).ToArray();
        }

        public IArchiveFile Open(Stream archiveStream)
        {
            return new ZipFile(archiveStream);
        }

        public IArchiveFile Open(string archiveFile, bool isReadOnly = true)
        {
            return new ZipFile(archiveFile, isReadOnly);
        }
        #endregion

        #region Methods
        protected void Dispose(bool disposing)
        {
            if (_zipArchive != null)
            {
                if (disposing)
                {
                    _zipArchive.Dispose();
                    _zipArchive = null;
                    if (_fileStream != null)
                    {
                        _fileStream.Dispose();
                        _fileStream = null;
                    }
                }
            }
        }

        private void CheckDisposed()
        {
            if (_zipArchive == null)
            {
                throw new ObjectDisposedException("_zipArchive");
            }
        }

        private ZipItem CreateZipItem(string itemName)
        {
            CheckDisposed();
            if (string.IsNullOrWhiteSpace(itemName))
            {
                throw new ArgumentException("The parameter cannot be null or empty.", "itemName");
            }
            return new ZipItem(_zipArchive.CreateEntry(itemName.Replace("..\\", ""), CompressionLevel));
        }
        #endregion
    }
}