﻿#region Using directives
using System;
using System.Net;
using System.Text;

#endregion

namespace Allag.Core.Tools.Archives.Tar
{
    public class SizeProperty : Property
    {
        #region Variables
        private readonly Action<TarItem, long> _setter;
        #endregion

        #region Constructors
        public SizeProperty(Action<TarItem, long> setter)
            : base(124, 13)
        {
            if (setter == null)
            {
                throw new ArgumentNullException("setter");
            }

            _setter = setter;
        }
        #endregion

        #region Implementation of Property
        protected override void ReadValue(TarItem item, byte[] data)
        {
            long lSize;
            if ((data[0] & 0x80) == 0x80)
            {
                lSize = BitConverter.ToInt64(data, data.Length - sizeof(long));
                lSize = IPAddress.NetworkToHostOrder(lSize);
            }
            else
            {
                lSize = Convert.ToInt64(Encoding.ASCII.GetString(data).Trim('\0', ' '), 8);
            }
            _setter(item, lSize);
        }

        protected override void WriteValue(TarItem item, byte[] data)
        {
            byte[] buffer;
            int nPosition;
            long lSize = item.Size;
            if (lSize >= 0x1FFFFFFFF)
            {
                lSize = IPAddress.HostToNetworkOrder(lSize);
                buffer = BitConverter.GetBytes(lSize);
                Array.Clear(data, 0, data.Length);
                data[0] |= 0x80;
                nPosition = data.Length - buffer.Length;
            }
            else
            {
                buffer = Encoding.ASCII.GetBytes(Convert.ToString(lSize, 8));
                nPosition = data.Length - buffer.Length - 1;
                data[data.Length - 1] = 0;
            }
            buffer.CopyTo(data, nPosition);
        }
        #endregion
    }
}