﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Text;

#endregion

namespace Allag.Core.Tools.Archives.Tar
{
    public abstract class Property
    {
        #region Variables
        private readonly int _nPosition;
        private readonly int _nLength;
        #endregion

        #region Constructors
        protected Property(int position, int length)
        {
            if (position < 0)
            {
                throw new ArgumentOutOfRangeException("position", "The length should be greater or equal to zero.");
            }

            if (length <= 0)
            {
                throw new ArgumentOutOfRangeException("length", "The length should be greater than zero.");
            }
            _nPosition = position;
            Filler = '0';
            _nLength = length;
        }
        #endregion

        #region Properties
        public char Filler { get; protected set; }
        #endregion

        #region Abstract methods
        protected abstract void ReadValue(TarItem item, byte[] data);
        protected abstract void WriteValue(TarItem item, byte[] data);
        #endregion

        #region Methods
        public void Read(TarItem item, byte[] container)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            ValidateContainer(container);
            byte[] data = new byte[_nLength - 1];
            Array.Copy(container, _nPosition, data, 0, data.Length);
            ReadValue(item, data);
        }

        public void Write(TarItem item, byte[] container)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            ValidateContainer(container);

            byte[] data = new byte[_nLength - 1];
            byte filler = (byte)Filler;
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = filler;
            }
            WriteValue(item, data);
            data.CopyTo(container, _nPosition);
        }

        private void ValidateContainer(byte[] container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }

            if (container.Length < _nPosition + _nLength)
            {
                throw new ArgumentOutOfRangeException("container");
            }
        }
        #endregion
    }

    public class Property<TProperty> : Property
    {
        #region Variables
        private readonly Func<TarItem, TProperty> _getter;
        private readonly Action<TarItem, TProperty> _setter;
        private readonly Func<TProperty, int, string> _converterTo;
        private readonly Func<string, int, TProperty> _converterFrom;
        private readonly char[] _trimChars;
        #endregion

        #region Constructors
        public Property(int position, int length, Func<TarItem, TProperty> getter, Action<TarItem, TProperty> setter, Func<TProperty, int, string> converterTo, Func<string, int, TProperty> converterFrom, char[] trimChars = null)
            : base(position, length)
        {
            if (getter == null)
            {
                throw new ArgumentNullException("getter");
            }

            if (setter == null)
            {
                throw new ArgumentNullException("setter");
            }

            if (converterTo == null)
            {
                throw new ArgumentNullException("converterTo");
            }

            if (converterFrom == null)
            {
                throw new ArgumentNullException("converterFrom");
            }

            _getter = getter;
            _setter = setter;
            _converterTo = converterTo;
            _converterFrom = converterFrom;
            _trimChars = trimChars ?? new char[] {' ', '\0'};
            AlignToRight = true;
        }
        #endregion

        #region Implementation of Property
        protected override void ReadValue(TarItem item, byte[] data)
        {
            string str = Encoding.ASCII.GetString(data);
            str = AlignToRight ? str.TrimStart(Filler) : str.TrimEnd(Filler);
            str = str.Trim(_trimChars);
            TProperty value = _converterFrom(str, 8);
            _setter(item, value);
        }

        protected override void WriteValue(TarItem item, byte[] data)
        {
            byte[] buffer = Encoding.ASCII.GetBytes(_converterTo(_getter(item), 8));
            if (buffer.Length > data.Length)
            {
                throw new InvalidOperationException(string.Format("The data length should not exceed {0}.", data.Length));
            }
            buffer.CopyTo(data, AlignToRight ? data.Length - buffer.Length : 0);
        }
        #endregion

        #region Properties
        public bool AlignToRight { get; protected set; }

        public IEnumerable<char> TrimChars
        {
            get { return _trimChars; }
        }
        #endregion
    }
}