﻿#region Using directives
using System;

#endregion

namespace Allag.Core.Tools.Archives.Tar
{
    public class TarException : Exception
    {
        #region Constructors 
        public TarException(string message) : base(message) {}
        public TarException(string message, Exception innerException) : base(message, innerException) { }
        #endregion
    }
}