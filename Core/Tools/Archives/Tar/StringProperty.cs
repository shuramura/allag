﻿#region Using directives
using System;

#endregion

namespace Allag.Core.Tools.Archives.Tar
{
    public class StringProperty : Property<string>
    {
        #region Constructors
        public StringProperty(int position, int length, Func<TarItem, string> getter, Action<TarItem, string> setter)
            : base(position, length, getter, setter, (value, toBase) => value, (value, toBase) => value, new char[0])
        {
            Filler = '\0';
        }
        #endregion
    }
}