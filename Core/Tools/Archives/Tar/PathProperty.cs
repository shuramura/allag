﻿#region Using directives
using System;
using System.IO;

#endregion

namespace Allag.Core.Tools.Archives.Tar
{
    public class PathProperty : StringProperty
    {
        #region Constants
        private const char ARCHIVE_PATH_SEPARATOR = '/';
        private static readonly char[] _pathSeparators = new char[] {ARCHIVE_PATH_SEPARATOR, '\\', '|'};
        #endregion

        #region Constructors
        public PathProperty(int position, int length, Func<TarItem, string> getter, Action<TarItem, string> setter)
            : base(position, length,
                item => Normalize(getter(item), ARCHIVE_PATH_SEPARATOR),
                (item, value) => { setter(item, Normalize(value, Path.DirectorySeparatorChar)); })
        {
            AlignToRight = false;
        }
        #endregion

        #region Static methods
        private static string Normalize(string value, char separator)
        {
            if (value != null)
            {
                _pathSeparators.ForEach(x =>
                {
                    if (x != separator)
                    {
                        value = value.Replace(x, separator);
                    }
                });
            }
            return value;
        }
        #endregion
    }
}