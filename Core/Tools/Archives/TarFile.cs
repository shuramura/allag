﻿#region Using directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Allag.Core.Tools.Archives.Tar;

#endregion

namespace Allag.Core.Tools.Archives
{
    public class TarFile : IArchiveFile
    {
        #region Constants
        public const int BlockSize = 512;
        #endregion

        #region Variables
        private Stream _stream;
        private readonly long _lPosition;
        private long _lLength;
        private readonly bool _isStreamOwner;
        #endregion

        #region Constructors
        public TarFile()
        {
            BufferBlockCount = 2048;
            IsReadOnly = true;
            _lPosition = 0;
            _lLength = 0;
        }

        private TarFile(Stream stream) : this()
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            IsReadOnly = !stream.CanWrite;
            _isStreamOwner = false;
            _stream = stream;
            _lPosition = _stream.Position;
            _lLength = _stream.Length;
            AdjustLength();
        }

        private TarFile(string archiveFile, bool isReadOnly) : this()
        {
            if (string.IsNullOrWhiteSpace(archiveFile))
            {
                throw new ArgumentNullOrEmptyException("archiveFile");
            }

            if (isReadOnly)
            {
                _stream = File.OpenRead(archiveFile);
            }
            else if (!File.Exists(archiveFile))
            {
                _stream = File.Create(archiveFile);
            }
            else
            {
                _stream = File.Open(archiveFile, FileMode.OpenOrCreate);
            }

            IsReadOnly = isReadOnly;
            _isStreamOwner = true;
            _lLength = _stream.Length;
            AdjustLength();
        }

        ~TarFile()
        {
            Dispose(false);
        }
        #endregion

        #region Implementation of IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation of IEnumerable
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion

        #region Implementation of IEnumerable<IArchiveItem>
        public IEnumerator<IArchiveItem> GetEnumerator()
        {
            CheckDisposed();

            _stream.Seek(_lPosition, SeekOrigin.Begin);
            while (_stream.Position < _stream.Length)
            {
                yield return new TarItem(this, _stream);
            }
        }
        #endregion

        #region Implementation of IArchiveFile
        public IArchiveItem AddFile(string destFile, string sourceFile = null)
        {
            if (string.IsNullOrWhiteSpace(destFile))
            {
                throw new ArgumentNullOrEmptyException("destFile");
            }

            IArchiveItem item;
            FileInfo info = new FileInfo(sourceFile ?? destFile);
            using(FileStream stream = info.OpenRead())
            {
                item = AddFile(destFile, stream, archiveItem => { archiveItem.ModifiedOn = info.LastWriteTime; });
            }
            return item;
        }

        public IArchiveItem AddFile(string destFile, Stream data)
        {
            return AddFile(destFile, data, archiveItem => { });
        }

        public void ExtractAll(string destinationFolder, Action<IArchiveItem> progress = null)
        {
            CheckDisposed();
            foreach (IArchiveItem item in this)
            {
                item.Extract(destinationFolder);
                if (progress != null)
                {
                    progress(item);
                }
            }
        }

        public IEnumerable<IArchiveItem> AddFolder(string sourceRootFolder, SearchOption searchOption, Func<string, string> sourceFileProvider = null)
        {
            CheckDisposed();
            if (sourceFileProvider == null)
            {
                sourceFileProvider = x => x;
            }

            return Directory.EnumerateFiles(sourceRootFolder, "*.*", searchOption)
                .Select(file => AddFile(file.Substring(sourceRootFolder.Length), sourceFileProvider(file)))
                .ToArray();
        }

        public IArchiveFile Open(Stream archiveStream)
        {
            return new TarFile(archiveStream) {BufferBlockCount = BufferBlockCount};
        }

        public IArchiveFile Open(string archiveFile, bool isReadOnly = true)
        {
            return new TarFile(archiveFile, isReadOnly) {BufferBlockCount = BufferBlockCount};
        }

        public bool IsReadOnly { get; private set; }

        public CompressionLevel CompressionLevel
        {
            get { return CompressionLevel.NoCompression; }
            set { throw new NotSupportedException("The only 'NoCompression' level is supported."); }
        }
        #endregion

        #region Properties
        public int BufferBlockCount { get; set; }
        #endregion

        #region Methods
        private IArchiveItem AddFile(string destFile, Stream data, Action<TarItem> initizlizer)
        {
            CheckDisposed();

            if (string.IsNullOrWhiteSpace(destFile))
            {
                throw new ArgumentNullOrEmptyException("destFile");
            }

            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            _stream.Position = _lLength;
            TarItem item = new TarItem(this, _stream, destFile);
            initizlizer(item);
            item.Store(data);
            _lLength = _stream.Length;
            byte[] buffer = new byte[BlockSize];
            Array.Clear(buffer, 0, buffer.Length);
            _stream.Write(buffer, 0, buffer.Length);
            return item;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_stream != null)
            {
                if (disposing)
                {
                    if (_isStreamOwner)
                    {
                        _stream.Dispose();
                    }
                    _stream = null;
                }
            }
        }

        private void CheckDisposed()
        {
            if (_stream == null)
            {
                throw new ObjectDisposedException("_archive");
            }
        }

        private void AdjustLength()
        {
            if ((_lPosition + _lLength) % BlockSize != 0)
            {
                throw new TarException("The archive is broken.");
            }

            if (_lLength - _lPosition > BlockSize)
            {
                _lLength -= BlockSize;
            }
        }
        #endregion
    }
}