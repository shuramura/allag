﻿#region Using directives
using System;

#endregion

namespace Allag.Core.Tools.Archives
{
    public interface IArchiveItem
    {
        string FullName { get; }
        string Name { get; }
        bool IsFolder { get; }
        DateTime ModifiedOn { get; }

        string Extract(string destFolder, string destFile = null);
    }
}