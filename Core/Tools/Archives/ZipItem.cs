﻿#region Using directives
using System;
using System.IO;
using System.IO.Compression;

#endregion

namespace Allag.Core.Tools.Archives
{
    public class ZipItem : IArchiveItem
    {
        #region Vaiables
        private readonly ZipArchiveEntry _zipEntity;
        #endregion

        #region Constructors
        internal ZipItem(ZipArchiveEntry zipEntity)
        {
            _zipEntity = zipEntity;
        }
        #endregion

        #region Implementation of IArchiveItem
        public string FullName
        {
            get { return IsFolder ? _zipEntity.FullName.Substring(0, _zipEntity.FullName.Length - 1) : _zipEntity.FullName; }
        }

        public string Name
        {
            get { return Path.GetFileName(IsFolder ? _zipEntity.FullName.Substring(0, _zipEntity.FullName.Length - 1) : _zipEntity.FullName); }
        }

        public bool IsFolder
        {
            get { return _zipEntity.FullName.EndsWith("/"); }
        }

        public DateTime ModifiedOn
        {
            get { return _zipEntity.LastWriteTime.DateTime; }
        }

        public string Extract(string destFolder, string destFile = null)
        {
            string strFile = Path.Combine(destFolder, destFile ?? FullName);
            Directory.CreateDirectory(Path.GetDirectoryName(strFile));
            _zipEntity.ExtractToFile(strFile);
            return strFile;
        }
        #endregion

        #region Methods
        internal ZipItem Pack(string file = null)
        {
            using(FileStream fileStream = File.OpenRead(file ?? FullName))
            {
                using(Stream stream = _zipEntity.Open())
                {
                    fileStream.CopyTo(stream);
                }
            }
            return this;
        }

        internal ZipItem Pack(Stream data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            using(Stream stream = _zipEntity.Open())
            {
                data.CopyTo(stream);
            }

            return this;
        }
        #endregion
    }
}