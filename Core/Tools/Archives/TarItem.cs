﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Allag.Core.Tools.Archives.Tar;

#endregion

namespace Allag.Core.Tools.Archives
{
    public class TarItem : IArchiveItem
    {
        #region Constants
        private const string USTAR_MARKER = "ustar";
        private const int CHECKSUM_POSITION = 148;
        private const int CHECKSUM_LENGTH = 8;
        private const int MAX_FILENAME_LENGTH = 100;
        #endregion

        #region Variables
        private static readonly List<Property> _properties;
        private static readonly DateTime _dtMinimumDateTime;
        private static readonly byte[] _defaultChecksum;
       
        private readonly TarFile _file;
        private readonly Stream _stream;
        private readonly long _nPosition;
        private readonly byte[] _headerData;
        private long _lChecksum;
        private string _strFile;
        private string _strPrefix;
        private int _nMode;
        private string _strUserName;
        private string _strGroupName;
        #endregion

        #region Constructors
        static TarItem()
        {
            _dtMinimumDateTime = new DateTime(1970, 1, 1, 0, 0, 0);
            _defaultChecksum = new byte[] {0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20};
       
            Func<string, int, int> toInt = (s, i) => s.Length > 0 ? Convert.ToInt32(s, i) : 0;
            Func<string, int, byte> toByte = (s, i) => s.Length > 0 ? Convert.ToByte(s, i) : (byte)0;
            _properties = new List<Property>
            {
                new StringProperty(257, 6, item => USTAR_MARKER, (item, value) => { }),
                new PathProperty(0, 100, item => item._strFile, (item, value) => item._strFile = value),
                new Property<int>(100, 8, item => item._nMode, (item, value) => item._nMode = value, Convert.ToString, toInt),
                new Property<int>(108, 8, item => item.UserId, (item, value) => item.UserId = value, Convert.ToString, toInt),
                new Property<int>(116, 8, item => item.GroupId, (item, value) => item.GroupId = value, Convert.ToString, toInt),
                new SizeProperty((item, value) => item.Size = value),
                new Property<long>(136, 12, item => (long)(item.ModifiedOn - _dtMinimumDateTime).TotalSeconds, (item, value) => item.ModifiedOn = _dtMinimumDateTime.AddSeconds(value), Convert.ToString, Convert.ToInt64),
                new Property<byte>(156, 2, item => (byte)(item.IsFolder ? 5 : 0), (item, value) => item.IsFolder = value == 5, Convert.ToString, toByte),
                new StringProperty(265, 32, item => item._strUserName, (item, value) => item._strUserName = value),
                new StringProperty(297, 32, item => item._strGroupName, (item, value) => item._strGroupName = value),
                new PathProperty(345, 155, item => item._strPrefix, (item, value) => item._strPrefix = value),
                new Property<long>(CHECKSUM_POSITION, CHECKSUM_LENGTH,
                    item => CalculateChecksum(item._headerData),
                    (item, value) => item._lChecksum = value,
                    (value, toBase) => Convert.ToString(value, toBase) + '\0',
                    (value, toBase) => Convert.ToInt64(value.Trim('\0'), toBase))
            };
        }

        private TarItem(Stream stream, TarFile file)
        {
            if (file == null)
            {
                throw new ArgumentNullException("file");
            }

            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            _headerData = new byte[TarFile.BlockSize];
            Array.Clear(_headerData, 0, _headerData.Length);
            _file = file;
            _stream = stream;
            _nPosition = stream.Position;
            _strUserName = string.Empty;
            _strGroupName = string.Empty;
        }

        public TarItem(TarFile archive, Stream stream, string file)
            : this(stream, archive)
        {
            if (string.IsNullOrWhiteSpace(file))
            {
                throw new ArgumentNullOrEmptyException("file");
            }

            SetFileName(file);
            _nMode = 511;
            ModifiedOn = DateTime.Now;
            IsFolder = false;
        }

        public TarItem(TarFile file, Stream stream) : this(stream, file)
        {
            if (!ReadHeader(_stream))
            {
                throw new TarException("The tar item header is wrong.");
            }

            try
            {
                Initialize();
                long lAlignment = (_stream.Position + Size) % TarFile.BlockSize;
                if (lAlignment > 0)
                {
                    lAlignment = TarFile.BlockSize - lAlignment;
                }
                _stream.Seek(Size + lAlignment, SeekOrigin.Current);
                if (ReadHeader(_stream))
                {
                    _stream.Seek(-TarFile.BlockSize, SeekOrigin.Current);
                }
            }
            catch (Exception exp)
            {
                throw new TarException("The tar item header is wrong.", exp);
            }
        }
        #endregion

        #region Implementation of IArchiveItem
        public string Extract(string destFolder, string destFile = null)
        {
            long lPosition = _stream.Position;
            try
            {
                _stream.Seek(_nPosition + TarFile.BlockSize, SeekOrigin.Begin);
                string strItem = destFile ?? FullName;
                if (Path.IsPathRooted(strItem))
                {
                    strItem = strItem.Substring(Path.GetPathRoot(strItem).Length);
                }

                strItem = Path.Combine(destFolder, strItem);
                if (IsFolder)
                {
                    Directory.CreateDirectory(strItem);
                }
                else
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(strItem));
                    using(FileStream fileStream = File.Create(strItem))
                    {
                        CopyTo(_stream, Size, fileStream);
                    }
                }

                return strItem;
            }
            finally
            {
                _stream.Seek(lPosition, SeekOrigin.Begin);
            }
        }

        public string FullName
        {
            get { return GetNormalizedName(_strPrefix.Trim('\0') + _strFile.Trim('\0')); }
        }

        public string Name
        {
            get { return Path.GetFileName(FullName); }
        }

        public bool IsFolder { get; private set; }

        public DateTime ModifiedOn { get; set; }
        #endregion

        #region Properties
        public int UserId { get; set; }
        public int GroupId { get; set; }

        public string UserName
        {
            get { return string.IsNullOrEmpty(_strUserName) ? UserId.ToString() : _strUserName; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentNullOrEmptyException("value");
                }

                if (value.Length >= 32)
                {
                    throw new ArgumentOutOfRangeException("value", "The user name length can not be greater than 32 character.");
                }
                _strUserName = value;
            }
        }

        public string GroupName
        {
            get { return string.IsNullOrEmpty(_strGroupName) ? GroupId.ToString() : _strGroupName; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentNullOrEmptyException("value");
                }

                if (value.Length >= 32)
                {
                    throw new ArgumentOutOfRangeException("value", "The group name length can not be greater than 32 character.");
                }
                _strGroupName = value;
            }
        }

        public long Size { get; private set; }
        #endregion

        #region Methods
        public void Store(Stream source)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            _stream.Seek(_nPosition, SeekOrigin.Begin);
            source.Seek(0, SeekOrigin.Begin);
            Size = source.Length;
            _properties.ForEach(x => x.Write(this, _headerData));
            _stream.Write(_headerData, 0, _headerData.Length);

            CopyTo(source, Size, _stream);

            long lAlignment = _stream.Position % TarFile.BlockSize;
            if (lAlignment > 0)
            {
                lAlignment = TarFile.BlockSize - lAlignment;
                byte[] buffer = new byte[lAlignment];
                Array.Clear(buffer, 0, buffer.Length);
                _stream.Write(buffer, 0, buffer.Length);
            }
        }

        private void Initialize()
        {
            _properties.ForEach(x => x.Read(this, _headerData));

            if (_lChecksum != CalculateChecksum(_headerData) &&
                _lChecksum != CalculateChecksum(_headerData, value => (value & 0x80) == 0x80 ? (value ^ 0x80) * (-1) : value))
            {
                throw new TarException("The header checksum is wrong.");
            }
        }

        private void CopyTo(Stream source, long size, Stream destination)
        {
            long lBufferSize = _file.BufferBlockCount * TarFile.BlockSize;
            if (lBufferSize > size)
            {
                lBufferSize = size;
            }

            byte[] buffer = new byte[lBufferSize];
            long lReadBytes = 0;
            while (lReadBytes < size)
            {
                int nCount = source.Read(buffer, 0, buffer.Length);
                if (nCount == 0)
                {
                    throw new InvalidDataException("The item archive size is wrong.");
                }
                if (lReadBytes + nCount > size)
                {
                    nCount = (int)(size - lReadBytes);
                }
                destination.Write(buffer, 0, nCount);
                lReadBytes += nCount;
            }
        }

        private void SetFileName(string file)
        {
            if (string.IsNullOrWhiteSpace("file"))
            {
                throw new ArgumentNullOrEmptyException("file");
            }

            if (file.Length > 255)
            {
                throw new ArgumentOutOfRangeException("file", "The file name ca not be longer than 254 characters.");
            }

            _strPrefix = string.Empty;
            if (file.Length > MAX_FILENAME_LENGTH)
            {
                int i = MAX_FILENAME_LENGTH;
                while (i < file.Length && Path.DirectorySeparatorChar != file[i])
                {
                    i++;
                }

                if (i == file.Length)
                {
                    i = MAX_FILENAME_LENGTH;
                }
                _strPrefix = file.Substring(0, i);
                file = file.Substring(i, file.Length - i);
            }
            _strFile = file;
        }

        private bool ReadHeader(Stream stream)
        {
            while (stream.Position < stream.Length)
            {
                int nRead = stream.Read(_headerData, 0, _headerData.Length);
                if (nRead == TarFile.BlockSize && _headerData.Any(x => x != 0))
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region Static methods
        private static long CalculateChecksum(byte[] container, Func<byte, int> formula = null)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }

            if (container.Length < CHECKSUM_POSITION + CHECKSUM_LENGTH)
            {
                throw new ArgumentOutOfRangeException("container");
            }

            if (formula == null)
            {
                formula = value => value;
            }

            byte[] buffer = new byte[container.Length];
            container.CopyTo(buffer, 0);
            _defaultChecksum.CopyTo(buffer, CHECKSUM_POSITION);
            long ret = 0;
            for (int i = 0; i < buffer.Length; i++)
            {
                ret += formula(buffer[i]);
            }
            return ret;
        }

        private static string GetNormalizedName(string name)
        {
            if (name != null)
            {
                name = name.TrimEnd(Path.DirectorySeparatorChar);
            }
            return name;
        }
        #endregion
    }
}