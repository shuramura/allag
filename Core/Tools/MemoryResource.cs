﻿#region Using directives
using System.Collections.Generic;
using System.IO;
using System.Text;

#endregion

namespace Allag.Core.Tools
{
    public class MemoryResource : BaseResource
    {
        #region Constructors
        public MemoryResource(string value = "", IDictionary<string, string> settings = null)
            : base(ResourceLocation.Memory, value, settings)
        {
        }
        #endregion

        #region Overrides of BaseResource
        public override Stream GetStream(bool readOnly = true)
        {
            byte[] fBytes = new UTF8Encoding().GetBytes(Value);
            return new MemoryStream(fBytes, !readOnly);
        }

        public override string ToString(object parameters)
        {
            return TransformValueToString(Value, parameters);
        }
        #endregion
    }
}