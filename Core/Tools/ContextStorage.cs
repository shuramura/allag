﻿#region Using directives
using System;
using System.Runtime.Remoting.Messaging;

#endregion

namespace Allag.Core.Tools
{
    public class ContextStorage : IContextStorage
    {
        #region Constructors
        public ContextStorage()
        {
            IsLogicalContext = true;
        }
        #endregion

        #region Implementation of IContextStorage
        public virtual TValue GetValue<TValue>(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException("The value cannot be null or empty.", "id");
            }
            Wrapper<TValue> wrapper = (Wrapper<TValue>)(IsLogicalContext ? CallContext.LogicalGetData(id) : CallContext.GetData(id));
            return wrapper != null ? wrapper.Value : default(TValue);
        }

        public virtual IContextStorage SetValue<TValue>(string id, TValue value)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException("The value cannot be null or empty.", "id");
            }
            Wrapper<TValue> wrapper = new Wrapper<TValue> { Value = value };
            if (IsLogicalContext)
            {
                CallContext.LogicalSetData(id, wrapper);  
            }
            else
            {
                CallContext.SetData(id, wrapper); 
            }
            
            return this;
        }
        #endregion

        #region Properties
        public bool IsLogicalContext { get; set; }
        #endregion

        #region Classes
        [Serializable]
        private class Wrapper<TValue> : MarshalByRefObject
        {
            #region Properties
            public TValue Value { get; set; }
            #endregion
        }
        #endregion
    }
}