﻿#region Using directives
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Security.Permissions;
using System.Text.RegularExpressions;

#endregion

namespace Allag.Core.Tools
{
    [HostProtection(SecurityAction.LinkDemand, SharedState = true)]
    public class ResourceTypeConverter : BaseTypeConverter<IResource>
    {
        #region Constants
        private const string OPTION = "option";
        private const string NAME = "name";
        private const string VALUE = "value";
        private const string PATH = "path";

        public const string LocationProperty = "location";
        public const string TypeProperty = "type";
        #endregion

        #region Variables
        private static readonly Regex _regex;
        #endregion

        #region Constructors
        static ResourceTypeConverter()
        {
            _regex = new Regex(string.Format(@"^\[\s*(?<{0}>{{(?<{1}>[a-zA-Z0-9]+):(?<{2}>[^{{}}]+)}}[,\s]*)+\s*](?<{3}>.*)", OPTION, NAME, VALUE, PATH),
                               RegexOptions.Compiled | RegexOptions.Singleline);
        }
        #endregion

        #region Override methods
        protected override IResource ConvertFromString(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            IResource ret = null;
            string str = (string)value;
            Dictionary<string, string> settings = new Dictionary<string, string>();
            ResourceLocation location = ResourceLocation.Memory;
            Match match = _regex.Match(str);
            if (match.Success)
            {
                str = match.Groups[PATH].Value;
                CaptureCollection names = match.Groups[NAME].Captures;
                CaptureCollection values = match.Groups[VALUE].Captures;
                for (int i = 0; i < names.Count; i++)
                {
                    settings.Add(match.Groups[NAME].Captures[i].Value, values[i].Value);
                }
                string strLocation;
                if (settings.TryGetValue(LocationProperty, out strLocation))
                {
                    location = strLocation.ParseEnum<ResourceLocation>(true);
                }
            }
            if (!settings.ContainsKey(LocationProperty))
            {
                settings.Add(LocationProperty, location.ToString());
            }

            switch (location)
            {
                case ResourceLocation.Memory:
                    ret = new MemoryResource(str, settings);
                    break;
                case ResourceLocation.File:
                    ret = new FileResource(str, settings);
                    break;
                case ResourceLocation.Assembly:
                    ret = new AssemblyResource(str, settings);
                    break;
            }
            return ret;
        }

        protected override object ConvertToString(ITypeDescriptorContext context, CultureInfo culture, IResource value)
        {
            return value.ValueWithOptions;
        }
        #endregion
    }
}