﻿#region Using directives
using System.Collections.Generic;
using System.IO;

#endregion

namespace Allag.Core.Tools
{
    public class FileResource : BaseResource
    {
        #region Constructors
        public FileResource(string path, IDictionary<string, string> settings = null)
            : base(ResourceLocation.File, path, settings)
        {
        }
        #endregion

        #region Override methods
        public override Stream GetStream(bool readOnly = true)
        {
            return readOnly ? File.OpenRead(Path) : File.Open(Path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
        }
        #endregion

        #region Methods
        public string Path
        {
            get
            {
                string ret = Value;
                if(!(System.IO.Path.IsPathRooted(ret) && File.Exists(ret)))
                {
                    ret = System.IO.Path.Combine(Application.Base, ret);
                }
                return ret;
            }
        }
        #endregion
    }
}