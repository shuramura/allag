﻿namespace Allag.Core.Tools
{
    public interface IContextStorage
    {
        TValue GetValue<TValue>(string id);
        IContextStorage SetValue<TValue>(string id, TValue value);
    }
}