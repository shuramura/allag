﻿#region Using directives
using System;
using Allag.Core.Reflection;

#endregion

namespace Allag.Core.Tools
{
    public class Context<TItem> where TItem : class
    {
        #region Variables
        private readonly Func<Guid, TItem> _valueFactory;
        private IContextStorage _contextStorage;
        private readonly Guid _id;
        private readonly string _strId;
        #endregion

        #region Constructors
        public Context(Guid id, Func<Guid, TItem> valueFactory = null, IContextStorage contextStorage = null)
        {
            _id = id;
            _strId = id.ToString("N");
            _valueFactory = valueFactory;
            _contextStorage = contextStorage;
        }
        #endregion

        #region Properties
        public Guid Id
        {
            get { return Guid.Parse(_strId); }
        }

        public TItem Value
        {
            get
            {
                TItem ret = ContextStorage.GetValue<TItem>(_strId);
                if (ret == default(TItem) && _valueFactory != null)
                {
                    ret = _valueFactory(_id);
                    ContextStorage.SetValue(_strId, ret);
                }
                return ret;
            }
            set { ContextStorage.SetValue(_strId, value); }
        }

        private IContextStorage ContextStorage
        {
            get { return (_contextStorage ?? (_contextStorage = ObjectFactory.Get<IContextStorage>(this))) ?? new ContextStorage(); }
        }
        #endregion
    }
}