﻿#region Using directives
using System.ComponentModel;
using System.Globalization;
using System.Security.Permissions;
using Allag.Core.Configuration;

#endregion

namespace Allag.Core.Tools
{
    [HostProtection(SecurityAction.LinkDemand, SharedState = true)]
    public abstract class ConfigurableTypeConverter<TObject, TElement> : BaseTypeConverter<TObject>
        where TElement : Element<string>, new()
        where TObject : IConfigurable<TElement>, new()
    {
        #region Abstract properties
        protected abstract ElementCollection<TElement> Elements { get; }
        #endregion

        #region Implementation of BaseTypeConverter<TObject>
        protected override TObject ConvertFromString(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            TElement element = Elements[(string)value];
            return element == null ? CreateElement(context, culture, value) : new TObject {Configuration = element};
        }

        protected override object ConvertToString(ITypeDescriptorContext context, CultureInfo culture, TObject value)
        {
            return value.Configuration != null ? value.Configuration.Name : null;
        }
        #endregion

        #region Methods
        protected virtual TObject CreateElement(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            return new TObject {Configuration = Elements[string.Empty] ?? new TElement()};
        }
        #endregion
    }
}