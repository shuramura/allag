#region Using directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using Allag.Core.Configuration;
using Allag.Core.Configuration.Net;
using SmtpClient = Allag.Core.Net.SmtpClient;

#endregion

namespace Allag.Core.Tools
{
    public class EmailHelper
    {
        #region Variables
        private EmailElement _toEmailElement;
        private SmtpClient _smtpClient;

        private IResource _subject;
        private IResource _message;
        private List<KeyValuePair<string, IResource>> _defaultAttachments;
        #endregion

        #region Constructors
        public EmailHelper(string toEmail, string smtpClient = null)
            : this(CoreCfg.Emails.Items[toEmail] ?? new EmailElement(toEmail, smtpClient), smtpClient != null ? CoreCfg.Servers.Smtps[smtpClient] : null) {}

        public EmailHelper(EmailElement toEmailElement, SmtpElement smtpElement = null)
        {
            Attachments = new Dictionary<string, IResource>();
            Initialize(toEmailElement, smtpElement != null ? new SmtpClient(smtpElement) : null);
        }
        #endregion

        #region Properties
        public SmtpClient SmtpClient
        {
            get { return _smtpClient; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }

                _smtpClient = value;
            }
        }

        public IResource Cc { get; set; }
        public IResource Bcc { get; set; }

        public IResource Subject
        {
            get { return _subject; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }

                _subject = value;
            }
        }

        public IResource Message
        {
            get { return _message; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }

                _message = value;
            }
        }

        public bool IsBodyHtml { get; set; }

        public bool HandleExceptions { get; set; }

        public IDictionary<string, IResource>  Attachments { get; private set; }
        #endregion 

        #region Methods
        private void Initialize(EmailElement toEmailElement, SmtpClient smtpClient)
        {
            if (toEmailElement == null)
            {
                throw new ArgumentNullException("toEmailElement");
            }

            HandleExceptions = true;

            _toEmailElement = toEmailElement;
            SmtpClient = smtpClient ?? _toEmailElement.Smtp;

            _subject = _toEmailElement.Subject;
            _message = _toEmailElement.Message;
            Cc = _toEmailElement.Cc;
            Bcc = _toEmailElement.Bcc;
            IsBodyHtml = _toEmailElement.IsBodyHtml;

            _defaultAttachments = new List<KeyValuePair<string, IResource>>();
            _defaultAttachments.AddRange(_toEmailElement.Attachments.Select(x => new KeyValuePair<string, IResource>(x.Name, x.Resource)));
            if (SmtpClient.Configuration != null)
            {
                _defaultAttachments.AddRange(SmtpClient.Configuration.Attachments.Select(x => new KeyValuePair<string, IResource>(x.Name, x.Resource)));
            }
        }

        public void SendEmail(object parameters = null, MailPriority priority = MailPriority.Normal, string[] attachmentFilePaths = null)
        {
            Dictionary<string, Stream> attachments = new Dictionary<string, Stream>();
            try
            {
                if (attachmentFilePaths != null && attachmentFilePaths.Length > 0)
                {
                    attachments = new Dictionary<string, Stream>(attachmentFilePaths.Length * 2);

                    foreach (string t in attachmentFilePaths)
                    {
                        attachments.Add(Path.GetFileName(t), File.OpenRead(t));
                    }
                }

                SendEmail(parameters, priority, attachments.ToArray());
            }
            finally
            {
                foreach (Stream stream in attachments.Values)
                {
                    stream.Close();
                }
            }
        }

        public void SendEmail(object parameters, MailPriority priority, KeyValuePair<string, Stream>[] attachments)
        {
            using(LogBlock.New<EmailHelper>())
            {
                string strSubject = string.Empty;
                string strMessage = string.Empty;
                string[] toEmails = new string[0];
                string[] ccEmails = new string[0];
                string[] bccEmails = new string[0];
                List<KeyValuePair<string, Stream>> defaultAttachments = new List<KeyValuePair<string, Stream>>();
                try
                {
                    MailAddress from = _smtpClient.GetMailAddress(parameters);
                    strSubject = Subject.ToString(parameters);
                    strMessage = Message.ToString(parameters);
                    toEmails = _toEmailElement.Addresses.ToString(parameters).Split(',');
                    ccEmails = Cc == null ? ccEmails : Cc.ToString(parameters).Split(',');
                    bccEmails = Bcc == null ? bccEmails : Bcc.ToString(parameters).Split(',');

                    LogBlock.Logger.Info("The mail is sending. From=[{0}] To=[{1}] Cc=[{2}] Bcc=[{3}] Subject=[{4}] Body=[{5}].",
                        _smtpClient.Configuration.Email,
                        EmailAddressesToString(toEmails), EmailAddressesToString(ccEmails), EmailAddressesToString(bccEmails),
                        strSubject, strMessage);

                    MailMessage msg = new MailMessage
                    {
                        From = from,
                        Subject = strSubject,
                        Body = strMessage,
                        IsBodyHtml = IsBodyHtml,
                        Priority = priority
                    };
                    AddAddresses(toEmails, msg.To);
                    AddAddresses(ccEmails, msg.CC);
                    AddAddresses(bccEmails, msg.Bcc);

                    List<KeyValuePair<string, Stream>> list = new List<KeyValuePair<string, Stream>>(Attachments.Select(pair => new KeyValuePair<string, Stream>(pair.Key, pair.Value.GetStream())));
                    defaultAttachments.AddRange(Attachments.Select(pair => new KeyValuePair<string, Stream>(pair.Key, pair.Value.GetStream())));
                    defaultAttachments.AddRange(_defaultAttachments.Select(pair => new KeyValuePair<string, Stream>(pair.Key, pair.Value.GetStream())));
                    if (attachments != null)
                    {
                        list.AddRange(attachments);
                    }
                    list.AddRange(defaultAttachments);
                    foreach (KeyValuePair<string, Stream> attachment in list)
                    {
                        attachment.Value.Seek(0, SeekOrigin.Begin);
                        msg.Attachments.Add(new Attachment(attachment.Value, attachment.Key));
                    }

                    lock(_smtpClient)
                    {
                        _smtpClient.Send(msg);
                    }
                }
                catch (Exception e)
                {
                    LogBlock.Logger.Error(string.Format("Error when mailing. From=[{0}] To=[{1}] Cc=[{2}] Bcc=[{3}] Subject=[{4}] Body=[{5}].",
                        _smtpClient.Configuration.Email,
                        EmailAddressesToString(toEmails), EmailAddressesToString(ccEmails), EmailAddressesToString(bccEmails),
                        strSubject, strMessage), e);
                    if (!HandleExceptions)
                    {
                        throw;
                    }
                }
                finally
                {
                    foreach (KeyValuePair<string, Stream> pair in defaultAttachments)
                    {
                        pair.Value.Close();
                    }
                }
            }
        }
        #endregion

        #region Static methods
        private static void AddAddresses(IEnumerable<string> addresses, MailAddressCollection collection)
        {
            foreach (string email in addresses)
            {
                if (!string.IsNullOrWhiteSpace(email))
                {
                    collection.Add(email);
                }
            }
        }

        private static string EmailAddressesToString(IEnumerable addresses)
        {
            return (string)new ArrayTypeConverter<string>().ConvertTo(null, null, addresses, typeof(string));
        }
        #endregion
    }
}