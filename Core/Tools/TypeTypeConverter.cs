﻿#region Using directives
using System;
using System.ComponentModel;
using System.Globalization;
using System.Security.Permissions;
#endregion

namespace Allag.Core.Tools
{
    [HostProtection(SecurityAction.LinkDemand, SharedState = true)]
    public class TypeTypeConverter : BaseTypeConverter<Type>
    {
        #region Overrides of BaseTypeConverter<Type>
        protected override Type ConvertFromString(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            Type ret = null;
            string strValue = (string)value;
            if (!string.IsNullOrWhiteSpace(strValue))
            {
                ret = Type.GetType(strValue, true);
            }
            return ret;
        }

        protected override object ConvertToString(ITypeDescriptorContext context, CultureInfo culture, Type value)
        {
            string ret = string.Empty;
            if (value != null)
            {
                ret = value.AssemblyQualifiedName;
            }
            return ret;
        }
        #endregion
    }
}