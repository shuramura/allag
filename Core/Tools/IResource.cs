﻿#region Using directives
using System.ComponentModel;
using System.IO;

#endregion

namespace Allag.Core.Tools
{
    [TypeConverter(typeof(ResourceTypeConverter))]
    public interface IResource
    {
        string Value { get; }
        string ValueWithOptions { get; }
        
        ResourceLocation Location { get; }
        ResourceType Type { get; }
        Stream GetStream(bool readOnly = true);

        string ToString(object parameters);
    }
}