﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

#endregion

namespace Allag.Core.Tools
{
    public class AssemblyResource : BaseResource
    {
        #region Constants
        public const string AssemblyProperty = "assembly";
        #endregion

        #region Constructors
        public AssemblyResource(string namespacePath, IDictionary<string, string> settings = null)
            : base(ResourceLocation.Assembly, namespacePath, settings)
        {
        }
        #endregion

        #region Overrides of BaseResource
        public override Stream GetStream(bool readOnly = true)
        {
            if(!readOnly)
            {
                throw new NotSupportedException("The assembly resource does not support writable stream.");
            }

            return ResourceAssembly.GetManifestResourceStream(Value);
        }
        #endregion

        #region Properties
        private Assembly ResourceAssembly
        {
            get
            {
                string strValue;
                return Settings.TryGetValue(AssemblyProperty, out strValue) ? Assembly.LoadFrom(new Uri(Path.Combine(Path.GetDirectoryName(Application.Root.Assembly.CodeBase), strValue)).LocalPath) : Application.Root.Assembly;
            }
        }
        #endregion
    }
}