﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Xsl;

#endregion

namespace Allag.Core.Tools
{
    public abstract class BaseResource : IResource
    {
        #region Constants
        private const string TYPE_PROPERTY = "type";
        #endregion

        #region Constructors
        protected BaseResource(ResourceLocation location, string value, IDictionary<string, string> settings = null)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            if (settings == null)
            {
                settings = new Dictionary<string, string> {{ResourceTypeConverter.LocationProperty, location.EnumToString()}};
            }

            string strLocation;
            if (!settings.TryGetValue(ResourceTypeConverter.LocationProperty, out strLocation) ||
                strLocation.ToLower() != location.ToString().ToLower())
            {
                throw new ArgumentException(string.Format("The setting's location property '{0}' does not correspond to the resource location '{1}'.", strLocation, location));
            }

            Location = location;
            Value = value;

            Settings = new Dictionary<string, string>(settings);
        }
        #endregion

        #region Implementation of IResource
        public abstract Stream GetStream(bool readOnly = true);

        public string ValueWithOptions
        {
            get { return string.Format("[{0}]{1}", Settings.Join(",", "{", "}", x => x.Key + ':' + x.Value), Value); }
        }

        public string Value { get; private set; }

        public ResourceLocation Location { get; private set; }

        public ResourceType Type
        {
            get
            {
                ResourceType ret = ResourceType.Text;
                string strValue;
                if (Settings.TryGetValue(TYPE_PROPERTY, out strValue))
                {
                    ret = strValue.ParseEnum<ResourceType>(true);
                }
                return ret;
            }
            set
            {
                string strValue = value.EnumToString();
                if (Settings.ContainsKey(TYPE_PROPERTY))
                {
                    Settings[TYPE_PROPERTY] = strValue;
                }
                else
                {
                    Settings.Add(TYPE_PROPERTY, strValue);
                }
            }
        }

        public virtual string ToString(object parameters)
        {
            using(StreamReader reader = new StreamReader(GetStream()))
            {
                return TransformValueToString(reader.ReadToEnd(), parameters);
            }
        }
        #endregion

        #region Properties
        protected IDictionary<string, string> Settings { get; private set; }
        #endregion

        #region Override methods
        public override string ToString()
        {
            return ToString(null);
        }
        #endregion

        #region Methods
        protected string TransformValueToString(string value, object parameters = null)
        {
            if (Type == ResourceType.Xslt)
            {
                string strXml = "<root />";
                if (parameters != null)
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    parameters.Serialize(stringBuilder);
                    strXml = stringBuilder.ToString();
                }
               
                StringBuilder result = new StringBuilder(1000);
                using(TextWriter textWriter = new StringWriter(result))
                {
                    using(XmlReader xmlReader = XmlReader.Create(new StringReader(strXml)))
                    {
                        using(XmlReader xsltReader = XmlReader.Create(new StringReader(value)))
                        {
                            XslCompiledTransform xslt = new XslCompiledTransform();
                            xslt.Load(xsltReader);
                            xslt.Transform(xmlReader, null, textWriter);
                        }
                    }
                }
                value = result.ToString();
            }
            return value;
        }
        #endregion
    }
}