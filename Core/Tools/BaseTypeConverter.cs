﻿#region Using directives
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
#endregion

namespace Allag.Core.Tools
{
    [HostProtection(SecurityAction.LinkDemand, SharedState = true)]
    public abstract class BaseTypeConverter<TType> : TypeConverter
    {
        #region Variables
        private readonly Dictionary<Type, Func<ITypeDescriptorContext, CultureInfo, object, TType>> _fromConverters;
        private readonly Dictionary<Type, Func<ITypeDescriptorContext, CultureInfo, TType, object>> _toConverters;
        #endregion

        #region Constructors
        protected BaseTypeConverter()
        {
            _fromConverters = new Dictionary<Type, Func<ITypeDescriptorContext, CultureInfo, object, TType>>();
            _toConverters = new Dictionary<Type, Func<ITypeDescriptorContext, CultureInfo, TType, object>>();

            AddFromType<string>(ConvertFromString);
            AddToType<string>(ConvertToString);
        }
        #endregion

        #region Abstract methods
        protected abstract TType ConvertFromString(ITypeDescriptorContext context, CultureInfo culture, object value);
        protected abstract object ConvertToString(ITypeDescriptorContext context, CultureInfo culture, TType value);
        #endregion

        #region Override methods
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return CanConvert(sourceType, _fromConverters.Keys) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            object ret;
            Type valueType = GetValueType(value);
            if (!CanConvert(valueType, _fromConverters.Keys))
            {
                ret = base.ConvertFrom(context, culture, value);
            }
            else
            {
                Func<ITypeDescriptorContext, CultureInfo, object, TType> converter;
                if (_fromConverters.TryGetValue(valueType, out converter) || (converter = _fromConverters.FirstOrDefault(x => x.Key.IsAssignableFrom(valueType)).Value) != null)
                {
                    ret = converter(context, culture, value);
                }
                else
                {
                    ret = ConvertFrom(context, culture, (TType) value);
                }
            }

            return ret;
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return CanConvert(destinationType, _toConverters.Keys) || base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value,
                                         Type destinationType)
        {
            object ret;
            Type valueType = GetValueType(value);
            if (!CanConvert(destinationType, _toConverters.Keys) || !typeof (TType).IsAssignableFrom(valueType))
            {
                ret = base.ConvertTo(context, culture, value, destinationType);
            }
            else
            {
                TType instance = (TType) value;
                Func<ITypeDescriptorContext, CultureInfo, TType, object> converter;
                if (_toConverters.TryGetValue(destinationType, out converter) || (converter = _toConverters.FirstOrDefault(x => x.Key.IsAssignableFrom(destinationType)).Value) != null)
                {
                    ret = converter(context, culture, instance);
                }
                else
                {
                    ret = ConvertTo(context, culture, instance, destinationType);
                }
            }
            return ret;
        }
        #endregion

        #region Methods
        protected virtual TType ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, TType value)
        {
            return value;
        }

        protected virtual object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, TType value,
                                           Type destinationType)
        {
            return value;
        }

        protected void AddFromType<TFromType>(Func<ITypeDescriptorContext, CultureInfo, object, TType> converterFrom)
        {
            _fromConverters.Add(typeof (TFromType), converterFrom);
        }

        protected void AddToType<TToType>(Func<ITypeDescriptorContext, CultureInfo, TType, object> converterTo)
        {
            _toConverters.Add(typeof (TToType), converterTo);
        }

        protected virtual bool CanConvert(Type type, ICollection<Type> supportedTypes)
        {
            return type != null && (supportedTypes.Any(x => x.IsAssignableFrom(type)) || typeof (TType).IsAssignableFrom(type));
        }
        #endregion

        #region Static methods
        private static Type GetValueType(object value)
        {
            Type ret = null;
            if (value != null)
            {
                ret = value.GetType();
            }
            return ret;
        }
        #endregion
    }
}