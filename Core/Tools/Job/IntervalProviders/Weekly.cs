#region Using directives

using System;

#endregion

namespace Allag.Core.Tools.Job.IntervalProviders
{
    public class Weekly : BaseTimeIntervalProvider
    {
        #region Variables
        private readonly System.DayOfWeek _dayOfWeek;
        #endregion

        #region Constructors

        public Weekly(System.DayOfWeek dayOfWeek, double every)
            : base(ScheduleType.Weekly, every)
        {
            _dayOfWeek = dayOfWeek;
        }

        #endregion

        #region override methods

        public override DateTime GetNextRun(DateTime lastRun)
        {
            CheckDisposed();

            bool isFirstTime = false;
            if (lastRun == DateTime.MinValue)
            {
                lastRun = DateTime.Now;
                isFirstTime = true;
            }
            DateTime ret = lastRun;
            ret = new DateTime(ret.Year, ret.Month, ret.Day, StartTime.Hours, StartTime.Minutes, StartTime.Seconds);
            if (ret < lastRun)
            {
                ret = ret.AddDays(1);
            }

            while (_dayOfWeek != ret.DayOfWeek)
            {
                ret = ret.AddDays(1);
            }

            if (!isFirstTime)
            {
                ret = ret.AddDays(7 * Every);
            }

            return ret;
        }

        #endregion
    }
}
