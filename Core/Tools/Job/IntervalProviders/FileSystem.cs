﻿#region Using directives
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

#endregion

namespace Allag.Core.Tools.Job.IntervalProviders
{
    public class FileSystem : BaseIntervalProvider
    {
        #region Variables
        private readonly ConcurrentDictionary<IJob, bool> _startedJobs;
        private readonly FileSystemWatcher _watcher;
        private Timer _timer;
        #endregion

        #region Constructors
        public FileSystem(string path, string mask) : base(ScheduleType.FileSystem)
        {
            using(LogBlock.New<FileSystem>(new {path, filter = mask}))
            {
                if (string.IsNullOrWhiteSpace(path))
                {
                    throw new ArgumentNullOrEmptyException("path");
                }
               
                _timer = null;
                _startedJobs = new ConcurrentDictionary<IJob, bool>();
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                _watcher = new FileSystemWatcher(path, mask){NotifyFilter = NotifyFilters.LastWrite};
                _watcher.Created += WatcherEvent;
                _watcher.Changed += WatcherEvent;
                _watcher.Deleted += WatcherEvent;
                _watcher.Renamed += WatcherEvent;
            }
        }
        #endregion

        #region Properties
        public NotifyFilters Filter
        {
            get { return _watcher.NotifyFilter; }
            set { _watcher.NotifyFilter = value; }
        }
        public int EventBuffer
        {
            get { return _watcher.InternalBufferSize; }
            set { _watcher.InternalBufferSize = value; }
        }

        public bool Subdirectories
        {
            get { return _watcher.IncludeSubdirectories; }
            set { _watcher.IncludeSubdirectories = value; }
        }
        #endregion

        #region Override methods
        public override void Start(IJob job)
        {
            using(LogBlock.New<FileSystem>())
            {
                base.Start(job);

                if (_startedJobs.TryAdd(job, false))
                {
                    if (!_watcher.EnableRaisingEvents)
                    {
                        _watcher.EnableRaisingEvents = true;    
                    }
                    if (RunImmediately)
                    {
                        Parallel.Invoke(() => WatcherEvent(_watcher, new FileSystemEventArgs(WatcherChangeTypes.Changed, string.Empty, string.Empty)));
                    }
                }
            }
        }

        public override void Stop(IJob job)
        {
            base.Stop(job);
            bool value;
            if (_startedJobs.TryRemove(job, out value) && _watcher.EnableRaisingEvents)
            {
                _watcher.EnableRaisingEvents = false;
            }
        }

        protected override void Disposing()
        {
            base.Disposing();
            _watcher.Dispose();
            if (_timer != null)
            {
                _timer.Dispose();
                _timer = null;
            }
        }
        #endregion

        #region Event's handlers
        private void WatcherEvent(object sender, FileSystemEventArgs fileSystemEventArgs)
        {
            TimeSpan now = DateTime.Now.TimeOfDay;
            if (StartTime < now && now < StopTime)
            {
                Thread.Sleep(500);
                foreach (KeyValuePair<IJob, bool> pair in _startedJobs)
                {
                    if (!pair.Value)
                    {
                        _startedJobs[pair.Key] = true;
                        Jobs[pair.Key](new IJob[]{ pair.Key});
                        _startedJobs[pair.Key] = false;
                    }
                }    
            }
            else
            {
                lock(this)
                {
                    if (_timer == null)
                    {
                        _timer = new Timer(TimerCallback, null, StartTime - now, new TimeSpan(-1));
                    }
                }
            }
        }

        private void TimerCallback(object state)
        {
            _timer.Dispose();
            _timer = null;
            WatcherEvent(_watcher, new FileSystemEventArgs(WatcherChangeTypes.Changed, string.Empty, string.Empty));   
        }
        #endregion
    }
}