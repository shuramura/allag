#region Using directives
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;

#endregion

namespace Allag.Core.Tools.Job.IntervalProviders
{
    public abstract class BaseIntervalProvider : IIntervalProvider
    {
        #region Variables
        private readonly ConcurrentDictionary<IJob, Action<IJob[]>> _jobActions;
        #endregion

        #region Constructors
        protected BaseIntervalProvider(ScheduleType scheduleType)
        {
            using(LogBlock.New<BaseIntervalProvider>())
            {
                IsDisposed = false;

                _jobActions = new ConcurrentDictionary<IJob, Action<IJob[]>>();
                RunImmediately = false;
                StartTime = TimeSpan.Zero;
                StopTime = TimeSpan.MaxValue;

                ScheduleType = scheduleType;
            }
        }

        ~BaseIntervalProvider()
        {
            Dispose(false);
        }
        #endregion

        #region Implementation of IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }   
        #endregion

        #region Implementation of IIntervalProvider
        public virtual void Add(IJob job, Action<IJob[]> action)
        {
            if (job == null)
            {
                throw new ArgumentNullException("job");
            }

            if (action == null)
            {
                throw new ArgumentNullException("action");
            }
            _jobActions.TryAdd(job, action);
        }

        public virtual void Start(IJob job)
        {
            CheckDisposed();
            if (!Jobs.ContainsKey(job))
            {
                throw new ArgumentException("The job does not belong to the provider.", "job");
            }
        }
        public virtual void Stop(IJob job)
        {
            CheckDisposed();
            if (!Jobs.ContainsKey(job))
            {
                throw new ArgumentException("The job does not belong to the provider.", "job");
            }
        }
        #endregion

        #region Properties
        public ScheduleType ScheduleType { get; private set; }
        public bool RunImmediately { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan StopTime { get; set; }

        public IReadOnlyDictionary<IJob, Action<IJob[]>> Jobs
        {
            get
            {
                CheckDisposed();
                return new ReadOnlyDictionary<IJob, Action<IJob[]>>(_jobActions);
            }
        }

        protected bool IsDisposed { get; private set; }
        #endregion

        #region Methods
        private void Dispose(bool isDisposing)
        {
            if (!IsDisposed)
            {
                if (isDisposing)
                {
                    Disposing();
                }
            }
            IsDisposed = true;
        }

        protected virtual void Disposing() {}

        protected void CheckDisposed()
        {
            if (IsDisposed)
            {
                throw new ObjectDisposedException(GetType().FullName);
            }
        }
        #endregion
    }
}