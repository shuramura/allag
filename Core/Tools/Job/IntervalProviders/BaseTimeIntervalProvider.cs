﻿#region Using directives
using System;
using System.Collections.Concurrent;
using System.Threading;

#endregion

namespace Allag.Core.Tools.Job.IntervalProviders
{
    public abstract class BaseTimeIntervalProvider : BaseIntervalProvider
    {
        #region Variables
        private static readonly int _nInternalTimerInterval;
        private readonly ConcurrentDictionary<IJob, JobInfo> _jobs;
        #endregion

        #region Constructors
        static BaseTimeIntervalProvider()
        {
            _nInternalTimerInterval = Int32.MaxValue;
        }

        protected BaseTimeIntervalProvider(ScheduleType scheduleType, double every)
            : base(scheduleType)
        {
            using(LogBlock.New<BaseTimeIntervalProvider>(new {scheduleType, every}))
            {
                if (every <= 0)
                {
                    throw new ArgumentException("The parameter should be greater then zero.", nameof(every));
                }

                _jobs = new ConcurrentDictionary<IJob, JobInfo>();

                Every = every;
            }
        }
        #endregion

        #region Properties
        public double Every { get; private set; }
        #endregion

        #region Abstract methods
        public abstract DateTime GetNextRun(DateTime lastRun);
        #endregion

        #region Override methods
        public override void Start(IJob job)
        {
            using(LogBlock.New<BaseTimeIntervalProvider>())
            {
                base.Start(job);
                _jobs.AddOrUpdate(job, add => new JobInfo(job, RunImmediately, this, TaskExecuting), (update, current) => current);
            }
        }

        public override void Stop(IJob job)
        {
            using(LogBlock.New<BaseTimeIntervalProvider>())
            {
                base.Stop(job);

                JobInfo jobInfo;
                if (_jobs.TryRemove(job, out jobInfo))
                {
                    jobInfo.Dispose();
                }
            }
        }

        protected override void Disposing()
        {
            lock (_jobs)
            {
                base.Disposing();
                foreach (JobInfo jobInfo in _jobs.Values)
                {
                    jobInfo.Dispose();
                }
            }
        }
        #endregion

        #region Methods
        private void TaskExecuting(object state)
        {
            using(LogBlock.New<BaseTimeIntervalProvider>())
            {
                Action<IJob[]> action = null;
                JobInfo jobInfo = (JobInfo)state;
                IJob job = jobInfo.Job;
                lock(_jobs)
                {
                    if (job != null)
                    {
                        action = Jobs[job];
                    }
                }
                
                DateTime lastRun = DateTime.Now;
                if (action != null)
                {
                    action(new IJob[] {job});
                }

                lock(_jobs)
                {
                    if (job != null && _jobs.ContainsKey(job))
                    {
                        jobInfo.SetNextRun(lastRun);
                    }    
                }
                
            }
        }
        #endregion

        #region Classes
        private class JobInfo : IDisposable
        {
            #region Variables
            private bool _isDisposed;
            private readonly TimerCallback _timerCallback;
            private readonly Timer _timer;
            private DateTime _dtNextRun;
            #endregion

            #region Constructors
            public JobInfo(IJob job, bool runImmediately, BaseTimeIntervalProvider intervalProvider, TimerCallback callback)
            {
                _isDisposed = false;
                _dtNextRun = DateTime.MinValue;

                Job = job;
                IntervalProvider = intervalProvider;
                _timerCallback = callback;
                _timer = new Timer(InternalTimerHandler);
                if (runImmediately)
                {
                    SetTimer(DateTime.Now.AddMilliseconds(100));
                }
                else
                {
                    SetNextRun(DateTime.MinValue);
                }
            }

            ~JobInfo()
            {
                Dispose(false);
            }
            #endregion

            #region Implementation of IDisposable
            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            #endregion

            #region Properties
            public IJob Job { get; private set; }
            private BaseTimeIntervalProvider IntervalProvider { get; set; }
            #endregion

            #region Methods
            public void SetNextRun(DateTime lastRun)
            {
                SetTimer(IntervalProvider.GetNextRun(lastRun));
            }

            private void Dispose(bool isDisposing)
            {
                lock(_timer)
                {
                    if (!_isDisposed)
                    {
                        if (isDisposing)
                        {
                            _timer.Dispose();
                            Job = null;
                        }
                        _isDisposed = true;
                    }
                }
            }

            private void SetTimer(DateTime nextRun)
            {
                using(LogBlock.New<JobInfo>(new {nextRun}))
                {
                    lock(_timer)
                    {
                        if (!_isDisposed)
                        {
                            _dtNextRun = nextRun;
                            _timer.Change(0, 0);
                            LogBlock.Logger.Info("The job '{0}' will be run at {1}, after {2} seconds.", Job.Name, _dtNextRun, (_dtNextRun - DateTime.Now).TotalSeconds);
                        }
                    }
                }
            }

            private void InternalTimerHandler(object state)
            {
                double dInterval = (_dtNextRun - DateTime.Now).TotalMilliseconds;
                if (dInterval <= 0)
                {
                    _timerCallback(this);
                }
                else
                {
                    lock(_timer)
                    {
                        if (!_isDisposed)
                        {
                            _timer.Change(dInterval <= _nInternalTimerInterval ? (long)dInterval : _nInternalTimerInterval, 0);
                        }
                    }
                }
            }
            #endregion
        }
        #endregion
    }
}