#region Using directives
using System;

#endregion

namespace Allag.Core.Tools.Job.IntervalProviders
{
    public class Simple : BaseTimeIntervalProvider
    {
        #region Constructors
        public Simple(ScheduleType scheduleType, double every)
            : base(scheduleType, every)
        {
            using(LogBlock.New<Simple>())
            {
                if(scheduleType != ScheduleType.BySecond &&
                   scheduleType != ScheduleType.ByMinute && scheduleType != ScheduleType.Hourly)
                {
                    throw new ArgumentException("The parameter cannot be equal to " + scheduleType, "scheduleType");
                }
            }
        }
        #endregion

        #region Override methods
        public override DateTime GetNextRun(DateTime lastRun)
        {
            CheckDisposed();

            DateTime dtNextRun = lastRun;

            for(int i = 0; i < 2; i++)
            {
                if(i == 0 && dtNextRun == DateTime.MinValue)
                {
                    dtNextRun = DateTime.Now;
                }
                else
                {
                    switch(ScheduleType)
                    {
                        case ScheduleType.BySecond:
                            dtNextRun = dtNextRun.AddSeconds(Every);
                            break;
                        case ScheduleType.ByMinute:
                            dtNextRun = dtNextRun.AddMinutes(Every);
                            break;
                        case ScheduleType.Hourly:
                            dtNextRun = dtNextRun.AddHours(Every);
                            break;
                    }
                }

                if (StopTime < dtNextRun.TimeOfDay || StartTime > dtNextRun.TimeOfDay)
                {
                    if(i == 1)
                    {
                        dtNextRun = DateTime.MaxValue;
                        break;
                    }
                    if (StopTime != TimeSpan.Zero && StopTime < dtNextRun.TimeOfDay)
                    {
                        dtNextRun = dtNextRun.AddDays(1).Date;
                    }
                    if (StartTime != TimeSpan.Zero && StartTime > dtNextRun.TimeOfDay)
                    {
                        dtNextRun = new DateTime(dtNextRun.Year, dtNextRun.Month, dtNextRun.Day, StartTime.Hours, StartTime.Minutes, StartTime.Seconds);
                    }
                }
                else
                {
                    break;
                }
            }
            return dtNextRun;
        }
        #endregion
    }
}