#region Using directives
using System;
using System.Threading;

#endregion

namespace Allag.Core.Tools.Job
{
    public interface IJob : IDisposable
    {
        string Name { get; set; }
        string Description { get; }
     
        object Run(IScheduler scheduler, CancellationToken cancellationToken);
    }
}