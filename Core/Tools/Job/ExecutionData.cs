﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;

#endregion

namespace Allag.Core.Tools.Job
{
    public class ExecutionData
    {
        #region Variables
        private readonly List<Tuple<string, Exception>> _warnings;
        private readonly Dictionary<string, Stream> _files;
        #endregion

        #region Constructors
        public ExecutionData(IScheduler scheduler, IJob job, CancellationToken cancellationToken)
        {
            if (scheduler == null)
            {
                throw new ArgumentNullException("scheduler");
            }

            if (job == null)
            {
                throw new ArgumentNullException("job");
            }

            CancellationToken = cancellationToken;
            Scheduler = scheduler;
            Job = job;
            _warnings = new List<Tuple<string, Exception>>();
            _files = new Dictionary<string, Stream>();
            StartTime = DateTime.UtcNow;
            EndTime = DateTime.MinValue;
        }
        #endregion

        #region Properties
        public CancellationToken CancellationToken { get; private set; }
        public IScheduler Scheduler { get; private set; }
        public IJob Job { get; private set; }
        public DateTime StartTime { get; private set; }
        public DateTime EndTime { get; private set; }
       
        public IEnumerable<Tuple<string, Exception>>   Warnings
        {
            get { return _warnings; }
        }

        public IReadOnlyDictionary<string, Stream> Files
        {
            get { return new ReadOnlyDictionary<string, Stream>(_files); }
        }

        public Exception Exception { get; set; }
        #endregion

        #region Methods
        public virtual string AddWarning(string message, params object[] args)
        {
            return AddWarning(null, message, args);
        }

        public virtual string AddWarning(Exception exception)
        {
            if (exception == null)
            {
                throw new ArgumentNullException("exception");
            }
            return AddWarning(exception, exception.Message);
        }

        public virtual string AddWarning(Exception exception, string message, params object[] args)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentNullOrEmptyException("message");
            }

            Tuple<string, Exception> item = new Tuple<string, Exception>(string.Format(message, args), exception);
            _warnings.Add(item);
            return item.Item1;
        }

        public virtual void AddFile(string name, Stream content)
        {
            _files.Add(name, content);
        }

        public virtual void Finish()
        {
            EndTime = DateTime.UtcNow;
        }
        #endregion
    }
}