#region Using directives
using System;

#endregion

namespace Allag.Core.Tools.Job
{
    public interface IIntervalProvider : IDisposable
    {
        void Add(IJob job, Action<IJob[]> action);
        void Start(IJob job);
        void Stop(IJob job);
    }
}