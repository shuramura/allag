﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Allag.Core.Net;
using Allag.Core.Tools.Archives;

#endregion

namespace Allag.Core.Tools.Job
{
    public class DownloadFile<TResult, TItemResult> : BaseFileSystemEntryJob<TResult, TItemResult>
        where TResult : DownloadFileResult<TItemResult>, new()
        where TItemResult : DownloadFileItemResult, new()
    {
        #region Variables
        private IArchiveFile _archiveFile;
        #endregion

        #region Constructors
        public DownloadFile(string fileClientConfig)
        {
            RemoteFolder = string.Empty;
            FolderTemplate = null;
            AllowDuplication = false;
            Unarchive = true;
            UnarchivedExtension = "unarchived";
            TempExtension = "temp";

            FileManager = new FileManager(fileClientConfig);
        }
        #endregion

        #region Properties
        public FileManager FileManager { get; private set; }

        public string RemoteFolder { get; set; }

        public bool Unarchive { get; set; }
        public string UnarchivedExtension { get; set; }
        public string TempExtension { get; set; }

        public bool AllowDuplication { get; set; }

        public IArchiveFile ArchiveFile
        {
            get { return _archiveFile ?? (_archiveFile = new ZipFile()); }
            set { _archiveFile = value; }
        }
        #endregion

        #region Override methods
        protected override IEnumerable<FileSystemInfo> GetItems(TResult result, ExecutionData data)
        {
            using(LogBlock.New<DownloadFile>())
            {
                Func<string, string> pathTransformer;
                if (AllowDuplication)
                {
                    pathTransformer = GetUniqueNormalizedItemName;
                }
                else
                {
                    pathTransformer = path => path;
                }
                result.Files = new List<string>();
                return base.GetItems(result, data).Concat(FileManager.DownloadEnumeration(RemoteFolder, ProcessingFolder ?? SourceFolder, FileTemplate, true, pathTransformer).Select(x =>
                {
                    result.Files.Add(Path.GetFileName(x));
                    return new FileInfo(x);
                }));
            }
        }

        protected override void Initialization(TResult result, ExecutionData data)
        {
            using(LogBlock.New<DownloadFile>())
            {
                result.RemoteFolder = RemoteFolder;
                result.Unarchive = Unarchive;
                result.AllowDuplication = AllowDuplication;

                base.Initialization(result, data);

                if (string.IsNullOrEmpty(RemoteFolder))
                {
                    throw new InvalidDataException("The remote folder is not specified.");
                }
            }
        }

        protected override bool ProcessFile(TResult result, FileInfo item, TItemResult itemResult, ExecutionData data)
        {
            Func<string, string> pathTransformer;
            if (AllowDuplication)
            {
                pathTransformer = GetUniqueNormalizedItemName;
            }
            else
            {
                pathTransformer = path => path;
            }
            if (Unarchive)
            {
                itemResult.FileNames = new List<string>();
                string strPath = Path.ChangeExtension(pathTransformer(item.FullName), TempExtension ?? UnarchivedExtension);
                using(IArchiveFile archive = ArchiveFile.Open(item.FullName))
                {
                    archive.ExtractAll(strPath, archiveItem =>
                    {
                        if (!archiveItem.IsFolder)
                        {
                            itemResult.FileNames.Add(archiveItem.FullName);
                        }
                    });
                }
                try
                {
                    if (ProcessingFolder != null)
                    {
                        string strNewPath = pathTransformer(Path.Combine(SourceFolder, Path.GetFileName(item.Name)));
                        RepeatAction(Directory.Move, strPath, strNewPath);
                        strPath = strNewPath;
                    }
                    if (TempExtension != null)
                    {
                        RepeatAction(Directory.Move, strPath, pathTransformer(Path.ChangeExtension(strPath, UnarchivedExtension)));
                    }
                }
                catch (Exception)
                {
                    if (Directory.Exists(strPath))
                    {
                        RepeatAction(Directory.Delete, strPath, true);
                    }
                    throw;
                }
            }
            else if (ProcessingFolder != null)
            {
                item.MoveTo(pathTransformer(Path.Combine(SourceFolder, item.Name)));
            }

            return base.ProcessFile(result, item, itemResult, data);
        }
        #endregion
    }

    public class DownloadFile : DownloadFile<DownloadFileResult<DownloadFileItemResult>, DownloadFileItemResult>
    {
        #region Constructors
        public DownloadFile(string fileClientConfig) : base(fileClientConfig) {}
        #endregion
    }
}