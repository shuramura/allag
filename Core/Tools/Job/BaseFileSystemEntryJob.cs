﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

#endregion

namespace Allag.Core.Tools.Job
{
    public class BaseFileSystemEntryJob<TResult, TItemResult> : BaseItemJob<TResult, FileSystemInfo, TItemResult>
        where TResult : BaseFileSystemEntryJobResult<TItemResult>, new()
        where TItemResult : BaseFileSystemEntryItemResult, new()
    {
        #region Variables
        private string _strArchiveFolder;
        private string _strErrorFolder;
        private string _srtSourceFolder;
        private string _srtProcessingFolder;
        #endregion

        #region Constructors
        protected BaseFileSystemEntryJob()
        {
            _strArchiveFolder = null;
            _strErrorFolder = null;
            _srtSourceFolder = null;
            _srtProcessingFolder = null;

            FileTemplate = ".+";
            FolderTemplate = ".+";
        }
        #endregion

        #region Properties
        public string ArchiveFolder
        {
            get { return _strArchiveFolder; }
            set { _strArchiveFolder = NormalizePath(value); }
        }

        public string ErrorFolder
        {
            get { return _strErrorFolder; }
            set { _strErrorFolder = NormalizePath(value); }
        }

        public string SourceFolder
        {
            get { return _srtSourceFolder; }
            set { _srtSourceFolder = NormalizePath(value); }
        }

        public string ProcessingFolder
        {
            get { return _srtProcessingFolder; }
            set { _srtProcessingFolder = NormalizePath(value); }
        }

        public string FolderTemplate { get; set; }
        public string TriggerFileTemplate { get; set; }
        public string FileTemplate { get; set; }
        #endregion

        #region Override methods
        protected virtual bool ProcessFolder(TResult result, DirectoryInfo item, TItemResult itemResult, ExecutionData data)
        {
            return true;
        }

        protected virtual bool ProcessFile(TResult result, FileInfo item, TItemResult itemResult, ExecutionData data)
        {
            return true;
        }

        protected override IEnumerable<FileSystemInfo> GetItems(TResult result, ExecutionData data)
        {
            foreach (FileSystemInfo entry in EnumerateFileSystemEntries(ProcessingFolder))
            {
                yield return entry;
            }
            foreach (FileSystemInfo entry in EnumerateFileSystemEntries(SourceFolder))
            {
                yield return entry;
            }
        }

        protected override void Initialization(TResult result, ExecutionData data)
        {
            using(LogBlock.New(GetType()))
            {
                ValidateFolder(ArchiveFolder, "ArchiveFolder");
                ValidateFolder(ErrorFolder, "ErrorFolder");
                ValidateFolder(SourceFolder, "SourceFolder");
                ValidateFolder(ProcessingFolder, "ProcessingFolder");

                if (SourceFolder == null)
                {
                    throw new InvalidDataException("The source folder is not specified.");
                }

                result.SourceFolder = SourceFolder;
                result.ProcessingFolder = ProcessingFolder;
                result.FileTemplate = FileTemplate;
                result.FolderTemplate = FolderTemplate;
                result.ArchiveFolder = ArchiveFolder;
                result.ErrorFolder = ErrorFolder;

                base.Initialization(result, data);
            }
        }

        protected override sealed void Execute(TResult result, FileSystemInfo item, TItemResult itemResult, ExecutionData data)
        {
            using(LogBlock.New(GetType()))
            {
                FileInfo fileInfo = item as FileInfo;
                if (ProcessingFolder != null && (Path.GetDirectoryName(item.FullName) + Path.DirectorySeparatorChar) != Path.GetFullPath(ProcessingFolder))
                {
                    string strPath = Path.Combine(ProcessingFolder, item.Name);
                    Directory.Move(item.FullName, strPath);
                    if (fileInfo != null)
                    {
                        item = fileInfo = new FileInfo(strPath);
                    }
                    else
                    {
                        item = new DirectoryInfo(strPath);
                    }
                }

                try
                {
                    itemResult.Name = item.Name;
                    if (fileInfo != null)
                    {
                        itemResult.IsHandled = ProcessFile(result, fileInfo, itemResult, data);
                    }
                    else
                    {
                        itemResult.IsHandled = ProcessFolder(result, (DirectoryInfo)item, itemResult, data);
                    }
                   
                    if (itemResult.IsHandled)
                    {
                        if (ArchiveFolder != null)
                        {
                            RepeatAction(Directory.Move, item.FullName, GetUniqueNormalizedItemName(Path.Combine(ArchiveFolder, item.Name)));
                            LogBlock.Logger.Info("The item '{0}' was archived.", item.Name);
                        }
                        else if (item.Exists)
                        {
                            Delete(item);
                            LogBlock.Logger.Info("The item '{0}' was deleted.", item.Name);
                        }
                    }
                }
                catch (Exception)
                {
                    HandleErrorItem(item, data, "The {1} '{0}' cannot be processed.", item.Name, fileInfo == null ? "folder" : "file");
                    throw;
                }
            }
        }
        #endregion

        #region Methods
        protected string GetUniqueNormalizedItemName(string path)
        {
            if (File.Exists(path) || Directory.Exists(path))
            {
                path = GetUniqueNormalizedItemName(Path.Combine(Path.GetDirectoryName(path), string.Format("{0}_{1}{2}", Path.GetFileNameWithoutExtension(path), DateTime.Now.ToString("yyMMddHHmmssFFF"), Path.GetExtension(path))));
            }
            return path;
        }

        protected void HandleErrorItem(FileSystemInfo item, ExecutionData data, string messageTemplate, params object[] parameters)
        {
            data.AddWarning(messageTemplate, parameters);
            if (item.Exists)
            {
                if (ErrorFolder != null)
                {
                    RepeatAction(Directory.Move, item.FullName, GetUniqueNormalizedItemName(Path.Combine(ErrorFolder, item.Name)));
                }
                else
                {
                    Delete(item);
                }
            }
        }

        private IEnumerable<FileSystemInfo> EnumerateFileSystemEntries(string folder)
        {
            if (folder != null)
            {
                const RegexOptions options = RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.Singleline;
                if (FolderTemplate != null)
                {
                    Regex reFolder = new Regex(FolderTemplate, options);
                    Regex reFile = string.IsNullOrEmpty(TriggerFileTemplate) ? null : new Regex(TriggerFileTemplate, options);
                    foreach (string item in Directory.EnumerateDirectories(folder).Where(x => reFolder.IsMatch(Path.GetFileName(x)) && (reFile == null || Directory.EnumerateFiles(x).Any(f => reFile.IsMatch(Path.GetFileName(f))))))
                    {
                        yield return new DirectoryInfo(item);
                    }
                }
                if (FileTemplate != null)
                {
                    Regex reFile = new Regex(FileTemplate, options);
                    foreach (string item in Directory.EnumerateFiles(folder).Where(x => reFile.IsMatch(Path.GetFileName(x))))
                    {
                        yield return new FileInfo(item);
                    }
                }
            }
        }
        #endregion

        #region Static methods
        protected static void RepeatAction(Action action, byte count = 3)
        {
            RepeatAction(x => action(), 0, count);
        }

        protected static void RepeatAction<TArg0>(Action<TArg0> action, TArg0 arg0, byte count = 3)
        {
            RepeatAction((x, y) => action(x), arg0, 0, count);
        }

        protected static void RepeatAction<TArg0, TArg1>(Action<TArg0, TArg1> action, TArg0 arg0, TArg1 arg1, byte count = 3)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }
            if (count == 0)
            {
                throw new ArgumentOutOfRangeException("count", "The argument cannot be equal to zero.");
            }

            bool isInProgress = true;
            for (byte i = 0; isInProgress && i < count; i++)
            {
                try
                {
                    action(arg0, arg1);
                    isInProgress = false;
                }
                catch (Exception)
                {
                    if (i == count - 1)
                    {
                        throw;
                    }
                    Thread.Sleep(100);
                }
            }
        }

        private static void Delete(FileSystemInfo info)
        {
            DirectoryInfo directoryInfo = info as DirectoryInfo;
            if (directoryInfo != null)
            {
                RepeatAction(directoryInfo.Delete, true);
            }
            else
            {
                RepeatAction(info.Delete);
            }
        }

        private static void ValidateFolder(string folder, string folderType)
        {
            try
            {
                if (folder != null && !Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
            }
            catch (Exception e)
            {
                string strMessage = string.Format("The folder '{0}' is not valid.", folderType);
                throw new InvalidDataException(strMessage, e);
            }
        }

        private static string NormalizePath(string path)
        {
            if (path != null)
            {
                path = Path.GetFullPath(Path.Combine(Application.Base, path));
                if (!path.EndsWith("" + Path.DirectorySeparatorChar))
                {
                    path += Path.DirectorySeparatorChar;
                }
            }
            return path;
        }
        #endregion
    }
}