namespace Allag.Core.Tools.Job
{
    public enum ScheduleType
    {
        None,
        BySecond,
        ByMinute,
        Hourly,
        Daily,
        Weekly,
        Monthly,

        FileSystem,

        Custom
    }
}
