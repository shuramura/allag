﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

#endregion

namespace Allag.Core.Tools.Job
{
    [Serializable]
    public class UploadFileItemResult : BaseFileSystemEntryItemResult
    {
        #region Constructors
        public UploadFileItemResult()
        {
            FileNames = new List<string>();
        }
        #endregion

        #region Properties
        public string UploadedName { get; set; }
        public long Size { get; set; }
        public byte[] Hash { get; set; }

        [XmlArray("files")]
        [XmlArrayItem("name")]
        public List<string> FileNames { get; set; }
        #endregion
    }
}