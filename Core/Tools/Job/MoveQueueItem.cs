﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

#endregion

namespace Allag.Core.Tools.Job
{
    public class MoveQueueItem
    {
        #region Variables
        private readonly Collection<QueueItem> _items;
        #endregion

        #region Constructors
        public MoveQueueItem()
        {
            _items = new Collection<QueueItem>();
        }
        #endregion

        #region Properties
        public string Id { get; set; }

        public IEnumerable<QueueItem> Items
        {
            get { return _items; }
        }
        #endregion

        #region Methods
        public QueueItem Add(Guid queueId, string id)
        {
            if (queueId == Guid.Empty)
            {
                throw new ArgumentException("The queue is is not specified.", "queueId");
            }

            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullOrEmptyException("id");
            }

            QueueItem ret = new QueueItem {QueueId = queueId, Id = id};
            _items.Add(ret);
            return ret;
        }
        #endregion
    }
}