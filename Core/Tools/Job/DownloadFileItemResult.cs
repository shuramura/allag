﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

#endregion

namespace Allag.Core.Tools.Job
{
    [Serializable]
    public class DownloadFileItemResult : BaseFileSystemEntryItemResult
    {
        #region Properties
        [XmlArray("files")]
        [XmlArrayItem("name")]
        public List<string> FileNames { get; set; }
        #endregion
    }
}