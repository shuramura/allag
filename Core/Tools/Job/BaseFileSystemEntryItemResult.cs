﻿#region Using directives
using System;
using System.Xml.Serialization;

#endregion

namespace Allag.Core.Tools.Job
{
    [Serializable]
    public class BaseFileSystemEntryItemResult
    {
        #region Properties
        [XmlAttribute]
        public bool IsHandled { get; set; }
        public string Name { get; set; }
        #endregion
    }
}