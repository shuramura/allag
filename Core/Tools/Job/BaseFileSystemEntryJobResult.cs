﻿#region Using directives
using System;
using System.Xml.Serialization;

#endregion

namespace Allag.Core.Tools.Job
{
    [Serializable]
    public class BaseFileSystemEntryJobResult<TItemResult> : BaseItemJobResult<TItemResult>
        where TItemResult : BaseFileSystemEntryItemResult, new()
    {
        #region Properties
        [XmlElement("SourceFolder")]
        public string SourceFolder { get; set; }

        [XmlElement("processingFolder")]
        public string ProcessingFolder { get; set; }

        [XmlElement("fileTemplate")]
        public string FileTemplate { get; set; }

        [XmlElement("folderTemplate")]
        public string FolderTemplate { get; set; }

        [XmlElement("archiveFolder")]
        public string ArchiveFolder { get; set; }

        [XmlElement("errorFolder")]
        public string ErrorFolder { get; set; }
        #endregion
    }
}