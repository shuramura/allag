﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Threading;

#endregion

namespace Allag.Core.Tools.Job
{
    //public class JobInfo : IJobInfo
    //{
    //    #region Variables
    //    private bool _isDisposed;
    //    private readonly List<IJobInfo> _dependents;
    //    #endregion

    //    #region Constructors
    //    public JobInfo(IJob job, IIntervalProvider intervalProvider, IEnumerable<IJobInfo> dependents)
    //    {
    //        _isDisposed = false;
    //        Job = job;
    //        IntervalProvider = intervalProvider;
    //        _dependents = new List<IJobInfo>(dependents);
    //    }

    //    ~JobInfo()
    //    {
    //        Dispose(false);
    //    }
    //    #endregion

    //    #region Implementation of IDisposable
    //    public void Dispose()
    //    {
    //        Dispose(true);
    //        GC.SuppressFinalize(this);
    //    }
    //    #endregion

    //    #region Properties
    //    public string Name
    //    {
    //        get { return Job.Name; }
    //    }

    //    internal IJob Job { get; private set; }
    //    private IIntervalProvider IntervalProvider { get; set; }
    //    #endregion

       
    //    public void Run(IScheduler scheduler)
    //    {
    //        LogBlock.Logger.Info("The job '{0}' is executing.", Job.Name);
    //        Job.Run(scheduler);

    //        LogBlock.Logger.Info("The dependent jobs for the job '{0}' are executing.", Job.Name);
    //        _dependents.ForEach(jobInfo => jobInfo.Run(scheduler));
    //    }

    //    public void Start()
    //    {
    //        if (Job.IsStopped)
    //        {
    //            Job.Start();
    //            if (IntervalProvider != null)
    //            {
    //                IntervalProvider.Start(Job);
    //            }
    //        }
    //    }

    //    public WaitHandle Stop()
    //    {
    //        WaitHandle ret = null;
    //        if (!Job.IsStopped)
    //        {
    //            if (IntervalProvider != null)
    //            {
    //                IntervalProvider.Stop(Job);
    //            }
    //            ret = Job.Stop();
    //        }
    //        return ret;
    //    }

    //    #region Methods
    //    private void Dispose(bool isDisposing)
    //    {
    //        if (!_isDisposed)
    //        {
    //            if (isDisposing)
    //            {
    //                if (IntervalProvider != null)
    //                {
    //                    IntervalProvider.Dispose();
    //                    IntervalProvider = null;
    //                }
    //                Job.Dispose();

    //                _dependents.ForEach(x => x.Dispose());
    //                _dependents.Clear();
    //            }
    //        }
    //        _isDisposed = true;
    //    }
    //    #endregion
    //}
}