﻿#region Using directives
using System;
using System.Xml.Serialization;

#endregion

namespace Allag.Core.Tools.Job
{
    [Serializable]
    public class UploadFileResult<TItemResult> : BaseFileSystemEntryJobResult<TItemResult>
        where TItemResult : UploadFileItemResult, new()
    {
        #region Properties
        [XmlElement("remoteFolder")]
        public string RemoteFolder { get; set; }

        [XmlAttribute("archive")]
        public bool Archive { get; set; }

        [XmlAttribute("archivedExtension")]
        public string ArchivedExtension { get; set; }
        #endregion
    }
}