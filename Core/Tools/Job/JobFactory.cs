#region Using directives
using System;
using System.Collections.Generic;
using Allag.Core.Configuration;
using Allag.Core.Configuration.Job;

#endregion

namespace Allag.Core.Tools.Job
{
    public static class JobFactory
    {
        #region Variables
        private static readonly Scheduler _scheduler;
        #endregion

        #region Constructors
        static JobFactory()
        {
            _scheduler = new Scheduler();

            JobsSection section = CoreCfg.Jobs;
            AddJobs(section, section.Items);

            foreach (GroupElement group in section.Groups)
            {
                AddJobs(section, group.Items);
            }
        }
        #endregion

        #region Properties
        public static IScheduler Scheduler
        {
            get { return _scheduler; }
        }
        #endregion

        #region Static methods
        public static void Start()
        {
            _scheduler.Start();
        }

        public static void Stop(TimeSpan timeout)
        {
            _scheduler.Stop(timeout);
        }

        private static IList<string> GetJobNameList(ElementCollection<DependentJobElement> list)
        {
            List<string> ret = new List<string>(list.Count);
            foreach (DependentJobElement element in list)
            {
                ret.Add(element.Name);
            }
            return ret;
        }

        private static IIntervalProvider GetJobIntervalProvider(JobElement item, JobsSection section)
        {
            IIntervalProvider provider = null;
            switch (item.ScheduleType)
            {
                case ScheduleType.BySecond:
                case ScheduleType.ByMinute:
                case ScheduleType.Hourly:
                    provider = GetIntervalProviderInfo(item, section.SimpleSchedule).GetProvider(item.ScheduleType);
                    break;
                case ScheduleType.Daily:
                    provider = GetIntervalProviderInfo(item, section.Daily).GetProvider(item.ScheduleType);
                    break;
                case ScheduleType.Weekly:
                    provider = GetIntervalProviderInfo(item, section.Weekly).GetProvider(item.ScheduleType);
                    break;
                case ScheduleType.Monthly:
                    provider = GetIntervalProviderInfo(item, section.Monthly).GetProvider(item.ScheduleType);
                    break;
                case ScheduleType.FileSystem:
                    provider = GetIntervalProviderInfo(item, section.FileSystem).GetProvider(item.ScheduleType);
                    break;
                case ScheduleType.Custom:
                    provider = GetIntervalProviderInfo(item, section.Custom).CreateObject() as IIntervalProvider;
                    break;
            }

            return provider;
        }

        private static TIntervalProviderElement GetIntervalProviderInfo<TIntervalProviderElement>(JobElement jobElement, ElementCollection<TIntervalProviderElement> schedules)
            where TIntervalProviderElement : Element<string>, new()

        {
            TIntervalProviderElement ret = null;
            List<string> names = new List<string>(2);
            if (string.IsNullOrEmpty(jobElement.ScheduleName))
            {
                names.Add(jobElement.Name);
            }
            if (!jobElement.ScheduleName.Equals(jobElement.Name, StringComparison.InvariantCultureIgnoreCase))
            {
                names.Add(jobElement.ScheduleName);
            }
            for (int i = 0; i < names.Count && ret == null; i++)
            {
                string strName = names[i];
                foreach (TIntervalProviderElement element in schedules)
                {
                    if (element.Name.Equals(strName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        ret = element;
                        break;
                    }
                }
            }

            return ret ?? new TIntervalProviderElement();
        }

        private static void AddJobs(JobsSection section, IEnumerable<JobElement> items)
        {
            foreach (JobElement item in items)
            {
                IJob job = (IJob)item.CreateObject();

                job.Name = item.Name;

                _scheduler.AddJob(job, GetJobIntervalProvider(item, section), GetJobNameList(item.Dependents));
            }
        }
        #endregion
    }
}