﻿#region Using directives
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Allag.Core.Data.Queues;

#endregion

namespace Allag.Core.Tools.Job
{
    public abstract class MoveQueueMessagesTask<TIQueue, TMessage> : BaseJob<MoveQueueResult>
        where TIQueue : class, IQueue<TMessage>
        where TMessage : class
    {
        #region Variables
        private TIQueue _source;
        private readonly TIQueue[] _destinations;
        #endregion

        #region Constructors
        protected MoveQueueMessagesTask(TIQueue source, params TIQueue[] destinations)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            if (destinations == null)
            {
                throw new ArgumentNullException("destinations");
            }
            _source = source;
            _destinations = destinations.Where(x => x != null).ToArray();

            if (_destinations.Length == 0)
            {
                throw new ArgumentException("There is no any destination queue.", "destinations");
            }

            BatchSize = 1000;
        }
        #endregion

        #region Properties
        public int BatchSize { get; set; }
        public IResource LocalStorage { get; set; }
        #endregion

        #region Override method
        protected override void Initialization(MoveQueueResult result, ExecutionData data)
        {
            using(LogBlock.New(GetType()))
            {
                base.Initialization(result, data);
                result.QueueId = _source.Id;
                LogBlock.Logger.Info("The source queue id is: {0}", result.QueueId);
            }
        }

        protected override void Execute(MoveQueueResult result, ExecutionData data)
        {
            try
            {
                Task.WaitAll(_source.MoveToAsync(_destinations, LocalStorage, (id, queueId, newId) =>
                {
                    result.Get(id).Add(queueId, newId);
                    return result.Items.Count() < BatchSize;
                }, data.CancellationToken));
            }
            catch (QueueException exp)
            {
                data.AddWarning(exp);
                using(LogBlock.New(GetType()))
                {
                    LogBlock.Logger.Warning(exp.Message, exp);
                }
            }
        }

        protected override void Dispose(bool isDisposing)
        {
            if (_source != null)
            {
                _source.Dispose();
                _destinations.ForEach(x => x.Dispose());
                _source = null;
            }
            base.Dispose(isDisposing);
        }
        #endregion
    }

    public class MoveQueueMessagesTask<TMessage> : MoveQueueMessagesTask<IQueue<TMessage>, TMessage>
        where TMessage : class
    {
        #region Constructors
        public MoveQueueMessagesTask(IQueue<TMessage> source, params IQueue<TMessage>[] destinations) : base(source, destinations) {}
        #endregion
    }

    public class MoveQueueMessagesTask : MoveQueueMessagesTask<IQueue, Stream>
    {
        #region Constructors
        public MoveQueueMessagesTask(IQueue source, params IQueue[] destinations) : base(source, destinations) {}
        #endregion
    }
}