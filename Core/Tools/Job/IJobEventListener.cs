﻿namespace Allag.Core.Tools.Job
{
    public interface IJobEventListener
    {
        void Initialization<TResult>(TResult result, ExecutionData data) where TResult : class;
        void Finalizing<TResult>(TResult result, ExecutionData data) where TResult : class;
    }
}