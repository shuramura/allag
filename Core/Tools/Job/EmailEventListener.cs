﻿#region Using directives
using System;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;

#endregion

namespace Allag.Core.Tools.Job
{
    public class EmailEventListener : IJobEventListener
    {
        #region Constructors
        public EmailEventListener(string emailConfiguration)
        {
            if (emailConfiguration == null)
            {
                throw new ArgumentNullException("emailConfiguration");
            }
            Helper = new EmailHelper(emailConfiguration);
            Priority = Priority.All;
        }
        #endregion

        #region Implementation of IJobNotification
        public void Initialization<TResult>(TResult result, ExecutionData data) where TResult : class {}

        public void Finalizing<TResult>(TResult result, ExecutionData data) where TResult : class
        {
            using(LogBlock.New<EmailEventListener>())
            {
                if (result == null)
                {
                    throw new ArgumentNullException("result");
                }

                if (data == null)
                {
                    throw new ArgumentNullException("data");
                }
                ResultContainer<TResult> resultContainer = new ResultContainer<TResult>(result, data);

                string strResult = resultContainer.Serialize();
                data.AddFile("Result.xml", new MemoryStream(Encoding.UTF8.GetBytes(strResult)));
                LogBlock.Logger.Debug("The result container is: {0}", strResult);

                if (Priority != Priority.None)
                {
                    Priority priority = Priority.None;
                    if (data.Exception != null && (Priority & Priority.High) == Priority.High)
                    {
                        priority = Priority.High;
                    }
                    else if (data.Warnings.Any() && (Priority & Priority.Normal) == Priority.Normal)
                    {
                        priority = Priority.Normal;
                    }
                    else if ((Priority & Priority.Low) == Priority.Low)
                    {
                        priority = Priority.Low;
                    }

                    if (priority != Priority.None)
                    {
                        MailPriority mailPriority = MailPriority.Low;
                        switch (priority)
                        {
                            case Priority.Normal:
                                mailPriority = MailPriority.Normal;
                                break;
                            case Priority.High:
                                mailPriority = MailPriority.High;
                                break;
                        }

                        Helper.SendEmail(resultContainer, mailPriority, data.Files.ToArray());
                    }
                }
            }
        }
        #endregion

        #region Properties
        public EmailHelper Helper { get; private set; }
        public Priority Priority { get; set; }
        #endregion
    }
}