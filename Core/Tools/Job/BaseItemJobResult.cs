﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

#endregion

namespace Allag.Core.Tools.Job
{
    [Serializable]
    public class BaseItemJobResult<TItemResult>
        where TItemResult : class, new()
    {
        #region Constructors
        public BaseItemJobResult()
        {
            Items = new List<TItemResult>();
        }
        #endregion

        #region Properties
        [XmlArray("items")]
        [XmlArrayItem("item")]
        public List<TItemResult> Items { get; set; }
        #endregion
    }
}