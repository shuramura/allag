﻿#region Using directives
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

#endregion

namespace Allag.Core.Tools.Job
{
    public class MoveQueueResult
    {
        #region Variables
        private readonly ConcurrentDictionary<string, MoveQueueItem> _items;
        #endregion

        #region Constructors
        public MoveQueueResult()
        {
            _items = new ConcurrentDictionary<string, MoveQueueItem>();
        }
        #endregion

        #region Properties
        public Guid QueueId { get; set; }

        public IEnumerable<MoveQueueItem> Items
        {
            get { return _items.Values; }
        }
        #endregion

        #region Methods
        public MoveQueueItem Get(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullOrEmptyException("id");
            }
            return _items.GetOrAdd(id, x => new MoveQueueItem {Id = x});
        }
        #endregion
    }
}