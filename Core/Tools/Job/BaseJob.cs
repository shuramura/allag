#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

#endregion

namespace Allag.Core.Tools.Job
{
    public abstract class BaseJob<TResult> : IJob
        where TResult : class, new()
    {
        #region Variables
        private bool _isDisposed;
        private string _strName;
        private readonly List<IJobEventListener> _eventListeners;
        #endregion

        #region Constructors
        protected BaseJob()
        {
            _isDisposed = false;
            _strName = string.Empty;

            _eventListeners = new List<IJobEventListener>();
        }

        ~BaseJob()
        {
            Dispose(false);
        }
        #endregion

        #region Implementation of IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation of IJob
        public string Name
        {
            get { return _strName; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                _strName = value;
            }
        }

        public string Description { get; set; }

        object IJob.Run(IScheduler scheduler, CancellationToken cancellationToken)
        {
            using(LogBlock.New<BaseJob<TResult>>())
            {
                CheckDisposed();

                ExecutionData data = new ExecutionData(scheduler, this, cancellationToken);
                TResult result = new TResult();

                try
                {
                    cancellationToken.ThrowIfCancellationRequested();
                    try
                    {
                        try
                        {
                            Initialization(result, data);
                        }
                        finally
                        {
                            InvokeListeners(x => x.Initialization(result, data));
                        }
                        Execute(result, data);
                    }
                    catch (Exception e)
                    {
                        HandleException(result, data, e);
                        LogBlock.Logger.Error("The initialization is failed.", e);
                    }
                    finally
                    {
                        if (cancellationToken.IsCancellationRequested)
                        {
                            data.AddWarning("The job '{0}' is canceled.", _strName);
                        }
                        data.Finish();
                        InvokeListeners(x => x.Finalizing(result, data));
                        Finalizing(result, data);
                    }
                }
                finally
                {
                    foreach (Stream stream in data.Files.Values)
                    {
                        stream.Dispose();
                    }
                }
                return result;
            }
        }
        #endregion

        #region Abstract methods
        protected abstract void Execute(TResult result, ExecutionData data);
        #endregion

        #region Methods
        public void AddListener(IJobEventListener eventListener)
        {
            if (eventListener == null)
            {
                throw new ArgumentNullException("eventListener");
            }

            _eventListeners.Add(eventListener);
        }

        protected virtual void Initialization(TResult result, ExecutionData data) {}
  
        protected virtual void HandleException(TResult result, ExecutionData data, Exception exception)
        {
            if (!data.CancellationToken.IsCancellationRequested)
            {
                data.Exception = exception;    
            }
        }

        protected virtual void Finalizing(TResult result, ExecutionData data) {}

        protected virtual void Dispose(bool isDisposing)
        {
            using(LogBlock.New(GetType()))
            {
                _isDisposed = true;
            }
        }

        private void CheckDisposed()
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException(GetType().FullName);
            }
        }

        private void InvokeListeners(Action<IJobEventListener> action)
        {
            foreach (IJobEventListener listener in _eventListeners)
            {
                try
                {
                    action(listener);
                }
                catch (Exception exp)
                {
                    LogBlock.Logger.Error("The listener is failed.", exp);
                }
            }
        }
        #endregion
    }
}