﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Threading;

#endregion

namespace Allag.Core.Tools.Job
{
    public interface IJobContainer : IDisposable
    {
        string Name { get; }
        bool IsReadyToRun { get; }
        bool IsStopped { get; }

        void Start();
        bool Run();
        WaitHandle Stop();

        void SetNextRun(DateTime lastRun);

        void Init(IJob job, IIntervalProvider intervalProvider, Action<IJobContainer> callback,
            IEnumerable<IJobContainer> preJobs, IEnumerable<IJobContainer> postJobs);
    }

    public interface IJobContainer1 : IDisposable
    {
        void Init(IJob job, IEnumerable<IJobContainer1> preJobs, IEnumerable<IJobContainer1> postJobs, Action<JobExceptionEventArgs> errorHandler = null);

        void Start();
        bool Run();
        WaitHandle Stop();
    }
}