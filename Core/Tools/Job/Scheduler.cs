#region Using directives
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

#endregion

namespace Allag.Core.Tools.Job
{
    public class Scheduler : IDisposable, IScheduler
    {
        #region Variables
        private readonly WindowsIdentity _identity;
        private readonly ConcurrentDictionary<IJob, Tuple<IIntervalProvider, IJob[]>> _jobs;

        private bool _isDisposed;
        private ConcurrentDictionary<string, Task> _inProgressJobs;

        private CancellationTokenSource _jobCancellationTokenSource;
        private CancellationTokenSource _jobAbortTokenSource;
        #endregion

        #region Constructors
        public Scheduler()
        {
            _isDisposed = false;
            _jobs = new ConcurrentDictionary<IJob, Tuple<IIntervalProvider, IJob[]>>();
            _identity = WindowsIdentity.GetCurrent();
        }

        ~Scheduler()
        {
            Dispose(false);
        }
        #endregion

        #region Implementation of IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation of IEnumerable
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion

        #region Implementation of IEnumerator<IJob>
        public IEnumerator<IJob> GetEnumerator()
        {
            return _jobs.Keys.GetEnumerator();
        }
        #endregion

        #region Implementation of IScheduler
        public IJob this[string jobName]
        {
            get { return _jobs.FirstOrDefault(x => string.Compare(x.Key.Name, jobName, StringComparison.OrdinalIgnoreCase) == 0).Key; }
        }

        public int Count
        {
            get { return _jobs.Count; }
        }

        public void Start()
        {
            using(LogBlock.New<Scheduler>())
            {
                CheckDisposed();

                _inProgressJobs = new ConcurrentDictionary<string, Task>();
                _jobCancellationTokenSource = new CancellationTokenSource();
                _jobAbortTokenSource = new CancellationTokenSource();

                _jobs.ForEach(x =>
                {
                    if (x.Value.Item1 != null)
                    {
                        x.Value.Item1.Start(x.Key);
                    }
                });
            }
        }

        public void Stop(TimeSpan timeout)
        {
            using(LogBlock.New<Scheduler>())
            {
                CheckDisposed();
                lock(_jobs)
                {
                    if (_inProgressJobs != null)
                    {
                        _jobs.ForEach(x =>
                        {
                            if (x.Value.Item1 != null)
                            {
                                x.Value.Item1.Stop(x.Key);
                            }
                        });

                        new CancellationTokenSource[] {_jobCancellationTokenSource, _jobAbortTokenSource}.ForEach(x =>
                        {
                            x.Cancel();
                            try
                            {
                                Task.WaitAll(_inProgressJobs.Values.ToArray(), timeout);
                            }
                            catch (AggregateException exp)
                            {
                                if (!(exp.InnerException is TaskCanceledException))
                                {
                                    throw;
                                }
                            }
                        });

                        _inProgressJobs.ForEach(x =>
                        {
                            if (x.Value.IsCompleted)
                            {
                                x.Value.Dispose();
                            }
                            else
                            {
                                LogBlock.Logger.Warning("The job '{0}' is not stopped.", x.Key);
                            }
                        });

                        _inProgressJobs = null;
                        _jobCancellationTokenSource.Dispose();
                        _jobCancellationTokenSource = null;
                        _jobAbortTokenSource.Dispose();
                        _jobAbortTokenSource = null;
                    }
                }
            }
        }

        public void RunDependants(IJob job)
        {
            if (job == null)
            {
                throw new ArgumentNullException("job");
            }
            IJob[] jobs = _jobs[job].Item2;
            if (jobs.Length > 0)
            {
                LogBlock.Logger.Info("The dependent jobs for the job '{0}' are executing.", job.Name);
                ExecuteJob(jobs);
            }
        }
        #endregion

        #region Methods
        public void AddJob(IJob job, IIntervalProvider intervalProvider = null, IList<string> dependentJobs = null)
        {
            using(LogBlock.New<Scheduler>())
            {
                CheckDisposed();

                if (job == null)
                {
                    throw new ArgumentNullException("job");
                }

                if (_jobs.TryAdd(job, new Tuple<IIntervalProvider, IJob[]>(intervalProvider, GetJobList(dependentJobs))) && intervalProvider != null)
                {
                    intervalProvider.Add(job, ExecuteJob);
                }
            }
        }

        private IJob[] GetJobList(IList<string> jobs)
        {
            List<IJob> ret = new List<IJob>();
            for (int i = 0; jobs != null && i < jobs.Count; i++)
            {
                ret.Add(_jobs.Keys.First(x => string.Compare(x.Name, jobs[i], StringComparison.OrdinalIgnoreCase) == 0));
            }

            return ret.ToArray();
        }

        private void Dispose(bool isDisposing)
        {
            if (!_isDisposed)
            {
                if (isDisposing)
                {
                    Stop(TimeSpan.Zero);
                    _jobs.ForEach(pair =>
                    {
                        pair.Key.Dispose();
                        if (pair.Value.Item1 != null)
                        {
                            pair.Value.Item1.Dispose();
                        }
                    });

                    _jobs.Clear();
                    _identity.Dispose();
                }
            }
            _isDisposed = true;
        }

        private void CheckDisposed()
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException(typeof(Scheduler).FullName);
            }
        }

        private void ExecuteJob(params IJob[] jobs)
        {
            if (!_isDisposed && _inProgressJobs != null)
            {
                lock(_jobs)
                {
                    if (_inProgressJobs != null)
                    {
                        jobs.ForEach(x =>
                        {
                            Func<object> func = () => InternalExecuteJob(x);
                            _inProgressJobs.AddOrUpdate(x.Name, name => Task.Run(func, _jobAbortTokenSource.Token),
                                (name, current) =>
                                {
                                    if (current.IsCompleted)
                                    {
                                        current.Dispose();
                                        current = Task.Run(func, _jobAbortTokenSource.Token);
                                    }
                                    else if (current.Status == TaskStatus.Running)
                                    {
                                        current = current.ContinueWith(task => func(), _jobAbortTokenSource.Token, TaskContinuationOptions.ExecuteSynchronously | TaskContinuationOptions.NotOnCanceled, TaskScheduler.Current);
                                    }
                                    return current;
                                });
                        });
                    }
                }
            }
        }

        private object InternalExecuteJob(IJob job)
        {
            using(LogBlock.New<Scheduler>(new {job.Name}))
            {
                object ret = null;
                if (!_isDisposed)
                {
                    using(_identity.Impersonate())
                    {
                        try
                        {
                            LogBlock.Logger.Info("The job '{0}' is started.", job.Name);
                            ret = job.Run(this, _jobCancellationTokenSource.Token);

                            RunDependants(job);

                            LogBlock.Logger.Info("The job '{0}' was executed.", job.Name);
                        }
                        catch (Exception exp)
                        {
                            LogBlock.Logger.FatalError(exp);
                        }
                    }
                }
                return ret;
            }
        }
        #endregion
    }
}