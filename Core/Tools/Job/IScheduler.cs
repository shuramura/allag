using System;
using System.Collections.Generic;

namespace Allag.Core.Tools.Job
{
    public interface IScheduler : IEnumerable<IJob>
    {
        IJob this[string jobName] { get; }
        int Count { get; }

        void Start();
        void Stop(TimeSpan timeout);
        void RunDependants(IJob job);
    }
}
