﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

#endregion

namespace Allag.Core.Tools.Job
{
    [Serializable]
    public class DownloadFileResult<TItemResult> : BaseFileSystemEntryJobResult<TItemResult>
        where TItemResult : DownloadFileItemResult, new()
    {
        #region Properties
        [XmlElement("remoteFolder")]
        public string RemoteFolder { get; set; }

        [XmlAttribute("unarchive")]
        public bool Unarchive { get; set; }

        [XmlAttribute("allowDuplication")]
        public bool AllowDuplication { get; set; }

        [XmlArray("downloaded")]
        [XmlArrayItem("file")]
        public List<string> Files { get; set; }
        #endregion
    }
}