﻿#region Using directives
using System;

#endregion

namespace Allag.Core.Tools.Job
{
    public class QueueItem
    {
        #region Properties
        public string Id { get; set; }
        public Guid QueueId { get; set; }
        #endregion
    }
}