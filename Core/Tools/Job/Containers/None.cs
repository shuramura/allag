﻿#region Using directives
using System;
using System.Security.Principal;
using System.Threading;

#endregion

namespace Allag.Core.Tools.Job.Containers
{
    public class None : BaseJobContainer
    {
        #region Variables
        private static readonly int _nInternalTimerInterval;
        private DateTime _dtNextRun;
        #endregion

        #region Constructors
        static None()
        {
            _nInternalTimerInterval = Int32.MaxValue;
        }

        public None()
            : this(ScheduleType.None, 0, null)
        {
        }

        protected None(ScheduleType scheduleType, double every, WindowsIdentity identity) :
            base(scheduleType, every, identity)
        {
            _dtNextRun = DateTime.MinValue;
            Timer = null;
        }
        #endregion

        #region Properties
        protected Timer Timer { get; set; }
        #endregion

        #region Abstract methods
        
        #endregion

        #region Override methods
        public override void Start()
        {
            bool isStopped = Job.IsStopped;

            base.Start();

            if (!IsDisposed && isStopped && Timer != null)
            {
                if (Job.RunImmediately)
                {
                    TaskExecuting();
                }

                SetNextRun(DateTime.Now);
            }
        }

        public override WaitHandle Stop()
        {
            if (!IsDisposed && Timer != null)
            {
                Timer.Change(Timeout.Infinite, 0);
            }
            return base.Stop();
        }

        protected override void Dispose(bool isDisposing)
        {
            if (!IsDisposed && isDisposing && Timer != null)
            {
                Timer.Dispose();
                Timer = null;
            }
            base.Dispose(isDisposing);
        }
        #endregion

        #region Methods
        public virtual DateTime GetNextRun(DateTime lastRun)
        {
            throw new NotSupportedException();
        }

        private void SetNextRun(DateTime lastRun)
        {
            _dtNextRun = GetNextRun(lastRun);
            LogBlock.Logger.InfoFormat("The job '{0}' will be run at {1}, after {2} seconds.", Job.Name, _dtNextRun, (_dtNextRun - DateTime.Now).TotalSeconds);
            Timer.Change(0, 0);   
        }

        protected void InternalTimerHandler(object state)
        {
            if (Timer != null)
            {
                double dInterval = (_dtNextRun - DateTime.Now).TotalMilliseconds;
                if (dInterval <= 0)
                {
                    TaskExecuting();
                }
                else
                {
                    Timer.Change(dInterval <= _nInternalTimerInterval ? (long)dInterval : _nInternalTimerInterval, 0);
                }
            }
        }

        protected void TaskExecuting()
        {
            if (!IsDisposed)
            {
                using (LogBlock.New("TaskExecuting", GetType()))
                {
                    WindowsImpersonationContext context = null;
                    if (Identity != null)
                    {
                        context = Identity.Impersonate();
                    }

                    try
                    {
                        DateTime lastRun = DateTime.Now;
                        if (Run())
                        {
                            LogBlock.Logger.InfoFormat("The job '{0}' executed.", Job.Name);
                        }
                        else
                        {
                            LogBlock.Logger.InfoFormat("The job '{0}' was stopped.", Job.Name);
                            return;
                        }

                        if (!Job.IsStopped)
                        {
                            SetNextRun(lastRun);
                        }
                        else
                        {
                            LogBlock.Logger.InfoFormat("The job '{0}' is already running.", Job.Name);
                        }
                    }
                    catch (Exception exp)
                    {
                        LogBlock.Logger.Fatal(exp);
                        if (ErrorHandler != null)
                        {
                            ErrorHandler(new JobExceptionEventArgs(Job.Name, exp));
                        }
                    }
                    finally
                    {
                        if (context != null)
                        {
                            context.Dispose();
                        }
                    }
                }
            }
        }
        #endregion
    }
}