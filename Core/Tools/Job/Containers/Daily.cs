﻿#region Using directives
using System;
using System.Security.Principal;

#endregion

namespace Allag.Core.Tools.Job.Containers
{
    public class Daily : None
    {
        #region Variables
        private readonly DayOfWeek _dayOfWeek;
        private readonly bool _useDayOfWeek;
        #endregion

        #region Constructors
        public Daily(DayOfWeek dayOfWeek, double every, bool useDayOfWeek, WindowsIdentity identity)
            : base(ScheduleType.Daily, every, identity)
        {
            _dayOfWeek = dayOfWeek;
            _useDayOfWeek = useDayOfWeek;
        }
        #endregion

        #region Override methods
        public override DateTime GetNextRun(DateTime lastRun)
        {
            bool isFirstTime = false;
            if (lastRun == DateTime.MinValue)
            {
                lastRun = DateTime.Now;
                isFirstTime = true;
            }

            DateTime start = Job.StartTime;

            DateTime ret = lastRun;
            if (_useDayOfWeek)
            {
                ret = new DateTime(ret.Year, ret.Month, ret.Day, start.Hour, start.Minute, start.Second);
                if (ret < lastRun)
                {
                    ret = ret.AddDays(1);
                }
                DayOfWeek dayOfWeek;
                while ((_dayOfWeek & (dayOfWeek = GetDayOfWeek(ret.DayOfWeek))) != dayOfWeek)
                {
                    ret = ret.AddDays(1);
                }
            }
            else
            {
                double dEvery = Every;
                if (isFirstTime)
                {
                    ret = new DateTime(ret.Year, ret.Month, ret.Day, start.Hour, start.Minute, start.Second);
                    dEvery = ret < lastRun ? 1 : 0;
                }
                ret = ret.AddDays(dEvery);
            }

            return ret;
        }
        #endregion

        #region Methods
        private static DayOfWeek GetDayOfWeek(System.DayOfWeek dayOfWeek)
        {
            DayOfWeek ret;
            switch (dayOfWeek)
            {
                case System.DayOfWeek.Monday:
                    ret = DayOfWeek.Monday;
                    break;
                case System.DayOfWeek.Tuesday:
                    ret = DayOfWeek.Tuesday;
                    break;
                case System.DayOfWeek.Wednesday:
                    ret = DayOfWeek.Wednesday;
                    break;
                case System.DayOfWeek.Thursday:
                    ret = DayOfWeek.Thursday;
                    break;
                case System.DayOfWeek.Friday:
                    ret = DayOfWeek.Friday;
                    break;
                case System.DayOfWeek.Saturday:
                    ret = DayOfWeek.Saturday;
                    break;
                default:
                    ret = DayOfWeek.Sunday;
                    break;
            }
            return ret;
        }
        #endregion
    }
}