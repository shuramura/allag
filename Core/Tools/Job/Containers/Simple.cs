﻿#region Using directives
using System;
using System.Security.Principal;

#endregion

namespace Allag.Core.Tools.Job.Containers
{
    public class Simple : None
    {
        #region Constructors
        public Simple(ScheduleType scheduleType, double every, WindowsIdentity identity = null) : 
            base(scheduleType, every, identity)
        {
            if (scheduleType != ScheduleType.BySecond &&
                   scheduleType != ScheduleType.ByMinute && scheduleType != ScheduleType.Hourly)
            {
                throw new ArgumentException("The parameter can not be equal to " + scheduleType, "scheduleType");
            }
        }
        #endregion

        #region Override methods
        public override DateTime GetNextRun(DateTime lastRun)
        {
            DateTime start = Job.StartTime;
            DateTime stop = Job.StopTime;

            DateTime ret = lastRun;
           
            for (int i = 0; i < 2; i++)
            {
                if (i == 0 && ret == DateTime.MinValue)
                {
                    ret = DateTime.Now;
                }
                else
                {
                    switch (ScheduleType)
                    {
                        case ScheduleType.BySecond:
                            ret = ret.AddSeconds(Every);
                            break;
                        case ScheduleType.ByMinute:
                            ret = ret.AddMinutes(Every);
                            break;
                        case ScheduleType.Hourly:
                            ret = ret.AddHours(Every);
                            break;
                    }
                }

                if (stop.TimeOfDay < ret.TimeOfDay || start.TimeOfDay > ret.TimeOfDay)
                {
                    if (i == 1)
                    {
                        ret = DateTime.MaxValue;
                        break;
                    }
                    if (stop != DateTime.MinValue && stop.TimeOfDay < ret.TimeOfDay)
                    {
                        ret = ret.AddDays(1).Date;
                    }
                    if (start != DateTime.MinValue && start.TimeOfDay > ret.TimeOfDay)
                    {
                        ret = new DateTime(ret.Year, ret.Month, ret.Day, start.Hour, start.Minute, start.Second, start.Millisecond);
                    }
                }
                else
                {
                    break;
                }
            }
            return ret;
        }
        #endregion
    }
}