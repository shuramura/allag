﻿#region Using directives
using System;
using System.Security.Principal;

#endregion

namespace Allag.Core.Tools.Job.Containers
{
    public class Monthly : None
    {
        #region Variables
        private readonly int _day;
        private readonly NumberOfWeek _numberOfWeek;
        private readonly System.DayOfWeek _dayOfWeek;
        private readonly Month _months;
        private readonly bool _useMonths;
        #endregion

        #region Constructors
        public Monthly(int day, NumberOfWeek numberOfWeek, System.DayOfWeek dayOfWeek, Month months, double every, bool useMonths, WindowsIdentity identity)
            : base(ScheduleType.Monthly, every, identity)
        {
            if (day < 0 || day > 31)
            {
                throw new ArgumentOutOfRangeException("day", day, "The parameter should be greater than -1 and less than 32.");
            }

            if (every > Int32.MaxValue || every < Int32.MinValue)
            {
                throw new ArgumentException("The parameter should be integer", "every");
            }

            _day = day;
            _numberOfWeek = numberOfWeek;
            _dayOfWeek = dayOfWeek;
            _months = months;
            _useMonths = useMonths;
        }
        #endregion

        #region Override methods
        public override DateTime GetNextRun(DateTime lastRun)
        {
            bool isFirstTime = false;
            if (lastRun == DateTime.MinValue)
            {
                lastRun = DateTime.Now;
                isFirstTime = true;
            }

            DateTime start = Job.StartTime;

            DateTime ret = lastRun;
            while (ret.Year < 9999)
            {
                ret = CalculateMonth(ret, lastRun, isFirstTime);

                if (_day > 0)
                {
                    if (_day <= DateTime.DaysInMonth(ret.Year, ret.Month))
                    {
                        ret = new DateTime(ret.Year, ret.Month, _day, start.Hour, start.Minute, start.Second);
                        if (ret > lastRun)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    ret = new DateTime(ret.Year, ret.Month, 1, start.Hour, start.Minute, start.Second);
                    int nMonth = ret.Month;
                    int nNumberOfWeek = 0;
                    while (nMonth == ret.Month)
                    {
                        while (_dayOfWeek != ret.DayOfWeek)
                        {
                            ret = AddDay(ret, ref nNumberOfWeek);
                        }
                        if (ret > lastRun &&
                            ((_numberOfWeek == NumberOfWeek.First && nNumberOfWeek == 0) ||
                             (_numberOfWeek == NumberOfWeek.Second && nNumberOfWeek == 1) ||
                             (_numberOfWeek == NumberOfWeek.Third && nNumberOfWeek == 2) ||
                             (_numberOfWeek == NumberOfWeek.Fourth && nNumberOfWeek == 3) ||
                             (_numberOfWeek == NumberOfWeek.Last && nNumberOfWeek > 3)))
                        {
                            break;
                        }

                        ret = AddDay(ret, ref nNumberOfWeek);
                    }
                    if (nMonth == ret.Month)
                    {
                        break;
                    }
                }
            }

            return ret;
        }
        #endregion

        #region Methods
        private DateTime CalculateMonth(DateTime dateTime, DateTime lastRun, bool isFirstTime)
        {
            if (_useMonths)
            {
                if (dateTime.Day <= _day)
                {
                    dateTime = dateTime.AddMonths(1);
                }
                Month month;
                while ((_months & (month = GetMonth(dateTime.Month))) != month)
                {
                    dateTime = dateTime.AddMonths(1);
                }
            }
            else
            {
                double dEvery = Every;
                if (isFirstTime)
                {
                    dEvery = dateTime < lastRun ? 1 : 0;
                }
                dateTime = dateTime.AddMonths((int)dEvery);
            }
            return dateTime;
        }
        #endregion

        #region Static methods
        private static DateTime AddDay(DateTime dateTime, ref int numberOfWeek)
        {
            if (dateTime.DayOfWeek == System.DayOfWeek.Sunday)
            {
                numberOfWeek++;
            }
            return dateTime.AddDays(1);
        }

        private static Month GetMonth(int month)
        {
            Month ret;
            switch (month)
            {
                case 1:
                    ret = Month.January;
                    break;
                case 2:
                    ret = Month.February;
                    break;
                case 3:
                    ret = Month.March;
                    break;
                case 4:
                    ret = Month.April;
                    break;
                case 5:
                    ret = Month.May;
                    break;
                case 6:
                    ret = Month.June;
                    break;
                case 7:
                    ret = Month.July;
                    break;
                case 8:
                    ret = Month.August;
                    break;
                case 9:
                    ret = Month.September;
                    break;
                case 10:
                    ret = Month.October;
                    break;
                case 11:
                    ret = Month.November;
                    break;
                default:
                    ret = Month.December;
                    break;
            }
            return ret;
        }
        #endregion
    }
}