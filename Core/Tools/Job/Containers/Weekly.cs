﻿#region Using directives
using System;
using System.Security.Principal;

#endregion

namespace Allag.Core.Tools.Job.Containers
{
    public class Weekly : None
    {
        #region Variables
        private readonly System.DayOfWeek _dayOfWeek;
        #endregion

        #region Constructors
        public Weekly(System.DayOfWeek dayOfWeek, double every, WindowsIdentity identity)
            : base(ScheduleType.Weekly, every, identity)
        {
            _dayOfWeek = dayOfWeek;
        }
        #endregion

        #region override methods
        public override DateTime GetNextRun(DateTime lastRun)
        {
            bool isFirstTime = false;
            if (lastRun == DateTime.MinValue)
            {
                lastRun = DateTime.Now;
                isFirstTime = true;
            }

            DateTime start = Job.StartTime;
            DateTime ret = lastRun;
            ret = new DateTime(ret.Year, ret.Month, ret.Day, start.Hour, start.Minute, start.Second);
            if (ret < lastRun)
            {
                ret = ret.AddDays(1);
            }

            while (_dayOfWeek != ret.DayOfWeek)
            {
                ret = ret.AddDays(1);
            }

            if (!isFirstTime)
            {
                ret = ret.AddDays(7 * Every);
            }

            return ret;
        }
        #endregion
    }
}