﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;

#endregion

namespace Allag.Core.Tools.Job.Containers
{
    public class BaseJobContainer : IJobContainer1
    {
        #region Variables
        private List<IJobContainer1> _preJobs;
        private List<IJobContainer1> _postJobs;
        #endregion

        #region Constructors
        protected BaseJobContainer(ScheduleType scheduleType, double every, WindowsIdentity identity)
        {
            using (LogBlock.New<BaseJobContainer>("BaseJobContainer"))
            {
                if (every <= 0)
                {
                    throw new ArgumentException("The parameter should be greater then zero.", "every");
                }


                _preJobs = null;
                _postJobs = null;

                Identity = identity;
                IsDisposed = false;

                ScheduleType = scheduleType;
                Every = every;
            }
        }

        ~BaseJobContainer()
        {
            Dispose(false);
        }
        #endregion

        #region Implementation of IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation of IJobContainer1
        public void Init(IJob job, IEnumerable<IJobContainer1> preJobs, IEnumerable<IJobContainer1> postJobs, Action<JobExceptionEventArgs> errorHandler = null)
        {
            if (job == null)
            {
                throw new ArgumentNullException("job");
            }

            if (preJobs == null)
            {
                throw new ArgumentNullException("preJobs");
            }

            if (postJobs == null)
            {
                throw new ArgumentNullException("postJobs");
            }

            if (Job != null)
            {
                throw new InvalidOperationException("The container has already been initialized.");
            }

            ErrorHandler = errorHandler;

            Job = job;
            _preJobs = new List<IJobContainer1>(preJobs);
            _postJobs = new List<IJobContainer1>(postJobs);
        }

        public virtual void Start()
        {
            if (!IsDisposed && Job.IsStopped)
            {
                Job.Start();
            }
        }

        public bool Run()
        {
            LogBlock.Logger.InfoFormat("The pre jobs for the job '{0}' are executing.", Job.Name);
            _preJobs.ForEach(jobInfo => jobInfo.Run());

            LogBlock.Logger.InfoFormat("The job '{0}' is executing.", Job.Name);
            bool ret = Job.Run();

            LogBlock.Logger.InfoFormat("The post jobs for the job '{0}' are executing.", Job.Name);
            _postJobs.ForEach(jobInfo => jobInfo.Run());

            return ret;
        }

        public virtual WaitHandle Stop()
        {
            WaitHandle ret = null;
            if (!IsDisposed)
            {
                ret = Job.IsStopped ? null : Job.Stop();
            }

            return ret;
        }
        #endregion

        #region Properties
        public double Every { get; private set; }
        public bool IsDisposed { get; private set; }
        public ScheduleType ScheduleType { get; private set; }

        protected IJob Job { get; private set; }
        protected WindowsIdentity Identity { get; private set; }
        protected Action<JobExceptionEventArgs> ErrorHandler { get; private set; }
        #endregion

        #region Methods
        protected virtual void Dispose(bool isDisposing)
        {
            if (!IsDisposed)
            {
                if (isDisposing)
                {
                    WaitHandle handle = Stop();
                    if (handle != null)
                    {
                        handle.WaitOne(10000);
                    }
                    Job.Dispose();
                    Job = null;
                }
                IsDisposed = true;
            }
        }
        #endregion
    }
}