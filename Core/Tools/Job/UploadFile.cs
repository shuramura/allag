﻿#region Using directives
using System;
using System.IO;
using System.Security.Cryptography;
using Allag.Core.Net;
using Allag.Core.Tools.Archives;

#endregion

namespace Allag.Core.Tools.Job
{
    public class UploadFile<TResult, TItemResult> : BaseFileSystemEntryJob<TResult, TItemResult>
        where TResult : UploadFileResult<TItemResult>, new()
        where TItemResult : UploadFileItemResult, new()

    {
        #region Variables
        private IArchiveFile _archiveFile;
        #endregion

        #region Constructors
        public UploadFile(string fileClientConfig)
        {
            RemoteFolder = string.Empty;
            Archive = true;
            ArchivedExtension = "zip";

            FileManager = new FileManager(fileClientConfig);
        }
        #endregion

        #region Properties
        public FileManager FileManager { get; private set; }

        public string RemoteFolder { get; set; }

        public bool Archive { get; set; }
        public string ArchivedExtension { get; set; }

        public IArchiveFile ArchiveFile
        {
            get { return _archiveFile ?? (_archiveFile = new ZipFile()); }
            set { _archiveFile = value; }
        }

        public string Md5ImplementationName { get; set; }
        #endregion

        #region Override methods
        protected override void Initialization(TResult result, ExecutionData data)
        {
            using(LogBlock.New<DownloadFile>())
            {
                result.RemoteFolder = RemoteFolder;
                result.Archive = Archive;
                result.ArchivedExtension = ArchivedExtension;

                base.Initialization(result, data);

                if (RemoteFolder == null)
                {
                    throw new InvalidDataException("The remote folder is not specified.");
                }
            }
        }

        protected override bool ProcessFile(TResult result, FileInfo item, TItemResult itemResult, ExecutionData data)
        {
            if (Archive)
            {
                string strArchiveFile = Path.ChangeExtension(item.FullName, ArchivedExtension);
                using(IArchiveFile archiveFile = ArchiveFile.Open(strArchiveFile, false))
                {
                    itemResult.FileNames.Add(item.Name);
                    archiveFile.AddFile(item.Name, item.FullName);
                }
                item = new FileInfo(strArchiveFile);
            }
            UploadFileHandler(item, itemResult, fileInfo =>
            {
                if (Archive)
                {
                    RepeatAction(fileInfo.Delete);
                }
            });
            
            return base.ProcessFile(result, item, itemResult, data);
        }

        protected override bool ProcessFolder(TResult result, DirectoryInfo item, TItemResult itemResult, ExecutionData data)
        {
            if (Archive)
            {
                string strArchiveFile = Path.ChangeExtension(item.FullName, ArchivedExtension);
                string strArchiveFolder = Path.GetDirectoryName(item.FullName);
                using(IArchiveFile archiveFile = ArchiveFile.Open(strArchiveFile, false))
                {
                    foreach (FileInfo fileInfo in item.GetFiles("*", SearchOption.AllDirectories))
                    {
                        string strFileName = fileInfo.FullName.Substring(strArchiveFolder.Length);
                        itemResult.FileNames.Add(strFileName);
                        archiveFile.AddFile(strFileName, fileInfo.FullName);
                    }
                }
                UploadFileHandler(new FileInfo(strArchiveFile), itemResult, fileInfo => fileInfo.Delete());
            }
            else
            {
                throw new InvalidDataException(string.Format("The folder '{0}' cannot be uploaded. Please enable archive mode.", item.Name));
            }

            return base.ProcessFolder(result, item, itemResult, data);
        }

        private void UploadFileHandler(FileInfo fileInfo, TItemResult itemResult, Action<FileInfo> finalAction)
        {
            try
            {
                using(Stream stream = fileInfo.OpenRead())
                {
                    FileManager.Upload(stream, RemoteFolder, fileInfo.Name);
                }
                itemResult.UploadedName = fileInfo.Name;
                itemResult.Size = fileInfo.Length;
                if (Md5ImplementationName != null)
                {
                    using(Stream stream = fileInfo.OpenRead())
                    {
                        using(MD5 md5 = Md5ImplementationName.Length > 0 ? MD5.Create(Md5ImplementationName) : MD5.Create())
                        {
                            itemResult.Hash = md5.ComputeHash(stream);
                        }
                    }
                }
            }
            finally
            {
                if (finalAction != null)
                {
                    finalAction(fileInfo);
                }
            }
        }
        #endregion
    }

    public class UploadFile : UploadFile<UploadFileResult<UploadFileItemResult>, UploadFileItemResult> 
    {
        #region Constructors
        public UploadFile(string fileClientConfig) : base(fileClientConfig) {}
        #endregion

    }
}