﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

#endregion

namespace Allag.Core.Tools.Job
{
    [Serializable]
    [XmlRoot("root")]
    public class ResultContainer<TResult> where TResult : class
    {
        #region Constructors
        public ResultContainer()
        {
            Warnings = new Warning[0];
            Errors = new Error[0];
        }

        public ResultContainer(TResult result, ExecutionData data)
        {
            if (result == null)
            {
                throw new ArgumentNullException("result");
            }

            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            Name = data.Job.Name;
            Description = data.Job.Description;
            StartTime = data.StartTime;
            EndTime = data.EndTime;
            Result = result;

            Warnings = data.Warnings.Select(x => new Warning{Message = x.Item1, Errors = GetErrors(x.Item2)}).ToArray();
            Errors = GetErrors(data.Exception);
        }
        #endregion

        #region Properties
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("description")]
        public string Description { get; set; }

        [XmlAttribute("start")]
        public DateTime StartTime { get; set; }

        [XmlAttribute("end")]
        public DateTime EndTime { get; set; }

        [XmlElement("result")]
        public TResult Result { get; set; }

        [XmlArray("warnings")]
        [XmlArrayItem("item")]
        public Warning[] Warnings { get; set; }

        [XmlArray("errors")]
        [XmlArrayItem("item")]
        public Error[] Errors { get; set; }
        #endregion

        #region Methods
        private Error[] GetErrors(Exception exception)
        {
            List<Error> errors = new List<Error>();
            while (exception != null)
            {
                errors.Add(new Error { Message = exception.Message, StackTrace = exception.StackTrace });
                exception = exception.InnerException;
            }
            return errors.ToArray();
        }
        #endregion


        #region Classes
        [Serializable]
        public class Error
        {
            #region Properties
            [XmlAttribute("message")]
            public string Message { get; set; }
            [XmlElement("stackTrace")]
            public string StackTrace { get; set; }
            #endregion
        }

        [Serializable]
        public class Warning
        {
            #region Properties
            [XmlAttribute("message")]
            public string Message { get; set; }
            [XmlArray("errors")]
            [XmlArrayItem("item")]
            public Error[] Errors { get; set; }
            #endregion
        }
        #endregion
    }
}