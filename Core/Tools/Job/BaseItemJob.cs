﻿#region Using directives
using System;
using System.Collections.Generic;

#endregion

namespace Allag.Core.Tools.Job
{
    public abstract class BaseItemJob<TResult, TItem, TItemResult> : BaseJob<TResult>
        where TResult : BaseItemJobResult<TItemResult>, new()
        where TItemResult : class, new()
    {
        #region Implementation of BaseJob<TResult>
        protected override sealed void Execute(TResult result, ExecutionData data)
        {
            using(LogBlock.New(GetType()))
            {
                List<TItemResult> items = new List<TItemResult>();
                foreach (TItem item in GetItems(result, data))
                {
                    data.CancellationToken.ThrowIfCancellationRequested();
                    TItemResult itemResult = new TItemResult();
                    try
                    {
                        Initialization(result, itemResult, data);
                        Execute(result, item, itemResult, data);
                        items.Add(itemResult);
                        if (RunDependatsPerItem)
                        {
                            data.Scheduler.RunDependants(this);
                        }
                    }
                    catch (Exception exp)
                    {
                        LogBlock.Logger.Error("The task is failed.", exp);
                        HandleItemException(result, item, itemResult, data, exp);
                    }
                    finally
                    {
                        Finalizing(result, itemResult, data);
                    }
                }

                result.Items = items;
            }
        }
        #endregion

        #region Abstract properties
        protected abstract IEnumerable<TItem> GetItems(TResult result, ExecutionData data);
        #endregion

        #region Properties
        public bool RunDependatsPerItem { get; set; }
        #endregion

        #region Abstract methods
        protected abstract void Execute(TResult result, TItem item, TItemResult itemResult, ExecutionData data);
        #endregion

        #region Methods
        protected virtual void Initialization(TResult result, TItemResult item, ExecutionData data) { }
        protected virtual void Finalizing(TResult result, TItemResult item, ExecutionData data) { }

        protected virtual void HandleItemException(TResult result, TItem item, TItemResult itemResult, ExecutionData data, Exception exception)
        {
            for (int i = 0; exception != null; exception = exception != null ? exception.InnerException : null)
            {
                data.AddWarning("The exception {0} is: {1}.", i, exception.Message);
                exception = exception.InnerException;
            }
        }
        #endregion

    }
}