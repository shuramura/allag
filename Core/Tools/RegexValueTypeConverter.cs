﻿#region Using directives
using System.ComponentModel;
using System.Globalization;
using System.Security.Permissions;

#endregion

namespace Allag.Core.Tools
{
    [HostProtection(SecurityAction.LinkDemand, SharedState = true)]
    public class RegexValueTypeConverter : BaseTypeConverter<RegexValue>
    {
        #region Implementation of BaseTypeConverter<RegexValue>
        protected override RegexValue ConvertFromString(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            return value != null ? value.ToString() : null;
        }

        protected override object ConvertToString(ITypeDescriptorContext context, CultureInfo culture, RegexValue value)
        {
            return (string)value;
        }
        #endregion
    }
}