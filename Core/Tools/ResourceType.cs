﻿namespace Allag.Core.Tools
{
    public enum ResourceType
    {
        Text,
        Xslt,
        Binary
    }
}