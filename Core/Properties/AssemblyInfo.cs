﻿#region Using directives
using System.Runtime.InteropServices;
using System.Reflection;
#endregion

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attrSibute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("Allag Core")]
[assembly: AssemblyDescription("Core library")]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("50A1607B-58D1-46B9-9322-5F55F35AFF00")]