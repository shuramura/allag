#region Using directives
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Linq;
using System.Runtime.CompilerServices;
using Allag.Core.Reflection;
using Allag.Core.Tools;

#endregion

namespace Allag.Core
{
    public sealed class LogBlock
    {
        #region Variables
        private static readonly ConcurrentDictionary<Guid, LogBlock> _logBlocks;
        private static readonly Context<LogBlock> _context;
        private static readonly Func<object, string> _arrHandler;
        private static readonly ILogger _rootLogger;
        private readonly ILogger _logger;
        #endregion

        #region Constructors
        static LogBlock()
        {
            _logBlocks = new ConcurrentDictionary<Guid, LogBlock>();
            _context = new Context<LogBlock>(Guid.NewGuid(), id => null, new ContextStorage {IsLogicalContext = false});

            _arrHandler = delegate(object arg)
            {
                string ret;
                if (arg == null)
                {
                    ret = "<null>";
                }
                else
                {
                    IEnumerable enumerable = arg as IEnumerable;
                    if (enumerable != null && !(arg is string))
                    {
                        ret = (enumerable.Cast<object>()).Join(", ", "{", "}", _arrHandler);
                    }
                    else
                    {
                        ret = arg.ToString();
                    }
                }

                return ret;
            };

            if (Application.Root != null)
            {
                _rootLogger = ObjectFactory.Get<ILogger>(Application.Root);
                _rootLogger?.Info("==== Application version: {0} ==============================================================================", Application.ProductVersion);
            }
        }

        private LogBlock(ILogger logger)
        {
            _logger = logger;
        }
        #endregion

        #region Properties
        public static ILogger Logger
        {
            get
            {
                if (_context.Value == null || _context.Value._logger == null)
                {
                    throw new InvalidOperationException("The LogBlock have not been opened.");
                }
                return _context.Value._logger;
            }
        }
        #endregion

        #region Static methods
        public static ILogger New<TObject>(TObject owner, object @params = null, [CallerMemberName] string blockName = "")
        {
            return New<TObject>(@params, blockName);
        }

        public static ILogger New<TObject>(object @params = null, [CallerMemberName] string blockName = "")
        {
            return InternalNew(typeof(TObject), @params, blockName);
        }

        public static ILogger New(Type type, object @params = null, [CallerMemberName] string blockName = "")
        {
            return InternalNew(type, @params, blockName);
        }

        public static TResult Action<TObject, TResult>(TObject owner, Func<TResult> actionHandler, object @params = null, [CallerMemberName] string blockName = "")
        {
            return Action<TObject, TResult>(actionHandler, @params, blockName);
        }

        public static TResult Action<TObject, TResult>(Func<TResult> actionHandler, object @params = null, [CallerMemberName] string blockName = "")
        {
            return Action(typeof(TObject), actionHandler, @params);
        }

        public static TResult Action<TResult>(Type type, Func<TResult> actionHandler, object @params = null, [CallerMemberName] string blockName = "")
        {
            return InternalAction(type, actionHandler, @params, blockName);
        }

        private static TResult InternalAction<TResult>(Type type, Func<TResult> actionHandler, object @params, string blockName = "")
        {
            if (actionHandler == null)
            {
                throw new ArgumentNullException("actionHandler");
            }

            using(InternalNew(type, @params, blockName))
            {
                TResult ret = actionHandler();
                Logger.Debug("{0} == {1}", blockName, ret);
                return ret;
            }
        }

        private static ILogger InternalNew(Type type, object @params, string blockName = "")
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }
            Guid id = Guid.NewGuid();
            LogBlock previousBlock = _context.Value;
            Logger logger = new Logger(_rootLogger?.GetLogger(type), 
                x => 
                {
                    LogBlock local;
                    _context.Value = previousBlock;
                    _logBlocks.TryRemove(id, out local);
                    x.Debug("{0} ==>", blockName);
                });
            LogBlock logBlock = new LogBlock(logger);
            _context.Value = logBlock;
            logger.Debug("{0} <== {1}", blockName, @params);
            _logBlocks.TryAdd(id, logBlock);
            return logger;
        }
        #endregion
    }
}