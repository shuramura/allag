﻿namespace Allag.Core.Security
{
    public interface ICryptoProvider
    {
        string SignDate();
        string VerifyDate();

        byte[] Encrypt(byte[] data);
        byte[] Decrypt(byte[] data);
    }
}