﻿#region Using directives
using System;
using System.IO;

#endregion

namespace Allag.Core.Security
{
    //[AttributeUsageAttribute(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    //public class LicenseAttribute : Attribute
    //{
    //    #region Variables
    //    private readonly Guid _guid;
    //    private readonly long _lCreationTickCount;
    //    private readonly string _strKey;
    //    private readonly string _strLicenseFileName;
    //    #endregion

    //    #region Constructors
    //    public LicenseAttribute() : this(Guid.Empty.ToString(), DateTime.MinValue.Ticks, string.Empty)
    //    {
    //    }

    //    public LicenseAttribute(string guid, long creationDate, string key)
    //        : this(guid, creationDate, key, "lic.dat")
    //    {
    //    }

    //    public LicenseAttribute(string guid, long creationDate, string key, string licenseFileName)
    //    {
    //        if (string.IsNullOrEmpty(guid))
    //        {
    //            throw new ArgumentException("The argument cannot be null or empty.", "guid");
    //        }

    //        if (key == null)
    //        {
    //            throw new ArgumentNullException("key");
    //        }

    //        if (string.IsNullOrEmpty(licenseFileName))
    //        {
    //            throw new ArgumentException("The argument cannot be null or empty.", "licenseFileName");
    //        }

    //        if (licenseFileName.IndexOfAny(Path.GetInvalidFileNameChars()) != -1)
    //        {
    //            throw new ArgumentException("The argument cannot contain any of the following characters:\n" + new string(Path.GetInvalidFileNameChars()), "licenseFileName");
    //        }

    //        _guid = new Guid(guid);
    //        _lCreationTickCount = creationDate;
    //        _strKey = key;
    //        _strLicenseFileName = licenseFileName;
    //    }
    //    #endregion

    //    #region Properties
    //    public Guid GUID
    //    {
    //        get { return _guid; }
    //    }

    //    public long CreationTickCount
    //    {
    //        get { return _lCreationTickCount; }
    //    }

    //    public string Key
    //    {
    //        get { return _strKey; }
    //    }

    //    public string LicenseFileName
    //    {
    //        get { return _strLicenseFileName; }
    //    }
    //    #endregion
    //}
}