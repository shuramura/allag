﻿#region Using directives
using System;
using System.Linq;
using System.Security.Claims;

#endregion

namespace Allag.Core.Security
{
    public class ClaimsAuthorizationManager : System.Security.Claims.ClaimsAuthorizationManager
    {
        #region Constants
        public const string OperationSeparator = ",";
        public const string ResourceSeparator = "|";
        #endregion

        #region Static properties
        public static string ResourcePrefix { get; set; }
        public static Func<string, string> ResourceSplitter { get; set; }
        #endregion

        #region Override methods
        public override bool CheckAccess(AuthorizationContext context)
        {
            return LogBlock.Action<ClaimsAuthorizationManager, bool>(
                () =>
                {
                    bool ret = false;
                    ClaimsPrincipal principal = context.Principal;
                    for (int i = 0; !ret && i < context.Resource.Count; i++)
                    {
                        string[] resources = context.Resource[i].Value.Split(ResourceSeparator[0]).Select(x => x.Trim()).ToArray();
                        string[] actions = context.Action[i].Value.Split(OperationSeparator[0]).Select(x => x.Trim()).ToArray();
                        LogBlock.Logger.Debug("Claimed resources: '{0}'; Claimed actions: '{1}'", context.Resource[i].Value, context.Action[i].Value);
                        for (int j = 0; !ret && j < resources.Length; j++)
                        {
                            string strResource = resources[j];
                            do
                            {
                                Claim claim;
                                string strClaim = ResourcePrefix + strResource;
                                if ((claim = principal.FindFirst(strClaim)) != null)
                                {
                                    ret = actions.All(x => claim.Value.Contains(x));
                                    strResource = null;
                                }
                                else
                                {
                                    strResource = SplitResource(strResource);
                                }
                            } while (strResource != null);
                        }
                    }

                    return ret;
                });
        }
        #endregion

        #region Methods
        private static string SplitResource(string resource)
        {
            string ret = null;
            if (resource != null)
            {
                if (ResourceSplitter != null)
                {
                    ret = ResourceSplitter(resource);
                }
                else if (resource.Length > 32)
                {
                    ret = resource.Substring(0, resource.Length - 32);
                }
            }

            return ret;
        }
        #endregion
    }
}