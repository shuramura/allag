﻿#region Using directives
using System;
using System.ComponentModel;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.Security
{
    public static class IdentityFactory
    {
        #region Variables
        private static readonly Context<IIdentity> _context;
        #endregion

        #region Constructors
        static IdentityFactory()
        {
            _context = new Context<IIdentity>(Guid.NewGuid());
        }
        #endregion

        #region Static properties
        public static Guid CurrentSessionKey
        {
            get
            {
                Guid ret = Guid.Empty;
                if (Current != null)
                {
                    ret = Current.GetClaim(ClaimTypes.Sid, Guid.Parse);
                }
                return ret;
            }
            set
            {
                Current?.SetClaim(ClaimTypes.Sid, value.ToString("N"));
            }
        }

        public static Guid CurrentUserId
        {
            get
            {
                return LogBlock.Action(typeof(IdentityFactory),
                    () =>
                    {
                        Guid ret = Guid.Empty;
                        if (Current != null)
                        {
                            ret = Current.GetClaim(ClaimTypes.NameIdentifier, Guid.Parse);
                        }
                        return ret;
                    });
            }
        }

        private static ClaimsIdentity Current => _context.Value as ClaimsIdentity;
        #endregion

        #region Static methods
        public static void SetIdentity(IIdentity identity, Action<IPrincipal> postAction = null)
        {
            if (identity == null)
            {
                throw new ArgumentNullException(nameof(identity));
            }

            IPrincipal principal = new ClaimsPrincipal(identity);
            Thread.CurrentPrincipal = principal;
            _context.Value = identity;

            postAction?.Invoke(principal);
        }

        public static void SetSessionKey(this IIdentity identity, Guid key)
        {
            identity.SetClaim(ClaimTypes.Sid, key, x => x.ToString("N"));
        }

        public static IIdentity SetClaim<TValue>(this IIdentity identity, string type, TValue value, Func<TValue, string> converter = null)
        {
            return LogBlock.Action(typeof(IdentityFactory),
                () =>
                {
                    if (identity == null)
                    {
                        throw new ArgumentNullException(nameof(identity));
                    }
                    ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;
                    if (claimsIdentity == null)
                    {
                        throw new ArgumentException("The claims identity is required.", "identity");
                    }

                    if (converter == null)
                    {
                        TypeConverter typeConverter = TypeDescriptor.GetConverter(typeof(TValue));
                        converter = x => typeConverter.ConvertToString(x);
                    }
                    Claim claim = claimsIdentity.FindFirst(type);
                    if (claim != null)
                    {
                        claimsIdentity.RemoveClaim(claim);
                    }

                    claimsIdentity.AddClaim(new Claim(type, converter(value)));

                    return identity;
                }, new {identity, type, value});
        }

        public static TValue GetClaim<TValue>(this IIdentity identity, string type, Func<string, TValue> converter = null)
        {
            return LogBlock.Action(typeof(IdentityFactory),
                () =>
                {
                    if (identity == null)
                    {
                        throw new ArgumentNullException("identity");
                    }
                    ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;
                    if (claimsIdentity == null)
                    {
                        throw new ArgumentException("The claims identity is required.", "identity");
                    }

                    if (converter == null)
                    {
                        TypeConverter typeConverter = TypeDescriptor.GetConverter(typeof(TValue));
                        converter = x => (TValue)typeConverter.ConvertFromString(x);
                    }

                    TValue ret = default(TValue);
                    Claim claim;
                    if ((claim = claimsIdentity.FindFirst(type)) != null)
                    {
                        ret = converter(claim.Value);
                    }
                    return ret;
                },
                new {identity, type});
        }
        #endregion
    }
}