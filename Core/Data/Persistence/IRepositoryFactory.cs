﻿#region Using directives
using System;

#endregion

namespace Allag.Core.Data.Persistence
{
    public interface IRepositoryFactory
    {
        IUnitOfWork UnitOfWork { get; }
        IRepository Get<TEntity>(params Type[] types);
        IRepository Get(Type entityType, params Type[] types);
    }

    public interface IRepositoryFactory<TIRepositoryFactory, TId> : IRepositoryFactory
        where TIRepositoryFactory : IRepositoryFactory<TIRepositoryFactory, TId>
    {
        IRepository<TIRepositoryFactory, TEntity, TId> Get<TEntity>() where TEntity : Entity<TIRepositoryFactory, TEntity, TId>;

        IRepository<TIRepositoryFactory, TEntity, TEntityId> Get<TEntity, TEntityId>()
            where TEntity : Entity<TIRepositoryFactory, TEntity, TEntityId>;
    }
}