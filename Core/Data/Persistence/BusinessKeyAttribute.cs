#region Using directives

using System;

#endregion

namespace Allag.Core.Data.Persistence
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class BusinessKeyAttribute : Attribute {}
}
