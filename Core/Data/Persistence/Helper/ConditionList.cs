﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Text;

#endregion

namespace Allag.Core.Data.Persistence.Helper
{
    public class ConditionList : List<Condition>
    {
        #region Variables
        private readonly List<ConditionList> _groups;
        #endregion

        #region Constructors
        public ConditionList(Type entityType, bool isAndJunction)
        {
            if (entityType == null)
            {
                throw new ArgumentNullException(nameof(entityType));
            }

            EntityType = entityType;
            IsNot = false;
            IsAndGroup = isAndJunction;
            _groups = new List<ConditionList>();
        }
        #endregion

        #region Properties
        public Type EntityType { get; }
        public bool IsNot { get; set; }
        public bool IsAndGroup { get; }
        public IEnumerable<ConditionList> Groups => _groups;
        #endregion

        #region Methods
        public ConditionList AddGroup(bool isAndJunction)
        {
            ConditionList ret = this;
            if (isAndJunction != IsAndGroup)
            {
                ret = new ConditionList(EntityType, isAndJunction);
                _groups.Add(ret);    
            }
            
            return ret;
        }
        #endregion

        #region Override methods
        public override string ToString()
        {
            string strGroupType = IsAndGroup ? "And" : "Or";
            StringBuilder ret = new StringBuilder(2048);
            foreach(Condition condition in this)
            {
                ret.Append(condition).Append(' ').Append(strGroupType).Append(' ');
            }

            foreach(ConditionList conditionList in Groups)
            {
                ret.Append('(').Append(conditionList).Append(')').Append(' ').Append(strGroupType).Append(' ');
            }

            int nLength = ret.Length;
            if(nLength > 0)
            {
                int nSuffixLength = 2 + strGroupType.Length;
                ret.Remove(nLength - nSuffixLength, nSuffixLength);
            }

            return IsNot ? "Not(" + ret + ")" : ret.ToString();
        }
        #endregion
    }
}