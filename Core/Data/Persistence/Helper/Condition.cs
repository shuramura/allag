﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Linq;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.Data.Persistence.Helper
{
    public class Condition
    {
        #region Constructors
        public Condition(ICollection<string> namePrefixes, Tuple<string, Type> name, string alias, Operation operation, bool isNot, object[] values)
        {
            IsNot = false;
            NamePrefixes = namePrefixes ?? new string[0];
            Name = name ?? new Tuple<string, Type>(string.Empty, null);
            Op = operation;
            if (values == null)
            {
                values = new object[0];
            }
            IsNot = isNot;
            Values = values;

            if (Op >= Operation.Projection)
            {
                Alias = alias;
                if (string.IsNullOrWhiteSpace(Alias))
                {
                    foreach (string namePrefix in NamePrefixes)
                    {
                        Alias += namePrefix;
                    }
                    Alias += Name.Item1;
                    if (Op != Operation.Property)
                    {
                        Alias += Op.EnumToString();
                    }
                }
            }
        }
        #endregion

        #region Properties
        public bool IsNot { get; }
        public string Alias { get; }
        public ICollection<string> NamePrefixes { get; }
        public Tuple<string, Type> Name { get; }
        public Operation Op { get; }
        public IEnumerable<object> Values { get; }

        public object Value => Values.FirstOrDefault();
        #endregion

        #region Override methods
        public override string ToString()
        {
            string ret;
            string value = Values.Join(", ", "[", "]", ConvertToString);
            if (Name.Item1.Length == 0)
            {
                ret = value;
            }
            else
            {
                ret = string.Empty;
                foreach (string prefix in NamePrefixes)
                {
                    ret += prefix + '.';
                }
                ret = $"{ret}{Name.Item1} {Op} {value}";
            }

            return IsNot ? "Not(" + ret + ")" : ret;
        }
        #endregion

        #region Static methods
        private static string ConvertToString(object value)
        {
            string ret = null;
            if (value != null)
            {
                if (value.GetType().IsArray)
                {
                    ret = ((Array)value).Cast<object>().Join(", ", "[", "]", ConvertToString);
                }
                else
                {
                    ret = value.ToString();
                }
            }
            return ret;
        }

        #endregion

    }
}