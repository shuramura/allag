﻿#region Using directives
using System;

#endregion

namespace Allag.Core.Data.Persistence.Helper
{
    [AttributeUsage(AttributeTargets.Method)]
    public class OperationAttribute : Attribute
    {
        #region Constructors
        public OperationAttribute(Operation operation)
        {
            Op = operation;
        }
        #endregion

        #region Properties
        public Operation Op { get; private set; }
        #endregion
    }
}