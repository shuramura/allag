﻿using System;

namespace Allag.Core.Data.Persistence.Helper
{
    [Flags]
    public enum Operation
    {
        None = 0,

        Equal = 0x000001,
        NotEqual = 0x000002,
        GreaterThan = 0x000004,
        LessThan = 0x000008,
        GreaterThanOrEqual = GreaterThan | Equal,
        LessThanOrEqual = LessThan | Equal,
        Between = 0x000010,

        Constant = 0x000020,
        Like = 0x000040,
        In = 0x000080,
        Contains = 0x000100,
        IsEmpty = 0x000200,

        Order = 0x001000,
        FirstResult = 0x002000,
        MaxResults = 0x004000,

        // Only projection operations
        Property = 0x010000,
        Distinct = 0x020000,
        GroupBy = 0x040000,
        Minimum = 0x080000,
        Maximum = 0x100000,
        Average = 0x200000,
        Sum = 0x400000,
        Count = 0x800000,

        // Internal operations
        InternalIf = 0x10000000,

        Projection = Property
    }
}
