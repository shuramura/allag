﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Reflection;
using Allag.Core.Reflection;

#endregion

namespace Allag.Core.Data.Persistence.Helper
{
    public class ExpressionCriteriaBuilder : ICriteriaBuilder
    {
        #region Implementation of ICriteriaBuilder
        public ConditionList Build(Type entityType, object criteria)
        {
            return GetConditions(criteria as LambdaExpression, entityType, false);
        }
        #endregion

        #region Static Methods
        private static ConditionList GetConditions(LambdaExpression expression, Type entityType, bool isNot)
        {
            if (expression == null)
            {
                throw new ArgumentNullException(nameof(expression));
            }
            ConditionList ret = new ConditionList(entityType, true);
            ParseExpression(expression.Body, entityType, ret, isNot);
            return ret;
        }

        private static void ParseExpression(Expression expression, Type entityType, ConditionList conditionList, bool isNot)
        {
            bool? isAndJunction = null;
            switch (expression.NodeType)
            {
                case ExpressionType.And:
                case ExpressionType.AndAlso:
                    isAndJunction = true;
                    break;
                case ExpressionType.Or:
                case ExpressionType.OrElse:
                    isAndJunction = false;
                    break;
                case ExpressionType.Not:
                    ParseExpression(((UnaryExpression)expression).Operand, entityType, conditionList, !isNot);
                    break;
                default:
                    conditionList.AddRange(GetConditions(expression, entityType, isNot));
                    break;
            }
            if (isAndJunction != null)
            {
                ConditionList list = conditionList.AddGroup(isAndJunction.Value);
                list.IsNot = isNot;
                BinaryExpression binaryExpression = (BinaryExpression)expression;
                ParseExpression(binaryExpression.Left, entityType, list, false);
                ParseExpression(binaryExpression.Right, entityType, list, false);
            }
        }

        private static IEnumerable<Condition> HandleSpecificOperation(Type entityType, MethodInfo methodInfo, IList<Expression> args, bool isNot)
        {
            List<Condition> ret = new List<Condition>();
            Operation operation = methodInfo.GetCustomAttribute<OperationAttribute>().Op;
            if (operation == Operation.InternalIf)
            {
                ret.AddRange(GetIfConditions(entityType, args, isNot));
            }
            else
            {
                IList<string> namePrefixes = null;
                Tuple<string, Type> name = null;
                object[] values = null;

                if (args.Count > 0)
                {
                    Type type;
                    name = GetName(args[0], out type, out namePrefixes);
                    int nIndex = name != null ? 1 : 0;
                    values = new object[args.Count - nIndex];
                    for (int i = nIndex; i < args.Count; i++)
                    {
                        UnaryExpression quotExpression = args[i] as UnaryExpression;
                        if (quotExpression != null && quotExpression.NodeType == ExpressionType.Quote)
                        {
                            LambdaExpression lambdaExpression = (LambdaExpression)quotExpression.Operand;
                            values[i - nIndex] = GetConditions(lambdaExpression, lambdaExpression.Parameters[0].Type, false);
                        }
                        else
                        {
                            values[i - nIndex] = Expression.Lambda<Func<object>>(Expression.Convert(args[i], typeof(object))).Compile()();
                        }
                    }
                    if (args.Count == 2 && (operation == Operation.Distinct || operation == Operation.GroupBy))
                    {
                        ret.Add(new Condition(namePrefixes, name, null, Operation.Property, isNot, values));
                    }
                }
                ret.Add(new Condition(namePrefixes, name, null, operation, isNot, values));
            }
            return ret;
        }

        private static IEnumerable<Condition> GetIfConditions(Type entityType, IList<Expression> args, bool isNot)
        {
            if (entityType != args[0].Type)
            {
                throw new InvalidDataException("The conditional type is invalid.");
            }

            int nIndex = 3;
            if (Expression.Lambda<Func<bool>>(args[1]).Compile()())
            {
                nIndex = 2;
            }
            IEnumerable<Condition> ret = new Condition[0];
            if (args[nIndex].NodeType != ExpressionType.Constant)
            {
                LambdaExpression lambdaExpression = (LambdaExpression)((UnaryExpression)args[nIndex]).Operand;
                ret = GetConditions(lambdaExpression.Body, args[0].Type, isNot);
            }
            
            return ret;
        }

        private static IEnumerable<Condition> GetConditions(Expression expression, Type entityType, bool isNot)
        {
            List<Condition> ret = new List<Condition>();
            if (!expression.HandleMethodExpression(typeof(OpMethods), (info, args) => ret.AddRange(HandleSpecificOperation(entityType, info, args, isNot))))
            {
                ret.AddRange(HandleMemeberCondition(expression, entityType, isNot));
            }

            return ret;
        }

        private static Tuple<string, Type> GetName(Expression expression, out Type type, out IList<string> namePrefixes)
        {
            List<string> list = new List<string>();
            type = null;

            Tuple<string, Type> ret = null;
            while (expression.NodeType == ExpressionType.Convert)
            {
                expression = ((UnaryExpression)expression).Operand;
            }

            MemberExpression memberExpression = expression as MemberExpression;
            MemberExpression rootExpression = memberExpression;
            while (rootExpression != null)
            {
                list.Add(rootExpression.Member.Name);
                if (!(rootExpression.Expression is ParameterExpression))
                {
                    rootExpression = rootExpression.Expression as MemberExpression;
                }
                else
                {
                    break;
                }
            }

            if (rootExpression != null)
            {
                type = memberExpression.Type;
                ret = new Tuple<string, Type>(memberExpression.Member.Name, memberExpression.Member.ReflectedType);
                list.Reverse();
                list.RemoveAt(list.Count - 1);
            }

            namePrefixes = list;
            return ret;
        }

        private static IEnumerable<Condition> HandleMemeberCondition(Expression expression, Type entityType, bool isNot)
        {
            List<Condition> ret = new List<Condition>();
            BinaryExpression expBinary = expression as BinaryExpression;
            if (expBinary != null && expBinary.Left.NodeType == ExpressionType.Call)
            {
                ret.AddRange(GetConditions(expBinary.Left, entityType, isNot));
            }
            else
            {
                Operation operation;
                Tuple<string, Type> name = null;
                Expression value;
                IList<string> namePrefixes = null;
                Type type;
                if (expression.NodeType == ExpressionType.Constant ||
                    (expression.NodeType == ExpressionType.MemberAccess && ((MemberExpression)expression).Expression.Type != entityType))
                {
                    operation = Operation.Constant;
                    value = expression;
                }
                else if (expression.NodeType == ExpressionType.MemberAccess)
                {
                    operation = Operation.Equal;
                    name = GetName(expression, out type, out namePrefixes);
                    value = Expression.Constant(true);
                }
                else 
                {
                    switch (expression.NodeType)
                    {
                        case ExpressionType.Equal:
                            operation = Operation.Equal;
                            break;
                        case ExpressionType.NotEqual:
                            operation = Operation.NotEqual;
                            break;
                        case ExpressionType.GreaterThanOrEqual:
                            operation = Operation.GreaterThanOrEqual;
                            break;
                        case ExpressionType.GreaterThan:
                            operation = Operation.GreaterThan;
                            break;
                        case ExpressionType.LessThan:
                            operation = Operation.LessThan;
                            break;
                        case ExpressionType.LessThanOrEqual:
                            operation = Operation.LessThanOrEqual;
                            break;
                        default:
                            throw new InvalidOperationException($"The operation '{expression.NodeType}' is not supported.");
                    }

                    Expression expLeft = expBinary.Left;
                    Expression expRight = expBinary.Right;

                    MethodCallExpression methodCallExpression;
                    name = GetName(expLeft, out type, out namePrefixes);
                    if (name == null)
                    {
                        operation = Operation.Constant;
                        value = expBinary;
                    }
                    else if ((methodCallExpression = expRight as MethodCallExpression) != null && methodCallExpression.Method.DeclaringType == typeof(OpMethods))
                    {
                        ConditionList conditionList = new ConditionList(entityType, true);
                        conditionList.AddRange(HandleSpecificOperation(entityType, methodCallExpression.Method, methodCallExpression.Arguments, isNot));
                        value = Expression.Constant(conditionList);
                    }
                    else
                    {
                        value = Expression.Convert(expRight, type);
                    }
                }
                try
                {
                    ret.Add(new Condition(namePrefixes, name, null, operation, isNot, new object[] {Expression.Lambda<Func<object>>(Expression.Convert(value, typeof(object))).Compile()()}));
                }
                catch (InvalidOperationException exp)
                {
                    throw new InvalidOperationException("The left part of condition should reference to the entity property.", exp);
                }
            }
            return ret;
        }
        #endregion
    }
}