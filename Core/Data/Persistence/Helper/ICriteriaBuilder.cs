﻿#region Using directives
using System;

#endregion

namespace Allag.Core.Data.Persistence.Helper
{
    public interface ICriteriaBuilder
    {
        ConditionList Build(Type entityType, object criteria);
    }
}