﻿#region Using directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;

#endregion

namespace Allag.Core.Data.Persistence.Helper
{
    [ExcludeFromCodeCoverage]
    public static class OpMethods
    {
        [Operation(Operation.Equal)]
        public static bool Equal<TEntity>(this object property, Expression<Func<TEntity, bool>> subquery)
        {
            return true;
        }

        [Operation(Operation.Like)]
        public static bool Like(this string property, string value)
        {
            return true;
        }

        [Operation(Operation.In)]
        public static bool In<TValue>(this TValue property, IEnumerable<TValue> value)
        {
            return true;
        }

        [Operation(Operation.In)]
        public static bool In<TValue>(this TValue property, params TValue[] value)
        {
            return true;
        }

        [Operation(Operation.In)]
        public static bool In<TEntity>(this object property, Expression<Func<TEntity, bool>> subquery)
        {
            return true;
        }

        [Operation(Operation.Contains)]
        public static bool Contains<TEntity, TValue>(IEnumerable<TEntity> property, params TValue[] values)
        {
            return true;
        }

        [Operation(Operation.Contains)]
        public static bool Contains<TEntity>(this IEnumerable<TEntity> property, Expression<Func<TEntity, bool>> subquery)
        {
            return true;
        }

        [Operation(Operation.Contains)]
        public static bool Contains<TEntity>(this IEnumerable property, Expression<Func<TEntity, bool>> subquery)
        {
            return true;
        }

        [Operation(Operation.IsEmpty)]
        public static bool IsEmpty<TValue>(this IEnumerable<TValue> property)
        {
            return true;
        }

        [Operation(Operation.Between)]
        public static bool Between<TValue>(this TValue property, TValue start, TValue end)
        {
            return true;
        }

        [Operation(Operation.Order)]
        public static bool Order<TValue>(this TValue property, bool isAscendant)
        {
            return true;
        }

        [Operation(Operation.GroupBy)]
        public static TValue GroupBy<TValue>(this TValue value)
        {
            return value;
        }

        [Operation(Operation.GroupBy)]
        public static bool GroupBy<TValue>(this TValue value, bool result)
        {
            return result;
        }

        [Operation(Operation.Distinct)]
        public static TValue Distinct<TValue>(this TValue value)
        {
            return value;
        }

        [Operation(Operation.Distinct)]
        public static bool Distinct<TValue>(this TValue value, bool result)
        {
            return result;
        }

        [Operation(Operation.Property)]
        public static TValue Property<TValue>(this TValue property)
        {
            return property;
        }

        [Operation(Operation.Maximum)]
        public static TValue Max<TValue>(this TValue property)
        {
            return default(TValue);
        }

        [Operation(Operation.Minimum)]
        public static TValue Min<TValue>(this TValue property)
        {
            return default(TValue);
        }

        [Operation(Operation.Average)]
        public static double Avg(this object property)
        {
            return default(double);
        }

        [Operation(Operation.Sum)]
        public static TValue Sum<TValue>(this TValue property)
        {
            return default(TValue);
        }

        [Operation(Operation.Count)]
        public static int RowCount<TValue>(this TValue property)
        {
            return default(int);
        }

        [Operation(Operation.FirstResult)]
        public static bool FirstResult(this int value)
        {
            return true;
        }

        [Operation(Operation.MaxResults)]
        public static bool MaxResults(this int value)
        {
            return true;
        }

        [Operation(Operation.InternalIf)]
        public static bool If<TEntity>(this TEntity entity, bool conditions, Expression<Func<TEntity, bool>> then, Expression<Func<TEntity, bool>> @else = null)
        {
            return true;
        }
    }
}