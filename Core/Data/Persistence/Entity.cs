#region Using directives
using System;
using Allag.Core.Data.Attributes;
using Allag.Core.Reflection;

#endregion

namespace Allag.Core.Data.Persistence
{
    [Serializable]
    public class Entity<TIRepositoryFactory, TEntity, TId> : IEntity
        where TEntity : Entity<TIRepositoryFactory, TEntity, TId>
    {
        #region Variables
        private static readonly Func<TEntity, TEntity, bool> _equalityHandler;
        private static readonly Func<TEntity, int> _hashCodeHandler;
        #endregion

        #region Constructors
        static Entity()
        {
            ReflectionExtension.CreateEqualityAndHashCodeHandlers<TEntity, BusinessKeyAttribute>(out _equalityHandler, out _hashCodeHandler);
        }
        #endregion

        #region Implementation of IEntity
        object IEntity.Id => Id;
        #endregion

        #region Properties
        public virtual TId Id { get; protected set; }

        [ExcludeFromSearch]
        public virtual bool IsTransient
        {
            get { return ReferenceEquals(Id, null) || Id.Equals(default(TId)); }
        }
        #endregion

        #region Override methods
        public override bool Equals(object obj)
        {
            return Equals(obj as TEntity);
        }

        public override int GetHashCode()
        {
            int ret;
            if (IsTransient)
            {
                ret = _hashCodeHandler((TEntity)this);
            }
            else
            {
                unchecked
                {
                    ret = (ReflectionExtension.MagicPrimeNumber ^ Id.GetHashCode());
                }
            }
            return ret == 0 ? base.GetHashCode() : ret;
        }

        public override string ToString()
        {
            return $"{base.ToString()}(Id: {Id})";
        }
        #endregion

        #region Methods
        public virtual bool Equals(TEntity other)
        {
            return other != null && IsTransient == other.IsTransient &&
                   ((IsTransient && _equalityHandler((TEntity)this, other)) ||
                    (!IsTransient && (ReferenceEquals(this, other) || Id.Equals(other.Id))));
        }
        #endregion

        #region Static methods
        public static bool operator ==(Entity<TIRepositoryFactory, TEntity, TId> first, Entity<TIRepositoryFactory, TEntity, TId> second)
        {
            return Equals(first, second);
        }

        public static bool operator !=(Entity<TIRepositoryFactory, TEntity, TId> first, Entity<TIRepositoryFactory, TEntity, TId> second)
        {
            return !(first == second);
        }
        #endregion
    }
}