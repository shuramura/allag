﻿#region Using directives
using System;
using Allag.Core.Reflection;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.Data.Persistence
{
    public class UnitOfWorkFactory
    {
        #region Variables
        private static readonly Context<IUnitOfWork> _session;
        #endregion

        #region Constructors
        static UnitOfWorkFactory()
        {
            _session = new Context<IUnitOfWork>(Guid.NewGuid());
        }
        #endregion

        #region Static properties
        public static IUnitOfWork Current
        {
            get { return _session.Value; }
            private set { _session.Value = value; }
        }
        #endregion

        #region Static methods
        public static IUnitOfWork Create(bool isLightSession = false, bool isTransactional = false)
        {
            IUnitOfWork ret = Current;
            if (ret != null)
            {
                throw new InvalidOperationException("The unit of work is already created.");
            }
            ret = ObjectFactory.Create<IUnitOfWork>(typeof(UnitOfWorkFactory)).OpenSession(isLightSession, isTransactional);
            ret.Closed += (sender, args) => { Current = null; };
            Current = ret;
            return ret;
        }

        public static void Execute(bool isLightSession, bool isTransactional, Action action, Action<Exception> exceptionHandler = null)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }
            Execute(isLightSession, isTransactional, uow => action(), exceptionHandler);
        }

        public static void Execute(bool isLightSession, bool isTransactional, Action<IUnitOfWork> action, Action<Exception> exceptionHandler = null)
        {
            using(LogBlock.New<UnitOfWorkFactory>(new {isLightSession, isTransactional}))
            {
                if (action == null)
                {
                    throw new ArgumentNullException(nameof(action));
                }

                Execute(isLightSession, isTransactional, uow =>
                {
                    action(uow);
                    return true;
                }, exceptionHandler);
            }
        }

        public static TResult Execute<TResult>(bool isLightSession, bool isTransactional, Func<TResult> action, Action<Exception> exceptionHandler = null)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            return Execute(isLightSession, isTransactional, uow => action(), exceptionHandler);
        }

        public static TResult Execute<TResult>(bool isLightSession, bool isTransactional, Func<IUnitOfWork, TResult> action, Action<Exception> exceptionHandler = null)
        {
            using(LogBlock.New<UnitOfWorkFactory>(new {isLightSession, isTransactional}))
            {
                if (action == null)
                {
                    throw new ArgumentNullException(nameof(action));
                }

                TResult ret = default(TResult);
                IUnitOfWork unitOfWork = null;
                if (Current == null)
                {
                    unitOfWork = Create(isLightSession, isTransactional);
                }
                try
                {
                    try
                    {
                        ret = action(Current);
                    }
                    catch (OperationCanceledException)
                    {
                        LogBlock.Logger.Info("The task is canceled.");
                    }
                    unitOfWork?.Commit();
                }
                catch (Exception exp)
                {
                    LogBlock.Logger.Error("The action is failed.", exp);
                    Current.Rollback();
                    exceptionHandler?.Invoke(exp);
                    throw;
                }
                finally
                {
                    unitOfWork?.Dispose();
                }
                return ret;
            }
        }

        public static void Close()
        {
            Current?.Dispose();
        }
        #endregion
    }
}