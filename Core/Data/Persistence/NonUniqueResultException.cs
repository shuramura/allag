﻿#region Using directives
using System;

#endregion

namespace Allag.Core.Data.Persistence
{
    public class NonUniqueResultException : Exception
    {
        #region Constructors
        public NonUniqueResultException(string message, Exception innerException) : base(message, innerException) {}
        #endregion
    }
}