﻿namespace Allag.Core.Data.Persistence
{
    public interface IEntity
    {
        object Id { get; }
    }
}