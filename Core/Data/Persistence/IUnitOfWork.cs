﻿#region Using directives
using System;

#endregion

namespace Allag.Core.Data.Persistence
{
    public interface IUnitOfWork : IDisposable
    {
        Guid Id { get; }
       
        IRepositoryFactory this[Type factoryType] { get; }
        TIRepositoryFactory Get<TIRepositoryFactory>() where TIRepositoryFactory : class, IRepositoryFactory;

        IUnitOfWork OpenSession(bool isLightSession = false, bool isTransactional = false);

        void Commit();
        void Rollback();

        void BeginTransaction();
        void CloseTransaction(bool isCommitted = true);

        void CloseSession();

        event EventHandler Closed;
    }
}