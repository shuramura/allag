﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

#endregion

namespace Allag.Core.Data.Persistence
{
    public interface IRepository
    {
        Operations SupportedOperations { get; }
        IUnitOfWork UnitOfWork { get; }

        object this[object id, bool throwException = false] { get; }

        dynamic Get<TCriteria>(params TCriteria[] criteria) where TCriteria : class;
        IEnumerable<dynamic> Find<TCriteria>(params TCriteria[] criteria) where TCriteria : class;

        Lazy<long> Count();
        Lazy<long> Count<TCriteria>(params TCriteria[] criteria) where TCriteria : class;

        void Delete(object entity);
        object Add(object entity);
        object Update(object entity);
    }

    public interface IRepository<out TIRepositoryFactory, TEntity, in TId> : IRepository
        where TEntity : Entity<TIRepositoryFactory, TEntity, TId>
        where TIRepositoryFactory : IRepositoryFactory
    {
        TIRepositoryFactory RepositoryFactory { get; }

        IQueryable<TEntity> Items { get; }

        Lazy<long> Count(Expression<Func<TEntity, bool>> criterion, params Expression<Func<TEntity, bool>>[] criteria);

        TEntity this[TId id, bool throwException = false] { get; }
        TEntity this[params Expression<Func<TEntity, bool>>[] criteria] { get; }

        IEnumerable<TEntity> Find(params Expression<Func<TEntity, bool>>[] criteria);
        IEnumerable<TItem> Find<TItem>(Expression<Func<TEntity, TItem>> properties, params Expression<Func<TEntity, bool>>[] criteria);

        bool Exists(params Expression<Func<TEntity, bool>>[] criteria);

        TEntity Add(TEntity entity);
        TEntity Update(TEntity entity);
        void Delete(TEntity entity);
    }
}