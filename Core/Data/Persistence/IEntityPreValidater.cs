﻿namespace Allag.Core.Data.Persistence
{
    public interface IEntityPreValidater
    {
        void Validate(Operations operations);
    }
}