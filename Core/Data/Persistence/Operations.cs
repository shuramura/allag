﻿#region Using directives
using System;

#endregion

namespace Allag.Core.Data.Persistence
{
    [Flags]
    public enum Operations
    {
        None = 0x0,
        Add = 0x01,
        Update = 0x02,
        Delete = 0x04,
        All = Add | Update | Delete
    }
}