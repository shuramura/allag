﻿#region Using directives
using System;

#endregion

namespace Allag.Core.Data.Persistence
{
    public class EntityNotFoundException : Exception
    {
        #region Constructors
        protected EntityNotFoundException(Type entityType, string message, params object[] args)
            : base(string.Format(message, args))
        {
            EntityType = entityType;
        }
        #endregion

        #region Properties
        public Type EntityType { get; private set; }
        #endregion
    }

    public class EntityNotFoundException<TEntity> : EntityNotFoundException
        where TEntity : class
    {
        #region Constructors
        public EntityNotFoundException(string message, params object[] args)
            : base(typeof(TEntity), message, args) {}
        #endregion
    }

    public class EntityNotFoundException<TEntity, TId> : EntityNotFoundException<TEntity>
        where TEntity : class
    {
        #region Constructors
        public EntityNotFoundException(TId entityId)
            : base("The entity '{0}' with EntityId '{1}' is not found.", typeof(TEntity), entityId)
        {
            EntityId = entityId;
        }
        #endregion

        #region Properties
        public TId EntityId { get; private set; }
        #endregion
    }
}