#region Using directives
using System.Collections;
using System.Collections.Generic;

#endregion

namespace Allag.Core.Data.Text
{
    public interface IComplexParseProvider<TItem>
    {
        void StartParsing();
        object GetObjectForParsing(IDictionary<string, string> row);
        IList<TItem> EndParsing();

        IEnumerable ObjectToSave { get; }
    }
}