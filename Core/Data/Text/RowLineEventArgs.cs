﻿#region Using directives
using System;
using System.Collections.Generic;

#endregion

namespace Allag.Core.Data.Text
{
    public class RowLineEventArgs : EventArgs
    {
        #region Constructors
        public RowLineEventArgs(IEnumerable<string> columns, IEnumerable<string> values)
        {
            Columns = columns;
            Values = values;
        }
        #endregion

        #region Properties
        public IEnumerable<string> Columns { get; private set; }
        public IEnumerable<string> Values { get; private set; }
        #endregion
    }
}