#region Using directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

#endregion

namespace Allag.Core.Data.Text
{
    public class DelimitedParseEngine : IParseEngine
    {
        #region Variables
        private readonly char[] _delimiters;
        private char _currentDelimiter;
        private readonly char _serviceCharacter;
        private Stream _stream;
        private readonly bool _isColumnCountFixed;

        private string[] _columnNames;
        private string[] _currentValues;

        private StreamReader _streamReader;
        private bool _bDisposed;
        #endregion

        #region Events
        public EventHandler<RowLineEventArgs> BeforeAddRowLineEvent;
        #endregion


        #region Constructors
        public DelimitedParseEngine(Stream stream, bool isHeaderExists = false, bool isColumnCountFixed = true, string delimiters = ",", char serviceCharacter = '\"')
        {
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            if (string.IsNullOrEmpty(delimiters))
            {
                throw new ArgumentNullOrEmptyException(nameof(delimiters));
            }

            _delimiters = delimiters.ToCharArray();
            if (Array.IndexOf(_delimiters, char.MinValue) != -1)
            {
                throw new ArgumentOutOfRangeException(nameof(delimiters), delimiters, $"The parameter cannot contain '{char.MinValue}' delimiter.");
            }

            if (Array.IndexOf(_delimiters, serviceCharacter) != -1)
            {
                throw new ArgumentOutOfRangeException(nameof(delimiters), delimiters, $"The parameter cannot contain '{serviceCharacter}' character.");
            }

            SkipedLineCount = 0;
            _streamReader = null;
            _columnNames = null;
            _currentValues = null;

            _currentDelimiter = char.MinValue;

            _serviceCharacter = serviceCharacter;
            _stream = stream;
            _isColumnCountFixed = isColumnCountFixed;
            IsHeaderExists = isHeaderExists;
            Reset();
        }

        ~DelimitedParseEngine()
        {
            Dispose(false);
        }
        #endregion

        #region Implementation of IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation of IEnumerator
        public virtual bool MoveNext()
        {
            CheckDisposed();

            _currentValues = ParseString(_streamReader.ReadLine(), CurrentLine + 1);
            if (_currentValues != null)
            {
                CurrentLine++;
                return true;
            }

            return false;
        }

        public virtual void Reset()
        {
            CheckDisposed();

            Clear();

            _columnNames = ParseString(_streamReader.ReadLine(), ++CurrentLine) ?? new string[0];

            if (!IsHeaderExists)
            {
                if (IsColumnCountFixed)
                {
                    for (int i = 0; i < _columnNames.Length; i++)
                    {
                        _columnNames[i] = i.ToString(CultureInfo.InvariantCulture);
                    }
                }
                else
                {
                    _columnNames = null;
                }

                CreateStreamerReader();
                CurrentLine = 0;
            }
            else if (IsColumnCountFixed)
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>(_columnNames.Length * 2);
                for (int i = 0; i < _columnNames.Length; i++)
                {
                    _columnNames[i] = _columnNames[i].ToLower();
                    try
                    {
                        dictionary.Add(_columnNames[i], null);
                    }
                    catch (ArgumentException)
                    {
                        throw new ParseFormatException(ParseState.DuplicatedColumnNameError, $"The column with the name '{_columnNames[i]}' already exists.", 1);
                    }
                }
            }
        }

        public virtual IDictionary<string, string> Current
        {
            get
            {
                CheckDisposed();

                if (_currentValues == null)
                {
                    throw new InvalidOperationException();
                }

                Dictionary<string, string> ret = new Dictionary<string, string>(_currentValues.Length * 2);

                for (int i = 0; i < _currentValues.Length; i++)
                {
                    string strColumnName = _columnNames != null && _columnNames.Length > i ? _columnNames[i] : i.ToString(CultureInfo.InvariantCulture);
                    ret.Add(strColumnName, _currentValues[i]);
                }

                return ret;
            }
        }

        object IEnumerator.Current => Current;
        #endregion

        #region Implementation of IParseEngine
        public bool IsHeaderExists { get; }

        public bool IsColumnCountFixed => IsHeaderExists || _isColumnCountFixed;

        public IList<string> Columns => _columnNames?.ToList();

        public bool IsEmpty => _stream == null || _stream.Length == 0;
        #endregion

        #region Properties
        private int SkipedLineCount { get; }
        private int CurrentLine { get; set; }
        #endregion

        #region Methods
        private void Dispose(bool disposing)
        {
            if (!_bDisposed && disposing && _streamReader != null)
            {
                _stream = null;
                _streamReader = null;
            }

            _bDisposed = true;
        }

        private void Clear()
        {
            CreateStreamerReader();
            _columnNames = null;
            _currentValues = null;
            CurrentLine = -1;
            _currentDelimiter = char.MinValue;
        }

        private void CreateStreamerReader()
        {
            _stream.Seek(0, SeekOrigin.Begin);
            _streamReader = new StreamReader(_stream);
            int nIndex = 0;
            while (!_streamReader.EndOfStream && nIndex < SkipedLineCount)
            {
                _streamReader.ReadLine();
                nIndex++;
            }

        }

        protected virtual string[] ParseString(string line, int lineIndex)
        {
            if (line == null || (line = line.Trim((char)0x1A)).Length == 0)
            {
                return null;
            }

            List<string> ret = new List<string>();

            int nIndex = 0;
            StringBuilder str = new StringBuilder(100);
            char chStart = char.MinValue;
            bool isDoubleServiceCharacter = false;

            while (nIndex < line.Length)
            {
                bool bAppend = true;
                char ch = line[nIndex];
                if (_currentDelimiter == char.MinValue)
                {
                    foreach (char delimiter in _delimiters)
                    {
                        if (ch == delimiter)
                        {
                            _currentDelimiter = delimiter;
                            break;
                        }
                    }
                }
                if (ch == _currentDelimiter)
                {
                    if (isDoubleServiceCharacter && chStart != _serviceCharacter)
                    {
                        ThrowFormatException(lineIndex, ret.Count, nIndex);
                    }
                    if (chStart == char.MinValue || chStart != _serviceCharacter ||
                        (chStart == _serviceCharacter && isDoubleServiceCharacter))
                    {
                        ret.Add(str.ToString().Trim());
                        str.Length = 0;
                        chStart = char.MinValue;
                        isDoubleServiceCharacter = false;
                        bAppend = false;
                    }
                }
                else if (ch == _serviceCharacter)
                {
                    if (chStart == char.MinValue)
                    {
                        chStart = ch;
                        bAppend = false;
                    }
                    else if (isDoubleServiceCharacter)
                    {
                        isDoubleServiceCharacter = false;
                    }
                    else
                    {
                        isDoubleServiceCharacter = true;
                        bAppend = false;
                    }
                }
                else
                {
                    if (isDoubleServiceCharacter)
                    {
                        ThrowFormatException(lineIndex, ret.Count, nIndex);
                    }
                    if (chStart == char.MinValue)
                    {
                        chStart = ch;
                    }
                }

                if (bAppend)
                {
                    str.Append(ch);
                }

                nIndex++;
            }

            if (chStart != char.MinValue || (chStart == char.MinValue && ret.Count > 0))
            {
                if ((isDoubleServiceCharacter && chStart != _serviceCharacter) || (!isDoubleServiceCharacter && chStart == _serviceCharacter))
                {
                    ThrowFormatException(lineIndex, ret.Count, nIndex);
                }

                ret.Add(str.ToString().Trim());
            }

            BeforeAddRowLineEventHandler(ret);
            return ret.ToArray();
        }

        private void CheckDisposed()
        {
            if (_bDisposed)
            {
                throw new ObjectDisposedException(GetType().FullName);
            }
        }

        private static void ThrowFormatException(int lineIndex, int itemIndex, int position)
        {
            throw new ParseFormatException(lineIndex, itemIndex, position + 1);
        }
        #endregion

        #region Event's methods
        private void BeforeAddRowLineEventHandler(IEnumerable<string> values)
        {
            BeforeAddRowLineEvent?.Invoke(this, new RowLineEventArgs(_columnNames, values));
        }
        #endregion

    }
}