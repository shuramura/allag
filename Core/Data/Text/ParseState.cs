#region Using directives

#endregion

namespace Allag.Core.Data.Text
{
    public enum ParseState
    {
        UnknownError,
        FormatError,
        ColumnCountError,
        ColumnNameError,
        DuplicatedColumnNameError,
        DataError,
        LineTypeError
    }
}