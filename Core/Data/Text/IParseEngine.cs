#region Using directives
using System.Collections.Generic;

#endregion

namespace Allag.Core.Data.Text
{
    public interface IParseEngine : IEnumerator<IDictionary<string, string>>
    {
        bool IsHeaderExists { get; }
        bool IsColumnCountFixed { get; }
        IList<string> Columns { get; }

        bool IsEmpty { get; }
    }
}