#region Using directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

#endregion

namespace Allag.Core.Data.Text
{
    public class Parser<TItem>
        where TItem : new()
    {
        #region Variables
        private readonly Dictionary<Type, ObjectHelper> _objectHelpers;
        private readonly Stream _stream;
        private readonly string _delimiters;
        private readonly string _serviceCharacter;
        private readonly string _doubleServiceCharacter;
        private readonly bool _isComplexType;
        #endregion

        #region Constructors
        public Parser(Stream stream, bool isHeaderExists = false, string delimiters = ",", char serviceCharacter = '\"', IParseEngine parseEngine = null)
        {
            _stream = stream;
            _delimiters = delimiters;
            _serviceCharacter = new string(serviceCharacter, 1);
            _doubleServiceCharacter = new string(serviceCharacter, 2);

            Type itemType = typeof(TItem);
            _objectHelpers = new Dictionary<Type, ObjectHelper> {{itemType, new ObjectHelper(itemType)}};

            _isComplexType = typeof(IComplexParseProvider<TItem>).IsAssignableFrom(itemType);
            if (parseEngine == null)
            {
                parseEngine = new DelimitedParseEngine(stream, isHeaderExists, !_isComplexType, delimiters, serviceCharacter);
            }
            ParseEngine = parseEngine;
        }
        #endregion

        #region Properties
        private IParseEngine ParseEngine { get; }
        #endregion

        #region Methods
        public IEnumerable<TItem> Load()
        {
            using(LogBlock.New<Parser<TItem>>())
            {
                ParseEngine.Reset();
                IEnumerable<TItem> ret;
                if (ParseEngine.IsEmpty)
                {
                    ret = new List<TItem>();
                }
                else
                {
                    ret = _isComplexType ? ComplexParsing() : SimpleParsing();
                }
        
                return ret;
            }
        }

        public void Save(TItem item)
        {
            if (!typeof(TItem).IsValueType && ReferenceEquals(item, null))
            {
                throw new ArgumentNullException(nameof(item));
            }
            Save(new List<TItem> {item});
        }

        public void Save(IEnumerable<TItem> items)
        {
            using(LogBlock.New<Parser<TItem>>())
            {
                if(items == null)
                {
                    throw new ArgumentNullException(nameof(items));
                }

                _stream.SetLength(0);
                ObjectHelper objectHelper = GetObjectHelper(typeof(TItem));
                StreamWriter writer = new StreamWriter(_stream);
                List<DataParameter> columns = objectHelper.OutputFields;
                List<string> row = new List<string>(columns.Count);

                // Write header
                if (ParseEngine.IsHeaderExists)
                {
                    string strDelimiter = "" + _delimiters[0];
                    columns.ForEach(dataParameter => row.Add(NormalizeValue(dataParameter.ParameterName, strDelimiter)));
                    writer.WriteLine(string.Join(strDelimiter, row.ToArray()));
                }

                // Write rows
                if(_isComplexType)
                {
                    foreach(TItem item in items)
                    {
                        WriteRows(((IComplexParseProvider<TItem>) item).ObjectToSave, writer);
                    }
                }
                else
                {
                    WriteRows(items, writer);
                }
                writer.Flush();
            }
        }

        private void WriteRows(IEnumerable items, TextWriter writer)
        {
            List<string> row = new List<string>();
            string strDelimiter = "" + _delimiters[0];
            foreach(object item in items)
            {
                row.Clear();
                ObjectHelper objectHelper = GetObjectHelper(item.GetType());
                objectHelper.Object = item;
                objectHelper.OutputFields.ForEach(delegate(DataParameter dataParameter)
                                                     {
                                                         object value = dataParameter.Value ?? string.Empty;
                                                         string strValue = value.ToString();
                                                         if(dataParameter.Size != -1)
                                                         {
                                                             if(dataParameter.ParameterType == typeof(string))
                                                             {
                                                                 strValue = strValue.PadRight(dataParameter.Size);
                                                             }
                                                             else
                                                             {
                                                                 strValue = strValue.PadLeft(dataParameter.Size);
                                                             }
                                                         }
                                                         if(dataParameter.Order != int.MaxValue)
                                                         {
                                                             while(dataParameter.Order > row.Count)
                                                             {
                                                                 row.Add(string.Empty);
                                                             }
                                                         }

                                                         row.Add(NormalizeValue(strValue, strDelimiter));
                                                     });
                writer.WriteLine(string.Join(strDelimiter, row.ToArray()));
            }
        }

        private IEnumerable<TItem> ComplexParsing()
        {
            IComplexParseProvider<TItem> complexParseProvider = (IComplexParseProvider<TItem>) new TItem();
            complexParseProvider.StartParsing();

            int nLineIndex = 1;
            while (ParseEngine.MoveNext())
            {
                IDictionary<string, string> row = ParseEngine.Current;
                // Get current object
                object item;
                try
                {
                    item = complexParseProvider.GetObjectForParsing(row);
                }
                catch(Exception exc)
                {
                    throw new ParseFormatException("The error occurred within getting parsing object operation.", nLineIndex, exc);
                }

                if(item == null)
                {
                    throw new ParseFormatException(ParseState.LineTypeError, "The line represents unsupported line type.", nLineIndex);
                }

                // Get object helper for the current object
                ObjectHelper objectHelper = GetObjectHelper(item.GetType());

                // Get column names for the current object
                objectHelper.Object = item;
                ValidateColumnCount(row.Values.Count, objectHelper, nLineIndex);
                IDictionary<DataParameter, string> columnNames = GetColumnNames(objectHelper.InputFields, false, null, nLineIndex);

                // Set values to the current object
                SetParameterValues(objectHelper, columnNames, row, nLineIndex);

                nLineIndex++;
            }

            IList<TItem> ret;
            try
            {
                ret = complexParseProvider.EndParsing();
            }
            catch(Exception exc)
            {
                throw new ParseFormatException("The error occurred within finishing parsing operation.", -1, exc);
            }

            return ret;
        }

        private IEnumerable<TItem> SimpleParsing()
        {
            ObjectHelper objectHelper = GetObjectHelper(typeof(TItem));
            int nLineIndex = 1;

            // Check columns
            IList<string> columns = ParseEngine.Columns;

            ValidateColumnCount(columns?.Count ?? -1, objectHelper, nLineIndex);
            IDictionary<DataParameter, string> columnNames = GetColumnNames(objectHelper.InputFields, true, columns, nLineIndex);

            // Read data
            while (ParseEngine.MoveNext())
            {
                IDictionary<string, string> row = ParseEngine.Current;

                TItem item = new TItem();
                objectHelper.Object = item;
                SetParameterValues(objectHelper, columnNames, row, nLineIndex);
                nLineIndex++;
                yield return item;
            }
        }


        private IDictionary<DataParameter, string> GetColumnNames(List<DataParameter> dataParameters, bool isValidate, IList<string> columns, int lineIndex)
        {
            Dictionary<DataParameter, string> columnNames = new Dictionary<DataParameter, string>(dataParameters.Count * 2);
            int nMaxNotNullColumn = 0;
            dataParameters.ForEach(delegate(DataParameter dataParameter)
                                       {
                                           string strColumnName = ParseEngine.IsHeaderExists ? dataParameter.ParameterName.ToLower() : dataParameter.Order.ToString(CultureInfo.InvariantCulture);
                                           if(isValidate && columns.IndexOf(strColumnName) == -1 && (!dataParameter.IsNullable || nMaxNotNullColumn > dataParameter.Order))
                                           {
                                               throw new ParseFormatException(ParseState.ColumnNameError, $"The column '{strColumnName}' is not found.", lineIndex);
                                           }
                                           if(!dataParameter.IsNullable)
                                           {
                                               nMaxNotNullColumn = Math.Max(nMaxNotNullColumn, dataParameter.Order);
                                           }
                                           columnNames.Add(dataParameter, strColumnName);
                                       });
            return columnNames;
        }

        private ObjectHelper GetObjectHelper(Type type)
        {
            ObjectHelper ret;
            if(!_objectHelpers.TryGetValue(type, out ret))
            {
                ret = new ObjectHelper(type);
                _objectHelpers.Add(type, ret);
            }
            return ret;
        }

        private string NormalizeValue(string value, string delimiter)
        {
            value = value.Replace(_serviceCharacter, _doubleServiceCharacter);
            if(value.Contains(delimiter) || value.Contains(_serviceCharacter))
            {
                value = _serviceCharacter + value + _serviceCharacter;
            }
            return value;
        }
        #endregion

        #region Static methods
        private static void ValidateColumnCount(int columnCount, ObjectHelper objectHelper, int lineIndex)
        {
            List<DataParameter> columns = objectHelper.InputFields;
            int nColumCount = columns.Count;
            int nMaxColumnCount = 0;
            int nMinColumnCount = 0;
            if(nColumCount > 0)
            {
                nMaxColumnCount = GetColumnCount(columns[nColumCount - 1].Order, nColumCount);

                while (nColumCount > 0 && columns[nColumCount - 1].IsNullable) nColumCount--;
                if(nColumCount > 0)
                {
                    nMinColumnCount = GetColumnCount(columns[nColumCount - 1].Order, nColumCount);
                }
            }

            if(columnCount > nMaxColumnCount || columnCount < nMinColumnCount)
            {
                string strMessageTemplate;
                if(nMaxColumnCount == nMinColumnCount)
                {
                    strMessageTemplate = "The count of columns {0} is wrong. The possible value is {1}.";
                }
                else
                {
                    strMessageTemplate = "The count of columns {0} is wrong. The range of possible values is [{1} - {2}].";
                }
                throw new ParseFormatException(ParseState.ColumnCountError, string.Format(strMessageTemplate, columnCount, nMinColumnCount, nMaxColumnCount), lineIndex);
            }
        }

        private static int GetColumnCount(int columnCount, int defaultValue)
        {
            if(columnCount == int.MaxValue)
            {
                columnCount = defaultValue;
            }
            else
            {
                columnCount++;
            }
            return columnCount;
        }

        private static void SetParameterValues(ObjectHelper objectHelper, IDictionary<DataParameter, string> columnNames, IDictionary<string, string> row, int lineIndex)
        {
            foreach(DataParameter dataParameter in objectHelper.InputFields)
            {
                string strColumnName = null;
                try
                {
                    strColumnName = columnNames[dataParameter];
                    if(dataParameter.IsNullable)
                    {
                        string strValue;
                        if(row.TryGetValue(strColumnName, out strValue))
                        {
                            dataParameter.Value = strValue;
                        }
                    }
                    else
                    {
                        dataParameter.Value = row[strColumnName];
                    }
                }
                catch(Exception exc)
                {
                    throw new ParseFormatException(ParseState.DataError, lineIndex, strColumnName, exc);
                }
            }
        }
        #endregion
    }
}