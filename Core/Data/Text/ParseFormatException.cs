#region Using directives
using System;
#endregion

namespace Allag.Core.Data.Text
{
    public class ParseFormatException : FormatException
    {
        #region Variables
        #endregion

        #region Constructors

        public ParseFormatException(ParseState parseState, string message, int lineIndex)
            : base(message)
        {
            State = parseState;
            LineIndex = lineIndex;
            ItemIndex = -1;
            Position = -1;
        }

        public ParseFormatException(string message, int lineIndex, Exception innerException)
            : base(message, innerException)
        {
            State = ParseState.UnknownError;
            LineIndex = lineIndex;
            ItemIndex = -1;
            Position = -1;
        }

        public ParseFormatException(int lineIndex, int itemIndex, int position)
            : base(string.Format("The value ({0}, {1}) in the line '{0}' and position '{2}' is not valid.",
                                               lineIndex, itemIndex, position))
        {
            State = ParseState.FormatError;
            LineIndex = lineIndex;
            ItemIndex = itemIndex;
            Position = position;
        }

        public ParseFormatException(ParseState parseState, int lineIndex, string itemName, Exception innerException)
            : base($"The item '{itemName}' in the line '{lineIndex}' is not valid.", innerException)
        {
            State = parseState;
            LineIndex = lineIndex;
            ItemIndex = -1;
            Position = -1;
        }

        #endregion

        #region Properties

        public ParseState State { get; }

        public int LineIndex { get; }

        public int ItemIndex { get; }

        public int Position { get; }
        #endregion
    }
}
