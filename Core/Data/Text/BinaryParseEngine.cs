﻿#region Using directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

#endregion

namespace Allag.Core.Data.Text
{
    public class BinaryParseEngine: IParseEngine<byte[]>
    {
        #region Variables
        private Stream _stream;
        private readonly long _lOffset;
       
        private bool _bDisposed;
        #endregion

        #region Constructors
        public BinaryParseEngine(Stream stream, long offset = 0)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            if (offset < 0)
            {
                throw new ArgumentOutOfRangeException("offset", offset, "The value should be equal or greater zero.");
            }

            _bDisposed = false;
            _stream = stream;
            _lOffset = offset;

            Reset();
        }

        ~BinaryParseEngine()
        {
            Dispose(false);
        }
        #endregion

        #region Implementation of IDisposable
        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

        #region Implementation of IEnumerator
        public bool MoveNext()
        {
            throw new NotImplementedException();
        }

        public void Reset()
        {
            CheckDisposed();
            Clear();
        }

        public IDictionary<string, byte[]> Current
        {
            get { throw new NotImplementedException(); }
        }

        object IEnumerator.Current
        {
            get { return Current; }
        }
        #endregion

        #region Implementation of IParseEngine
        public bool IsHeaderExists
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsColumnCountFixed
        {
            get { throw new NotImplementedException(); }
        }

        public IList<string> Columns
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsEmpty
        {
            get { throw new NotImplementedException(); }
        }
        #endregion

        #region Methods
        private void Dispose(bool disposing)
        {
            if (!_bDisposed && disposing)
            {
                _stream = null;
            }

            _bDisposed = true;
        }

        private void CheckDisposed()
        {
            if (_bDisposed)
            {
                throw new ObjectDisposedException(GetType().FullName);
            }
        }

        private void Clear()
        {
            _stream.Seek(_lOffset, SeekOrigin.Begin);
        }
        #endregion
    }
}