#region Using directives

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;
using Allag.Core.Data.Attributes;
using Allag.Core.Reflection;

#endregion

namespace Allag.Core.Data
{
    public class TypeDescription
    {
        #region Constants
        private const BindingFlags FIELD_BINDING_FLAGS = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField | BindingFlags.SetField | BindingFlags.GetProperty | BindingFlags.SetProperty;
        #endregion

        #region Variables
        private static readonly ConcurrentDictionary<Type, TypeDescription> _typeDescriptions;
        private readonly Type _type;
        private readonly List<FieldDescription> _fields;
        private readonly List<FieldDescription> _children;

        #endregion

        #region Constructors

        static TypeDescription()
        {
            _typeDescriptions = new ConcurrentDictionary<Type, TypeDescription>();
        }

        private TypeDescription(Type type)
        {
            _fields = new List<FieldDescription>();
            _children = new List<FieldDescription>();

            _type = type;

            ReadFields();
        }

        #endregion

        #region Properties
        public IList<FieldDescription> Fields
        {
            get { return _fields.AsReadOnly(); }
        }

        public IList<FieldDescription> Children
        {
            get { return _children.AsReadOnly(); }
        }
        #endregion

        #region Methods
        private void ReadFields()
        {
            // Read parameters
            _type.GetMembers(FIELD_BINDING_FLAGS).HandleMember(delegate(MemberInfo memberInfo)
            {
                memberInfo.HandleAttribute(delegate(FieldAttribute fieldAttribute)
                {
                    FieldDescription fieldDescription = new FieldDescription(memberInfo, fieldAttribute);
                    if (fieldDescription.ParameterType.IsClass && fieldDescription.ParameterType != typeof(string)
                        && !fieldDescription.ParameterType.IsArray && !fieldDescription.FieldAttribute.IsSimpleField)
                    {
                        // Read complex field (class)
                        _children.Add(fieldDescription);
                    }
                    else
                    {
                        // Read simple field(primitive, string and value types)
                        _fields.Add(fieldDescription);
                    }
                    return false;
                });
                return true;
            });
        }
        #endregion

        #region Static methods
        public static TypeDescription Get<TInstance>()
        {
            return Get(typeof(TInstance));
        }

        public static TypeDescription Get(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return _typeDescriptions.GetOrAdd(type, t => new TypeDescription(t));
        }

        #endregion
    }
}
