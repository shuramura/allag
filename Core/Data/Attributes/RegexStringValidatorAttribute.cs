#region Using directives
using System;

#endregion

namespace Allag.Core.Data.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class RegexStringValidatorAttribute : BaseValidatorAttribute
    {
        #region Constructors
        public RegexStringValidatorAttribute(string regex) : base(new System.Configuration.RegexStringValidatorAttribute(regex)) {}
        #endregion
    }
}