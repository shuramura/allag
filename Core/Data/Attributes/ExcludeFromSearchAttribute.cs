﻿#region Using directives
using System;

#endregion

namespace Allag.Core.Data.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Class)]
    public class ExcludeFromSearchAttribute : Attribute {}
}