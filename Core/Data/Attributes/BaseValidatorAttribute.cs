#region Using directives

using System;
using System.Configuration;

#endregion

namespace Allag.Core.Data.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public abstract class BaseValidatorAttribute : ConfigurationValidatorAttribute 
    {
        #region Constructors

        protected BaseValidatorAttribute(ConfigurationValidatorAttribute parentAttribute)
        {
            if (parentAttribute == null)
            {
                throw new ArgumentNullException(nameof(parentAttribute));
            }

            ParentAttribute = parentAttribute;
        }

        #endregion

        #region Properties
        public override ConfigurationValidatorBase ValidatorInstance => ParentAttribute.ValidatorInstance;

        protected ConfigurationValidatorAttribute ParentAttribute { get; }
        #endregion
    }
}
