#region Using directives

using System;

#endregion

namespace Allag.Core.Data.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class CallbackValidatorAttribute : BaseValidatorAttribute
    {
        #region Constructors

        public CallbackValidatorAttribute() : base(new System.Configuration.CallbackValidatorAttribute()) { }

        #endregion

        #region Properties
        public string CallbackMethodName
        {
            get { return Parent.CallbackMethodName; }
            set { Parent.CallbackMethodName = value; }
        }

        public Type Type
        {
            get { return Parent.Type; }
            set { Parent.Type = value; }
        }

        private System.Configuration.CallbackValidatorAttribute Parent => (System.Configuration.CallbackValidatorAttribute)ParentAttribute;
        #endregion
    }
}
