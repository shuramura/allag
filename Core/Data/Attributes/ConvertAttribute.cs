#region Using directives
using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq.Expressions;
using System.Reflection;

#endregion

namespace Allag.Core.Data.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = true)]
    public class ConvertAttribute : Attribute
    {
        #region Variables
        private static readonly CultureInfo _cultureInfo;
       
        private Func<object, object, object> _convertToHandler;
        private Func<object, object, object> _convertFromHandler;
        #endregion

        #region Constructors
        static ConvertAttribute()
        {
            _cultureInfo = new CultureInfo("en-US");
        }

        public ConvertAttribute(Type toType)
        {
            if (toType == null)
            {
                throw new ArgumentNullException(nameof(toType));
            }

            ToType = toType;

            ConverterType = null;
            MethodName = null;
            TypeConverterType = null;
            _convertToHandler = null;
            _convertFromHandler = null;

            Order = -1;

        }

        public ConvertAttribute(Type toType, string methodName)
            : this(toType)
        {
            MethodName = methodName;
        }

        public ConvertAttribute(Type toType, Type typeConverterType)
            : this(toType)
        {
            TypeConverterType = typeConverterType;
        }
        #endregion

        #region Properties
        public int Order { get; set; }
        public Type ConverterType { get; set; }

        public Type ToType { get; set; }

        public string MethodName { get; set; }

        public Type TypeConverterType { get; set; }
        #endregion

        #region Methods
        public object ConvertTo(object instance, object value)
        {
            if (_convertToHandler == null)
            {
                throw new InvalidOperationException("The object has not been initialized.");
            }
            return _convertToHandler(value, instance);
        }

        public object ConvertFrom(object instance, object value)
        {
            if (_convertFromHandler == null)
            {
                throw new InvalidOperationException("The object has not been initialized.");
            }
            return _convertFromHandler(value, instance);
        }

        public void Initialize(Type instanceType, string parameterName, Type parameterType)
        {
            if (string.IsNullOrWhiteSpace(parameterName))
            {
                throw new ArgumentException("The parameter cannot be null or empty.", nameof(parameterName));
            }

            if (instanceType == null)
            {
                throw new ArgumentNullException(nameof(instanceType));
            }

            if (parameterType == null)
            {
                throw new ArgumentNullException(nameof(parameterType));
            }

            string strMethodName = MethodName;
            if (MethodName == null)
            {
                strMethodName = parameterType.Name;
            }
           
            try
            {
                _convertToHandler = GetMethodDelegate(strMethodName + "To", instanceType, parameterName, parameterType, ToType);
            }
            catch (Exception)
            {
                _convertToHandler = GetMethodDelegate(parameterType, ToType);
            }

            try
            {
                _convertFromHandler = GetMethodDelegate(strMethodName + "From", instanceType, parameterName, ToType, parameterType);
            }
            catch (Exception)
            {
                _convertFromHandler = GetMethodDelegate(ToType, parameterType);
            }
        }

        private Func<object, object, object> GetMethodDelegate(Type sourceType, Type destinationType)
        {
            ParameterExpression expValue = Expression.Parameter(typeof(object), "value");
            ParameterExpression expInstance = Expression.Parameter(typeof(object), "instance");
            Expression expBody = expValue;
            if (sourceType != null && destinationType != null && sourceType != destinationType)
            {
                TypeConverter typeConverter;
                bool? isToMethod = null;
                if (TypeConverterType == null)
                {
                    typeConverter = TypeDescriptor.GetConverter(sourceType);

                    if (typeConverter.CanConvertTo(null, destinationType))
                    {
                        isToMethod = true;
                    }
                    else
                    {
                        typeConverter = TypeDescriptor.GetConverter(destinationType);

                        if (typeConverter.CanConvertFrom(null, sourceType))
                        {
                            isToMethod = false;
                        }
                    }
                }
                else
                {
                    typeConverter = (TypeConverter)Activator.CreateInstance(TypeConverterType);
                    isToMethod = (ToType == destinationType);
                }

                if (isToMethod != null)
                {
                    if (isToMethod.Value)
                    {
                        MethodInfo methodInfo = typeof(TypeConverter).GetMethod("ConvertTo", new[] { typeof(ITypeDescriptorContext), typeof(CultureInfo), typeof(object), typeof(Type) });
                        expBody = Expression.Call(Expression.Constant(typeConverter), methodInfo,
                                                  Expression.Convert(Expression.Constant(null), typeof(ITypeDescriptorContext)),
                                                  Expression.Constant(_cultureInfo), expValue, Expression.Constant(destinationType));
                    }
                    else
                    {
                        MethodInfo methodInfo = typeof(TypeConverter).GetMethod("ConvertFrom", new[] { typeof(ITypeDescriptorContext), typeof(CultureInfo), typeof(object) });
                        expBody = Expression.Call(Expression.Constant(typeConverter), methodInfo, Expression.Convert(Expression.Constant(null), typeof(ITypeDescriptorContext)),
                                                  Expression.Constant(_cultureInfo), expValue);
                    }
                }
            }
            return Expression.Lambda<Func<object, object, object>>(expBody, expValue, expInstance).Compile();
        }

        private Func<object, object, object> GetMethodDelegate(string methodName, Type instanceType, string parameterName, Type sourceType, Type destinationType)
        {
            Type converterType;
            BindingFlags bindingFlags = BindingFlags.Public;
            Type[] types;
            if (ConverterType == null)
            {
                converterType = instanceType;
                bindingFlags |= BindingFlags.NonPublic | BindingFlags.Instance;
                types = new[] { sourceType, typeof(string) };
            }
            else
            {
                converterType = ConverterType;
                bindingFlags |= BindingFlags.Static;
                types = new[] { sourceType, typeof(string), instanceType };
            }

            MethodInfo methodInfo = converterType.GetMethod(methodName, bindingFlags, null, types, null);
            if (methodInfo == null)
            {
                string strStatic = (bindingFlags & BindingFlags.Static) == BindingFlags.Static ? "static " : string.Empty;
                string[] parameterTypes = new string[types.Length];
                for (int i = 0; i < parameterTypes.Length; i++)
                {
                    parameterTypes[i] = types[i].FullName;
                }
                throw new InvalidOperationException($"The type '{converterType.FullName}' does not contain {strStatic}method '{destinationType} {methodName}({string.Join(", ", parameterTypes)})'");
            }

            ParameterExpression expValue = Expression.Parameter(typeof(object), "value");
            ParameterExpression expInstance = Expression.Parameter(typeof(object), "instance");
            Expression expBody;
            if (ConverterType != null)
            {
                expBody = Expression.Call(methodInfo, Expression.Convert(expValue, sourceType), Expression.Constant(parameterName), Expression.Convert(expInstance, instanceType));
            }
            else
            {
                expBody = Expression.Call(Expression.Convert(expInstance, instanceType), methodInfo, Expression.Convert(expValue, sourceType), Expression.Constant(parameterName));
            }
            expBody = Expression.Convert(expBody, typeof(object));
            return Expression.Lambda<Func<object, object, object>>(expBody, expValue, expInstance).Compile();
        }
        #endregion
    }
}