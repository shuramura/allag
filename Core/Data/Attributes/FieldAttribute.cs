#region Using directives
using System;
using System.Data;

#endregion

namespace Allag.Core.Data.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class FieldAttribute : Attribute
    {
        #region Variables
        private int _nMaxSize;
        #endregion

        #region Constructors
        public FieldAttribute() : this(null) {}

        public FieldAttribute(DbType dbType) : this(null)
        {
            DbType = dbType;
        }

        public FieldAttribute(string name)
        {
            IsNullable = true;
            Order = int.MaxValue;
            Direction = ParameterDirection.InputOutput;
            DbType = null;
            Size = -1;
            _nMaxSize = -1;

            Precision = 0;
            Scale = 0;
            IsSimpleField = false;

            Name = name;
        }
        #endregion

        #region Override properties
        public string Name { get; set; }
        #endregion

        #region Properties
        public bool IsNullable { get; set; }

        public int Order { get; set; }

        public ParameterDirection Direction { get; set; }

        public DbType? DbType { get; set; }

        public int Size { get; set; }

        public byte Precision { get; set; }
        public byte Scale { get; set; }

        public int MaxSize
        {
            get
            {
                int ret = _nMaxSize;
                if (ret == -1)
                {
                    ret = Size;
                }
                return ret;
            }
            set { _nMaxSize = value; }
        }

        public bool IsSimpleField { get; set; }
        #endregion
    }
}