#region Using directives
using System;
using System.Collections.Generic;
using System.Data;

#endregion

namespace Allag.Core.Data
{
    public class ObjectHelper
    {
        #region Variables
        private readonly DataParameter _parent;
        private object _object;
        private readonly Type _objectType;

        private TypeDescription _typeDescription;
        #endregion

        #region Constructors
        public ObjectHelper(Type objectType)
            : this()
        {
            if (objectType == null)
            {
                throw new ArgumentNullException(nameof(objectType));
            }

            _objectType = objectType;

            Initialize();
        }

        public ObjectHelper(object @object)
            : this()
        {
            if (@object == null)
            {
                throw new ArgumentNullException(nameof(@object));
            }

            _object = @object;
            _objectType = @object.GetType();

            Initialize();
        }

        private ObjectHelper()
        {
            _parent = null;
            _object = null;
            _objectType = null;

            Fields = new List<DataParameter>();
            InputFields = new List<DataParameter>();
            OutputFields = new List<DataParameter>();
        }

        private ObjectHelper(DataParameter parent)
            : this(parent.ParameterType, null)
        {
            _parent = parent;
        }

        private ObjectHelper(Type objectType, object @object)
            : this()
        {
            _object = @object;
            _objectType = objectType;

            Initialize();
        }
        #endregion

        #region Properties
        public object Object
        {
            get
            {
                object ret = _object;
                if (_parent != null)
                {
                    ret = _parent.Value;
                    if (ret == null)
                    {
                        ret = Activator.CreateInstance(_parent.ParameterType);
                        Object = ret;
                    }
                }

                return ret;
            }
            set
            {
                if (!_objectType.IsInstanceOfType(value))
                {
                    throw new ArgumentException($"The value type is wrong. The value should be '{{{_objectType.FullName}}}'.", nameof(value));
                }
                if (_parent != null)
                {
                    _parent.SetOriginalValue(value);
                }
                else
                {
                    _object = value;
                }
            }
        }

        public List<DataParameter> Fields { get; }

        public List<DataParameter> InputFields { get; }

        public List<DataParameter> OutputFields { get; }
        #endregion

        #region Methods
        private void Initialize()
        {
            _typeDescription = TypeDescription.Get(_objectType);

            foreach (FieldDescription fieldDescription in _typeDescription.Fields)
            {
                DataParameter dataParameter = new DataParameter(this, fieldDescription);
                Fields.Add(dataParameter);
                switch (fieldDescription.Direction)
                {
                    case ParameterDirection.Input:
                        InputFields.Add(dataParameter);
                        break;
                    case ParameterDirection.Output:
                        OutputFields.Add(dataParameter);
                        break;
                    case ParameterDirection.InputOutput:
                        InputFields.Add(dataParameter);
                        OutputFields.Add(dataParameter);
                        break;
                }
            }

            foreach (FieldDescription fieldDescription in _typeDescription.Children)
            {
                ObjectHelper objectHelper = new ObjectHelper(new DataParameter(this, fieldDescription));
                Fields.AddRange(objectHelper.Fields);
                InputFields.AddRange(objectHelper.InputFields);
                OutputFields.AddRange(objectHelper.OutputFields);
            }

            Fields.Sort(CompareDataParameter);
            InputFields.Sort(CompareDataParameter);
            OutputFields.Sort(CompareDataParameter);
        }
        #endregion

        #region Static methods
        private static int CompareDataParameter(DataParameter x, DataParameter y)
        {
            return x.Order.CompareTo(y.Order);
        }
        #endregion
    }
}