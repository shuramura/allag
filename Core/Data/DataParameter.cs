#region Using directives

using System;
using System.Collections.Generic;
using System.Data;

#endregion

namespace Allag.Core.Data
{
    public class DataParameter : IDbDataParameter
    {
        #region Variables
        private static readonly IDictionary<TypeCode, DbType> _dbTypeMap;

        private readonly ObjectHelper _objectHelper;
        private readonly FieldDescription _fieldDescription;

        private string _strParameterName;
        #endregion

        #region Constructors

        static DataParameter()
        {
            _dbTypeMap = new Dictionary<TypeCode, DbType>
                             {
                                 {TypeCode.Object, DbType.Object}, 
                                 {TypeCode.Boolean, DbType.Boolean}, 
                                 {TypeCode.Char, DbType.Byte}, 
                                 {TypeCode.Byte, DbType.Byte}, 
                                 {TypeCode.Int16, DbType.Int16}, 
                                 {TypeCode.UInt16, DbType.UInt16}, 
                                 {TypeCode.Int32, DbType.Int32}, 
                                 {TypeCode.UInt32, DbType.UInt32}, 
                                 {TypeCode.Int64, DbType.Int64}, 
                                 {TypeCode.UInt64, DbType.UInt64}, 
                                 {TypeCode.Single, DbType.Single}, 
                                 {TypeCode.Double, DbType.Double}, 
                                 {TypeCode.Decimal, DbType.Decimal}, 
                                 {TypeCode.DateTime, DbType.DateTime}, 
                                 {TypeCode.String, DbType.String}
                             };
        }

        public DataParameter(ObjectHelper objectHelper, FieldDescription fieldDescription)
        {
            if (objectHelper == null)
            {
                throw new ArgumentNullException(nameof(objectHelper));
            }

            if (fieldDescription == null)
            {
                throw new ArgumentNullException(nameof(fieldDescription));
            }

            _strParameterName = null;
            _objectHelper = objectHelper;
            _fieldDescription = fieldDescription;

            DbType = _fieldDescription.FieldAttribute.DbType ?? _dbTypeMap[Type.GetTypeCode(_fieldDescription.ParameterType)];
        }

        #endregion

        #region Implementation of IDataParameter

        public DbType DbType { get; set; }
        public ParameterDirection Direction
        {
            get { return _fieldDescription.FieldAttribute.Direction; }
            set { _fieldDescription.FieldAttribute.Direction = value; }
        }

        public bool IsNullable
        {
            get { return _fieldDescription.FieldAttribute.IsNullable; }
        }

        public string ParameterName
        {
            get { return _strParameterName ?? _fieldDescription.ParameterName; }
            set { _strParameterName = value; }
        }

        public string SourceColumn
        {
            get { throw new NotSupportedException(); }
            set { throw new NotSupportedException(); }
        }

        public DataRowVersion SourceVersion
        {
            get { throw new NotSupportedException(); }
            set { throw new NotSupportedException(); }
        }

        public object Value
        {
            get
            {
                return _fieldDescription.GetValue(_objectHelper.Object);
            }
            set
            {
                _fieldDescription.SetValue(_objectHelper.Object, value);
            }
        }

        #endregion

        #region Implementation of IDbDataParameter

        public byte Precision
        {
            get { return _fieldDescription.FieldAttribute.Precision; }
            set { _fieldDescription.FieldAttribute.Precision = value; }
        }

        public byte Scale
        {
            get { return _fieldDescription.FieldAttribute.Scale; }
            set { _fieldDescription.FieldAttribute.Scale = value; }
        }

        public int Size
        {
            get { return _fieldDescription.FieldAttribute.MaxSize; }
            set { _fieldDescription.FieldAttribute.MaxSize = value; }
        }

        #endregion

        #region Properties

        public int Order => _fieldDescription.FieldAttribute.Order;

        public Type ParameterType => _fieldDescription.ParameterType;
        #endregion

        #region Methods
        public void SetOriginalValue(object value)
        {
            _fieldDescription.SetOriginalValue(_objectHelper.Object, value);
        }

        #endregion
    }
}
