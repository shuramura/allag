﻿#region Using directives
using System.IO;

#endregion

namespace Allag.Core.Data.Queues
{
    public interface IMessageSerializer<TMessage, TData>
        where TMessage : class
        where TData : class
    {
        Container<TData> Serialize(TMessage message);
        Container<TMessage> Deserialize(TData data);
    }

    public interface IMessageSerializer<TMessage> : IMessageSerializer<TMessage, byte[]> where TMessage : class {}

    public interface IMessageSerializer : IMessageSerializer<Stream> {}
}