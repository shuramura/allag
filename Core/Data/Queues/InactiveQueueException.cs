﻿#region Using directives
using System;

#endregion

namespace Allag.Core.Data.Queues
{
    [Serializable]
    public class InactiveQueueException : QueueException
    {
        #region Constructors
        public InactiveQueueException() {}
        public InactiveQueueException(Guid id) : base(id, $"The queue '{id}' is inactive.") {}
        #endregion
    }
}