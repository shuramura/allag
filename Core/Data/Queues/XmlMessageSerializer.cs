﻿#region Using directives
using System.Text;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.Data.Queues
{
    public class XmlMessageSerializer<TMessage> : BinaryMessageSerializer<TMessage>
        where TMessage : class, new()
    {
        #region Implementation of MessageSerializer<TMessage>
        protected override byte[] GetData(TMessage message)
        {
            StringBuilder builder = new StringBuilder();
            message.Serialize(builder);
            return Encoding.UTF8.GetBytes(builder.ToString());
        }

        protected override TMessage GetMessage(byte[] data)
        {
            StringBuilder builder = new StringBuilder(Encoding.UTF8.GetString(data));
            return builder.Deserialize<TMessage>();
        }
        #endregion
    }
}