﻿#region Using directives
using System;

#endregion

namespace Allag.Core.Data.Queues
{
    public class Container<TItem> : IDisposable
        where TItem : class
    {
        #region Variables
        private bool _disposed;
        private readonly Func<string> _idProvider;
        #endregion

        #region Constructors
        public Container(string id, TItem item) : this(() => id, item)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullOrEmptyException("id");
            }
        }

        public Container(Func<string> idProvider, TItem item)
        {
            if (idProvider == null)
            {
                throw new ArgumentNullException(nameof(idProvider));
            }

            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            _disposed = false;
            _idProvider = idProvider;
            Item = item;
        }

        ~Container()
        {
            Dispose(false);
        }
        #endregion

        #region Implementation of IDisposable
        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

        #region Properties
        public string Id => _idProvider();

        public TItem Item { get; private set; }
        #endregion

        #region Methods
        private void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {
                IDisposable disposable = Item as IDisposable;
                if (disposable != null)
                {
                    disposable.Dispose();
                    Item = null;
                }
            }
            _disposed = true;
        }
        #endregion
    }
}