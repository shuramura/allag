﻿#region Using directives
using System;
using System.IO;
using System.Linq;

#endregion

namespace Allag.Core.Data.Queues
{
    public class BinaryMessageSerializer<TMessage> : IMessageSerializer<TMessage>
        where TMessage : class
    {
        #region Implementation of IMessageSerializer<TMessage>
        public Container<byte[]> Serialize(TMessage message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            Guid id = Guid.NewGuid();
            byte[] idData = id.ToByteArray();
            byte[] data;
            Stream stream = message as Stream;
            if (stream != null)
            {
                data = new byte[stream.Length];
                stream.Position = 0;
                stream.Read(data, 0, data.Length);
                stream.Position = 0;
            }
            else
            {
                data = GetData(message);
            }
            return new Container<byte[]>(id.ToString("D"), idData.Concat(data).ToArray());
        }

        public Container<TMessage> Deserialize(byte[] data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            byte[] idData = new byte[16];
            if (data.Length < idData.Length)
            {
                throw new ArgumentException("The minimum length of the data is 16.", nameof(data));
            }
            
            Array.Copy(data, idData, idData.Length);
            byte[] messageData = new byte[data.Length - idData.Length];
            Array.Copy(data, idData.Length, messageData, 0, messageData.Length);
            TMessage message;
            if (typeof(TMessage) == typeof(Stream))
            {
                message = new MemoryStream(messageData) as TMessage;
            }
            else
            {
                message = GetMessage(messageData);
            }

            return new Container<TMessage>(new Guid(idData).ToString("D"), message);
        }
        #endregion

        #region Methods
        protected virtual byte[] GetData(TMessage message)
        {
            throw new NotSupportedException();
        }

        protected virtual TMessage GetMessage(byte[] data)
        {
            throw new NotSupportedException();
        }
        #endregion
    }

    public class BinaryMessageSerializer : BinaryMessageSerializer<Stream>, IMessageSerializer{}
}