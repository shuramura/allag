﻿#region Using directives
using System;
using System.Configuration;
using System.IO;
using System.Messaging;
using System.Threading;
using System.Threading.Tasks;
using Allag.Core.Reflection;

#endregion

namespace Allag.Core.Data.Queues
{
    public class MsmqQueue<TMessage> : Queue<MessageQueue, TMessage>
        where TMessage : class
    {
        #region Constructors
        public MsmqQueue(Guid id, string connectionString) : base(id, new {ConnectionString = connectionString})
        {
            ReadTimeout = TimeSpan.FromSeconds(10);
        }
        #endregion

        #region Implementation of Queue<MessageQueue, TMessage>
        protected override Task<bool> IsActiveAsync(MessageQueue storage, CancellationToken token)
        {
            CheckIsDisposed();
            return Task.Run(() => storage.CanWrite, token);
        }

        protected override StorageInfo CreateStorageInitializer(dynamic args)
        {
            string strConnectionString = args.ConnectionString;
            if (string.IsNullOrWhiteSpace(strConnectionString))
            {
                throw new ArgumentNullOrEmptyException("connectionString");
            }
            MessageQueue messageQueue = new MessageQueue(ConfigurationManager.AppSettings[strConnectionString] ?? strConnectionString);
            if (!messageQueue.CanRead && !messageQueue.CanWrite)
            {
                throw new InvalidDataException($"The queue '{Id}' is not active.");
            }
            return new StorageInfo(() => messageQueue);
        }

        protected override Task PushAsync(MessageQueue storage, byte[] entity, CancellationToken token)
        {
            return Task.Run(() =>
            {
                using(MemoryStream stream = new MemoryStream(entity))
                {
                    using(Message container = new Message {BodyStream = stream})
                    {
                        container.UseAuthentication = storage.Authenticate;
                        storage.Send(container);
                    }
                }
            }, default(CancellationToken));
        }

        protected override Task<byte[]> PopAsync(MessageQueue storage, CancellationToken token)
        {
            return Task.Run(() =>
            {
                byte[] data = null;
                try
                {
                    using(Message message = storage.Receive(ReadTimeout))
                    {
                        if (message != null)
                        {
                            message.BodyStream.Position = 0;
                            data = new byte[message.BodyStream.Length];
                            message.BodyStream.Read(data, 0, data.Length);
                            message.BodyStream.Position = 0;
                        }
                    }
                }
                catch (MessageQueueException) {}
                return data;
            }, default(CancellationToken));
        }
        #endregion

        #region Properties
        public TimeSpan ReadTimeout { get; set; }
        #endregion
    }

    public class MsmqQueue : MsmqQueue<Stream>, IQueue
    {
        #region Constructors
        public MsmqQueue(Guid id, string connectionString) : base(id, connectionString)
        {
            Serializer = Serializer ?? ObjectFactory.Get<IMessageSerializer>(this);
        }
        #endregion
    }
}