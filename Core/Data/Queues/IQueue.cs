﻿#region Using directives
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

#endregion

namespace Allag.Core.Data.Queues
{
    public interface IQueue<TMessage> : IDisposable where TMessage : class
    {
        Guid Id { get; }
        Task<bool> IsActiveAsync(CancellationToken token = default(CancellationToken));
        Task<string> WriteAsync(TMessage message, CancellationToken token = default(CancellationToken));
        Task<Container<TMessage>> ReadAsync(CancellationToken token = default(CancellationToken));
    }

    public interface IQueue : IQueue<Stream> {}
}