﻿#region Using directives
using System;

#endregion

namespace Allag.Core.Data.Queues
{
    [Serializable]
    public class QueueException : ApplicationException
    {
        #region Constructors
        public QueueException() : this(Guid.Empty) {}
        public QueueException(Guid id, string message = null) : this(id, message, null) {}

        public QueueException(Guid id, string message, Exception innerException) : base(message, innerException)
        {
            Id = id;
        }
        #endregion

        #region Properties
        public Guid Id { get; private set; }
        #endregion
    }
}