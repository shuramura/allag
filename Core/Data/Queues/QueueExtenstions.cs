﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.Data.Queues
{
    public static class QueueExtenstions
    {
        #region Variables
        private static readonly IFormatter _formatter;
        #endregion

        #region Constructors
        static QueueExtenstions()
        {
            _formatter = new BinaryFormatter();
        }
        #endregion

        #region Static methods
        public static async Task MoveToAsync<TMessage>(this IQueue<TMessage> source, IEnumerable<IQueue<TMessage>> destinations, IResource localStorage, Func<string, Guid, string, bool> notify, CancellationToken token = default(CancellationToken)) where TMessage : class
        {
            IQueue<TMessage>[] queues = ValidateInput(source, destinations);

            Stream loacalStream = null;
            Func<Guid, Task> logger = id => Task.FromResult(0);
            if (localStorage != null)
            {
                loacalStream = localStorage.GetStream(false);
                logger = queueId =>
                {
                    _formatter.Serialize(loacalStream, queueId);
                    return loacalStream.FlushAsync();
                };
            }

            try
            {
                List<Guid> ids = new List<Guid>();
                using(Container<TMessage> result = await DeserializeAsync<TMessage>(loacalStream, ids))
                {
                    if (result != null)
                    {
                        await WriteAsync(result, queues.Where(x => !ids.Contains(x.Id)), logger, notify);
                    }
                }
                while (!token.IsCancellationRequested)
                {
                    try
                    {
                        foreach (IQueue<TMessage> queue in queues)
                        {
                            if (!await queue.IsActiveAsync(token))
                            {
                                throw new InactiveQueueException(queue.Id);
                            }
                        }

                        using(Container<TMessage> result = await source.ReadAsync(token))
                        {
                            await SerializeAsync(loacalStream, result);
                            if (!await WriteAsync(result, queues, logger, notify))
                            {
                                break;
                            }
                        }
                        loacalStream?.SetLength(0);
                    }
                    catch (OperationCanceledException) {}
                    catch (AggregateException exp)
                    {
                        if (!(exp.InnerException is TaskCanceledException))
                        {
                            throw;
                        }
                    }
                }
            }
            finally
            {
                loacalStream?.Dispose();
            }
        }

        private static async Task<bool> WriteAsync<TMessage>(Container<TMessage> container, IEnumerable<IQueue<TMessage>> queues, Func<Guid, Task> logger, Func<string, Guid, string, bool> notify) where TMessage : class
        {
            bool continueExecution = true;
            if (container != null)
            {
                foreach (IQueue<TMessage> queue in queues)
                {
                    string strNewId = await queue.WriteAsync(container.Item);
                    await logger(queue.Id);
                    if (notify != null)
                    {
                        continueExecution &= notify(container.Id, queue.Id, strNewId);
                    }
                }
            }

            return continueExecution;
        }

        private static Task SerializeAsync<TMessage>(Stream stream, Container<TMessage> container) where TMessage : class
        {
            Task ret = Task.FromResult(0);
            if (container != null && stream != null)
            {
                stream.SetLength(0);
                _formatter.Serialize(stream, container.Id);
                _formatter.Serialize(stream, container.Item);
                ret = stream.FlushAsync();
            }
            return ret;
        }

        private static Task<Container<TMessage>> DeserializeAsync<TMessage>(Stream stream, IList<Guid> ids) where TMessage : class
        {
            Container<TMessage> container = null;
            if (stream != null && stream.Length > 0)
            {
                stream.Position = 0;
                container = new Container<TMessage>((string)_formatter.Deserialize(stream), (TMessage)_formatter.Deserialize(stream));
                while (stream.Position != stream.Length)
                {
                    ids.Add((Guid)_formatter.Deserialize(stream));
                }
            }
            return Task.FromResult(container);
        }

        private static TQueue[] ValidateInput<TQueue>(TQueue source, IEnumerable<TQueue> destinations)
            where TQueue : class
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (destinations == null)
            {
                throw new ArgumentNullException(nameof(destinations));
            }

            TQueue[] queues = destinations.Where(x => x != null).ToArray();
            if (queues.Length == 0)
            {
                throw new ArgumentException("There is no any queue.", nameof(destinations));
            }
            return queues;
        }
        #endregion
    }
}