﻿#region Using directives
using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using Allag.Core.Reflection;

#endregion

namespace Allag.Core.Data.Queues
{
    public abstract class Queue<TStorage, TMessage, TData, TIMessageSerializer> : IQueue<TMessage>
        where TStorage : class
        where TMessage : class
        where TData : class
        where TIMessageSerializer : class, IMessageSerializer<TMessage, TData>
    {
        #region Variables
        private StorageInfo _storageInfo;
        #endregion

        #region Constructors
        protected Queue(Guid id, dynamic args)
        {
            using(LogBlock.New<Queue<TStorage, TMessage, TData, TIMessageSerializer>>(new {id}))
            {
                if (id == Guid.Empty)
                {
                    throw new ArgumentException("The empty value is wrong.", "id");
                }

                IsDisposed = false;
                Id = id;
                ReadDelay = TimeSpan.FromMinutes(1);
                Serializer = ObjectFactory.Get<TIMessageSerializer>(this);

                _storageInfo = CreateStorageInitializer(args);
                Task.WaitAll(RepositoryActionWrapper<int>(null));
                //Task<bool> task = IsActiveAsync();
                //task.Wait(5000);
                //if (!task.IsCompleted || !task.Result)
                //{
                //    throw new InvalidDataException(string.Format("Task result: '{0}'", task.IsCompleted));
                //}    
            }
        }

        [ExcludeFromCodeCoverage]
        ~Queue()
        {
            Dispose(false);
        }
        #endregion

        #region Implementation of IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation of IQueue<TMessage>
        public Guid Id { get; private set; }

        public Task<bool> IsActiveAsync(CancellationToken token = default(CancellationToken))
        {
            CheckIsDisposed();
            return RepositoryActionWrapper(storage => IsActiveAsync(storage, token));
        }

        public Task<string> WriteAsync(TMessage message, CancellationToken token = default(CancellationToken))
        {
            CheckIsDisposed();
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            return Task.Run(async () =>
            {
                using(Container<TData> container = Serializer.Serialize(message))
                {
                    return await RepositoryActionWrapper(async storage =>
                    {
                        await PushAsync(storage, container.Item, token);
                        return LogBlock.Action(GetType(), () => container.Id);
                    });
                }
            }, default(CancellationToken));
        }

        public async Task<Container<TMessage>> ReadAsync(CancellationToken token = default(CancellationToken))
        {
            CheckIsDisposed();

            return await Task.Run(async () =>
            {
                while (true)
                {
                    TData data = await RepositoryActionWrapper(storage => PopAsync(storage, token));
                    if (data != null)
                    {
                        Container<TMessage> result = Serializer.Deserialize(data);
                        if (result != null)
                        {
                            LogBlock.Action(GetType(), () => result.Id);
                            return result;
                        }
                    }

                    await Task.Delay(ReadDelay, token);
                }
            }, default(CancellationToken));
        }
        #endregion

        #region Properties
        public TIMessageSerializer Serializer { get; set; }
        public TimeSpan ReadDelay { get; set; }
        private bool IsDisposed { get; set; }
        #endregion

        #region Abstract methods
        protected abstract StorageInfo CreateStorageInitializer(dynamic args);
        protected abstract Task<bool> IsActiveAsync(TStorage storage, CancellationToken token);
        protected abstract Task PushAsync(TStorage storage, TData entity, CancellationToken token);
        protected abstract Task<TData> PopAsync(TStorage storage, CancellationToken token);
        #endregion

        #region Methods
        private async Task<TResult> RepositoryActionWrapper<TResult>(Func<TStorage, Task<TResult>> action)
        {
            TResult result = default(TResult);
            TStorage repository = _storageInfo.GetInstance();
            try
            {
                if (action != null)
                {
                    result = await action(repository);
                }
                return result;
            }
            finally
            {
                _storageInfo?.Callback?.Invoke(repository);
            }
        }

        private void Dispose(bool disposing)
        {
            if (!IsDisposed && disposing)
            {
                if (_storageInfo.Callback == null && typeof(IDisposable).IsAssignableFrom(typeof(TStorage)))
                {
                    IDisposable repository = (IDisposable)_storageInfo.GetInstance();
                    repository?.Dispose();
                }
                _storageInfo = null;
            }
            IsDisposed = true;
        }

        protected void CheckIsDisposed()
        {
            if (IsDisposed)
            {
                throw new ObjectDisposedException(GetType().FullName);
            }
        }
        #endregion

        #region Classes
        protected class StorageInfo
        {
            #region Variables
            private readonly Func<TStorage> _storageCreator;
            #endregion

            #region Constructors
            public StorageInfo(Func<TStorage> storageCreator, Action<TStorage> releaseCallback = null)
            {
                _storageCreator = storageCreator;
                Callback = releaseCallback;
            }
            #endregion

            #region Properties
            public Action<TStorage> Callback { get; }
            #endregion

            #region Methods
            public TStorage GetInstance()
            {
                return _storageCreator();
            }
            #endregion
        }
        #endregion
    }

    public abstract class Queue<TRepository, TMessage, TData> : Queue<TRepository, TMessage, TData, IMessageSerializer<TMessage, TData>>
        where TRepository : class
        where TMessage : class
        where TData : class
    {
        #region Constructors
        protected Queue(Guid id, object args) : base(id, args) {}
        #endregion
    }

    public abstract class Queue<TRepository, TMessage> : Queue<TRepository, TMessage, byte[], IMessageSerializer<TMessage>>
        where TRepository : class
        where TMessage : class
    {
        #region Constructors
        protected Queue(Guid id, object args) : base(id, args) {}
        #endregion
    }
}