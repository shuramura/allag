﻿#region Using directives
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Allag.Core.Reflection;

#endregion

namespace Allag.Core.Data.Queues
{
    public class MemoryQueue<TMessage> : Queue<MemoryStream, TMessage>
        where TMessage : class
    {
        #region Variables
        private long _nCurrentItem;
        private long _nEnd;
        private int _nCapacity;
        #endregion

        #region Constructors
        public MemoryQueue(Guid id) : base(id, new {})
        {
            _nCurrentItem = 0;
            _nEnd = 0;

            _nCapacity = 4096;
        }
        #endregion

        #region Implementation of Queue<MessageQueue, TMessage>
        protected override StorageInfo CreateStorageInitializer(dynamic args)
        {
            MemoryStream storage = new MemoryStream(Capacity);
            return new StorageInfo(() => storage, x =>
            {
                if (x.Length < Capacity)
                {
                    x.Capacity = Capacity;
                }
            });
        }

        protected override Task<bool> IsActiveAsync(MemoryStream storage, CancellationToken token)
        {
            return Task.FromResult(true);
        }

        protected override async Task PushAsync(MemoryStream storage, byte[] entity, CancellationToken token)
        {
            storage.Seek(_nEnd, SeekOrigin.Begin);
            byte[] size = BitConverter.GetBytes(entity.Length);
            await storage.WriteAsync(size, 0, size.Length, token);
            await storage.WriteAsync(entity, 0, entity.Length, token);
            _nEnd = storage.Position;
            if (storage.Position > Capacity)
            {
                await ShrinkAsync(storage, token);
            }
        }

        protected override async Task<byte[]> PopAsync(MemoryStream storage, CancellationToken token)
        {
            byte[] ret = null;
            if (_nCurrentItem < _nEnd)
            {
                storage.Seek(_nCurrentItem, SeekOrigin.Begin);

                byte[] size = new byte[sizeof(int)];
                await storage.ReadAsync(size, 0, size.Length, token);
                ret = new byte[BitConverter.ToInt32(size, 0)];
                await storage.ReadAsync(ret, 0, ret.Length, token);
                _nCurrentItem = storage.Position;
            }
            return ret;
        }
        #endregion

        #region Properties
        public int Capacity
        {
            get { return _nCapacity; }
            set
            {
                if (value > _nCapacity)
                {
                    _nCapacity = value;
                }
            }
        }
        #endregion

        #region Methods
        private async Task ShrinkAsync(Stream stream, CancellationToken token)
        {
            byte[] data = new byte[_nEnd - _nCurrentItem];
            stream.Seek(_nCurrentItem, SeekOrigin.Begin);
            await stream.ReadAsync(data, 0, data.Length, token);
            stream.Seek(0, SeekOrigin.Begin);
            await stream.WriteAsync(data, 0, data.Length, token);
            _nEnd = data.Length;
            _nCurrentItem = 0;
        }
        #endregion
    }

    public class MemoryQueue : MemoryQueue<Stream>, IQueue
    {
        #region Constructors
        public MemoryQueue(Guid id) : base(id)
        {
            Serializer = Serializer ?? ObjectFactory.Get<IMessageSerializer>(this);
        }
        #endregion
    }
}