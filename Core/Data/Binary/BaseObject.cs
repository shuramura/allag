﻿#region Using directives
using System;
using System.Text;

#endregion

namespace Allag.Core.Data.Binary
{
    public class BaseObject
    {
        #region Variables
        private readonly Encoding _encoding;
        #endregion

        #region Constructors
        protected BaseObject() : this(Encoding.ASCII) {}

        protected BaseObject(Encoding encoding)
        {
            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }
            _encoding = encoding;
        }
        #endregion

        #region Methods
        public virtual double DoubleFrom(byte[] value, string propertyName)
        {
            return BitConverter.ToDouble(value, 0);
        }

        public virtual byte[] DoubleTo(double value, string propertyName)
        {
            return BitConverter.GetBytes(value);
        }

        public virtual float SingleFrom(byte[] value, string propertyName)
        {
            return BitConverter.ToSingle(value, 0);
        }

        public virtual byte[] SingleTo(float value, string propertyName)
        {
            return BitConverter.GetBytes(value);
        }

        public virtual long Int64From(byte[] value, string propertyName)
        {
            return BitConverter.ToInt64(value, 0);
        }

        public virtual byte[] Int64To(long value, string propertyName)
        {
            return BitConverter.GetBytes(value);
        }

        public virtual int Int32From(byte[] value, string propertyName)
        {
            return BitConverter.ToInt32(value, 0);
        }

        public virtual byte[] Int32To(int value, string propertyName)
        {
            return BitConverter.GetBytes(value);
        }

        public virtual short Int16From(byte[] value, string propertyName)
        {
            return BitConverter.ToInt16(value, 0);
        }

        public virtual byte[] Int16To(short value, string propertyName)
        {
            return BitConverter.GetBytes(value);
        }

        public virtual bool BooleanFrom(byte[] value, string propertyName)
        {
            return BitConverter.ToBoolean(value, 0);
        }

        public virtual byte[] BooleanTo(bool value, string propertyName)
        {
            return BitConverter.GetBytes(value);
        }

        public virtual string StringFrom(byte[] value, string propertyName)
        {
            return _encoding.GetString(value);
        }

        public virtual byte[] StringTo(string value, string propertyName)
        {
            return _encoding.GetBytes(value);
        }
        #endregion
    }
}