﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

#endregion

namespace Allag.Core.Data.Binary
{
    public class Parser
    {
        #region Static methods
        public static byte[] Read(Stream stream, long offset, int size)
        {
            PositionStream(stream, offset);
            return Read(stream, size);
        }

        public static void Save(Stream stream, long offset, byte[] data, int size = -1)
        {
            PositionStream(stream, offset);
            Save(stream, data, size);
        }

        protected static byte[] Read(Stream stream, int size)
        {
            byte[] ret = new byte[size];
            stream.Read(ret, 0, ret.Length);
            return ret;
        }

        protected static void Save(Stream stream, byte[] data, int size)
        {
            if (size == -1)
            {
                size = data.Length;
            }
            Array.Resize(ref data, size);
            stream.Write(data, 0, data.Length);
        }

        protected static void PositionStream(Stream stream, long offset)
        {
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            if (offset < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(offset), offset, "The value should be equal or greater then zero.");
            }

            stream.Seek(offset, SeekOrigin.Begin);
        }
        #endregion
    }

    public class Parser<TItem> : Parser where TItem : class
    {
        #region Static methods
        public static TItem Load(Stream stream, long offset = 0, IObjectContainer<TItem> container = null)
        {
            PositionStream(stream, offset);

            ObjectHelper objectHelper = new ObjectHelper(typeof(TItem));
            TItem item = null;
            if (container != null)
            {
                item = container.Create(stream);
            }

            if (item == null)
            {
                item = (TItem)Activator.CreateInstance(typeof(TItem));
            }

            objectHelper.Object = item;
            container?.Loading(item, stream);

            foreach (DataParameter dataParameter in objectHelper.InputFields)
            {
                if (dataParameter.ParameterType.IsArray)
                {
                    List<byte[]> list = new List<byte[]>();
                    if (container != null)
                    {
                        Type itemType = dataParameter.ParameterType.GetElementType();

                        ObjectHelper itemObjectHelper = new ObjectHelper(itemType);
                        DataParameter[] parameters = itemObjectHelper.InputFields.ToArray();
                        int nObjectSize;
                        if (parameters.Length > 0)
                        {
                            nObjectSize = GetFieldSize(parameters);
                        }
                        else
                        {
                            nObjectSize = GetFieldSize(dataParameter);
                        }

                        int nLength = container.GetArrayLength(dataParameter, item, stream, offset);
                        for (int i = 0; i < nLength; i++)
                        {
                            list.Add(Read(stream, nObjectSize));
                        }
                    }
                    dataParameter.Value = list.ToArray();
                }
                else
                {
                    dataParameter.Value = Read(stream, GetFieldSize(dataParameter));
                }
            }

            if (container != null)
            {
                long nCurrentOffset = stream.Position;
                container.Loaded(item, stream, offset);
                if (stream.Position != nCurrentOffset)
                {
                    stream.Seek(nCurrentOffset, SeekOrigin.Begin);
                }
            }
            return item;
        }

        public static void Save(TItem item, Stream stream, long offset = 0, IObjectContainer<TItem> container = null)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            PositionStream(stream, offset);

            ObjectHelper objectHelper = new ObjectHelper(item);
            container?.Saving(item, stream);

            foreach (DataParameter dataParameter in objectHelper.OutputFields)
            {
                if (dataParameter.ParameterType.IsArray)
                {
                    IEnumerable<byte[]> enumerable;
                    if (container != null && (enumerable = (IEnumerable<byte[]>)dataParameter.Value) != null)
                    {
                        foreach (byte[] arrItem in enumerable)
                        {
                            Save(stream, arrItem, arrItem.Length);
                        }
                    }
                }
                else
                {
                    Save(stream, (byte[])dataParameter.Value, GetFieldSize(dataParameter));
                }
            }

            if (container != null)
            {
                long nCurrentOffset = stream.Position;
                container.Saved(item, stream, offset);
                if (stream.Position != nCurrentOffset)
                {
                    stream.Seek(nCurrentOffset, SeekOrigin.Begin);
                }
            }
        }

        private static int GetFieldSize(params DataParameter[] parameters)
        {
            int ret = 0;
            foreach (DataParameter parameter in parameters)
            {
                if (parameter.Size == -1)
                {
                    Type itemType = parameter.ParameterType;
                    if (itemType.IsArray)
                    {
                        itemType = itemType.GetElementType();
                    }
                    ret += Marshal.SizeOf(itemType);
                }
                else
                {
                    ret += parameter.Size;
                }
            }
            return ret;
        }
        #endregion
    }
}