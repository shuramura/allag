﻿#region Using directives
using System.Data;
using System.IO;

#endregion

namespace Allag.Core.Data.Binary
{
    public interface IObjectContainer<TItem> where TItem : class
    {
        TItem Create(Stream stream);
        int GetArrayLength(IDbDataParameter dataParameter, TItem owner, Stream stream, long offset);

        void Loading(TItem item, Stream stream);
        void Loaded(TItem item, Stream stream, long offset);

        void Saving(TItem item, Stream stream);
        void Saved(TItem item, Stream stream, long offset);
    }
}