#region Using directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using Allag.Core.Data.Attributes;
using Allag.Core.Reflection;

#endregion

namespace Allag.Core.Data
{
    public class FieldDescription
    {
        #region Variables
        private readonly MemberInfo _parameterInfo;
        private readonly List<ConvertAttribute> _convertAttributes;
        private readonly List<ConfigurationValidatorAttribute> _validatorAttributes;
        private readonly Func<object, object> _getHandler;
        private readonly Action<object, object> _setHandler;
        #endregion

        #region Constructors
        public FieldDescription(MemberInfo parameterInfo, FieldAttribute parameterAttribute)
        {
            if (parameterInfo == null)
            {
                throw new ArgumentNullException(nameof(parameterInfo));
            }

            if (parameterAttribute == null)
            {
                throw new ArgumentNullException(nameof(parameterAttribute));
            }

            _getHandler = null;
            _setHandler = null;

            _parameterInfo = parameterInfo;
            FieldAttribute = parameterAttribute;

            switch (parameterInfo.MemberType)
            {
                case MemberTypes.Property:
                    ParameterType = ((PropertyInfo)parameterInfo).PropertyType;
                    break;
                case MemberTypes.Field:
                    ParameterType = ((FieldInfo)parameterInfo).FieldType;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(parameterInfo), parameterInfo.MemberType, "The parameter should be property or field.");
            }

            if (parameterAttribute.Direction == ParameterDirection.InputOutput ||
                parameterAttribute.Direction == ParameterDirection.Input)
            {
                _setHandler = parameterInfo.CreateSetHandler<FieldDescription>();
            }

            if (parameterAttribute.Direction == ParameterDirection.InputOutput ||
                parameterAttribute.Direction == ParameterDirection.Output)
            {
                _getHandler = parameterInfo.CreateGetHandler<FieldDescription>();
            }

            _convertAttributes = new List<ConvertAttribute>();
            _parameterInfo.HandleAttribute(delegate(ConvertAttribute attribute)
                                               {
                                                   _convertAttributes.Add(attribute);
                                                   return true;
                                               });

            Type parameterType = ParameterType;
            if (parameterType.IsArray && !FieldAttribute.IsSimpleField)
            {
                parameterType = parameterType.GetElementType();
            }
            if (_convertAttributes.Count == 0)
            {
                _convertAttributes.Add(new ConvertAttribute(parameterType));
            }
            else
            {
                _convertAttributes = _convertAttributes.OrderBy(x => x.Order).ToList();
            }

            int i = 0;
            while (i < _convertAttributes.Count - 1)
            {
                _convertAttributes[i].Initialize(_parameterInfo.DeclaringType, _parameterInfo.Name, _convertAttributes[++i].ToType);
            }
            _convertAttributes[i].Initialize(_parameterInfo.DeclaringType, _parameterInfo.Name, parameterType);

            _validatorAttributes = new List<ConfigurationValidatorAttribute>();
            _parameterInfo.HandleAttribute(delegate(ConfigurationValidatorAttribute attribute)
                                               {
                                                   _validatorAttributes.Add(attribute);
                                                   return true;
                                               });
        }
        #endregion

        #region Properties
        public FieldAttribute FieldAttribute { get; }

        public string ParameterName => FieldAttribute.Name ?? _parameterInfo.Name;

        public Type ParameterType { get; }

        public ParameterDirection Direction
        {
            get { return FieldAttribute.Direction; }
            set { FieldAttribute.Direction = value; }
        }
        #endregion

        #region Methods
        public void SetOriginalValue(object @object, object value)
        {
            if (_setHandler == null)
            {
                throw new InvalidOperationException($"The member '{_parameterInfo.Name}' of the type '{_parameterInfo.DeclaringType}' does not support set operation.");
            }

            foreach (ConfigurationValidatorAttribute validatorAttribute in _validatorAttributes)
            {
                if (validatorAttribute.ValidatorInstance.CanValidate(ParameterType))
                {
                    validatorAttribute.ValidatorInstance.Validate(value);
                }
            }

            _setHandler(@object, value);
        }

        public object GetOriginalValue(object @object)
        {
            if (_getHandler == null)
            {
                throw new InvalidOperationException($"The member '{_parameterInfo.Name}' of the type '{_parameterInfo.DeclaringType}' does not support get operation.");
            }
            return _getHandler(@object);
        }

        public void SetValue(object @object, object value)
        {
            if (value != null)
            {
                Array array;
                if (!FieldAttribute.IsSimpleField && ParameterType.IsArray && (array = value as Array) != null)
                {
                    ArrayList outArray = new ArrayList();
                    foreach (object item in array)
                    {
                        object temp = item;
                        foreach (ConvertAttribute convertAttribute in _convertAttributes)
                        {
                            temp = convertAttribute.ConvertFrom(@object, temp);
                        }
                        outArray.Add(temp);
                    }
                    value = outArray.ToArray(ParameterType.GetElementType());
                }
                else
                {
                    foreach (ConvertAttribute convertAttribute in _convertAttributes)
                    {
                        value = convertAttribute.ConvertFrom(@object, value);
                    }
                }
            }
            SetOriginalValue(@object, value);
        }

        public object GetValue(object @object)
        {
            object value = GetOriginalValue(@object);
            if (value != null)
            {
                Array array;
                if (!FieldAttribute.IsSimpleField && ParameterType.IsArray && (array = value as Array) != null)
                {
                    ArrayList outArray = new ArrayList();
                    foreach (object item in array)
                    {
                        object temp = item;
                        for (int i = _convertAttributes.Count - 1; i >= 0; i--)
                        {
                            temp = _convertAttributes[i].ConvertTo(@object, temp);
                        }

                        outArray.Add(temp);
                    }
                    value = outArray.ToArray(_convertAttributes[0].ToType);
                }
                else
                {
                    for (int i = _convertAttributes.Count - 1; i >= 0; i--)
                    {
                        value = _convertAttributes[i].ConvertTo(@object, value);
                    }
                }
            }
            return value;
        }
        #endregion
    }
}