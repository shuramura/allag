#region Using directives
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using Allag.Core.Configuration;

#endregion

namespace Allag.Core
{
    public static class Application
    {
        #region Variables
        private static Type _root;
        #endregion

        #region Constructors
        static Application()
        {
            AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
        }
        #endregion

        #region Property
        public static Type Root
        {
            get { return _root; }
            set
            {
                if (_root == null && value != null)
                {
                    _root = value;
                    AssemblyFileVersionAttribute attr =
                        (AssemblyFileVersionAttribute)
                            _root.Assembly.GetCustomAttributes(typeof(AssemblyFileVersionAttribute), false)
                                .FirstOrDefault();
                    ProductVersion = attr != null ? attr.Version : string.Empty;
                }
            }
        }

        public static string ProductVersion { get; private set; }

        public static string Base => CoreCfg.General.ApplicationBase;
        #endregion

        #region Methods
        [ExcludeFromCodeCoverage]
        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            using(LogBlock.New(typeof(Application), new {sender, e}))
            {
                Exception exception = e.ExceptionObject as Exception;
                if (exception != null)
                {
                    LogBlock.Logger.FatalError(exception);
                }
                else
                {
                    LogBlock.Logger.FatalError(e.ExceptionObject?.ToString(), null);
                }
            }
        }
        #endregion
    }
}