﻿#region Using directives
using System;

#endregion

namespace Allag.Core
{
    public interface ILogger : IDisposable
    {
        ILogger GetLogger(Type type);
        void Debug(string format, params object[] args);
        void Info(string format, params object[] args);
        void Warning(string format, Exception exception, params object[] args);
        void Warning(string format, params object[] args);
        void Error(string format, Exception exception, params object[] args);
        void FatalError(string format, Exception exception, params object[] args);
        void FatalError(Exception exception);
    }
}