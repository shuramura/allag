#region Using directives
using System;
using System.Runtime.InteropServices;

#endregion

namespace Allag.Core.Native
{
    internal static class NativeMethods
    {
        #region External methods
        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern int QueryServiceConfig(IntPtr handle, IntPtr queryServiceConfig, int bufferSize, ref int bytesNeeded);
        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern int ChangeServiceConfig(IntPtr handle, uint serviceType, uint startType, uint errorControl, 
            [MarshalAs(UnmanagedType.LPTStr)]string binaryPathName, [MarshalAs(UnmanagedType.LPTStr)] string loadOrderGroup,
            IntPtr tagId, [MarshalAs(UnmanagedType.LPTStr)] string dependencies,[MarshalAs(UnmanagedType.LPTStr)] string startName,
            [MarshalAs(UnmanagedType.LPTStr)] string password, [MarshalAs(UnmanagedType.LPTStr)]string displayName);
        #endregion
    }
}
