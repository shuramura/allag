#region Using directives
using System;
using System.Runtime.InteropServices;

#endregion

namespace Allag.Core.Native
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct ServiceConfig
    {
        #region Variables
        public uint ServiceType;
        public uint StartType;
        public uint ErrorControl;
        public IntPtr BinaryPathName;
        public IntPtr LoadOrderGroup;
        public uint TagId;
        public IntPtr Dependencies;
        public IntPtr StartName;
        public IntPtr DisplayName;
        #endregion
    }
}