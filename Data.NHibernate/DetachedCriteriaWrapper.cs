﻿#region Using directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;

#endregion

namespace Allag.Data
{
    [ExcludeFromCodeCoverage]
    public class DetachedCriteriaWrapper : ICriteria
    {
        #region Constructors
        public DetachedCriteriaWrapper(DetachedCriteria detachedCriteria)
        {
            if (detachedCriteria == null)
            {
                throw new ArgumentNullException("detachedCriteria");
            }

            Original = detachedCriteria;
        }
        #endregion

        #region Implementation of ICriteria
        public object Clone()
        {
            throw new NotSupportedException();
        }

        public ICriteria SetProjection(params IProjection[] projections)
        {
            foreach (IProjection projection in projections)
            {
                Original.SetProjection(projection);
            }
            return this;
        }

        public ICriteria Add(ICriterion expression)
        {
            Original.Add(expression);
            return this;
        }

        public ICriteria AddOrder(Order order)
        {
            Original.AddOrder(order);
            return this;
        }

        public ICriteria SetFetchMode(string associationPath, FetchMode mode)
        {
            Original.SetFetchMode(associationPath, mode);
            return this;
        }

        public ICriteria SetLockMode(LockMode lockMode)
        {
            Original.SetLockMode(lockMode);
            return this;
        }

        public ICriteria SetLockMode(string alias, LockMode lockMode)
        {
            Original.SetLockMode(alias, lockMode);
            return this;
        }

        public ICriteria CreateAlias(string associationPath, string alias)
        {
            Original.CreateAlias(associationPath, alias);
            return this;
        }

        public ICriteria CreateAlias(string associationPath, string alias, JoinType joinType)
        {
            Original.CreateAlias(associationPath, alias, joinType);
            return this;
        }

        public ICriteria CreateAlias(string associationPath, string alias, JoinType joinType, ICriterion withClause)
        {
            Original.CreateAlias(associationPath, alias, joinType, withClause);
            return this;
        }

        public ICriteria CreateCriteria(string associationPath)
        {
            Original.CreateCriteria(associationPath);
            return this;
        }

        public ICriteria CreateCriteria(string associationPath, JoinType joinType)
        {
            Original.CreateCriteria(associationPath, joinType);
            return this;
        }

        public ICriteria CreateCriteria(string associationPath, string alias)
        {
            Original.CreateCriteria(associationPath, alias);
            return this;
        }

        public ICriteria CreateCriteria(string associationPath, string alias, JoinType joinType)
        {
            Original.CreateCriteria(associationPath, alias, joinType);
            return this;
        }

        public ICriteria CreateCriteria(string associationPath, string alias, JoinType joinType, ICriterion withClause)
        {
            Original.CreateCriteria(associationPath, alias, joinType, withClause);
            return this;
        }

        public ICriteria SetResultTransformer(IResultTransformer resultTransformer)
        {
            Original.SetResultTransformer(resultTransformer);
            return this;
        }

        public ICriteria SetMaxResults(int maxResults)
        {
            Original.SetMaxResults(maxResults);
            return this;
        }

        public ICriteria SetFirstResult(int firstResult)
        {
            Original.SetFirstResult(firstResult);
            return this;
        }

        public ICriteria SetFetchSize(int fetchSize)
        {
            return this;
        }

        public ICriteria SetTimeout(int timeout)
        {
            return this;
        }

        public ICriteria SetCacheable(bool cacheable)
        {
            Original.SetCacheable(cacheable);
            return this;
        }

        public ICriteria SetCacheRegion(string cacheRegion)
        {
            Original.SetCacheRegion(cacheRegion);
            return this;
        }

        public ICriteria SetComment(string comment)
        {
            return this;
        }

        public ICriteria SetFlushMode(FlushMode flushMode)
        {
            return this;
        }

        public ICriteria SetCacheMode(CacheMode cacheMode)
        {
            Original.SetCacheMode(cacheMode);
            return this;
        }

        public IList List()
        {
            throw new NotSupportedException();
        }

        public object UniqueResult()
        {
            throw new NotSupportedException();
        }

        public IEnumerable<T> Future<T>()
        {
            throw new NotSupportedException();
        }

        public IFutureValue<T> FutureValue<T>()
        {
            throw new NotSupportedException();
        }

        public ICriteria SetReadOnly(bool readOnly)
        {
            return this;
        }

        public void List(IList results)
        {
            throw new NotSupportedException();
        }

        public IList<T> List<T>()
        {
            throw new NotSupportedException();
        }

        public T UniqueResult<T>()
        {
            throw new NotSupportedException();
        }

        public void ClearOrders()
        {
            Original.ClearOrders();
        }

        public ICriteria GetCriteriaByPath(string path)
        {
            return new DetachedCriteriaWrapper(Original.GetCriteriaByPath(path));
        }

        public ICriteria GetCriteriaByAlias(string alias)
        {
            return new DetachedCriteriaWrapper(Original.GetCriteriaByAlias(alias));
        }

        public System.Type GetRootEntityTypeIfAvailable()
        {
            return Original.GetRootEntityTypeIfAvailable();
        }

        public string Alias
        {
            get { return Original.Alias; }
        }

        public bool IsReadOnlyInitialized
        {
            get { return false; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }
        #endregion

        #region Properties
        public DetachedCriteria Original { get; private set; }
        #endregion
    }
}