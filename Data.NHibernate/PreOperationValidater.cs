﻿#region Using directives
using Allag.Core.Data.Persistence;
using NHibernate.Event;

#endregion

namespace Allag.Data
{
    public class PreOperationValidater : IPreInsertEventListener, IPreUpdateEventListener, IPreDeleteEventListener
    {
        #region Implementation of IPreInsertEventListener
        public bool OnPreInsert(PreInsertEvent @event)
        {
            IEntityPreValidater validater = @event.Entity as IEntityPreValidater;
            if(validater != null)
            {
                validater.Validate(Operations.Add);
            }
            return false;
        }
        #endregion

        #region Implementation of IPreUpdateEventListener
        public bool OnPreUpdate(PreUpdateEvent @event)
        {
            IEntityPreValidater validater = @event.Entity as IEntityPreValidater;
            if(validater != null)
            {
                validater.Validate(Operations.Update);
            }
            return false;
        }
        #endregion

        #region Implementation of IPreDeleteEventListener
        public bool OnPreDelete(PreDeleteEvent @event)
        {
            IEntityPreValidater validater = @event.Entity as IEntityPreValidater;
            if(validater != null)
            {
                validater.Validate(Operations.Delete);
            }
            return false;
        }
        #endregion
    }
}