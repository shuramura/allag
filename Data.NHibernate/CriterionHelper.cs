#region Using directives
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using Allag.Core.Data.Persistence;
using Allag.Core.Data.Persistence.Helper;
using Allag.Core.Reflection;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Metadata;
using NHibernate.SqlCommand;

#endregion

namespace Allag.Data
{
    public static class CriterionHelper
    {
        #region Constants
        private const char PREFIX_SEPARATOR = '_';
        private const char ALIASE_SEPARATOR = '.';
        #endregion

        #region Variables
        private static readonly ConcurrentDictionary<System.Type, ICriteriaBuilder> _criteriaBuilders;
        #endregion

        #region Constructors
        static CriterionHelper()
        {
            _criteriaBuilders = new ConcurrentDictionary<System.Type, ICriteriaBuilder>();
        }
        #endregion

        #region Methods
        public static ICriteria CreateCriteria<TEntity>(this Session session, params Expression<Func<TEntity, bool>>[] criteria)
            where TEntity : class
        {
            return session.CreateCriteria<TEntity, LambdaExpression>(new List<Condition>(), criteria.ToArray<LambdaExpression>());
        }

        public static ICriteria CreateCriteria<TEntity, TCriteria>(this Session session, ICollection<Condition> projections, params TCriteria[] criteria)
            where TEntity : class
            where TCriteria : class
        {
            if (session == null)
            {
                throw new ArgumentNullException("session");
            }

            ICriteria ret = session.CreateCriteria<TEntity>();
            if (criteria != null && criteria.Length > 0)
            {
                ret = PopulateCriteria(session, ret, projections, criteria.Where(x => x != null).Select(x =>
                {
                    ICriteriaBuilder criteriaBuilder = _criteriaBuilders.GetOrAdd(typeof(TCriteria), type =>
                    {
                        ICriteriaBuilder builder = ObjectFactory.Get<ICriteriaBuilder>(x);
                        if (builder == null && type == typeof(LambdaExpression))
                        {
                            builder = new ExpressionCriteriaBuilder();
                        }
                        return builder;
                    });
                    ConditionList conditionList = null;
                    if (criteriaBuilder != null)
                    {
                        conditionList = criteriaBuilder.Build(typeof(TEntity), x);
                        conditionList = !conditionList.Groups.Any() || (conditionList.Count + conditionList.Groups.Count() > 1) ? conditionList : conditionList.Groups.First();
                    }

                    return conditionList;
                }).ToArray());
            }
            return ret;
        }

        private static ICriteria PopulateCriteria(Session session, ICriteria criteria, ICollection<Condition> projections, params ConditionList[] lists)
        {
            if (lists != null && lists.Length > 0)
            {
                Junction junction = Restrictions.Conjunction();
                IDictionary<string, string> aliases = new Dictionary<string, string>(20);
                IList<Tuple<Condition, IProjection>> projectedConditions = new List<Tuple<Condition, IProjection>>();

                IDictionary<Operation, object> parameters = new Dictionary<Operation, object>
                {
                    {Operation.FirstResult, null},
                    {Operation.MaxResults, null},
                    {Operation.GroupBy, false},
                    {Operation.Distinct, false}
                };

                IDictionary<Operation, Action<ICriteria, object>> parameterSetters = new Dictionary<Operation, Action<ICriteria, object>>
                {
                    {Operation.FirstResult, (creteria, value) => creteria.SetFirstResult((int)value)},
                    {Operation.MaxResults, (creteria, value) => creteria.SetMaxResults((int)value)}
                };

                foreach (ConditionList list in lists)
                {
                    if (list != null)
                    {
                        junction.Add(GenerateJunction(session, criteria, list, aliases, projectedConditions, parameters));
                    }
                }
                criteria = criteria.Add(junction);
                foreach (KeyValuePair<string, string> pair in aliases)
                {
                    criteria = criteria.CreateAlias(pair.Key, pair.Value, JoinType.LeftOuterJoin);
                }
               
                List<IProjection> selectedProjections = new List<IProjection>(projectedConditions.Count);
                ProjectionList distinctList = Projections.ProjectionList();
                bool isGroupBy = (bool)parameters[Operation.GroupBy];
                bool isDistinct = (bool)parameters[Operation.Distinct];

                foreach (Tuple<Condition, IProjection> pair in projectedConditions)
                {
                    switch (pair.Item1.Op)
                    {
                        case Operation.Minimum:
                            selectedProjections.Add(Projections.Min(pair.Item2));
                            break;
                        case Operation.Maximum:
                            selectedProjections.Add(Projections.Max(pair.Item2));
                            break;
                        case Operation.Average:
                            selectedProjections.Add(Projections.Avg(pair.Item2));
                            break;
                        case Operation.Sum:
                            selectedProjections.Add(Projections.Sum(pair.Item2));
                            break;
                        case Operation.Count:
                            selectedProjections.Add(Projections.Count(pair.Item2));
                            break;
                        default:
                            if (isGroupBy || (isDistinct && projections == null))
                            {
                                selectedProjections.Add(Projections.GroupProperty(pair.Item2));
                            }
                            else if (isDistinct)
                            {
                                distinctList.Add(pair.Item2);
                            }
                            else
                            {
                                selectedProjections.Add(pair.Item2);
                            }

                            break;
                    }
                    if (projections != null)
                    {
                        projections.Add(pair.Item1);
                    }
                }

                if (distinctList.Length > 0)
                {
                    selectedProjections.Add(Projections.Distinct(distinctList));
                }
                if (selectedProjections.Count > 0)
                {
                    criteria = criteria.SetProjection(selectedProjections.ToArray());
                }

                if (projections != null)
                {
                    foreach (KeyValuePair<Operation, Action<ICriteria, object>> pair in parameterSetters)
                    {
                        object value = parameters[pair.Key];
                        if (value != null)
                        {
                            pair.Value(criteria, value);
                        }
                    }
                }
            }
            return criteria;
        }

        private static ICriterion GenerateJunction(Session session, ICriteria criteria, ConditionList list, IDictionary<string, string> aliases, IList<Tuple<Condition, IProjection>> projections, IDictionary<Operation, object> parameters)
        {
            Junction junction;
            if (list.IsAndGroup)
            {
                junction = Restrictions.Conjunction();
            }
            else
            {
                junction = Restrictions.Disjunction();
            }

            foreach (Condition condition in list)
            {
                string strPrefix;
                string strName = GetConditionName(condition, out strPrefix);

                ICriterion criterion = null;
                ConditionList subList = condition.Value as ConditionList;
                DetachedCriteriaWrapper wrapper = null;
                if (subList != null)
                {
                    wrapper = new DetachedCriteriaWrapper(DetachedCriteria.For(subList.EntityType));
                    ICollection<Condition> subProjections = new Collection<Condition>();
                    PopulateCriteria(session, wrapper, subProjections, subList);
                    if (subProjections.Count == 0)
                    {
                        string strSubIdName = GetEntityIdName(session, string.Empty, subList.EntityType);
                        wrapper.SetProjection(Projections.Distinct(Projections.Alias(Projections.Property(strSubIdName), strSubIdName)));
                    }
                }
                switch (condition.Op)
                {
                    case Operation.Equal:
                        if (condition.Value == null)
                        {
                            criterion = Restrictions.IsNull(strName);
                        }
                        else if (wrapper != null)
                        {
                            criterion = Subqueries.PropertyEq(strName, wrapper.Original);
                        }
                        else
                        {
                            criterion = Restrictions.Eq(strName, condition.Value);
                        }
                        break;
                    case Operation.NotEqual:
                        if (condition.Value == null)
                        {
                            criterion = Restrictions.IsNotNull(strName);
                        }
                        else if (wrapper != null)
                        {
                            criterion = Restrictions.Not(Subqueries.PropertyEq(strName, wrapper.Original));
                        }
                        else
                        {
                            criterion = Restrictions.Not(Restrictions.Eq(strName, condition.Value));
                        }
                        break;
                    case Operation.GreaterThanOrEqual:
                        if (wrapper != null)
                        {
                            criterion = Subqueries.PropertyGe(strName, wrapper.Original);
                        }
                        else
                        {
                            criterion = Restrictions.Ge(strName, condition.Value);
                        }
                        break;
                    case Operation.GreaterThan:
                        if (wrapper != null)
                        {
                            criterion = Subqueries.PropertyGt(strName, wrapper.Original);
                        }
                        else
                        {
                            criterion = Restrictions.Gt(strName, condition.Value);
                        }
                        break;
                    case Operation.LessThan:
                        if (wrapper != null)
                        {
                            criterion = Subqueries.PropertyLt(strName, wrapper.Original);
                        }
                        else
                        {
                            criterion = Restrictions.Lt(strName, condition.Value);
                        }
                        break;
                    case Operation.LessThanOrEqual:
                        if (wrapper != null)
                        {
                            criterion = Subqueries.PropertyLe(strName, wrapper.Original);
                        }
                        else
                        {
                            criterion = Restrictions.Le(strName, condition.Value);
                        }
                        break;
                    case Operation.Between:
                        criterion = Restrictions.Between(strName, condition.Value, condition.Values.Skip(1).FirstOrDefault());
                        break;
                    case Operation.Like:
                        criterion = Restrictions.Like(strName, condition.Value);
                        break;
                    case Operation.In:
                        if (wrapper != null)
                        {
                            criterion = Subqueries.PropertyIn(strName, wrapper.Original);
                        }
                        else
                        {
                            criterion = Restrictions.In(strName, ((IEnumerable)condition.Value).Cast<object>().ToArray());
                        }

                        break;
                    case Operation.Contains:
                        strPrefix = strName.Replace(ALIASE_SEPARATOR, PREFIX_SEPARATOR);
                        if (wrapper != null)
                        {
                            criterion = Subqueries.PropertyIn(GetEntityIdName(session, strPrefix, subList.EntityType), wrapper.Original);
                        }
                        else
                        {
                            criterion = GetCollectionContainsCriterion(session, strPrefix, list.EntityType, condition.Value);
                        }
                        break;
                    case Operation.IsEmpty:
                        criterion = Restrictions.IsEmpty(strName);
                        break;
                    case Operation.Constant:
                        criterion = Restrictions.Eq(Projections.Constant(true), condition.Value != null && (bool)condition.Value);
                        break;
                    case Operation.Order:
                        criteria.AddOrder((condition.Value != null && (bool)condition.Value) ? Order.Asc(strName) : Order.Desc(strName));
                        break;
                    case Operation.Distinct:
                    case Operation.GroupBy:
                        parameters[condition.Op] = true;
                        break;
                    case Operation.Property:
                    case Operation.Minimum:
                    case Operation.Maximum:
                    case Operation.Average:
                    case Operation.Sum:
                    case Operation.Count:
                        projections.Add(new Tuple<Condition, IProjection>(condition, (Projections.Property(strName))));
                        break;
                    case Operation.FirstResult:
                    case Operation.MaxResults:
                        parameters[condition.Op] = condition.Value;
                        break;
                }

                if (criterion != null)
                {
                    if (condition.IsNot)
                    {
                        criterion = Restrictions.Not(criterion);
                    }

                    junction.Add(criterion);
                }

                AddAliase(condition.Name.Item2, strPrefix, strName, aliases);
            }

            foreach (ConditionList conditionList in list.Groups)
            {
                junction.Add(GenerateJunction(session, criteria, conditionList, aliases, projections, parameters));
            }

            return list.IsNot ? Restrictions.Not(junction) : junction;
        }

        private static void AddAliase(System.Type type, string prefix, string name, IDictionary<string, string> aliases)
        {
            if (prefix.Length > 0 && name.Length > 0 && typeof(IEntity).IsAssignableFrom(type))
            {
                string strAlias = prefix.Replace(PREFIX_SEPARATOR, ALIASE_SEPARATOR);
                if (!aliases.ContainsKey(strAlias))
                {
                    aliases.Add(strAlias, prefix);
                }
            }
        }

        private static string GetConditionName(Condition condition, out string prefix)
        {
            prefix = string.Empty;
            string strName = condition.Name.Item1 ?? string.Empty;
            if (strName.Length > 0)
            {
                prefix = condition.NamePrefixes.Aggregate(string.Empty, (current, next) => current + (next + PREFIX_SEPARATOR));
                if (prefix.Length > 0)
                {
                    prefix = prefix.TrimEnd(PREFIX_SEPARATOR);
                    strName = prefix + ALIASE_SEPARATOR + strName;
                }
            }
            return strName;
        }

        private static string GetEntityIdName(Session session, string prefix, System.Type entityType)
        {
            IClassMetadata metadata = session.SessionFactory.GetClassMetadata(entityType);
            string ret = metadata.IdentifierPropertyName;

            if (prefix.Length > 0)
            {
                ret = prefix + ALIASE_SEPARATOR + ret;
            }
            return ret;
        }

        private static ICriterion GetCollectionContainsCriterion(Session session, string prefix, System.Type entityType, object value)
        {
            ICriterion ret;
            string strEntityIdName = GetEntityIdName(session, prefix, entityType);
            object[] items = ((IEnumerable)value).Cast<object>().ToArray();
            if (items.Length == 0)
            {
                ret = Restrictions.IsNull(strEntityIdName);
            }
            else
            {
                Junction junction = null;
                List<object> resultValues = new List<object>(items.Length);
                foreach (var item in items)
                {
                    if (item != null)
                    {
                        System.Type itemType = item.GetType();
                        if (typeof(IEntity).IsAssignableFrom(itemType))
                        {
                            resultValues.Add(session.SessionFactory.GetClassMetadata(itemType).GetIdentifier(item, EntityMode.Poco));
                        }
                        else
                        {
                            resultValues.Add(item);
                        }
                    }
                    else if (junction == null)
                    {
                        junction = Restrictions.Disjunction();
                        junction.Add(Restrictions.IsNull(strEntityIdName));
                    }
                }
                if (junction != null)
                {
                    ret = junction.Add(Restrictions.In(strEntityIdName, resultValues));
                }
                else
                {
                    ret = Restrictions.In(strEntityIdName, resultValues);
                }
            }
          
            return ret;
        }
        #endregion
    }
}