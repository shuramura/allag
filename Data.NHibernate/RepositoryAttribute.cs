#region Using directives
using System;

#endregion

namespace Allag.Data
{
    public class RepositoryAttribute : Attribute
    {
        #region Constructors
        public RepositoryAttribute() { }

        public RepositoryAttribute(System.Type entityType)
        {
            Entity = entityType;
        }
        #endregion

        #region Properties
        public System.Type Factory { get; set; }
        public System.Type Entity { get; set; }
        #endregion
    }
}