﻿#region Using directives
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Allag.Core.Data.Persistence;
using Allag.Data.Configuration;

#endregion

namespace Allag.Data
{
    public class RepositoryFactory : IRepositoryFactory
    {
        #region Variables
        private static readonly Dictionary<System.Type, System.Type> _entityRepositories;
        private static readonly Dictionary<System.Type, System.Type> _defaultRepositories;

        private readonly ConcurrentDictionary<System.Type, Repository> _repositories;
        #endregion

        #region Constructors
        static RepositoryFactory()
        {
            _entityRepositories = new Dictionary<System.Type, System.Type>();
            _defaultRepositories = new Dictionary<System.Type, System.Type>();

            foreach (SessionElement item in DataNHibernateConfiguration.Sessions.Items)
            {
                System.Type[] types = item.Type.Assembly.GetTypes();
                foreach (System.Type type in types)
                {
                    if (typeof(IRepository).IsAssignableFrom(type))
                    {
                        System.Type tempType = type;
                        RepositoryAttribute repositoryAttr = type.GetCustomAttribute<RepositoryAttribute>();
                        if (repositoryAttr != null)
                        {
                            if (repositoryAttr.Factory != null && !_defaultRepositories.ContainsKey(repositoryAttr.Factory))
                            {
                                _defaultRepositories.Add(repositoryAttr.Factory, type);
                            }
                            if (repositoryAttr.Entity != null && !_entityRepositories.ContainsKey(repositoryAttr.Entity))
                            {
                                _entityRepositories.Add(repositoryAttr.Entity, tempType);
                            }
                        }
                    }
                }
            }
        }

        protected RepositoryFactory()
        {
            _repositories = new ConcurrentDictionary<System.Type, Repository>();
        }
        #endregion

        #region Implementation of IRepositoryFactory
        public IUnitOfWork UnitOfWork
        {
            get { return InternalUnitOfWork; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }

                UnitOfWork unitOfWork = value as UnitOfWork;
                if (unitOfWork == null)
                {
                    throw new ArgumentException(string.Format("The type '{0}' is expected.", typeof(UnitOfWork)));
                }

                InternalUnitOfWork = unitOfWork;
            }
        }

        IRepository IRepositoryFactory.Get<TEntity>(params System.Type[] types)
        {
            return Get(typeof(TEntity), types);
        }

        public virtual IRepository Get(System.Type entityType, params System.Type[] types)
        {
            if (entityType == null)
            {
                throw new ArgumentNullException("entityType");
            }
            return _repositories.GetOrAdd(entityType, type =>
            {
                System.Type repositoryType;
                _entityRepositories.TryGetValue(type, out repositoryType);
                return CreateRepository(repositoryType, type, types);
            });
        }
        #endregion

        #region Properties
        protected UnitOfWork InternalUnitOfWork { get; private set; }
        #endregion

        #region Methods
        private Repository CreateRepository(System.Type repositoryType, System.Type entityType, params System.Type[] types)
        {
            repositoryType = repositoryType ?? GetDefaultRepository(typeof(IRepositoryFactory), entityType, types);
            Repository repository = null;
            if (repositoryType != null)
            {
                repository = (Repository)Activator.CreateInstance(repositoryType);
                repository.UnitOfWork = InternalUnitOfWork;
            }
            return repository;
        }

        protected virtual System.Type GetDefaultRepository(System.Type factoryType, System.Type entityType, params System.Type[] types)
        {
            System.Type ret;
            _defaultRepositories.TryGetValue(factoryType, out ret);
            return ret;
        }
        #endregion
    }

    public class RepositoryFactory<TIRepositoryFactory, TId> : RepositoryFactory, IRepositoryFactory<TIRepositoryFactory, TId>
        where TIRepositoryFactory : class, IRepositoryFactory<TIRepositoryFactory, TId>
    {
        #region Implementation of IRepositoryFactory<TIRepositoryFactory, TId>
        public IRepository<TIRepositoryFactory, TEntity, TId> Get<TEntity>()
            where TEntity : Entity<TIRepositoryFactory, TEntity, TId>
        {
            return Get<TEntity, TId>();
        }

        public IRepository<TIRepositoryFactory, TEntity, TEntityId> Get<TEntity, TEntityId>()
            where TEntity : Entity<TIRepositoryFactory, TEntity, TEntityId>
        {
            return (IRepository<TIRepositoryFactory, TEntity, TEntityId>)((IRepositoryFactory)this).Get<TEntity>(typeof(TEntityId));
        }
        #endregion

        #region Override methods
        protected override System.Type GetDefaultRepository(System.Type factoryType, System.Type entityType, params System.Type[] types)
        {
            System.Type repositoryType = base.GetDefaultRepository(typeof(TIRepositoryFactory), entityType, types);
            if (repositoryType != null && repositoryType.IsGenericType)
            {
                repositoryType = repositoryType.MakeGenericType(new System.Type[] {entityType}.Concat(types).ToArray());
            }
            return repositoryType;
        }
        #endregion
    }
}