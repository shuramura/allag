#region Using directives
using System;
using System.Diagnostics.CodeAnalysis;
using System.Web;
using Allag.Core.Data.Persistence;

#endregion

namespace Allag.Data.Management
{
    [ExcludeFromCodeCoverage]
    public class SessionModule : IHttpModule
    {
        #region IHttpModule Members
        public void Init(HttpApplication context)
        {
            context.BeginRequest += BeginTransaction;
            context.EndRequest += CommitAndCloseSession;
        }

        public void Dispose() {}
        #endregion

        #region Event's handlers
        private static void BeginTransaction(object sender, EventArgs e)
        {
            UnitOfWorkFactory.Create(false, true);
        }

        private static void CommitAndCloseSession(object sender, EventArgs e)
        {
            IUnitOfWork unitOfWork = UnitOfWorkFactory.Current;
            try
            {
                unitOfWork.CloseSession();
            }
            catch (Exception)
            {
                unitOfWork.Rollback();
                throw;
            }
            finally
            {
                unitOfWork.Dispose();
            }
        }
        #endregion
    }
}