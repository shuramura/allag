﻿#region Using directives
using System;
using Allag.Core;
using NHibernate;

#endregion

namespace Allag.Data.Management
{
    public class DbContext
    {
        #region Variables
        private bool? _isLightSession;
        private Session _session;

        private int _nCountOfOpenTransaction;
        private ITransaction _transaction;
        private readonly ISessionFactory _sessionFactory;
        #endregion

        #region Constructors
        public DbContext(ISessionFactory sessionFactory)
        {
            if (sessionFactory == null)
            {
                throw new ArgumentNullException(nameof(sessionFactory));
            }

            _session = null;
            _isLightSession = null;
            _nCountOfOpenTransaction = 0;
            _transaction = null;

            _sessionFactory = sessionFactory;
        }
        #endregion

        #region Properties
        public Session Session
        {
            get
            {
                CheckOpenSessionStatus();

                if (_session == null && _isLightSession != null)
                {
                    _session = new Session(_sessionFactory, _isLightSession.Value);
                }

                if (_session != null && _nCountOfOpenTransaction > 0 && _transaction == null)
                {
                    _transaction = _session.BeginTransaction();
                }
                return _session;
            }
        }

        private bool IsTransactionOpened => _transaction != null && !_transaction.WasCommitted && !_transaction.WasRolledBack;

        #endregion

        #region Methods
        public void BeginTransaction()
        {
            using(LogBlock.New<DbContext>(new {_nCountOfOpenTransaction, _transaction, _session}))
            {
                CheckOpenSessionStatus();
                if (_nCountOfOpenTransaction == 0 && _transaction == null && _session != null)
                {
                    _transaction = _session.BeginTransaction();
                    LogBlock.Logger.Debug("The transaction is opened.");
                }
                _nCountOfOpenTransaction++;
            }
        }

        public void CloseTransaction(bool isCommit = true)
        {
            using(LogBlock.New<DbContext>(new {isCommit, _nCountOfOpenTransaction, _transaction, _session}))
            {
                CheckOpenSessionStatus();
                if (_nCountOfOpenTransaction > 0)
                {
                    _nCountOfOpenTransaction--;
                    if (_transaction != null)
                    {
                        if (isCommit)
                        {
                            if (_nCountOfOpenTransaction == 0)
                            {
                                try
                                {
                                    Commit();
                                }
                                catch (HibernateException)
                                {
                                    Rollback();
                                    throw;
                                }
                            }
                        }
                        else
                        {
                            Rollback();
                        }
                    }
                }
            }
        }

        public void OpenSession(bool isLightSession = false)
        {
            if (_isLightSession != null)
            {
                throw new InvalidOperationException("The session has been already opened.");
            }
            _isLightSession = isLightSession;
        }

        public void CloseSession()
        {
            if (_session != null && _session.IsOpen)
            {
                Commit();
                _session.Dispose();
                _session = null;
            }
            _isLightSession = null;
        }

        public void Commit()
        {
            using(LogBlock.New<DbContext>())
            {
                CheckOpenSessionStatus();
                if (_session != null)
                {
                    if (IsTransactionOpened)
                    {
                        _transaction.Commit();
                        _transaction.Dispose();
                        LogBlock.Logger.Debug("The transaction is committed.");
                    }
                    else
                    {
                        _session.Flush();
                        LogBlock.Logger.Debug("The session is flushed.");
                    }
                }
                _transaction = null;
                _nCountOfOpenTransaction = 0;
            }
        }

        public void Rollback()
        {
            using(LogBlock.New<DbContext>())
            {
                if (IsTransactionOpened)
                {
                    _transaction.Rollback();
                    _transaction.Dispose();
                    _session.Clear();
                    LogBlock.Logger.Debug("The transaction is rollback.");
                }
                _nCountOfOpenTransaction = 0;
                _transaction = null;
            }
        }
        #endregion

        #region Static methods
        private void CheckOpenSessionStatus()
        {
            if (_isLightSession == null)
            {
                throw new InvalidOperationException("The session has not been opened.");
            }
        }
        #endregion
    }
}