﻿using System;
using System.Xml;
using NHibernate;

namespace Allag.Data.Management
{
    public interface ISessionBuilder
    {
        Tuple<NHibernate.Cfg.Configuration, ISessionFactory>  BuildSessionFactory(XmlReader configuration);
    }
}