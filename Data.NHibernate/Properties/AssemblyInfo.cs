﻿#region Using directives

using System.Reflection;
using System.Runtime.InteropServices;

#endregion

[assembly: AssemblyTitle("Allag Data.NHibernate")]
[assembly: AssemblyDescription("Data Core library")]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("3DCDC22C-ECEE-4C7B-8473-2132CEB98FD7")]