﻿#region Using directives
using Allag.Core.Tools;

#endregion

namespace Allag.Data.Type
{
    public class EnumStringType<TEnum> : NHibernate.Type.EnumStringType<TEnum>
        where TEnum : struct
    {
        #region Override methods
        public override object GetValue(object code)
        {
            object ret;
            if(!(code is TEnum))
            {
                ret = base.GetValue(code);
            }
            else
            {
                ret = ((TEnum) code).EnumToString();
            }
            return ret;
        }

        public override object GetInstance(object code)
        {
            string value = code as string;
            object ret;
            if(value == null)
            {
                ret = base.GetInstance(code);
            }
            else
            {
                ret = value.ParseEnum<TEnum>(true, delimiters: " ,");
            }
            return ret;
        }
        #endregion
    }
}