﻿#region Using directives
using System;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Allag.Core.Tools;
using NHibernate;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;

#endregion

namespace Allag.Data.Type
{
    [Serializable]
    public class XmlType<TItem> : IEnhancedUserType
        where TItem : new()
    {
        #region Implementation of IUserType
        bool IUserType.Equals(object x, object y)
        {
            return Equals(x, y);
        }

        public int GetHashCode(object x)
        {
            return x.GetHashCode();
        }

        public object NullSafeGet(IDataReader data, string[] names, object owner)
        {
            return Deserialize(NHibernateUtil.String.NullSafeGet(data, names[0]) as string);
        }

        public void NullSafeSet(IDbCommand cmd, object value, int index)
        {
            if (value == null)
            {
                NHibernateUtil.String.NullSafeSet(cmd, null, index);
            }
            else
            {
                NHibernateUtil.String.NullSafeSet(cmd, Serialize(value), index);
            }
        }

        public object DeepCopy(object value)
        {
            if (value != null)
            {
                System.Type type = value.GetType();
                if (!type.IsSerializable)
                {
                    throw new ArgumentException("The type must be serializable.", "value");
                }

                BinaryFormatter formatter = new BinaryFormatter();
                using(MemoryStream stream = new MemoryStream())
                {
                    formatter.Serialize(stream, value);
                    stream.Seek(0, SeekOrigin.Begin);
                    value = formatter.Deserialize(stream);
                }
            }

            return value;
        }

        public object Replace(object original, object target, object owner)
        {
            return original;
        }

        public object Assemble(object cached, object owner)
        {
            return DeepCopy(cached);
        }

        public object Disassemble(object value)
        {
            return DeepCopy(value);
        }

        public SqlType[] SqlTypes
        {
            get { return new SqlType[] { new XmlSqlType() }; }
        }

        public System.Type ReturnedType
        {
            get { return typeof(TItem); }
        }

        public bool IsMutable
        {
            get { return false; }
        }
        #endregion

        #region Implementation of IEnhancedUserType
        public object FromXMLString(string xml)
        {
            return Deserialize(xml);
        }

        public string ObjectToSQLString(object value)
        {
            return value == null ? null : "'" + ToXMLString(value) + "'";
        }

        public string ToXMLString(object value)
        {
            return Serialize(value);
        }
        #endregion

        #region Static methods
        private static string Serialize(object value)
        {
            return value == null ? null : ((TItem)value).Serialize();
        }

        private static object Deserialize(string value)
        {
            return value == null ? (object)null : new StringBuilder(value).Deserialize<TItem>();
        }
        #endregion
    }
}