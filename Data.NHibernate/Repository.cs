﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Allag.Core.Data.Persistence;
using Allag.Core.Data.Persistence.Helper;
using Allag.Core.Reflection;
using Allag.Core.Tools;
using NHibernate;
using NHibernate.Criterion;
using Expression = System.Linq.Expressions.Expression;
using NonUniqueResultException = NHibernate.NonUniqueResultException;

#endregion

namespace Allag.Data
{
    public abstract class Repository : IRepository
    {
        #region Constructors
        static Repository()
        {
            PropertyMethodInfo = typeof(OpMethods).GetMethod("Property").GetGenericMethodDefinition();
        }
        #endregion

        #region Implementation of IRepository
        public virtual Operations SupportedOperations
        {
            get { return Operations.All; }
        }

        public IUnitOfWork UnitOfWork
        {
            get { return InternalUnitOfWork; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }

                UnitOfWork unitOfWork = value as UnitOfWork;
                if (unitOfWork == null)
                {
                    throw new ArgumentException(string.Format("The type '{0}' is expected.", typeof(UnitOfWork)));
                }

                InternalUnitOfWork = unitOfWork;
            }
        }

        object IRepository.this[object id, bool throwException]
        {
            get { return Session.Get(EntityType, id); }
        }

        public abstract dynamic Get<TCriteria>(params TCriteria[] criteria) where TCriteria : class;
        public abstract IEnumerable<dynamic> Find<TCriteria>(params TCriteria[] criteria) where TCriteria : class;
        public abstract Lazy<long> Count();
        public abstract Lazy<long> Count<TCriteria>(params TCriteria[] criteria) where TCriteria : class;

        void IRepository.Delete(object entity)
        {
            CheckOperationSupport(entity, Operations.Delete);

            Session.Delete(entity);
        }

        object IRepository.Add(object entity)
        {
            CheckOperationSupport(entity, Operations.Add);

            Session.Save(entity);
            return entity;
        }

        object IRepository.Update(object entity)
        {
            CheckOperationSupport(entity, Operations.Update);

            Session.Update(entity);
            return entity;
        }
        #endregion

        #region Abstract properties
        protected abstract Session Session { get; }
        protected abstract System.Type EntityType { get; }
        #endregion

        #region Properties
        protected UnitOfWork InternalUnitOfWork { get; private set; }
        #endregion

        #region Static properties
        protected static MethodInfo PropertyMethodInfo { get; private set; }
        #endregion

        #region Methods
        private void CheckOperationSupport(object entity, Operations operation)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            if (!SupportedOperations.HasFlag(operation))
            {
                throw new InvalidOperationException(string.Format("The operation '{0}' does not support by {1}. The supported operations are: {2}.", operation, GetType(), SupportedOperations));
            }
        }
        #endregion
    }

    public class Repository<TIRepositoryFactory, TDefaultId, TEntity, TId> : Repository, IRepository<TIRepositoryFactory, TEntity, TId>
        where TIRepositoryFactory : class, IRepositoryFactory<TIRepositoryFactory, TDefaultId>
        where TEntity : Entity<TIRepositoryFactory, TEntity, TId>
    {
        #region Implementation of IDao<TEntity,TId>
        public TIRepositoryFactory RepositoryFactory
        {
            get { return UnitOfWork.Get<TIRepositoryFactory>(); }
        }

        public IQueryable<TEntity> Items
        {
            get { return Session.GetQueryable<TEntity>(); }
        }

        public Lazy<long> Count(Expression<Func<TEntity, bool>> criterion, params Expression<Func<TEntity, bool>>[] criteria)
        {
            List<Expression<Func<TEntity, bool>>> list = new List<Expression<Func<TEntity, bool>>> {criterion};
            if (criteria != null)
            {
                list.AddRange(criteria);
            }
            return Count(list.ToArray<LambdaExpression>());
        }

        public TEntity this[TId id, bool throwException = false]
        {
            get
            {
                TEntity ret = Session.Get<TEntity>(id);

                if (throwException && ret == null)
                {
                    throw new EntityNotFoundException<TEntity, TId>(id);
                }
                return ret;
            }
        }

        public TEntity this[params Expression<Func<TEntity, bool>>[] criteria]
        {
            get
            {
                try
                {
                    return Session.CreateCriteria(criteria).UniqueResult<TEntity>();
                }
                catch (NonUniqueResultException exp)
                {
                    throw new Core.Data.Persistence.NonUniqueResultException(exp.Message, exp);
                }
            }
        }

        public IEnumerable<TEntity> Find(params Expression<Func<TEntity, bool>>[] criteria)
        {
            return Session.Future<TEntity>(Session.CreateCriteria(criteria));
        }

        public IEnumerable<TItem> Find<TItem>(Expression<Func<TEntity, TItem>> properties, params Expression<Func<TEntity, bool>>[] criteria)
        {
            if (properties == null)
            {
                throw new ArgumentNullException("properties");
            }

            ParameterExpression expParameter = Expression.Parameter(typeof(object[]), properties.Parameters[0].Name);

            Expression body = Expression.Constant(true);
            bool? isAggregation = null;
            int nCount = 0;
            Func<object[], TItem> itemCreater = CreateInitializer<TItem>(properties.Body, expParameter, ref body, ref nCount, ref isAggregation).Compile();
            Func<object, TItem> initializer;
            if (nCount > 1)
            {
                initializer = x => itemCreater((object[])x);
            }
            else
            {
                initializer = x => itemCreater(new object[] {x});
            }
            List<Expression<Func<TEntity, bool>>> list = new List<Expression<Func<TEntity, bool>>>(criteria)
            {
                Expression.Lambda<Func<TEntity, bool>>(body, properties.Parameters[0])
            };
            return Session.Future<object>(Session.CreateCriteria(list.ToArray())).Where(x => x != null).Select(x => initializer(x));
        }

        public bool Exists(params Expression<Func<TEntity, bool>>[] criteria)
        {
            return Session.CreateCriteria(criteria).SetMaxResults(1).List<TEntity>().Any();
        }

        public virtual TEntity Add(TEntity entity)
        {
            return (TEntity)((IRepository)this).Add(entity);
        }

        public virtual TEntity Update(TEntity entity)
        {
            return (TEntity)((IRepository)this).Update(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            ((IRepository)this).Delete(entity);
        }
        #endregion

        #region Implementation of BaseDao
        public override dynamic Get<TCriteria>(params TCriteria[] criteria)
        {
            return ExecuteCriteria(criteria, (internalCriteria, projections, initializer) =>
            {
                try
                {
                    object result;
                    if (projections.Count > 0)
                    {
                        result = internalCriteria.UniqueResult<object>();
                    }
                    else
                    {
                        result = internalCriteria.UniqueResult<TEntity>();
                    }

                    if (result == null)
                    {
                        throw new EntityNotFoundException<TEntity>("The entity '{0}' with criteria '{1}' is not found.", typeof(TEntity), criteria);
                    }
                    return initializer(result);
                }
                catch (NonUniqueResultException exp)
                {
                    throw new Core.Data.Persistence.NonUniqueResultException(exp.Message, exp);
                }
            });
        }

        public override IEnumerable<dynamic> Find<TCriteria>(params TCriteria[] criteria)
        {
            return ExecuteCriteria(criteria, (internalCriteria, projections, initializer) =>
            {
                IEnumerable<object> result;
                if (projections.Count > 0)
                {
                    result = Session.Future<object>(internalCriteria);
                }
                else
                {
                    result = Session.Future<TEntity>(internalCriteria);
                }
                return result.Where(x => x != null).Select(initializer);
            });
        }

        public override Lazy<long> Count()
        {
            return Count(x => true);
        }

        public override Lazy<long> Count<TCriteria>(params TCriteria[] criteria)
        {
            ICriteria internalCriteria = Session.CreateCriteria<TEntity, TCriteria>(null, criteria).SetProjection(Projections.RowCountInt64());
            return Session.FutureValue<long>(internalCriteria);
        }

        protected override Session Session
        {
            get { return InternalUnitOfWork.GetContext<TIRepositoryFactory>().Session; }
        }

        protected override System.Type EntityType
        {
            get { return typeof(TEntity); }
        }
        #endregion

        #region Methods
        private TResult ExecuteCriteria<TCriteria, TResult>(TCriteria[] criteria, Func<ICriteria, ICollection<Condition>, Func<object, object>, TResult> action) where TCriteria : class
        {
            ICollection<Condition> projections = new Collection<Condition>();
            ICriteria criterion = Session.CreateCriteria<TEntity, TCriteria>(projections, criteria);
            Func<object, object> initializer;
            if (projections.Count > 0)
            {
                IEnumerable<string> names = projections.Select(x => x.Alias);
                Func<object[], object> itemCreater = row =>
                {
                    ExpandoObject ret = new ExpandoObject();
                    IDictionary<string, object> dictionary = ret;
                    names.ForEach((i, name) => dictionary.Add(name, row[i]));
                    return ret;
                };

                if (projections.Count > 1)
                {
                    initializer = x => itemCreater((object[])x);
                }
                else
                {
                    initializer = x => itemCreater(new object[] {x});
                }
            }
            else
            {
                initializer = x => x;
            }
            return action(criterion, projections, initializer);
        }
        #endregion

        #region Static methods
        private static Expression<Func<object[], TItem>> CreateInitializer<TItem>(Expression expression, ParameterExpression parameters, ref Expression body, ref int count, ref bool? isAggregation)
        {
            Expression extra = null;
            bool localIsAggregation = false;
            expression.HandleMethodExpression(typeof(OpMethods), (methodInfo, args) =>
            {
                Operation operation = methodInfo.GetCustomAttribute<OperationAttribute>().Op;
                if (operation == Operation.Distinct || operation == Operation.GroupBy)
                {
                    expression = args[0];
                    Expression expConstant = Expression.Constant(0);
                    extra = Expression.Equal(Expression.Call(methodInfo.GetGenericMethodDefinition().MakeGenericMethod(typeof(int)), expConstant), expConstant);
                    localIsAggregation = operation == Operation.GroupBy;
                }
            });

            if (extra != null)
            {
                body = Expression.AndAlso(body, extra);
                isAggregation = localIsAggregation;
            }
            Expression ret;
            switch (expression.NodeType)
            {
                case ExpressionType.New:
                    ret = ParseNewExpression((NewExpression)expression, parameters, ref body, ref count, ref isAggregation);
                    break;
                case ExpressionType.MemberInit:
                    ret = ParseMemberInitExpression((MemberInitExpression)expression, parameters, ref body, ref count, ref isAggregation);
                    break;
                case ExpressionType.NewArrayInit:
                    ret = ParseNewArrayExpression((NewArrayExpression)expression, parameters, ref body, ref count, ref isAggregation);
                    break;
                default:
                    throw new InvalidDataException(string.Format("The expression type '{0}' is not supported.", expression.NodeType));
            }
            return Expression.Lambda<Func<object[], TItem>>(ret, parameters);
        }

        private static NewArrayExpression ParseNewArrayExpression(NewArrayExpression expression, Expression parameters, ref Expression body, ref int count, ref bool? isAggregation)
        {
            List<Expression> initializers = new List<Expression>();
            foreach (Expression exp in expression.Expressions)
            {
                body = ParsePropertyExpression(body, exp, ref isAggregation);
                initializers.Add(GetParameter(parameters, count, exp.Type));
                count++;
            }
            return Expression.NewArrayInit(expression.Type.GetElementType(), initializers);
        }

        private static NewExpression ParseNewExpression(NewExpression expression, Expression parameters, ref Expression body, ref int count, ref bool? isAggregation)
        {
            List<Expression> ctorParameters = new List<Expression>();
            foreach (Expression exp in expression.Arguments)
            {
                body = ParsePropertyExpression(body, exp, ref isAggregation);
                ctorParameters.Add(GetParameter(parameters, count, exp.Type));
                count++;
            }
            return Expression.New(expression.Constructor, ctorParameters);
        }

        private static Expression ParseMemberInitExpression(MemberInitExpression expression, Expression parameters, ref Expression body, ref int count, ref bool? isAggregation)
        {
            NewExpression expNew = ParseNewExpression(expression.NewExpression, parameters, ref body, ref count, ref isAggregation);
            List<MemberBinding> bindParameters = new List<MemberBinding>();
            foreach (MemberBinding binding in expression.Bindings)
            {
                switch (binding.BindingType)
                {
                    case MemberBindingType.Assignment:
                    {
                        MemberAssignment assignment = (MemberAssignment)binding;
                        body = ParsePropertyExpression(body, assignment.Expression, ref isAggregation);
                        bindParameters.Add(assignment.Update(GetParameter(parameters, count, assignment.Expression.Type)));
                        count++;
                    }
                        break;
                    case MemberBindingType.ListBinding:
                    {
                        MemberListBinding listBinding = (MemberListBinding)binding;
                        List<ElementInit> initElementInits = new List<ElementInit>();
                        foreach (ElementInit elementInit in listBinding.Initializers)
                        {
                            List<Expression> arguments = new List<Expression>();
                            foreach (Expression exp in elementInit.Arguments)
                            {
                                body = ParsePropertyExpression(body, exp, ref isAggregation);
                                arguments.Add(GetParameter(parameters, count, exp.Type));
                                count++;
                            }
                            initElementInits.Add(elementInit.Update(arguments));
                        }
                        bindParameters.Add(listBinding.Update(initElementInits));
                    }
                        break;
                }
            }

            return Expression.MemberInit(expNew, bindParameters);
        }

        private static Expression GetParameter(Expression array, int index, System.Type type)
        {
            Expression expression = Expression.ArrayAccess(array, Expression.Constant(index));
            return Expression.Condition(Expression.NotEqual(expression, Expression.Constant(null)), Expression.Convert(expression, type), Expression.Default(type));
        }

        private static Expression ParsePropertyExpression(Expression seed, Expression expression, ref bool? isAggregation)
        {
            bool isMethod = false;
            MethodCallExpression expCallMethod;
            UnaryExpression unary = null;
            if (expression.NodeType == ExpressionType.Call ||
                (expression.NodeType == ExpressionType.Convert && (unary = (UnaryExpression)expression).Operand.NodeType == ExpressionType.Call))
            {
                expCallMethod = (MethodCallExpression)(unary != null ? unary.Operand : expression);
                expCallMethod.HandleMethodExpression(typeof(OpMethods), (methodInfo, args) => { isMethod = true; });
            }
            else
            {
                expCallMethod = Expression.Call(PropertyMethodInfo.MakeGenericMethod(expression.Type), expression);
            }

            if (isAggregation == null)
            {
                isAggregation = isMethod;
            }
            if (!isAggregation.Value && isMethod)
            {
                throw new InvalidDataException("It is not possible to combine aggregation methods with entity properties.");
            }
            seed = Expression.AndAlso(seed, Expression.Equal(expCallMethod, expCallMethod));

            return seed;
        }
        #endregion
    }
}