﻿#region Using directives
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Allag.Core;
using Allag.Core.Data.Persistence;
using Allag.Data.Configuration;
using Allag.Data.Management;
using NHibernate;

#endregion

namespace Allag.Data
{
    public sealed class UnitOfWork : IUnitOfWork
    {
        #region Variables
        private static readonly IList<RepositoryFactoryInfo> _repositoryFactoryInfos;

        private readonly Dictionary<System.Type, DbContext> _dbContexts;
        private readonly ConcurrentDictionary<System.Type, RepositoryFactory> _repositoryFactories;
        private bool _isDisposed;
        #endregion

        #region Events
        public event EventHandler Closed;
        #endregion

        #region Constructors
        static UnitOfWork()
        {
            ICollection items = DataNHibernateConfiguration.Sessions.Items;
            _repositoryFactoryInfos = new List<RepositoryFactoryInfo>(items.Count);
            foreach (SessionElement sessionElement in items)
            {
                _repositoryFactoryInfos.Add(new RepositoryFactoryInfo(sessionElement));
            }
        }

        public UnitOfWork()
        {
            _isDisposed = false;
            _dbContexts = new Dictionary<System.Type, DbContext>();
            _repositoryFactories = new ConcurrentDictionary<System.Type, RepositoryFactory>();
            Id = Guid.NewGuid();
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion

        #region Implementation of IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation of IUnitOfWork
        public Guid Id { get; private set; }

        public IRepositoryFactory this[System.Type factoryType]
        {
            get
            {
                return _repositoryFactories.GetOrAdd(factoryType, x => GetRepositoryFactoryInfoProperty(x, y => y.GetInstance(this)));    
            }
        }

        public TIRepositoryFactory Get<TIRepositoryFactory>() where TIRepositoryFactory : class, IRepositoryFactory
        {
            return (TIRepositoryFactory)this[typeof(TIRepositoryFactory)];
        }

        public IUnitOfWork OpenSession(bool isLightSession = false, bool isTransactional = false)
        {
            foreach (RepositoryFactoryInfo info in _repositoryFactoryInfos)
            {
                DbContext dbContext = new DbContext(info.SessionFactory);
                _dbContexts.Add(info.Type, dbContext);
                dbContext.OpenSession(isLightSession);
            }

            if (isTransactional)
            {
                BeginTransaction();
            }

            return this;
        }

        public void Commit()
        {
            using(LogBlock.New<UnitOfWork>())
            {
                foreach (DbContext dbContext in _dbContexts.Values)
                {
                    dbContext.Commit();
                }
            }
        }

        public void Rollback()
        {
            using(LogBlock.New<UnitOfWork>())
            {
                foreach (DbContext dbContext in _dbContexts.Values)
                {
                    dbContext.Rollback();
                }
            }
        }

        public void BeginTransaction()
        {
            using(LogBlock.New<UnitOfWork>())
            {
                foreach (DbContext dbContext in _dbContexts.Values)
                {
                    dbContext.BeginTransaction();
                }
            }
        }

        public void CloseTransaction(bool isCommitted = true)
        {
            using(LogBlock.New<UnitOfWork>())
            {
                foreach (DbContext dbContext in _dbContexts.Values)
                {
                    dbContext.CloseTransaction(isCommitted);
                }
            }
        }

        public void CloseSession()
        {
            using(LogBlock.New<UnitOfWork>())
            {
                List<Exception> exceptions = new List<Exception>();
                foreach (DbContext dbContext in _dbContexts.Values)
                {
                    try
                    {
                        dbContext.CloseSession();
                    }
                    catch (Exception e)
                    {
                        exceptions.Add(e);
                        LogBlock.Logger.Error("The close session is failed.", e);
                    }
                }
                if (exceptions.Count > 0)
                {
                    throw new AggregateException("The close session threw exceptions", exceptions);
                }
                _dbContexts.Clear();
                if (Closed != null)
                {
                    Closed(this, EventArgs.Empty);
                }
            }
        }
        #endregion

        #region Override methods
        public override string ToString()
        {
            return string.Format("{{Id: {0}}}", Id);
        }
        #endregion

        #region Methods
        public DbContext GetContext<TRepositoryFactory>()
        {
            DbContext ret;
            if (!_dbContexts.TryGetValue(typeof(TRepositoryFactory), out ret))
            {
                throw new InvalidOperationException("The session has not been opened.");
            }
            return ret;
        }

        private void Dispose(bool disposing)
        {
            using(LogBlock.New<UnitOfWork>())
            {
                if (!_isDisposed && disposing)
                {
                    try
                    {
                        CloseSession();
                    }
                    catch (Exception exp)
                    {
                        LogBlock.Logger.Error("The close session is failed.", exp);
                        Rollback();
                        CloseSession();
                    }
                    _isDisposed = true;
                }
            }
        }
        #endregion

        #region Static methods
        public static NHibernate.Cfg.Configuration GetConfiguration<TIRepositoryFactory>()
        {
            return GetRepositoryFactoryInfoProperty(typeof(TIRepositoryFactory), x => x.Configuration);
        }

        private static TValue GetRepositoryFactoryInfoProperty<TValue>(System.Type repositoryFactoryType, Func<RepositoryFactoryInfo, TValue> provider)
        {
            RepositoryFactoryInfo info = _repositoryFactoryInfos.FirstOrDefault(x => x.IsMatch(repositoryFactoryType));
            return info != null ? provider(info) : default(TValue);
        }
        #endregion

        #region Classes
        private class RepositoryFactoryInfo
        {
            #region Variables
            private readonly Func<UnitOfWork, RepositoryFactory> _factoryCreator;
            #endregion

            #region Constructors
            public RepositoryFactoryInfo(SessionElement sessionElement)
            {
                Type = sessionElement.Interface;
                _factoryCreator = unitOfWork =>
                {
                    RepositoryFactory factory = (RepositoryFactory)sessionElement.CreateObject();
                    factory.UnitOfWork = unitOfWork;
                    return factory;
                };

                using(XmlReader reader = XmlReader.Create(sessionElement.ConfigurationPath.GetStream()))
                {
                    Tuple<NHibernate.Cfg.Configuration, ISessionFactory> configuration;
                    if (typeof(ISessionBuilder).IsAssignableFrom(sessionElement.Type))
                    {
                        configuration = ((ISessionBuilder)sessionElement.CreateObject()).BuildSessionFactory(reader);
                    }
                    else
                    {
                        NHibernate.Cfg.Configuration cfg = new NHibernate.Cfg.Configuration().Configure(reader);
                        configuration = new Tuple<NHibernate.Cfg.Configuration, ISessionFactory>(cfg, cfg.BuildSessionFactory());
                    }

                    Configuration = configuration.Item1;
                    SessionFactory = configuration.Item2;
                }
            }
            #endregion

            #region Properties
            public System.Type Type { get; private set; }
            public NHibernate.Cfg.Configuration Configuration { get; private set; }
            public ISessionFactory SessionFactory { get; private set; }
            #endregion

            #region Methods
            public RepositoryFactory GetInstance(UnitOfWork unitOfWork)
            {
                return _factoryCreator(unitOfWork);
            }

            public bool IsMatch(System.Type type)
            {
                return type != null && type.IsAssignableFrom(Type);
            }
            #endregion
        }
        #endregion
    }
}