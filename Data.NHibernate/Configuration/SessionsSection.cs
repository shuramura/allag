#region Using directives

using System.Configuration;
using Allag.Core.Configuration;

#endregion

namespace Allag.Data.Configuration
{
    public class SessionsSection : Section
    {
        #region Properties
        [ConfigurationProperty("", Options = ConfigurationPropertyOptions.IsDefaultCollection)]
        [ConfigurationCollection(typeof(ElementCollection<SessionElement>))]
        public ElementCollection<SessionElement> Items
        {
            get
            {
                return (ElementCollection<SessionElement>)base[""];
            }
        }
        #endregion
    }
}
