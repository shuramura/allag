#region Using directives
using System.ComponentModel;
using System.Configuration;
using Allag.Core.Configuration;
using Allag.Core.Configuration.Reflection;
using Allag.Core.Tools;

#endregion

namespace Allag.Data.Configuration
{
    public class SessionElement : ObjectElement
    {
        #region Properties
        [ConfigurationProperty("path", IsRequired = true)]
        public IResource ConfigurationPath
        {
            get { return (IResource) base["path"]; }
        }
        #endregion
    }
}