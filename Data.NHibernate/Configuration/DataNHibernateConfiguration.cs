#region Using directives
using Allag.Core.Configuration;

#endregion

namespace Allag.Data.Configuration
{
    public static class DataNHibernateConfiguration
    {
        #region Constructors
        static DataNHibernateConfiguration()
        {
            Root = SectionGroup.Instance.GetRoot("allag.data.nhibernate");
        }
        #endregion

        #region Static properties
        private static SectionGroup Root { get; set; }

        public static SessionsSection Sessions
        {
            get { return Root.GetSection<SessionsSection>(); }
        }
        #endregion
    }
}