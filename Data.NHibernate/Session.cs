﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using NHibernate;
using NHibernate.Linq;

#endregion

namespace Allag.Data
{
    public sealed class Session : IDisposable
    {
        #region Variables
        private ISession _session;
        private IStatelessSession _statelessSession;

        private readonly ISessionFactory _sessionFactory;
        #endregion

        #region Constructors
        public Session(ISessionFactory sessionFactory, bool isLightSession = false)
        {
            if (sessionFactory == null)
            {
                throw new ArgumentNullException("sessionFactory");
            }

            _sessionFactory = sessionFactory;

            if (isLightSession)
            {
                _statelessSession = _sessionFactory.OpenStatelessSession();
            }
            else
            {
                _session = _sessionFactory.OpenSession();
            }
        }

        ~Session()
        {
            Dispose(false);
        }
        #endregion

        #region Implementation of IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Properties
        public bool IsOpen
        {
            get
            {
                CheckDisposeStatus();
                return _session == null || _session.IsOpen;
            }
        }

        public ISessionFactory SessionFactory
        {
            get
            {
                CheckDisposeStatus();
                return _session != null ? _session.SessionFactory : _sessionFactory;
            }
        }

        public IDbConnection Connection
        {
            get
            {
                CheckDisposeStatus();
                IDbConnection ret;
                if (_session != null)
                {
                    ret = _session.Connection;
                }
                else
                {
                    ret = _statelessSession.Connection;
                }
                return ret;
            }
        }
        #endregion

        #region Methods
        public ITransaction BeginTransaction()
        {
            CheckDisposeStatus();

            ITransaction ret;
            if (_session != null)
            {
                ret = _session.BeginTransaction();
            }
            else
            {
                ret = _statelessSession.BeginTransaction();
            }
            return ret;
        }

        public T Get<T>(object id)
        {
            CheckDisposeStatus();

            T ret;
            if (_session != null)
            {
                ret = _session.Get<T>(id);
            }
            else
            {
                ret = _statelessSession.Get<T>(id);
            }
            return ret;
        }

        public object Get(System.Type entityType, object id)
        {
            CheckDisposeStatus();

            if (entityType == null)
            {
                throw new ArgumentNullException("entityType");
            }

            object ret;
            if (_session != null)
            {
                ret = _session.Get(entityType, id);
            }
            else
            {
                ret = _statelessSession.Get(entityType.FullName, id);
            }
            return ret;
        }

        public object Save(object obj)
        {
            CheckDisposeStatus();

            object ret;
            if (_session != null)
            {
                ret = _session.Save(obj);
            }
            else
            {
                ret = _statelessSession.Insert(obj);
            }
            return ret;
        }

        public void Update(object obj)
        {
            CheckDisposeStatus();

            if (_session != null)
            {
                _session.Update(obj);
            }
            else
            {
                _statelessSession.Update(obj);
            }
        }

        public void Delete(object obj)
        {
            CheckDisposeStatus();

            if (_session != null)
            {
                _session.Delete(obj);
            }
            else
            {
                _statelessSession.Delete(obj);
            }
        }

        public IEnumerable<T> Future<T>(ICriteria criteria)
        {
            if (criteria == null)
            {
                throw new ArgumentNullException("criteria");
            }
            IEnumerable<T> ret;
            if (_session != null)
            {
                ret = criteria.Future<T>();
            }
            else
            {
                ret = criteria.List<T>();
            }
            return ret;
        }

        public Lazy<T> FutureValue<T>(ICriteria criteria)
        {
            if (criteria == null)
            {
                throw new ArgumentNullException("criteria");
            }
            Lazy<T> ret;
            if (_session != null)
            {
                IFutureValue<T> value = criteria.FutureValue<T>();
                ret = new Lazy<T>(() => value.Value);
            }
            else
            {
                T value = criteria.UniqueResult<T>();
                ret = new Lazy<T>(() => value);
            }
            return ret;
        }

        public void Flush()
        {
            CheckDisposeStatus();

            if (_session != null)
            {
                _session.Flush();
            }

        }

        public void Clear()
        {
            CheckDisposeStatus();

            if (_session != null)
            {
                _session.Clear();
            }
        }

        public IDbConnection Close()
        {
            CheckDisposeStatus();

            IDbConnection ret = null;
            if (_session != null)
            {
                ret = _session.Close();
            }
            else
            {
                _statelessSession.Close();
            }
            return ret;
        }

        public ICriteria CreateCriteria<T>() where T : class
        {
            CheckDisposeStatus();

            ICriteria ret;
            if (_session != null)
            {
                ret = _session.CreateCriteria<T>();
            }
            else
            {
                ret = _statelessSession.CreateCriteria<T>();
            }
            return ret;
        }

        public ICriteria CreateCriteria(System.Type entityType)
        {
            CheckDisposeStatus();

            ICriteria ret;
            if (_session != null)
            {
                ret = _session.CreateCriteria(entityType);
            }
            else
            {
                ret = _statelessSession.CreateCriteria(entityType);
            }
            return ret;
        }

        public IQuery CreateQuery(string queryString)
        {
            CheckDisposeStatus();

            IQuery ret;
            if (_session != null)
            {
                ret = _session.CreateQuery(queryString);
            }
            else
            {
                ret = _statelessSession.CreateQuery(queryString);
            }
            return ret;
        }

        public IQuery GetNamedQuery(string queryName)
        {
            CheckDisposeStatus();

            IQuery ret;
            if (_session != null)
            {
                ret = _session.GetNamedQuery(queryName);
            }
            else
            {
                ret = _statelessSession.GetNamedQuery(queryName);
            }
            return ret;
        }

        public IQueryOver<T, T> QueryOver<T>() where T : class
        {
            CheckDisposeStatus();

            IQueryOver<T, T> ret;
            if (_session != null)
            {
                ret = _session.QueryOver<T>();
            }
            else
            {
                ret = _statelessSession.QueryOver<T>();
            }
            return ret;
        }

        public IQueryOver<T, T> QueryOver<T>(Expression<Func<T>> alias) where T : class
        {
            CheckDisposeStatus();

            IQueryOver<T, T> ret;
            if (_session != null)
            {
                ret = _session.QueryOver(alias);
            }
            else
            {
                ret = _statelessSession.QueryOver(alias);
            }
            return ret;
        }

        public IQueryable<TEntity> GetQueryable<TEntity>()
        {
            IQueryable<TEntity> ret;
            if (_session != null)
            {
                ret = _session.Query<TEntity>();
            }
            else
            {
                ret = _statelessSession.Query<TEntity>();
            }
            return ret;
        }
       
        private void Dispose(bool disposing)
        {
            if (_session != null || _statelessSession != null)
            {
                if (disposing)
                {
                    if (_session != null)
                    {
                        _session.Dispose();
                    }
                    else
                    {
                        _statelessSession.Dispose();
                    }

                    _session = null;
                    _statelessSession = null;
                }
            }
        }

        private void CheckDisposeStatus()
        {
            if (_session == null && _statelessSession == null)
            {
                throw new ObjectDisposedException(GetType().FullName);
            }
        }
        #endregion
    }
}