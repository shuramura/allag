﻿using Allag.Core;
using NUnit.Framework;

namespace Allag.Logging.Log4Net
{
    [SetUpFixture]
    public class SetUpTests
    {
        #region Initialization
        [OneTimeSetUp]
        public void Init()
        {
            log4net.Config.XmlConfigurator.Configure();
            Application.Root = typeof(SetUpTests);
        }
        #endregion
    }
}
