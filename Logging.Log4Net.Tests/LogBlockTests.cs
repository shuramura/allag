#region Using directives
using System;
using System.Collections.Generic;
using Allag.Core;
using Allag.Core.Tools;
using NUnit.Framework;

#endregion

namespace Allag.Logging.Log4Net
{
    [TestFixture]
    public class LogBlockTests
    {
        #region Tests
        [Test]
        public void NewOneLevel_Test()
        {
            // Action
            using(LogBlock.New<LogBlockTests>()) {}
        }

        [Test]
        public void NewTwoLevel_Test()
        {
            // Action
            using(LogBlock.New<LogBlockTests>())
            {
                NewOneLevel_Test();
            }
        }

        [Test]
        public void NewLevelWithParameters_Test()
        {
            // Action
            using(LogBlock.New<LogBlockTests>(new {param0 = 1, param1 = 2, param2 = 3})) {}
        }

        [Test]
        public void NewLevelWithArrayParameters_Test()
        {
            // Action
            using(LogBlock.New<LogBlockTests>(new {param0 = 1, param1 = 2, param2 = new List<int[]> {new[] {30, 31}, new int[0], null}.Join(",", "[", "]")})) {}
        }

        [Test]
        public void Logger_Get_Test()
        {
            using(LogBlock.New<LogBlockTests>())
            {
                // Action
                ILogger result = LogBlock.Logger;

                // Post-Conditions
                Assert.IsNotNull(result);
            }
        }

        [Test]
        public void Logger_Get_WithNullType_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => LogBlock.New((Type)null));
        }

        [Test]
        public void Logger_Get_WithoutOpenedLogBlock_Test()
        {
            // Action
            ILogger result;
            Assert.Throws<InvalidOperationException>(() => result = LogBlock.Logger);
        }

        [Test]
        public void Action_WithNullAction_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => LogBlock.Action<LogBlockTests, bool>(null));
        }

        [Test]
        public void Action_Test()
        {
            // Action
            bool result = LogBlock.Action<LogBlockTests, bool>(() => true);

            // Post-Conditions
            Assert.IsTrue(result);
        }
        #endregion
    }
}