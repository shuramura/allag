#region Using directives
using System;
using Allag.Core;
using log4net;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Logging.Log4Net
{
    [TestFixture]
    public class LoggerTests
    {
        #region Variables
        private Logger _instance;
        private ILog _mockLog;
        private MockRepository _mocks;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _mocks = new MockRepository();
            _mockLog = _mocks.StrictMock<ILog>();
            _instance = new Logger(_mockLog);
        }
        #endregion

        #region Tests
        [Test]
        public void GetLogger_Test()
        {
            // Action 
            ILogger result = _instance.GetLogger(typeof(LoggerTests));

            // Post-Conditions  
            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void GetLoggerWithNullTypeTest()
        {
            // Action 
            Assert.That(() => _instance.GetLogger(null), Throws.InstanceOf<ArgumentNullException>());
        }

        #region Debug
        [Test]
        public void DebugTest()
        {
            // Pre-Conditions 
            const string str = "lalala";
            _mockLog.DebugFormat(str);

            _mocks.ReplayAll();

            // Action
            _instance.Debug(str);

            //Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void DebugFormatWithParametersTest()
        {
            // Pre-Conditions 
            const string strFormat = "{0}{1}{2}{3}{4}{5}";
            int[] nValues = new[] {0, 1, 2, 3, 4, 5};
            _mockLog.DebugFormat(strFormat, nValues[0], nValues[1], nValues[2], nValues[3], nValues[4], nValues[5]);

            _mocks.ReplayAll();

            // Action
            _instance.Debug(strFormat, nValues[0], nValues[1], nValues[2], nValues[3], nValues[4], nValues[5]);

            //Post-Conditions
            _mocks.VerifyAll();
        }
        #endregion

        #region Info
        [Test]
        public void InfoTest()
        {
            // Pre-Conditions 
            const string str = "lalala";
            _mockLog.InfoFormat(str);

            _mocks.ReplayAll();

            // Action
            _instance.Info(str);

            //Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void InfoFormatWithParametersTest()
        {
            // Pre-Conditions 
            const string strFormat = "{0}{1}{2}{3}{4}{5}";
            int[] nValues = new[] {0, 1, 2, 3, 4, 5};
            _mockLog.InfoFormat(strFormat, nValues[0], nValues[1], nValues[2], nValues[3], nValues[4], nValues[5]);

            _mocks.ReplayAll();

            // Action
            _instance.Info(strFormat, nValues[0], nValues[1], nValues[2], nValues[3], nValues[4], nValues[5]);

            //Post-Conditions
            _mocks.VerifyAll();
        }
        #endregion

        #region Warning
        [Test]
        public void Warning_Test()
        {
            // Pre-Conditions 
            const string str = "lalala";
            _mockLog.WarnFormat(str);

            _mocks.ReplayAll();

            // Action
            _instance.Warning(str);

            //Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void WarnWithExceptionTest()
        {
            // Pre-Conditions 
            const string str = "lalala";
            Exception exception = new Exception();
            _mockLog.Warn(str, exception);

            _mocks.ReplayAll();

            // Action
            _instance.Warning(str, exception);

            //Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void WarnFormatWithParametersTest()
        {
            // Pre-Conditions 
            const string strFormat = "{0}{1}{2}{3}{4}{5}";
            int[] nValues = new[] {0, 1, 2, 3, 4, 5};
            _mockLog.WarnFormat(strFormat, nValues[0], nValues[1], nValues[2], nValues[3], nValues[4], nValues[5]);

            _mocks.ReplayAll();

            // Action
            _instance.Warning(strFormat, nValues[0], nValues[1], nValues[2], nValues[3], nValues[4], nValues[5]);

            //Post-Conditions
            _mocks.VerifyAll();
        }
        #endregion

        #region Error
        [Test]
        public void Error_Test()
        {
            // Pre-Conditions 
            const string str = "lalala";
            Exception exception = new Exception(str);
            _mockLog.Error(str, exception);

            _mocks.ReplayAll();

            // Action
            _instance.Error(str, exception);

            //Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void ErrorWithExceptionTest()
        {
            // Pre-Conditions 
            const string str = "lalala";
            Exception exception = new Exception();
            _mockLog.Error(str, exception);

            _mocks.ReplayAll();

            // Action
            _instance.Error(str, exception);

            //Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void ErrorFormatWithParametersTest()
        {
            // Pre-Conditions 
            const string strFormat = "{0}{1}{2}{3}{4}{5}";
            int[] nValues = new[] {0, 1, 2, 3, 4, 5};
            Exception exception = new Exception();
            _mockLog.Error(string.Format(strFormat, nValues[0], nValues[1], nValues[2], nValues[3], nValues[4], nValues[5]), exception);

            _mocks.ReplayAll();

            // Action
            _instance.Error(strFormat, exception, nValues[0], nValues[1], nValues[2], nValues[3], nValues[4], nValues[5]);

            //Post-Conditions
            _mocks.VerifyAll();
        }
        #endregion

        #region Fatal
        [Test]
        public void FatalError_Test()
        {
            // Pre-Conditions 
            const string str = "lalala";
            Exception exception = new Exception();
            _mockLog.Fatal(str, exception);

            _mocks.ReplayAll();

            // Action
            _instance.FatalError(str, exception);

            //Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void FatalError_ExceptionOnly_Test()
        {
            // Pre-Conditions 
            Exception exception = new Exception();
            _mockLog.Fatal(exception);

            _mocks.ReplayAll();

            // Action
            _instance.FatalError(exception);

            //Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void FatalFormatWithParametersTest()
        {
            // Pre-Conditions 
            const string strFormat = "{0}{1}{2}{3}{4}{5}";
            int[] nValues = new[] {0, 1, 2, 3, 4, 5};
            Exception exception = new Exception();
            _mockLog.Fatal(string.Format(strFormat, nValues[0], nValues[1], nValues[2], nValues[3], nValues[4], nValues[5]), exception);

            _mocks.ReplayAll();

            // Action
            _instance.FatalError(strFormat, exception, nValues[0], nValues[1], nValues[2], nValues[3], nValues[4], nValues[5]);

            //Post-Conditions
            _mocks.VerifyAll();
        }
        #endregion

        #endregion
    }
}