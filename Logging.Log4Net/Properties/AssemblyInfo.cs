﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("All Logging.Log4Net")]
[assembly: AssemblyDescription("Logging module for log4net library")]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("049c7d2e-215c-41ea-a438-57fa96585243")]
