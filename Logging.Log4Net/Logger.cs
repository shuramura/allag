#region Using directives
using System;
using Allag.Core;
using log4net;

#endregion

namespace Allag.Logging.Log4Net
{
    public sealed class Logger : ILogger
    {
        #region Variables
        private readonly ILog _log;
        #endregion

        #region Constructors
        public Logger()
        {
            _log = LogManager.GetLogger(Application.Root);
        }

        public Logger(ILog log)
        {
            _log = log;
        }
        #endregion

        #region Implementation of IDisposable
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation of ILogger
        public ILogger GetLogger(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }
            return new Logger(LogManager.GetLogger(type));
        }
        public void Debug(string format, params object[] args)
        {
            _log.DebugFormat(format, args);
        }

        public void Info(string format, params object[] args)
        {
            _log.InfoFormat(format, args);
        }

        public void Warning(string format, Exception exception, params object[] args)
        {
            if (args != null && args.Length > 0)
            {
                _log.Warn(string.Format(format, args), exception);
            }
            else
            {
                _log.Warn(format, exception);
            }
        }

        public void Warning(string format, params object[] args)
        {
            _log.WarnFormat(format, args);
        }

        public void Error(string format, Exception exception, params object[] args)
        {
            if (args != null && args.Length > 0)
            {
                _log.Error(string.Format(format, args), exception);
            }
            else
            {
                _log.Error(format, exception);
            }
        }

        public void FatalError(string format, Exception exception, params object[] args)
        {
            if (args != null && args.Length > 0)
            {
                _log.Fatal(string.Format(format, args), exception);
            }
            else
            {
                _log.Fatal(format, exception);
            }
        }

        public void FatalError(Exception exception)
        {
            _log.Fatal(exception);
        }
        #endregion
    }
}