﻿#region Using directives
using System;
using System.IO;
using System.Linq;
using NUnit.Framework;

#endregion

namespace Allag.Core.Net
{
    public abstract class BaseItemTests<TClient, TItem> 
        where TItem : IItem
        where TClient : IFileClient
    {
        #region Constants
        private const string LOCAL_FOLDER = @".\TestData\Net\";
        protected const string FOLDER = @"UnitTesting\Item";
        protected const string FILE_NAME = "Item.txt";
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            FileClient = CreateClientInstance();
            FileClient.Upload(FOLDER, index => index == 0 ? new Tuple<string, Stream>(FILE_NAME, File.OpenRead(LOCAL_FOLDER + FILE_NAME)) : null, (i, stream) => stream.Dispose());
            Instance = (TItem)FileClient.GetDirectoryList(FOLDER, FILE_NAME).First();
        }

        [TearDown]
        public void TearDown()
        {
            FileClient.Delete(FOLDER, FILE_NAME);
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual('/' + Path.Combine(FOLDER, FILE_NAME).Replace('\\', '/'), Instance.Path, "The property 'Path' is wrong.");
            Assert.AreEqual('/' + FOLDER.Replace('\\', '/') + '/', Instance.Parent, "The property 'Parent' is wrong.");
            Assert.AreEqual(ItemType.File, Instance.Type, "The property 'Type' is wrong.");
            Assert.AreEqual(FILE_NAME, Instance.FullName, "The property 'FullName' is wrong.");
            Assert.AreEqual(8, Instance.Size, "The property 'Size' is wrong.");
            Assert.AreEqual(DateTime.Today, Instance.LastModified.Date, "The property 'LastModified' is wrong.");
        }

        [Test]
        public virtual void CtorDirectoryTest()
        {
            // Action
            TItem instance = (TItem)FileClient.GetDirectoryList(FOLDER, ".+").First();

            // Post-Conditions
            Assert.AreEqual('/' + FOLDER.Replace('\\', '/') + '/', instance.Parent, "The property 'Parent' is wrong.");
            Assert.AreEqual(ItemType.Directory, instance.Type, "The property 'Type' is wrong.");
            Assert.AreEqual("Folder", instance.FullName, "The property 'FullName' is wrong.");
            Assert.AreEqual('/' + FOLDER.Replace('\\', '/') + "/Folder", instance.Path, "The property 'Path' is wrong.");
            Assert.AreEqual(0, instance.Size, "The property 'Size' is wrong.");
        }

        #endregion

        #region Properties
        protected TItem Instance { get; private set; }
        protected TClient FileClient { get; private set; }
        #endregion

        #region Abstract
        protected abstract TClient CreateClientInstance();
        #endregion

    }
}