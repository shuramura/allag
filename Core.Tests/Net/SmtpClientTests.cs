#region Using directives

using System;
using System.Net.Mail;
using Allag.Core.Configuration;
using Allag.Core.Configuration.Net;
using NUnit.Framework;

#endregion

namespace Allag.Core.Net
{
    [TestFixture]
    public class SmtpClientTests
    {
        #region Variables
        private SmtpClient _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new SmtpClient("1");
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Action
            SmtpClient instance = new SmtpClient();

            // Post-Conditions
            Assert.IsNull(instance.Host, "The property 'Host' is wrong.");
            Assert.AreEqual(25, instance.Port, "The property 'Port' is wrong.");
            Assert.IsFalse(instance.EnableSsl, "The property 'EnableSsl' is wrong.");
            Assert.AreEqual(100000, instance.Timeout, "The property 'Timeout' is wrong.");
            Assert.AreEqual(SmtpDeliveryMethod.Network, instance.DeliveryMethod, "The property 'DeliveryMethod' is wrong.");
            Assert.IsNull(instance.PickupDirectoryLocation, "The property 'PickupDirectoryLocation' is wrong.");
            Assert.IsNull(instance.Configuration, "The property 'Configuration' is wrong.");
        }

        [Test]
        public void CtorFromConfigurationTest()
        {
            // Post-Conditions
            Assert.AreEqual("host", _instance.Host, "The property 'Host' is wrong.");
            Assert.AreEqual(100, _instance.Port, "The property 'Port' is wrong.");
            Assert.AreEqual(true, _instance.EnableSsl, "The property 'EnableSsl' is wrong.");
            Assert.AreEqual(120000, _instance.Timeout, "The property 'Timeout' is wrong.");
            Assert.AreEqual(SmtpDeliveryMethod.PickupDirectoryFromIis, _instance.DeliveryMethod, "The property 'DeliveryMethod' is wrong.");
            Assert.AreEqual(Application.Base, _instance.PickupDirectoryLocation, "The property 'PickupDirectoryLocation' is wrong.");
            Assert.AreEqual(new MailAddress("email@test.com.au", "test"), _instance.GetMailAddress(), "The property 'Email' is wrong.");
            Assert.AreEqual("1", _instance.Configuration.Name, "The property 'Configuration' is wrong.");
        }

        [Test]
        public void ConfigurationSetTest()
        {
            // Pre-Conditions
            SmtpElement expected = CoreCfg.Servers.Smtps["0"];

            // Action
            _instance.Configuration = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Configuration);
        }

        [Test]
        public void ConfigurationSetNullTest()
        {
            // Action
            Assert.That(() => _instance.Configuration = null, Throws.InstanceOf<ArgumentNullException>());
        }
        #endregion
    }
}
