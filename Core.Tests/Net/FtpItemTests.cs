#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Net
{
    [TestFixture]
    public class FtpItemTests : BaseItemTests<FtpClient, FtpItem>
    {
        #region Tests
        [Test]
        public void CtorNullFtpClientTest()
        {
            // Action
            Assert.That(() => new FtpItem(null, "lalala", "lalala"), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CtorNullParentTest()
        {
            // Action
            Assert.That(() => new FtpItem(FileClient, null, null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CtorNullDataTest()
        {
            // Action
            Assert.That(() => new FtpItem(FileClient, string.Empty, null), Throws.ArgumentException);
        }

        [Test]
        public void CtorEmptyDataTest()
        {
            // Action
            Assert.That(() => new FtpItem(FileClient, string.Empty, string.Empty), Throws.ArgumentException);
        }

        [Test]
        public void CtorWrongDataTest()
        {
            // Action
            Assert.That(() => new FtpItem(FileClient, string.Empty, "lalala"), Throws.InvalidOperationException);
        }
        #endregion

        #region Implementation of BaseItemTests<FtpClient, FtpItem>
        protected override FtpClient CreateClientInstance()
        {
            return new FtpClient();
        }
        #endregion
    }
}