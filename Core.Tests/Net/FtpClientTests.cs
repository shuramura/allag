#region Using directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using NUnit.Framework;

#endregion

namespace Allag.Core.Net
{
    [TestFixture]
    public class FtpClientTests : BaseClientTests<FtpClient>
    {
        #region Initialization
        [TearDown]
        public override void TearDown()
        {
            Instance.RemoteCertificateValidationHandler = delegate { return true; };
            base.TearDown();
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual("localhost", Instance.Host, "The property 'Host' is wrong.");
            Assert.AreEqual(21, Instance.Port, "The property 'Port' is wrong.");
            Assert.IsNotNull(Instance.RemoteCertificateValidationHandler, "The property 'RemoteCertificateValidationHandler' is wrong.");
        }

        [Test]
        public void CtorAbsentParameterTest()
        {
            // Action
            Assert.That(() => new FtpClient("lalala"), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void RemoteCertificateValidationHandler_Set_Test()
        {
            // Pre-Conditions 
            RemoteCertificateValidationCallback expected = delegate { return false; };

            // Action
            Instance.RemoteCertificateValidationHandler = expected;

            // Post-Conditions
            Assert.AreEqual(expected, Instance.RemoteCertificateValidationHandler);
        }

        [Test]
        public void RemoteCertificateValidationHandler_Set_NullTest()
        {
            // Action
            Instance.RemoteCertificateValidationHandler = null;

            // Post-Conditions
            Assert.IsNotNull(Instance.RemoteCertificateValidationHandler, "The property 'RemoteCertificateValidationHandler' is wrong.");
            Assert.IsTrue(Instance.RemoteCertificateValidationHandler(this, null, null, new SslPolicyErrors()), "The handler result  is wrong.");
        }

        [Test]
        public void GetLastModifiedDateTimeTest()
        {
            // Pre-Conditions 
            DateTime expected = DateTime.Now;

            IEnumerable<IItem> ftpItems = Instance.GetDirectoryList(REMOVE_FOLDER, ".+\\.dat");

            // Action
            DateTime result = Instance.GetLastModifiedDateTime(REMOVE_FOLDER, ftpItems.First().FullName);

            // Post-Conditions
            Assert.Greater(result, expected.AddMinutes(-10));
            Assert.Less(result, expected);
        }

        [Test]
        public void GetLastModifiedDateTimeNullItemTest()
        {
            // Action
            Assert.That(() => Instance.GetLastModifiedDateTime(REMOVE_FOLDER, null), Throws.ArgumentException);
        }

        [Test]
        public void GetLastModifiedDateTimeEmptyItemTest()
        {
            // Action
            Assert.That(() => Instance.GetLastModifiedDateTime(REMOVE_FOLDER, string.Empty), Throws.ArgumentException);
        }

        [Test]
        public void GetDirectoryList_WrongMask_Test([Values(null, "", " ")] string mask)
        {
            // Action
            Assert.That(() => Instance.GetDirectoryList(REMOVE_FOLDER, mask), Throws.InstanceOf<ArgumentNullOrEmptyException>());

        }

        [Test]
        public void GetDirectoryList_Test()
        {
            // Action
            IEnumerable<IItem> ftpItems = Instance.GetDirectoryList(REMOVE_FOLDER, ".+\\.dat");

            // Post-Conditions
            Assert.AreEqual(1, ftpItems.Count());
        }

        #endregion

        #region Implementation of BaseClientTests<FtpClient>
        protected override FtpClient CreateInstance()
        {
            return new FtpClient();
        }
        #endregion
    }
}