﻿#region Using directives
using System;
using Allag.Core.Configuration;
using Allag.Core.Configuration.Net;
using NUnit.Framework;

#endregion

namespace Allag.Core.Net
{
    using System.IO;

    [TestFixture]
    public class FileSystemClientTests : BaseClientTests<FileSystemClient>
    {
        #region Tests
        [Test]
        public void GetDirectoryList_OutOfRootFolderScope_Test()
        {
            // Action
            Assert.That(() => Instance.GetDirectoryList(@"..", ".+"), Throws.InstanceOf<ArgumentOutOfRangeException>());
        }

        [Test]
        public void Ctor_Test()
        {
            // Post-Conditions
            Assert.IsTrue(Instance.IsLocked, "The property 'IsLocked' is wrong.");
        }

        [Test]
        public void Ctor_FromConfig_Test()
        {
            // Pre-Conditions
            FileSystemElement element = CoreCfg.Servers.FileSystems["relative"];

            // Action
            FileSystemClient instance = new FileSystemClient(element);

            // Post-Conditions
            Assert.IsFalse(instance.IsLocked, "The property 'IsLocked' is wrong.");
        }

        [Test]
        public void Ctor_NullConfig_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => new FileSystemClient((FileSystemElement)null));
        }
        #endregion

        #region Implementation of BaseClientTests<FileSystemClient>
        protected override FileSystemClient CreateInstance()
        {
            return new FileSystemClient();
        }
        #endregion

        #region Override methods
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            Instance.RootFolder.CreateSubdirectory(Path.Combine(REMOVE_FOLDER, Guid.NewGuid().ToString()));
        }

        public override void TearDown()
        {
            base.TearDown();
            Instance.RootFolder.GetDirectories(REMOVE_FOLDER)[0].Delete(true);
        }

        #endregion
    }
}