﻿#region Using directives
using System;
using System.IO;
using NUnit.Framework;

#endregion

namespace Allag.Core.Net
{
    [TestFixture]
    public class FileSystemItemTests : BaseItemTests<FileSystemClient, FileSystemItem>
    {
        #region Override methods
        [Test]
        public override void CtorDirectoryTest()
        {
            // Pre-Conditions
            FileClient.RootFolder.CreateSubdirectory(Path.Combine(FOLDER, "Folder"));

            base.CtorDirectoryTest();
        }

        #endregion

        #region Tests
        [Test]
        public void Ctor_NullRoot_Test()
        {
            // Action
            Assert.That(() => new FileSystemItem(null, new FileInfo("c:\\")), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void Ctor_NullInfo_Test()
        {
            // Action
            Assert.That(() => new FileSystemItem(new DirectoryInfo("c:\\"), null), Throws.InstanceOf<ArgumentNullException>());
        }
        #endregion

        #region Implementation of BaseItemTests<FileSystemClient, FileSystemItem>
        protected override FileSystemClient CreateClientInstance()
        {
            return new FileSystemClient();
        }
        #endregion
    }
}