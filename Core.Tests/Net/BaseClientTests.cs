﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using NUnit.Framework;

#endregion

namespace Allag.Core.Net
{
    using System.Threading.Tasks;

    public abstract class BaseClientTests<TClient>
        where TClient : IFileClient
    {
        #region Constants
        protected const string REMOVE_FOLDER = "UnitTesting/Client/";
        private const string LOCAL_FOLDER = @".\TestData\Net\Client";
        private const string TEMP_EXTENSION = "tmp";
        #endregion

        #region Initialization
        [SetUp]
        public virtual void SetUp()
        {
            Instance = CreateInstance();
            IList<string> files = CreateFiles(1, "dat").Concat(CreateFiles(2, "txt")).ToList();

            Instance.Upload(REMOVE_FOLDER, delegate(int index)
            {
                Tuple<string, Stream> ret = null;
                if (index < files.Count)
                {
                    ret = new Tuple<string, Stream>(Path.GetFileName(files[index]), File.OpenRead(files[index]));
                }

                return ret;
            }, (i, stream) => stream.Dispose());
        }

        [TearDown]
        public virtual void TearDown()
        {
            string strFolder = Path.Combine(Environment.CurrentDirectory, LOCAL_FOLDER);
            if (Directory.Exists(strFolder))
            {
                Thread.Sleep(100);
                Directory.Delete(strFolder, true);
            }

            IEnumerable<IItem> items = Instance.GetDirectoryList(REMOVE_FOLDER, ".*");
            foreach (IItem item in items)
            {
                if (item.Type == ItemType.File)
                {
                    Instance.Delete(REMOVE_FOLDER, item.FullName);
                }
            }
        }
        #endregion

        #region Tests
        [Test]
        public void GetDirectoryListWithNullFolderTest()
        {
            // Action
            Assert.That(() => Instance.GetDirectoryList(null, "*.*"), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void GetDirectoryList_IncorrectMaskTest([Values(null, "", "  ")] string mask)
        {
            // Action
            Assert.That(() => Instance.GetDirectoryList(REMOVE_FOLDER, mask), Throws.InstanceOf<ArgumentNullOrEmptyException>());
        }

        [Test]
        public void GetDirectoryListTest()
        {
            // Action
            IEnumerable<IItem> result = Instance.GetDirectoryList(REMOVE_FOLDER, ".*");

            // Post-Conditions
            Assert.AreEqual(4, result.Count(), "The property 'Count' is wrong.");

            int nFileCount = 0;
            int nFolderCount = 0;
            foreach (IItem item in result)
            {
                if (item.Type == ItemType.Directory)
                {
                    nFolderCount++;
                }
                else if (item.Type == ItemType.File)
                {
                    nFileCount++;
                }
            }
            Assert.AreEqual(3, nFileCount, "The count of files is wrong.");
            Assert.AreEqual(1, nFolderCount, "The count of folders is wrong.");
        }

        [Test]
        public void GetDirectoryListZeroFilesTest()
        {
            // Action
            IEnumerable<IItem> result = Instance.GetDirectoryList(REMOVE_FOLDER, ".+\\.lalala");

            // Post-Conditions
            Assert.AreEqual(0, result.Count(), "The property 'Count' is wrong.");
        }

        [Test]
        public void GetDirectoryListWithMaskTest()
        {
            // Action
            IEnumerable<IItem> result = Instance.GetDirectoryList(REMOVE_FOLDER, ".+\\.dat");

            // Post-Conditions
            Assert.AreEqual(1, result.Count(), "The property 'Count' is wrong.");

            int nFileCount = 0;
            int nFolderCount = 0;
            foreach (IItem item in result)
            {
                if (item.Type == ItemType.Directory)
                {
                    nFolderCount++;
                }
                else if (item.Type == ItemType.File)
                {
                    nFileCount++;
                }
            }
            Assert.AreEqual(1, nFileCount, "The count of files is wrong.");
            Assert.AreEqual(0, nFolderCount, "The count of folders is wrong.");
        }

        [Test]
        public void GetDirectoryListNotExistFolderTest()
        {
            // Action
            Assert.That(() => Instance.GetDirectoryList(REMOVE_FOLDER + "lalala", ".+\\.dat"), Throws.InstanceOf<InvalidDataException>());
        }

        [Test]
        public async Task Download_Test()
        {
            // Pre-Conditions
            string[] expected = CreateFiles(1);

            Instance.Upload(REMOVE_FOLDER, delegate(int index)
            {
                Tuple<string, Stream> ret = null;
                if (index < expected.Length)
                {
                    ret = new Tuple<string, Stream>(Path.GetFileName(expected[index]), File.OpenRead(expected[index]));
                }

                return ret;
            }, (i, stream) => stream.Dispose());
            while (!Instance.GetDirectoryList(REMOVE_FOLDER, "." + TEMP_EXTENSION).Any())
            {
                await Task.Delay(1000);
            }
            Directory.Delete(LOCAL_FOLDER, true);

            // Action
            int nFileCount = 0;
            string[] result = Instance.Download(REMOVE_FOLDER, ".+\\." + TEMP_EXTENSION, delegate(int index, IItem file, Stream stream)
            {
                // Post-Conditions
                Assert.AreEqual(Path.GetFileName(file.FullName), file.FullName, "The file name is wrong.");
                nFileCount++;
                return file.FullName;
            }, item => true, item => true).ToArray();

            // Post-Conditions
            Assert.AreEqual(expected.Length, result.Length, "The result's count is wrong.");
            Assert.AreEqual(expected.Length, nFileCount, "The item's count is wrong.");

            IEnumerable<IItem> items = Instance.GetDirectoryList(REMOVE_FOLDER, ".+\\." + TEMP_EXTENSION);
            Assert.AreEqual(0, items.Count(), "The file was not deleted.");
        }

        [Test]
        public void UploadTest()
        {
            // Pre-Conditions 
            const int expected = 3;
            string[] files = CreateFiles(expected);

            // Action
            int nFileCount = 0;
            Instance.Upload(REMOVE_FOLDER, delegate(int index)
            {
                Tuple<string, Stream> ret = null;
                if (index < files.Length)
                {
                    nFileCount++;
                    ret = new Tuple<string, Stream>(Path.GetFileName(files[index]), File.OpenRead(files[index]));
                }

                return ret;
            }, (i, stream) => stream.Dispose());
            //
            // Post-Conditions
            Assert.AreEqual(expected, nFileCount, "The result count is wrong.");
            IEnumerable<IItem> items = Instance.GetDirectoryList(REMOVE_FOLDER, ".+\\." + TEMP_EXTENSION);
            Assert.AreEqual(expected, items.Count(), "The item count is wrong.");
        }

        [Test]
        public void DeleteTest()
        {
            // Pre-Conditions 
            int expected = Instance.GetDirectoryList(REMOVE_FOLDER, ".*").Count();
            string[] files = CreateFiles(3);
            Instance.Upload(REMOVE_FOLDER, delegate(int index)
            {
                Tuple<string, Stream> ret = null;
                if (index < files.Length)
                {
                    ret = new Tuple<string, Stream>(Path.GetFileName(files[index]), File.OpenRead(files[index]));
                }

                return ret;
            }, (i, stream) => stream.Dispose());

            // Action
            foreach (string file in files)
            {
                Instance.Delete(REMOVE_FOLDER, Path.GetFileName(file));
            }

            // Post-Conditions
            IEnumerable<IItem> items = Instance.GetDirectoryList(REMOVE_FOLDER, ".*");
            Assert.AreEqual(expected, items.Count());
        }
        #endregion

        #region Properties
        protected TClient Instance { get; private set; }
        #endregion

        #region Abstract
        protected abstract TClient CreateInstance();
        #endregion

        #region Methods
        public static string[] CreateFiles(int count, string extention = TEMP_EXTENSION)
        {
            string[] ret = new string[count];
            string strFolder = Path.Combine(Environment.CurrentDirectory, LOCAL_FOLDER);
            if (!Directory.Exists(strFolder))
            {
                Directory.CreateDirectory(strFolder);
            }
            for (int i = 0; i < count; i++)
            {
                ret[i] = Path.Combine(strFolder, Guid.NewGuid() + "." + extention);
                using(StreamWriter streamWriter = File.CreateText(ret[i]))
                {
                    streamWriter.WriteLine(DateTime.Now);
                }
            }
            return ret;
        }
        #endregion
    }
}