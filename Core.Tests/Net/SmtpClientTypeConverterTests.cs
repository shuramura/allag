﻿#region Using directives
using Allag.Core.Configuration;
using NUnit.Framework;

#endregion

namespace Allag.Core.Net
{
    [TestFixture]
    public class SmtpClientTypeConverterTests
    {
        #region Variables
        private SmtpClientTypeConverter _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new SmtpClientTypeConverter();
        }
        #endregion

        #region Tests
        [Test]
        public void ConvertFrom_Test()
        {
            // Action
            SmtpClient result = (SmtpClient) _instance.ConvertFrom(null, null, "0");

            // Post-Conditions
            Assert.AreEqual("SYDDEVIIS01", result.Host);
        }

        [Test]
        public void ConvertFrom_UnknownElement_Test()
        {
            // Action
            SmtpClient result = (SmtpClient)_instance.ConvertFrom(null, null, "unknown element");

            // Post-Conditions
            Assert.AreEqual("default", result.Host);
        }

        [Test]
        public void ConvertTo_Test()
        {
            // Pre-Conditions
            const string expected = "0";
            SmtpClient smtpClient = new SmtpClient(CoreCfg.Servers.Smtps[expected]);

            // Action
            string result = (string)_instance.ConvertTo(null, null, smtpClient, typeof(string));

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }
        #endregion
    }
}