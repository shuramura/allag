﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using Rhino.Mocks;
using Is = Rhino.Mocks.Constraints.Is;

#endregion

namespace Allag.Core.Net
{
    [TestFixture]
    public class FileManagerTests
    {
        #region Constants
        protected const string REMOVE_FOLDER = "RemoveFolder";
        private const string LOCAL_FOLDER = @".\TestData\Net\FileManager";
        private const string MASK = "*.txt";
        private const string FILE_NAME = "Test.txt";
        #endregion

        #region Variables
        private MockRepository _mocks;
        private IFileClient _fileClient;
        private FileManager _instance;
        #endregion

        #region Delegates
        private delegate void ClientUplodDelegate(string destinationFolder, Func<int, Tuple<string, Stream>> getFileSource, Action<int, Stream> releaseResource);

        public delegate void ClientDownloadDelegate(string sourceFolder, string mask, Func<int, IItem, Stream, string> saveFileHndler, Func<IItem, bool> isDownloadHandler, Func<IItem, bool> isDeleteHandler);
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _mocks = new MockRepository();
            _fileClient = _mocks.DynamicMock<IFileClient>();
            _instance = new FileManager();
            _instance.Clients.Add("Test", _fileClient);
        }

        [TearDown]
        public void TearDown()
        {
            string strFolder = Path.Combine(Environment.CurrentDirectory, LOCAL_FOLDER);
            if (Directory.Exists(strFolder))
            {
                Directory.Delete(strFolder, true);
            }
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(4096, _instance.DataBuffer, "The property 'DataBuffer' is wrong.");
            Assert.AreEqual(1, _instance.Clients.Count, "The length of property 'Clients' is wrong.");
            Assert.AreEqual(_fileClient, _instance.Clients["Test"], "The property 'Clients' is wrong.");
        }

        [Test]
        public void Ctor_WithParameters_Test()
        {
            // Action
            const string strConfig = "ftp://TestFtp";
            FileManager instance = new FileManager(strConfig);

            // Post-Conditions
            Assert.AreEqual(4096, instance.DataBuffer, "The property 'DataBuffer' is wrong.");
            Assert.AreEqual(1, instance.Clients.Count, "The length of property 'Clients' is wrong.");
            Assert.IsInstanceOf<FtpClient>(instance.Clients[strConfig], "The property 'Clients' is wrong.");
        }

        [Test]
        public void CtorWithoutClienInitializationTest()
        {
            // Action
            FileManager instance = new FileManager();

            // Post-Conditions
            Assert.AreEqual(4096, instance.DataBuffer, "The property 'DataBuffer' is wrong.");
            Assert.AreEqual(0, instance.Clients.Count, "The property 'Clients' is wrong.");
        }

        [Test]
        public void DownloadEnumeration_Test()
        {
            // Pre-Conditions
            IItem item = _mocks.Stub<IItem>();
            SetupResult.For(item.FullName).Return(FILE_NAME);
            byte[] expected = new byte[] {84, 101, 115, 116, 46, 116, 120, 116};
            string[] expectedResult = new string[] {"1", "2"};
            _fileClient.Expect(x => x.Download(Arg<string>.Is.Equal(REMOVE_FOLDER), Arg<string>.Is.Equal(MASK),
                Arg<Func<int, IItem, Stream, string>>.Is.NotNull, Arg<Func<IItem, bool>>.Is.NotNull, Arg<Func<IItem, bool>>.Is.NotNull))
                .WhenCalled(invocation =>
                {
                    Func<int, IItem, Stream, string> saveFileHndler = (Func<int, IItem, Stream, string>)invocation.Arguments[2];
                    using(MemoryStream sourceStream = new MemoryStream())
                    {
                        sourceStream.Write(expected, 0, expected.Length);
                        sourceStream.Seek(0, SeekOrigin.Begin);

                        saveFileHndler(0, item, sourceStream);
                    }
                    invocation.ReturnValue = expectedResult;
                }).Return(null);

            _mocks.ReplayAll();

            // Action
            IEnumerable<string> result = _instance.DownloadEnumeration(REMOVE_FOLDER, LOCAL_FOLDER, MASK, true);

            // Post-Conditions
            Assert.IsTrue(Directory.Exists(Path.Combine(Environment.CurrentDirectory, LOCAL_FOLDER)), "The folder is absent.");
            string strFile = Path.Combine(Path.Combine(Environment.CurrentDirectory, LOCAL_FOLDER), FILE_NAME);
            Assert.IsFalse(File.Exists(strFile), "The file is already downloaded.");
            CollectionAssert.AreEqual(expectedResult, result, "The result is wrong.");
            _mocks.VerifyAll();
            Assert.IsTrue(File.Exists(strFile), "The file is not downloaded.");
            using (StreamReader streamReader = File.OpenText(strFile))
            {
                Assert.AreEqual(FILE_NAME, streamReader.ReadToEnd(), "The file is wrong.");
            }
        }

        [Test]
        public void Download_Test()
        {
            // Pre-Conditions
            IItem item = _mocks.Stub<IItem>();
            SetupResult.For(item.FullName).Return(FILE_NAME);
            byte[] expected = new byte[] { 84, 101, 115, 116, 46, 116, 120, 116 };
            string[] expectedResult = new string[] { "1", "2" };
            _fileClient.Expect(x => x.Download(Arg<string>.Is.Equal(REMOVE_FOLDER), Arg<string>.Is.Equal(MASK),
                Arg<Func<int, IItem, Stream, string>>.Is.NotNull, Arg<Func<IItem, bool>>.Is.NotNull, Arg<Func<IItem, bool>>.Is.NotNull))
                .WhenCalled(invocation =>
                {
                    Func<int, IItem, Stream, string> saveFileHndler = (Func<int, IItem, Stream, string>)invocation.Arguments[2];
                    using (MemoryStream sourceStream = new MemoryStream())
                    {
                        sourceStream.Write(expected, 0, expected.Length);
                        sourceStream.Seek(0, SeekOrigin.Begin);

                        saveFileHndler(0, item, sourceStream);
                    }
                    invocation.ReturnValue = expectedResult;
                }).Return(null);

            _mocks.ReplayAll();

            // Action
            _instance.Download(REMOVE_FOLDER, LOCAL_FOLDER, MASK, true);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.IsTrue(Directory.Exists(Path.Combine(Environment.CurrentDirectory, LOCAL_FOLDER)), "The folder is absent.");
            string strFile = Path.Combine(Path.Combine(Environment.CurrentDirectory, LOCAL_FOLDER), FILE_NAME);
            Assert.IsTrue(File.Exists(strFile), "The file is not downloaded.");
            using (StreamReader streamReader = File.OpenText(strFile))
            {
                Assert.AreEqual(FILE_NAME, streamReader.ReadToEnd(), "The file is wrong.");
            }
        }

        [Test]
        public void DownloadWithNullLocalFolderTest()
        {
            // Action
            Assert.That(() => _instance.Download(REMOVE_FOLDER, null, MASK, true), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void UploadFilesTest()
        {
            // Pre-Conditions
            byte[] expected = new byte[] {84, 101, 115, 116, 46, 116, 120, 116, 13, 10};
            CreateFile();
            Directory.CreateDirectory(LOCAL_FOLDER);
            _fileClient.Upload(null, null, null);
            LastCall.On(_fileClient).Constraints(Is.Equal(REMOVE_FOLDER), Is.NotNull(), Is.NotNull()).Do((ClientUplodDelegate)delegate(string destinationFolder, Func<int, Tuple<string, Stream>> getFileSource, Action<int, Stream> releaseResource)
            {
                Tuple<string, Stream> item = getFileSource(0);
                try
                {
                    item.Item2.Seek(0, SeekOrigin.Begin);

                    // Action
                    byte[] result = new byte[10];
                    item.Item2.Read(result, 0, result.Length);

                    // Post-Conditions
                    Assert.AreEqual(10, item.Item2.Length, "The stream length is wrong.");
                    CollectionAssert.AreEqual(expected, result, "The stream is wrong.");
                }
                finally
                {
                    releaseResource(0, item.Item2);
                }
            });
            _mocks.ReplayAll();

            // Action
            _instance.Upload(LOCAL_FOLDER, REMOVE_FOLDER, MASK);

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void UploadFilesWithNullLocalFolderTest()
        {
            // Action
            Assert.That(() => _instance.Upload((string)null, REMOVE_FOLDER, MASK), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void UploadStreamTest()
        {
            // Pre-Conditions
            using(MemoryStream stream = new MemoryStream())
            {
                byte[] expected = new byte[] {0, 1, 2, 3, 4};
                stream.Write(expected, 0, expected.Length);
                stream.Seek(0, SeekOrigin.Begin);

                Directory.CreateDirectory(LOCAL_FOLDER);
                _fileClient.Upload(null, null, null);
                LastCall.On(_fileClient).Constraints(Is.Equal(REMOVE_FOLDER), Is.NotNull(), Is.Null()).Do((ClientUplodDelegate)delegate(string destinationFolder, Func<int, Tuple<string, Stream>> getFileSource, Action<int, Stream> releaseResource)
                {
                    Tuple<string, Stream> item = getFileSource(0);
                    item.Item2.Seek(0, SeekOrigin.Begin);

                    // Action
                    byte[] result = new byte[5];
                    item.Item2.Read(result, 0, result.Length);

                    // Post-Conditions
                    Assert.AreEqual(5, item.Item2.Length, "The stream length is wrong.");
                    CollectionAssert.AreEqual(expected, result, "The stream is wrong.");
                });
                _mocks.ReplayAll();

                // Action
                _instance.Upload(stream, REMOVE_FOLDER, FILE_NAME);

                // Post-Conditions
                _mocks.VerifyAll();
            }
        }

        [Test]
        public void UploadStreamNullStreamTest()
        {
            // Action
            Assert.That(() => _instance.Upload((Stream)null, REMOVE_FOLDER, FILE_NAME), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void UploadStreamNullFileNameTest()
        {
            // Action
            using(MemoryStream stream = new MemoryStream())
            {
                Assert.That(() => _instance.Upload(stream, REMOVE_FOLDER, null), Throws.ArgumentException);
            }
        }

        [Test]
        public void UploadStreamEmptyFileNameTest()
        {
            // Action
            using(MemoryStream stream = new MemoryStream())
            {
                Assert.That(() => _instance.Upload(stream, REMOVE_FOLDER, string.Empty), Throws.InstanceOf<ArgumentException>());
            }
        }

        [Test]
         public void UploadStreamInvalidFileNameTest()
        {
            // Action
            using(MemoryStream stream = new MemoryStream())
            {
                Assert.That(() => _instance.Upload(stream, REMOVE_FOLDER, ":test.txt"), Throws.InstanceOf<ArgumentException>());
            }
        }

        [Test]
        public void DeleteFileTest()
        {
            // Pre-Conditions
            _fileClient.Delete(LOCAL_FOLDER, FILE_NAME);
            _mocks.ReplayAll();

            // Action
            _instance.DeleteFile(LOCAL_FOLDER, FILE_NAME);

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void GetDirectoryListTest()
        {
            // Pre-Conditions
            List<IItem> expected = new List<IItem>();
            Expect.Call(_fileClient.GetDirectoryList(LOCAL_FOLDER, MASK)).Return(expected);
            _mocks.ReplayAll();

            // Action
            IEnumerable<IItem> result = _instance.GetDirectoryList(LOCAL_FOLDER, MASK);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void AddFileClient_Test()
        {
            // Pre-Conditions
            int nCount = _instance.Clients.Count;
            const string strConfigName = "file://relative";

            // Action
            _instance.AddFileClient(strConfigName);

            // Post-Conditions
            Assert.AreEqual(nCount + 1, _instance.Clients.Count, "The count is wrong.");
            Assert.IsNotNull(_instance.Clients[strConfigName], "The client is wrong.");
        }

        [Test]
        public void AddFileClient_WrongConfigName_Test([Values(null, "", "  ")] string configName)
        {
            // Action
            Assert.That(() => _instance.AddFileClient(configName), Throws.InstanceOf<ArgumentNullOrEmptyException>());
        }
        #endregion

        #region Helpers
        private static void CreateFile()
        {
            string strFolder = Path.Combine(Environment.CurrentDirectory, LOCAL_FOLDER);
            string ret = Path.Combine(strFolder, FILE_NAME);
            if (!Directory.Exists(strFolder))
            {
                Directory.CreateDirectory(strFolder);
            }
            using(StreamWriter streamWriter = File.CreateText(ret))
            {
                streamWriter.WriteLine(FILE_NAME);
            }
        }
        #endregion
    }
}