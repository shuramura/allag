#region Using directives
using System.ComponentModel;
using System.Configuration.Install;
using System.Reflection;
using Allag.Core.Service;
using Allag.Core.Tools.Job.IntervalProviders;
using NUnit.Framework;

#endregion

namespace Allag.Core
{
    [RunInstaller(true)]
    [SetUpFixture]
    public class SetUpTests : Installer
    {
        #region Constructors
        public SetUpTests()
        {
            //Installers.AddRange(ServiceFactory.GetInstallers());
        }
        #endregion

        #region Initialization
        [OneTimeSetUp]
        public void Init()
        {
            Application.Root = typeof(SetUpTests);

            //try
            //{
            //    ServiceFactory.RunApplication(ServiceFactory.InstallArg);
            //}
            //catch (Exception)
            //{
            //    ServiceFactory.RunApplication(ServiceFactory.UninstallArg);
            //    ServiceFactory.RunApplication(ServiceFactory.InstallArg);
            //}

            FieldInfo fieldInfo = typeof(BaseTimeIntervalProvider).GetField("_nInternalTimerInterval", BindingFlags.NonPublic | BindingFlags.Static);
            fieldInfo.SetValue(null, 1000);
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            //ServiceFactory.RunApplication(ServiceFactory.UninstallArg);
        }
        #endregion
    }
}