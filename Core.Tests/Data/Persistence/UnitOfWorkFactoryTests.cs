﻿#region Using directives
using System;
using System.IO;
using Allag.Core.Reflection;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Data.Persistence
{
    [TestFixture]
    public class UnitOfWorkFactoryTests
    {
        #region Variables
        private MockRepository _mocks;
        private IUnitOfWork _unitOfWork;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _mocks = new MockRepository();
            _unitOfWork = _mocks.StrictMock<IUnitOfWork>();
            ObjectFactory.Add(() => _unitOfWork);
        }

        [TearDown]
        public void CleanUp()
        {
            ReflectionExtension.SetProperty(() => UnitOfWorkFactory.Current, null);
            ObjectFactory.Remove<IUnitOfWork>(this);
        }
        #endregion

        #region Tests
        [Test]
        public void Current_Get_NotExists_Test()
        {
            // Action
            IUnitOfWork result = UnitOfWorkFactory.Current;

            // Post-Condition
            Assert.IsNull(result);
        }

        [Test]
        public void Create_Test([Values(true, false)] bool isLightSession,
            [Values(true, false)] bool isTransactional)
        {
            // Pre-Conditions
            EventHandler eventHandler = null;
            _unitOfWork.Expect(x => x.OpenSession(isLightSession, isTransactional)).Return(_unitOfWork);
            _unitOfWork.Expect(x => x.Closed += Arg<EventHandler>.Is.Anything)
                .WhenCalled(invocation => { eventHandler = ((EventHandler)invocation.Arguments[0]); }); 
            _mocks.ReplayAll();

            // Action
            UnitOfWorkFactory.Create(isLightSession, isTransactional);

            // Post-Condition
            _mocks.VerifyAll();
            Assert.AreEqual(_unitOfWork, UnitOfWorkFactory.Current, "The unit of work is wrong.");
            eventHandler(_unitOfWork, EventArgs.Empty);
            Assert.IsNull(UnitOfWorkFactory.Current, "The unit of work is wrong.");
        }

        [Test]
        public void Create_WithInnerSession_Test()
        {
            // Pre-Conditions
            IUnitOfWork unitOfWork = _mocks.StrictMock<IUnitOfWork>();
            _unitOfWork.Expect(x => x.OpenSession()).Return(_unitOfWork);
            _unitOfWork.Expect(x => x.Closed += Arg<EventHandler>.Is.Anything);
            _unitOfWork.Expect(x => x.OpenSession()).Return(unitOfWork);
            _mocks.ReplayAll();
            UnitOfWorkFactory.Create();

            // Action
            Assert.That(() => UnitOfWorkFactory.Create(), Throws.InvalidOperationException);
        }

        [Test]
        public void Close_Test()
        {
            // Pre-Conditions
            _unitOfWork.Expect(x => x.OpenSession()).Return(_unitOfWork);
            _unitOfWork.Expect(x => x.Closed += Arg<EventHandler>.Is.Anything);
            _unitOfWork.Expect(x => x.Dispose());
            _mocks.ReplayAll();
            UnitOfWorkFactory.Create();

            // Action
            UnitOfWorkFactory.Close();

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void Execute_NullAction_Test()
        {
            // Pre-Conditions
            _mocks.ReplayAll();

            // Action
            Assert.That(() => UnitOfWorkFactory.Execute(true, false, (Action)null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void Execute_NullActionWithUnitOfWork_Test()
        {
            // Pre-Conditions
            _mocks.ReplayAll();

            // Action
            Assert.That(() => UnitOfWorkFactory.Execute(true, false, (Action<IUnitOfWork>)null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void Execute_WithResultNullAction_Test()
        {
            // Pre-Conditions
            _mocks.ReplayAll();

            // Action
            Assert.That(() => UnitOfWorkFactory.Execute(true, false, (Func<int>)null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void Execute_WithResultNullActionWithUnitOfWork_Test()
        {
            // Pre-Conditions
            _mocks.ReplayAll();

            // Action
            Assert.That(() => UnitOfWorkFactory.Execute(true, false, (Func<IUnitOfWork, int>)null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void Execute_Test()
        {
            // Pre-Conditions
            int nActionCount = 0;
            int nExceptionCount = 0;

            _unitOfWork.Expect(x => x.OpenSession(true)).Return(_unitOfWork);
            _unitOfWork.Expect(x => x.Closed += Arg<EventHandler>.Is.Anything);
            _unitOfWork.Expect(x => x.Commit());
            _unitOfWork.Expect(x => x.Dispose());
            _mocks.ReplayAll();

            // Action
            UnitOfWorkFactory.Execute(true, false, () => { nActionCount++; }, exception => { nExceptionCount++; });

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(1, nActionCount, "The action count is wrong.");
            Assert.AreEqual(0, nExceptionCount, "The exception count is wrong.");
        }

        [Test]
        public void Execute_WithExistsUnitOfWork_Test()
        {
            // Pre-Conditions
            int nActionCount = 0;
            int nExceptionCount = 0;
            ReflectionExtension.SetProperty(() => UnitOfWorkFactory.Current, _unitOfWork);
            _mocks.ReplayAll();

            // Action
            UnitOfWorkFactory.Execute(true, false, () => { nActionCount++; }, exception => { nExceptionCount++; });

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(1, nActionCount, "The action count is wrong.");
            Assert.AreEqual(0, nExceptionCount, "The exception count is wrong.");
        }

        [Test]
        public void Execute_WithException_Test()
        {
            // Pre-Conditions
            Exception exception = new InvalidDataException();
            ReflectionExtension.SetProperty(() => UnitOfWorkFactory.Current, _unitOfWork);
            _unitOfWork.Expect(x => x.Rollback());
            _mocks.ReplayAll();

            // Action
            Assert.That(() => UnitOfWorkFactory.Execute(true, false, () => { throw exception; }, exp => Assert.AreEqual(exception, exp, "The exception is wrong.")), Throws.InstanceOf<InvalidDataException>());
        }
        #endregion
    }
}