#region Using directives
using Allag.Core.Reflection;
using Allag.Core.TestData.Data.Persistence;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Persistence
{
    [TestFixture]
    public class EntityTests
    {
        #region Variables
        private MockEntity _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new MockEntity();
        }

        [TearDown]
        public void TearDown()
        {
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(true, _instance.IsTransient, "The property 'IsTransient' is wrong.");
            Assert.AreEqual(default(int), _instance.Id, "The property 'Id' is wrong.");
            Assert.AreEqual(_instance.Id, ((IEntity)_instance).Id, "The property 'IEntity.Id' is wrong.");
        }

        [Test]
        public void EqualsForNullTest()
        {
            // Post-Conditions
            Assert.IsFalse(_instance.Equals((object)null), "The method 'Equals' is wrong.");
            Assert.IsFalse(_instance == null, "The operation '==' is wrong.");
            Assert.IsTrue(_instance != null, "The operation '!=' is wrong.");
        }

        [Test]
        public void EqualsTrueForTransientTest()
        {
            // Pre-Conditions 
            MockEntity instance0 = new MockEntity {NullableNumber = 1, Number = 2, String = "true"};
            MockEntity instance1 = new MockEntity {NullableNumber = 1, Number = 2, String = "false"};

            // Post-Conditions
            Assert.IsTrue(instance0.Equals(instance1), "The method 'Equals' is wrong.");
            Assert.IsTrue(instance0 == instance1, "The operation '==' is wrong.");
            Assert.IsFalse(instance0 != instance1, "The operation '!=' is wrong.");
        }

        [Test]
        public void EqualsFalseForTransientTest()
        {
            // Pre-Conditions 
            MockEntity instance0 = new MockEntity {NullableNumber = 1, Number = 2, String = "true"};
            MockEntity instance1 = new MockEntity {NullableNumber = 3, Number = 2, String = "false"};

            // Post-Conditions
            Assert.IsFalse(instance0.Equals(instance1), "The method 'Equals' is wrong.");
            Assert.IsFalse(instance0 == instance1, "The operation '==' is wrong.");
            Assert.IsTrue(instance0 != instance1, "The operation '!=' is wrong.");
        }

        [Test]
        public void EqualsTrueForPersistentTest()
        {
            // Pre-Conditions 
            MockEntity instance0 = new MockEntity {NullableNumber = 1, Number = 2, String = "true"};
            instance0.SetId(10);
            MockEntity instance1 = new MockEntity {NullableNumber = 3, Number = 4, String = "false"};
            instance1.SetId(10);

            // Post-Conditions
            Assert.IsTrue(instance0.Equals(instance1), "The method 'Equals' is wrong.");
            Assert.IsTrue(instance0 == instance1, "The operation '==' is wrong.");
            Assert.IsFalse(instance0 != instance1, "The operation '!=' is wrong.");
        }

        [Test]
        public void EqualsFalseForPersistentTest()
        {
            // Pre-Conditions 
            MockEntity instance0 = new MockEntity {NullableNumber = 1, Number = 2, String = "true"};
            instance0.SetId(10);
            MockEntity instance1 = new MockEntity {NullableNumber = 1, Number = 2, String = "false"};
            instance1.SetId(11);

            // Post-Conditions
            Assert.IsFalse(instance0.Equals(instance1), "The method 'Equals' is wrong.");
            Assert.IsFalse(instance0 == instance1, "The operation '==' is wrong.");
            Assert.IsTrue(instance0 != instance1, "The operation '!=' is wrong.");
        }

        [Test]
        public void GetHashCodeForTransientTest()
        {
            // Pre-Conditions 
            MockEntity instance0 = new MockEntity {NullableNumber = 1, Number = 2, String = "true"};
            MockEntity instance1 = new MockEntity {NullableNumber = 1, Number = 2, String = "false"};

            // Post-Conditions
            Assert.AreEqual(1630019797, instance0.GetHashCode(), "The hash code is wrong.");
            Assert.AreEqual(instance0.GetHashCode(), instance1.GetHashCode(), "The instance hash codes are wrong.");
        }

        [Test]
        public void GetHashCode_TransientWithoutBusinessKeyAttribute_Test()
        {
            // Pre-Conditions 
            MockEntityParent instance0 = new MockEntityParent();
            MockEntityParent instance1 = new MockEntityParent();

            // Post-Conditions
            Assert.AreNotEqual(0, instance0.GetHashCode(), "The hash code is wrong.");
            Assert.AreNotEqual(instance0.GetHashCode(), instance1.GetHashCode(), "The instance hash codes are wrong.");
        }

        [Test]
        public void GetHashCodeForPersistentTest()
        {
            // Pre-Conditions 
            MockEntity instance0 = new MockEntity {NullableNumber = 1, Number = 2, String = "true"};
            instance0.SetId(10);
            MockEntity instance1 = new MockEntity {NullableNumber = 3, Number = 4, String = "false"};
            instance1.SetId(10);

            // Post-Conditions
            Assert.AreEqual(40381, instance0.GetHashCode(), "The hash code is wrong.");
            Assert.AreEqual(instance0.GetHashCode(), instance1.GetHashCode(), "The instance hash codes are wrong.");
        }

        [Test]
        public void EqualsOnlyIdTest()
        {
            // Post-Conditions
            Assert.AreEqual(new OnlyIdEntity(10), new OnlyIdEntity(10), "The entities are not equal.");
            Assert.AreNotEqual(new OnlyIdEntity(0), new OnlyIdEntity(10), "The entities are equal.");
        }

        [Test]
        public void ToString_Test()
        {
            // Pre-Conditions
            _instance.SetProperty(x => x.Id, 100);

            // Action
            string result = _instance.ToString();

            // Post-Conditions
            Assert.AreEqual("Allag.Core.TestData.Data.Persistence.MockEntity(Id: 100)", result);
        }
        #endregion

        #region Classes
        public class OnlyIdEntity : Entity<IMockDaoFactory, OnlyIdEntity, int>
        {
            #region Constructors
            public OnlyIdEntity()
            {
            }

            public OnlyIdEntity(int id)
            {
                Id = id;
            }
            #endregion
        }
        #endregion
    }
}