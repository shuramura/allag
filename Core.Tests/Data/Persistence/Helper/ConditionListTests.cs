#region Using directives
using System;
using System.Linq;
using Allag.Core.Reflection;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Persistence.Helper
{
    [TestFixture]
    public class ConditionListTests
    {
        #region Variables
        private ConditionList _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new ConditionList(typeof(ConditionListTests), true);
        }

        [TearDown]
        public void TearDown()
        {
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.IsTrue(_instance.IsAndGroup, "The property 'IsAndGroup' is wrong.");
            Assert.AreEqual(typeof(ConditionListTests), _instance.EntityType, "The property 'EntityType' is wrong.");
            Assert.AreEqual(0, _instance.Groups.Count(), "The property 'Groups' is wrong.");
        }

        [Test]
        public void Ctor_NullEntityType_Test()
        {
            // Action
            Assert.That(() => new ConditionList(null, true), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void AddGroup_SameType_Test()
        {
            // Pre-Conditions
            bool isAndGroup = _instance.IsAndGroup;

            // Action
            ConditionList result = _instance.AddGroup(isAndGroup);

            // Post-Conditions
            Assert.AreEqual(_instance, result, "The result is wrong.");
            Assert.AreEqual(isAndGroup, result.IsAndGroup, "The property 'IsAndGroup' is wrong.");
        }

        [Test]
        public void AddGroup_DifferentType_Test()
        {
            // Pre-Conditions
            bool isAndGroup = !_instance.IsAndGroup;

            // Action
            ConditionList result = _instance.AddGroup(isAndGroup);

            // Post-Conditions
            Assert.AreNotEqual(_instance.IsAndGroup, result.IsAndGroup, "The result is wrong.");
            Assert.AreEqual(isAndGroup, result.IsAndGroup, "The property 'IsAndGroup' is wrong.");
        }

        [Test]
        public void ToStringTest()
        {
            // Pre-Conditions 

            _instance.AddRange(new[]
            {
                new Condition(new string[0], new Tuple<string, Type>("test0", null), null, Operation.Equal, false, new object[]{1}), 
                new Condition(new string[0], new Tuple<string, Type>("test1", null), null, Operation.Equal, false, new object[]{2})
            });
            ConditionList list = _instance.AddGroup(false);
            list.AddRange(new[]
            {
                new Condition(new string[0], new Tuple<string, Type>("test2", null), null, Operation.Equal, false, new object[]{3}), 
                new Condition(new string[0], new Tuple<string, Type>("test2", null), null, Operation.Equal, false, new object[]{4})
            });
            
            // Action
            string result = _instance.ToString();

            // Post-Conditions
            Assert.AreEqual("test0 Equal [1] And test1 Equal [2] And (test2 Equal [3] Or test2 Equal [4])", result);
        }

        [Test]
        public void ToStringWithNotTest()
        {
            // Pre-Conditions 
            _instance.SetProperty(x => x.IsNot, true);
            _instance.AddRange(new[]
            {
                new Condition(new string[0], new Tuple<string, Type>("test0", null), null, Operation.Equal, false, new object[]{1}), 
                new Condition(new string[0], new Tuple<string, Type>("test1", null), null, Operation.Equal, false, new object[]{2})
            });
            ConditionList list = _instance.AddGroup(false);
            list.AddRange(new[]
            {
                new Condition(new string[0], new Tuple<string, Type>("test2", null), null, Operation.Equal, false, new object[]{3}), 
                new Condition(new string[0], new Tuple<string, Type>("test2", null), null, Operation.Equal, false, new object[]{4})
            });
            

            // Action
            string result = _instance.ToString();

            // Post-Conditions
            Assert.AreEqual("Not(test0 Equal [1] And test1 Equal [2] And (test2 Equal [3] Or test2 Equal [4]))", result);
        }
        #endregion
    }
}