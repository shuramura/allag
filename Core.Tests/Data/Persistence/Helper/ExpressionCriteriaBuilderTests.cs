#region Using directives
using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using Allag.Core.TestData.Data.Persistence;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Persistence.Helper
{
    [TestFixture]
    public class ExpressionCriteriaBuilderTests
    {
        #region Variables
        private ExpressionCriteriaBuilder _insatnce;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _insatnce = new ExpressionCriteriaBuilder();
        }
        #endregion

        #region Tests
        [Test]
        public void GetConditionsNullExpressionTest()
        {
            // Action
            Assert.That(() => _insatnce.Build(typeof(MockEntity), null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void GetConditionsConjuctionAndTest()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> criteria = x => x.Number == 1 && x.Number == 5;

            // Action
            ConditionList result = _insatnce.Build(typeof(MockEntity), criteria);

            // Post-Conditions
            Assert.IsTrue(result.IsAndGroup, "The junction type is wrong.");
            Assert.AreEqual(false, result.IsNot, "The complement type is wrong.");
        }

        [Test]
        public void GetConditionsConjuctionOrTest()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> criteria = x => x.Number == 1 || x.Number == 5;

            // Action
            ConditionList result = _insatnce.Build(typeof(MockEntity), criteria);

            // Post-Conditions
            Assert.AreEqual(1, result.Groups.Count(), "The group count is wrong.");
            Assert.AreEqual(0, result.Count(), "The condition count is wrong.");
            result = result.Groups.First();
            Assert.IsFalse(result.IsAndGroup, "The junction type is wrong.");
            Assert.AreEqual(false, result.IsNot, "The complement type is wrong.");
        }

        [Test]
        public void GetConditionsTest()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> criteria = x => x.Number < 1 && x.Number <= 2 && x.Number == 3
                                                               || x.Number >= 4 || x.Number > 5 && (x.Number != 6 || OpMethods.Like(x.String, "7")) || "lalala" == "lala";

            // Action
            ConditionList result = _insatnce.Build(typeof(MockEntity), criteria);

            // Post-Conditions
            Assert.AreEqual(1, result.Groups.Count(), "The group count is wrong.");
            Assert.AreEqual(0, result.Count(), "The condition count is wrong.");
            result = result.Groups.First();
            Assert.AreEqual(2, result.Count, "The count of conditions is wrong.");
            Assert.AreEqual(2, result.Groups.Count(), "The count of groups is wrong.");
        }

        [Test]
        public void GetConditionsConvertPropertyTest()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> criteria = x => x.Attr == FileAttributes.Directory;

            // Action
            ConditionList result = _insatnce.Build(typeof(MockEntity), criteria);

            // Post-Conditions
            Assert.AreEqual(1, result.Count, "The count of conditions is wrong.");
            Assert.IsInstanceOf<FileAttributes>(result[0].Value, "The value type is wrong.");
        }

        [Test]
        public void GetConditions_WithSubRequest_Test()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> criteria = x => OpMethods.Contains(x.Children, y => y.Boolean);

            // Action
            ConditionList result = _insatnce.Build(typeof(MockEntity), criteria);

            // Post-Conditions
            Assert.AreEqual(1, result.Count, "The count of conditions is wrong.");
            Assert.IsInstanceOf<ConditionList>(result[0].Value, "The value type is wrong.");
        }

        [Test]
        public void GetConditionsComplexPropertyTest()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> criteria = x => x.Parent.Entity != null && x.Parent.Entity.Number == 0;

            // Action
            ConditionList result = _insatnce.Build(typeof(MockEntity), criteria);

            // Post-Conditions
            Assert.AreEqual(2, result.Count, "The count of conditions is wrong.");
            Assert.AreEqual(1, result[0].NamePrefixes.Count, "The count of first condition is wrong.");
            Assert.AreEqual(2, result[1].NamePrefixes.Count, "The count of second condition is wrong.");
        }

        [Test]
        public void GetConditionsConstantTest()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> criteria = x => 10 == 1;

            // Action
            ConditionList result = _insatnce.Build(typeof(MockEntity), criteria);

            // Post-Conditions
            Assert.AreEqual(1, result.Count, "The count of conditions is wrong.");
            Assert.AreEqual(Operation.Constant, result[0].Op, "The operation type is wrong.");
            Assert.AreEqual(0, result.Groups.Count(), "The count of groups is wrong.");
        }

        [Test]
        public void GetConditionsConstantEqualTest()
        {
            // Pre-Conditions
            const int i = 10;
            Expression<Func<MockEntity, bool>> criteria = x => i == 1;

            // Action
            ConditionList result = _insatnce.Build(typeof(MockEntity), criteria);

            // Post-Conditions
            Assert.AreEqual(1, result.Count, "The count of conditions is wrong.");
            Assert.AreEqual(Operation.Constant, result[0].Op, "The operation type is wrong.");
            Assert.AreEqual(0, result.Groups.Count(), "The count of groups is wrong.");
        }

        [Test]
        public void GetConditionsWrongMethodTest()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> criteria = x => x.String.Contains("test");

            // Action
            Assert.That(() => _insatnce.Build(typeof(MockEntity), criteria), Throws.InvalidOperationException);
        }

        [Test]
        public void GetConditionsWrongExpressionTypeTest()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> criteria = x => x.Number == 1 ? true : false;

            // Action
            Assert.That(() => _insatnce.Build(typeof(MockEntity), criteria), Throws.InvalidOperationException);
        }

        [Test]
        public void GetConditionsWrongNameValueOrderTest()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> criteria = x => 1 == x.Number;

            // Action
            Assert.That(() => _insatnce.Build(typeof(MockEntity), criteria), Throws.InvalidOperationException);
        }

        [Test]
        public void GetConditionsBooleanPropertyTest()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> criteria = x => x.Boolean;

            // Action
            ConditionList result = _insatnce.Build(typeof(MockEntity), criteria);

       
            // Post-Conditions
            Assert.IsFalse(result.IsNot, "The list complement is wrong.");
            Assert.AreEqual(Operation.Equal, result[0].Op, "The operation is wrong.");
            Assert.AreEqual(true, result[0].Value, "The value is wrong.");
            Assert.IsFalse(result[0].IsNot, "The complement is wrong.");
        }

        [Test]
        public void GetConditionsBooleanPropertyWithNotTest()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> criteria = x => !x.Boolean;

            // Action
            ConditionList result = _insatnce.Build(typeof(MockEntity), criteria);

            // Post-Conditions
            Assert.IsFalse(result.IsNot, "The list complement is wrong.");
            Assert.AreEqual(Operation.Equal, result[0].Op, "The operation is wrong.");
            Assert.AreEqual(true, result[0].Value, "The value is wrong.");
            Assert.IsTrue(result[0].IsNot, "The complement is wrong.");
        }

        [Test]
        public void GetConditionsMoreThenOnePropertiesWithNotTest()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> criteria = x => !x.Boolean && x.Number > 0;

            // Action
            ConditionList result = _insatnce.Build(typeof(MockEntity), criteria);

            // Post-Conditions
            Assert.IsFalse(result.IsNot, "The list complement is wrong.");
            Assert.AreEqual(Operation.Equal, result[0].Op, "The operation is wrong.");
            Assert.AreEqual(true, result[0].Value, "The value is wrong.");
            Assert.IsTrue(result[0].IsNot, "The complement is wrong.");
        }

        [Test]
        public void GetConditionsListWithNotTest()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> criteria = x => !(x.Boolean && x.Number > 0);

            // Action
            ConditionList result = _insatnce.Build(typeof(MockEntity), criteria);

            // Post-Conditions
            Assert.IsTrue(result.IsNot, "The list complement is wrong.");
            Assert.AreEqual(Operation.Equal, result[0].Op, "The operation is wrong.");
            Assert.AreEqual(true, result[0].Value, "The value is wrong.");
            Assert.IsFalse(result[0].IsNot, "The complement is wrong.");
        }

        [Test]
        public void GetConditionsListWithDoubleNotTest()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> criteria = x => !(!(x.Boolean && x.Number > 0));

            // Action
            ConditionList result = _insatnce.Build(typeof(MockEntity), criteria);

            // Post-Conditions
            Assert.IsFalse(result.IsNot, "The list complement is wrong.");
            Assert.AreEqual(Operation.Equal, result[0].Op, "The operation is wrong.");
            Assert.AreEqual(true, result[0].Value, "The value is wrong.");
            Assert.IsFalse(result[0].IsNot, "The complement is wrong.");
        }
        #endregion
    }
}