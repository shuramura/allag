#region Using directives

using System;
using System.Linq;
using Allag.Core.Reflection;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Persistence.Helper
{
    [TestFixture]
    public class ConditionTests
    {
        #region Variables
        private Condition _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new Condition(new [] {"1", "2", "3"}, new Tuple<string, Type>("test", GetType()), null, Operation.Equal, false, new object[]{10});
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        #region Tests
        [TestCase(null, new object[0],  Operation.Equal, null)]
        [TestCase(new object[] { 1, 2, 3 }, new object[] { 1, 2, 3 }, Operation.Property, "123test")]
        [TestCase(new object[] { 10 }, new object[] { 10 }, Operation.Sum, "123testSum")]
        public void CtorTest(object[] values, object[] expectedValues, Operation operation, string alias)
        {
            // Action
            Condition instance = new Condition(new[] { "1", "2", "3" }, new Tuple<string, Type>("test", GetType()), null, operation, false, values);

			// Post-Conditions
            CollectionAssert.AreEqual(new[] {"1", "2", "3"}, instance.NamePrefixes, "The property 'NamePrefixes' is wrong.");
            Assert.AreEqual(new Tuple<string, Type>("test", GetType()), _instance.Name, "The property 'Name' is wrong.");
            Assert.AreEqual(operation, instance.Op, "The property 'Op' is wrong.");
            CollectionAssert.AreEqual(expectedValues, instance.Values, "The property 'Value' is wrong.");
            Assert.AreEqual(values == null ? null : values[0], instance.Value, "The property 'Value' is wrong.");
            Assert.IsFalse(instance.IsNot, "The complement is wrong.");
            Assert.AreEqual(alias, instance.Alias, "The property 'OutputName' is wrong.");
        }

        [Test]
        public void CtorNullObjectsTest()
        {
            // Action
            Condition instance = new Condition(new[] { "1" }, new Tuple<string, Type>("test", GetType()), null, Operation.NotEqual, false, null);

            // Post-Conditions
            Assert.AreEqual(0, instance.Values.Count(), "The property 'Values' is wrong.");
        }

        [Test]
        public void ToString_Test()
        {
            // Action
            string result = _instance.ToString();

            // Post-Conditions
            Assert.AreEqual("1.2.3.test Equal [10]", result);
        }

        [Test]
        public void ToString_ArrayValue_Test()
        {
            // Pre-Conditions
            Condition instance = new Condition(null, new Tuple<string, Type>("test", GetType()), "alias", Operation.Contains, false, new object[]{new int[]{1, 2}, 3});

            // Action
            string result = instance.ToString();

            // Post-Conditions
            Assert.AreEqual("test Contains [[1], [2]], [3]", result);
        }

        [Test]
        public void ToStringConstTest()
        {
            // Pre-Conditions
            Condition instance = new Condition(null, null, null, Operation.Constant, false, new object[]{10});

            // Action
            string result = instance.ToString();

            // Post-Conditions
            Assert.AreEqual(string.Format("[{0}]", instance.Value), result);
        }

        [Test]
        public void ToStringNotTest()
        {
            // Pre-Conditions
            _instance.SetProperty(x => x.IsNot, true);

            // Action
            string result = _instance.ToString();

            // Post-Conditions
            Assert.AreEqual("Not(1.2.3.test Equal [10])", result);
        }

        [Test]
        public void IsNotSetTest()
        {
            // Pre-Conditions 
            bool expected = !_instance.IsNot;

            // Action
            _instance.SetProperty(x => x.IsNot, expected);

            // Post-Conditions
            Assert.AreEqual(expected, _instance.IsNot);
        }
        #endregion
    }
}
