﻿#region Using directives
using Allag.Core.TestData.Data.Persistence;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Persistence
{
    [TestFixture]
    public class EntityWithIdTests
    {
        #region Variables
        private MockEntity _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new MockEntity();
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(0, _instance.Id, "The property 'Id' is wrong.");
        }

        [Test]
        public void SetIdTest()
        {
            // Pre-Conditions
            const int expected = 100;

            // Action
            MockEntity result = _instance.SetId(expected);

            // Post-Conditions
            Assert.AreEqual(_instance, result, "The result is wrong.");
            Assert.AreEqual(expected, result.Id, "The property 'Id' is wrong.");
        }
        #endregion
    }
}