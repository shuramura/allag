﻿#region Using directives
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Persistence
{
    [TestFixture]
    public class EntityNotFoundExceptionTests
    {
        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Pre-Conditions
            const int nEntityId = 100;

            // Action
            EntityNotFoundException<EntityNotFoundExceptionTests, int> instance = new EntityNotFoundException<EntityNotFoundExceptionTests, int>(nEntityId);

            // Post-Conditions
            Assert.AreEqual(typeof(EntityNotFoundExceptionTests), instance.EntityType, "The property 'EntityType' is wrong.");
            Assert.AreEqual(nEntityId, instance.EntityId, "The property 'EntityId' is wrong.");
        }
        #endregion
    }
}