#region Using directives

using System;
using System.Configuration;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Data.Attributes
{
    [TestFixture]
    public class BaseValidatorAttributeTests
    {
        #region Variables
        private MockRepository _mocks;
        private BaseValidatorAttribute _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _mocks = new MockRepository();
            _instance = _mocks.PartialMock<BaseValidatorAttribute>(new ConfigurationValidatorAttribute(typeof(DefaultValidator)));
            _mocks.ReplayAll();
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        #region Tests
        [Test]
        public void CtorNullParameterTest()
        {
            // Action
            Assert.That(() => _mocks.PartialMock<BaseValidatorAttribute>((ConfigurationValidatorAttribute)null), Throws.InnerException.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CtorTest()
        {
			// Post-Conditions
            Assert.IsNotNull(_instance.ValidatorInstance, "The property 'ValidatorInstance' is wrong.");
        }

        #endregion
    }
}
