#region Using directives

using System.Data;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Attributes
{
    [TestFixture]
    public class FieldAttributeTests
    {
        #region Variables
        private FieldAttribute _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new FieldAttribute();
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(true, _instance.IsNullable, "The property 'IsNullable' is wrong.");
            Assert.AreEqual(int.MaxValue, _instance.Order, "The property 'Order' is wrong.");
            Assert.AreEqual(ParameterDirection.InputOutput, _instance.Direction, "The property 'Direction' is wrong.");
            Assert.AreEqual(null, _instance.DbType, "The property 'DbType' is wrong.");
            Assert.AreEqual(null, _instance.Name, "The property 'Name' is wrong.");
            
            Assert.AreEqual(0, _instance.Precision, "The property 'Precision' is wrong.");
            Assert.AreEqual(0, _instance.Scale, "The property 'Scale' is wrong.");
            Assert.AreEqual(-1, _instance.Size, "The property 'Size' is wrong.");
            Assert.AreEqual(-1, _instance.MaxSize, "The property 'MaxSize' is wrong.");
            Assert.IsFalse(_instance.IsSimpleField, "The property 'IsSimpleField' is wrong.");
        }

        [Test]
        public void CtorWithDbTypeTest()
        {
            // Pre-Condition
            FieldAttribute instance = new FieldAttribute(DbType.Binary);

            // Post-Conditions
            Assert.AreEqual(true, instance.IsNullable, "The property 'IsNullable' is wrong.");
            Assert.AreEqual(int.MaxValue, instance.Order, "The property 'Order' is wrong.");
            Assert.AreEqual(ParameterDirection.InputOutput, instance.Direction, "The property 'Direction' is wrong.");
            Assert.AreEqual(DbType.Binary, instance.DbType, "The property 'DbType' is wrong.");
            Assert.AreEqual(null, instance.Name, "The property 'Name' is wrong.");
            
            Assert.AreEqual(0, instance.Precision, "The property 'Precision' is wrong.");
            Assert.AreEqual(0, instance.Scale, "The property 'Scale' is wrong.");
            Assert.AreEqual(-1, instance.Size, "The property 'Size' is wrong.");
            Assert.AreEqual(-1, instance.MaxSize, "The property 'MaxSize' is wrong.");
        }

        [Test]
        public void NameSetTest()
        {
            // Pre-Conditions 
            const string expected = "lalala";

            // Action
            _instance.Name = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Name);
        }

        [Test]
        public void IsNullableSetTest()
        {
            // Pre-Conditions 
            bool expected = !_instance.IsNullable;

            // Action
            _instance.IsNullable = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.IsNullable);
        }

        [Test]
        public void OrderSetTest()
        {
            // Pre-Conditions 
            int expected = _instance.Order + 100;

            // Action
            _instance.Order = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Order);
        }

        [Test]
        public void DirectionSetTest()
        {
            // Pre-Conditions 
            const ParameterDirection expected = ParameterDirection.InputOutput;

            // Action
            _instance.Direction = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Direction);
        }

        [Test]
        public void DbTypeSetTest()
        {
            // Pre-Conditions 
            const DbType expected = DbType.Xml;

            // Action
            _instance.DbType = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.DbType);
        }

        [Test]
        public void PrecisionSetTest()
        {
            // Pre-Conditions 
            const byte expected = 100;

            // Action
            _instance.Precision = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Precision);
        }

        [Test]
        public void ScaleSetTest()
        {
            // Pre-Conditions 
            const byte expected = 100;

            // Action
            _instance.Scale = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Scale);
        }

        [Test]
        public void SizeSetTest()
        {
            // Pre-Conditions 
            const int expected = 100;

            // Action
            _instance.Size = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Size, "The property 'Size' is wrong.");
            Assert.AreEqual(expected, _instance.MaxSize, "The property 'MaxSize' is wrong.");
        }

        [Test]
        public void MaxSizeSetTest()
        {
            // Pre-Conditions 
            const int expected = 100;

            // Action
            _instance.MaxSize = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.MaxSize);
        }
        #endregion
    }
}
