#region Using directives

using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Attributes
{
    [TestFixture]
    public class CallbackValidatorAttributeTests
    {
        #region Variables
        private CallbackValidatorAttribute _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new CallbackValidatorAttribute();
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
			// Post-Conditions
            Assert.AreEqual(string.Empty, _instance.CallbackMethodName, "The property 'CallbackMethodName' is wrong.");
            Assert.AreEqual(null, _instance.Type, "The property 'Type' is wrong.");
        }

        [Test]
        public void CallbackMethodNameSetTest()
        {
            // Pre-Conditions 
            const string expected = "lalala";

            // Action
            _instance.CallbackMethodName = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.CallbackMethodName);
        }

        [Test]
        public void TypeSetTest()
        {
            // Pre-Conditions 
            Type expected = GetType();

            // Action
            _instance.Type = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Type);
        }
        #endregion
    }
}
