#region Using directives

using System;
using System.Globalization;
using Allag.Core.TestData.Tools;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Attributes
{
    [TestFixture]
    public class ConvertAttributeTests
    {
        #region Variables
        private ConvertAttribute _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new ConvertAttribute(typeof(string), "Convert");
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.IsNull(_instance.ConverterType, "The property 'ConverterType' is wrong.");
            Assert.AreEqual("Convert", _instance.MethodName, "The property 'MethodName' is wrong.");
            Assert.AreEqual(typeof(string), _instance.ToType, "The property 'ToType' is wrong.");
            Assert.IsNull(_instance.TypeConverterType, "The property 'TypeConverterType' is wrong.");
            Assert.AreEqual(-1, _instance.Order, "The property 'Order' is wrong.");
        }

        [Test]
        public void CtorEmptyTest()
        {
            // Action
            ConvertAttribute instance = new ConvertAttribute(typeof(int));

            // Post-Conditions
            Assert.IsNull(_instance.ConverterType, "The property 'ConverterType' is wrong.");
            Assert.IsNull(instance.MethodName, "The property 'MethodName' is wrong.");
            Assert.AreEqual(typeof(int), instance.ToType, "The property 'ToType' is wrong.");
            Assert.IsNull(instance.TypeConverterType, "The property 'TypeConverterType' is wrong.");
            Assert.AreEqual(-1, instance.Order, "The property 'Order' is wrong.");
        }

        [Test]
        public void CtorNullToTypeTest()
        {
            // Action 
            Assert.That(() => new ConvertAttribute(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CtorOnlyTypeTest()
        {
            // Action
            ConvertAttribute instance = new ConvertAttribute(typeof(int));

            // Post-Conditions
            Assert.IsNull(instance.ConverterType, "The property 'ConverterType' is wrong.");
            Assert.IsNull(instance.MethodName, "The property 'MethodName' is wrong.");
            Assert.AreEqual(typeof(int), instance.ToType, "The property 'ToType' is wrong.");
            Assert.IsNull(_instance.TypeConverterType, "The property 'TypeConverterType' is wrong.");
        }

        [Test]
        public void CtorWithTypeConverterTest()
        {
            // Action
            ConvertAttribute instance = new ConvertAttribute(typeof(int), typeof(MockBaseTypeConverter));

            // Post-Conditions
            Assert.IsNull(instance.ConverterType, "The property 'ConverterType' is wrong.");
            Assert.IsNull(instance.MethodName, "The property 'MethodName' is wrong.");
            Assert.AreEqual(typeof(int), instance.ToType, "The property 'ToType' is wrong.");
            Assert.AreEqual(typeof(MockBaseTypeConverter), instance.TypeConverterType, "The property 'TypeConverterType' is wrong.");
        }

        [Test]
        public void ConverterTypeSetTest()
        {
            // Pre-Conditions 
            Type expected = GetType();

            // Action
            _instance.ConverterType = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.ConverterType);
        }

        [Test]
        public void TypeConverterTypeSetTest()
        {
            // Pre-Conditions 
            Type expected = GetType();

            // Action
            _instance.TypeConverterType = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.TypeConverterType);
        }

        [Test]
        public void MethodNameSetTest()
        {
            // Pre-Conditions 
            const string expected = "lalala";

            // Action
            _instance.MethodName = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.MethodName);
        }

        [Test]
        public void ToTypeSetTest()
        {
            // Pre-Conditions 
            Type expected = GetType();

            // Action
            _instance.ToType = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.ToType);
        }

        [Test]
        public void InitializeWithNullInstanceTypeTest()
        {
            // Action
            Assert.That(() => _instance.Initialize(null, "lalala", typeof(int)), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void InitializeWithWrongParameterNameTest([Values(null, "", "  ")] string parameterName)
        {
            // Action
            Assert.That(() => _instance.Initialize(GetType(), parameterName, typeof(int)), Throws.ArgumentException);
        }

        [Test]
        public void InitializeWithNullParameterTypeTest()
        {
            // Action
            Assert.That(() => _instance.Initialize(GetType(), "Int", null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void ConvertToWithoutInitializationTest()
        {
            // Action
            Assert.That(() => _instance.ConvertTo(this, 101), Throws.InvalidOperationException);
        }

        [Test]
        public void ConvertToTest()
        {
            // Pre-Conditions 
            Int = 100;
            const string expected = "101";
            _instance.Initialize(GetType(), "Int", typeof(int));

            // Action
            object result = _instance.ConvertTo(this, 101);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ConvertToStaticMethodTest()
        {
            // Pre-Conditions 
            _instance.ConverterType = GetType();
            Int = 100;
            const string expected = "101";
            _instance.Initialize(GetType(), "Int", typeof(int));

            // Action
            object result = _instance.ConvertTo(this, 101);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ConvertToEmptyCtorTest()
        {
            // Pre-Conditions 
            ConvertAttribute instance = new ConvertAttribute(typeof(int));
            Int = 100;
            int expected = Int + 1;
            instance.Initialize(GetType(), "Int", typeof(int));

            // Action
            object result = instance.ConvertTo(this, expected);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ConvertFromWithoutInitializationTest()
        {
            // Action
            Assert.That(() => _instance.ConvertFrom(this, "100"), Throws.InvalidOperationException);
        }

        [Test]
        public void ConvertFromTest()
        {
            // Pre-Conditions 
            Int = 100;
            const string str = "101";
            const int expected = 101;
            _instance.Initialize(GetType(), "Int", typeof(int));

            // Action
            object result = _instance.ConvertFrom(this, str);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ConvertFromStaticMethodTest()
        {
            // Pre-Conditions 
            _instance.ConverterType = GetType();

            Int = 100;
            const string str = "101";
            const int expected = 101;
            _instance.Initialize(GetType(), "Int", typeof(int));

            // Action
            object result = _instance.ConvertFrom(this, str);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ConvertFromByUsingInbuildFunctionalityTest()
        {
            // Pre-Conditions 
            ConvertAttribute instance = new ConvertAttribute(typeof(string));
            
            Int = 100;
            const string str = "101";
            const int expected = 101;
            instance.Initialize(GetType(), "Int", typeof(int));

            // Action
            object result = instance.ConvertFrom(this, str);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ConvertToByUsingInbuildFunctionalityTest()
        {
            // Pre-Conditions 
            ConvertAttribute instance = new ConvertAttribute(typeof(string));

            Int = 100;
            const string expected = "100";
            instance.Initialize(GetType(), "Int", typeof(int));

            // Action
            object result = instance.ConvertTo(this, Int);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ConvertFromWithTypeConverterTest()
        {
            // Pre-Conditions
            int nConvertFrom = 0;
            int nConvertTo = 0;
            MockBaseTypeConverter.ConvertingFrom += delegate { nConvertFrom++; };
            MockBaseTypeConverter.ConvertingTo += delegate { nConvertTo++; };

            ConvertAttribute instance = new ConvertAttribute(typeof(string), typeof(MockBaseTypeConverter));
            instance.Initialize(GetType(), "Float", typeof(float));

            // Action
            instance.ConvertFrom(this, "100");


            // Post-Conditions
            Assert.AreEqual(1, nConvertFrom, "The convert from count is wrong.");
            Assert.AreEqual(0, nConvertTo, "The convert to count is wrong.");
        }

        [Test]
        public void ConvertToWithTypeConverterTest()
        {
            // Pre-Conditions
            int nConvertFrom = 0;
            int nConvertTo = 0;
            MockBaseTypeConverter.ConvertingFrom += delegate { nConvertFrom++; };
            MockBaseTypeConverter.ConvertingTo += delegate { nConvertTo++; };

            ConvertAttribute instance = new ConvertAttribute(typeof(string), typeof(MockBaseTypeConverter));
            instance.Initialize(GetType(), "Float", typeof(float));

            // Action
            instance.ConvertTo(this, 100f);


            // Post-Conditions
            Assert.AreEqual(0, nConvertFrom, "The convert from count is wrong.");
            Assert.AreEqual(1, nConvertTo, "The convert to count is wrong.");
        }

        [Test]
        public void ConvertToByUsingDefaultConverMethodTest()
        {
            // Pre-Conditions 
            ConvertAttribute instance = new ConvertAttribute(typeof(string));

            Int = 100;
            const string expected = "Default Method";
            instance.Initialize(GetType(), "DoubleProp", typeof(double));

            // Action
            object result = instance.ConvertTo(this, 123.4d);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }
        #endregion

        #region Properties
        public int Int { get; set; }

        public float Float { get; set; }

        public double DoubleProp { get; set; }
        public short[] Shorts { get; set; }
        #endregion

        #region Methods
        public double DoubleFrom(string value, string propertyName)
        {
            return 10;
        }

        public string DoubleTo(double value, string propertyName)
        {
            return "Default Method";
        }

        public static string ConvertTo(int value, string propertyName, ConvertAttributeTests instance)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        public string ConvertTo(int value, string propertyName)
        {
            return ConvertTo(value, propertyName, null);
        }

        public static int ConvertFrom(string value, string propertyName, ConvertAttributeTests instance)
        {
            return int.Parse(value);
        }

        public int ConvertFrom(string value, string propertyName)
        {
            return ConvertFrom(value, propertyName, null);
        }

        #endregion
    }
}
