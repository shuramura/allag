#region Using directives

using System.Configuration;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Attributes
{
    [TestFixture]
    public class RegexStringValidatorAttributeTests
    {
        #region Variables
        private RegexStringValidatorAttribute _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new RegexStringValidatorAttribute("lalala");
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
        }
        #endregion
    }
}
