#region Using directives

using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data
{
    [TestFixture]
    public class ObjectHelperTests
    {
        #region Variables
        private TypeDescriptionTests.Test<int> _object;
        private ObjectHelper _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _object = new TypeDescriptionTests.Test<int>();
            _instance = new ObjectHelper(_object);
            _instance.Object = _object;
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        #region Tests

        [Test]
        public void CtorWithObjectTypeTest()
        {
            // Pre-Conditions
            ObjectHelper instance = new ObjectHelper(typeof(TypeDescriptionTests.Test<int>));

            // Post-Conditions
            Assert.AreEqual(null, instance.Object, "The property 'Object' is wrong.");

            Assert.AreEqual(10, instance.Fields.Count, "The property 'Fields' is wrong.");
            Assert.AreEqual("Id", instance.Fields[0].ParameterName, "The property 'Fields[0]' is wrong.");
            Assert.AreEqual("Two", instance.Fields[1].ParameterName, "The property 'Fields[1]' is wrong.");
            Assert.AreEqual("One", instance.Fields[2].ParameterName, "The property 'Fields[2]' is wrong.");
            Assert.AreEqual("Three", instance.Fields[3].ParameterName, "The property 'Fields[3]' is wrong.");

            Assert.AreEqual(8, instance.InputFields.Count, "The property 'InputFields' is wrong.");
            Assert.AreEqual(10, instance.OutputFields.Count, "The property 'OutputFields' is wrong.");
        }

        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(_object, _instance.Object, "The property 'Object' is wrong.");
            Assert.AreEqual(0, _object.Id, "The property 'Id' is wrong.");

            Assert.AreEqual(10, _instance.Fields.Count, "The property 'Fields' is wrong.");
            Assert.AreEqual("Id", _instance.Fields[0].ParameterName, "The property 'Fields[0]' is wrong.");
            Assert.AreEqual("Two", _instance.Fields[1].ParameterName, "The property 'Fields[1]' is wrong.");
            Assert.AreEqual("One", _instance.Fields[2].ParameterName, "The property 'Fields[2]' is wrong.");
            Assert.AreEqual("Three", _instance.Fields[3].ParameterName, "The property 'Fields[3]' is wrong.");

            Assert.AreEqual(8, _instance.InputFields.Count, "The property 'InputFields' is wrong.");
            Assert.AreEqual(10, _instance.OutputFields.Count, "The property 'OutputFields' is wrong.");
        }

        [Test]
        public void CtorWithArrayFieldTest()
        {
            // Pre-Conditions
            ObjectHelper instance = new ObjectHelper(typeof(TypeDescriptionTests.TestArray));

            // Post-Conditions
            Assert.AreEqual(null, instance.Object, "The property 'Object' is wrong.");

            Assert.AreEqual(3, instance.Fields.Count, "The property 'Fields' is wrong.");
            Assert.AreEqual("Int", instance.Fields[0].ParameterName, "The property 'Fields[0]' is wrong.");
            Assert.AreEqual("IntArray", instance.Fields[1].ParameterName, "The property 'Fields[1]' is wrong.");
            Assert.AreEqual("Array", instance.Fields[2].ParameterName, "The property 'Fields[2]' is wrong.");
        }

        [Test]
        public void CtorWithNullObjectTest()
        {
            // Action
            Assert.That(() => new ObjectHelper((object)null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CtorWithNullTypeTest()
        {
            // Action
            Assert.That(() => new ObjectHelper(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CtorWithNullObjectWithIdValueTest()
        {
            // Action
            Assert.That(() => new ObjectHelper(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void ObjectSetTest()
        {
            // Pre-Conditions
            TypeDescriptionTests.TestInt expected = new TypeDescriptionTests.TestInt();
            // Action
            _instance.Object = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Object);
        }

        [Test]
        public void ObjectGetForComplexFieldTest()
        {
            // Pre-Conditions
            DataParameter dataParameter = null;
            foreach (DataParameter parameter in _instance.Fields)
            {
                if (parameter.ParameterName == "ComplexOne")
                {
                    dataParameter = parameter;
                    break;
                }
            }
            const string expected = "lalala";
           
            // Action
            dataParameter.Value = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _object.TestComplexField.ComplexOne);
        }

        [Test]
        public void ObjectSetNullTest()
        {
            // Action
            Assert.That(() => _instance.Object = null, Throws.ArgumentException);
        }

        [Test]
        public void ObjectSetWrongTypeTest()
        {
            // Action
            Assert.That(() => _instance.Object = new TypeDescriptionTests.TestId(), Throws.ArgumentException);
        }
        #endregion
    }
}
