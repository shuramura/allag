#region Using directives

using System;
using System.Data;
using Allag.Core.Data.Attributes;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data
{
    [TestFixture]
    public class TypeDescriptionTests
    {
        #region Variables
        private TypeDescription _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = TypeDescription.Get<Test<int>>();
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(8, _instance.Fields.Count, "The property 'Fields' is wrong.");
            Assert.AreEqual("Id", _instance.Fields[0].ParameterName, "The property 'Fields[0]' is wrong.");
            Assert.AreEqual("Field", _instance.Fields[1].ParameterName, "The property 'Fields[1]' is wrong.");
            Assert.AreEqual("One", _instance.Fields[2].ParameterName, "The property 'Fields[2]' is wrong.");
            Assert.AreEqual("Two", _instance.Fields[3].ParameterName, "The property 'Fields[3]' is wrong.");

            Assert.AreEqual(1, _instance.Children.Count, "The property 'Children' is wrong.");
        }

        [Test]
        public void CtorForComplexIdTest()
        {
            // Pre-Conditions
            TypeDescription instance = TypeDescription.Get<Test<TestId>>();

            // Post-Conditions
            Assert.AreEqual(7, instance.Fields.Count, "The property 'Fields' is wrong.");
            Assert.AreEqual("Field", instance.Fields[0].ParameterName, "The property 'Fields[0]' is wrong.");
            Assert.AreEqual("One", instance.Fields[1].ParameterName, "The property 'Fields[1]' is wrong.");
            Assert.AreEqual("Two", instance.Fields[2].ParameterName, "The property 'Fields[2]' is wrong.");
            Assert.AreEqual("Three", instance.Fields[3].ParameterName, "The property 'Fields[3]' is wrong.");
            Assert.AreEqual("Four", instance.Fields[4].ParameterName, "The property 'Fields[4]' is wrong.");
            Assert.AreEqual("TestComplexAsSimpleField", instance.Fields[5].ParameterName, "The property 'Fields[5]' is wrong.");

            Assert.AreEqual(2, instance.Children.Count, "The property 'Children' is wrong.");
        }

        [Test]
        public void GetNullTypeTest()
        {
            // Action
            Assert.That(() => TypeDescription.Get(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CtorArrayTest()
        {
            // Action
            TypeDescription instance = TypeDescription.Get<TestArray>();

            // Post-Conditions
            Assert.AreEqual(3, instance.Fields.Count, "The property 'Fields' is wrong.");
            Assert.AreEqual("Int", instance.Fields[0].ParameterName, "The property 'Fields[0]' is wrong.");
            Assert.AreEqual("IntArray", instance.Fields[1].ParameterName, "The property 'Fields[0]' is wrong.");
            Assert.AreEqual("Array", instance.Fields[2].ParameterName, "The property 'Fields[0]' is wrong.");
        }
        #endregion

        #region Methods
        #endregion

        #region Enums
        public enum TestActionType
        {
            Select,
            SelectAll,

            Insert,
            Update,

            Delete,
            DeleteAll,

            Execute
        }
        #endregion

        #region Classes
        public class Test0<TId>
        {
            #region Variables
            [Field]
            public DateTime DateTimeField;

            #endregion

            #region Properties

            [Field(IsNullable = false, Order = 2)]
            public virtual TId Id { get; set; }

            [Field]
            public virtual short Short { get; set; }

            #endregion
        }

        public class TestDoubleIdField : Test<int>
        {
            #region Variables

            [Field]
            public int SecondId;
            #endregion
        }

        public class Test<TId> : Test0<TId>
        {
            #region Variables
            [Field(Order = 2)]
            public int One;
            [Field(Order = 1, Direction = ParameterDirection.InputOutput)]
            public int Two;
            [Field(Order = 3, Direction = ParameterDirection.Output)]
            public int Three;
            [Field(Direction = ParameterDirection.Output)]
// ReSharper disable InconsistentNaming
#pragma warning disable 169
            private int Four;
#pragma warning restore 169
// ReSharper restore InconsistentNaming
            [Field]
            public TestComplex TestComplexField;

            [Field(IsSimpleField = true)]
            public TestComplex TestComplexAsSimpleField;

            private string _strField;
            #endregion

            #region Override properties
            [Field(Order = 0)]
            public override TId Id
            {
                get
                {
                    return base.Id;
                }
                set
                {
                    base.Id = value;
                }
            }

#pragma warning disable 0169
// ReSharper disable RedundantOverridenMember
            public override short Short
            {
                get
                {
                    return base.Short;
                }
                set
                {
                    base.Short = value;
                }
            }
// ReSharper restore RedundantOverridenMember
#pragma warning restore 0169

            #endregion

            #region Properties
            [Field]
// ReSharper disable UnusedMember.Local
// ReSharper disable ConvertToAutoProperty
            private string Field
// ReSharper restore ConvertToAutoProperty
// ReSharper restore UnusedMember.Local
            {
                get { return _strField; }
                set { _strField = value; }
            }

            #endregion
        }

        public class TestComplex
        {
            #region Variables
            [Field]
            public string ComplexOne;
            [Field]
            public TimeSpan ComplexTwo;
            #endregion
        }

        public class TestId
        {
            #region Variables
            [Field(Order = -1)]
            public int OneId;
            [Field(Order = 0)]
            public int TwoId;
            #endregion
        }

        public class TestInt : Test<int> { }

        public class TestArray
        {
            #region Variables
            [Field(Order = 0)]
            public int Int;
            [Field(Order = 1)]
            public int[] IntArray;
            [Field(Order = 2)]
            public Test<int>[] Array;
            #endregion
        }

        #endregion
    }
}
