#region Using directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Allag.Core.TestData.Data.Text;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Text
{
    [TestFixture]
    public class ParserTests
    {
        #region Constants
        private const string CSV_ZERO_SIZE_FILE = @".\TestData\Data\Text\zero_size.csv";
        private const string CSV_WITHOUT_HEADER_FILE = @".\TestData\Data\Text\test_without_header.csv";
        private const string CSV_COMPLEX_FILE = @".\TestData\Data\Text\test_complex.csv";
        private const string CSV_COMPLEX_WRONG_DATA_FILE = @".\TestData\Data\Text\test_complex_wrong_data.csv";
        private const string CSV_COMPLEX_WRONG_ITEM_CONDITION_FILE = @".\TestData\Data\Text\test_complex_wrong_item_condition.csv";
        private const string CSV_COMPLEX_UNSUPPORTED_TYPE_FILE = @".\TestData\Data\Text\test_complex_unsupported_type.csv";
        private const string CSV_COMPLEX_PARSING_ENDING_ERROR_FILE = @".\TestData\Data\Text\test_complex_parsing_ending_error.csv";
        private const string CSV_COMPLEX_WRONG_COLUMN_COUNT_FILE = @".\TestData\Data\Text\test_complex_wrong_column_count.csv";
        
        #endregion

        #region Variables
        private Stream _csvFileStream;
        private Parser<Test> _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _csvFileStream = DelimitedParseEngineTests.OpenStream();
            _instance = new Parser<Test>(_csvFileStream, true);
        }

        [TearDown]
        public void TearDown()
        {
            if (_csvFileStream != null)
            {
                _csvFileStream.Dispose();
            }
        }

        #endregion

        #region Tests
        [Test]
        public void LoadTest()
        {
            // Action
            IEnumerable<Test> result = _instance.Load();

            // Post-Conditions
            Assert.AreEqual(5, result.Count());
        }

        [Test]
        public void LoadZeroSizeTest()
        {
            using (Stream stream = DelimitedParseEngineTests.OpenStream(CSV_ZERO_SIZE_FILE))
            {
                // Pre-Conditions
                Parser<Test> instance = new Parser<Test>(stream, false);

                // Action
                IEnumerable<Test> result = instance.Load();

                // Post-Conditions
                Assert.AreEqual(0, result.Count());
            }
        }

        [Test]
        public void LoadWithOrderTest()
        {
            using (Stream stream = DelimitedParseEngineTests.OpenStream(CSV_WITHOUT_HEADER_FILE))
            {
                // Pre-Conditions
                Parser<Test> instance = new Parser<Test>(stream, false);

                // Action
                IEnumerable<Test> result = instance.Load();

                // Post-Conditions
                Assert.AreEqual(4, result.Count());
            }
        }

        [Test]
        public void LoadWrongColumnCountTest()
        {
            // Pre-Conditions
            _csvFileStream.Seek(0, SeekOrigin.Begin);
            Parser<TestWrongColumnCount> instance = new Parser<TestWrongColumnCount>(_csvFileStream, true);

            // Action
            Assert.That(() => instance.Load().ToArray(), Throws.InstanceOf<ParseFormatException>());
        }

        [Test]
        public void LoadWrongColumnNameTest()
        {
            // Pre-Conditions
            Parser<TestWrongColumnName> instance = new Parser<TestWrongColumnName>(_csvFileStream, true);

            // Action
            Assert.That(() => instance.Load().ToArray(), Throws.InstanceOf<ParseFormatException>());
        }

        [Test]
        public void LoadWrongDataTest()
        {
            // Pre-Conditions
            _csvFileStream.Seek(0, SeekOrigin.Begin);
            Parser<TestWrongData> instance = new Parser<TestWrongData>(_csvFileStream, true);

            // Action
            Assert.That(() => instance.Load().ToArray(), Throws.InstanceOf<ParseFormatException>());
        }

        [Test]
        public void SaveWithNullItemsTest()
        {
            // Action
            Assert.That(() => _instance.Save((IList<Test>)null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void SaveWithNullItemTest()
        {
            // Action
            Assert.That(() => _instance.Save((Test)null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void SaveWithHeaderTest()
        {
            using (Stream stream = new MemoryStream())
            {
                // Pre-Conditions
                Test expected = new Test {Column0 = 0, Column1 = 1, Column2 = 2, Column3 = null, Column4 = "4"};

                Parser<Test> instance = new Parser<Test>(stream, true);

                // Action
                instance.Save(expected);

                // Post-Conditions
                stream.Seek(0, SeekOrigin.Begin);
                using (StreamReader streamReader = new StreamReader(stream))
                {
                    string result = streamReader.ReadToEnd();
                    Assert.AreEqual("Column0,OutputColumn,\"Column2,\",\"C\"\"olum,n4\",\r\n         0,,2,,4   \r\n", result);
                }
            }
        }

        [Test]
        public void LoadComplexTest()
        {
            using (Stream stream = DelimitedParseEngineTests.OpenStream(CSV_COMPLEX_FILE))
            {
                // Pre-Conditions
                Parser<TestComplex> instance = new Parser<TestComplex>(stream, false);

                // Action
                IEnumerable<TestComplex> result = instance.Load();

                // Post-Conditions
                Assert.AreEqual(1, result.Count(), "The item count is wrong.");
                Assert.AreEqual("test", result.First().Test, "The property 'Test' is wrong.");
                Assert.AreEqual(1, result.First().Tests0.Count, "The property 'Tests0.Count' is wrong.");
                Assert.AreEqual(2, result.First().Tests1.Count, "The property 'Tests1.Count' is wrong.");
                Assert.AreEqual(3, result.First().Tests2.Count, "The property 'Tests2.Count' is wrong.");
            }
        }

        [Test]
        public void LoadComplexWrongDataTest()
        {
            using (Stream stream = DelimitedParseEngineTests.OpenStream(CSV_COMPLEX_WRONG_DATA_FILE))
            {
                // Pre-Conditions
                Parser<TestComplex> instance = new Parser<TestComplex>(stream, false);

                // Action
                Assert.That(() => instance.Load(), Throws.InstanceOf<ParseFormatException>());
            }
        }

        [Test]
        public void LoadComplexWrongItemConditionTest()
        {
            using (Stream stream = DelimitedParseEngineTests.OpenStream(CSV_COMPLEX_WRONG_ITEM_CONDITION_FILE))
            {
                // Pre-Conditions
                Parser<TestComplex> instance = new Parser<TestComplex>(stream, false);

                // Action
                Assert.That(() => instance.Load(), Throws.InstanceOf<ParseFormatException>());
            }
        }

        [Test]
        public void LoadComplexUnsupportedTypeTest()
        {
            using (Stream stream = DelimitedParseEngineTests.OpenStream(CSV_COMPLEX_UNSUPPORTED_TYPE_FILE))
            {
                // Pre-Conditions
                Parser<TestComplex> instance = new Parser<TestComplex>(stream, false);

                // Action
                Assert.That(() => instance.Load(), Throws.InstanceOf<ParseFormatException>());
            }
        }

        [Test]
        public void LoadComplexParsingEndingErrorTest()
        {
            using (Stream stream = DelimitedParseEngineTests.OpenStream(CSV_COMPLEX_PARSING_ENDING_ERROR_FILE))
            {
                // Pre-Conditions
                Parser<TestComplex> instance = new Parser<TestComplex>(stream, false);

                // Action
                Assert.That(() => instance.Load(), Throws.InstanceOf<ParseFormatException>());
            }
        }

        [Test]
        public void LoadComplexWrongColumnCountTest()
        {
            using (Stream stream = DelimitedParseEngineTests.OpenStream(CSV_COMPLEX_WRONG_COLUMN_COUNT_FILE))
            {
                // Pre-Conditions
                Parser<TestComplex> instance = new Parser<TestComplex>(stream, false);

                // Action
                Assert.That(() => instance.Load(), Throws.InstanceOf<ParseFormatException>());
            }
        }

        [Test]
        public void SaveComplexTest()
        {
            using (Stream stream = new MemoryStream())
            {
                // Pre-Conditions
                TestComplex expected = new TestComplex {Test = "new test"};
                expected.Tests0.Add(new Test {Column0 = 1, OutputColumn = "1", Column4 = "la"});
                expected.Tests1.Add(new Test {Column0 = 2, OutputColumn = "2", Column4 = "lala"});
                expected.Tests1.Add(new Test {Column0 = 2, OutputColumn = "3", Column4 = "lalala"});
                expected.Tests2.Add(new Test {Column0 = 3, OutputColumn = "4", Column4 = "lalalala"});
                expected.Tests2.Add(new Test {Column0 = 3, OutputColumn = "5", Column4 = "lalalalala"});
                expected.Tests2.Add(new Test {Column0 = 3, OutputColumn = "6", Column4 = "lalalalalala"});
                expected.Tests2.Add(new Test {Column0 = 3, OutputColumn = "7", Column4 = "lalalalalalala"});

                Parser<TestComplex> instance = new Parser<TestComplex>(stream, false);

                // Action
                instance.Save(expected);

                // Post-Conditions
                stream.Seek(0, SeekOrigin.Begin);
                using (StreamReader streamReader = new StreamReader(stream))
                {
                    string result = streamReader.ReadToEnd();
                    Assert.AreEqual("0,new test\r\n         1,1,0,,la  \r\n         2,2,0,,lala\r\n         2,3,0,,lalala\r\n         3,4,0,,lalalala\r\n         3,5,0,,lalalalala\r\n         3,6,0,,lalalalalala\r\n         3,7,0,,lalalalalalala\r\n", result);
                }
            }
        }

        [Test]
        public void SaveWithGapTest()
        {
            using (Stream stream = new MemoryStream())
            {
                // Pre-Conditions
                TestWithGap expected = new TestWithGap {Column0 = 0, Column1 = 1, Column2 = 2};

                Parser<TestWithGap> instance = new Parser<TestWithGap>(stream, false);

                // Action
                instance.Save(expected);

                // Post-Conditions
                TestWithGap[] result =  instance.Load().ToArray();

                Assert.AreEqual(1, result.Length, "The count is wrong.");
                Assert.AreEqual(expected, result[0], "The item is wrong.");
            }
        }
        #endregion

        #region Methods

        #endregion
    }
}
