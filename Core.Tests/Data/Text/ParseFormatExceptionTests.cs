#region Using directives

using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Text
{
    [TestFixture]
    public class ParseFormatExceptionTests
    {
        #region Variables
        private ParseFormatException _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new ParseFormatException(ParseState.UnknownError, string.Empty, -1);
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(ParseState.UnknownError, _instance.State, "The property 'State' is wrong.");
            Assert.AreEqual(-1, _instance.LineIndex, "The property 'LineIndex' is wrong.");
            Assert.AreEqual(-1, _instance.ItemIndex, "The property 'ItemIndex' is wrong.");
            Assert.AreEqual(-1, _instance.Position, "The property 'Position' is wrong.");
            Assert.AreEqual(null, _instance.InnerException, "The property 'InnerException' is wrong.");
        }

        [Test]
        public void CtorWithInnerExceptionTest()
        {
            // Pre-Conditions
            Exception exception = new Exception();
            ParseFormatException instance = new ParseFormatException(string.Empty, -1, exception);

            // Post-Conditions
            Assert.AreEqual(ParseState.UnknownError, instance.State, "The property 'State' is wrong.");
            Assert.AreEqual(-1, instance.LineIndex, "The property 'LineIndex' is wrong.");
            Assert.AreEqual(-1, instance.ItemIndex, "The property 'ItemIndex' is wrong.");
            Assert.AreEqual(-1, instance.Position, "The property 'Position' is wrong.");
            Assert.AreEqual(exception, instance.InnerException, "The property 'InnerException' is wrong.");
        }

        [Test]
        public void CtorWithDetailesTest()
        {
            // Pre-Conditions
            ParseFormatException instance = new ParseFormatException(1, 2, 3);

            // Post-Conditions
            Assert.AreEqual(ParseState.FormatError, instance.State, "The property 'State' is wrong.");
            Assert.AreEqual(1, instance.LineIndex, "The property 'LineIndex' is wrong.");
            Assert.AreEqual(2, instance.ItemIndex, "The property 'ItemIndex' is wrong.");
            Assert.AreEqual(3, instance.Position, "The property 'Position' is wrong.");
            Assert.AreEqual(null, instance.InnerException, "The property 'InnerException' is wrong.");
        }

        [Test]
        public void CtorWithLineAndItemIndexAndInnerExceptionTest()
        {
            // Pre-Conditions
            Exception exception = new Exception();
            ParseFormatException instance = new ParseFormatException(ParseState.DataError, 1, "2", exception);

            // Post-Conditions
            Assert.AreEqual(ParseState.DataError, instance.State, "The property 'State' is wrong.");
            Assert.AreEqual(1, instance.LineIndex, "The property 'LineIndex' is wrong.");
            Assert.AreEqual(-1, instance.ItemIndex, "The property 'ItemIndex' is wrong.");
            Assert.AreEqual(-1, instance.Position, "The property 'Position' is wrong.");
            Assert.AreEqual(exception, instance.InnerException, "The property 'InnerException' is wrong.");
        }
        #endregion

        #region Methods

        #endregion
    }
}
