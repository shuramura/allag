﻿#region Using directives
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Text
{
    [TestFixture]
    public class RowLineEventArgsTests
    {
        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Pre-Conditions
            string[] columns = new string[]{"c0", "c1"};
            string[] values = new string[] { "v0", "v1" };

            // Action
            RowLineEventArgs instance = new RowLineEventArgs(columns, values);

            // Post-Conditions
            CollectionAssert.AreEqual(columns, instance.Columns, "The property 'Columns' is wrong.");
            CollectionAssert.AreEqual(values, instance.Values, "The property 'Values' is wrong.");
        }
        #endregion
    }
}