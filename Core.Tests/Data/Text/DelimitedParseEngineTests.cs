#region Using directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Text
{
    [TestFixture]
    public class DelimitedParseEngineTests
    {
        #region Constants
        private const string CSV_FILE = @".\TestData\Data\Text\test.csv";
        private const string CSV_WITH_TAB_DELIMITER_FILE = @".\TestData\Data\Text\test_tab_delimiter.csv";
        private const string CSV_FLOAT_COLUMN_COUNT_FILE = @".\TestData\Data\Text\test_float_column_count.csv";

        public readonly string[][] CVS_FILE_ROWS = new[]
        {
            new[] {"Column0", "\"Column1", "Column2,", "C\"olum,n4", ""},
            new[] {"0", "1", "2", "3", "4"},
            new[] {"10", "11", "12", "", "14"},
            new[] {"20", "21", "22", "", ""},
            new[] {"", "31", "32", "", "34"},
            new[] {"40", "41", "42", "", ""}
        };

        private const string CSV_WRONG_FILE_TEMPLATE = @".\TestData\Data\Text\test_wrong_format_{0}.csv";
        #endregion

        #region Variables
        private Stream _csvFileStream;
        private IParseEngine _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _csvFileStream = OpenStream();

            _instance = CreateWithHeaderAndFixedColumnCount();
        }

        [TearDown]
        public void TearDown()
        {
            if (_csvFileStream != null)
            {
                _csvFileStream.Close();
            }
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(true, _instance.IsHeaderExists, "The property 'IsHeaderExists' is wrong.");
            Assert.AreEqual(true, _instance.IsColumnCountFixed, "The property 'IsColumnCountFixed' is wrong.");
            Assert.AreEqual(CVS_FILE_ROWS[0].Length, _instance.Columns.Count, "The property 'Columns.Count' is wrong.");
            Assert.IsFalse(_instance.IsEmpty, "The property 'IsEmpty' is wrong.");
        }

        [Test]
        public void IsEmptyTest()
        {
            // Pre-Conditions 
            _csvFileStream = new MemoryStream();

            // Action
            IParseEngine instance = new DelimitedParseEngine(_csvFileStream, true);

            // Post-Conditions
            Assert.IsTrue(instance.IsEmpty);
        }

        [Test]
        public void CtorWithNullStreamTest()
        {
            Assert.That(() => new DelimitedParseEngine(null, false, false), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CtorWithDelimiterEqualToServiceSymbolTest()
        {
            Assert.That(() => new DelimitedParseEngine(_csvFileStream, false, false, "\""), Throws.InstanceOf<ArgumentOutOfRangeException>());
        }

        [Test]
        public void CtorWithWrongDelimiterTest()
        {
            Assert.That(() => new DelimitedParseEngine(_csvFileStream, false, false, new string(new[] {char.MinValue})), Throws.InstanceOf<ArgumentOutOfRangeException>());
        }

        [Test]
        public void CtorWithEmptyDelimiterTest([Values(null, "")] string delimiters)
        {
            Assert.That(() => new DelimitedParseEngine(_csvFileStream, false, false, delimiters), Throws.InstanceOf<ArgumentNullOrEmptyException>());
        }

        [Test]
        public void DisposedTest()
        {
            // Action
            _instance.Dispose();

            // Post-Conditions
            Assert.That(() => _instance.Reset(), Throws.InstanceOf<ObjectDisposedException>());
        }

        [Test]
        public void CurrentGetWithoutMoveNextTest()
        {
            // Action
            Assert.That(() => _instance.Current, Throws.InvalidOperationException);
        }

        [Test]
        public void CurrentGetExplicitWithoutMoveNextTest()
        {
            // Action
            Assert.That(() => ((IEnumerator)_instance).Current, Throws.InvalidOperationException);
        }

        [Test]
        public void CurrentGetTest()
        {
            // Pre-Conditions
            _instance.MoveNext();

            // Actions
            IDictionary<string, string> result = _instance.Current;
            IDictionary<string, string> resultExplicit = ((IEnumerator)_instance).Current as IDictionary<string, string>;

            // Post-Conditions
            Assert.IsNotNull(result, "The result is wrong");
            Assert.AreEqual(result, resultExplicit, "The explicit result is wrong.");

            for (int i = 0; i < 5; i++)
            {
                Assert.AreEqual(i.ToString(), result[CVS_FILE_ROWS[0][i].ToLower()], "The property 'result[{0}]' is wrong.", CVS_FILE_ROWS[0][i]);
            }
        }

        [Test]
        public void CurrentOutOfEndTest()
        {
            // Actions
            IParseEngine instance = CreateWithHeaderAndFixedColumnCount();
            while (instance.MoveNext()) ;

            // Post-Conditions
            Assert.That(() => instance.Current, Throws.InvalidOperationException);
        }

        [Test]
        public void ResetTest()
        {
            // Pre-Conditions
            int expected = 0;
            while (_instance.MoveNext()) expected++;

            // Actions
            _instance.Reset();
            int result = 0;
            while (_instance.MoveNext()) result++;

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void WrongFileFormat0Test()
        {
            // Action
            Assert.That(() => TestWrongFileFormat(string.Format(CSV_WRONG_FILE_TEMPLATE, 0)), Throws.InstanceOf<ParseFormatException>());
        }

        [Test]
        public void WrongFileFormat1Test()
        {
            // Action
            Assert.That(() => TestWrongFileFormat(string.Format(CSV_WRONG_FILE_TEMPLATE, 1)), Throws.InstanceOf<ParseFormatException>());
        }

        [Test]
        public void WrongFileFormat2Test()
        {
            // Action
            Assert.That(() => TestWrongFileFormat(string.Format(CSV_WRONG_FILE_TEMPLATE, 2)), Throws.InstanceOf<ParseFormatException>());
        }

        [Test]
        public void WrongFileFormat3Test()
        {
            // Action
            Assert.That(() => TestWrongFileFormat(string.Format(CSV_WRONG_FILE_TEMPLATE, 3)), Throws.InstanceOf<ParseFormatException>());
        }

        [Test]
        public void WrongFileFormat4Test()
        {
            // Action
            Assert.That(() => TestWrongFileFormat(string.Format(CSV_WRONG_FILE_TEMPLATE, 4)), Throws.InstanceOf<ParseFormatException>());
        }

        [Test]
        public void ColumnsWithHeaderTest()
        {
            // Post-Conditions
            Assert.AreEqual(CVS_FILE_ROWS[0].Length, _instance.Columns.Count, "The count of the columns is wrong.");

            for (int i = 0; i < _instance.Columns.Count; i++)
            {
                CheckColumnName(i, CVS_FILE_ROWS[0][i], _instance.Columns[i]);
            }
        }

        [Test]
        public void ColumnsWithoutHeaderTest()
        {
            // Pre-Conditions
            IParseEngine instance = CreateWithoutHeaderAndWithFixedColumnCount();

            // Post-Conditions
            Assert.AreEqual(CVS_FILE_ROWS[0].Length, instance.Columns.Count, "The count of the columns is wrong.");

            for (int i = 0; i < instance.Columns.Count; i++)
            {
                CheckColumnName(i, i.ToString(), instance.Columns[i]);
            }
        }

        [Test]
        public void RowWithColumnsTest()
        {
            // Post-Conditions
            CheckRows(_instance, 1, CVS_FILE_ROWS.Length - 1);
        }

        [Test]
        public void RowWithColumnsAndTabDelimiterTest()
        {
            using(Stream stream = OpenStream(CSV_WITH_TAB_DELIMITER_FILE))
            {
                // Post-Conditions
                CheckRows(CreateWithHeaderAndFixedColumnCount(stream, '\t'), 1, CVS_FILE_ROWS.Length - 1);
            }
        }

        [Test]
        public void RowWithoutHeaderTest()
        {
            // Pre-Conditions
            IParseEngine instance = CreateWithoutHeaderAndWithFixedColumnCount();

            // Post-Conditions
            CheckRows(instance, 0, CVS_FILE_ROWS.Length);
        }

        [Test]
        public void FloatColumnCountTest()
        {
            using(Stream stream = OpenStream(CSV_FLOAT_COLUMN_COUNT_FILE))
            {
                // Pre-Conditions
                IParseEngine instance = new DelimitedParseEngine(stream, false, false);

                // Post-Conditions
                Assert.AreEqual(null, instance.Columns, "The property 'Columns' is wrong.");
                int i = 0;
                while (instance.MoveNext())
                {
                    int j = 0;
                    foreach (string value in instance.Current.Values)
                    {
                        Assert.AreEqual(i.ToString() + j, value, "The value '[{0}][{1}]' is wrong.", i, j);
                        j++;
                    }

                    i++;
                }
                Assert.AreEqual(5, i, "The row count is wrong.");
            }
        }
        #endregion

        #region Methods
        private IParseEngine CreateWithHeaderAndFixedColumnCount()
        {
            return CreateWithHeaderAndFixedColumnCount(_csvFileStream, ',');
        }

        private static IParseEngine CreateWithHeaderAndFixedColumnCount(Stream stream, char delimiter)
        {
            return new DelimitedParseEngine(stream, true, true, new string(new[] {delimiter}));
        }

        private IParseEngine CreateWithoutHeaderAndWithFixedColumnCount()
        {
            _csvFileStream.Seek(0, SeekOrigin.Begin);
            return new DelimitedParseEngine(_csvFileStream);
        }

        private static void CheckColumnName(int columnIndex, string expected, string actual)
        {
            Assert.AreEqual(expected.ToLower(), actual.ToLower(), "The name of column '{0}' is wrong and equals to '{1}'.", columnIndex, actual);
        }

        public static Stream OpenStream()
        {
            return OpenStream(CSV_FILE);
        }

        public static Stream OpenStream(string filePath)
        {
            return
                new FileStream(Path.Combine(Environment.CurrentDirectory, filePath), FileMode.Open, FileAccess.Read);
        }

        private void CheckRows(IParseEngine instance, int startRowIndex, int rowCount)
        {
            IList<string> columns = instance.Columns;

            int i = 0;
            while (instance.MoveNext())
            {
                IDictionary<string, string> rows = instance.Current;

                Assert.AreEqual(columns.Count, rows.Count, "The count of items is wrong.");
                for (int k = 0; k < columns.Count; k++)
                {
                    Assert.AreEqual(CVS_FILE_ROWS[startRowIndex + i][k].ToLower(), rows[columns[k]].ToLower(), "The item in the position ({0}, {1}) is wrong.", i, k);
                }

                i++;
            }

            Assert.AreEqual(rowCount, i, "The count of row is wrong.");
        }

        private static void TestWrongFileFormat(string file)
        {
            using(Stream stream = OpenStream(file))
            {
                // Post-Conditions
                CreateWithHeaderAndFixedColumnCount(stream, ',');
            }
        }
        #endregion
    }
}