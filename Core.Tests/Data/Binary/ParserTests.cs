﻿#region Using directives
using System;
using System.Data;
using System.IO;
using Allag.Core.Data.Attributes;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Binary
{
    [TestFixture]
    public class ParserTests : IObjectContainer<ParserTests.TestClass>
    {
        #region Variables
        private bool _isVisitedCreate;
        private bool _isVisitedSaving;
        private bool _isVisitedSaved;
        private bool _isVisitedLoading;
        private bool _isVisitedLoaded;
        private bool _isVisitedGetArrayLength;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _isVisitedCreate = false;
            _isVisitedSaving = false;
            _isVisitedSaved = false;
            _isVisitedLoading = false;
            _isVisitedLoaded = false;
            _isVisitedGetArrayLength = false;
        }
        #endregion

        #region Tests
        [Test]
        public void LoadWithNullStreamTest()
        {
            // Action
            Assert.That(() => Parser<TestClass>.Load(null, 0, this), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void LoadWithWrongOffsetTest()
        {
            // Action
            using (MemoryStream stream = new MemoryStream())
            {
                Assert.That(() => Parser<TestClass>.Load(stream, -10, this), Throws.InstanceOf<ArgumentOutOfRangeException>());
            }
        }

        [Test]
        public void SaveWithNullItemTest()
        {
            // Action
            using (MemoryStream stream = new MemoryStream())
            {
                Assert.That(() => Parser<TestClass>.Save(null, stream, 0, this), Throws.InstanceOf<ArgumentNullException>());
            }
        }

        [Test]
        public void SaveWithNullStreamTest()
        {
            // Action
            Assert.That(() => Parser<TestClass>.Save(new TestClass(), null, 0, this), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void SaveWithWrongOffsetTest()
        {
            // Action
            using (MemoryStream stream = new MemoryStream())
            {
                Assert.That(() => Parser<TestClass>.Save(new TestClass(), stream, -10, this), Throws.InstanceOf<ArgumentOutOfRangeException>());
            }
        }

        [Test]
        public void SaveLoadTest()
        {
            // Pre-Conditions
            TestClass expected = new TestClass();
            expected.StrProp = Guid.NewGuid().ToString();
            expected.Items = new ArrayItem[] {new ArrayItem {Int = 0}, new ArrayItem {Int = 2}, new ArrayItem {Int = 4}};
            expected.Integers = new int[] {10, 20, 30};
            expected.FakeItem = new ArrayItem {Int = 100};
            expected.FakeInt = 1000;

            // Action
            using (MemoryStream stream = new MemoryStream())
            {
                Parser<TestClass>.Save(expected, stream, 0, this);

                TestClass result = Parser<TestClass>.Load(stream, 0, this);

                Assert.IsTrue(_isVisitedCreate, "The method 'Create' is not visited.");
                Assert.IsTrue(_isVisitedSaving, "The method 'Saving' is not visited.");
                Assert.IsTrue(_isVisitedSaved, "The method 'Saved' is not visited.");
                Assert.IsTrue(_isVisitedLoading, "The method 'Loading' is not visited.");
                Assert.IsTrue(_isVisitedLoaded, "The method 'Loaded' is not visited.");
                Assert.IsTrue(_isVisitedGetArrayLength, "The method 'GetArrayLength' is not visited.");


                Assert.AreEqual(expected.StrProp, result.StrProp, "The property 'StrProp' is wrong.");
                CollectionAssert.AreEqual(expected.Integers, result.Integers, "The property 'Integers' is wrong.");
                Assert.AreEqual(expected.FakeItem.Int, result.FakeItem.Int, "The property 'FakeItem.Int' is wrong.");
                Assert.AreEqual(expected.FakeInt, result.FakeInt, "The property 'FakeInt' is wrong.");
            }
        }
        #endregion

        #region Implementation of IObjectContainer<TestClass>
        public TestClass Create(Stream stream)
        {
            _isVisitedCreate = true;
            Assert.IsNotNull(stream, "The method 'Create' has wrong 'stream'.");
            return new TestClass();
        }

        public int GetArrayLength(IDbDataParameter dataParameter, TestClass owner, Stream stream, long offset)
        {
            _isVisitedGetArrayLength = true;
            Assert.IsNotNull(dataParameter, "The method 'GetArrayLength' has wrong 'dataParameter'.");
            Assert.IsNotNull(owner, "The method 'GetArrayLength' has wrong 'owner'.");
            Assert.IsNotNull(stream, "The method 'GetArrayLength' has wrong 'stream'.");
            Assert.AreNotEqual(stream.Position, offset, "The method 'GetArrayLength' has wrong 'offset'.");

            int ret = -1;
            switch (dataParameter.ParameterName)
            {
                case "Items":
                    ret = owner.ArrLength;
                    break;
                case "Integers":
                    ret = 3;
                    break;
            }

            return ret;
        }

        public void Loading(TestClass item, Stream stream)
        {
            _isVisitedLoading = true;

            Assert.IsNotNull(item, "The method 'Loading' has wrong 'item'.");
            Assert.IsNotNull(stream, "The method 'Loading' has wrong 'stream'.");
        }

        public void Loaded(TestClass item, Stream stream, long offset)
        {
            _isVisitedLoaded = true;

            Assert.IsNotNull(item, "The method 'Loaded' has wrong 'item'.");
            Assert.IsNotNull(stream, "The method 'Loaded' has wrong 'stream'.");
            Assert.AreNotEqual(stream.Position, offset, "The method 'Loaded' has wrong 'offset'.");


            item.FakeItem = Parser<ArrayItem>.Load(stream, stream.Position);
            item.FakeInt = BitConverter.ToInt32(Parser.Read(stream, stream.Position, sizeof(int)), 0);
        }

        public void Saving(TestClass item, Stream stream)
        {
            _isVisitedSaving = true;

            Assert.IsNotNull(item, "The method 'Saving' has wrong 'item'.");
            Assert.IsNotNull(stream, "The method 'Saving' has wrong 'stream'.");
        }

        public void Saved(TestClass item, Stream stream, long offset)
        {
            _isVisitedSaved = true;

            Assert.IsNotNull(item, "The method 'Saved' has wrong 'item'.");
            Assert.IsNotNull(stream, "The method 'Saved' has wrong 'stream'.");
            Assert.AreNotEqual(stream.Position, offset, "The method 'Saved' has wrong 'offset'.");

            Parser<ArrayItem>.Save(item.FakeItem, stream, stream.Position);
            Parser.Save(stream, stream.Position, BitConverter.GetBytes(item.FakeInt));
        }
        #endregion

        #region Classes
        public class TestClass : BaseObject
        {
            #region Properties
            [Field(Order = 0, Size = 36)]
            [Convert(typeof(byte[]))]
            public string StrProp { get; set; }

            [Field(Order = 1)]
            [Convert(typeof(byte[]))]
            public short ArrLength
            {
                get
                {
                    short ret = 0;
                    if (Items != null)
                    {
                        ret = (short)Items.Length;
                    }
                    return ret;
                }
                set { Items = new ArrayItem[value]; }
            }

            [Field(Order = 2)]
            [Convert(typeof(byte[]))]
            public ArrayItem[] Items { get; set; }

            [Field(Order = 3)]
            [Convert(typeof(byte[]))]
            public int[] Integers { get; set; }

            public ArrayItem FakeItem { get; set; }
            public int FakeInt { get; set; }
            #endregion

            #region Methods
            protected ArrayItem ArrayItemFrom(byte[] value, string propertyName)
            {
                using (MemoryStream stream = new MemoryStream(value))
                {
                    return Parser<ArrayItem>.Load(stream);
                }
            }

            protected byte[] ArrayItemTo(ArrayItem value, string propertyName)
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    Parser<ArrayItem>.Save(value, stream);
                    return stream.ToArray();
                }
            }
            #endregion
        }

        public class ArrayItem : BaseObject
        {
            #region Properties
            [Field(Order = 0)]
            [Convert(typeof(byte[]))]
            public int Int { get; set; }
            #endregion
        }
        #endregion
    }
}