﻿#region Using directives
using System;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Data.Binary
{
    [TestFixture]
    public class BaseObjectTests
    {
        #region Variables
        private MockRepository _mocks;
        private BaseObject _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _mocks = new MockRepository();
            _instance = _mocks.PartialMock<BaseObject>(Encoding.UTF32);
            _mocks.ReplayAll();
        }
        #endregion

        #region Tests
        [Test]
        public void CtorNullEncodingTest()
        {
            // Action
            Assert.That(() => _mocks.PartialMock<BaseObject>(new object[]{null}), Throws.Exception.With.Message.Contains("System.ArgumentNullException"));
        }

        [Test]
        public void DoubleFromTest()
        {
            // Pre-Conditions
            const double expected = 234.3d;
            byte[] buffer = BitConverter.GetBytes(expected);

            // Action
            double result = _instance.DoubleFrom(buffer, string.Empty);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void DoubleToTest()
        {
            // Pre-Conditions
            const double value = 234.3d;
            byte[] expected = BitConverter.GetBytes(value);

            // Action
            byte[] result = _instance.DoubleTo(value, string.Empty);

            // Post-Conditions
            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        public void SingleFromTest()
        {
            // Pre-Conditions
            const float expected = 234.3f;
            byte[] buffer = BitConverter.GetBytes(expected);

            // Action
            float result = _instance.SingleFrom(buffer, string.Empty);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void SingleToTest()
        {
            // Pre-Conditions
            const float value = 234.3f;
            byte[] expected = BitConverter.GetBytes(value);

            // Action
            byte[] result = _instance.SingleTo(value, string.Empty);

            // Post-Conditions
            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        public void Int64FromTest()
        {
            // Pre-Conditions
            const long expected = 234;
            byte[] buffer = BitConverter.GetBytes(expected);

            // Action
            long result = _instance.Int64From(buffer, string.Empty);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Int64ToTest()
        {
            // Pre-Conditions
            const long value = 234;
            byte[] expected = BitConverter.GetBytes(value);

            // Action
            byte[] result = _instance.Int64To(value, string.Empty);

            // Post-Conditions
            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        public void Int32FromTest()
        {
            // Pre-Conditions
            const int expected = 234;
            byte[] buffer = BitConverter.GetBytes(expected);

            // Action
            int result = _instance.Int32From(buffer, string.Empty);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Int32ToTest()
        {
            // Pre-Conditions
            const int value = 234;
            byte[] expected = BitConverter.GetBytes(value);

            // Action
            byte[] result = _instance.Int32To(value, string.Empty);

            // Post-Conditions
            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        public void Int16FromTest()
        {
            // Pre-Conditions
            const short expected = 234;
            byte[] buffer = BitConverter.GetBytes(expected);

            // Action
            short result = _instance.Int16From(buffer, string.Empty);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Int16ToTest()
        {
            // Pre-Conditions
            const short value = 234;
            byte[] expected = BitConverter.GetBytes(value);

            // Action
            byte[] result = _instance.Int16To(value, string.Empty);

            // Post-Conditions
            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        public void BooleanFromTest()
        {
            // Pre-Conditions
            const bool expected = true;
            byte[] buffer = BitConverter.GetBytes(expected);

            // Action
            bool result = _instance.BooleanFrom(buffer, string.Empty);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void BooleanToTest()
        {
            // Pre-Conditions
            const bool value = true;
            byte[] expected = BitConverter.GetBytes(value);

            // Action
            byte[] result = _instance.BooleanTo(value, string.Empty);

            // Post-Conditions
            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        public void StringFromTest()
        {
            // Pre-Conditions
            const string expected = "lalala";
            byte[] buffer = Encoding.UTF32.GetBytes(expected);

            // Action
            string result = _instance.StringFrom(buffer, string.Empty);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void StringToTest()
        {
            // Pre-Conditions
            const string value = "lalala";
            byte[] expected = Encoding.UTF32.GetBytes(value);

            // Action
            byte[] result = _instance.StringTo(value, string.Empty);

            // Post-Conditions
            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        public void StringFromWithDefaultEncodingTest()
        {
            // Pre-Conditions
            BaseObject instance = _mocks.PartialMock<BaseObject>();
            _mocks.Replay(instance);
            const string expected = "lalala";
            byte[] buffer = Encoding.ASCII.GetBytes(expected);

            // Action
            string result = instance.StringFrom(buffer, string.Empty);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void StringToWithDefaultEncodingTest()
        {
            // Pre-Conditions
            BaseObject instance = _mocks.PartialMock<BaseObject>();
            _mocks.Replay(instance);
            const string value = "lalala";
            byte[] expected = Encoding.ASCII.GetBytes(value);

            // Action
            byte[] result = instance.StringTo(value, string.Empty);

            // Post-Conditions
            CollectionAssert.AreEqual(expected, result);
        }
        #endregion
    }
}