#region Using directives
using System;
using System.Data;
using System.Linq;
using System.Reflection;
using Allag.Core.Data.Attributes;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data
{
    [TestFixture]
    public class FieldDescriptionTests
    {
        #region Variables
        public int TestField;

        private FieldAttribute _fieldAttribute;
        private FieldDescription _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            TestField = 100;

            _fieldAttribute = new FieldAttribute();
            _instance = CreateInstance(_fieldAttribute);
        }

        [TearDown]
        public void TearDown()
        {
        }
        #endregion

        #region Tests
        [Test]
        public void CtorForPropertyTest()
        {
            // Post-Conditions
            Assert.AreEqual(_fieldAttribute, _instance.FieldAttribute, "The property 'FieldAttribute' is wrong.");
            Assert.AreEqual(_fieldAttribute.Direction, _instance.Direction, "The property 'Direction' is wrong.");
            Assert.AreEqual("Test", _instance.ParameterName, "The property 'ParameterName' is wrong.");
            Assert.AreEqual(typeof(TestEnum), _instance.ParameterType, "The property 'ParameterType' is wrong.");
        }

        [Test]
        public void CtorForFieldTest()
        {
            // Pre-Conditions
            FieldDescription instance = new FieldDescription(GetType().GetField("TestField"), _fieldAttribute);

            // Post-Conditions
            Assert.AreEqual(_fieldAttribute, instance.FieldAttribute, "The property 'FieldAttribute' is wrong.");
            Assert.AreEqual(_fieldAttribute.Direction, instance.Direction, "The property 'Direction' is wrong.");
            Assert.AreEqual("TestField", instance.ParameterName, "The property 'ParameterName' is wrong.");
            Assert.AreEqual(typeof(int), instance.ParameterType, "The property 'ParameterType' is wrong.");
        }

        [Test]
        public void CtorWithNoDefaultParameterAttributeTest()
        {
            // Pre-Conditions
            FieldAttribute fieldAttribute = new FieldAttribute("Test0")
                                                {
                                                    Direction = ParameterDirection.InputOutput
                                                };

            // Action
            FieldDescription instance = CreateInstance(fieldAttribute);

            // Post-Conditions
            Assert.AreEqual(fieldAttribute, instance.FieldAttribute, "The property 'FieldAttribute' is wrong.");
            Assert.AreEqual(fieldAttribute.Direction, instance.Direction, "The property 'Direction' is wrong.");
            Assert.AreEqual(fieldAttribute.Name, instance.ParameterName, "The property 'ParameterName' is wrong.");
            Assert.AreEqual(typeof(TestEnum), _instance.ParameterType, "The property 'ParameterType' is wrong.");
        }

        [Test]
        public void CtorWithNullParameterAttributeTest()
        {
            // Action
            Assert.That(() => CreateInstance(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CtorWithNullParameterInfoTest()
        {
            // Action
            Assert.That(() => new FieldDescription(null, _fieldAttribute), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CtorWithWrongParameterInfoTest()
        {
            // Action
            Assert.That(() => new FieldDescription(GetType().GetMethod("CtorWithWrongParameterInfoTest"), _fieldAttribute), Throws.InstanceOf<ArgumentOutOfRangeException>());
        }

        [Test]
        public void DirectionSetTest()
        {
            // Pre-Conditions 
            const ParameterDirection expected = ParameterDirection.ReturnValue;

            // Action
            _instance.Direction = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Direction);
        }

        [Test]
        public void GetOriginalValueTest()
        {
            // Action
            TestEnum result = (TestEnum)_instance.GetOriginalValue(this);

            // Post-Conditions
            Assert.AreEqual(Test, result);
        }

        [Test]
        public void GetOriginalValueInputDirectionTest()
        {
            // Pre-Conditions
            FieldDescription instance = CreateInstance(new FieldAttribute{Direction = ParameterDirection.Input});

            // Action
            Assert.That(() => instance.GetOriginalValue(this), Throws.InvalidOperationException);
        }

        [Test]
        public void GetOriginalValueOutputtDirectionTest()
        {
            // Pre-Conditions
            FieldDescription instance = CreateInstance(new FieldAttribute { Direction = ParameterDirection.Output });

            // Action
            TestEnum result = (TestEnum)instance.GetOriginalValue(this);

            // Post-Conditions
            Assert.AreEqual(Test, result);
        }

        [Test]
        public void SetOriginalValueTest()
        {
            // Pre-Conditions 
            const TestEnum expected = TestEnum.One;

            // Action
            _instance.SetOriginalValue(this, expected);

            // Post-Conditions
            Assert.AreEqual(expected, Test);
        }

        [Test]
        public void SetOriginalValueOutputDirectionTest()
        {
            // Pre-Conditions
            FieldDescription instance = CreateInstance(new FieldAttribute { Direction = ParameterDirection.Output });

            // Action
            Assert.That(() => instance.SetOriginalValue(this, TestEnum.Two), Throws.InvalidOperationException);
        }

        [Test]
        public void SetOriginalValueInputDirectionTest()
        {
            // Pre-Conditions
            const TestEnum expected = TestEnum.One;
            FieldDescription instance = CreateInstance(new FieldAttribute { Direction = ParameterDirection.Input });

            // Action
            instance.SetOriginalValue(this, expected);

            // Post-Conditions
            Assert.AreEqual(expected, Test);
        }

        [Test]
        public void GetValuePropertyTest()
        {
            // Action
            string result = (string)_instance.GetValue(this);

            // Post-Conditions
            Assert.AreEqual(Test.ToString(), result);
        }

        [Test]
        public void SetValuePropertyTest()
        {
            // Pre-Conditions 
            const TestEnum expected = TestEnum.One;

            // Action
            _instance.SetValue(this, expected.ToString());

            // Post-Conditions
            Assert.AreEqual(expected, Test);
        }

        [Test]
        public void GetValueFieldTest()
        {
            // Pre-Conditions 
            TestField = 100;
            FieldInfo propertyInfo = typeof(FieldDescriptionTests).GetField("TestField");
            FieldDescription instance = new FieldDescription(propertyInfo, new FieldAttribute());

            // Action
            object result = instance.GetValue(this);

            // Post-Conditions
            Assert.AreEqual(TestField, result);
        }

        [Test]
        public void SetValueFieldTest()
        {
            // Pre-Conditions 
            TestField = 100;
            const int expected = 101;
            FieldInfo propertyInfo = typeof(FieldDescriptionTests).GetField("TestField");
            FieldDescription instance = new FieldDescription(propertyInfo, new FieldAttribute());

            // Action
            instance.SetValue(this, expected);

            // Post-Conditions
            Assert.AreEqual(expected, TestField);
        }

        [Test]
        public void GetValue_Array_Test()
        {
            // Pre-Conditions 
            string[] expected = new string[] { "1", "2", "3", "4" };
            IntArr = new int[] { 1, 2, 3, 4 };
            FieldDescription instance = CreateInstance(_fieldAttribute, "IntArr");

            // Action
            object result = instance.GetValue(this);

            // Post-Conditions
            CollectionAssert.AreEqual(expected, (string[])result);
        }

        [Test]
        public void SetValueArrayTest()
        {
            // Pre-Conditions 
            string[] values = new string[] { "1", "2", "3", "4" };
            int[] expected = new int[] { 1, 2, 3, 4 };
            IntArr = null;
            FieldDescription instance = CreateInstance(_fieldAttribute, "IntArr");

            // Action
            instance.SetValue(this, values);

            // Post-Conditions
            Assert.AreEqual(expected, IntArr);
        }

        [Test]
        public void SetValue_ArrayAsASimpleType_Test()
        {
            // Pre-Conditions 
            string values = Convert.ToBase64String(new byte[] { 1, 2, 3, 4 });
            int[] expected = new int[] { 1, 2, 3, 4 };
            IntArrAsSimpleField = null;
            _fieldAttribute.IsSimpleField = true;
            FieldDescription instance = CreateInstance(_fieldAttribute, "IntArrAsSimpleField");

            // Action
            instance.SetValue(this, values);

            // Post-Conditions
            Assert.AreEqual(expected, IntArrAsSimpleField);
        }

        [Test]
        public void GetValue_ArrayAsASimpleType_Test()
        {
            // Pre-Conditions 
            string expected = Convert.ToBase64String(new byte[] { 1, 2, 3, 4 });
            IntArrAsSimpleField = new int[] { 1, 2, 3, 4 };
            _fieldAttribute.IsSimpleField = true;
            FieldDescription instance = CreateInstance(_fieldAttribute, "IntArrAsSimpleField");

            // Action
            object result = instance.GetValue(this);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }
        #endregion

        #region Properties
        private TestEnum _testEnum = TestEnum.Two;

        [Convert(typeof(string))]
        [CallbackValidator(CallbackMethodName = "Validator", Type = typeof(FieldDescriptionTests))]
        public TestEnum Test
        {
            get { return _testEnum; }
            set { _testEnum = value; }
        }

        [Convert(typeof(string))]
        public int[] IntArr { get; set; }

        [Convert(typeof(string), "IntArrAsSimpleField")]
        public int[] IntArrAsSimpleField { get; set; }
        #endregion

        #region Methods
        protected int[] IntArrAsSimpleFieldFrom(string value, string propertyName)
        {
            return Convert.FromBase64String(value).Select(x => (int)x).ToArray();
        }

        protected string IntArrAsSimpleFieldTo(int[] value, string propertyName)
        {
            return Convert.ToBase64String(value.Select(x => (byte)x).ToArray());
        }

        private static FieldDescription CreateInstance(FieldAttribute fieldAttribute, string propertyName = "Test")
        {
            PropertyInfo propertyInfo = typeof(FieldDescriptionTests).GetProperty(propertyName);
            return new FieldDescription(propertyInfo, fieldAttribute);
        }
        #endregion

        #region Static methods
        public static void Validator(object value)
        {
            if (!(value is TestEnum))
            {
                throw new ArgumentOutOfRangeException();
            }
        }
        #endregion

        #region Enums
        public enum TestEnum : short
        {
            One,
            Two,
            Three
        }
        #endregion
    }
}