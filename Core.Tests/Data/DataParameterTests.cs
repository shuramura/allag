#region Using directives
using System;
using System.Data;
using System.Reflection;
using Allag.Core.Data.Attributes;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data
{
    [TestFixture]
    public class DataParameterTests
    {
        #region Variables
        public int TestField;

        private ObjectHelper _objectHelper;
        private FieldDescription _fieldDescription;
        private DataParameter _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            TestField = 100;

            _instance = CreateInstance<FieldDescriptionTests>("Test");
        }

        [TearDown]
        public void TearDown()
        {
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(DbType.Int16, _instance.DbType, "The property 'DbType' is wrong.");
            Assert.AreEqual(_fieldDescription.FieldAttribute.Direction, _instance.Direction, "The property 'Direction' is wrong.");
            Assert.AreEqual("Test", _instance.ParameterName, "The property 'ParameterName' is wrong.");
            Assert.AreEqual(FieldDescriptionTests.TestEnum.Two.ToString(), _instance.Value, "The property 'Value' is wrong.");
            Assert.AreEqual(_fieldDescription.FieldAttribute.Order, _instance.Order, "The property 'Order' is wrong.");
            Assert.AreEqual(_fieldDescription.FieldAttribute.IsNullable, _instance.IsNullable, "The property 'IsNullable' is wrong.");
            Assert.AreEqual(typeof(FieldDescriptionTests.TestEnum), _instance.ParameterType, "The property 'ParameterType' is wrong.");

            Assert.AreEqual(_fieldDescription.FieldAttribute.Precision, _instance.Precision, "The property 'Precision' is wrong.");
            Assert.AreEqual(_fieldDescription.FieldAttribute.Scale, _instance.Scale, "The property 'Scale' is wrong.");
            Assert.AreEqual(_fieldDescription.FieldAttribute.Size, _instance.Size, "The property 'Size' is wrong.");
        }

        [Test]
        public void CtorWithNullInstanceTest()
        {
            // Action
            Assert.That(() => new DataParameter(null, _fieldDescription), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CtorWithNullFieldDescriptionTest()
        {
            // Action
            Assert.That(() => new DataParameter(_objectHelper, null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void DbTypeSetTest()
        {
            // Pre-Conditions 
            const DbType expected = DbType.Xml;

            // Action
            _instance.DbType = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.DbType);
        }

        [Test]
        public void DirectionSetTest()
        {
            // Pre-Conditions 
            const ParameterDirection expected = ParameterDirection.ReturnValue;

            // Action
            _instance.Direction = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Direction);
        }

        [Test]
        public void ParameterNameSetTest()
        {
            // Pre-Conditions 
            const string expected = "lalala";

            // Action
            _instance.ParameterName = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.ParameterName);
        }

        [Test]
        public void ValueSetTest()
        {
            // Pre-Conditions 
            string expected = FieldDescriptionTests.TestEnum.Three.ToString();

            // Action
            _instance.Value = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Value);
        }

        [Test]
        public void ValueSetWithMultiConvertingTest()
        {
            // Pre-Conditions 
            DataParameter instance = CreateInstance<DataParameterTests>("MultiConvert");

            const bool expected = true;

            // Action
            instance.Value = expected;

            // Post-Conditions
            Assert.AreEqual(expected, instance.Value);
        }

        [Test]
        public void SourceColumnGetTest()
        {
            // Action
            Assert.That(() => _instance.SourceColumn, Throws.InstanceOf<NotSupportedException>());
        }

        [Test]
        public void SourceColumnSetTest()
        {
            // Action
            Assert.That(() => _instance.SourceColumn = "lalala", Throws.InstanceOf<NotSupportedException>());
        }

        [Test]
        public void SourceVersionGetTest()
        {
            // Action
            Assert.That(() => _instance.SourceVersion, Throws.InstanceOf<NotSupportedException>());
        }

        [Test]
        public void SourceVersionSetTest()
        {
            // Action
            Assert.That(() => _instance.SourceVersion = DataRowVersion.Current, Throws.InstanceOf<NotSupportedException>());
        }

        [Test]
        public void SetOriginalValueTest()
        {
            // Pre-Conditions 
            const FieldDescriptionTests.TestEnum expected = FieldDescriptionTests.TestEnum.Three;

            // Action
            _instance.SetOriginalValue(expected);

            // Post-Conditions
            Assert.AreEqual(expected.ToString(), _instance.Value);
        }

        [Test]
        public void PrecisionSetTest()
        {
            // Pre-Conditions 
            const byte expected = 100;

            // Action
            _instance.Precision = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Precision);
        }

        [Test]
        public void ScaleSetTest()
        {
            // Pre-Conditions 
            const byte expected = 100;

            // Action
            _instance.Scale = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Scale);
        }

        [Test]
        public void SizeSetTest()
        {
            // Pre-Conditions 
            const int expected = 100;

            // Action
            _instance.Size = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Size);
        }
        #endregion

        #region Properties
        [Convert(typeof(bool), Order = 0, MethodName = "Integer")]
        [Convert(typeof(int), Order = 1, MethodName = "Float")]
        public float MultiConvert { get; set; }
        #endregion

        #region Methods
        protected float FloatFrom(int value, string propertyName)
        {
            return value;
        }

        protected int FloatTo(float value, string propertyName)
        {
            return (int)Math.Round(value);
        }

        protected bool IntegerTo(int value, string propertyName)
        {
            return (value % 10) == 0;
        }

        protected int IntegerFrom(bool value, string propertyName)
        {
            return value ? 20 : 25;
        }

        private DataParameter CreateInstance<TInstance>(string propertyName) where TInstance : new()
        {
            PropertyInfo propertyInfo = typeof(TInstance).GetProperty(propertyName);
            _objectHelper = new ObjectHelper(new TInstance());
            _fieldDescription = new FieldDescription(propertyInfo, new FieldAttribute());
            return new DataParameter(_objectHelper, _fieldDescription);
        }
        #endregion
    }
}