﻿#region Using directives
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;
using Allag.Core.Tools;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Data.Queues
{
    [TestFixture]
    public class QueueExtenstionsTests
    {
        #region Tests
        [Test]
        public async Task MoveToAsync_NullQueue_Test()
        {
            // Action
            Assert.That(async () => await QueueExtenstions.MoveToAsync(null, new IQueue[] { MockRepository.GenerateStub<IQueue>() }, MockRepository.GenerateStub<IResource>(), (id, queueId, newId) => true), Throws.ArgumentNullException);
        }

        [Test]
        public async Task MoveToAsync_NullQueues_Test()
        {
            // Action
            Assert.That(async () => await QueueExtenstions.MoveToAsync(MockRepository.GenerateStub<IQueue>(), null, MockRepository.GenerateStub<IResource>(), (id, queueId, newId) => true), Throws.ArgumentNullException);
        }

        [Test]
        public async Task MoveToAsync_EmptyQueues_Test()
        {
            // Action
            Assert.That(async () => await QueueExtenstions.MoveToAsync(MockRepository.GenerateStub<IQueue>(), new IQueue[] { null }, MockRepository.GenerateStub<IResource>(), (id, queueId, newId) => true), Throws.ArgumentException);
        }

        [Test]
        [Timeout(2000)]
        public async Task MoveToAsync_Test()
        {
            // Pre-Conditions
            MockRepository mocks = new MockRepository();
            MemoryStream localStorage = new MemoryStream();
            IResource resource = mocks.StrictMock<IResource>();
            resource.Expect(x => x.GetStream(false)).Return(localStorage);
            IQueue queue = mocks.StrictMock<IQueue>();

            IQueue[] destinations = new IQueue[2];
            Guid[] ids = new Guid[destinations.Length];
            for (int i = 0; i < destinations.Length; i++)
            {
                destinations[i] = mocks.StrictMock<IQueue>();
                ids[i] = Guid.NewGuid();
                destinations[i].Stub(x => x.Id).Return(ids[i]);
            }

            Tuple<string, byte[]>[] messages = new Tuple<string, byte[]>[3];
            for (byte i = 0; i < messages.Length; i++)
            {
                byte[] data = new byte[i + 1];
                for (int j = 0; j < data.Length; j++)
                {
                    data[j] = (byte)(i + j);
                }
                messages[i] = new Tuple<string, byte[]>(Guid.NewGuid().ToString(), data);
            }

            using(CancellationTokenSource source = new CancellationTokenSource())
            {
                int nIndex = 0;
                queue.Expect(y => y.ReadAsync(Arg<CancellationToken>.Is.Equal(source.Token)))
                    .WhenCalled(invocation =>
                    {
                        MemoryStream stream = new MemoryStream();
                        Container<Stream> ret = null;
                        if (nIndex < messages.Length)
                        {
                            stream.Write(messages[nIndex].Item2, 0, messages[nIndex].Item2.Length);
                            ret = new Container<Stream>(messages[nIndex].Item1, stream);
                            stream.Position = 0;
                            nIndex++;
                        }
                        invocation.ReturnValue = Task.Run(async () =>
                        {
                            if (ret == null)
                            {
                                await Task.Delay(int.MaxValue, source.Token);
                            }
                            return ret;
                        });
                    }).Return(null);

                destinations.ForEach(y => y.Expect(x => x.IsActiveAsync(source.Token)).Return(Task.FromResult(true)).Repeat.AtLeastOnce());
                destinations.ForEach(y => y.Expect(x => x.WriteAsync(Arg<Stream>.Is.NotNull, Arg<CancellationToken>.Is.Equal(default(CancellationToken))))
                    .WhenCalled(invocation =>
                    {
                        Stream stream = (Stream)invocation.Arguments[0];
                        Assert.AreEqual(0, stream.Position, "The stream position is wrong.");
                        byte[] data = new byte[stream.Length];
                        stream.Read(data, 0, data.Length);
                        stream.Position = 0;
                        CollectionAssert.AreEqual(messages[data.Length - 1].Item2, data, "The message {0} is wrong.", data.Length - 1);
                        invocation.ReturnValue = Task.FromResult(data.Length.ToString(CultureInfo.InvariantCulture));
                    }).Repeat.Times(messages.Length).Return(null));

                int nNotificationCount = 0;

                mocks.ReplayAll();

                // Action
                Task result = QueueExtenstions.MoveToAsync(queue, destinations.Concat(new IQueue[] {null}).ToArray(),
                    resource,
                    (id, queueId, newId) =>
                    {
                        CollectionAssert.Contains(messages.Select(x => x.Item1), id, "The original message id is wrong.");
                        CollectionAssert.Contains(ids, queueId, "The destination queue id is wrong.");
                        Assert.IsNotNull(newId, "The new message id is wrong.");
                        nNotificationCount++;
                        return true;
                    }, source.Token);

                // Post-Conditions  
                await Task.Delay(1000);
                source.Cancel();
                result.Wait();
                mocks.VerifyAll();
                Assert.AreEqual(messages.Length * destinations.Length, nNotificationCount, "The notification count is wrong.");
            }
        }

        [Test]
        [Timeout(2000)]
        public async Task MoveToAsync_WithPrestoredMessage_Test()
        {
            // Pre-Conditions
            MockRepository mocks = new MockRepository();
            IQueue queue = mocks.StrictMock<IQueue>();

            IQueue[] destinations = new IQueue[2];
            Guid[] ids = new Guid[destinations.Length];
            for (int i = 0; i < destinations.Length; i++)
            {
                destinations[i] = mocks.StrictMock<IQueue>();
                ids[i] = Guid.NewGuid();
                destinations[i].Stub(x => x.Id).Return(ids[i]);
            }

            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream localStorage = new MemoryStream();
            byte[] messageData = new byte[] {1, 2, 3};
            MemoryStream message = new MemoryStream(messageData);
            const string strId = "id";
            formatter.Serialize(localStorage, strId);
            formatter.Serialize(localStorage, message);
            formatter.Serialize(localStorage, ids[0]);
            IResource resource = mocks.StrictMock<IResource>();
            resource.Expect(x => x.GetStream(false)).Return(localStorage);

            using (CancellationTokenSource source = new CancellationTokenSource())
            {
                destinations.Skip(1).ForEach(y => y.Expect(x => x.WriteAsync(Arg<Stream>.Is.NotNull, Arg<CancellationToken>.Is.Equal(default(CancellationToken))))
                    .WhenCalled(invocation =>
                    {
                        Stream stream = (Stream)invocation.Arguments[0];
                        Assert.AreEqual(0, stream.Position, "The stream position is wrong.");
                        byte[] data = new byte[stream.Length];
                        stream.Read(data, 0, data.Length);
                        stream.Position = 0;
                        CollectionAssert.AreEqual(messageData, data, "The message is wrong.");
                        invocation.ReturnValue = Task.FromResult(data.Length.ToString(CultureInfo.InvariantCulture));
                    }).Return(null));

                int nNotificationCount = 0;

                mocks.ReplayAll();

                // Action
                Task result = QueueExtenstions.MoveToAsync(queue, destinations.Concat(new IQueue[] { null }).ToArray(),
                    resource,
                    (id, queueId, newId) =>
                    {
                        Assert.AreEqual(strId, id, "The original message id is wrong.");
                        CollectionAssert.Contains(ids, queueId, "The destination queue id is wrong.");
                        Assert.IsNotNull(newId, "The new message id is wrong.");
                        nNotificationCount++;
                        source.Cancel();
                        return true;
                    }, source.Token);

                // Post-Conditions  
                await Task.Delay(1000);
                result.Wait();
                mocks.VerifyAll();
                Assert.AreEqual(ids.Length - 1, nNotificationCount, "The notification count is wrong.");
            }
        }

        [Test]
        public void MoveToAsync_InactiveDestinationQueue_Test()
        {
            // Pre-Conditions
            MockRepository mocks = new MockRepository();
            IQueue queue = mocks.StrictMock<IQueue>();
            IQueue destination = mocks.StrictMock<IQueue>();
            destination.Expect(x => x.Id).Return(Guid.NewGuid());
            destination.Expect(x => x.IsActiveAsync(Arg<CancellationToken>.Is.NotNull)).Return(Task.FromResult(false));
            mocks.ReplayAll();

            // Action
            Assert.That(async () => await QueueExtenstions.MoveToAsync(queue, new IQueue[] { destination }, null, (id, queueId, newId) => true), Throws.TypeOf<InactiveQueueException>());
        }

        [Test]
        public void MoveToAsync_WithException_Test()
        {
            // Pre-Conditions
            MockRepository mocks = new MockRepository();
            IQueue queue = mocks.StrictMock<IQueue>();
            IQueue destination = mocks.StrictMock<IQueue>();
            destination.Expect(x => x.Id).Return(Guid.NewGuid());
            destination.Expect(x => x.IsActiveAsync(Arg<CancellationToken>.Is.NotNull)).Throw(new AggregateException(new Exception()));
            mocks.ReplayAll();

            // Action
            Assert.That(async () => await QueueExtenstions.MoveToAsync(queue, new IQueue[] { destination }, null, (id, queueId, newId) => true), Throws.TypeOf<AggregateException>());
        }
        #endregion
    }
}