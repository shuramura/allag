﻿#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Queues
{
    [TestFixture]
    public class XmlMessageSerializerTests
    {
        #region Tests
        [Test]
        public void Serialize_Test()
        {
            // Pre-Conditions
            XmlMessageSerializer<TestMessage> instance = new XmlMessageSerializer<TestMessage>();
            TestMessage message = new TestMessage();

            // Action
            Container<byte[]> result = instance.Serialize(message);

            // Post-Conditions
            Assert.AreNotEqual(Guid.Empty, result.Id, "The property 'Id' is wrong.");
            Assert.IsNotNull(result.Item, "The property 'Item' is wrong.");
        }

        [Test]
        public void Deserialize_Test()
        {
            // Pre-Conditions
            XmlMessageSerializer<TestMessage> instance = new XmlMessageSerializer<TestMessage>();
            TestMessage message = new TestMessage();
            Container<byte[]> data = instance.Serialize(message);

            // Action
            Container<TestMessage> result = instance.Deserialize(data.Item);

            // Post-Conditions
            Assert.AreEqual(data.Id, result.Id, "The property 'Id' is wrong.");
            Assert.AreEqual(message.Id, result.Item.Id, "The property 'Item.Id' is wrong.");
        }
        #endregion

        #region Classes
        public class TestMessage
        {
            #region Constructors
            public TestMessage()
            {
                Id = Guid.NewGuid();
            }
            #endregion

            #region Properties
            public Guid Id { get; set; }
            #endregion
        }
        #endregion
    }
}