﻿#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Queues
{
    [TestFixture]
    public class MemoryQueueTests : BaseMemoryQueueTests<MemoryQueueTests.TestMessage>
    {
        #region Implementation of BaseQueueTests<TMessage>
        protected override MemoryQueue<TestMessage> CreateInstance(Guid id)
        {
            return new MemoryQueue<TestMessage>(id) {Serializer = new XmlMessageSerializer<TestMessage>()};
        }

        protected override TestMessage CreateMessage()
        {
            return new TestMessage();
        }

        protected override void ValidateMessage(TestMessage expected, TestMessage actual)
        {
            Assert.AreEqual(expected.Id, actual.Id, "The message is wrong");
        }
        #endregion

        #region Classes
        public class TestMessage
        {
            #region Constructors
            public TestMessage()
            {
                Id = Guid.NewGuid();
                Data = new string('a', 2048);
            }
            #endregion

            #region Properties
            public Guid Id { get; set; }
            public string Data { get; set; }
            #endregion
        }
        #endregion
    }
}