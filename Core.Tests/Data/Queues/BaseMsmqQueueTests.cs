﻿#region Using directives
using System;
using System.Messaging;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Queues
{
    public abstract class BaseMsmqQueueTests<TMessage>
        where TMessage : class
    {
        #region Constants
        private const string PATH = @".\private$\tcstest";
        private const string CONNECTION_STRING = "TestStorageConnectionString";
        #endregion

        #region Variables
        private Guid _id;
        private MsmqQueue<TMessage> _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _id = Guid.NewGuid();
            _instance = CreateInstance(_id, PATH);
        }

        [TearDown]
        public void CleanUp()
        {
            _instance.Dispose();
            MessageQueue queue = new MessageQueue(PATH);
            Message[] messages = queue.GetAllMessages();
            for (int i = 0; i < messages.Length; i++)
            {
                queue.Receive();
            }
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_EmptyId_Test()
        {
            // Action
            Assert.Throws<ArgumentException>(() => CreateInstance(Guid.Empty, PATH));
        }

        [Test]
        public void Ctor_WrongPath_Test([Values(null, "", "  ")] string path)
        {
            // Action
            Assert.Throws<ArgumentNullOrEmptyException>(() => CreateInstance(Guid.NewGuid(), path));
        }

        [Test]
        public async Task Ctor_WithConnectionString_Test()
        {
            // Action
            MsmqQueue<TMessage> instance = CreateInstance(Guid.NewGuid(), CONNECTION_STRING);

            // Post-Conditions
            Assert.IsTrue(await instance.IsActiveAsync());
        }

        [Test]
        public void Ctor_Test()
        {
            // Post-Conditions
            Assert.AreEqual(_id, _instance.Id, "The property 'Id' is wrong.");
            Assert.IsNotNull(_instance.Serializer, "The property 'Serializer' is wrong.");
            Assert.AreEqual(TimeSpan.FromMinutes(1), _instance.ReadDelay, "The property 'ReadDelay' is wrong.");
            Assert.AreEqual(TimeSpan.FromSeconds(10), _instance.ReadTimeout, "The property 'ReadTimeout' is wrong.");
        }

        [TestCase(ExpectedResult = true)]
        public async Task<bool> IsActiveAsync_Test()
        {
            // Action
            return await _instance.IsActiveAsync();
        }

        [Test]
        public async Task Dispose_Test()
        {
            // Action
            _instance.Dispose();

            // Post-Conditions
            Assert.That(async () => await _instance.WriteAsync(null), Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public async Task WriteAsync_NullStream_Test()
        {
            // Action
            string result;
            Assert.That(async () => result = await _instance.WriteAsync(null), Throws.ArgumentNullException);
        }

        [Test]
        public async Task WriteAsync_Test()
        {
            // Action
            string result = await _instance.WriteAsync(CreateMessage());

            // Post-Conditions
            Assert.That(result, Is.Not.Null.And.Not.Empty);
        }

        [Test]
        [Timeout(1000)]
        public async Task ReadAsync_Test()
        {
            // Pre-Conditions
            _instance.ReadTimeout = TimeSpan.FromMilliseconds(200);
            _instance.ReadDelay = TimeSpan.FromMilliseconds(100);

            // Action
            Task<Container<TMessage>> result = _instance.ReadAsync();

            // Post-Conditions
            Thread.Sleep(300);
            TMessage message = CreateMessage();
            string strId = await _instance.WriteAsync(message);
            Assert.AreEqual(strId, result.Result.Id, "The property 'Id' is wrong.");
            ValidateMessage(message, result.Result.Item);
            Assert.AreEqual(0, new MessageQueue(PATH).GetAllMessages().Length, "The queue length is wrong.");
        }

        [Test]
        [Timeout(2000)]
        public void ReadAsync_TypedEmptyQueue_Test()
        {
            // Pre-Conditions
            _instance.ReadTimeout = new TimeSpan(0, 0, 1);
            using(CancellationTokenSource source = new CancellationTokenSource())
            {
                CancellationToken token = source.Token;

                // Action
                Task<Container<TMessage>> result = _instance.ReadAsync(token);

                // Post-Conditions
                Thread.Sleep(100);
                source.Cancel();
                Assert.Throws<AggregateException>(() => result.Wait());
            }
        }
        #endregion

        #region Abstract methods
        protected abstract MsmqQueue<TMessage> CreateInstance(Guid id, string connectionString);
        protected abstract TMessage CreateMessage();
        protected abstract void ValidateMessage(TMessage expected, TMessage actual);
        #endregion
    }
}