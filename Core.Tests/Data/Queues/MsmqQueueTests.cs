﻿#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Queues
{
    [TestFixture]
    public class MsmqQueueTests : BaseMsmqQueueTests<MsmqQueueTests.TestMessage>
    {
        #region Implementation of BaseQueueTests<TMessage>
        protected override MsmqQueue<TestMessage> CreateInstance(Guid id, string connectionString)
        {
            return new MsmqQueue<TestMessage>(id, connectionString){Serializer = new XmlMessageSerializer<TestMessage>()};
        }

        protected override TestMessage CreateMessage()
        {
            return new TestMessage();
        }

        protected override void ValidateMessage(TestMessage expected, TestMessage actual)
        {
            Assert.AreEqual(expected.Id, actual.Id, "The message is wrong");
        }
        #endregion

        #region Classes
        public class TestMessage
        {
            #region Constructors
            public TestMessage()
            {
                Id = Guid.NewGuid();
            }
            #endregion

            #region Properties
            public Guid Id { get; set; }
            #endregion
        }
        #endregion
    }
}