﻿#region Using directives
using System;
using System.Threading.Tasks;
using Allag.Core.TestData.Data.Queues;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Data.Queues
{
    [TestFixture]
    public class QueueTests
    {
        #region Variables
        private Guid _id;
        private MockQueue<QueueTests, QueueTests> _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _id = Guid.NewGuid();
            _instance = new MockQueue<QueueTests, QueueTests>(_id, this);
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Post-Conditions
            Assert.AreEqual(_id, _instance.Id, "The property 'Id' is wrong.");
            Assert.IsNull(_instance.Serializer, "The property 'Serializer' is wrong.");
        }

        [Test]
        public void Ctor_WithCustomData_Test()
        {
            // Pre-Conditions
            Guid id = Guid.NewGuid();

            // Action
            Queue<QueueTests, QueueTests, QueueTests> instance = new MockQueueWithCustormData<QueueTests, QueueTests, QueueTests>(id);

            // Post-Conditions
            Assert.AreEqual(id, instance.Id, "The property 'Id' is wrong.");
        }

        [Test]
        public async Task IsActiveAsync_Test([Values(true, false)] bool expected)
        {
            // Pre-Conditions
            int nIsActiveCount = 0;
            _instance.IsActiveHandler = storage =>
            {
                Assert.AreEqual(this, storage, "The storage is wrong.");
                nIsActiveCount++;
                return expected;
            };

            // Action
            bool result = await _instance.IsActiveAsync();

            // Post-Conditions
            Assert.AreEqual(expected, result, "The result is wrong.");
            Assert.AreEqual(1, _instance.ReleaseCount, "The release count is wrong.");
            Assert.AreEqual(1, nIsActiveCount, "The active count is wrong.");
        }

        [Test]
        public async Task WriteAsync_Test()
        {
            // Pre-Conditions
            IMessageSerializer<QueueTests> serializer = MockRepository.GenerateStub<IMessageSerializer<QueueTests>>();
            serializer.Stub(x => x.Serialize(this)).Return(new Container<byte[]>("id", new byte[] {1, 2, 3}));
            _instance.Serializer = serializer;

            int nPushCount = 0;
            _instance.PushHandler = (repository, data) =>
            {
                Assert.AreEqual(this, repository, "The storage is wrong.");
                nPushCount++;
            };

            // Action
            string result = await _instance.WriteAsync(this);

            // Post-Conditions
            Assert.AreEqual("id", result, "The result is wrong.");
            Assert.AreEqual(1, _instance.ReleaseCount, "The release count is wrong.");
            Assert.AreEqual(1, nPushCount, "The push count is wrong.");
        }

        [Test]
        public async Task ReadAsync_Test()
        {
            // Pre-Conditions
            byte[] data = new byte[] {1, 2, 3};
            IMessageSerializer<QueueTests> serializer = MockRepository.GenerateStub<IMessageSerializer<QueueTests>>();
            serializer.Stub(x => x.Deserialize(Arg<byte[]>.Is.Equal(data))).Return(new Container<QueueTests>("id", this));
            _instance.Serializer = serializer;

            int nPopCount = 0;
            _instance.PopHandler = storage =>
            {
                Assert.AreEqual(this, storage, "The storage is wrong.");
                nPopCount++;
                return data;
            };

            // Action
            Container<QueueTests> result = await _instance.ReadAsync();

            // Post-Conditions
            Assert.AreEqual("id", result.Id, "The result id is wrong.");
            Assert.AreEqual(this, result.Item, "The result item is wrong.");
            Assert.AreEqual(1, _instance.ReleaseCount, "The release count is wrong.");
            Assert.AreEqual(1, nPopCount, "The pop count is wrong.");
        }
        #endregion
    }
}