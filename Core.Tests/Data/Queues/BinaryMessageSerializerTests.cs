﻿#region Using directives
using System;
using System.IO;
using System.Linq;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Queues
{
    [TestFixture]
    public class BinaryMessageSerializerTests
    {
        #region Variables
        private BinaryMessageSerializer _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _instance = new BinaryMessageSerializer();
        }
        #endregion

        #region Tests
        [Test]
        public void Serialize_NullMessage_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => _instance.Serialize(null));
        }

        [Test]
        public void Serialize_Stream_Test()
        {
            // Pre-Conditions
            byte[] expected = new byte[] {1, 2, 3, 4};
            Container<byte[]> result;

            // Action
            using(MemoryStream stream = new MemoryStream(expected))
            {
                result = _instance.Serialize(stream);
            }

            // Post-Conditions
            Assert.AreNotEqual(Guid.Empty, Guid.Parse(result.Id), "The property 'Id' is wrong.");
            CollectionAssert.AreEqual(expected, result.Item.Skip(16).ToArray(), "The property 'Item' is wrong.");
        }

        [Test]
        public void Serialize_Test()
        {
            // Pre-Conditions
            BinaryMessageSerializer<BinaryMessageSerializerTests> instance = new BinaryMessageSerializer<BinaryMessageSerializerTests>();

            // Action
            Assert.Throws<NotSupportedException>(() => instance.Serialize(this));
        }

        [Test]
        public void Deserialize_NullData_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => _instance.Deserialize(null));
        }

        [Test]
        public void Deserialize_WrongDataLength_Test()
        {
            // Action
            Assert.Throws<ArgumentException>(() => _instance.Deserialize(new byte[10]));
        }

        [Test]
        public void Deserialize_Stream_Test()
        {
            // Pre-Conditions
            byte[] expected = new byte[] { 1, 2, 3, 4 };
            Container<byte[]> container;
            using (MemoryStream stream = new MemoryStream(expected))
            {
                container = _instance.Serialize(stream);
            }

            // Action
            using (Container<Stream> result = _instance.Deserialize(container.Item))
            {
                // Post-Conditions
                byte[] resultData = new byte[result.Item.Length];
                result.Item.Read(resultData, 0, resultData.Length);
                Assert.AreEqual(container.Id, result.Id, "The property 'Id' is wrong.");
                CollectionAssert.AreEqual(expected, resultData, "The property 'Item' is wrong.");    
            }
        }

        [Test]
        public void Deserialize_Test()
        {
            // Pre-Conditions
            BinaryMessageSerializer<BinaryMessageSerializerTests> instance = new BinaryMessageSerializer<BinaryMessageSerializerTests>();

            // Action
            Assert.Throws<NotSupportedException>(() => instance.Deserialize(Guid.NewGuid().ToByteArray()));
        }
        #endregion
    }
}