﻿#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Queues
{
    [TestFixture]
    public class ContainerTests
    {
        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Pre-Conditions
            const string id = "id";

            // Action
            Container<ContainerTests> instance = new Container<ContainerTests>(id, this);

            // Post-Conditions
            Assert.AreEqual(id, instance.Id, "The property 'Id' is wrong.");
            Assert.AreEqual(this, instance.Item, "The property 'Item' is wrong.");
        }

        [Test]
        public void Ctor_WrongId_Test([Values(null, "", "  ")] string id)
        {
            // Action
            Assert.Throws<ArgumentNullOrEmptyException>(() => new Container<ContainerTests>(id, this));
        }

        [Test]
        public void Ctor_NullItem_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => new Container<ContainerTests>("id", null));
        }

        [Test]
        public void Ctor_NullIdProvider_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => new Container<ContainerTests>((Func<string>)null, null));
        }
        #endregion
    }
}