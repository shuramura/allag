﻿#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Queues
{
    [TestFixture]
    public class InactiveQueueExceptionTests
    {
        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Action
            InactiveQueueException instance = new InactiveQueueException();

            // Post-Conditions
            Assert.AreEqual(Guid.Empty, instance.Id, "The property 'Id' is wrong.");
            Assert.AreEqual("Exception of type 'Allag.Core.Data.Queues.InactiveQueueException' was thrown.", instance.Message, "The property 'Message' is wrong.");
            Assert.IsNull(instance.InnerException, "The property 'InnerException' is wrong.");
        }

        [Test]
        public void Ctor_WithId_Test()
        {
            // Pre-Conditions
            Guid id = Guid.NewGuid();

            // Action
            InactiveQueueException instance = new InactiveQueueException(id);

            // Post-Conditions
            Assert.AreEqual(id, instance.Id, "The property 'Id' is wrong.");
            Assert.AreEqual(string.Format("The queue '{0}' is inactive.", id), instance.Message, "The property 'Message' is wrong.");
            Assert.IsNull(instance.InnerException, "The property 'InnerException' is wrong.");
        }
        #endregion
    }
}