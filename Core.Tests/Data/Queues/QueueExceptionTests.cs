﻿#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Queues
{
    [TestFixture]
    public class QueueExceptionTests
    {
        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Action
            QueueException instance = new QueueException();

            // Post-Conditions
            Assert.AreEqual(Guid.Empty, instance.Id, "The property 'Id' is wrong.");
            Assert.AreEqual("Exception of type 'Allag.Core.Data.Queues.QueueException' was thrown.", instance.Message, "The property 'Message' is wrong.");
            Assert.IsNull(instance.InnerException, "The property 'InnerException' is wrong.");
        }

        [Test]
        public void Ctor_WithId_Test()
        {
            // Pre-Conditions
            Guid id = Guid.NewGuid();

            // Action
            QueueException instance = new QueueException(id);

            // Post-Conditions
            Assert.AreEqual(id, instance.Id, "The property 'Id' is wrong.");
            Assert.AreEqual("Exception of type 'Allag.Core.Data.Queues.QueueException' was thrown.", instance.Message, "The property 'Message' is wrong.");
            Assert.IsNull(instance.InnerException, "The property 'InnerException' is wrong.");
        }

        [Test]
        public void Ctor_WithInnerMessage_Test()
        {
            // Pre-Conditions
            Guid id = Guid.NewGuid();
            const string strMessage = "message";

            // Action
            QueueException instance = new QueueException(id, strMessage);

            // Post-Conditions
            Assert.AreEqual(id, instance.Id, "The property 'Id' is wrong.");
            Assert.AreEqual(strMessage, instance.Message, "The property 'Message' is wrong.");
            Assert.IsNull(instance.InnerException, "The property 'InnerException' is wrong.");
        }

        [Test]
        public void Ctor_WithInnerException_Test()
        {
            // Pre-Conditions
            Guid id = Guid.NewGuid();
            const string strMessage = "message";
            Exception exception = new Exception();

            // Action
            QueueException instance = new QueueException(id, strMessage, exception);

            // Post-Conditions
            Assert.AreEqual(id, instance.Id, "The property 'Id' is wrong.");
            Assert.AreEqual(strMessage, instance.Message, "The property 'Message' is wrong.");
            Assert.AreEqual(exception, instance.InnerException, "The property 'InnerException' is wrong.");
        }
        #endregion
    }
}