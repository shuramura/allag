﻿#region Using directives
using System;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Queues
{
    public abstract class BaseMemoryQueueTests<TMessage>
        where TMessage : class
    {
        #region Variables
        private Guid _id;
        private MemoryQueue<TMessage> _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _id = Guid.NewGuid();
            _instance = CreateInstance(_id);
        }

        [TearDown]
        public void CleanUp()
        {
            _instance.Dispose();
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_EmptyId_Test()
        {
            // Action
            Assert.That(() => CreateInstance(Guid.Empty), Throws.ArgumentException);
        }

        [Test]
        public void Ctor_Test()
        {
            // Post-Conditions
            Assert.AreEqual(_id, _instance.Id, "The property 'Id' is wrong.");
            Assert.AreEqual(4096, _instance.Capacity, "The property 'Capacity' is wrong.");
            Assert.IsNotNull(_instance.Serializer, "The property 'Serializer' is wrong.");
            Assert.AreEqual(TimeSpan.FromMinutes(1), _instance.ReadDelay, "The property 'ReadDelay' is wrong.");
        }

        [TestCase(ExpectedResult = true)]
        public async Task<bool> IsActiveAsync_Test()
        {
            // Action
           return await _instance.IsActiveAsync();
        }

        [Test]
        public async Task Dispose_Test()
        {
            // Action
            _instance.Dispose();

            // Post-Conditions
            Assert.That(async () => await _instance.WriteAsync(null), Throws.TypeOf<ObjectDisposedException>());
        }

        [TestCase(5000, ExpectedResult = 5000)]
        [TestCase(4000, ExpectedResult = 4096)]
        public int Capacity_Test(int capacity)
        {
            // Action
            _instance.Capacity = capacity;

            // Post-Conditions
            return _instance.Capacity;
        }

        [Test]
        public async Task WriteAsync_NullStream_Test()
        {
            // Action
            string result;
            Assert.That(async () => result = await _instance.WriteAsync(null), Throws.ArgumentNullException);
        }

        [Test]
        public async Task WriteAsync_Test()
        {
            // Action
            string result = await _instance.WriteAsync(CreateMessage());

            // Post-Conditions
            Assert.That(() => result, Is.Not.Null.And.Not.Empty);
        }

        [Test]
        [Timeout(2000)]
        public async Task ReadAsync_Test()
        {
            // Pre-Conditions
            _instance.ReadDelay = TimeSpan.FromMilliseconds(100);

            // Action
            Task<Container<TMessage>> result = _instance.ReadAsync();

            // Post-Conditions
            Thread.Sleep(300);
            TMessage message = CreateMessage();
            string strId = await _instance.WriteAsync(message);
            Assert.AreEqual(strId, result.Result.Id, "The property 'Id' is wrong.");
            ValidateMessage(message, result.Result.Item);
        }

        [Test]
        [Timeout(2000)]
        public void ReadAsync_TypedEmptyQueue_Test()
        {
            // Pre-Conditions
            using(CancellationTokenSource source = new CancellationTokenSource())
            {
                CancellationToken token = source.Token;

                // Action
                Task<Container<TMessage>> result = _instance.ReadAsync(token);

                // Post-Conditions
                Thread.Sleep(100);
                source.Cancel();
                Assert.Throws<AggregateException>(() => result.Wait());
            }
        }
        #endregion

        #region Abstract methods
        protected abstract MemoryQueue<TMessage> CreateInstance(Guid id);
        protected abstract TMessage CreateMessage();
        protected abstract void ValidateMessage(TMessage expected, TMessage actual);
        #endregion
    }
}