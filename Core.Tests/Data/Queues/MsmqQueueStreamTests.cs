﻿#region Using directives
using System;
using System.IO;
using NUnit.Framework;

#endregion

namespace Allag.Core.Data.Queues
{
    [TestFixture]
    public class MsmqQueueStreamTests : BaseMsmqQueueTests<Stream>
    {
        #region Implementation of BaseQueueTests<Stream>
        protected override MsmqQueue<Stream> CreateInstance(Guid id, string connectionString)
        {
            return new MsmqQueue(id, connectionString){Serializer = new BinaryMessageSerializer<Stream>()};
        }

        protected override Stream CreateMessage()
        {
            return new MemoryStream(new byte[] {1, 2, 3, 4});
        }

        protected override void ValidateMessage(Stream expected, Stream actual)
        {
            byte[] expectedData = new byte[expected.Length];
            expected.Read(expectedData, 0, expectedData.Length);

            byte[] data = new byte[actual.Length];
            actual.Read(data, 0, data.Length);

            CollectionAssert.AreEqual(expectedData, data, "The message is wrong.");
        }
        #endregion
    }
}