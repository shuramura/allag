#region Using directives

using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration
{
    [TestFixture]
    public class NameValueElementTests
    {
        #region Variables
        private NameValueElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new NameValueElement();
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
			// Post-Conditions
            Assert.AreEqual(string.Empty, _instance.Value, "The property 'Value' is wrong.");
        }

        [Test]
        public void ValueSetTest()
        {
            // Pre-Conditions
            const string expected = "lalala";

            // Action
            _instance.Value = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Value);
        }

        #endregion
    }
}
