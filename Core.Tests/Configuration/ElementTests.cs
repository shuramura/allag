#region Using directives
using System;
using System.IO;
using System.Xml;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration
{
    [TestFixture]
    public class ElementTests
    {
        #region Variables
        private Element _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new Element();
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(string.Empty, _instance.Name, "The property 'Name' is wrong.");
        }

        [Test]
        public void NameSetTest()
        {
            // Pre-Conditions
            const string expected = "lalala";

            // Action
            _instance.Name = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Name);
        }

        [Test]
        public void DeserializeNullReaderTest()
        {
            // Action
            Assert.That(() => Element.Deserialize(null, (reader, b) => b = false), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void DeserializeNullHandlerTest()
        {
            // Action
            using (StringReader reader = new StringReader("<root/>"))
            {
                Assert.That(() => Element.Deserialize(XmlReader.Create(reader), null), Throws.InstanceOf<ArgumentNullException>());
            }
        }

        [Test]
        public void DeserializeTest()
        {
            // Action
            using (StringReader reader = new StringReader("<root external=\"&lt;newRoot&gt;>\"/>"))
            {
                Element.Deserialize(XmlReader.Create(reader), (xmlReader, b) =>
                {
                    xmlReader.Read();
                    Assert.AreEqual("newRoot", xmlReader.Name);
                });
            }
        }
        #endregion
    }
}
