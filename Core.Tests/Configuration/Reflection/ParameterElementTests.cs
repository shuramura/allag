#region Using directives
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Allag.Core.Reflection;
using Allag.Core.TestData.Reflection;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Reflection
{
    [TestFixture]
    public class ParameterElementTests
    {
        #region Variables
        private ParameterElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new ParameterElement();
        }

        [TearDown]
        public void TearDown()
        {
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(0, _instance.Parameters.Count, "The property 'Parameters' is wrong.");
            Assert.AreEqual(0, _instance.Members.Count, "The property 'Members' is wrong.");
            Assert.AreEqual(CreationType.New, _instance.CreationType, "The property 'CreationType' is wrong.");
            Assert.AreEqual(false, _instance.IsNull, "The property 'IsNull' is wrong.");
            Assert.IsEmpty(_instance.StringOwner, "The property 'StringOwner' is wrong.");
        }

        [Test]
        public void GetValueNullTypeTest()
        {
            // Action
            Assert.That(() => _instance.GetValue(GetType(), null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void GetValueIntTest()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("int");

            // Action
            int result = (int)instance.GetValue(GetType(), typeof(int));

            // Post-Conditions
            Assert.AreEqual(20, result);
        }

        [Test]
        public void GetValueEnumTest()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("enum");

            // Action
            FileMode result = (FileMode)instance.GetValue(GetType(), typeof(FileMode));

            // Post-Conditions
            Assert.AreEqual(FileMode.CreateNew, result);
        }

        [Test]
        public void GetValueDateTimeTest()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("dateTime");

            // Action
            DateTime result = (DateTime)instance.GetValue(GetType(), typeof(DateTime));

            // Post-Conditions
            Assert.AreEqual(new DateTime(2009, 4, 22, 17, 5, 10), result);
        }

        [Test]
        public void GetValueTimeSpanTest()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("timeSpan");

            // Action
            TimeSpan result = (TimeSpan)instance.GetValue(GetType(), typeof(TimeSpan));

            // Post-Conditions
            Assert.AreEqual(new TimeSpan(0, 01, 10, 0), result);
        }

        [Test]
        public void GetValueStringTest()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("string");

            // Action
            string result = (string)instance.GetValue(GetType(), typeof(string));

            // Post-Conditions
            Assert.AreEqual("lalala", result);
        }

        [Test]
        public void GetValueArrayTest()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("array");

            // Action
            int[] result = (int[])instance.GetValue(GetType(), typeof(int[]));

            // Post-Conditions
            CollectionAssert.AreEqual(new[]{1, 2, 3, 4, 5, 6}, result);
        }

        [Test]
        public void GetValueMultiArrayTest()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("multiarray");

            // Action
            string[,] result = (string[,])instance.GetValue(GetType(), typeof(string[,]));

            // Post-Conditions
            CollectionAssert.AreEqual(new[,] { {"00", "01"}, {"10", null}}, result);
        }

        [Test]
        public void GetValueTypeTest()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("type");

            // Action
            Type result = (Type)instance.GetValue(GetType(), typeof(Type));

            // Post-Conditions
            Assert.AreEqual(typeof(int), result);
        }

        [Test]
        public void GetValueInterfaceWithNewCreationTest()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("testClass0");

            // Action
            ITestFake result = (ITestFake)instance.GetValue(GetType(), typeof(ITestFake));

            // Post-Conditions
            Assert.AreNotEqual(ObjectFactory.Get<ITestFake>(this), result);
        }

        [Test]
        public void GetValueComplexWithParametersTest()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("testClass1");

            // Action
            TestClass result = (TestClass)instance.GetValue(GetType(), typeof(TestClass));

            // Post-Conditions
            Assert.AreEqual("lala", result.String);
        }

        [Test]
        public void GetValue_DictionaryInitialization_Test()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("testClass1");

            // Action
            TestClass result = (TestClass)instance.GetValue(GetType(), typeof(TestClass));

            // Post-Conditions
            Assert.AreEqual(2, result.Dictionary.Count, "The count is wrong.");
            Assert.AreEqual(new TimeSpan(1, 2, 3), result.Dictionary["one"], "The value is wrong.");
        }

        [Test]
        public void GetValue_DictionaryThisProperty_Test()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("testClass1");

            // Action
            TestClass result = (TestClass)instance.GetValue(GetType(), typeof(TestClass));

            // Post-Conditions
            Assert.AreEqual(new TimeSpan(7, 8, 9), result.Dictionary["two"], "The value is wrong.");
        }

        [Test]
        public void GetValueInterfaceWithoutNewCreationTest()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("testFake");

            // Action
            ITestFake result = (ITestFake)instance.GetValue(GetType(), typeof(ITestFake));

            // Post-Conditions
            Assert.AreEqual(ObjectFactory.Get<ITestFake>(this), result);
        }

        [Test]
        public void GetValueComplexWithoutParametersTest()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("testClass2");

            // Action
            TestClass result = (TestClass)instance.GetValue(GetType(), typeof(TestClass));

            // Post-Conditions
            Assert.AreEqual(new TestClass().String, result.String);
        }

        [Test]
        public void GetValueGenericIEnumerableTest()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("enumerable");

            // Action
            int[] result = (int[])instance.GetValue(GetType(), typeof(IEnumerable<int>));

            // Post-Conditions
            CollectionAssert.AreEqual(new[] { 1, 2, 3, 4, 5, 6 }, result);
        }

        [Test]
        public void GetValueWrongTypeTest()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("wrongType");

            // Action
            Assert.That(() => instance.GetValue(GetType(), typeof (IEnumerable<int>)), Throws.InstanceOf<ConfigurationErrorsException>());
        }

        [Test]
        public void GetValueWrongParameterTest()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("wrongParameter");

            // Action
            Assert.That(() => instance.GetValue(GetType(), typeof(TestClass)), Throws.InvalidOperationException);
        }

        [Test]
        public void GetValueWrongMethodTest()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("wrongMethod");

            // Action
            Assert.That(() => instance.GetValue(GetType(), GetType()), Throws.InvalidOperationException);
        }

        [Test]
        public void GetValueInterfaceWithTypeConverterTest()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("convertableInterface");

            // Action
            object result = instance.GetValue(GetType(), typeof(IConvertableTest));

            // Post-Conditions
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetValueInterfaceWithoutTypeConverterTest()
        {
            // Pre-Conditions
            ParameterElement instance = GetInstance("notConvertableInterface");

            // Action
            object result = instance.GetValue(GetType(), typeof(IDisposable));

            // Post-Conditions
            Assert.IsNull(result);
        }
        #endregion

        #region Methods
        private static ParameterElement GetInstance(string parameterName)
        {
            return CoreCfg.Objects.Items["Allag.Core.Configuration.Reflection.ParameterElementTests, Allag.Core.Tests : Allag.Core.TestData.Reflection.ITest, Allag.Core.Tests"].Parameters[parameterName];
        }
        #endregion
    }
}