#region Using directives

using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Reflection
{
    [TestFixture]
    public class ObjectsSectionTests
    {
        #region Tests
        [Test]
        public void GetSectionTest()
        {
            // Action
            ObjectsSection result = CoreCfg.Objects;

            // Post-Conditions
            Assert.IsNotNull(result, "The result is wrong.");
            Assert.AreEqual("objects", result.DefaultName, "The property 'DefaultName' is wrong.");
        }
        #endregion
    }
}
