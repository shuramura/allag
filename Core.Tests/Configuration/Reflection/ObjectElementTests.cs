#region Using directives
using System;
using System.IO;
using Allag.Core.TestData.Reflection;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Reflection
{
    [TestFixture]
    public class ObjectElementTests
    {
        #region Variables
        private ObjectElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new ObjectElement();
        }

        [TearDown]
        public void TearDown()
        {
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(0, _instance.Parameters.Count, "The property 'Parameters' is wrong.");
            Assert.AreEqual(0, _instance.Members.Count, "The property 'Members' is wrong.");
            Assert.AreEqual(null, _instance.Owner, "The property 'Owner' is wrong.");
            Assert.AreEqual(null, _instance.Type, "The property 'Type' is wrong.");
        }

        [Test]
        public void TypeGetWithWrongValueTest()
        {
            // Action
            Assert.That(() => _instance.Interface, Throws.InstanceOf<TypeLoadException>());
        }

        [Test]
        public void CreateObjectWithWrongValueTest()
        {
            // Action
            object result = _instance.CreateObject();

            // Post-Conditions
            Assert.IsNull(result);
        }

        [Test]
        public void CreateObjectNullTypeTest()
        {
            // Action
            Assert.That(() => ObjectElement.CreateObject(GetType(), null, new ElementCollection<ParameterElement>()), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CreateObjectNullParametersTest()
        {
            // Action
            Assert.That(() => ObjectElement.CreateObject(GetType(), GetType(), null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void InitializeObjectNullInstanceTest()
        {
            // Action
            Assert.That(() => ObjectElement.InitializeObject(GetType(), null, new ElementCollection<MemberElement>()), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void InitializeObjectNullMembersTest()
        {
            // Action
            Assert.That(() => ObjectElement.InitializeObject(GetType(), this, null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CreateObject_Test()
        {
            // Pre-Conditions
            bool isInitializing = false;
            TestClass.Initializing += delegate { isInitializing = true; };

            bool isBeginInitialize = false;
            TestClass.BeginInitialize += delegate { isBeginInitialize = true; };

            bool isEndInitialize = false;
            TestClass.EndInitialize += delegate { isEndInitialize = true; };

            ObjectElement instance = CoreCfg.Objects.Items["Allag.Core.Configuration.Reflection.ObjectElementTests, Allag.Core.Tests : Allag.Core.TestData.Reflection.ITest, Allag.Core.Tests"];

            // Action
            TestClass result = (TestClass) instance.CreateObject();

            // Post-Conditions
            Assert.IsNotNull(result, "The result is wrong.");

            Assert.AreEqual(true, isBeginInitialize, "The begin initialization is not invoked.");
            Assert.AreEqual(true, isEndInitialize, "The end initialization is not invoked.");
            Assert.AreEqual(true, isInitializing, "The method is not invoked.");

            Assert.AreEqual(1, result.NullableInt, "The property 'NullableInt' is wrong.");
            Assert.AreEqual(20, result.Int, "The field 'Int' is wrong.");
            Assert.AreEqual(FileMode.CreateNew, result.Enum, "The property 'Enum' is wrong.");
            Assert.AreEqual(new DateTime(2009, 04, 22, 17, 05, 10), result.DateTime, "The property 'DateTime' is wrong.");
            Assert.AreEqual("lalala", result.String, "The field 'String' is wrong.");

            Assert.AreEqual(15, result.TestProperty, "The property 'TestProperty' is wrong.");
            Assert.AreEqual("lalalala", result.BaseString, "The property 'BaseString' is wrong.");

            Assert.AreEqual("lala", result.ChildInfo[0].String, "The property 'ChildInfo[0].String' is wrong.");

            Assert.AreEqual(40, result.ReadOnlyProperty.Int, "The readonly property is wrong.");
            Assert.AreEqual(FileMode.OpenOrCreate, result.ReadOnlyProperty.Enum, "The readonly property instance is wrong.");

            Assert.AreEqual(50, result.ReadOnlyField.Int, "The readonly field is wrong.");
            Assert.AreEqual(FileMode.Open, result.ReadOnlyField.Enum, "The readonly field instance is wrong.");

            Assert.AreEqual(new Guid("47620354-8189-4D5A-B6DB-947469938F99"), result.Guid, "The property 'Guid' is wrong.");
            Assert.AreEqual(typeof(ObjectElementTests), result.GenericType, "The property 'GenericType' is wrong.");
        }

        [Test]
        public void CreateObject_OverrideConstructorParameter_Test()
        {
            // Pre-Conditions
            ObjectElement instance = CoreCfg.Objects.Items["Allag.Core.Configuration.Reflection.ObjectElementTests, Allag.Core.Tests : Allag.Core.TestData.Reflection.ITest, Allag.Core.Tests"];

            // Action
            TestClass result = (TestClass)instance.CreateObject(new { @int = 10 });

            // Post-Conditions
            Assert.AreEqual(10, result.Int, "The field 'Int' is wrong.");
        }

        [Test]
        public void CreateObject_AddParameterFromCode_Test()
        {
            // Pre-Conditions
            TestClass readOnlyProperty = new TestClass();
            ObjectElement instance = CoreCfg.Objects.Items["Allag.Core.Configuration.Reflection.ObjectElementTests, Allag.Core.Tests : Allag.Core.TestData.Reflection.ITest, Allag.Core.Tests"];

            // Action
            TestClass result = (TestClass)instance.CreateObject(new { readOnlyProperty });

            // Post-Conditions
            Assert.AreEqual(readOnlyProperty, result.ReadOnlyProperty, "The field 'ReadOnlyProperty' is wrong.");
        }

        [Test]
        public void CreateObjectWithoutParametersAndWithMembersTest()
        {
            // Pre-Conditions
            ObjectElement instance = CoreCfg.Objects.Items["Allag.Core.Configuration.Reflection.ObjectElementTests, Allag.Core.Tests : Allag.Core.TestData.Reflection.TestClass+Child, Allag.Core.Tests"];

            // Action
            TestClass.Child result = (TestClass.Child) instance.CreateObject();

            // Post-Conditions
            Assert.IsNotNull(result, "The result is wrong.");
            Assert.AreEqual("lalala", result.String, "The field 'String' is wrong.");
        }

        [Test]
        public void CreateObject_WithDefaultParameters_Test()
        {
            // Pre-Conditions
            ObjectElement instance = CoreCfg.Objects.Items["Allag.Core.Configuration.Reflection.ObjectElementTests, Allag.Core.Tests : Allag.Core.TestData.Reflection.TestClass, Allag.Core.Tests"];

            // Action
            TestClass result = (TestClass) instance.CreateObject();

            // Post-Conditions
            Assert.IsNotNull(result, "The result is wrong.");
            Assert.AreEqual(50, result.Int, "The property 'Int' is wrong.");
            Assert.AreEqual(100, result.TestProperty, "The property 'TestProperty' is wrong.");
            Assert.AreEqual(FileMode.OpenOrCreate, result.Enum, "The property 'Enum' is wrong.");
        }
        #endregion
    }
}