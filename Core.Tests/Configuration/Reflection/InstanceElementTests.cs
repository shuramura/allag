﻿#region Using directives
using System;
using Allag.Core.TestData;
using Allag.Core.TestData.Configuration;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Reflection
{
    [TestFixture]
    public class InstanceElementTests
    {
        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Action
            InstanceElement instance = new InstanceElement();

            // Post-Conditions
            Assert.IsNull(instance.Name, "The property 'Name' is wrong.");
            Assert.IsNull(instance.Type, "The property 'Type' is wrong.");
        }

        [Test]
        public void Ctor_FromConfig_Test()
        {
            // Action
            InstanceElement instance = CoreCfg.Root.GetSection<TestSection>().Instance;

            // Post-Conditions
            Assert.AreEqual(typeof(TestElement), instance.Name, "The property 'Name' is wrong.");
            Assert.AreEqual(typeof(TestElement), instance.Type, "The property 'Type' is wrong.");
        }

        [Test]
        public void Name_Set_Test()
        {
            // Pre-Conditions
            InstanceElement instance = new InstanceElement();
            Type expected = GetType();

            // Action
            instance.Name = expected;

            // Post-Conditions
            Assert.AreEqual(expected, instance.Name, "The property 'Name' is wrong.");
            Assert.AreEqual(expected, instance.Type, "The property 'Type' is wrong.");

        }
        #endregion
    }
}