﻿#region Using directives
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Configuration
{
    public class SectionTests
    {
        #region Tests
        [Test]
        public void DefaultName_Get_Test()
        {
            // Pre-Conditions
            MockRepository mocks = new MockRepository();
            Section instance = mocks.PartialMock<Section>();
            string expected = instance.GetType().Name.Substring(0, instance.GetType().Name.Length - 7);
            expected = expected.Substring(0, 1).ToLower() + expected.Substring(1);
            mocks.ReplayAll();

            // Action
            string result = instance.DefaultName;

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }
        #endregion
    }
}