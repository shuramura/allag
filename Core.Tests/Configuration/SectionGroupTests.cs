#region Using directives
using System;
using Allag.Core.Configuration.Reflection;
using Allag.Core.TestData.Configuration;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration
{
    [TestFixture]
    public class SectionGroupTests
    {
        #region Variables
        private SectionGroup _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = SectionGroup.Instance.GetRoot("allag.core");
        }
        #endregion

        #region Tests
        [Test]
        public void CtorWithNullConfigNameTest()
        {
            // Action
            Assert.That(() => new SectionGroup(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CtorWithNotExistConfigNameTest()
        {
            // Action
            SectionGroup instance = new SectionGroup("lalala");

            // Post-Conditions
            Assert.AreEqual("allag.core", instance.GetRoot("allag.core").SectionGroupName);
        }

        [Test]
        public void GetRootWithNullNameTest()
        {
            // Action
            Assert.That(() => _instance.GetRoot(null), Throws.ArgumentException);
        }

        [Test]
        public void GetRootWithEmptyNameTest()
        {
            // Action
            Assert.That(() => _instance.GetRoot(string.Empty), Throws.ArgumentException);
        }

        [Test]
        public void GetSectionDefaultNmaeTest()
        {
            // Action
            TestSection result = _instance.GetSection<TestSection>();

            // Post-Conditions
            Assert.IsNotNull(result, "The result is wrong.");
            Assert.IsNotNull(result.Items["TestElement"], "The property 'Items.GetItem' is wrong.");
        }

        [Test]
        public void GetSectionWithNullNameTest()
        {
            // Action
            Assert.That(() => _instance.GetSection<ObjectsSection>(null), Throws.ArgumentException);
        }

        [Test]
        public void GetSectionWithEmptyNameTest()
        {
            // Action
            Assert.That(() => _instance.GetSection<ObjectsSection>(string.Empty), Throws.ArgumentException);
        }


        [Test]
        public void GetSectionWithAbsentNameTest()
        {
            // Action
            ObjectsSection result = _instance.GetSection<ObjectsSection>("lalala");

            // Post-Conditions
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetSection_NotInConfigFile_Test()
        {
            // Pre-Conditions
            SectionGroup instance = SectionGroup.Instance.GetRoot(Guid.NewGuid().ToString("N"));

            // Action
            TestSection result = instance.GetSection<TestSection>("lalala");

            // Post-Conditions
            Assert.IsNotNull(result);
        }
        #endregion
    }
}