#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration
{
    [TestFixture]
    public class GeneralSectionTests
    {
        #region Variables
        private GeneralSection _instance;
        #endregion

        #region Initializatin
        [SetUp]
        public void SetUp()
        {
            _instance = new GeneralSection();
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual("general", _instance.DefaultName, "The property 'DefaultName' is wrong.");
            Assert.AreEqual(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, _instance.ApplicationBase, "The property 'ApplicationBase' is wrong.");
        }
        #endregion
    }
}
