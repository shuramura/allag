#region Using directives

using Allag.Core.Tools.Job;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Job
{
    [TestFixture]
    public class MonthlyElementTests
    {
        #region Variables
        private MonthlyElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new MonthlyElement();
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(1, _instance.Day, "The property 'Day' is wrong.");
            Assert.AreEqual(NumberOfWeek.First, _instance.NumberOfWeek, "The property 'NumberOfWeek' is wrong.");
            Assert.AreEqual(System.DayOfWeek.Monday, _instance.DayOfWeek, "The property 'DayOfWeek' is wrong.");
            Assert.AreEqual(Month.All, _instance.Months, "The property 'Months' is wrong.");
            Assert.AreEqual(true, _instance.UseMonths, "The property 'UseMonths' is wrong.");
        }
        #endregion
    }
}
