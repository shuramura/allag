#region Using directives

using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Job
{
    [TestFixture]
    public class WeeklyElementTests
    {
        #region Variables
        private WeeklyElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new WeeklyElement();
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(DayOfWeek.Monday, _instance.DayOfWeek, "The property 'DayOfWeek' is wrong.");
        }
        #endregion
    }
}
