﻿#region Using directives
using System;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Configuration.Job
{
    [TestFixture]
    public class BaseIntervalElementTests
    {
        #region Variables
        private MockRepository _mocks;
        private BaseIntervalElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _mocks = new MockRepository();
            _instance = _mocks.PartialMock<BaseIntervalElement>();
            _mocks.ReplayAll();
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(TimeSpan.Zero, _instance.Start, "The property 'Start' is wrong.");
            Assert.AreEqual(TimeSpan.MaxValue, _instance.Stop, "The property 'Stop' is wrong.");
            Assert.AreEqual(false, _instance.RunImmediately, "The property 'RunImmediately' is wrong.");
        }
        #endregion
    }
}