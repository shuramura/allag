#region Using directives
using Allag.Core.Tools.Job;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Job
{
    [TestFixture]
    public class JobElementTests
    {
        #region Variables
        private JobElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new JobElement();
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(ScheduleType.None, _instance.ScheduleType, "The property 'ScheduleType' is wrong.");
            Assert.AreEqual(string.Empty, _instance.ScheduleName, "The property 'ScheduleName' is wrong.");
            Assert.AreEqual(0, _instance.Dependents.Count, "The property 'PostJobs' is wrong.");
        }
        #endregion
    }
}