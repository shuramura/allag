﻿#region Using directives
using System;
using System.IO;
using Allag.Core.Tools.Job;
using Allag.Core.Tools.Job.IntervalProviders;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Job
{
    [TestFixture]
    public class FileSystemElementTests
    {
        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Action
            FileSystemElement instance = new FileSystemElement();

            // Post-Conditions
            Assert.AreEqual(CoreCfg.General.ApplicationBase, instance.Path, "The property 'Path' is wrong.");
            Assert.AreEqual("", instance.Mask, "The property 'Mask' is wrong.");
            Assert.AreEqual(NotifyFilters.LastWrite, instance.Filter, "The property 'Filter' is wrong.");
            Assert.AreEqual(4096, instance.EventBuffer, "The property 'EventBuffer' is wrong.");
            Assert.AreEqual(false, instance.Subdirectories, "The property 'Subdirectories' is wrong.");
        }

        [Test]
        public void Ctor_FromConfigFile_Test()
        {
            // Action
            FileSystemElement instance = CoreCfg.Jobs.FileSystem[string.Empty];

            // Post-Conditions
            Assert.AreEqual(Path.Combine(Application.Base, @"TestData\Configuration\Job\FileSystem"), instance.Path, "The property 'Path' is wrong.");
            Assert.AreEqual("*.txt", instance.Mask, "The property 'Mask' is wrong.");
            Assert.AreEqual(NotifyFilters.Attributes | NotifyFilters.Size, instance.Filter, "The property 'Filter' is wrong.");
            Assert.AreEqual(5120, instance.EventBuffer, "The property 'EventBuffer' is wrong.");
            Assert.AreEqual(true, instance.Subdirectories, "The property 'Subdirectories' is wrong.");
        }

        [Test]
        public void Test()
        {
            // Pre-Conditions
            FileSystemElement instance = CoreCfg.Jobs.FileSystem[string.Empty];
            DateTime now = DateTime.Now;
            TimeSpan start = new TimeSpan(10, 10, 00);
            TimeSpan stop = new TimeSpan(20, 20, 00);

            // Action
            FileSystem result = instance.GetProvider(ScheduleType.FileSystem) as FileSystem;

            // Post-Conditions
            Assert.IsNotNull(result, "The result is wrong.");

            Assert.AreEqual(start, result.StartTime, "The property 'Start' is wrong.");
            Assert.AreEqual(stop, result.StopTime, "The property 'Stop' is wrong.");
            Assert.AreEqual(true, result.RunImmediately, "The property 'RunImmediately' is wrong.");
            Assert.AreEqual(NotifyFilters.Attributes | NotifyFilters.Size, result.Filter, "The property 'Filter' is wrong.");
            Assert.AreEqual(5120, result.EventBuffer, "The property 'EventBuffer' is wrong.");
            Assert.AreEqual(true, result.Subdirectories, "The property 'Subdirectories' is wrong.");
        }

        #endregion
    }
}