﻿#region Using directives
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Job
{
    [TestFixture]
    public class GroupElementTests
    {
        #region Variables
        private GroupElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new GroupElement();
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(0, _instance.Items.Count, "The property 'Items' is wrong.");
        }
        #endregion
    }
}