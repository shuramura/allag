#region Using directives

using Allag.Core.Tools.Job;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Job
{
    [TestFixture]
    public class DailyElementTests
    {
        #region Variables
        private DailyElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new DailyElement();
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(DayOfWeek.All, _instance.DayOfWeeks, "The property 'DayOfWeeks' is wrong.");
            Assert.AreEqual(true, _instance.UseDayOfWeeks, "The property 'UseDayOfWeeks' is wrong.");
        }

        #endregion
    }
}
