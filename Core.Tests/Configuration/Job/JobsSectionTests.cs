#region Using directives

using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Job
{
    [TestFixture]
    public class JobsSectionTests
    {
        #region Tests
        [Test]
        public void GetSectionTest()
        {
            // Action
            JobsSection result = CoreCfg.Jobs;

            // Post-Conditions
            Assert.IsNotNull(result, "The instance is wrong.");
            Assert.AreEqual("jobs", result.DefaultName, "The property 'DefaultName' is wrong.");

            Assert.AreEqual(1, result.Items.Count, "The property 'Items' is wrong.");
            Assert.AreEqual(2, result.Groups.Count, "The property 'Groups' is wrong.");

            Assert.AreEqual(1, result.SimpleSchedule.Count, "The property 'SimpleSchedule' is wrong.");
            Assert.AreEqual(2, result.Daily.Count, "The property 'Daily' is wrong.");
            Assert.AreEqual(3, result.Weekly.Count, "The property 'Weekly' is wrong.");
            Assert.AreEqual(0, result.Monthly.Count, "The property 'Monthly' is wrong.");
            Assert.AreEqual(1, result.FileSystem.Count, "The property 'FileSystem' is wrong.");
            Assert.AreEqual(0, result.Custom.Count, "The property 'Custom' is wrong.");
        }
        #endregion
    }
}
