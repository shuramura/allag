#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Job
{
    [TestFixture]
    public class SimpleElementTests
    {
        #region Variables
        private SimpleElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new SimpleElement();
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(1, _instance.Every, "The property 'Every' is wrong.");
        }

        [Test]
        public void ValidateEveryParameterLessThenZeroTest()
        {
            // Action
            Assert.That(() => SimpleElement.ValidateEveryParameter(-10D), Throws.ArgumentException);
        }

        [Test]
        public void ValidateEveryParameterZeroTest()
        {
            // Action
            Assert.That(() => SimpleElement.ValidateEveryParameter(0D), Throws.ArgumentException);
        }
        #endregion
    }
}
