﻿#region Using directives
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Job
{
    [TestFixture]
    public class DependentJobElementTests
    {
        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Action
            DependentJobElement instance = new DependentJobElement();

            // Post-Conditions
            Assert.IsTrue(instance.Synchronized, "The property 'Synchronized' is wrong.");
        }
        #endregion
    }
}