﻿#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration
{
    [TestFixture]
    public class SimpleElementTests
    {
        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Action
            SimpleElement instance = new SimpleElement();

            // Post-Conditions
            Assert.IsInstanceOf<Guid>(instance.Key, "The type of property 'Key' is wrong.");
            Assert.AreNotEqual(Guid.Empty, instance.Key, "The property 'Key' is wrong.");
        }
        #endregion
    }
}