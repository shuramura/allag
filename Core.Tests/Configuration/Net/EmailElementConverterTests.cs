﻿#region Using directives
using Allag.Core.Tools;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Net
{
    [TestFixture]
    public class EmailElementConverterTests
    {
        #region Variables
        private EmailElementConverter _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new EmailElementConverter();
        }

        [TearDown]
        public void TearDown()
        {
        }
        #endregion

        #region Tests
        [Test]
        public void Convert_From_Test()
        {
            // Post-Conditions
            Assert.AreEqual("TestEmail0", ((EmailElement) _instance.ConvertFrom(null, null, "TestEmail0")).Name);
        }

        [Test]
        public void Convert_FromNotExistElement_Test()
        {
            // Pre-Conditions
            string[] expected = new[] {"0", "1", "2"};

            // Action
            EmailElement result = (EmailElement) _instance.ConvertFrom(null, null, "0,1,2");

            // Post-Conditions
            CollectionAssert.AreEqual(expected, result.Addresses.ToString().Split(','), "The email list is wrong.");
        }

        [Test]
        public void Convert_FromIResource_Test()
        {
            // Pre-Conditions
            IResource resource = new MemoryResource("email@email.com");

            // Action
            EmailElement result = (EmailElement)_instance.ConvertFrom(null, null, resource);

            // Post-Conditions
            Assert.IsNotNull(result, "The result is wrong.");
            Assert.AreEqual(resource, result.Addresses, "The property 'Addresses' is wrong.");
        }

        [Test]
        public void Convert_ToIResource_Test()
        {
            // Pre-Conditions
            const string strEmail = "email@email.com";
            EmailElement emailElement = new EmailElement(strEmail);
            

            // Action
            IResource result = (IResource)_instance.ConvertTo(emailElement, typeof(IResource));

            // Post-Conditions
            Assert.IsInstanceOf<MemoryResource>(result, "The result is wrong.");
            Assert.AreEqual(strEmail, result.ToString(), "The result value is wrong.");
        }
        #endregion
    }
}