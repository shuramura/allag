#region Using directives

using System;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Configuration.Net
{
    [TestFixture]
    public class ServerElementTests
    {
        #region Variables
        private MockRepository _mocks;
        private ServerElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _mocks = new MockRepository();
            _instance = _mocks.PartialMock<ServerElement>(10, 70000);
            _mocks.ReplayAll();
        }
        #endregion

        #region Tests
        [Test]
        public void CtorWithWrongTimeoutTest()
        {
            // Action
            Assert.That(() => _mocks.PartialMock<ServerElement>(2, 59999), Throws.Exception.With.InnerException.InstanceOf<ArgumentOutOfRangeException>());
        }

        [Test]
        public void CtorTest()
        {
			// Post-Conditions
            Assert.AreEqual(false, _instance.EnableSsl, "The property 'EnableSsl' is wrong.");
            Assert.AreEqual(string.Empty, _instance.Host, "The property 'Host' is wrong.");
            Assert.AreEqual(10, _instance.Port, "The property 'Port' is wrong.");
            Assert.IsNotNull(_instance.Credential, "The property 'Credential' is wrong.");
            Assert.AreEqual(70000, _instance.Timeout, "The property 'Timeout' is wrong.");
        }
        #endregion
    }
}
