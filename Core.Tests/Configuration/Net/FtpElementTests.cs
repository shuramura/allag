#region Using directives
using System.Net;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Net
{
    [TestFixture]
    public class FtpElementTests
    {
        #region Variables
        protected FtpElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new FtpElement();
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual("ftp", _instance.UriScheme, "The property 'UriScheme' is wrong.");
            Assert.AreEqual(string.Empty, _instance.Host, "The property 'Host' is wrong.");
            Assert.AreEqual(21, _instance.Port, "The property 'Port' is wrong.");
            Assert.AreEqual(false, _instance.EnableSsl, "The property 'EnableSsl' is wrong.");
            Assert.AreEqual(false, _instance.UsePassive, "The property 'UsePassive' is wrong.");
            Assert.AreEqual(300000, _instance.Timeout, "The property 'Timeout' is wrong.");
            Assert.IsFalse(_instance.UseDefaultProxy, "The property 'UseDefaultProxy' is wrong.");
        }

        [Test]
        public void GetRequestTest()
        {
            // Pre-Conditions 
            const string strPath = "path";
            FtpElement instance = CoreCfg.Servers.Ftps["TestFtp"];

            // Action
            FtpWebRequest result = instance.GetRequest(strPath);

            // Post-Conditions
            Assert.AreEqual(instance.EnableSsl, result.EnableSsl, "The property 'EnableSsl' is wrong.");
            Assert.IsNotNull(result.Credentials, "The property 'Credentials' is wrong.");
            Assert.AreEqual(instance.KeepAlive, result.KeepAlive, "The property 'KeepAlive' is wrong.");
            Assert.AreEqual(null, result.Proxy, "The property 'Proxy' is wrong.");
            Assert.AreEqual(instance.Timeout, result.Timeout, "The property 'Timeout' is wrong.");
            Assert.IsTrue(instance.UseDefaultProxy, "The property 'UseDefaultProxy' is wrong.");
        }

        #endregion
    }
}
