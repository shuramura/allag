#region Using directives

using System;
using Allag.Core.Tools;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Net
{
    [TestFixture]
    public class EmailElementTests
    {
        #region Variables

        private EmailElement _instance;

        #endregion

        #region Initialization

        [SetUp]
        public void SetUp()
        {
            _instance = new EmailElement();
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        #region Tests

        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(null, _instance.Addresses, "The property 'Addresses' is wrong.");
            Assert.NotNull(_instance.Smtp, "The property 'Smtp' is wrong.");
            Assert.AreEqual(_instance.Smtp.Configuration.Subject, _instance.Subject, "The property 'Subject' is wrong.");
            Assert.AreEqual(_instance.Smtp.Configuration.IsBodyHtml, _instance.IsBodyHtml,
                            "The property 'IsBodyHtml' is wrong.");
            Assert.AreEqual(_instance.Smtp.Configuration.Message, _instance.Message, "The property 'Message' is wrong.");
        }

        [Test]
        public void CtorNullAddressesTest()
        {
            // Action
            Assert.That(() => new EmailElement((IResource)null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CtorWithParametersTest()
        {
            // Pre-Conditions
            const string expected = "0,1,2";

            // Action
            EmailElement instance = new EmailElement(expected);

            // Post-Conditions
            CollectionAssert.AreEqual(expected, instance.Addresses.ToString(), "The property 'Addresses' is wrong.");
            Assert.AreEqual("default", instance.Smtp.Host, "The property 'Smtp' is wrong.");
        }

        [Test]
        public void CtorWithSmtpTest()
        {
            // Pre-Conditions
            const string expected = "0,1,2";
            const string strSmtpName = "LocalSmtp";

            // Action
            EmailElement instance = new EmailElement(expected, strSmtpName);

            // Post-Conditions
            CollectionAssert.AreEqual(expected, instance.Addresses.ToString(), "The property 'Addresses' is wrong.");
            Assert.AreEqual("localhost", instance.Smtp.Host, "The property 'Smtp' is wrong.");
            Assert.IsTrue(instance.IsBodyHtml, "The property 'IsBodyHtml' is wrong.");
        }

        [Test]
        public void CtorFromConfigTest()
        {
            // Pre-Conditions 
            EmailsSection emailsSection = CoreCfg.Emails;
            EmailElement instance = emailsSection.Items["TestEmail0"];

            // Post-Conditions
            Assert.AreEqual(2, instance.Addresses.ToString().Split(',').Length, "The property 'Addresses' is wrong.");
            CollectionAssert.AreEqual("cc@test.com", instance.Cc.ToString(), "The property 'Cc' is wrong.");
            CollectionAssert.AreEqual("bcc@test.com", instance.Bcc.ToString(), "The property 'Bcc' is wrong.");
            Assert.AreEqual("Email subject", instance.Subject.ToString(), "The property 'Subject' is wrong.");
            Assert.IsTrue(instance.IsBodyHtml, "The property 'IsBodyHtml' is wrong.");
            Assert.AreEqual("Email message", instance.Message.ToString(), "The property 'Message' is wrong.");
            Assert.AreEqual(2, instance.Attachments.Count, "The property 'Attachments' is wrong.");
        }

        [Test]
        public void SmtpGetTest()
        {
            // Pre-Conditions 
            EmailsSection emailsSection = CoreCfg.Emails;
            EmailElement instance = emailsSection.Items["TestEmail0"];

            // Post-Conditions
            Assert.AreEqual("default", instance.Smtp.Host, "The property 'Smtp' is wrong.");
        }

        #endregion
    }
}