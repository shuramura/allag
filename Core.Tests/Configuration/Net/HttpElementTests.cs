#region Using directives

using System.Globalization;
using System.Net;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Net
{
    [TestFixture]
    public class HttpElementTests
    {
        #region Variables
        private HttpElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new HttpElement();
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
			// Post-Conditions
            Assert.AreEqual("http", _instance.UriScheme, "The property 'UriScheme' is wrong.");
            Assert.AreEqual(80, _instance.Port, "The property 'Port' is wrong.");
            Assert.AreEqual(100000, _instance.Timeout, "The property 'Timeout' is wrong.");
            Assert.AreEqual(false, _instance.KeepAlive, "The property 'KeepAlive' is wrong.");
            Assert.AreEqual(CultureInfo.CurrentCulture.Name, _instance.AcceptLanguage, "The property 'AcceptLanguage' is wrong.");
            Assert.IsTrue(_instance.UseDefaultProxy, "The property 'UseDefaultProxy' is wrong.");
        }

        [Test]
        public void GetRequestTest()
        {
            // Pre-Conditions 
            const string strPath = "path";
            HttpElement instance = CoreCfg.Servers.Https["1"];

            // Action
            HttpWebRequest result = instance.GetRequest(strPath);

            // Post-Conditions
            Assert.AreEqual(instance.Host, result.Address.Host, "The property 'Address.Host' is wrong.");
            Assert.AreEqual(instance.Port, result.Address.Port, "The property 'Address.Port' is wrong.");
            Assert.AreEqual("https", result.Address.Scheme, "The property 'Address.Scheme' is wrong.");
            Assert.AreEqual("/" + strPath, result.Address.PathAndQuery, "The property 'Address.PathAndQuery' is wrong.");

            Assert.AreEqual(false, result.UseDefaultCredentials, "The property 'UseDefaultCredentials' is wrong.");
            Assert.IsNotNull(result.Credentials, "The property 'Credentials' is wrong.");
            Assert.AreEqual(instance.KeepAlive, result.KeepAlive, "The property 'KeepAlive' is wrong.");
            Assert.IsNull(result.Proxy, "The property 'Proxy' is wrong.");
            Assert.AreEqual(instance.Timeout, result.Timeout, "The property 'Timeout' is wrong.");

            Assert.AreEqual(CultureInfo.CurrentCulture.Name, result.Headers[HttpRequestHeader.AcceptLanguage], "The property 'Accept-Language: ' is wrong.");
        }
        #endregion
    }
}
