﻿#region Using directives
using System;
using System.IO;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Net
{
    [TestFixture]
    public class FileSystemElementTests
    {
        #region Variables
        private FileSystemElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _instance = new FileSystemElement();
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Post-Conditions
            Assert.AreEqual("file", _instance.UriScheme, "The property 'UriScheme' is wrong.");
            Assert.AreEqual(CoreCfg.General.ApplicationBase, _instance.BasePath, "The property 'BasePath' is wrong.");
            Assert.IsTrue(_instance.IsLocked, "The property 'IsLocked' is wrong.");
        }

        [Test]
        public void Ctor_AbsolutePath_Test()
        {
            // Action
            FileSystemElement instance = CoreCfg.Servers.FileSystems["absolute"];

            // Post-Conditions
            Assert.AreEqual(@"c:\temp", instance.BasePath, "The property 'Root' is wrong.");
        }

        [Test]
        public void Ctor_RelativePath_Test()
        {
            // Action
            FileSystemElement instance = CoreCfg.Servers.FileSystems["relative"];

            // Post-Conditions
            Assert.AreEqual(Path.Combine(CoreCfg.General.ApplicationBase, "temp"), instance.BasePath, "The property 'Root' is wrong.");
            Assert.IsFalse(instance.IsLocked, "The property 'IsLocked' is wrong.");
        }

        [Test]
        public void GetDirectoryInfo_Test()
        {
            // Pre-Conditions
            FileSystemElement instance = CoreCfg.Servers.FileSystems["absolute"];

            // Action
            DirectoryInfo result = instance.GetDirectoryInfo();

            // Post-Conditions
            Assert.AreEqual(new DirectoryInfo(@"c:\temp"), result);
        }

        [Test]
        public void BaseUri_Get_NetworkPath_Test()
        {
            // Pre-Conditions
            FileSystemElement instance = CoreCfg.Servers.FileSystems["network"];

            // Action
            Uri result = instance.BaseUri;

            // Post-Conditions
            Assert.AreEqual(new Uri(@"file://\\test\TestData\Net\FileSystem"), result);
        }
        
        #endregion
    }
}