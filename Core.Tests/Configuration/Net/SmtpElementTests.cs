#region Using directives
using System;
using System.Net.Mail;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Net
{
    [TestFixture]
    public class SmtpElementTests
    {
        #region Variables
        private SmtpElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new SmtpElement();
        }

        [TearDown]
        public void TearDown()
        {
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual("mailto", _instance.UriScheme, "The property 'UriScheme' is wrong.");
            Assert.AreEqual(string.Empty, _instance.Host, "The property 'Host' is wrong.");
            Assert.AreEqual(25, _instance.Port, "The property 'Port' is wrong.");
            Assert.AreEqual(false, _instance.EnableSsl, "The property 'EnableSsl' is wrong.");
            Assert.AreEqual(100000, _instance.Timeout, "The property 'Timeout' is wrong.");
            Assert.AreEqual(SmtpDeliveryMethod.Network, _instance.DeliveryMethod, "The property 'DeliveryMethod' is wrong.");
            Assert.AreEqual(Application.Base, _instance.PickupDirectoryLocation, "The property 'PickupDirectoryLocation' is wrong.");
            Assert.AreEqual(string.Empty, _instance.Email.ToString(), "The property 'Email' is wrong.");
            Assert.AreEqual(string.Empty, _instance.DisplayName.ToString(), "The property 'DisplayName' is wrong.");
            Assert.AreEqual(string.Empty, _instance.Subject.ToString(), "The property 'Subject' is wrong.");
            Assert.AreEqual(false, _instance.IsBodyHtml, "The property 'IsBodyHtml' is wrong.");
            Assert.AreEqual(string.Empty, _instance.Message.ToString(), "The property 'Message' is wrong.");
            Assert.AreEqual(string.Empty, _instance.Bcc.ToString(), "The property 'Bcc' is wrong.");
            Assert.AreEqual(string.Empty, _instance.Cc.ToString(), "The property 'Cc' is wrong.");
        }

        [Test]
        public void InitializeSmtpClientNullSmtpClientTest()
        {
            // Action
            Assert.That(() => _instance.InitializeSmtpClient(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void InitializeSmtpClientTest()
        {
            // Pre-Conditions 
            SmtpElement instance = CoreCfg.Servers.Smtps["1"];
            SmtpClient result = new SmtpClient("1");

            // Action
            instance.InitializeSmtpClient(result);

            // Post-Conditions
            Assert.AreEqual("email@test.com.au", instance.Email.ToString(), "The property 'Email' is wrong.");
            Assert.AreEqual("test", instance.DisplayName.ToString(), "The property 'DisplayName' is wrong.");
            Assert.AreEqual(instance.Port, result.Port, "The property 'Port' is wrong.");
            Assert.AreEqual(instance.DeliveryMethod, result.DeliveryMethod, "The property 'DeliveryMethod' is wrong.");
            Assert.AreEqual(instance.PickupDirectoryLocation, result.PickupDirectoryLocation, "The property 'PickupDirectoryLocation' is wrong.");
            Assert.AreEqual(instance.EnableSsl, result.EnableSsl, "The property 'EnableSsl' is wrong.");
            Assert.AreEqual(instance.Timeout, result.Timeout, "The property 'Timeout' is wrong.");
            Assert.IsNotNull(result.Credentials, "The property 'Credentials' is wrong.");
            Assert.AreEqual(1, instance.Attachments.Count, "The property 'Attachments' is wrong.");
        }
        #endregion
    }
}