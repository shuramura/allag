﻿#region Using directives
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Net
{
    [TestFixture]
    public class CustomServerElementTests
    {
        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Action
            CustomServerElement instance = new CustomServerElement();

            // Post-Conditions
            Assert.AreEqual(string.Empty, instance.Schema, "The property 'Schema' is wrong.");
        }

        [Test]
        public void Ctor_FromConfig_Test()
        {
            // Action
            CustomServerElement instance = CoreCfg.Servers.Customs["CustomClient"];

            // Post-Conditions
            Assert.AreEqual("custom", instance.Schema, "The property 'Schema' is wrong.");
        }
        #endregion
    }
}