﻿#region Using directives
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Net
{
    [TestFixture]
    public class HttpElementConverterTests
    {
        #region Variables
        private HttpElementConverter _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new HttpElementConverter();
        }

        [TearDown]
        public void TearDown()
        {
        }
        #endregion

        #region Tests
        [Test]
        public void ConvertFromTest()
        {
            // Post-Conditions
            Assert.AreEqual("TestHttp", ((HttpElement) _instance.ConvertFrom(null, null, "TestHttp")).Name);
        }

        [Test]
        public void ConvertFromNotExistElementTest()
        {
            // Post-Conditions
            Assert.AreEqual("localhost", ((HttpElement) _instance.ConvertFrom(null, null, "lalala")).Host);
        }
        #endregion
    }
}