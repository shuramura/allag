#region Using directives

using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Net
{
    [TestFixture]
    public class CredentialElementTests
    {
        #region Variables
        private CredentialElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new CredentialElement();
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
			// Post-Conditions
            Assert.AreEqual(string.Empty, _instance.Domain, "The property 'Domain' is wrong.");
            Assert.AreEqual(string.Empty, _instance.UserName, "The property 'UserName' is wrong.");
            Assert.AreEqual(string.Empty, _instance.Password, "The property 'Password' is wrong.");
        }

        #endregion
    }
}
