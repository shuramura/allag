#region Using directives
using System;
using Allag.Core.Net;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Net
{
    [TestFixture]
    public class ServersSectionTests
    {
        #region Variables
        private ServersSection _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _instance = CoreCfg.Servers;
        }
        #endregion

        #region Tests
        [Test]
        public void GetSectionTest()
        {
            // Post-Conditions
            Assert.AreEqual("servers", _instance.DefaultName, "The property 'DefaultName' is wrong.");
            Assert.AreEqual(6, _instance.Https.Count, "The property 'Https' is wrong.");
            Assert.AreEqual(4, _instance.Ftps.Count, "The property 'Ftps' is wrong.");
            Assert.AreEqual(5, _instance.Smtps.Count, "The property 'Smtps' is wrong.");
        }

        [Test]
        public void GetFileClient_WrongScheme_Test([Values(null, "", "   ")] string scheme)
        {
            // Action
            Assert.That(() => _instance.GetFileClient(scheme, "name"), Throws.InstanceOf<ArgumentNullOrEmptyException>());
        }

        [Test]
        public void GetFileClient_UnsupportedScheme_Test()
        {
            // Action
            Assert.That(() => _instance.GetFileClient("unsupported", "name"), Throws.InstanceOf<NotSupportedException>());
        }

        [Test]
        public void GetFileClient_NullName_Test()
        {
            // Action
            Assert.That(() => _instance.GetFileClient(Uri.UriSchemeHttp, null), Throws.InstanceOf<ArgumentNullException>());
        }

        [TestCase("file", "absolute")]
        [TestCase("ftp", "TestFtp")]
        [TestCase("custom", "CustomClient")]
        public void GetFileClient_ExistsConfiguration_Test(string scheme, string name)
        {
            // Action
            IFileClient result = _instance.GetFileClient(scheme, name);

            // Post-Conditions
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetFileClient_NotExistsConfiguration_Test([Values("file", "ftp")] string scheme)
        {
            // Pre-Conditions 
            const string strName = "Unknown";

            // Action
            Assert.That(() =>_instance.GetFileClient(scheme, strName), Throws.InstanceOf<ArgumentNullException>());
        }
        #endregion
    }
}