#region Using directives

using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Net
{
    [TestFixture]
    public class EmailsSectionTests
    {
        #region Tests
        [Test]
        public void GetSectionTest()
        {
            // Action
            EmailsSection result = CoreCfg.Emails;

            // Post-Conditions
            Assert.AreEqual(3, result.Items.Count, "The property 'Items' is wrong.");
            Assert.AreEqual("emails", result.DefaultName, "The property 'DefaultName' is wrong.");
        }
        #endregion
    }
}
