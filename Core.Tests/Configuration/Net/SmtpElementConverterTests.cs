﻿#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Net
{
    [TestFixture]
    public class SmtpElementConverterTests
    {
        #region Variables
        private SmtpElementConverter _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new SmtpElementConverter();
        }

        [TearDown]
        public void TearDown()
        {
        }
        #endregion

        #region Tests
        [Test]
        public void CanConvertFromStringTest()
        {
            // Pre-Conditions 
            const bool expected = true;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.CanConvertFrom(null, typeof (string)));
        }

        [Test]
        public void CanConvertFromElementTest()
        {
            // Pre-Conditions 
            const bool expected = true;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.CanConvertFrom(null, typeof (SmtpElement)));
        }

        [Test]
        public void CanConvertFromOtherTest()
        {
            // Pre-Conditions 
            const bool expected = false;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.CanConvertFrom(null, typeof (byte)));
        }

        [Test]
        public void CanConvertToStringTest()
        {
            // Pre-Conditions 
            const bool expected = true;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.CanConvertTo(null, typeof (string)));
        }

        [Test]
        public void CanConvertToElementTest()
        {
            // Pre-Conditions 
            const bool expected = true;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.CanConvertTo(null, typeof (SmtpElement)));
        }

        [Test]
        public void CanConvertToOtherTest()
        {
            // Pre-Conditions 
            const bool expected = false;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.CanConvertTo(null, typeof (byte)));
        }

        [Test]
        public void ConvertFromStringTest()
        {
            // Pre-Conditions 
            SmtpElement expected = CoreCfg.Servers.Smtps["0"];

            // Action
            SmtpElement result = (SmtpElement) _instance.ConvertFrom(null, null, "0");

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ConvertFromElementTest()
        {
            // Pre-Conditions 
            SmtpElement expected = CoreCfg.Servers.Smtps["0"];

            // Action
            SmtpElement result = (SmtpElement) _instance.ConvertFrom(null, null, expected);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ConvertFromEmptyStringTest()
        {
            // Action
            SmtpElement result = (SmtpElement) _instance.ConvertFrom(null, null, "");

            // Post-Conditions
            Assert.AreEqual("default", result.Host);
        }

        [Test]
        public void ConvertFromOtherTest()
        {
            // Action
            Assert.That(() => _instance.ConvertFrom(null, null, 0), Throws.InstanceOf<NotSupportedException>());
        }

        [Test]
        public void ConvertToStringTest()
        {
            // Pre-Conditions 
            const string expected = "0";

            // Action
            string result =
                (string)
                _instance.ConvertTo(null, null, CoreCfg.Servers.Smtps["0"],
                                    typeof (string));

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ConvertToEmptyStringTest()
        {
            // Action
            string result = (string) _instance.ConvertTo(null, null, new SmtpElement(), typeof (string));

            // Post-Conditions
            Assert.IsEmpty(result);
        }

        [Test]
        public void ConvertToElementTest()
        {
            // Pre-Conditions 
            SmtpElement expected = CoreCfg.Servers.Smtps["0"];

            // Action
            SmtpElement result = (SmtpElement) _instance.ConvertTo(null, null, expected, typeof (SmtpElement));

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ConvertToOtherTest()
        {
            // Action
            Assert.That(() => _instance.ConvertTo(null, null, new SmtpElement(), typeof (float)), Throws.InstanceOf<NotSupportedException>());
        }
        #endregion
    }
}