﻿#region Using directives
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Net
{
    [TestFixture]
    public class FtpElementConverterTests
    {
        #region Variables
        private FtpElementConverter _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new FtpElementConverter();
        }

        [TearDown]
        public void TearDown()
        {
        }
        #endregion

        #region Tests
        [Test]
        public void ConvertFromTest()
        {
            // Post-Conditions
            Assert.AreEqual("TestFtp", ((FtpElement) _instance.ConvertFrom(null, null, "TestFtp")).Name);
        }

        [Test]
        public void ConvertFromNotExistElementTest()
        {
            // Post-Conditions
            Assert.AreEqual("localhost", ((FtpElement) _instance.ConvertFrom(null, null, "lalala")).Host);
        }
        #endregion
    }
}