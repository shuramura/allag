#region Using directives

using System.ServiceProcess;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Service
{
    [TestFixture]
    public class ServiceElementTests
    {
        #region Variables
        protected ServiceElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new ServiceElement();
        }
        #endregion

        #region Tests

        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(string.Empty, _instance.DisplayName, "The property 'DisplayName' is wrong.");
            Assert.AreEqual(string.Empty, _instance.Description, "The property 'Description' is wrong.");
            Assert.AreEqual(ServiceStartMode.Automatic, _instance.StartType, "The property 'StartType' is wrong.");
        }
        #endregion
    }
}
