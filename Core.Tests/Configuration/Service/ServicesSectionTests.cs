#region Using directives
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration.Service
{
    [TestFixture]
    public class ServicesSectionTests
    {
        #region Tests
        [Test]
        public void GetSectionTest()
        {
            // Action
            ServicesSection result = CoreCfg.Services;

            // Post-Conditions
            Assert.IsNotNull(result, "The result is wrong.");
            Assert.AreEqual("services", result.DefaultName, "The property 'DefaultName' is wrong.");
        }
        #endregion
    }
}