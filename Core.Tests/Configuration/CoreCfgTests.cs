﻿#region Using directives
using Allag.Core.Configuration.Reflection;
using Allag.Core.Configuration.Service;
using Allag.Core.Configuration.Workflow;
using NUnit.Framework;

#endregion

namespace Allag.Core.Configuration
{
    [TestFixture]
    public class CoreCfgTests
    {
        #region Tests
        [Test]
        public void SectionsTest()
        {
            // Action
            Assert.That(CoreCfg.General, Is.EqualTo(GetSection<GeneralSection>()), "The property 'General' is wrong.");
            Assert.That(CoreCfg.Objects, Is.EqualTo(GetSection<ObjectsSection>()), "The property 'Objects' is wrong.");
            Assert.That(CoreCfg.Services, Is.EqualTo(GetSection<ServicesSection>()), "The property 'Services' is wrong.");
            Assert.That(CoreCfg.Workflows, Is.EqualTo(GetSection<WorkflowsSection>()), "The property 'Workflows' is wrong.");

        }
        #endregion

        #region Properties
        private static TSection GetSection<TSection>() where TSection : Section, new()
        {
            return SectionGroup.Instance.GetRoot("allag.core").GetSection<TSection>();
        }
        #endregion
    }
}