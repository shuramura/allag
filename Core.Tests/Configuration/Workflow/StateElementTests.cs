﻿#region Using directives
using NUnit.Framework;
#endregion

namespace Allag.Core.Configuration.Workflow
{
    [TestFixture]
    public class StateElementTests
    {
        #region Variables
        private StateElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Initialize()
        {
            _instance = new StateElement();
        }
        #endregion

        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(0, _instance.Triggers.Count, "The property 'Triggers' is wrong.");
        }
    }
}