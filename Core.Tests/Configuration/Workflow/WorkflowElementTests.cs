﻿#region Using directives
using NUnit.Framework;
#endregion

namespace Allag.Core.Configuration.Workflow
{
    [TestFixture]
    public class WorkflowElementTests
    {
        #region Variables
        private WorkflowElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Initialize()
        {
            _instance = new WorkflowElement();
        }
        #endregion

        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(0, _instance.States.Count, "The property 'States' is wrong.");
            Assert.AreEqual(0, _instance.Actions.Count, "The property 'Actions' is wrong.");
            Assert.AreEqual(0, _instance.PreExecutedActions.Count, "The property 'PreExecutedActions' is wrong.");
            Assert.AreEqual(0, _instance.PostExecutedActions.Count, "The property 'PostExecutedActions' is wrong.");
        }

        [Test]
        public void CtorFormConfigTest()
        {
            // Pre-Conditions
            WorkflowElement instance = CoreCfg.Workflows.Items["Workflow"];

            // Post-Conditions
            Assert.AreEqual(3, instance.States.Count, "The property 'States' is wrong.");
            Assert.AreEqual(4, instance.Actions.Count, "The property 'Actions' is wrong.");
            Assert.AreEqual(1, instance.PreExecutedActions.Count, "The property 'PreExecutedActions' is wrong.");
            Assert.AreEqual(1, instance.PostExecutedActions.Count, "The property 'PostExecutedActions' is wrong.");
        }
    }
}