﻿#region Using directives
using NUnit.Framework;
#endregion

namespace Allag.Core.Configuration.Workflow
{
    [TestFixture]
    public class TriggerElementTests
    {
        #region Variables
        private TriggerElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Initialize()
        {
            _instance = new TriggerElement();
        }
        #endregion

        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(string.Empty, _instance.NewState, "The property 'NewState' is wrong.");
            Assert.AreEqual(string.Empty, _instance.Action, "The property 'Action' is wrong.");
            Assert.AreEqual(0, _instance.Actions.Count, "The property 'Actions' is wrong.");
        }
    }
}