﻿#region Using directives
using NUnit.Framework;
#endregion

namespace Allag.Core.Configuration.Workflow
{
    [TestFixture]
    public class WorkflowsSectionTests
    {
        #region Tests
        [Test]
        public void GetSectionTest()
        {
            // Action
            WorkflowsSection result = CoreCfg.Workflows;

            // Post-Conditions
            Assert.IsNotNull(result, "The instance is wrong.");
            Assert.AreEqual("workflows", result.DefaultName, "The property 'DefaultName' is wrong.");
            Assert.AreEqual(3, result.Items.Count, "The property 'Items' is wrong.");
        }
        #endregion
    }
}