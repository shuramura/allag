#region Using directives
using System;
using System.Configuration.Install;
using System.IO;
using Allag.Core.Configuration;
using Allag.Core.TestData.Service;
using NUnit.Framework;

#endregion

namespace Allag.Core.Service
{
    [TestFixture]
    public class ServiceFactoryTests
    {
        #region Initialization
        [TearDown]
        public void TearDown()
        {
            ServiceFactory.Stop(null);
        }
        #endregion

        #region Tests
        [Test]
        public void StartConsoleModeTest()
        {
            // Pre-Conditions
            int result = 0;
            TestService.OnStartAction += delegate { result++; };

            // Action
            ServiceFactory.Start(new string[0], true);

            // Post-Conditions
            Assert.AreEqual(3, result);
        }

        [Test]
        public void StartExactServiceTest()
        {
            // Pre-Conditions
            int result = 0;
            TestService.OnStartAction += delegate { result++; };

            // Action
            ServiceFactory.Start(new string[] { "TestService" }, true);

            // Post-Conditions
            Assert.AreEqual(1, result);
        }

        [Test]
        public void StopTest()
        {
            // Pre-Conditions
            ServiceFactory.Start(null, true);
            int result = 0;
            TestService.OnStopAction += delegate { result++; };

            // Action
            ServiceFactory.Stop(new string[0]);

            // Post-Conditions
            Assert.AreEqual(3, result);
        }

        [Test]
        public void StopExactServicesTest()
        {
            // Pre-Conditions
            ServiceFactory.Start(null, true);
            int result = 0;
            TestService.OnStopAction += delegate { result++; };

            // Action
            ServiceFactory.Stop(new string[] { "TestService", "TestService2" });

            // Post-Conditions
            Assert.AreEqual(2, result);
        }

        [Test]
        public void RestartExactServicesTest()
        {
            // Pre-Conditions
            ServiceFactory.Start(null, true);
            int nStopCount = 0;
            int nStartCount = 0;
            TestService.OnStopAction += delegate { nStopCount++; };
            TestService.OnStartAction += delegate { nStartCount++; };

            // Action
            ServiceFactory.Restart(new string[] { "TestService2" });

            // Post-Conditions
            Assert.AreEqual(1, nStopCount, "The stop count is wrong.");
            Assert.AreEqual(1, nStartCount, "The start count is wrong.");
        }

        [Test]
        public void InitializeTest()
        {
            // Post-Conditions
            Assert.AreEqual(ServiceCount, ServiceFactory.Count);
        }

        [Test]
        public void GetInstallersTest()
        {
            // Action
            Installer[] result = ServiceFactory.GetInstallers();

            // Post-Conditions
            Assert.AreEqual(ServiceCount + 1, result.Length);
        }

        [Test]
        public void RunApplicationZeroArgumentsTest()
        {
            // Action
            Assert.That(() => ServiceFactory.RunApplication(), Throws.ArgumentException);

        }

        [Test]
        public void RunApplicationUnknownArgumentTest()
        {
            // Action
            Assert.That(() => ServiceFactory.RunApplication("lalala"), Throws.InstanceOf<NotSupportedException>());
        }

        [Test]
        public void RunApplicationConsoleModeOneServiceTest()
        {
            // Pre-Conditions
            int nStopCount = 0;
            int nStartCount = 0;
            TestService.OnStopAction += delegate { nStopCount++; };
            TestService.OnStartAction += delegate { nStartCount++; };
            using (StringReader reader = new StringReader("lalala"))
            {
                Console.SetIn(reader);
                // Action
                ServiceFactory.RunApplication(ServiceFactory.ConsoleModeArg, "TestService", "TestService2");

                // Post-Conditions
                Assert.AreEqual(1, nStartCount, "The start count is wrong.");
                Assert.AreEqual(1, nStopCount, "The stop count is wrong.");
            }
        }

        [Test]
        public void RunApplicationConsoleModeAllServiceTest()
        {
            // Pre-Conditions
            int nStopCount = 0;
            int nStartCount = 0;
            TestService.OnStopAction += delegate { nStopCount++; };
            TestService.OnStartAction += delegate { nStartCount++; };
            using (StringReader reader = new StringReader("lalala"))
            {
                Console.SetIn(reader);

                // Action
                ServiceFactory.RunApplication(ServiceFactory.ConsoleModeArg, "", "TestService2");

                // Post-Conditions
                Assert.AreEqual(3, nStartCount, "The start count is wrong.");
                Assert.AreEqual(3, nStopCount, "The stop count is wrong.");
            }
        }
        #endregion

        #region Properties
        private static int ServiceCount
        {
            get { return CoreCfg.Services.Items.Count; }
        }
        #endregion
    }
}