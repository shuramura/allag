#region Using directives

using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Service
{
    [TestFixture]
    public class ServiceInfoTests
    {
        #region Variables
        private ServiceControlManager _serviceControlManager;
        private ServiceManager _serviceManager;
        private ServiceInfo _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _serviceControlManager = new ServiceControlManager(SCAccessType.All);
            _serviceManager = _serviceControlManager[ServiceManagerTests.ServiceName, ServiceAccessType.All];
            _instance = _serviceManager.Info;
        }

        [TearDown]
        public void TearDown()
        {
            if (_serviceControlManager != null)
            {
                _serviceControlManager.Dispose();
            }

            if (_serviceManager != null)
            {
                _serviceManager.Dispose();
            }
        }

        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
			// Post-Conditions
            Assert.AreEqual("NT AUTHORITY\\LocalService", _instance.AccountName, "The property 'AccountName' is wrong.");
            Assert.AreEqual("Windows Time", _instance.DisplayName, "The property 'DisplayName' is wrong.");
            Assert.That(_instance.File, Is.SamePath(@"C:\Windows\system32\svchost.exe"), "The property 'File' is wrong.");
            Assert.AreEqual("-k LocalService", _instance.Arguments, "The property 'Arguments' is wrong.");
            Assert.That(_instance.BinaryPath, Is.EqualTo("C:\\Windows\\system32\\svchost.exe -k LocalService").IgnoreCase, "The property 'BinaryPath' is wrong.");
        }

        [Test]
        public void CtorPathWithoutParametersTest()
        {
            // Pre-Conditions
            using (ServiceManager serviceManager = _serviceControlManager["W32Time", ServiceAccessType.All])
            {
                ServiceInfo instance = serviceManager.Info;

                // Post-Conditions
                Assert.AreEqual("NT AUTHORITY\\LocalService", instance.AccountName, "The property 'AccountName' is wrong.");
                Assert.AreEqual("Windows Time", instance.DisplayName, "The property 'DisplayName' is wrong.");
                Assert.That(instance.File, Is.SamePath("C:\\WINDOWS\\system32\\svchost.exe"), "The property 'File' is wrong.");
                Assert.AreEqual("-k LocalService", instance.Arguments, "The property 'Arguments' is wrong.");
                Assert.That(instance.BinaryPath, Is.EqualTo("C:\\WINDOWS\\system32\\svchost.exe -k LocalService").IgnoreCase, "The property 'BinaryPath' is wrong.");
            }
        }

        [Test]
        public void DisplayNameSetTest()
        {
            // Pre-Conditions 
            const string expected = "lalala";

            // Action
            _instance.DisplayName = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.DisplayName);
        }

        [Test]
        public void DisplayNameSetNullTest()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => _instance.DisplayName = null);
        }

        [Test]
        public void ArgumentsSetTest()
        {
            // Pre-Conditions 
            const string expected = "lalala";

            // Action
            _instance.Arguments = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Arguments);
        }

        [Test]
        public void ArgumentsSetNullTest()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => _instance.Arguments = null);
        }

        [Test]
        public void FileSetTest()
        {
            // Pre-Conditions 
            const string expected = @"c:\lalala";

            // Action
            _instance.File = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.File);
        }

        [Test]
        public void FileSetNullTest()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => _instance.File = null);
        }

        [Test]
        public void FileSetEmptyTest()
        {
            // Action
            Assert.Throws<ArgumentException>(() => _instance.File = string.Empty);
        }

        

        #endregion
    }
}
