#region Using directives

using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Service
{
    [TestFixture]
    public class ServiceManagerTests
    {
        #region Constants
        public const string ServiceName = "W32Time";
        #endregion

        #region Variables
        private ServiceControlManager _serviceControlManager;
        private ServiceManager _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _serviceControlManager = new ServiceControlManager(SCAccessType.All);
            _instance = _serviceControlManager[ServiceName, ServiceAccessType.All];
        }

        [TearDown]
        public void TearDown()
        {
            if (_serviceControlManager != null)
            {
                _serviceControlManager.Dispose();
            }

            if (_instance != null)
            {
                _instance.Dispose();
            }
        }

        #endregion

        #region Tests
        [Test]
        public void CtorNullServiceNameTest()
        {
            // Action
            Assert.Throws<ArgumentException>(() => new ServiceManager(null, new IntPtr(100), ServiceAccessType.All));
        }

        [Test]
        public void CtorEmptyServiceNameTest()
        {
            // Action
            Assert.Throws<ArgumentException>(() => new ServiceManager(string.Empty, new IntPtr(100), ServiceAccessType.All));
        }

        [Test]
        public void CtorZeroDatabaseHandleTest()
        {
            // Action
            Assert.Throws<ArgumentException>(() => new ServiceManager(ServiceName, IntPtr.Zero, ServiceAccessType.All));
        }

        [Test]
        public void CtorInvalidDatabaseHandleTest()
        {
            // Action
            Assert.Throws<InvalidOperationException>(() => new ServiceManager(ServiceName, new IntPtr(100), ServiceAccessType.All));
        }

        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(ServiceName, _instance.ServiceName, "The property 'ServiceName' is wrong.");
        }

        [Test]
        public void DtorTest()
        {
            // Action
            _instance = null;
            GC.WaitForPendingFinalizers();
        }

        [Test]
        public void InvoikeDisposedTest()
        {
            // Pre-Conditions 
            _instance.Dispose();

            // Action
            string result;
            Assert.Throws<ObjectDisposedException>(() => result = _instance.ServiceName);
        }

        [Test]
        public void InfoGetTest()
        {
            // Action
            ServiceInfo result = _instance.Info;

            // Post-Conditions
            Assert.IsNotNull(result);
        }

        [Test]
        public void InfoSetTest()
        {
            // Pre-Conditions 
            ServiceInfo expected = _instance.Info;
            string strDisplayName = expected.DisplayName;
            expected.DisplayName = "lalala";

            // Action
            _instance.Info = expected;

            // Post-Conditions
            ServiceInfo serviceInfo = _instance.Info;
            Assert.AreEqual(expected.DisplayName, serviceInfo.DisplayName);
            serviceInfo.DisplayName = strDisplayName;
            _instance.Info = serviceInfo;
        }
        #endregion
    }
}
