#region Using directives

using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Service
{
    [TestFixture]
    public class ServiceControlManagerTests
    {
        #region Variables
        private ServiceControlManager _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new ServiceControlManager(SCAccessType.All);
        }

        [TearDown]
        public void TearDown()
        {
            if (_instance != null)
            {
                _instance.Dispose();
            }
        }

        #endregion

        #region Tests
        [Test]
        public void CtorWrongMachineTest()
        {
            // Action 
            Assert.Throws<InvalidOperationException>(() => new ServiceControlManager("lalala", SCAccessType.All));
        }

        [Test]
        public void CtorTest()
        {
			// Post-Conditions
            Assert.AreEqual("Local computer", _instance.MachineName, "The property 'MachineName' is wrong.");
        }


        [Test]
        public void DtorTest()
        {
            // Action
            _instance = null;
            GC.WaitForPendingFinalizers();
        }

        [Test]
        public void ThisGetTest()
        {
            // Action
            using (ServiceManager result = _instance[ServiceManagerTests.ServiceName, ServiceAccessType.All])
            {
                // Post-Conditions
                Assert.IsNotNull(result);
            }
        }

        [Test]
        public void InvoikeDisposedTest()
        {
            // Pre-Conditions 
            _instance.Dispose();

            // Action
            string result;
            Assert.Throws<ObjectDisposedException>(() => result = _instance.MachineName);
        }
        #endregion
    }
}
