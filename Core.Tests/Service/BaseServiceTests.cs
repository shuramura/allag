#region Using directives

using System.Reflection;
using NUnit.Framework;
using Rhino.Mocks;
using Rhino.Mocks.Interfaces;

#endregion

namespace Allag.Core.Service
{
    [TestFixture]
    public class BaseServiceTests
    {
        #region Variables
        private BaseService _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new BaseService();
        }

        #endregion

        #region Tests

        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.IsFalse(_instance.IsStarted, "The property 'IsStarted' is wrong.");
            Assert.IsTrue(_instance.IsConsoleMode, "The property 'IsConsoleMode' is wrong.");
        }

        [Test]
        public void StartActionTest()
        {
            // Action
            _instance.StartAction();

            // Post-Conditions
            Assert.IsTrue(_instance.IsStarted, "The property 'IsStarted' is wrong.");
            Assert.IsTrue(_instance.IsConsoleMode, "The property 'IsConsoleMode' is wrong.");
        }

        [Test]
        public void StopActionTest()
        {
            // Pre-Conditions
            _instance.StartAction();

            // Action
            _instance.StopAction();

            // Post-Conditions
            Assert.IsFalse(_instance.IsStarted, "The property 'IsStarted' is wrong.");
            Assert.IsTrue(_instance.IsConsoleMode, "The property 'IsConsoleMode' is wrong.");
        }

        [Test]
        public void OnStartTest()
        {
            // Pre-Conditions
            string[] expected = new string[] {"1", "2", "3"};
            MockRepository mocks = new MockRepository();
            BaseService instance = mocks.PartialMock<BaseService>();
            Expect.Call(() => instance.StartAction(expected)).CallOriginalMethod(OriginalCallOptions.NoExpectation);
            
            mocks.ReplayAll();


            MethodInfo methodInfo = typeof(BaseService).GetMethod("OnStart", BindingFlags.Instance | BindingFlags.NonPublic);

            // Action
            methodInfo.Invoke(instance, new object[] { expected });

            // Post-Conditions
            mocks.VerifyAll();
            Assert.IsTrue(instance.IsStarted, "The property 'IsStarted' is wrong.");
            Assert.IsFalse(instance.IsConsoleMode, "The property 'IsConsoleMode' is wrong.");
        }

        [Test]
        public void OnStopTest()
        {
            // Pre-Conditions
            MockRepository mocks = new MockRepository();
            BaseService instance = mocks.PartialMock<BaseService>();
            instance.StopAction();
            mocks.ReplayAll();


            MethodInfo methodInfo = typeof(BaseService).GetMethod("OnStop", BindingFlags.Instance | BindingFlags.NonPublic);

            // Action
            methodInfo.Invoke(instance, null);

            // Post-Conditions
            mocks.VerifyAll();
            Assert.IsFalse(instance.IsStarted, "The property 'IsStarted' is wrong.");
            Assert.IsTrue(instance.IsConsoleMode, "The property 'IsConsoleMode' is wrong.");
        }

        #endregion
    }
}
