﻿#region Using directives
using System;
using System.Threading;
using Allag.Core.TestData.Tools.Job;
using NUnit.Framework;

#endregion

namespace Allag.Core.Service
{
    [TestFixture]
    public class JobHostServiceTests
    {
        #region Variables
        private JobHostService _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new JobHostService();
        }

        [TearDown]
        public void TearDown()
        {
            _instance.StopAction();
            TestJob.Executing = null;
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(TimeSpan.Zero, _instance.StopTimeout);
        }

        [Test]
        public void StartActionTest()
        {
            // Pre-Conditions
            bool isExecuted = false;
            TestJob.Executing = delegate { isExecuted = true; };

            // Action
            _instance.StartAction();

            // Post-Conditions
            Thread.Sleep(1000);
            Assert.IsTrue(isExecuted);

        }

        [Test]
        public void StopAction_Test()
        {
            // Pre-Conditions
            int nEntered = 0;
            int nFinished = 0;
            TestJob.Executing = delegate
            {
                nEntered++;
                Thread.Sleep(2000);
                nFinished++;
            };
            _instance.StartAction();
            Thread.Sleep(1000);

            // Action
            _instance.StopAction();

            // Post-Conditions
            Assert.AreEqual(1, nEntered, "The entered is wrong.");
            Assert.AreEqual(0, nFinished, "The task is finished wrongly.");
            Thread.Sleep(1500);
            Assert.AreEqual(1, nFinished, "The task is not finished.");
        }

        [Test]
        public void StopAction_InfinityJob_Test()
        {
            // Pre-Conditions
            int nEntered = 0;
            int nFinished = 0;
            _instance.StopTimeout = new TimeSpan(0, 0, 2);
            TestJob.Executing = delegate
            {
                nEntered++;
                Thread.Sleep(new TimeSpan(1, 0, 0));
                nFinished++;
            };
            _instance.StartAction();
            Thread.Sleep(500);

            // Action
            _instance.StopAction();

            // Post-Conditions
            Assert.AreEqual(1, nEntered, "The entered is wrong.");
            Assert.AreEqual(0, nFinished, "The finished is wrong.");
        }
        #endregion

    }
}