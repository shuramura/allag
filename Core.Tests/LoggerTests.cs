﻿using System;
using NUnit.Framework;
using Rhino.Mocks;

namespace Allag.Core
{
    [TestFixture]
    public class LoggerTests
    {
        #region Variables
        private Logger _instance;
        private MockRepository _mocks;
        private ILogger _logger;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _mocks = new MockRepository();
            _logger = _mocks.StrictMock<ILogger>();
            _instance = new Logger(_logger,
                x =>
                {
                    if (CloseAction != null)
                    {
                        CloseAction(x);
                    }
                });

        }
        #endregion

        #region Tests
        [Test]
        public void Dispose_Test()
        {
            // Pre-Conditions
            int nCount = 0;
            CloseAction = x =>
            {
                nCount++;
                Assert.That(x, Is.EqualTo(_instance), "The logger instance wrong.");
            };
            _logger.Expect(x => x.Dispose()).Repeat.Once();
            _mocks.ReplayAll();

            // Action
            _instance.Dispose();
            _instance.Dispose();

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.That(nCount, Is.EqualTo(1), "The visits count is wrong.");
        }

        [Test]
        public void Debug_Test()
        {
            // Pre-Conditions
            string format = "format";
            object[] args = new object[] { this };
            _logger.Expect(x => x.Debug(format, args)).Repeat.Once();
            _mocks.ReplayAll();

            // Action
            _instance.Debug(format, args);

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void Error_Test()
        {
            // Pre-Conditions
            string format = "format";
            object[] args = new object[] { this };
            Exception exception = new Exception();
            _logger.Expect(x => x.Error(format, exception, args)).Repeat.Once();
            _mocks.ReplayAll();

            // Action
            _instance.Error(format, exception, args);

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void FatalError_Test()
        {
            // Pre-Conditions
            Exception exception = new Exception();
            _logger.Expect(x => x.FatalError(exception)).Repeat.Once();
            _mocks.ReplayAll();

            // Action
            _instance.FatalError(exception);

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void FatalError_WithMessage_Test()
        {
            // Pre-Conditions
            string format = "format";
            object[] args = new object[] { this };
            Exception exception = new Exception();
            _logger.Expect(x => x.FatalError(format, exception, args)).Repeat.Once();
            _mocks.ReplayAll();

            // Action
            _instance.FatalError(format, exception, args);

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void GetLogger_Test()
        {
            // Action
            Assert.That(() => _instance.GetLogger(GetType()), Throws.InstanceOf<NotSupportedException>());
        }

        [Test]
        public void Info_Test()
        {
            // Pre-Conditions
            string format = "format";
            object[] args = new object[] { this };
            _logger.Expect(x => x.Info(format, args)).Repeat.Once();
            _mocks.ReplayAll();

            // Action
            _instance.Info(format, args);

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void Warning_Test()
        {
            // Pre-Conditions
            string format = "format";
            object[] args = new object[] { this };
            _logger.Expect(x => x.Warning(format, args)).Repeat.Once();
            _mocks.ReplayAll();

            // Action
            _instance.Warning(format, args);

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void Warning_WithException_Test()
        {
            // Pre-Conditions
            string format = "format";
            object[] args = new object[] { this };
            Exception exception = new Exception();
            _logger.Expect(x => x.Warning(format, exception, args)).Repeat.Once();
            _mocks.ReplayAll();

            // Action
            _instance.Warning(format, exception, args);

            // Post-Conditions
            _mocks.VerifyAll();
        }
        #endregion

        #region Properties
        private Action<ILogger> CloseAction { get; set; }
        #endregion
    }
}
