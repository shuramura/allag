﻿#region Using directives
using System.Linq;
using System.Reflection;
using Allag.Core.Configuration;
using Allag.Core.TestData;
using NUnit.Framework;

#endregion

namespace Allag.Core
{
    [TestFixture]
    public class ApplicationTests
    {
        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(typeof(SetUpTests), Application.Root, "The property 'Root' is wrong.");
            Assert.AreEqual(CoreCfg.General.ApplicationBase, Application.Base, "The property 'Base' is wrong.");
            Assert.That(Application.ProductVersion, Is.Not.Null.And.Not.Empty, "The property 'ProductVersion' is wrong.");
        }
        #endregion
    }
}