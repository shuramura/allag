#region Using directives
using System;
using Allag.Core.Service;

#endregion

namespace Allag.Core.TestData.Service
{
    public class TestService : BaseService
    {
        #region Events
        public static EventHandler OnStartAction;
        public static EventHandler OnStopAction;
        #endregion

        #region Override methods
        public override void StartAction(params string[] args)
        {
            base.StartAction(args);
            if(OnStartAction != null)
            {
                OnStartAction(this, EventArgs.Empty);
            }
        }

        public override void StopAction()
        {
            if(OnStopAction != null)
            {
                OnStopAction(this, EventArgs.Empty);
            }
            base.StopAction();
        }
        #endregion
    }
}