#region Using directives

using System.Data;
using Allag.Core.Data.Attributes;

#endregion

namespace Allag.Core.TestData.Data.Text
{
    public class Test
    {
        #region Variables
        [Field("", Order = 4, Size = 4)]
        [Convert(typeof(string))]
        public string Column4;
        [Field("C\"olum,n4", Order = 3)]
        [Convert(typeof(string))]
        public int? Column3;
        [Field("Column2,", Order = 2)]
        [Convert(typeof(string))]
        public int Column2;
        [Field("\"Column1", Direction = ParameterDirection.Input, Order = 1, IsNullable = false)]
        [Convert(typeof(string))]
        public int Column1;
        [Field("Column0", Order = 0, Size = 10)]
        [Convert(typeof(string))]
        public int? Column0;
        [Field("OutputColumn", Direction = ParameterDirection.Output, Order = 1)]
        public string OutputColumn;
        #endregion

        #region Override methods
        public override bool Equals(object obj)
        {
            return Equals(obj as Test);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = (Column0.HasValue ? Column0.Value : 0);
                result = (result * 397) ^ Column1;
                result = (result * 397) ^ Column2;
                result = (result * 397) ^ (Column3.HasValue ? Column3.Value : 0);
                result = (result * 397) ^ (Column4 != null ? Column4.GetHashCode() : 0);
                return result;
            }
        }
        #endregion

        #region Methods
        public bool Equals(Test obj)
        {
            return obj != null &&  obj.Column0.Equals(Column0) && obj.Column1 == Column1 && obj.Column2 == Column2 && obj.Column3.Equals(Column3) && obj.Column4.Equals(Column4);
        }
        #endregion
    }
}
