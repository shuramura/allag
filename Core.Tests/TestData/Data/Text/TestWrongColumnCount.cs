#region Using directives

using System.Data;
using Allag.Core.Data.Attributes;

#endregion

namespace Allag.Core.TestData.Data.Text
{
    public class TestWrongColumnCount
    {
        #region Variables
        [Field("Column0")]
        [Convert(typeof(string))]
        public int? Column0;
        [Field("\"Column1")]
        [Convert(typeof(string))]
        public int? Column1;
        [Field("Column2,")]
        [Convert(typeof(string))]
        public int? Column2;
        [Field("C\"olum,n4")]
        [Convert(typeof(string))]
        public int? Column3;
        [Field("", Direction = ParameterDirection.Output)]
        [Convert(typeof(string))]
        public int? Column4;
        #endregion
    }
}
