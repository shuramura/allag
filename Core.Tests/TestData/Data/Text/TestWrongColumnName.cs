#region Using directives
using System.Data;
using Allag.Core.Data.Attributes;

#endregion

namespace Allag.Core.TestData.Data.Text
{
    public class TestWrongColumnName
    {
        #region Variables
        [Field("Column0", Direction = ParameterDirection.InputOutput)]
        [Convert(typeof(string))]
        public int? Column0;

        [Field("\"Column1", Direction = ParameterDirection.InputOutput)]
        [Convert(typeof(string))]
        public int? Column1;

        [Field("Column2,", Direction = ParameterDirection.InputOutput)]
        [Convert(typeof(string))]
        public int? Column2;

        [Field("C\"olum,n4", Direction = ParameterDirection.InputOutput)]
        [Convert(typeof(string))]
        public int? Column3;

        [Field("lalala", IsNullable = false, Direction = ParameterDirection.InputOutput)]
        [Convert(typeof(string))]
        public int? Column4;
        #endregion
    }
}