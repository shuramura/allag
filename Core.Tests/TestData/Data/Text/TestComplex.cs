#region Using directives
using System;
using System.Collections;
using System.Collections.Generic;
using Allag.Core.Data.Attributes;
using Allag.Core.Data.Text;

#endregion

namespace Allag.Core.TestData.Data.Text
{
    public class TestComplex : IComplexParseProvider<TestComplex>
    {
        #region Variables
        public List<Test> Tests0;
        public List<Test> Tests1;
        public List<Test> Tests2;

        [Field(Order = 0, IsNullable = false)]
        [Convert(typeof(string))]
        public int Id;

        [Field(Order = 1, IsNullable = false)]
        public string Test;
        #endregion

        #region Constructors
        public TestComplex()
        {
            Id = 0;
            Clear();
        }
        #endregion

        #region Implementation of IComplexParseProvider<TestComplex>
        void IComplexParseProvider<TestComplex>.StartParsing()
        {
            Clear();
        }

        object IComplexParseProvider<TestComplex>.GetObjectForParsing(IDictionary<string, string> row)
        {
            Test test = new Test();
            object ret;
            switch(row["0"])
            {
                case "0":
                    ret = this;
                    break;
                case "1":
                    if(Tests0.Count > 0)
                    {
                        throw new IndexOutOfRangeException();
                    }
                    Tests0.Add(test);
                    ret = test;
                    break;
                case "2":
                    Tests1.Add(test);
                    ret = test;
                    break;
                case "3":
                    Tests2.Add(test);
                    ret = test;
                    break;
                default:
                    ret = null;
                    break;
            }

            return ret;
        }

        IList<TestComplex> IComplexParseProvider<TestComplex>.EndParsing()
        {
            if(Tests2.Count < 3)
            {
                throw new IndexOutOfRangeException();
            }

            return new List<TestComplex> {this};
        }

        public IEnumerable ObjectToSave
        {
            get
            {
                yield return this;

                foreach(Test test in Tests0)
                {
                    yield return test;
                }

                foreach(Test test in Tests1)
                {
                    yield return test;
                }

                foreach(Test test in Tests2)
                {
                    yield return test;
                }
            }
        }
        #endregion

        #region Override methods
        public override bool Equals(object obj)
        {
            return Equals(obj as TestComplex);
        }
        #endregion

        #region Methods
        private void Clear()
        {
            Tests0 = new List<Test>();
            Tests1 = new List<Test>();
            Tests2 = new List<Test>();
            Test = null;
        }

        public bool Equals(TestComplex obj)
        {
            bool ret = obj != null && obj.Id == Id && Equals(obj.Test, Test);
            for(int i = 0; ret && i < Tests0.Count; i++)
            {
                ret = Tests0[i].Equals(obj.Tests0[i]);
            }
            for(int i = 0; ret && i < Tests1.Count; i++)
            {
                ret = Tests1[i].Equals(obj.Tests1[i]);
            }
            for(int i = 0; ret && i < Tests2.Count; i++)
            {
                ret = Tests2[i].Equals(obj.Tests2[i]);
            }
            return ret;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = (Tests0 != null ? Tests0.GetHashCode() : 0);
                result = (result * 397) ^ (Tests1 != null ? Tests1.GetHashCode() : 0);
                result = (result * 397) ^ (Tests2 != null ? Tests2.GetHashCode() : 0);
                result = (result * 397) ^ Id;
                result = (result * 397) ^ (Test != null ? Test.GetHashCode() : 0);
                return result;
            }
        }
        #endregion
    }
}