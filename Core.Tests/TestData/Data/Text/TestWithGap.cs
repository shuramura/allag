#region Using directives

using System.Data;
using Allag.Core.Data.Attributes;

#endregion

namespace Allag.Core.TestData.Data.Text
{
    public class TestWithGap
    {
        #region Variables
        [Field(Direction = ParameterDirection.InputOutput, Order = 3, IsNullable = false)]
        [Convert(typeof(string))]
        public int Column2;
        [Field(Direction = ParameterDirection.InputOutput, Order = 1, IsNullable = false)]
        [Convert(typeof(string))]
        public int Column1;
        [Field(Direction = ParameterDirection.InputOutput, Order = 0, Size = 10, IsNullable = false)]
        [Convert(typeof(string))]
        public int? Column0;
        #endregion

        #region Override methods
        public override bool Equals(object obj)
        {
            return Equals(obj as TestWithGap);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = (Column0.HasValue ? Column0.Value : 0);
                result = (result * 397) ^ Column1;
                result = (result * 397) ^ Column2;
                return result;
            }
        }
        #endregion

        #region Methods
        public bool Equals(TestWithGap obj)
        {
            return obj != null &&  obj.Column0.Equals(Column0) && obj.Column1 == Column1 && obj.Column2 == Column2;
        }
        #endregion
    }
}
