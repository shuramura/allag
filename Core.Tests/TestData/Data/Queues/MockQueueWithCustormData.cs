﻿#region Using directives
using System;
using System.Threading;
using System.Threading.Tasks;
using Allag.Core.Data.Queues;

#endregion

namespace Allag.Core.TestData.Data.Queues
{
    public class MockQueueWithCustormData<TStorage, TMessage, TData> : Queue<TStorage, TMessage, TData>
        where TStorage : class
        where TMessage : class
        where TData : class
    {
        #region Constructors
        public MockQueueWithCustormData(Guid id) : base(id, null) {}
        #endregion

        #region Implementation of Queue<TStorage, TMessage, TData>
        protected override StorageInfo CreateStorageInitializer(dynamic args)
        {
            return new StorageInfo(() => null);
        }

        protected override Task<bool> IsActiveAsync(TStorage storage, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        protected override Task PushAsync(TStorage storage, TData entity, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        protected override Task<TData> PopAsync(TStorage storage, CancellationToken token)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}