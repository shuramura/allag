﻿#region Using directives
using System;
using System.Threading;
using System.Threading.Tasks;
using Allag.Core.Data.Queues;

#endregion

namespace Allag.Core.TestData.Data.Queues
{
    public class MockQueue<TRepository, TMessage> : Queue<TRepository, TMessage>
        where TRepository : class
        where TMessage : class
    {
        #region Variables
        public Func<TRepository, bool> IsActiveHandler;
        public Action<TRepository, byte[]> PushHandler;
        public Func<TRepository, byte[]> PopHandler;
        #endregion

        #region Constructors
        public MockQueue(Guid id, TRepository repository) : base(id, new {Repository = repository})
        {
            ReleaseCount = 0;
        }
        #endregion

        #region Implementation of Queue<TStorage, TMessage>
        protected override StorageInfo CreateStorageInitializer(dynamic args)
        {
            return new StorageInfo(() => args.Repository, storage => { ReleaseCount++; });
        }

        protected override Task<bool> IsActiveAsync(TRepository storage, CancellationToken token)
        {
            bool ret = true;
            if (IsActiveHandler != null)
            {
                ret = IsActiveHandler(storage);
            }
            return Task.FromResult(ret);
        }

        protected override Task PushAsync(TRepository storage, byte[] entity, CancellationToken token)
        {
            if (PushHandler != null)
            {
                PushHandler(storage, entity);
            }
            return Task.FromResult(0);
        }

        protected override Task<byte[]> PopAsync(TRepository storage, CancellationToken token)
        {
            byte[] ret = null;
            if (PopHandler != null)
            {
                ret = PopHandler(storage);
            }
            return Task.FromResult(ret);
        }
        #endregion

        #region Properties
        public int ReleaseCount { get; private set; }
        #endregion

    }
}