#region Using directives
using System.Collections.Generic;
using System.IO;
using Allag.Core.Data.Persistence;

#endregion

namespace Allag.Core.TestData.Data.Persistence
{
    public class MockEntity : Entity<IMockDaoFactory, MockEntity, int> 
    {
        #region Properties
        [BusinessKey]
        public virtual int Number { get; set; }
        [BusinessKey]
        public virtual int? NullableNumber { get; set; }
        public virtual string String { get; set; }
        public virtual FileAttributes Attr { get; set; }

        public virtual MockEntityParent Parent { get; set; }

        public virtual IEnumerable<MockEntity> Children { get; set; }

        public virtual bool Boolean { get; set; }
        #endregion

        #region Methods
        public virtual MockEntity SetId(int id)
        {
            Id = id;
            return this;
        }
        #endregion
    }
}
