#region Using directives

using Allag.Core.Data.Persistence;

#endregion

namespace Allag.Core.TestData.Data.Persistence
{
    public class MockEntityParent : Entity<IMockDaoFactory, MockEntityParent, int> 
    {
        #region Properties
        public virtual MockEntity Entity { get; set; }
        #endregion
    }
}
