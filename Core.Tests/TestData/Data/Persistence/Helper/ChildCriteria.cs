﻿namespace Allag.Core.TestData.Data.Persistence.Helper
{
    public class ChildCriteria
    {
        #region Properties
        public int ChildId { get; set; }
        #endregion
    }
}