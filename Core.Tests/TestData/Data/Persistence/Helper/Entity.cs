﻿using System.Collections.Generic;

namespace Allag.Core.TestData.Data.Persistence.Helper
{
    public class Entity
    {
        #region Properties
        public int Id { get; protected set; }
        public IEnumerable<ChildEntity> Children { get; protected set; }
        #endregion
    }
}