﻿#region Using directives
using System.Collections.Generic;

#endregion

namespace Allag.Core.TestData.Data.Persistence.Helper
{
    public class Criteria
    {
        #region Properties
        public int Id { get; set; }
        public IDictionary<string, ChildCriteria> Items { get; set; }
        #endregion
    }
}