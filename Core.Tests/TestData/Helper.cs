#region Using directives

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using NUnit.Framework;

#endregion

namespace Allag.Core.TestData
{
    public class Helper
    {
        #region Methods
        public static void Validate<TObject>(TObject expected, TObject result, string message)
        {
            foreach (PropertyInfo propertyInfo in typeof(TObject).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                if (propertyInfo.PropertyType == typeof(DateTime))
                {
                    DateTime dtExpected = (DateTime)propertyInfo.GetValue(expected, null);
                    DateTime dtResult = (DateTime)propertyInfo.GetValue(result, null);
                    Validate(dtExpected, dtResult, " of '" + propertyInfo.Name + "'" + message);
                }
                else if (propertyInfo.PropertyType.IsArray)
                {
                    if (propertyInfo.PropertyType.GetElementType().IsValueType)
                    {
                        Dictionary<object, int> _htExpected = new Dictionary<object, int>();
                        foreach (object obj in (IEnumerable)propertyInfo.GetValue(expected, null))
                        {
                            int occurences = _htExpected.ContainsKey(obj) ? _htExpected[obj] : 0;
                            _htExpected[obj] = ++occurences;
                        }

                        Dictionary<object, int> _htActual = new Dictionary<object, int>();
                        foreach (object obj in (IEnumerable)propertyInfo.GetValue(expected, null))
                        {
                            int occurences = _htActual.ContainsKey(obj) ? _htActual[obj] : 0;
                            _htActual[obj] = ++occurences;
                        }

                        foreach (object expectedObj in _htExpected.Keys)
                        {
                            int expectOcc = _htExpected[expectedObj],
                                actualOcc = _htActual.ContainsKey(expectedObj) ? _htActual[expectedObj] : 0;
                            Assert.AreEqual(expectOcc, actualOcc, String.Format(
                                "The property '{0}' {1} is wrong. Object '{2}' is present '{3}' times instead of expected '{4}'.",
                                    propertyInfo.Name,
                                    message,
                                    expectedObj,
                                    actualOcc,
                                    expectOcc));
                        }

                        foreach (object actualObj in _htActual.Keys)
                        {
                            Assert.IsTrue(_htExpected.ContainsKey(actualObj), String.Format(
                                "The property '{0}' {1} is wrong. Object '{2}' is not expected.",
                                    propertyInfo.Name,
                                    message,
                                    actualObj));
                        }
                    }
                    else
                    {
                        throw new NotSupportedException("Arrays of complex objects are not supported.");
                    }
                }
                else if (propertyInfo.PropertyType.IsClass)
                {
                    Validate(propertyInfo.GetValue(expected, null), propertyInfo.GetValue(result, null), message);
                }
                else
                {

                    Assert.AreEqual(propertyInfo.GetValue(expected, null), propertyInfo.GetValue(result, null),
                                    "The property '" + propertyInfo.Name + "'" + message + " is wrong.");
                }
            }
        }

        public static void Validate(DateTime expected, DateTime result, string message)
        {
            Assert.AreEqual(expected.Date, result.Date, "The property 'Date'" + message + " is wrong. The date is " + result + ".");
            Assert.AreEqual(expected.Hour, result.Hour, "The property 'Hour'" + message + " is wrong. The date is " + result + ".");
            Assert.AreEqual(expected.Minute, result.Minute, "The property 'Minute'" + message + " is wrong. The date is " + result + ".");
            Assert.AreEqual(expected.Second, result.Second, "The property 'Second'" + message + " is wrong. The date is " + result + ".");
        }

        #endregion
    }
}
