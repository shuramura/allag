#region Using directives
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Xml.Serialization;

#endregion

namespace Allag.Core.TestData.Reflection
{
    [XmlRoot("TestClass", Namespace = "http://tempuri.org/TestClass.xsd")]
    public class TestClass : ITest, ISupportInitialize
    {
        #region Variables
        [XmlIgnore]
        public static bool IsThrowExceptionWithinInitialization;

        private readonly DateTime _dateTime;

        [XmlElement("BaseString")]
        public string BaseString;

        [XmlElement("String")]
        public string String;

        [XmlElement("Int")]
        public int Int;

        [XmlElement("Child")]
        public Child[] ChildInfo;

        [XmlIgnore]
        public readonly TestClass ReadOnlyField;
        #endregion

        #region Events
        public static event EventHandler Initializing;
        public static event EventHandler BeginInitialize;
        public static event EventHandler EndInitialize;
        #endregion

        #region Constructors
        static TestClass()
        {
            IsThrowExceptionWithinInitialization = false;
        }

        public TestClass()
        {
        }

        public TestClass(int @int, string @string = null)
        {
            Int = @int;
            String = @string;
        }

        public TestClass(int @int, FileMode @enum, DateTime dateTime, string @string) : this(@int, @string)
        {
            Enum = @enum;
            _dateTime = dateTime;

            ReadOnlyProperty = new TestClass {Enum = FileMode.OpenOrCreate};

            ReadOnlyField = new TestClass {Enum = FileMode.Open};

            Dictionary = new Dictionary<string, TimeSpan>();
        }

        public TestClass(int @int, FileMode @enum, DateTime dateTime, string @string, TestClass readOnlyProperty)
            : this(@int, @enum, dateTime, @string)
        {
            ReadOnlyProperty = readOnlyProperty;
        }
        #endregion

        #region Implementation of IInitializer
        public void Initialize(Type entryType)
        {
            if (IsThrowExceptionWithinInitialization)
            {
                throw new InvalidOperationException();
            }
            if (Initializing != null)
            {
                Initializing(this, EventArgs.Empty);
            }
        }
        #endregion

        #region Implementation of ITest
        void ITest.TestMethod()
        {
        }
        #endregion

        #region Implementation of ISupportInitialize
        public void BeginInit()
        {
            if (BeginInitialize != null)
            {
                BeginInitialize(this, EventArgs.Empty);
            }
        }

        public void EndInit()
        {
            if (EndInitialize != null)
            {
                EndInitialize(this, EventArgs.Empty);
            }
        }
        #endregion

        #region Properties
        [XmlIgnore]
        public FileMode Enum { get; private set; }

        [XmlIgnore]
        public DateTime DateTime
        {
            get { return _dateTime; }
        }

        private Guid _guid;
        [XmlIgnore]
        public Guid Guid { get { return _guid; } set { _guid = value; } }

        [XmlIgnore]
        public int TestProperty { get; set; }

        [XmlIgnore]
        public TestClass ReadOnlyProperty { get; private set; }

        [XmlIgnore]
        public Dictionary<string, TimeSpan> Dictionary { get; private set; }

        [XmlIgnore]
        public int? NullableInt { get; set; }

        [XmlIgnore]
        public Type GenericType { get; set; }
        #endregion

        #region Methods
        public void WithDefaultParameters(int testProperty, FileMode @enum = FileMode.OpenOrCreate)
        {
            TestProperty = testProperty;
            Enum = @enum;
        }

        public void GenericMethod<TType>()
        {
            GenericType = typeof(TType);
        }
        #endregion

        #region Classes
        public class Child
        {
            #region Variables
            [XmlElement("String")]
            public string String;
            #endregion
        }
        #endregion
    }
}