﻿#region Using directives
using System.ComponentModel;

#endregion

namespace Allag.Core.TestData.Reflection
{
    [TypeConverter(typeof(ConvertableTestConverter))]
    public interface IConvertableTest
    {
    }
}