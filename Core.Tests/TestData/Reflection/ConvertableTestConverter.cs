﻿#region Using directives
using System.ComponentModel;
using System.Globalization;
using System.Security.Permissions;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.TestData.Reflection
{
    [HostProtection(SecurityAction.LinkDemand, SharedState = true)]
    public class ConvertableTestConverter : BaseTypeConverter<IConvertableTest>
    {
        #region Overrides of BaseTypeConverter<IConvertableTest>
        protected override IConvertableTest ConvertFromString(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            return new ConvertableTest();
        }

        protected override object ConvertToString(ITypeDescriptorContext context, CultureInfo culture, IConvertableTest value)
        {
            return value.ToString();
        }
        #endregion
    }
}