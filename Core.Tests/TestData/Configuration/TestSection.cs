﻿#region Using directives
using System.Configuration;
using Allag.Core.Configuration;
using Allag.Core.Configuration.Reflection;
#endregion

namespace Allag.Core.TestData.Configuration
{
    public class TestSection : Section
    {
        #region Overrides of Section
        public override string DefaultName
        {
            get { return "testSection"; }
        }
        #endregion

        #region Properties
        [ConfigurationProperty("", Options = ConfigurationPropertyOptions.IsDefaultCollection)]
        [ConfigurationCollection(typeof (ElementCollection<ObjectElement>))]
        public ElementCollection<ObjectElement> Items
        {
            get { return (ElementCollection<ObjectElement>)base[""]; }
        }

        [ConfigurationProperty("instance")]
        public InstanceElement Instance
        {
            get { return (InstanceElement)base["instance"]; }
        }
        #endregion
    }
}