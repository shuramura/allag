﻿#region Using directives
using System;
using Allag.Core.Configuration;

#endregion

namespace Allag.Core.TestData.Configuration
{
    public class ElementWithTypeName : Element<Type> {}
}