#region Using directives

using System;
using System.Xml.Serialization;

#endregion

namespace Allag.Core.TestData.Tools
{
    [Flags]
    public enum TestEnum
    {
        [XmlEnum("")]
        Empty = 16,
        One = 1,
        [XmlEnum("Second")]
        Two = 2,
        Three = 4,
        [XmlEnum("Second")]
        Four = 8
    }
}
