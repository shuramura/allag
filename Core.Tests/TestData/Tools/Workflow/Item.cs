﻿#region Using directives
using Allag.Core.Tools.Workflow;
#endregion

namespace Allag.Core.TestData.Tools.Workflow
{
    public class Item : IWorkflowItem<State>
    {
        #region Constructors
        public Item()
        {
            State = State.CurrentState;
        }
        #endregion

        #region Implementation of IWorkflowItem<State>
        public State State { get; set; }
        #endregion
    }
}