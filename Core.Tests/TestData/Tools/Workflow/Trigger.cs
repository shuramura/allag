﻿namespace Allag.Core.TestData.Tools.Workflow
{
    public enum Trigger
    {
        Undefined,
        MultiAction,
        SingleAction,
        OnlyAction,
        Trigger,
        CommonTrigger,
        CommonTriggerAll
    }
}