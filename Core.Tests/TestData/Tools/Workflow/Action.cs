﻿#region Using directives
using Allag.Core.Tools.Workflow;
#endregion

namespace Allag.Core.TestData.Tools.Workflow
{
    public class Action : CommonAction, IWorkflowAction<Item, State, Data>
    {
        #region Constructors
        public Action(int index) : base(index) {}
        #endregion

        #region Implementation of IWorkflowAction<Item,State,Data>
        public Item Execute(Item item, State state, Data data)
        {
            return item = (Item)((IWorkflowAction)this).Execute(item, state, data);
        }
        #endregion
    }
}