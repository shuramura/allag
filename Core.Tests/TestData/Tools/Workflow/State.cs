﻿#region Using directives
using System;
#endregion

namespace Allag.Core.TestData.Tools.Workflow
{
    [Flags]
    public enum State
    {
        Undefined = 0,
        CurrentState = 1,
        NewState = 2,
        State = 4
    }
}