﻿#region Using directives
using System;
using Allag.Core.Tools.Workflow;
#endregion

namespace Allag.Core.TestData.Tools.Workflow
{
    public class CommonAction : IWorkflowAction
    {
        #region Variables
        public static Func<int, object, object, object, object> Executing;
        #endregion

        #region Constructors
        public CommonAction(int index)
        {
            Index = index;
        }
        #endregion

        #region Implementation of IWorkflowAction
        object IWorkflowAction.Execute(object item, object state, object data)
        {
            if (Executing != null)
            {
                item = Executing(Index, item, state, data);
            }
            return item;
        }
        #endregion

        #region Properties
        public int Index { get; private set; }
        #endregion
    }
}