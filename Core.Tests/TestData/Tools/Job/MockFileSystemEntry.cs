﻿#region Using directives
using System;
using System.IO;
using Allag.Core.Tools.Job;

#endregion

namespace Allag.Core.TestData.Tools.Job
{
    public class MockFileSystemEntry : BaseFileSystemEntryJob<MockFileSystemEntry.Result, BaseFileSystemEntryItemResult>
    {
        #region Variables
        public Action<FileInfo> FileProcessingAction;
        public Action<DirectoryInfo> FolderProcessingAction;
        #endregion

        #region Constructors
        public MockFileSystemEntry()
        {
            FileProcessingAction = null;
        }
        #endregion

        #region Implementation of BaseFileSystemEntryJob<MockFileSystemEntry.Result,object>
        protected override bool ProcessFolder(Result result, DirectoryInfo item, BaseFileSystemEntryItemResult itemResult, ExecutionData data)
        {
            FolderProcessingAction?.Invoke(item);
            return base.ProcessFolder(result, item, itemResult, data);
        }

        protected override bool ProcessFile(Result result, FileInfo item, BaseFileSystemEntryItemResult itemResult, ExecutionData data)
        {
            FileProcessingAction?.Invoke(item);
            return base.ProcessFile(result, item, itemResult, data);
        }
        #endregion

        #region Classes
        public class Result : BaseFileSystemEntryJobResult<BaseFileSystemEntryItemResult>{}
        #endregion
    }
}