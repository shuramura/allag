﻿#region Using directives
using System;
using System.IO;
using Allag.Core.Tools.Job;

#endregion

namespace Allag.Core.TestData.Tools.Job
{
    public class MockBaseJob : BaseJob<MockBaseJob.ExecutionResult>
    {
        #region Events
        public static Action PreInitialization;
        public static Action Executing;
        public static Action PostFinalizing;
        #endregion

        #region Override methods
        protected override void Initialization(ExecutionResult result, ExecutionData data)
        {
            base.Initialization(result, data);

            ExecutionData = data;

            data.AddFile("Test", new MemoryStream(100));

            if (PreInitialization != null)
            {
                PreInitialization();
            }
        }

        protected override void Execute(ExecutionResult result, ExecutionData data)
        {
            if (Exception != null)
            {
                throw Exception;
            }

            if (Executing != null)
            {
                Executing();
            }
            result.Int = 123;
        }

        protected override void Finalizing(ExecutionResult result, ExecutionData data)
        {
            if (PostFinalizing != null)
            {
                PostFinalizing();
            }
            base.Finalizing(result, data);
        }
        #endregion

        #region Properties
        public static Exception Exception { get; set; }
        public static ExecutionData ExecutionData { get; set; }
        #endregion

        #region Classes
        public class ExecutionResult
        {
            public int Int;
        }
        #endregion
    }
}