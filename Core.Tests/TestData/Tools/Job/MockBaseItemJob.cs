﻿#region Using directives
using System;
using System.Collections.Generic;
using Allag.Core.Tools.Job;

#endregion

namespace Allag.Core.TestData.Tools.Job
{
    public class MockBaseItemJob : BaseItemJob<MockBaseItemJob.Result, string, object>
    {
        #region Events
        public static Func<IEnumerable<string>> GetItemsAction;
        public static Action ExecuteAction;
        #endregion

        #region Implementation of BaseItemJob<MockBaseItemJob.Result, object, object>
        protected override IEnumerable<string> GetItems(Result result, ExecutionData data)
        {
            return GetItemsAction != null ? GetItemsAction() : new string[0];
        }

        protected override void Execute(Result result, string item, object itemResult, ExecutionData data)
        {
            if (ExecuteAction != null)
            {
                ExecuteAction();
            }
        }
        #endregion

        #region Classes
        public class Result : BaseItemJobResult<object> {}
        #endregion

        
    }
}