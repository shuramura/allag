﻿#region Using directives
using System;
using System.Threading;
using Allag.Core.Tools.Job;

#endregion

namespace Allag.Core.TestData.Tools.Job
{
    public abstract class MockJob : IJob
    {
        #region Variables
        #endregion

        #region Constructors
        protected MockJob()
        {
            Name = "TestJob";
        }
        #endregion

        #region Implementation of IDisposable
        public abstract void Dispose();
        #endregion

        #region Implementation of IJob
        public string Name { get; set; }
        public abstract DateTime StartTime { get; set; }
        public abstract DateTime StopTime { get; set; }
        public abstract object Run(IScheduler scheduler, CancellationToken cancellationToken);

        public string Description { get; private set; }
        #endregion
    }
}