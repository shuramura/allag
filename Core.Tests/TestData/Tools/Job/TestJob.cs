#region Using directives

using System;
using Allag.Core.Tools.Job;

#endregion

namespace Allag.Core.TestData.Tools.Job
{
    public class TestJob : BaseJob<object>
    {
        #region Events
        public static Action Executing;
        #endregion

        #region Implementation of BaseJob<object>
        protected override void Execute(object result, ExecutionData data)
        {
            if (Executing != null)
            {
                Executing();
            }
        }
        #endregion

    }
}
