﻿#region Using directives
using Allag.Core.Configuration;
using Allag.Core.Configuration.Reflection;
using Allag.Core.TestData.Configuration;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.TestData.Tools
{
    public class MockConfigurableTypeConverter : ConfigurableTypeConverter<ConfigurableClass, ObjectElement>
    {
        #region Overrides of ConfigurableTypeConverter<ConfigurableClass,Element>
        protected override ElementCollection<ObjectElement> Elements
        {
            get
            {
                return CoreCfg.Root.GetSection<TestSection>().Items;
            }
        }
        #endregion
    }
}