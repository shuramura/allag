﻿#region Using directives
using System;
using System.ComponentModel;
using System.Globalization;
using Allag.Core.Tools;

#endregion

namespace Allag.Core.TestData.Tools
{
    public class MockBaseTypeConverter : BaseTypeConverter<float>
    {
        #region Events
        public static event EventHandler ConvertingFrom;
        public static event EventHandler ConvertingTo;
        #endregion


        #region Overrides of BaseTypeConverter<int>
        protected override float ConvertFromString(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (ConvertingFrom != null)
            {
                ConvertingFrom(this, EventArgs.Empty);
            }
            return 0;
        }

        protected override object ConvertToString(ITypeDescriptorContext context, CultureInfo culture, float value)
        {
            if (ConvertingTo != null)
            {
                ConvertingTo(this, EventArgs.Empty);
            }
            return string.Empty;
        }
        #endregion
    }
}