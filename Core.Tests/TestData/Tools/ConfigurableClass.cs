﻿#region Using directives
using Allag.Core.Configuration;
using Allag.Core.Configuration.Reflection;

#endregion

namespace Allag.Core.TestData.Tools
{
    public class ConfigurableClass : IConfigurable<ObjectElement>
    {
        #region Implementation of IConfigurable<ObjectElement>
        public ObjectElement Configuration { get; set; }
        #endregion
    }
}