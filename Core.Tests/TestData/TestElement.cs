﻿namespace Allag.Core.TestData
{
    public class TestElement
    {
        #region Constructors
        public TestElement(string value)
        {
            Value = value;
        }
        #endregion

        #region Properties
        public static string Value { get; set; }
        #endregion
    }
}