﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using Allag.Core.Net;

#endregion

namespace Allag.Core.TestData.Net
{
    public class CustomClient : IFileClient
    {
        #region Implementation of IFileClient
        public IEnumerable<string> Download(string sourceFolder, string mask, Func<int, IItem, Stream, string> saveFileHndler, Func<IItem, bool> isDownloadItemHandler = null, Func<IItem, bool> isDeleteItemHandler = null)
        {
            throw new NotImplementedException();
        }

        public void Upload(string destinationFolder, Func<int, Tuple<string, Stream>> getFileSource, Action<int, Stream> releaseResourse = null)
        {
            throw new NotImplementedException();
        }

        public void Delete(string folder, string itemName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IItem> GetDirectoryList(string folder, string mask)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}