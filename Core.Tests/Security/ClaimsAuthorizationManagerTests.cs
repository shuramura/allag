﻿#region Using directives
using System;
using System.Security.Claims;
using NUnit.Framework;

#endregion

namespace Allag.Core.Security
{
    [TestFixture]
    public class ClaimsAuthorizationManagerTests
    {
        #region Variables
        private ClaimsPrincipal _principal;
        private ClaimsIdentity _identity;
        private ClaimsAuthorizationManager _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _identity = new ClaimsIdentity();
            _principal = new ClaimsPrincipal(_identity);

            _instance = new ClaimsAuthorizationManager();
        }

        [TearDown]
        public void CleanUp()
        {
            ClaimsAuthorizationManager.ResourcePrefix = null;
            ClaimsAuthorizationManager.ResourceSplitter = null;
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Post-Conditions
            Assert.IsNull(ClaimsAuthorizationManager.ResourcePrefix, "The property 'ResourcePrefix' is wrong.");
            Assert.IsNull(ClaimsAuthorizationManager.ResourceSplitter, "The property 'ResourceSplitter' is wrong.");
        }

        [Test]
        public void ResourceIdentifier_Set_Test()
        {
            // Pre-Conditions
            string expected = Guid.NewGuid().ToString();

            // Action
            ClaimsAuthorizationManager.ResourcePrefix = expected;

            // Post-Conditions
            Assert.AreEqual(expected, ClaimsAuthorizationManager.ResourcePrefix);
        }

        [TestCase("resource", "action0,action1", true)]
        [TestCase("invalid", "action0", false)]
        [TestCase("resource", "invalid", false)]
        [TestCase("resource.any", "action0,action1", false)]
        [TestCase("resource.part0", "action0", true)]
        public void CheckAccess_CustomSplitter_Test(string resource, string action, bool expected)
        {
            // Pre-Conditions
            ClaimsAuthorizationManager.ResourcePrefix = "ResourcePrefix";
            ClaimsAuthorizationManager.ResourceSplitter = item =>
            {
                int nIndex;
                if ((nIndex = item.LastIndexOf(".", StringComparison.InvariantCulture)) != -1)
                {
                    item = item.Substring(0, nIndex);
                }
                else
                {
                    item = null;
                }
                return item;
            };
            _identity.AddClaim(new Claim(ClaimsAuthorizationManager.ResourcePrefix + resource, action));
            AuthorizationContext context = new AuthorizationContext(_principal, "resource.part0.part1", "action0");

            // Action
            bool result = _instance.CheckAccess(context);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [TestCase("f581ba326cb34491b1525e4a5d6f40d0", "action0,action1", true)]
        [TestCase("1f10f2f8c18841df9eb6d3ce717766bd", "action0", false)]
        [TestCase("f581ba326cb34491b1525e4a5d6f40d0", "invalid", false)]
        [TestCase("f581ba326cb34491b1525e4a5d6f40d01f10f2f8c18841df9eb6d3ce717766bd", "action0,action1", false)]
        [TestCase("f581ba326cb34491b1525e4a5d6f40d0a2c5f87be8724121884ee740e7e84f18", "action0", true)]
        public void CheckAccess_Test(string resource, string action, bool expected)
        {
            // Pre-Conditions
            _identity.AddClaim(new Claim(ClaimsAuthorizationManager.ResourcePrefix + resource, action));
            AuthorizationContext context = new AuthorizationContext(_principal, "f581ba326cb34491b1525e4a5d6f40d0a2c5f87be8724121884ee740e7e84f18", "action0");

            // Action
            bool result = _instance.CheckAccess(context);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void CheckAccess_MultiResource_Test()
        {
            // Pre-Conditions
            const string strResource = "Res0";
            const string strOperation = "Op0";
            _identity.AddClaim(new Claim(ClaimsAuthorizationManager.ResourcePrefix + strResource, strOperation));
            AuthorizationContext context = new AuthorizationContext(_principal, strResource + "|Res1", strOperation);

            // Action
            bool result = _instance.CheckAccess(context);

            // Post-Conditions
            Assert.IsTrue(result);
        }
        #endregion
    }
}