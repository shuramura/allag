﻿#region Using directives
using System;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using NUnit.Framework;

#endregion

namespace Allag.Core.Security
{
    [TestFixture]
    public class IdentityFactoryTests
    {
        #region Variables
        private IPrincipal _principal;
        private ClaimsIdentity _identity;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _principal = Thread.CurrentPrincipal;
            _identity = new ClaimsIdentity();

            IdentityFactory.SetIdentity(_identity);
        }

        [TearDown]
        public void CleanUp()
        {
            Thread.CurrentPrincipal = _principal;
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Pre-Conditions
            Guid userId = Guid.NewGuid();
            Guid sessionId = Guid.NewGuid();
            _identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, userId.ToString()));
            _identity.AddClaim(new Claim(ClaimTypes.Sid, sessionId.ToString()));

            // Post-Conditions
            Assert.AreEqual(userId, IdentityFactory.CurrentUserId, "The property 'CurrentUserId' is wrong.");
            Assert.AreEqual(sessionId, IdentityFactory.CurrentSessionKey, "The property 'CurrentSessionKey' is wrong.");
        }

        [Test]
        public void CurrentSessionKey_Set_Test()
        {
            // Pre-Conditions
            Guid expected = Guid.NewGuid();

            // Action
            IdentityFactory.CurrentSessionKey = expected;

            // Post-Conditions
            Assert.AreEqual(expected, IdentityFactory.CurrentSessionKey, "The property 'CurrentSessionKey' is wrong.");
            Assert.AreEqual(expected, Guid.Parse(_identity.FindFirst(ClaimTypes.Sid).Value), "The identity is wrong.");
        }

        [Test]
        public void SetSessionKey_NotExist_Test()
        {
            // Pre-Conditions
            ClaimsIdentity identity = new ClaimsIdentity();
            Guid key = Guid.NewGuid();

            // Action
            IdentityFactory.SetSessionKey(identity, key);

            // Post-Conditions
            Assert.AreEqual(key, Guid.Parse(identity.FindFirst(ClaimTypes.Sid).Value));
        }

        [Test]
        public void SetSessionKey_Exist_Test()
        {
            // Pre-Conditions
            Guid key = Guid.NewGuid();
            ClaimsIdentity identity = new ClaimsIdentity();
            identity.AddClaim(new Claim(ClaimTypes.Sid, Guid.NewGuid().ToString()));

            // Action
            IdentityFactory.SetSessionKey(identity, key);

            // Post-Conditions
            Assert.AreEqual(key, Guid.Parse(identity.FindFirst(ClaimTypes.Sid).Value));
        }

        [Test]
        public void SetIdentity_Test()
        {
            // Pre-Conditions
            int nCount = 0;
            IIdentity identity = new ClaimsIdentity();

            // Action
            IdentityFactory.SetIdentity(identity, principal =>
            {
                Assert.IsNotNull(principal, "The principal is wrong.");
                Assert.AreEqual(identity, principal.Identity, "The identity is wrong.");
                nCount++;
            });

            // Post-Conditions
            Assert.AreEqual(1, nCount, "The count is wrong.");
            Assert.AreEqual(identity, Thread.CurrentPrincipal.Identity, "The thread identity is wrong.");
        }

        [Test]
        public void SetIdentity_NullPostAction_Test()
        {
            // Pre-Conditions
            IIdentity identity = new ClaimsIdentity();

            // Action
            IdentityFactory.SetIdentity(identity, null);

            // Post-Conditions
            Assert.IsNotNull(Thread.CurrentPrincipal, "The principal is wrong.");
            Assert.AreEqual(identity, Thread.CurrentPrincipal.Identity, "The thread identity is wrong.");
        }

        [Test]
        public void SetIdentity_NullIdentity_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() =>IdentityFactory.SetIdentity(null));
        }

        [Test]
        public void GetClaim_NullIdentity_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => IdentityFactory.GetClaim<string>(null, "type"));
        }

        [Test]
        public void GetClaim_Tetst()
        {
            // Pre-Conditions
            ClaimsIdentity identity = new ClaimsIdentity();
            Guid expected = Guid.NewGuid();
            IdentityFactory.SetClaim(identity, ClaimTypes.Actor, expected);
            
            // Action
            Guid result = IdentityFactory.GetClaim<Guid>(identity, ClaimTypes.Actor);

            // Post-Conditions
            Assert.AreEqual(expected, result);

        }

        [Test]
        public void SetClaim_NullIdentity_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => IdentityFactory.SetClaim(null, "type", "value"));
        }
        #endregion
    }
}