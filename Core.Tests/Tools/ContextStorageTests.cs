﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools
{
    [TestFixture]
    public class ContextStorageTests
    {
        #region Variables
        private string _id;
        private ContextStorage _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _id = Guid.NewGuid().ToString();
            _instance = new ContextStorage();
        }
        #endregion

        #region Tests
        [Test]
        public void GetValueWrongIdTest([Values(null, "")] string id)
        {
            // Action 
            Assert.That(() => _instance.GetValue<ContextStorageTests>(id), Throws.ArgumentException);
        }

        [Test]
        public void GetValue_NotExist_Test()
        {
            // Pre-Conditions
            string id = Guid.NewGuid().ToString();

            // Action 
            object result = _instance.GetValue<object>(id);

            // Post-Conditions
            Assert.IsNull(result);
        }

        [Test]
        public void SetValue_Test()
        {
            // Pre-Conditions
            object expected = new object();

            // Action
            _instance.SetValue(_id, expected);

            // Post-Conditions
            Assert.AreEqual(expected, _instance.GetValue<object>(_id), "The property 'Value' is wrong.");
            Assert.IsNotNull(CallContext.LogicalGetData(_id), "The storage location is wrong.");
        }

        [Test]
        public void SetValueWrongIdTest([Values(null, "")] string id)
        {
            // Action 
            Assert.That(() => _instance.SetValue<object>(id, null), Throws.ArgumentException);
        }

        [Test]
        public void GetValue_MultiThreading_Test()
        {
            // Pre-Conditions
            string strKey = Guid.NewGuid().ToString();
            _instance.SetValue(strKey, this);

            List<object> expected = new List<object>();
            List<object> result = new List<object>();
            List<Task> tasks = new List<Task>();
            Action action = () => result.Add(_instance.GetValue<ContextStorageTests>(strKey));
            Task task = new Task(action);
            tasks.Add(task);
            expected.Add(this);

            const int nCount = 10;
            for (int i = 0; i < nCount; i++)
            {
                task = task.ContinueWith(x => action());
                tasks.Add(task);
                expected.Add(this);
            }

            // Action
            tasks[0].Start();

            // Post-Conditions
            Task.WaitAll(tasks.ToArray());
            CollectionAssert.AreEqual(expected, result);
        }
        #endregion
    }
}