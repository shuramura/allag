﻿#region Using directives
using System;
using Allag.Core.TestData.Tools;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools
{
    [TestFixture]
    public class BaseTypeConverterTests : BaseTypeConverterTests<MockBaseTypeConverter, float>
    {
        #region Overrides of BaseTypeConverterTests<MockBaseTypeConverter,int>
        [Test]
        public override void ConvertFromStringTest()
        {
            // Pre-Conditions
            int nConvertingFrom = 0;
            int nConvertingTo = 0;
            MockBaseTypeConverter.ConvertingFrom += delegate { nConvertingFrom++; };
            MockBaseTypeConverter.ConvertingTo += delegate { nConvertingTo++; };

            // Action
            Instance.ConvertFrom(null, null, "100");

            // Post-Conditions
            Assert.AreEqual(1, nConvertingFrom, "The coverting from count is wrong.");
            Assert.AreEqual(0, nConvertingTo, "The coverting from count is wrong.");
        }

        [Test]
        public override void ConvertToTypeStringTest()
        {
            // Pre-Conditions
            int nConvertingFrom = 0;
            int nConvertingTo = 0;
            MockBaseTypeConverter.ConvertingFrom += delegate { nConvertingFrom++; };
            MockBaseTypeConverter.ConvertingTo += delegate { nConvertingTo++; };
            //
            // Action
            Instance.ConvertTo(null, null, 0f, typeof(string));
            //
            // Post-Conditions
            Assert.AreEqual(0, nConvertingFrom, "The coverting from count is wrong.");
            Assert.AreEqual(1, nConvertingTo, "The coverting to count is wrong.");
        }

        protected override float ValueOfType
        {
            get { return 100f; }
        }
        #endregion
    }

    public abstract class BaseTypeConverterTests<TTypeConverter, TType>
        where TTypeConverter : BaseTypeConverter<TType>, new()
    {
        #region Initialization
        [SetUp]
        public virtual void SetUp()
        {
            Instance = new TTypeConverter();
        }
        #endregion

        #region Tests
        [Test]
        public abstract void ConvertFromStringTest();

        [Test]
        public abstract void ConvertToTypeStringTest();

        [Test]
        public void CanConvertFromNullTest()
        {
            // Action 
            bool result = Instance.CanConvertFrom(null, null);

            // Post-Conditions
            Assert.IsFalse(result);
        }

        [Test]
        public void CanConvertFromTypeStringTest()
        {
            // Action 
            bool result = Instance.CanConvertFrom(null, typeof(string));

            // Post-Conditions
            Assert.IsTrue(result);
        }

        [Test]
        public void CanConvertFromTypeObjectTest()
        {
            // Action 
            bool result = Instance.CanConvertFrom(null, typeof(TType));

            // Post-Conditions
            Assert.IsTrue(result);
        }

        [Test]
        public void CanConvertFromTypeWrongTest()
        {
            // Action 
            bool result = Instance.CanConvertFrom(null, GetType());

            // Post-Conditions
            Assert.IsFalse(result);
        }

        [Test]
        public void CanConvertToNullTest()
        {
            // Action 
            bool result = Instance.CanConvertTo(null, null);

            // Post-Conditions
            Assert.IsFalse(result);
        }

        [Test]
        public void CanConvertToTypeStringTest()
        {
            // Action 
            bool result = Instance.CanConvertTo(null, typeof(string));

            // Post-Conditions
            Assert.IsTrue(result);
        }

        [Test]
        public void CanConvertToTypeObjectTest()
        {
            // Action 
            bool result = Instance.CanConvertTo(null, typeof(TType));

            // Post-Conditions
            Assert.IsTrue(result);
        }

        [Test]
        public void CanConvertToTypeWrongTest()
        {
            // Action 
            bool result = Instance.CanConvertTo(null, GetType());

            // Post-Conditions
            Assert.IsFalse(result);
        }

        [Test]
        public void ConvertFromWrongTypeTest()
        {
            // Action
            Assert.That(() => Instance.ConvertFrom(null, null, this), Throws.InstanceOf<NotSupportedException>());
        }

        [Test]
        public void ConvertFromTypeObjectTest()
        {
            // Pre-Conditions
            TType expected = ValueOfType;

            // Action
            TType result = (TType)Instance.ConvertFrom(null, null, expected);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ConvertToWrongTypeTest()
        {
            // Action
            Assert.That(() => Instance.ConvertTo(null, null, this, GetType()), Throws.InstanceOf<NotSupportedException>());
        }

        [Test]
        public void ConvertToTypeObjectTest()
        {
            // Pre-Conditions
            TType expected = ValueOfType;

            // Action
            TType result = (TType)Instance.ConvertTo(null, null, expected, typeof(TType));

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }


        #endregion

        #region Abstruct properties
        protected abstract TType ValueOfType { get; }
        #endregion

        #region Properties
        protected TTypeConverter Instance { get; private set; }
        #endregion

    }
}