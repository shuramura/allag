﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Allag.Core.TestData.Tools.Workflow;
using NUnit.Framework;
using Action = Allag.Core.TestData.Tools.Workflow.Action;
#endregion

namespace Allag.Core.Tools.Workflow
{
    [TestFixture]
    public class WorkflowTests
    {
        #region Variables
        private Workflow<Item, State, Trigger, TestData.Tools.Workflow.Data> _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Initialize()
        {
            _instance = Workflow.Get<Item, State, Trigger, TestData.Tools.Workflow.Data>("Workflow");
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual("Workflow", _instance.Name, "The property 'Name' is wrong.");
        }

        [Test]
        public void GetNullNameTest()
        {
            // Action
            Assert.That(() => Workflow.Get<Item, State, Trigger, TestData.Tools.Workflow.Data>(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void GetNotExistsNameTest()
        {
            // Action
            Workflow<Item, State, Trigger, TestData.Tools.Workflow.Data> result = Workflow.Get<Item, State, Trigger, TestData.Tools.Workflow.Data>(Guid.NewGuid());

            // Post-Conditions
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetInvalidActionImplementationTest()
        {
            // Action
            Assert.That(() => Workflow.Get<Item, State, Trigger, TestData.Tools.Workflow.Data>("InvalidActionImplementation"), Throws.InstanceOf<InvalidDataException>());
        }

        [Test]
        public void GetNotExistActionTest()
        {
            // Action
            Assert.That(() => Workflow.Get<Item, State, Trigger, TestData.Tools.Workflow.Data>("NotExistAction"), Throws.InstanceOf<InvalidDataException>());
        }

        [Test]
        public void ExecuteWrongStateTest()
        {
            // Pre-Conditions
            Item item = new Item {State = State.Undefined};

            // Action
            Assert.That(() => _instance.Execute(item, Trigger.SingleAction, null), Throws.InstanceOf<InvalidDataException>());
        }

        [Test]
        public void ExecuteWrongTriggerTest()
        {
            // Pre-Conditions
            Item item = new Item();

            // Action
            Assert.That(() => _instance.Execute(item, Trigger.Trigger, null), Throws.InstanceOf<InvalidDataException>());
        }

        [Test]
        public void ExecuteSingleActionTest()
        {
            // Pre-Conditions
            const Trigger trigger = Trigger.SingleAction;
            const State expectedState = State.NewState;
            int[] expectedActionOrder = new int[] {0, 1, 3};

            // Action
            Execute(_instance, trigger, expectedState, expectedActionOrder);
        }

        [Test]
        public void ExecuteMultiActionTest()
        {
            // Pre-Conditions
            const Trigger trigger = Trigger.MultiAction;
            const State expectedState = State.NewState;
            int[] expectedActionOrder = new int[] {0, 1, 2, 3};

            // Action
            Execute(_instance, trigger, expectedState, expectedActionOrder);
        }

        [Test]
        public void ExecuteOnlyActionTest()
        {
            // Pre-Conditions
            const Trigger trigger = Trigger.OnlyAction;
            const State expectedState = State.CurrentState;
            int[] expectedActionOrder = new int[] {0, 1, 3};

            // Action
            Execute(_instance, trigger, expectedState, expectedActionOrder);
        }

        [Test]
        public void ExecuteCommonTriggerTest()
        {
            // Pre-Conditions
            const Trigger trigger = Trigger.CommonTrigger;
            const State expectedState = State.CurrentState;
            int[] expectedActionOrder = new int[] {0, 1, 3};

            // Action
            Execute(_instance, trigger, expectedState, expectedActionOrder);
        }

        [Test]
        public void ExecuteCommonTriggerAllImplicitDeclarationTest()
        {
            // Pre-Conditions
            const Trigger trigger = Trigger.CommonTriggerAll;
            const State expectedState = State.State;
            int[] expectedActionOrder = new int[] {0, 2, 3};

            // Action
            Execute(_instance, trigger, expectedState, expectedActionOrder, new Item {State = expectedState});
        }

        [Test]
        public void ExecuteCommonTriggerAllExplicitDeclarationTest()
        {
            // Pre-Conditions
            const Trigger trigger = Trigger.CommonTriggerAll;
            const State expectedState = State.CurrentState;
            int[] expectedActionOrder = new int[] {0, 1, 3};

            // Action
            Execute(_instance, trigger, expectedState, expectedActionOrder);
        }

        [Test]
        public void GetActionsTest()
        {
            // Pre-Conditions
            const Trigger trigger = Trigger.MultiAction;
            const State expectedState = State.NewState;
            State state = State.CurrentState;

            // Action
            object[] result = _instance.GetActions(trigger, ref state);

            // Post-Conditions
            Assert.AreEqual(expectedState, state, "The state is wrong.");
            CollectionAssert.AreEqual(new Type[] {typeof(CommonAction), typeof(Action), typeof(Action), typeof(CommonAction)}, result.Select(x => x.GetType()).ToArray(), "The actions are wrong.");
        }

        [Test]
        public void ConstructNewWorkflowTest()
        {
            // Pre-Conditions
            const Trigger trigger = Trigger.MultiAction;
            const State expectedState = State.NewState;
            const State state = State.CurrentState;
            Workflow<Item, State, Trigger, TestData.Tools.Workflow.Data> instance = Workflow.Get<Item, State, Trigger, TestData.Tools.Workflow.Data>(Guid.NewGuid());

            // Action
            instance.AddAction("action0", new CommonAction(0)).
                     AddAction("action1", (IWorkflowAction<Item, State, TestData.Tools.Workflow.Data>)new Action(1))
                    .AddTrigger(state, trigger, expectedState, "action1", "action0");


            // Post-Conditions
            Execute(instance, trigger, expectedState, new int[] {1, 0});
        }

        [Test]
        public void ConstructNewWorkflowNotExistsActionTest()
        {
            // Pre-Conditions
            const Trigger trigger = Trigger.MultiAction;
            const State expectedState = State.NewState;
            const State state = State.CurrentState;
            Workflow<Item, State, Trigger, TestData.Tools.Workflow.Data> instance = Workflow.Get<Item, State, Trigger, TestData.Tools.Workflow.Data>(Guid.NewGuid());

            // Action
            Assert.That(() => instance.AddAction("action0", new CommonAction(0))
                    .AddTrigger(state, trigger, expectedState, "action1"), Throws.InstanceOf<InvalidDataException>());
        }
        #endregion

        #region Helpers
        private static void Execute(Workflow<Item, State, Trigger, TestData.Tools.Workflow.Data> instance, Trigger trigger, State expectedState, IList<int> expectedActionOrder, Item input = null)
        {
            // Pre-Conditions
            TestData.Tools.Workflow.Data expectedData = new TestData.Tools.Workflow.Data();
            int nCount = 0;
            if (input == null)
            {
                input = new Item();
            }
            CommonAction.Executing = (index, item, state, data) =>
                {
                    Assert.AreEqual(expectedActionOrder[nCount], index, "The index is wrong.");

                    Assert.AreEqual(input, item, "The item '{0}' is wrong.", nCount);
                    Assert.AreEqual(expectedState, state, "The state '{0}' is wrong.", nCount);
                    Assert.AreEqual(expectedData, data, "The data '{0}' is wrong.", nCount);

                    nCount++;
                    return item;
                };

            // Action
            instance.Execute(input, trigger, expectedData);

            // Post-Conditions
            Assert.AreEqual(expectedActionOrder.Count, nCount, "The action's count is wrong.");
            Assert.AreEqual(expectedState, input.State, "The property 'State' is wrong.");
        }
        #endregion
    }
}