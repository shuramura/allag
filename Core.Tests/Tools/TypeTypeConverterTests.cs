﻿#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools
{
    [TestFixture]
    public class TypeTypeConverterTests : BaseTypeConverterTests<TypeTypeConverter, Type>
    {
        #region Overrides of BaseTypeConverterTests<TypeTypeConverter,Type>
        [Test]
        public override void ConvertFromStringTest()
        {
            // Action
            Type result = (Type)Instance.ConvertFrom(null, null, "Allag.Core.Tools.TypeTypeConverterTests, Allag.Core.Tests");

            // Post-Conditions
            Assert.AreEqual(GetType(), result);
        }

        [Test]
        public void ConvertFromEmptyStringTest([Values("", "    ")] string str)
        {
            // Action
            Type result = (Type)Instance.ConvertFrom(null, null, str);

            // Post-Conditions
            Assert.IsNull(result);
        }

        [Test]
        public void ConvertFromWrongStringTest()
        {
            // Action
            Assert.That(() => Instance.ConvertFrom(null, null, "lalala"), Throws.InstanceOf<TypeLoadException>());
        }

        [Test]
        public override void ConvertToTypeStringTest()
        {
            // Action
            string result = (string)Instance.ConvertTo(null, null, GetType(), typeof(string));
            //
            // Post-Conditions
            Assert.AreEqual(GetType().AssemblyQualifiedName, result);
        }

        [Test]
        public void ConvertToNullTypeObjectTest()
        {
            // Action
            string result = (string)Instance.ConvertTo(null, null, null, typeof(string));

            // Post-Conditions
            Assert.AreEqual(string.Empty, result);
        }

        protected override Type ValueOfType
        {
            get { return GetType(); }
        }
        #endregion
    }
}