﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Tools
{
    [TestFixture]
    public class BaseResourceTests
    {
        #region Variables
        private MockRepository _mocks;
        private BaseResource _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _mocks = new MockRepository();

            _instance = CreateInstance(ResourceLocation.File, "value");
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual("value", _instance.Value, "The property 'Value' is wrong.");
            Assert.AreEqual("[{location:File},{1:2}]value", _instance.ValueWithOptions, "The property 'ValueWithOptions' is wrong.");
            Assert.AreEqual(ResourceLocation.File, _instance.Location, "The property 'Location' is wrong.");
            Assert.AreEqual(ResourceType.Text, _instance.Type, "The property 'Type' is wrong.");
        }

        [Test]
        public void CtorNullValueTest()
        {
            // Action
            Assert.That(() => CreateInstance(ResourceLocation.File, null), Throws.InnerException.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CtorNullSettingsTest()
        {
            // Action
            BaseResource instance = CreateInstance(ResourceLocation.File, "", null);

            // Post-Conditions
            Assert.AreEqual(ResourceLocation.File, instance.Location);
        }

        [Test]
        public void CtorWrongLocationPathTest()
        {
            // Action
            Assert.That(() => CreateInstance(ResourceLocation.Assembly, ""), Throws.InnerException.InstanceOf<ArgumentException>());
        }

        [Test]
        public void TypeSetFirstTimeTest()
        {
            // Pre-Conditions
            const ResourceType expected = ResourceType.Binary;

            // Action
            _instance.Type = expected;

            // Pose-Conditions
            Assert.AreEqual(expected, _instance.Type);
        }

        [Test]
        public void TypeSetSecondTimeTest()
        {
            // Pre-Conditions
            const ResourceType expected = ResourceType.Binary;
            _instance.Type = ResourceType.Xslt;

            // Action
            _instance.Type = expected;

            // Pose-Conditions
            Assert.AreEqual(expected, _instance.Type);
        }

        [Test]
        public void ToStringTest()
        {
            // Pre-Conditions
            const string expected = "value";

            // Action
            string result = _instance.ToString();

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ToStringWithXsltAndNullParametersTest()
        {
            // Pre-Conditions
            BaseResource instance = CreateInstance(ResourceLocation.File, "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" xmlns:msxsl=\"urn:schemas-microsoft-com:xslt\"><xsl:output method=\"text\" encoding=\"utf-8\"/><xsl:template match=\"/\"><xsl:text>Xslt value</xsl:text></xsl:template></xsl:stylesheet>");
            instance.Type = ResourceType.Xslt;
            const string expected = "Xslt value";

            // Action
            string result = instance.ToString();

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ToStringWithXsltAndParametersTest()
        {
            // Pre-Conditions
            BaseResource instance = CreateInstance(ResourceLocation.File, "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" xmlns:msxsl=\"urn:schemas-microsoft-com:xslt\"><xsl:output method=\"text\" encoding=\"utf-8\"/><xsl:template match=\"/Parameters\"><xsl:value-of select=\"Test1\"/><xsl:value-of select=\"Test2\"/><xsl:value-of select=\"Test3\"/></xsl:template></xsl:stylesheet>");
            instance.Type = ResourceType.Xslt;
            const string expected = "123";

            // Action
            string result = instance.ToString(new Parameters { Test1 = "1", Test2 = 2, Test3 = 3 });

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }
        #endregion

        #region Methods
        private BaseResource CreateInstance(ResourceLocation location, string value)
        {
            return CreateInstance(location, value, new Dictionary<string, string> { { "location", "File" }, { "1", "2" } });
        }


        private BaseResource CreateInstance(ResourceLocation location, string value, IDictionary<string, string> settings)
        {
            BaseResource ret = _mocks.PartialMock<BaseResource>(location, value, settings);
            SetupResult.For(ret.GetStream()).Return(new MemoryStream(new UTF8Encoding().GetBytes(value)));
            _mocks.ReplayAll();
            return ret;
        }
        #endregion

        #region Classes
        public class Parameters
        {
            #region Properties
            public string Test1 { get; set; }
            public int Test2 { get; set; }
            public long Test3 { get; set; }
            #endregion

        }

        #endregion

    }
}