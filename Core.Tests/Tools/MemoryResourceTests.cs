﻿#region Using directives
using System.Collections.Generic;
using System.IO;
using System.Text;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools
{
    [TestFixture]
    public class MemoryResourceTests
    {
        #region Variables
        private MemoryResource _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = CreateInstance("value");
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual("value", _instance.ToString(), "The content is wrong.");
        }

        [Test]
        public void GetStreamTest()
        {
            // Pre-Conditions
            byte[] expected = new UTF8Encoding().GetBytes("value");
            // Action
            using(Stream stream = _instance.GetStream())
            {
                // Post-Conditions    
                byte[] result = new byte[stream.Length];
                stream.Read(result, 0, result.Length);
                CollectionAssert.AreEqual(expected, result);
            }

        }
        #endregion

        #region Methods
        private static MemoryResource CreateInstance(string value)
        {
            return new MemoryResource(value, new Dictionary<string, string> {{ResourceTypeConverter.LocationProperty, "memory"}});
        }
        #endregion
    }
}