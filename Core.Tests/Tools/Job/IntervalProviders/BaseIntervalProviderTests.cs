#region Using directives

using System;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Tools.Job.IntervalProviders
{
    [TestFixture]
    public class BaseIntervalProviderTests
    {
        #region Variables
        private MockRepository _mocks;
        private BaseIntervalProvider _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _mocks = new MockRepository();
            _instance = _mocks.PartialMock<BaseIntervalProvider>(ScheduleType.BySecond);
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(ScheduleType.BySecond, _instance.ScheduleType, "The property 'ScheduleType' is wrong.");
            Assert.IsFalse(_instance.RunImmediately, "The property 'RunImmediately' is wrong.");
            Assert.AreEqual(0, _instance.Jobs.Count, "The job count is wrong.");
            Assert.AreEqual(TimeSpan.Zero, _instance.StartTime, "The property 'StartTime' is wrong.");
            Assert.AreEqual(TimeSpan.MaxValue, _instance.StopTime, "The property 'StopTime' is wrong.");
        }

        [Test]
        public void Add_Test()
        {
            // Pre-Conditions
            IJob job = _mocks.DynamicMock<IJob>();
            _mocks.ReplayAll();

            Action<IJob[]> action = jobs => {};

            // Action
            _instance.Add(job, action);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(1, _instance.Jobs.Count, "The count is wrong.");
            Assert.AreEqual(action, _instance.Jobs[job], "The job action is wrong.");
        }

        [Test]
        public void Add_NullJob_Test()
        {
            // Pre-Conditions
            _mocks.ReplayAll();
            
            // Action
            Assert.That(() => _instance.Add(null, jobs => { }), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void Add_NullAction_Test()
        {
            // Pre-Conditions
            IJob job = _mocks.DynamicMock<IJob>();
            _mocks.ReplayAll();

            // Action
            Assert.That(() => _instance.Add(job, null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void Dispose_Test()
        {
            // Action
            _instance.Dispose();

            // Post-Conditions
            object temp;
            Assert.That(() => temp = _instance.Jobs, Throws.InstanceOf<ObjectDisposedException>());
        }
        #endregion
    }
}
