#region Using directives
using System;
using Allag.Core.TestData;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools.Job.IntervalProviders
{
    [TestFixture]
    public class SimpleTests
    {
        #region Tests
        [Test]
        public void CtorWrongScheduleTypeTest()
        {
            // Action
            Assert.That(() => new Simple(ScheduleType.Daily, 1), Throws.ArgumentException);
        }

        #region BySecond
        [Test]
        public void GetNextRunForBySecondTest()
        {
            // Pre-Conditions 
            Simple instance = new Simple(ScheduleType.BySecond, 10);
            instance.GetNextRun(DateTime.Now);
            DateTime dtLastRun = DateTime.Now;
            DateTime expected = dtLastRun.AddSeconds(10);

            // Action
            DateTime result = instance.GetNextRun(dtLastRun);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetNextRunForBySecondFirstTimeTest()
        {
            // Pre-Conditions 
            Simple instance = new Simple(ScheduleType.BySecond, 10);
            instance.GetNextRun(DateTime.Now);
            DateTime dtLastRun = DateTime.MinValue;
            DateTime expected = DateTime.Now;

            // Action
            DateTime result = instance.GetNextRun(dtLastRun);

            // Post-Conditions
            Assert.GreaterOrEqual(result, expected);
            Assert.LessOrEqual(result, expected.AddMilliseconds(5));
        }

        [Test]
        public void GetNextRunForBySecondWithTimeWindowStartTest()
        {
            // Pre-Conditions 
            DateTime dateTime = DateTime.Now.AddSeconds(20);
            DateTime dtLastRun = DateTime.Now;
            DateTime expected = dateTime.AddSeconds(10);
            Simple instance = new Simple(ScheduleType.BySecond, 10) {StartTime = dateTime.TimeOfDay, StopTime = dateTime.AddSeconds(20).TimeOfDay};

            // Action
            DateTime result = instance.GetNextRun(dtLastRun);

            // Post-Conditions
            Helper.Validate(expected, result, string.Empty);
        }

        [Test]
        public void GetNextRunForBySecondWithTimeWindowStopTest()
        {
            // Pre-Conditions 
            DateTime dateTime = DateTime.Now.AddSeconds(-20);
            DateTime expected = dateTime.AddDays(1).AddSeconds(10);
            Simple instance = new Simple(ScheduleType.BySecond, 10) { StartTime = dateTime.TimeOfDay, StopTime = dateTime.AddSeconds(20).TimeOfDay };

            // Action
            DateTime result = instance.GetNextRun(DateTime.Now);

            // Post-Conditions
            Helper.Validate(expected, result, string.Empty);
        }

        [Test]
        public void GetNextRunForBySecondWithWrongTimeWindowStopTest()
        {
            // Pre-Conditions 
            DateTime dateTime = DateTime.Now;
            DateTime expected = DateTime.MaxValue;
            Simple instance = new Simple(ScheduleType.BySecond, 10) { StartTime = dateTime.TimeOfDay, StopTime = dateTime.AddSeconds(5).TimeOfDay };

            // Action
            DateTime result = instance.GetNextRun(DateTime.Now);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }
        #endregion

        #region ByMinute
        [Test]
        public void GetNextRunForByMinuteTest()
        {
            // Pre-Conditions 
            Simple instance = new Simple(ScheduleType.ByMinute, 1);
            DateTime dtLastRun = DateTime.Now;
            DateTime expected = dtLastRun.AddMinutes(1);

            // Action
            DateTime result = instance.GetNextRun(dtLastRun);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetNextRunForByMinuteFirstTimeTest()
        {
            // Pre-Conditions 
            Simple instance = new Simple(ScheduleType.ByMinute, 1);
            DateTime dtLastRun = DateTime.MinValue;
            DateTime expected = DateTime.Now;

            // Action
            DateTime result = instance.GetNextRun(dtLastRun);

            // Post-Conditions
            Assert.GreaterOrEqual(result, expected);
            Assert.LessOrEqual(result, expected.AddSeconds(1));
        }

        [Test]
        public void GetNextRunForByMinuteWithTimeWindowStartTest()
        {
            // Pre-Conditions 
            DateTime dateTime = DateTime.Now.AddMinutes(2);
            DateTime expected = dateTime.AddMinutes(1);
            Simple instance = new Simple(ScheduleType.ByMinute, 1) { StartTime = dateTime.TimeOfDay, StopTime = dateTime.AddMinutes(2).TimeOfDay };

            // Action
            DateTime result = instance.GetNextRun(DateTime.Now);

            // Post-Conditions
            Helper.Validate(expected, result, string.Empty);
        }

        [Test]
        public void GetNextRunForByMinuteWithTimeWindowStopTest()
        {
            // Pre-Conditions 
            DateTime dateTime = DateTime.Now.AddMinutes(-2);
            DateTime expected = dateTime.AddDays(1).AddMinutes(1);
            Simple instance = new Simple(ScheduleType.ByMinute, 1) { StartTime = dateTime.TimeOfDay, StopTime = dateTime.AddMinutes(2).TimeOfDay };

            // Action
            DateTime result = instance.GetNextRun(DateTime.Now);

            // Post-Conditions
            Helper.Validate(expected, result, string.Empty);
        }

        [Test]
        public void GetNextRunForByMinuteWithWrongTimeWindowStopTest()
        {
            // Pre-Conditions 
            DateTime dateTime = DateTime.Now;
            DateTime expected = DateTime.MaxValue;
            Simple instance = new Simple(ScheduleType.ByMinute, 10) { StartTime = dateTime.TimeOfDay, StopTime = dateTime.AddMinutes(5).TimeOfDay };

            // Action
            DateTime result = instance.GetNextRun(DateTime.Now);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }
        #endregion

        #region Hourly
        [Test]
        public void GetNextRunForHourlyTest()
        {
            // Pre-Conditions 
            Simple instance = new Simple(ScheduleType.Hourly, 0.5);
            DateTime dtLastRun = DateTime.Now;
            DateTime expected = dtLastRun.AddHours(0.5);
            

            // Action
            DateTime result = instance.GetNextRun(dtLastRun);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetNextRunForHourlyFirstTimeTest()
        {
            // Pre-Conditions 
            Simple instance = new Simple(ScheduleType.Hourly, 0.5);
            DateTime dtLastRun = DateTime.MinValue;
            DateTime expected = DateTime.Now;

            // Action
            DateTime result = instance.GetNextRun(dtLastRun);

            // Post-Conditions
            Assert.GreaterOrEqual(result, expected);
            Assert.LessOrEqual(result, expected.AddMilliseconds(5));
        }

        [Test]
        public void GetNextRunForHourlyWithTimeWindowStartTest()
        {
            // Pre-Conditions 
            DateTime dateTime = DateTime.Now.AddHours(1);
            DateTime expected = dateTime.AddHours(0.5);
            Simple instance = new Simple(ScheduleType.Hourly, 0.5) { StartTime = dateTime.TimeOfDay, StopTime = dateTime.AddHours(2).TimeOfDay };

            // Action
            DateTime result = instance.GetNextRun(DateTime.Now);

            // Post-Conditions
            Helper.Validate(expected, result, string.Empty);
        }

        [Test]
        public void GetNextRunForHourlyWithTimeWindowStopTest()
        {
            // Pre-Conditions 
            DateTime dateTime = DateTime.Now.AddHours(-2);
            DateTime expected = dateTime.AddDays(1).AddHours(0.5);
            Simple instance = new Simple(ScheduleType.Hourly, 0.5) { StartTime = dateTime.TimeOfDay, StopTime = dateTime.AddHours(1).TimeOfDay };

            // Action
            DateTime result = instance.GetNextRun(DateTime.Now);

            // Post-Conditions
            Helper.Validate(expected, result, string.Empty);
        }

        [Test]
        public void GetNextRunForHourlyWithWrongTimeWindowStopTest()
        {
            // Pre-Conditions 
            DateTime dateTime = DateTime.Now;
            DateTime expected = DateTime.MaxValue;
            Simple instance = new Simple(ScheduleType.Hourly, 1) { StartTime = dateTime.TimeOfDay, StopTime = dateTime.AddHours(0.5).TimeOfDay };

            // Action
            DateTime result = instance.GetNextRun(DateTime.Now);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }
        #endregion

        #endregion
    }
}