#region Using directives

using System;
using Allag.Core.TestData;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools.Job.IntervalProviders
{
    [TestFixture]
    public class MonthlyTests
    {
        #region Variables
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        #region Tests

        [Test]
        public void CtorWrongDayLessZeroTest()
        {
            // Action
            Assert.That(() => new Monthly(-1, NumberOfWeek.Second, System.DayOfWeek.Monday, Month.All, 1, true), Throws.InstanceOf<ArgumentOutOfRangeException>());
        }

        [Test]
        public void CtorWrongDayGreater31Test()
        {
            // Action
            Assert.That(() => new Monthly(32, NumberOfWeek.Second, System.DayOfWeek.Monday, Month.All, 1, true), Throws.InstanceOf<ArgumentOutOfRangeException>());
        }

        [Test]
        public void CtorWrongEveryNotIntegerTest()
        {
            // Action
            Assert.That(() => new Monthly(1, NumberOfWeek.Second, System.DayOfWeek.Monday, Month.All, 1.5, true), Throws.ArgumentException);
        }

        [Test]
        public void GetNextRunUseMonthWeekTestTest()
        {
            // Pre-Conditions 
            NumberOfWeek[] numberOfWeeks = new[] {
                NumberOfWeek.First, NumberOfWeek.Second, NumberOfWeek.Third, 
                NumberOfWeek.Fourth, NumberOfWeek.Last
            };
            DateTime[] dtLastRuns = new[] { 
                new DateTime(2009, 01, 05), new DateTime(2009, 01, 05), new DateTime(2009, 01, 12),
                new DateTime(2009, 01, 19), new DateTime(2009, 01, 26)
            };
            DateTime[] dtExpecteds = new[] {
                new DateTime(2009, 06, 01), new DateTime(2009, 02, 02), new DateTime(2009, 02, 09),
                new DateTime(2009, 02, 16), new DateTime(2009, 02, 23)
            };

            // Action
            for (int i = 0; i < numberOfWeeks.Length; i++)
            {
                TestUseMonthWeek(numberOfWeeks[i], dtLastRuns[i], dtExpecteds[i]);
            }
        }

        [Test]
        public void GetNextRunUseMonthTest()
        {
            // Pre-Conditions
            Month[] months = new Month[] {
                Month.January, Month.February, Month.March, 
                Month.April, Month.May, Month.June, 
                Month.July, Month.August, Month.September, 
                Month.October, Month.November, Month.December
            };
            DateTime[] dtLastRuns = new DateTime[] { 
                new DateTime(2009, 01, 05), new DateTime(2009, 02, 02), new DateTime(2009, 03, 02), 
                new DateTime(2009, 04, 06), new DateTime(2009, 05, 04), new DateTime(2009, 06, 08),
                new DateTime(2009, 07, 06), new DateTime(2009, 08, 03), new DateTime(2009, 09, 07), 
                new DateTime(2009, 10, 05), new DateTime(2009, 11, 02), new DateTime(2009, 12, 07) 
            };
            DateTime[] dtExpecteds = new DateTime[] {
                new DateTime(2010, 01, 04), new DateTime(2010, 02, 08), new DateTime(2010, 03, 08),
                new DateTime(2010, 04, 05), new DateTime(2010, 05, 03), new DateTime(2010, 06, 07),
                new DateTime(2010, 07, 05), new DateTime(2010, 08, 02), new DateTime(2010, 09, 06), 
                new DateTime(2010, 10, 04), new DateTime(2010, 11, 08), new DateTime(2010, 12, 06)
            };

            // Action
            for (int i = 0; i < months.Length; i++)
            {
                TestUseMonth(months[i], dtLastRuns[i], dtExpecteds[i]);
            }
        }

        [Test]
        public void GetNextRunUseMonthFirstTimeTest()
        {
            // Pre-Conditions
            DateTime dtStart = DateTime.Now;
            DateTime expected = new DateTime(2017, 01, 02, dtStart.Hour, dtStart.Minute, dtStart.Second);
            
            // Action
            TestUseMonth(Month.January, DateTime.MinValue, dtStart, expected);
        }

        [Test]
        public void GetNextRunEveryTest()
        {
            // Action
            TestEvery(1, 2, new DateTime(2009, 01, 01), new DateTime(2009, 03, 01));
        }

        [Test]
        public void GetNextRunEvery31Test()
        {
            // Action
            TestEvery(31, 1, new DateTime(2009, 01, 01), new DateTime(2009, 03, 31));
        }

        [Test]
        public void GetNextRunEveryFirstTimeTest()
        {
            // Pre-Conditions
            const int nDay = 15;
            DateTime dtStart = DateTime.Now.AddSeconds(10);
            DateTime expected = new DateTime(dtStart.Year, dtStart.Month, nDay, dtStart.Hour, dtStart.Minute, dtStart.Second);
            if (dtStart.Day > nDay)
            {
                expected = expected.AddMonths(1);
            }
            
            // Action
            TestEvery(nDay, 1, DateTime.MinValue, dtStart, expected);
        }

        [Test]
        public void GetNextRunNotExistDayTest()
        {
            // Action
            TestEvery(31, 12, new DateTime(2009, 02, 01), new DateTime(9999, 02, 01));
        }

        [Test]
        public void GetNextRunCertainDayOfMonth()
        {
            // Pre-Conditions
            DateTime lastRun = DateTime.Now;
            lastRun = lastRun.AddDays(15 - lastRun.Day);
            DateTime expected = lastRun.AddMonths(1);
            Monthly instance = new Monthly(lastRun.Day, NumberOfWeek.First, System.DayOfWeek.Monday, Month.All, 1, true) {StartTime = lastRun.TimeOfDay};

            // Action
            DateTime result = instance.GetNextRun(lastRun);
            
            // Post-Conditions
            Helper.Validate(expected, result, " for day of the month");
        }

        [Test]
        public void GetNextRunNotExistDayOfMonth()
        {
            // Pre-Conditions
            DateTime lastRun = DateTime.Now;
            const int expected = 9999;
            Monthly instance = new Monthly(30, NumberOfWeek.First, System.DayOfWeek.Monday, Month.February, 1, true) {StartTime = lastRun.TimeOfDay};

            // Action
            DateTime result = instance.GetNextRun(lastRun);

            // Post-Conditions
            Assert.AreEqual(expected, result.Year);
        }

        #endregion

        #region Methods
        private static void TestUseMonthWeek(NumberOfWeek numberOfWeek, DateTime lastRun, DateTime expected)
        {
            // Pre-Conditions
            Monthly instance = CreateMonthly(Month.All, numberOfWeek);
            DateTime dtStart = lastRun;
            instance.StartTime = dtStart.TimeOfDay;
           
            // Action
            DateTime result = instance.GetNextRun(lastRun);

            // Post-Conditions
            Helper.Validate(expected, result, " for week '" + numberOfWeek + "'");
        }

        private static void TestUseMonth(Month month, DateTime lastRun, DateTime expected)
        {
            TestUseMonth(month, lastRun, lastRun, expected);
        }

        private static void TestUseMonth(Month month, DateTime lastRun, DateTime start, DateTime expected)
        {
            // Pre-Conditions
            Monthly instance = CreateMonthly(month, NumberOfWeek.Second);
            instance.StartTime = start.TimeOfDay;

            // Action
            DateTime result = instance.GetNextRun(lastRun);

            // Post-Conditions
            Helper.Validate(expected, result, " for month '" + month + "'");
        }

        private static void TestEvery(int day, int every, DateTime lastRun, DateTime expected)
        {
            TestEvery(day, every, lastRun, lastRun, expected);
        }

        private static void TestEvery(int day, int every, DateTime lastRun, DateTime start, DateTime expected)
        {
            // Pre-Conditions
            Monthly instance = CreateMonthly(day, every);
            instance.StartTime = start.TimeOfDay;

            // Action
            DateTime result = instance.GetNextRun(lastRun);

            // Post-Conditions
            Helper.Validate(expected, result, string.Empty);
        }

        private static Monthly CreateMonthly(Month months, NumberOfWeek numberOfWeek)
        {
            return new Monthly(0, numberOfWeek, System.DayOfWeek.Monday, months, 1, true);
        }

        private static Monthly CreateMonthly(int day, int every)
        {
            return new Monthly(day, NumberOfWeek.First, System.DayOfWeek.Monday, Month.August, every, false);
        }

        #endregion
    }
}
