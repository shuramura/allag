﻿#region Using directives
using System;
using System.IO;
using System.Threading;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Tools.Job.IntervalProviders
{
    [TestFixture]
    public class FileSystemTests
    {
        #region Constants
        private const string FOLDER = @"TestData\Tools\Job\IntervalProviders";
        #endregion

        #region Variables
        private string _strPath;
        private MockRepository _mocks;
        private IJob _job;
        private FileSystem _instance;
        #endregion

        #region Initialization 
        [SetUp]
        public void Init()
        {
            _strPath = Path.Combine(FOLDER, Guid.NewGuid().ToString("N"));
            _mocks = new MockRepository();
            _job = _mocks.DynamicMock<IJob>();
            _instance = new FileSystem(_strPath, "*.tmp");
        }

        [TearDown]
        public void CleanUp()
        {
            _instance.Dispose();
            if (Directory.Exists(_strPath))
            {
                Directory.Delete(_strPath, true);
            }
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Post-Conditions
            Assert.IsTrue(Directory.Exists(_strPath), "The folder is not created.");
            Assert.AreEqual(NotifyFilters.LastWrite, _instance.Filter, "The property 'Filter' is wrong.");
            Assert.AreEqual(8192, _instance.EventBuffer, "The property 'EventBuffer' is wrong.");
            Assert.IsFalse(_instance.Subdirectories, "The property 'Subdirectories' is wrong.");
        }

        [Test]
        public void Ctor_WrongPath_Test([Values(null, "", "  ")] string path)
        {
            // Action
            Assert.That(() => new FileSystem(path, ""), Throws.InstanceOf<ArgumentNullOrEmptyException>());
        }

        [Test]
        public void Filter_Set_Test()
        {
            // Pre-Conditions
            const NotifyFilters expected = NotifyFilters.Size;

            // Action
            _instance.Filter = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Filter);
        }

        [Test]
        public void EventBuffer_Set_Test()
        {
            // Pre-Conditions
            const int expected = 4096;

            // Action
            _instance.EventBuffer = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.EventBuffer);
        }

        [Test]
        public void Subdirectories_Set_Test()
        {
            // Pre-Conditions
            bool expected = !_instance.Subdirectories;

            // Action
            _instance.Subdirectories = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Subdirectories);
        }

        [Test]
        public void Start_Test()
        {
            // Pre-Conditions
            int nCount = 0;
            _instance.Add(_job, jobs => nCount++);
            _mocks.ReplayAll();

            // Action
            _instance.Start(_job);

            // Post-Conditions
            ModifyFile(CreateNewFile());
            Thread.Sleep(600);
            Assert.AreEqual(1, nCount);
        }

        [Test]
        public void Start_RunImmediately_Test()
        {
            // Pre-Conditions
            int nCount = 0;
            _instance.RunImmediately = true;
            _instance.Add(_job, jobs => nCount++);
            _mocks.ReplayAll();

            // Action
            _instance.Start(_job);

            // Post-Conditions
            Thread.Sleep(1000);
            Assert.AreEqual(1, nCount);
        }

        [Test]
        public void Start_OutOfTimeWindow_Test()
        {
            // Pre-Conditions
            int nCount = 0;
            _instance.Add(_job, jobs => nCount++);
            _mocks.ReplayAll();
            _instance.StartTime = DateTime.Now.AddMilliseconds(500).TimeOfDay;
            _instance.StopTime = DateTime.Now.AddMilliseconds(1000).TimeOfDay;

            // Action
            _instance.Start(_job);

            // Post-Conditions
            ModifyFile(CreateNewFile());
            Thread.Sleep(100);
            Assert.AreEqual(0, nCount);
        }

        [Test]
        public void Start_InTimeWindow_Test()
        {
            // Pre-Conditions
            int nCount = 0;
            _instance.Add(_job, jobs => nCount++);
            _mocks.ReplayAll();
            _instance.StartTime = DateTime.Now.AddMilliseconds(500).TimeOfDay;
            _instance.StopTime = DateTime.Now.AddMilliseconds(1000).TimeOfDay;

            // Action
            _instance.Start(_job);

            // Post-Conditions
            ModifyFile(CreateNewFile());
            Thread.Sleep(1200);
            Assert.AreEqual(1, nCount);
        }


        [Test]
        public void Stop_Test()
        {
            // Pre-Conditions
            int nCount = 0;
            _instance.Add(_job, jobs => nCount++);
            _mocks.ReplayAll();
            _instance.Start(_job);
            string strFile = CreateNewFile();
            Thread.Sleep(600);

            // Action
            _instance.Stop(_job);

            // Post-Conditions
            ModifyFile(strFile);

            Assert.AreEqual(1, nCount);
        }

        #endregion

        #region Methods
        private string CreateNewFile(string extension = "tmp")
        {
            string strFile = Path.Combine(_strPath, Guid.NewGuid().ToString("N") + "." + extension);
            using (StreamWriter writer = File.CreateText(strFile))
            {
                writer.WriteLine("test");
            }
            return strFile;
        }
        #endregion

        #region Static methods
        private static void ModifyFile(string file)
        {
            using (FileStream stream = File.OpenWrite(file))
            {
                stream.Write(new byte[] { 0, 1, 2, 3, 4 }, 0, 5);
            }
        }
        #endregion


    }
}