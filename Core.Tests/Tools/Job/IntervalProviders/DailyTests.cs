#region Using directives
using System;
using Allag.Core.TestData;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools.Job.IntervalProviders
{
    [TestFixture]
    public class DailyTests
    {
        #region Variables
        private Daily _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new Daily(DayOfWeek.All, 1, true);
        }

        [TearDown]
        public void TearDown() {}
        #endregion

        #region Tests
        [Test]
        public void GetNextRunUseDayOfWeekFirstTimeLessNowTest()
        {
            // Pre-Conditions
            DateTime dateTime = DateTime.Now.AddMinutes(-10);
            _instance.StartTime = dateTime.TimeOfDay;
            DateTime expected = dateTime.AddDays(1);
            
            // Action
            DateTime result = _instance.GetNextRun(DateTime.MinValue);

            // Post-Conditions
            Helper.Validate(expected, result, string.Empty);
        }

        [Test]
        public void GetNextRunUseDayOfWeekFirstTimeGreaterNowTest()
        {
            // Pre-Conditions
            DateTime dateTime = DateTime.Now.AddMinutes(10);
            _instance.StartTime = dateTime.TimeOfDay;
            DateTime expected = dateTime;
           
            // Action
            DateTime result = _instance.GetNextRun(DateTime.MinValue);

            // Post-Conditions
            Helper.Validate(expected, result, string.Empty);
        }

        [Test]
        [Sequential]
        public void GetNextRun_UseDayOfWeek_Test([Values(DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday,
                                                     DayOfWeek.Thursday, DayOfWeek.Friday, DayOfWeek.Saturday, DayOfWeek.Sunday)] DayOfWeek dayOfWeek,
        [Values(1, 2, 3, 4, 5, 6, 7)] int dayNumber)
        {
            // Pre-Conditions
            Daily instance = new Daily(dayOfWeek, 1, true);
            DateTime dtLastRun = new DateTime(2009, 01, 25, 23, 59, 59).AddDays(dayNumber);
            DateTime dtStart = DateTime.Now.AddMinutes(-10);
            DateTime expected = dtLastRun.AddDays(7);
            expected = new DateTime(expected.Year, expected.Month, expected.Day, dtStart.Hour, dtStart.Minute, dtStart.Second);
            instance.StartTime = dtStart.TimeOfDay;

            // Action
            DateTime result = instance.GetNextRun(dtLastRun);

            // Post-Conditions
            Helper.Validate(expected, result, string.Empty);
        }

        [Test]
        public void GetNextRunUseDayOfWeekTest()
        {
            // Pre-Conditions
            DateTime dtLastRun = DateTime.Now;
            DateTime dtStart = dtLastRun.AddMinutes(-10);
            DateTime expected = dtStart.AddDays(1);
            _instance.StartTime = dtStart.TimeOfDay;

            // Action
            DateTime result = _instance.GetNextRun(dtLastRun);

            // Post-Conditions
            Helper.Validate(expected, result, string.Empty);
        }

        [Test]
        public void GetNextRunNotUseDayOfWeekFirstTimeLessNowTest()
        {
            // Pre-Conditions
            Daily instance = new Daily(DayOfWeek.Sunday, 1, false);
            DateTime dtStart = DateTime.Now.AddMinutes(-10);
            DateTime expected = dtStart.AddDays(1);
            instance.StartTime = dtStart.TimeOfDay;

            // Action
            DateTime result = instance.GetNextRun(DateTime.MinValue);

            // Post-Conditions
            Helper.Validate(expected, result, string.Empty);
        }

        [Test]
        public void GetNextRunNotUseDayOfWeekFirstTimeGreaterNowTest()
        {
            // Pre-Conditions
            Daily instance = new Daily(DayOfWeek.Sunday, 1, false);
            DateTime dtStart = DateTime.Now.AddMinutes(10);
            DateTime expected = dtStart;
            instance.StartTime = dtStart.TimeOfDay;

            // Action
            DateTime result = instance.GetNextRun(DateTime.MinValue);

            // Post-Conditions
            Helper.Validate(expected, result, string.Empty);
        }

        [Test]
        public void GetNextRunNotUseDayOfWeekTest()
        {
            // Pre-Conditions
            Daily instance = new Daily(DayOfWeek.Sunday, 2.5, false);
            DateTime dtLastRun = DateTime.Now;
            DateTime dtStart = dtLastRun.AddMinutes(10);
            DateTime expected = dtLastRun.AddDays(2.5);
            instance.StartTime = dtStart.TimeOfDay;

            // Action
            DateTime result = instance.GetNextRun(dtLastRun);

            // Post-Conditions
            Helper.Validate(expected, result, string.Empty);
        }
        #endregion

        #region Methods
        #endregion
    }
}