﻿#region Using directives
using System;
using System.Threading;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Tools.Job.IntervalProviders
{
    [TestFixture]
    public class BaseTimeIntervalProviderTests
    {
        #region Variables
        private MockRepository _mocks;
        private BaseTimeIntervalProvider _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _mocks = new MockRepository();
            _instance = _mocks.PartialMock<BaseTimeIntervalProvider>(ScheduleType.BySecond, 10);
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_WrongFrequency_Test()
        {
            // Action
            Assert.That(() => _mocks.PartialMock<BaseTimeIntervalProvider>(ScheduleType.BySecond, 0), Throws.Exception.With.InnerException.TypeOf<ArgumentException>());
        }

        [Test]
        public void Ctor_Test()
        {
            // Post-Conditions
            Assert.AreEqual(ScheduleType.BySecond, _instance.ScheduleType, "The property 'ScheduleType' is wrong.");
            Assert.AreEqual(10, _instance.Every, "The property 'Every' is wrong.");
            Assert.IsFalse(_instance.RunImmediately, "The property 'RunImmediately' is wrong.");
        }

        [Test]
        public void Start_WrongJob_Test()
        {
            // Pre-Conditions
            IJob job = _mocks.DynamicMock<IJob>();
            IJob otherJob = _mocks.DynamicMock<IJob>();
            _mocks.ReplayAll();
            _instance.Add(job, jobs => { });

            // Action
            Assert.That(() => _instance.Start(otherJob), Throws.ArgumentException);
        }

        [Test]
        public void Start_Test()
        {
            // Pre-Conditions
            BaseTimeIntervalProvider instance = _mocks.PartialMock<BaseTimeIntervalProvider>(ScheduleType.BySecond, 10);

            int nInvokationCount = 0;
            Tuple<IJob, Action<IJob[]>> jobInfo = CreateJob(instance, x => nInvokationCount++);
            instance.Expect(x => x.GetNextRun(DateTime.MinValue)).
                      WhenCalled(invocation => { invocation.ReturnValue = DateTime.Now.AddMilliseconds(500); }).Return(DateTime.MinValue);
            _mocks.ReplayAll();
            instance.Add(jobInfo.Item1, jobInfo.Item2);

            // Action
            instance.Start(jobInfo.Item1);

            // Post-Conditions
            Thread.Sleep(1500);
            Assert.AreEqual(2, nInvokationCount, "The action was invoked wrong times.");
        }

        [Test]
        public void Start_Immediately_Test()
        {
            // Pre-Conditions
            int nInvokationCount = 0;
            _instance.RunImmediately = true;
            Tuple<IJob, Action<IJob[]>> jobInfo = CreateJob(_instance, x => nInvokationCount++);
            _mocks.ReplayAll();
            _instance.Add(jobInfo.Item1, jobInfo.Item2);

            // Action
            _instance.Start(jobInfo.Item1);

            // Post-Conditions
            Thread.Sleep(1500);
            Assert.AreEqual(3, nInvokationCount, "The action was invoked wrong times.");
        }

        [Test]
        public void Stop_Test()
        {
            // Pre-Conditions
            int nInvokationCount = 0;
            _instance.RunImmediately = true;
            Tuple<IJob, Action<IJob[]>> jobInfo = CreateJob(_instance, x => nInvokationCount++);
            _mocks.ReplayAll();
            _instance.Add(jobInfo.Item1, jobInfo.Item2);
            _instance.Start(jobInfo.Item1);
            Thread.Sleep(1000);

            // Action
            _instance.Stop(jobInfo.Item1);

            // Post-Conditions
            Thread.Sleep(500);
            Assert.AreEqual(2, nInvokationCount, "The action was invoked wrong times.");
        }

        [Test]
        public void Stop_WrongJob_Test()
        {
            // Pre-Conditions
            IJob job = _mocks.DynamicMock<IJob>();
            IJob otherJob = _mocks.DynamicMock<IJob>();
            _mocks.ReplayAll();
            _instance.Add(job, jobs => { });

            // Action
            Assert.That(() => _instance.Stop(otherJob), Throws.ArgumentException);
        }

        [Test]
        public void Dispose_Test()
        {
            // Pre-Conditions
            _instance.RunImmediately = true;
            Tuple<IJob, Action<IJob[]>> jobInfo = CreateJob(_instance);
            _mocks.ReplayAll();
            _instance.Add(jobInfo.Item1, jobInfo.Item2);
            _instance.Start(jobInfo.Item1);
           
            // Action
            _instance.Dispose();

            // Post-Conditions
            Assert.That(() => _instance.Stop(jobInfo.Item1), Throws.InstanceOf<ObjectDisposedException>());
        }
        #endregion

        #region Methods
        private Tuple<IJob, Action<IJob[]>> CreateJob(BaseTimeIntervalProvider instance, Action<IJob[]> action = null)
        {
            DateTime startTime = DateTime.Now;
            
            IJob job = _mocks.DynamicMock<IJob>();
            Action<IJob[]> internalAction = jobs =>
                {
                    if (action != null)
                    {
                        action(jobs);
                    }
                    CollectionAssert.AreEqual(new IJob[]{job}, jobs, "The job is wrong.");
                };

            instance.Expect(x => x.GetNextRun(Arg<DateTime>.Is.GreaterThanOrEqual(startTime))).
                      WhenCalled(invocation => { invocation.ReturnValue = DateTime.Now.AddMilliseconds(500); }).Return(DateTime.MinValue);
            _mocks.Replay(job);
            return new Tuple<IJob, Action<IJob[]>>(job, internalAction);
        }
        #endregion
    }
}