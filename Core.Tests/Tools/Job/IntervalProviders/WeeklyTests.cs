#region Using directives

using System;
using Allag.Core.TestData;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools.Job.IntervalProviders
{
    [TestFixture]
    public class WeeklyTests
    {
        #region Variables
        private Weekly _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new Weekly(DateTime.Now.DayOfWeek, 1);
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        #region Tests
        [Test]
        public void GetNextRunFirstTimeLessNowTest()
        {
            // Pre-Conditions
            DateTime dtStart = DateTime.Now.AddMinutes(-10);
            DateTime expected = dtStart.AddDays(7);
            _instance.StartTime = dtStart.TimeOfDay;

            // Action
            DateTime result = _instance.GetNextRun(DateTime.MinValue);

            // Post-Conditions
            Helper.Validate(expected, result, string.Empty);
        }

        [Test]
        public void GetNextRunFirstTimeGreaterNowTest()
        {
            // Pre-Conditions
            DateTime dtStart = DateTime.Now.AddMinutes(10);
            DateTime expected = dtStart;
            _instance.StartTime = dtStart.TimeOfDay;

            // Action
            DateTime result = _instance.GetNextRun(DateTime.MinValue);

            // Post-Conditions
            Helper.Validate(expected, result, string.Empty);
        }

        [Test]
        public void GetNextRunTest()
        {
            // Pre-Conditions
            Weekly instance = new Weekly(System.DayOfWeek.Monday, 0.5);
            DateTime dtLastRun = new DateTime(2009, 01, 26, 10, 10, 10);
            DateTime dtStart = dtLastRun;
            DateTime expected = dtLastRun.AddDays(3.5);
            instance.StartTime = dtStart.TimeOfDay;

            // Action
            DateTime result = instance.GetNextRun(dtLastRun);

            // Post-Conditions
            Helper.Validate(expected, result, string.Empty);
        }
        #endregion

        #region Methods

        #endregion
    }
}
