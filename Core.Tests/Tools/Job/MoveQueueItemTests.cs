﻿#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools.Job
{
    [TestFixture]
    public class MoveQueueItemTests
    {
        #region Variables
        private MoveQueueItem _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _instance = new MoveQueueItem();
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Post-Conditions
            Assert.IsNull(_instance.Id, "The property 'Id' is wrong.");
            CollectionAssert.AreEqual(new QueueItem[0], _instance.Items, "The property 'Items' is wrong.");
        }

        [Test]
        public void Add_WrongQueueId_Test()
        {
            // Action
            Assert.Throws<ArgumentException>(() => _instance.Add(Guid.Empty, "id"));
        }

        [Test]
        public void Add_WrongId_Test([Values(null, "", "  ")] string id)
        {
            // Action
            Assert.Throws<ArgumentNullOrEmptyException>(() => _instance.Add(Guid.NewGuid(), id));
        }

        [Test]
        public void Add_Test()
        {
            // Pre-Conditions
            Guid queueId = Guid.NewGuid();
            const string strId = "id";

            // Action
            QueueItem result = _instance.Add(queueId, strId);

            // Post-Conditions
            Assert.IsNotNull(result, "The result is wrong.");
            Assert.AreEqual(queueId, result.QueueId, "The property 'QueueId' is wrong.");
            Assert.AreEqual(strId, result.Id, "The property 'Id' is wrong.");
        }
        #endregion
    }
}