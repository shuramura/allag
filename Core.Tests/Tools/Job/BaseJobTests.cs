#region Using directives
using System;
using System.Threading;
using Allag.Core.TestData.Tools.Job;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Tools.Job
{
    [TestFixture]
    public class BaseJobTests
    {
        #region Variables
        private MockRepository _mocks;
        private IScheduler _scheduler;
        private IJobEventListener _eventListener;
        private CancellationTokenSource _cancellationTokenSource;
        private MockBaseJob _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _mocks = new MockRepository();
            _scheduler = _mocks.StrictMock<IScheduler>();
            _eventListener = _mocks.StrictMock<IJobEventListener>();
            _cancellationTokenSource = new CancellationTokenSource();

            _instance = new MockBaseJob();
        }

        [TearDown]
        public void TearDown()
        {
            MockBaseJob.Exception = null;
          
            MockBaseJob.PreInitialization = null;
            MockBaseJob.Executing = null;
            MockBaseJob.PostFinalizing = null;
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(string.Empty, _instance.Name, "The property 'Name' is wrong.");
            Assert.IsNull(_instance.Description, "The property 'Description' is wrong.");
        }

        [Test]
        public void Name_SetNullValue_Test()
        {
            // Action
            Assert.That(() => _instance.Name = null, Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void Name_Set_Test()
        {
            // Pre-Conditions 
            const string expected = "lalala";

            // Action
            _instance.Name = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Name);
        }

        [Test]
        public void Description_Set_Test()
        {
            // Pre-Conditions 
            const string expected = "lalala";

            // Action
            _instance.Description = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Description);
        }

        [Test]
        public void Run_Test()
        {
            // Pre-Conditions
            int nPreExecuting = 0;
            int nExecuting = 0;
            int nPostFinalizing = 0;

            MockBaseJob.PreInitialization = delegate { nPreExecuting++; };
            MockBaseJob.Executing = delegate { nExecuting++; };
            MockBaseJob.PostFinalizing = delegate { nPostFinalizing++; };
            _eventListener.Expect(x => x.Initialization(Arg<MockBaseJob.ExecutionResult>.Is.NotNull, Arg<ExecutionData>.Is.NotNull)).Throw(new Exception());
            _eventListener.Expect(x => x.Finalizing(Arg<MockBaseJob.ExecutionResult>.Is.NotNull, Arg<ExecutionData>.Is.NotNull));
             _instance.AddListener(_eventListener);
            _mocks.ReplayAll();

            // Action
            ((IJob)_instance).Run(_scheduler, _cancellationTokenSource.Token);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(1, nPreExecuting, "The pre-executing is wrong.");
            Assert.AreEqual(1, nExecuting, "The executing is wrong.");
            Assert.AreEqual(1, nPostFinalizing, "The post-finalizing is wrong.");
        }

        [Test]
        public void Run_WithException_Test()
        {
            // Pre-Conditions
            int nPreExecuting = 0;
            int nExecuting = 0;
            int nPostFinalizing = 0;

            MockBaseJob.Exception = new ArgumentException();

            MockBaseJob.PreInitialization = delegate { nPreExecuting++; };
            MockBaseJob.Executing = delegate { nExecuting++; };
            MockBaseJob.PostFinalizing = delegate { nPostFinalizing++; };
            _mocks.ReplayAll();

            // Action
            object result = ((IJob)_instance).Run(_scheduler, _cancellationTokenSource.Token);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.IsInstanceOf<MockBaseJob.ExecutionResult>(result, "The result is wrong.");
            Assert.AreEqual(1, nPreExecuting, "The pre-executing is wrong.");
            Assert.AreEqual(0, nExecuting, "The executing is wrong.");
            Assert.AreEqual(1, nPostFinalizing, "The post-finalizing is wrong.");
        }

        [Test]
        public void Run_WithWarning_Test()
        {
            // Pre-Conditions
            int nPreExecuting = 0;
            int nExecuting = 0;
            int nPostFinalizing = 0;

            MockBaseJob.PreInitialization = delegate { nPreExecuting++; };
            MockBaseJob.Executing = delegate { nExecuting++; };
            MockBaseJob.PostFinalizing = delegate
            {
                nPostFinalizing++;
                _cancellationTokenSource.Cancel();
            };

            _mocks.ReplayAll();

            // Action
            ((IJob)_instance).Run(_scheduler, _cancellationTokenSource.Token);

            // Post-Conditions
            Assert.AreEqual(1, nPreExecuting, "The pre-executing is wrong.");
            Assert.AreEqual(1, nExecuting, "The executing is wrong.");
            Assert.AreEqual(1, nPostFinalizing, "The post-finalizing is wrong.");
     }

        [Test]
        public void Dispose_InvokeDisposed_Test()
        {
            // Pre-Conditions 
            
            _instance.Dispose();

            // Action
            Assert.That(() => ((IJob)_instance).Run(_scheduler, _cancellationTokenSource.Token), Throws.InstanceOf<ObjectDisposedException>());
        }


        [Test]
        public void AddListener_NullListener_Test()
        {
            // Action
            Assert.That(() => _instance.AddListener(null), Throws.InstanceOf<ArgumentNullException>());
        }
        #endregion
    }
}