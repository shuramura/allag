﻿#region Using directives
using System;
using System.Linq;
using System.Threading;
using Allag.Core.TestData.Tools.Job;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Tools.Job
{
    [TestFixture]
    public class BaseItemJobTests
    {
        #region Variables
        private MockRepository _mocks;
        private IScheduler _scheduler;
        private IJobEventListener _listener;
        private MockBaseItemJob _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _mocks = new MockRepository();
            _scheduler = _mocks.StrictMock<IScheduler>();
            _listener = _mocks.DynamicMock<IJobEventListener>();

            _instance = new MockBaseItemJob();
        }

        #endregion

        #region Tests

        [Test]
        public void Ctor_Test()
        {
            // Post-Conditions
            Assert.IsFalse(_instance.RunDependatsPerItem, "The property 'RunDependatsPerItem' is wrong.");
        }

        [Test]
        public void Run_Test()
        {
            // Pre-Conditions
            CancellationTokenSource source = new CancellationTokenSource();
            int nCount = 0;
            MockBaseItemJob.GetItemsAction = (() => new string[] { "1", "2", "3" });
            MockBaseItemJob.ExecuteAction = (() =>
                {
                    nCount++;
                    if (nCount == 2)
                    {
                        source.Cancel();
                    }
                });
            _mocks.ReplayAll();

            // Action
            ((IJob)_instance).Run(_scheduler, source.Token);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(2, nCount);
        }

        [Test]
        public void Run_RunDependatsPerItem_Test()
        {
            // Pre-Conditions
            _instance.RunDependatsPerItem = true;
            CancellationTokenSource source = new CancellationTokenSource();
            int nCount = 0;
            string[] items = new string[] {"1", "2", "3"};
            MockBaseItemJob.GetItemsAction = (() => items);
            MockBaseItemJob.ExecuteAction = (() =>
            {
                nCount++;
                if (nCount == 2)
                {
                    source.Cancel();
                }
            });
            _scheduler.Expect(x => x.RunDependants(_instance)).Repeat.Times(items.Length - 1);
            _mocks.ReplayAll();

            // Action
            ((IJob)_instance).Run(_scheduler, source.Token);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(2, nCount);
        }

        [Test]
        public void Run_ZeroItems_Test()
        {
            // Pre-Conditions
            CancellationTokenSource source = new CancellationTokenSource();
            int nCount = 0;
            MockBaseItemJob.GetItemsAction = (() => new string[0]);
            MockBaseItemJob.ExecuteAction = (() =>
            {
                nCount++;
            });
            _mocks.ReplayAll();

            // Action
            ((IJob)_instance).Run(_scheduler, source.Token);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(0, nCount);
        }

        [Test]
        public void Run_WithException_Test()
        {
            // Pre-Conditions
            CancellationTokenSource source = new CancellationTokenSource();
            const string strMessage = "Test message.";
            MockBaseItemJob.ExecuteAction = (() =>
            {
                throw new Exception("Test message.");
            });
            _listener.Expect(x => x.Finalizing(Arg<MockBaseItemJob.Result>.Is.NotNull, Arg<ExecutionData>.Is.NotNull))
                .WhenCalled(invocation =>
                {
                    ExecutionData data = (ExecutionData)invocation.Arguments[1];
                    Assert.AreEqual(1, data.Warnings.Count(), "The warning count is wrong.");
                    StringAssert.Contains(strMessage, data.Warnings.First().Item1, "The warning is wrong.");
                });

            _mocks.ReplayAll();
            _instance.AddListener(_listener);

            // Action
            ((IJob)_instance).Run(_scheduler, source.Token);

            // Post-Conditions
            _mocks.VerifyAll();
        }

        #endregion

    }
}