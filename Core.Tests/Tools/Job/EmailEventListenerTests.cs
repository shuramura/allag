﻿#region Using directives
using System;
using System.Threading;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Tools.Job
{
    [TestFixture]
    public class EmailEventListenerTests
    {
        #region Variables
        private EmailEventListener _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _instance = new EmailEventListener(EmailHelperTests.EmailName)
            {
                Helper = {Subject = new MemoryResource("<?xml version='1.0' encoding='UTF-8' ?><xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' xmlns:msxsl='urn:schemas-microsoft-com:xslt'><xsl:output method='text' encoding='utf-8'/><xsl:template match='/'><xsl:value-of select='//result/Int'/></xsl:template></xsl:stylesheet>") {Type = ResourceType.Xslt}}
            };
        }

        [TearDown]
        public void CleanUp()
        {
            EmailHelperTests.ClearEmailFolder();
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Post-Conditions
            Assert.AreEqual(Priority.All, _instance.Priority, "The property 'Priority' is wrong.");
            Assert.IsNotNull(_instance.Helper, "The property 'Helper' is wrong.");
        }

        [Test]
        public void Ctor_NullEmailConfiguration_Test()
        {
            // Action
            Assert.That(() => new EmailEventListener(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void Initialization_Test()
        {
            // Action
            _instance.Initialization<Result>(null, null);

            // Post-Conditions
            EmailHelperTests.Email[] emails = EmailHelperTests.GetEmails();
            Assert.AreEqual(0, emails.Length, "The email count is wrong.");
        }

        [Test]
        public void Finalizing_NullResult_Test()
        {
            // Action
            Assert.That(() => _instance.Finalizing<Result>(null, new ExecutionData(MockRepository.GenerateStub<IScheduler>(), MockRepository.GenerateStub<IJob>(), new CancellationToken())), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void Finalizing_NullData_Test()
        {
            // Action
            Assert.That(() => _instance.Finalizing(new Result(), null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void Finalizing_Test()
        {
            // Pre-Conditions
            ExecutionData data = new ExecutionData(MockRepository.GenerateStub<IScheduler>(), MockRepository.GenerateStub<IJob>(), new CancellationToken());

            // Action
            _instance.Finalizing(new Result {Int = 123}, data);

            // Post-Conditions
            EmailHelperTests.Email[] emails = EmailHelperTests.GetEmails();
            Assert.AreEqual(1, emails.Length, "The result is wrong.");
            Assert.AreEqual("lalala@lala.com", emails[0].To, "The to address is wrong.");
            Assert.AreEqual("123", emails[0].Subject, "The subject is wrong.");
            Assert.AreEqual("non-urgent", emails[0].Priority, "The priority is wrong.");
            Assert.AreEqual("low", emails[0].Importance, "The importance is wrong.");
        }

        [Test]
        public void Finalizing_WithWarnings_Test()
        {
            // Pre-Conditions
            ExecutionData data = new ExecutionData(MockRepository.GenerateStub<IScheduler>(), MockRepository.GenerateStub<IJob>(), new CancellationToken());
            data.AddWarning("Test");

            // Action
            _instance.Finalizing(new Result {Int = 123}, data);

            // Post-Conditions
            EmailHelperTests.Email[] emails = EmailHelperTests.GetEmails();
            Assert.AreEqual(1, emails.Length, "The result is wrong.");
            Assert.AreEqual("lalala@lala.com", emails[0].To, "The to address is wrong.");
            Assert.AreEqual("123", emails[0].Subject, "The subject is wrong.");
            Assert.AreEqual(string.Empty, emails[0].Priority, "The priority is wrong.");
            Assert.AreEqual(string.Empty, emails[0].Importance, "The importance is wrong.");
        }

        [Test]
        public void Finalizing_WithException_Test()
        {
            // Pre-Conditions
            ExecutionData data = new ExecutionData(MockRepository.GenerateStub<IScheduler>(), MockRepository.GenerateStub<IJob>(), new CancellationToken())
            {
                Exception = new Exception("Test")
            };

            // Action
            _instance.Finalizing(new Result {Int = 123}, data);

            // Post-Conditions
            EmailHelperTests.Email[] emails = EmailHelperTests.GetEmails();
            Assert.AreEqual(1, emails.Length, "The result is wrong.");
            Assert.AreEqual("lalala@lala.com", emails[0].To, "The to address is wrong.");
            Assert.AreEqual("123", emails[0].Subject, "The subject is wrong.");
            Assert.AreEqual("urgent", emails[0].Priority, "The priority is wrong.");
            Assert.AreEqual("high", emails[0].Importance, "The importance is wrong.");
        }
        #endregion

        #region Classes
        public class Result
        {
            #region Properties
            public int Int { get; set; }
            #endregion
        }
        #endregion
    }
}