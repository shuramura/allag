#region Using directives
using System;
using System.Threading;
using Allag.Core.TestData.Tools.Job;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools.Job
{
    [TestFixture]
    public class JobFactoryTests
    {
        #region Initialization
        [TearDown]
        public void TearDown()
        {
            JobFactory.Stop(TimeSpan.Zero);
        }
        #endregion

        #region Tests
        [Test]
        public void InitializeTest()
        {
            // Post-Conditions
            Assert.AreEqual(5, JobFactory.Scheduler.Count);
        }

        [Test]
        public void StartTest()
        {
            // Pre-Conditions 
            int result = 0;
            TestJob.Executing = delegate { result++; };

            // Action
            JobFactory.Start();

            // Post-Conditions
            Thread.Sleep(5000);
            Assert.GreaterOrEqual(result, 4);
        }

        [Test]
        public void StopTest()
        {
            // Pre-Conditions 
            int result = 0;
            TestJob.Executing = delegate { result++; };
            JobFactory.Start();
            Thread.Sleep(5000);

            // Action
            JobFactory.Stop(TimeSpan.Zero);

            // Post-Conditions
            Thread.Sleep(3000);
            Assert.Greater(result, 0);
            Assert.LessOrEqual(result, 6);
        }

        [Test]
        public void StopInfinityWaitTest()
        {
            // Pre-Conditions 
            int result = 0;
            TestJob.Executing = delegate
            {
                while (result < 5)
                {
                    Thread.Sleep(1000);
                    result++;
                }
            };
            JobFactory.Start();
            Thread.Sleep(1000);

            // Action
            JobFactory.Stop(new TimeSpan(0, 0, 0, 0, -1));

            // Post-Conditions
            Assert.AreEqual(5, result);
        }

        [Test]
        public void Stop_WithWaiting_Test()
        {
            // Pre-Conditions 
            int result = 0;
            TestJob.Executing = delegate
                                     {
                                         while (true)
                                         {
                                             Thread.Sleep(1000);
                                             result++;
                                         }
                                     };
            JobFactory.Start();
            Thread.Sleep(1000);

            // Action
            JobFactory.Stop(new TimeSpan(0, 0, 5));

            // Post-Conditions
            Assert.Greater(result, 0);
            Assert.LessOrEqual(result, 11);
        }

        [Test]
        public void StopWithoutWaitingTest()
        {
            // Pre-Conditions 
            int result = 0;
            TestJob.Executing = delegate
            {
                while (true)
                {
                    Thread.Sleep(2000);
                    result++;
                }
            };
            JobFactory.Start();
            Thread.Sleep(1000);

            // Action
            JobFactory.Stop(TimeSpan.Zero);

            // Post-Conditions
            Assert.AreEqual(0, result);
        }
        #endregion
    }
}