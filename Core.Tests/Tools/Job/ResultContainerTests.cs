﻿#region Using directives
using System;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Tools.Job
{
    [TestFixture]
    public class ResultContainerTests
    {
        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Action
            ResultContainer<ResultContainerTests> instance = new ResultContainer<ResultContainerTests>();

            // Post-Conditions
            Assert.IsNull(instance.Name, "The property 'JobName' is wrong.");
            Assert.IsNull(instance.Description, "The property 'Description' is wrong.");
            Assert.AreEqual(DateTime.MinValue, instance.StartTime, "The property 'StartTime' is wrong.");
            Assert.AreEqual(DateTime.MinValue, instance.EndTime, "The property 'EndTime' is wrong.");
            Assert.IsNull(instance.Result, "The property 'Result' is wrong.");
            Assert.AreEqual(0, instance.Warnings.Count(), "The property 'Warnings' is wrong.");
            Assert.AreEqual(0, instance.Errors.Count(), "The property 'Errors' is wrong.");
        }

        [Test]
        public void Ctor_WithParameters_Test()
        {
            // Pre-Conditions
            IJob job = MockRepository.GenerateStub<IJob>();
            job.Name = "Name";
            job.Stub(x => x.Description).Return("Description");
            string[] errors = new string[] {"Error0", "Error1"};
            ExecutionData data = new ExecutionData(MockRepository.GenerateStub<IScheduler>(), job, new CancellationToken());
            data.AddWarning("Warning0");
            data.AddWarning("Warning1");
            data.Exception = new Exception(errors[0], new Exception(errors[1]));

            // Action
            ResultContainer<ResultContainerTests> instance = new ResultContainer<ResultContainerTests>(this, data);

            // Post-Conditions
            Assert.AreEqual(job.Name, instance.Name, "The property 'JobName' is wrong.");
            Assert.AreEqual(job.Description, instance.Description, "The property 'Description' is wrong.");
            Assert.AreEqual(data.StartTime, instance.StartTime, "The property 'StartTime' is wrong.");
            Assert.AreEqual(data.EndTime, instance.EndTime, "The property 'EndTime' is wrong.");
            Assert.AreEqual(this, instance.Result, "The property 'Result' is wrong.");
            Assert.AreEqual(2, instance.Warnings.Length, "The property 'Warnings' is wrong.");
            Assert.AreEqual(2, instance.Errors.Length, "The property 'Errors' is wrong.");
        }

        [Test]
        public void Ctor_NullResult_Test()
        {
            // Action
            Assert.That(() => new ResultContainer<ResultContainerTests>(null, new ExecutionData(MockRepository.GenerateStub<IScheduler>(), MockRepository.GenerateStub<IJob>(), new CancellationToken())), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void Ctor_NullExcecutionData_Test()
        {
            // Action
            Assert.That(() => new ResultContainer<ResultContainerTests>(this, null), Throws.InstanceOf<ArgumentNullException>());
        }
        #endregion
    }
}