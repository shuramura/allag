﻿#region Using directives
using System.IO;
using System.Threading;
using Allag.Core.Net;
using Allag.Core.Tools.Archives;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Tools.Job
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO.Compression;

    [TestFixture]
    public class DownloadFileTests
    {
        #region Constants
        private const string DOWNLOAD_FOLDER = "Download";
        #endregion

        #region Variables
        private CancellationTokenSource _cancellationTokenSource;
        private FileManager _fileManager;
        private DownloadFile _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _cancellationTokenSource = new CancellationTokenSource();
            _instance = new DownloadFile("file://downloadFileTest")
            {
                SourceFolder = @"Download\Source",
                ArchiveFolder = @"Download\Archive",
                ErrorFolder = @"Download\Error",
                RemoteFolder = DOWNLOAD_FOLDER
            };

            _fileManager = _instance.FileManager;
            _fileManager.Upload(@".\TestData\Tools\Job", _instance.RemoteFolder, "ValidZip.zip");
            _fileManager.Upload(@".\TestData\Tools\Job", _instance.RemoteFolder, "1.zip");
        }

        [TearDown]
        public void TearDown()
        {
            _instance.Dispose();
            DeletFiles();

            if (Directory.Exists(".\\Download"))
            {
                Directory.Delete(".\\Download", true);
            }
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Action
            DownloadFile instance = new DownloadFile(null);

            // Post-Conditions
            Assert.IsNull(instance.SourceFolder, "The property 'SourceFolder' is wrong.");
            Assert.IsNull(instance.ArchiveFolder, "The property 'ArchiveFolder' is wrong.");
            Assert.IsNull(instance.ErrorFolder, "The property 'ErrorFolder' is wrong.");
            Assert.IsNotNull(instance.FileManager, "The property 'FileManager' is wrong.");
            Assert.AreEqual(0, instance.FileManager.Clients.Count, "The count of property 'FileManager' is wrong.");
            Assert.AreEqual(string.Empty, instance.RemoteFolder, "The property 'RemoteFolder' is wrong.");
            Assert.AreEqual(".+", instance.FileTemplate, "The property 'FileTemplate' is wrong.");
            Assert.IsNull(instance.FolderTemplate, "The property 'FolderTemplate' is wrong.");
            Assert.IsTrue(instance.Unarchive, "The property 'Unarchive' is wrong.");
            Assert.AreEqual("unarchived", instance.UnarchivedExtension, "The property 'UnarchivedExtension' is wrong.");
        }

        [Test]
        public void ArchiveFile_Set_Test()
        {
            // Pre-Conditions
            IArchiveFile archiveFile = MockRepository.GenerateStub<IArchiveFile>();

            // Action
            _instance.ArchiveFile = archiveFile;

            // Post-Conditions
            Assert.That(_instance.ArchiveFile == archiveFile, Is.True);
        }

        [Test]
        public void Run_Test()
        {
            // Pre-Conditions
            _fileManager.DeleteFile(_instance.RemoteFolder, "1.zip");

            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            CheckFolders(1, 0, 1);
        }

        [Test]
        public void Run_RemoteFolderIsNotSpecified_Test()
        {
            // Pre-Conditions
            _instance.RemoteFolder = null;

            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            CheckFolders(0, 0, 0);
        }

        [Test]
        public void Run_WithoutFileOnFtp_Test()
        {
            // Pre-Conditions
            DeletFiles();

            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            CheckFolders(0, 0, 0);
        }

        [Test]
        public void Run_DuplicatedFilesAllowDuplication_Test()
        {
            // Pre-Conditions
            _instance.AllowDuplication = true;
            _fileManager.Download(_instance.RemoteFolder, _instance.SourceFolder, _instance.FileTemplate, false);

            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            CheckFolders(2, 2, 2);
        }

        [Test]
        public void Run_DuplicatedFilesNotAllowDuplication_Test()
        {
            // Pre-Conditions
            _instance.AllowDuplication = false;
            _fileManager.Download(_instance.RemoteFolder, _instance.SourceFolder, _instance.FileTemplate, false);

            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            CheckFolders(1, 3, 1);
        }

        [Test]
        public void RunWithoutArchiveFilderTest()
        {
            // Pre-Conditions
            _instance.ArchiveFolder = null;

            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            CheckFolders(0, 1, 1);
        }

        [Test]
        public void RunUnspecifiedFileClientTest()
        {
            // Pre-Conditions
            _instance.FileManager.Clients.Clear();

            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            CheckFolders(0, 0, 0);
        }

        [Test]
        public void Run_UnspecifiedFtpFolder_Test()
        {
            // Pre-Conditions
            _instance.SourceFolder = null;

            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            CheckFolders(0, 0, 0);
        }

        [Test]
        public void RunUnspecifiedFileTemplateTest()
        {
            // Pre-Conditions
            _instance.FileTemplate = null;

            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            CheckFolders(0, 0, 0);
        }

        [Test]
        public void Run_ArchiveDuplicatedFilesWithoutAllowDuplication_Test()
        {
            // Pre-Conditions
            _instance.AllowDuplication = false;
            _fileManager.Download(_instance.RemoteFolder, _instance.ArchiveFolder, _instance.FileTemplate, false);

            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            CheckFolders(3, 1, 1);
        }

        [Test]
        public void Run_NotZipFiles_Test()
        {
            // Pre-Conditions
            _instance.Unarchive = false;

            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            CheckFolders(2, 0, 0);
        }

        [Test]
        public void Run_NotZipArchiveDuplicatedFileWithoutAllowDuplication_Test()
        {
            // Pre-Conditions
            _instance.AllowDuplication = false;
            _instance.Unarchive = false;
            _fileManager.Download(_instance.RemoteFolder, _instance.ArchiveFolder, _instance.FileTemplate, false);

            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            CheckFolders(4, 0, 0);
        }
        #endregion

        #region Methods
        private void DeletFiles()
        {
            foreach (IItem ftpItem in _fileManager.GetDirectoryList(DOWNLOAD_FOLDER, ".*"))
            {
                if (ftpItem.Type == ItemType.File)
                {
                    _fileManager.DeleteFile(DOWNLOAD_FOLDER, ftpItem.FullName);
                }
            }
        }

        private void CheckFolders(int archiveFileCount, int errorFileCount, int downloadedFileCount)
        {
            CheckFolders(_instance, archiveFileCount, errorFileCount, downloadedFileCount);
        }

        private static void CheckFolders(DownloadFile instance, int archiveFileCount, int errorFileCount, int downloadedFileCount)
        {
            if (instance.ArchiveFolder != null)
            {
                Assert.AreEqual(archiveFileCount, Directory.GetFiles(instance.ArchiveFolder, "*.*").Length, "The archive files are wrong.");
            }
            if (instance.ErrorFolder != null)
            {
                Assert.AreEqual(errorFileCount, Directory.GetFiles(instance.ErrorFolder, "*.*").Length, "The error files are wrong.");
            }
            if (instance.SourceFolder != null)
            {
                Assert.AreEqual(downloadedFileCount, Directory.GetFileSystemEntries(instance.SourceFolder, "*.*").Length, "The downloaded files are wrong.");
            }
        }
        #endregion
    }
}