#region Using directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Tools.Job
{
    [TestFixture]
    public class SchedulerTests
    {
        #region Variables
        private MockRepository _mocks;
        private IIntervalProvider _intervalProvider;
        private IJob _job;
        private Action<IJob[]> _action;
        private Scheduler _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _mocks = new MockRepository();
            _job = _mocks.StrictMock<IJob>();
            _job.Stub(x => x.Name).Return("Name");

            _intervalProvider = _mocks.StrictMock<IIntervalProvider>();
            _intervalProvider.Stub(x => x.Add(Arg<IJob>.Is.Equal(_job), Arg<Action<IJob[]>>.Is.NotNull)).WhenCalled(invocation => { _action = (Action<IJob[]>)invocation.Arguments[1]; });

            _instance = new Scheduler();
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Post-Conditions
            Assert.AreEqual(0, _instance.Count, "The property 'Count' is wrong.");
        }

        [Test]
        public void AddJob_Test()
        {
            // Pre-Conditions 
            _mocks.ReplayAll();

            // Action
            _instance.AddJob(_job, _intervalProvider);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(1, _instance.Count);
        }

        [Test]
        public void AddJob_WithNullIntervalProvider_Test()
        {
            // Pre-Conditions 
            _mocks.ReplayAll();

            // Action
            _instance.AddJob(_job);

            // Post-Conditions
            Assert.AreEqual(1, _instance.Count);
        }

        [Test]
        public void AddJob_WithNullJob_Test()
        {
            // Pre-Conditions 
            _mocks.ReplayAll();

            // Action
            Assert.That(() => _instance.AddJob(null, _intervalProvider), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void Dispose_InvokeDisposed_Test()
        {
            // Pre-Conditions 
            _instance.Dispose();
            _mocks.ReplayAll();

            // Action
            Assert.That(() => _instance.AddJob(_job, _intervalProvider), Throws.InstanceOf<ObjectDisposedException>());
        }

        [Test]
        public void Start_Test()
        {
            // Pre-Conditions 
            _intervalProvider.Expect(x => x.Start(_job)).WhenCalled(invocation => _action(new IJob[] {_job}));
            _job.Expect(x => x.Run(Arg<IScheduler>.Is.Equal(_instance), Arg<CancellationToken>.Is.NotNull));
            _intervalProvider.Expect(x => x.Stop(_job));

            _mocks.ReplayAll();

            _instance.AddJob(_job, _intervalProvider);

            // Action
            _instance.Start();

            // Post-Conditions
            Thread.Sleep(100);
            _instance.Stop(TimeSpan.Zero);
            _mocks.VerifyAll();
        }

        [Test]
        public void Start_WithException_Test()
        {
            // Pre-Conditions 
            _intervalProvider.Expect(x => x.Start(_job)).WhenCalled(invocation => _action(new IJob[] {_job}));
            _job.Expect(x => x.Run(Arg<IScheduler>.Is.Equal(_instance), Arg<CancellationToken>.Is.NotNull)).Throw(new NotImplementedException());

            _mocks.ReplayAll();

            _instance.AddJob(_job, _intervalProvider);

            // Action
            _instance.Start();

            // Post-Conditions
            Thread.Sleep(500);
            _mocks.VerifyAll();
        }

        [Test]
        public void Start_WithDependentJob_Test()
        {
            // Pre-Conditions 
            IJob dependentJob = _mocks.Stub<IJob>();
            dependentJob.Name = "dependent job";

            DateTime dependentDateTime = DateTime.MinValue;
            dependentJob.Expect(x => x.Run(Arg<IScheduler>.Is.Equal(_instance), Arg<CancellationToken>.Is.NotNull))
                .WhenCalled(invocation => { dependentDateTime = DateTime.Now; });

            DateTime dateTime = DateTime.MinValue;
            _intervalProvider.Expect(x => x.Start(_job)).WhenCalled(invocation => _action(new IJob[] {_job}));
            _job.Expect(x => x.Run(Arg<IScheduler>.Is.Equal(_instance), Arg<CancellationToken>.Is.NotNull)).WhenCalled(invocation =>
            {
                dateTime = DateTime.Now;
                Thread.Sleep(500);
            });

            _mocks.ReplayAll();

            _instance.AddJob(dependentJob);

            _instance.AddJob(_job, _intervalProvider, new[] {dependentJob.Name});

            // Action
            _instance.Start();

            // Post-Conditions
            Thread.Sleep(1000);
            _mocks.VerifyAll();
            Assert.Less(dateTime, dependentDateTime, "The post DateTime is wrong.");
        }

        [Test]
        public void Start_WithRunStopping_Test()
        {
            // Pre-Conditions 
            _intervalProvider.Expect(x => x.Start(_job)).WhenCalled(invocation => _action(new IJob[] {_job}));
            _job.Expect(x => x.Run(Arg<IScheduler>.Is.Equal(_instance), Arg<CancellationToken>.Is.NotNull));
            _intervalProvider.Expect(x => x.Stop(_job));

            _mocks.ReplayAll();

            _instance.AddJob(_job, _intervalProvider);

            // Action
            _instance.Start();

            // Post-Conditions
            Thread.Sleep(500);
            _mocks.ReplayAll();
        }

        [Test]
        public void GetEnumerator_General_Test()
        {
            // Pre-Conditions
            _mocks.ReplayAll();
            _instance.AddJob(_job);

            // Action
            IEnumerator result = ((IEnumerable)_instance).GetEnumerator();

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.IsNotNull(result, "The result is wrong.");
            result.MoveNext();
            Assert.AreEqual(_job, result.Current, "The first element is wrong.");
        }

        [Test]
        public void GetEnumerator_Test()
        {
            // Pre-Conditions
            _mocks.ReplayAll();
            _instance.AddJob(_job);

            // Action
            IEnumerator<IJob> result = _instance.GetEnumerator();

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.IsNotNull(result, "The result is wrong.");
            result.MoveNext();
            Assert.AreEqual(_job, result.Current, "The first element is wrong.");
        }

        [Test]
        public void This_Get_Test()
        {
            // Pre-Conditions
            _mocks.ReplayAll();
            _instance.AddJob(_job);

            // Action
            IJob result = _instance[_job.Name];

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(_job, result);
        }

        [Test]
        public void Dispose_Test()
        {
            // Pre-Conditions
            _job.Expect(x => x.Dispose());
            _intervalProvider.Expect(x => x.Dispose());

            _mocks.ReplayAll();
            _instance.AddJob(_job, _intervalProvider);

            // Action
            _instance.Dispose();

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(0, _instance.Count);
        }

        [Test]
        public void Dispose_Started_Test()
        {
            // Pre-Conditions
            _intervalProvider.Expect(x => x.Start(_job));
            _job.Expect(x => x.Dispose());
            _intervalProvider.Expect(x => x.Stop(_job));
            _intervalProvider.Expect(x => x.Dispose());

            _mocks.ReplayAll();
            _instance.AddJob(_job, _intervalProvider);
            _instance.Start();

            // Action
            _instance.Dispose();

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(0, _instance.Count);
        }

        [Test]
        public void RunDependants_NullJob_Test()
        {
            // Action
            Assert.That(() => _instance.RunDependants(null), Throws.InstanceOf<ArgumentNullException>());
        }
        #endregion
    }
}