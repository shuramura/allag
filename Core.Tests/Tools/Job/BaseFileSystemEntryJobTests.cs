﻿#region Using directives
using System;
using System.IO;
using System.Threading;
using Allag.Core.TestData.Tools.Job;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Tools.Job
{
    [TestFixture]
    public class BaseFileSystemEntryJobTests
    {
        #region Constants
        private const string ROOT_FOLDER = @".\TestData\Tools\Job\BaseFileSystemEntryJobTest";
        #endregion

        #region Variables
        private CancellationTokenSource _cancellationTokenSource;
        private MockFileSystemEntry _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _cancellationTokenSource = new CancellationTokenSource();
            _instance = new MockFileSystemEntry();
        }

        [TearDown]
        public void TearDown()
        {
            if (Directory.Exists(ROOT_FOLDER))
            {
                Thread.Sleep(500);
                Directory.Delete(ROOT_FOLDER, true);
            }
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Action
            // Post-Conditions
            Assert.IsNull(_instance.ArchiveFolder, "The property 'ArchiveFolder' is wrong.");
            Assert.IsNull(_instance.ErrorFolder, "The property 'ErrorFolder' is wrong.");
            Assert.IsNull(_instance.SourceFolder, "The property 'SourceFolder' is wrong.");
            Assert.IsNull(_instance.ProcessingFolder, "The property 'ProcessingFolder' is wrong.");
            Assert.AreEqual(".+", _instance.FileTemplate, "The property 'FileTemplate' is wrong.");
            Assert.AreEqual(".+", _instance.FolderTemplate, "The property 'FolderTemplate' is wrong.");
        }

        [Test]
        public void Run_Test()
        {
            // Pre-Conditions
            SetFolders();
            AddFolder(_instance.ProcessingFolder, "1");
            AddFolder(_instance.SourceFolder, "1");

            AddFile(_instance.SourceFolder, "2");
            AddFile(_instance.ProcessingFolder, "2");
            AddFile(_instance.ProcessingFolder);

            int nFileCount = 0;
            int nFolderCount = 0;
            _instance.FileProcessingAction = info => { nFileCount++; };
            _instance.FolderProcessingAction = info => { nFolderCount++; };
        
            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            Assert.IsTrue(Directory.Exists(_instance.ArchiveFolder), "The property 'ArchiveFolder' is wrong.");
            Assert.IsTrue(Directory.Exists(_instance.ErrorFolder), "The property 'ErrorFolder' is wrong.");
            Assert.IsTrue(Directory.Exists(_instance.SourceFolder), "The property 'SourceFolder' is wrong.");
            Assert.IsTrue(Directory.Exists(_instance.ProcessingFolder), "The property 'ProcessingFolder' is wrong.");

            Assert.AreEqual(0, Directory.GetFileSystemEntries(_instance.ProcessingFolder).Length, "The final processing folder is wrong.");
            Assert.AreEqual(0, Directory.GetFileSystemEntries(_instance.SourceFolder).Length, "The final source folder is wrong.");
            Assert.AreEqual(5, Directory.GetFileSystemEntries(_instance.ArchiveFolder).Length, "The final archive folder is wrong.");
            Assert.AreEqual(0, Directory.GetFileSystemEntries(_instance.ErrorFolder).Length, "The final error folder is wrong.");

            Assert.AreEqual(2, nFolderCount, "The count of processed folder is wrong.");
            Assert.AreEqual(3, nFileCount, "The count of processed files is wrong.");
        }

        [Test]
        public void Run_WithError_Test()
        {
            // Pre-Conditions
            SetFolders();
            AddFolder(_instance.ProcessingFolder);
            AddFolder(_instance.SourceFolder, "1");

            AddFile(_instance.SourceFolder);
            AddFile(_instance.ProcessingFolder, "1");

            int nFileCount = 0;
            int nFolderCount = 0;
            _instance.FileProcessingAction = info =>
                {
                    nFileCount++;
                    if (nFileCount == 1)
                    {
                        throw new Exception();
                    }
                };
            _instance.FolderProcessingAction = info =>
                {
                    nFolderCount++;
                    if (nFolderCount == 2)
                    {
                        throw new Exception();
                    }
                };
          
            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            Assert.AreEqual(0, Directory.GetFileSystemEntries(_instance.ProcessingFolder).Length, "The final processing folder is wrong.");
            Assert.AreEqual(0, Directory.GetFileSystemEntries(_instance.SourceFolder).Length, "The final source folder is wrong.");
            Assert.AreEqual(2, Directory.GetFileSystemEntries(_instance.ArchiveFolder).Length, "The final archive folder is wrong.");
            Assert.AreEqual(2, Directory.GetFileSystemEntries(_instance.ErrorFolder).Length, "The final error folder is wrong.");

            Assert.AreEqual(2, nFolderCount, "The count of processed folder is wrong.");
            Assert.AreEqual(2, nFileCount, "The count of processed files is wrong.");
        }

        [Test]
        public void Run_WithoutArchiveFolder_Test()
        {
            // Pre-Conditions
            SetFolders();
            _instance.ArchiveFolder = null;
            
            AddFolder(_instance.ProcessingFolder, "1");
            AddFolder(_instance.SourceFolder, "1");

            AddFile(_instance.SourceFolder, "2");
            AddFile(_instance.ProcessingFolder, "2");
            AddFile(_instance.ProcessingFolder);

            int nFileCount = 0;
            int nFolderCount = 0;
            _instance.FileProcessingAction = info => { nFileCount++; };
            _instance.FolderProcessingAction = info => { nFolderCount++; };
           
            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            Assert.AreEqual(0, Directory.GetFileSystemEntries(_instance.ProcessingFolder).Length, "The final processing folder is wrong.");
            Assert.AreEqual(0, Directory.GetFileSystemEntries(_instance.SourceFolder).Length, "The final source folder is wrong.");
            Assert.AreEqual(0, Directory.GetFileSystemEntries(_instance.ErrorFolder).Length, "The final error folder is wrong.");

            Assert.AreEqual(2, nFolderCount, "The count of processed folder is wrong.");
            Assert.AreEqual(3, nFileCount, "The count of processed files is wrong.");
        }

        [Test]
        public void Run_WithErrorWithoutErrorFolder_Test()
        {
            // Pre-Conditions
            SetFolders();
            _instance.ErrorFolder = null;
            AddFolder(_instance.ProcessingFolder);
            AddFolder(_instance.SourceFolder, "1");

            AddFile(_instance.SourceFolder);
            AddFile(_instance.ProcessingFolder, "1");

            int nFileCount = 0;
            int nFolderCount = 0;
            _instance.FileProcessingAction = info =>
            {
                nFileCount++;
                if (nFileCount == 1)
                {
                    throw new Exception();
                }
            };
            _instance.FolderProcessingAction = info =>
            {
                nFolderCount++;
                if (nFolderCount == 2)
                {
                    throw new Exception();
                }
            };
           
            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            Assert.AreEqual(0, Directory.GetFileSystemEntries(_instance.ProcessingFolder).Length, "The final processing folder is wrong.");
            Assert.AreEqual(0, Directory.GetFileSystemEntries(_instance.SourceFolder).Length, "The final source folder is wrong.");
            Assert.AreEqual(2, Directory.GetFileSystemEntries(_instance.ArchiveFolder).Length, "The final archive folder is wrong.");
           
            Assert.AreEqual(2, nFolderCount, "The count of processed folder is wrong.");
            Assert.AreEqual(2, nFileCount, "The count of processed files is wrong.");
        }

        [Test]
        public void Run_WrongSourceFolder_Test()
        {
            // Pre-Conditions
            SetFolders();
            _instance.SourceFolder = null;

            AddFolder(_instance.ProcessingFolder, "1");
            AddFile(_instance.ProcessingFolder, "2");
            AddFile(_instance.ProcessingFolder);

            int nFileCount = 0;
            int nFolderCount = 0;
            _instance.FileProcessingAction = info => { nFileCount++; };
            _instance.FolderProcessingAction = info => { nFolderCount++; };
           
            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            Assert.IsTrue(Directory.Exists(_instance.ArchiveFolder), "The property 'ArchiveFolder' is wrong.");
            Assert.IsTrue(Directory.Exists(_instance.ErrorFolder), "The property 'ErrorFolder' is wrong.");
            Assert.IsTrue(Directory.Exists(_instance.ProcessingFolder), "The property 'ProcessingFolder' is wrong.");

            Assert.AreEqual(3, Directory.GetFileSystemEntries(_instance.ProcessingFolder).Length, "The final processing folder is wrong.");
            Assert.AreEqual(0, Directory.GetFileSystemEntries(_instance.ArchiveFolder).Length, "The final archive folder is wrong.");
            Assert.AreEqual(0, Directory.GetFileSystemEntries(_instance.ErrorFolder).Length, "The final error folder is wrong.");

            Assert.AreEqual(0, nFolderCount, "The count of processed folder is wrong.");
            Assert.AreEqual(0, nFileCount, "The count of processed files is wrong.");
        }

        [Test]
        public void Run_WithInvalidProcessingFolder_Test()
        {
            // Pre-Conditions
            SetFolders();
            _instance.ProcessingFolder = @"y:\";

            AddFolder(_instance.SourceFolder);
            AddFile(_instance.SourceFolder);

            int nFileCount = 0;
            int nFolderCount = 0;
            _instance.FileProcessingAction = info => { nFileCount++; };
            _instance.FolderProcessingAction = info => { nFolderCount++; };

            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            Assert.AreEqual(0, nFolderCount, "The count of processed folder is wrong.");
            Assert.AreEqual(0, nFileCount, "The count of processed files is wrong.");
        }
        #endregion

        #region Methods
        private void SetFolders()
        {
            _instance.ErrorFolder = Path.Combine(ROOT_FOLDER, "Error");
            _instance.ArchiveFolder = Path.Combine(ROOT_FOLDER, "Archive");
            _instance.SourceFolder = Path.Combine(ROOT_FOLDER, "Source");
            _instance.ProcessingFolder = Path.Combine(ROOT_FOLDER, "Processing");
        }
        #endregion

        #region Static methods
        private static void AddFolder(string parent, string name = null)
        {
            if (name == null)
            {
                name = Guid.NewGuid().ToString("N");
            }
            Directory.CreateDirectory(Path.Combine(parent, name));
        }

        private static void AddFile(string parent, string name = null)
        {
            if (name == null)
            {
                name = Guid.NewGuid().ToString("N");
            }
            using(StreamWriter writer = File.CreateText(Path.Combine(parent, name)))
            {
                writer.WriteLine(name);
            }
        }
        #endregion
    }
}