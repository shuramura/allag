﻿#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools.Job
{
    [TestFixture]
    public class MoveQueueResultTests
    {
        #region Variables
        private MoveQueueResult _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _instance = new MoveQueueResult();
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Post-Conditions
            Assert.AreEqual(Guid.Empty, _instance.QueueId, "The property 'QueueId   ' is wrong.");
            CollectionAssert.AreEqual(new MoveQueueItem[0], _instance.Items, "The property 'Items' is wrong.");
        }

        [Test]
        public void Get_WrongOriginalId_Test([Values(null, "", "  ")] string originalId)
        {
            // Action
            Assert.Throws<ArgumentNullOrEmptyException>(() => _instance.Get(originalId));
        }

        [Test]
        public void Get_Test()
        {
            // Pre-Conditions
            const string strId = "id";

            // Action
            MoveQueueItem result = _instance.Get(strId);

            // Post-Conditions
            Assert.IsNotNull(result, "The result is wrong.");
            Assert.AreEqual(strId, result.Id, "The property 'Id' is wrong.");
        }

        [Test]
        public void Get_Exists_Test()
        {
            // Pre-Conditions
            const string strId = "id";
            MoveQueueItem item = _instance.Get(strId);

            // Action
            MoveQueueItem result = _instance.Get(strId);

            // Post-Conditions
            Assert.AreEqual(item, result);
        }
        #endregion
    }
}