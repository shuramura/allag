﻿#region Using directives
using System;
using System.IO;
using System.Linq;
using System.Threading;
using Allag.Core.Net;
using Allag.Core.Tools.Archives;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Tools.Job
{
    [TestFixture]
    public class UploadFileTests
    {
        #region Constants
        private const string UPLOAD_FOLDER = "UploadFile";
        #endregion

        #region Variables
        private CancellationTokenSource _cancellationTokenSource;
        private FileManager _fileManager;
        private UploadFile _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _cancellationTokenSource = new CancellationTokenSource();
            _instance = new UploadFile("file://uploadFileTests")
                {
                    SourceFolder = @"UploadFile\Source",
                    ArchiveFolder = @"UploadFile\Archive",
                    ErrorFolder = @"UploadFile\Error",
                    RemoteFolder = UPLOAD_FOLDER
                };

            Directory.CreateDirectory(_instance.SourceFolder);
            _fileManager = _instance.FileManager;
        }

        [TearDown]
        public void CleanUp()
        {
            _instance.Dispose();

            try
            {
                if (_instance.RemoteFolder != null)
                {
                    foreach (IItem item in _fileManager.GetDirectoryList(_instance.RemoteFolder, ".+\\.zip"))
                    {
                        if (item.Type == ItemType.File)
                        {
                            _fileManager.DeleteFile(_instance.RemoteFolder, item.FullName);
                        }
                    }
                }    
            }
            catch{}
           
            if (Directory.Exists(".\\UploadFile"))
            {
               
                Thread.Sleep(500);
                Directory.Delete(".\\UploadFile", true);
            }
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Action
            UploadFile instance = new UploadFile(null);

            // Post-Conditions
            Assert.IsNull(instance.SourceFolder, "The property 'SourceFolder' is wrong.");
            Assert.IsNull(instance.ArchiveFolder, "The property 'ArchiveFolder' is wrong.");
            Assert.IsNull(instance.ErrorFolder, "The property 'ErrorFolder' is wrong.");
            Assert.IsNotNull(instance.FileManager, "The property 'FileManager' is wrong.");
            Assert.AreEqual(0, instance.FileManager.Clients.Count, "The count of property 'FileManager' is wrong.");
            Assert.AreEqual(string.Empty, instance.RemoteFolder, "The property 'RemoteFolder' is wrong.");
            Assert.AreEqual(".+", instance.FileTemplate, "The property 'FileTemplate' is wrong.");
            Assert.AreEqual(".+", instance.FolderTemplate, "The property 'FolderTemplate' is wrong.");
            Assert.IsTrue(instance.Archive, "The property 'Archive' is wrong.");
            Assert.AreEqual("zip", instance.ArchivedExtension, "The property 'ArchivedExtension' is wrong.");
        }

        [Test]
        public void ArchiveFile_Set_Test()
        {
            // Pre-Conditions
            IArchiveFile archiveFile = MockRepository.GenerateStub<IArchiveFile>();

            // Action
            _instance.ArchiveFile = archiveFile;

            // Post-Conditions
            Assert.That(_instance.ArchiveFile == archiveFile, Is.True);
        }

        [Test]
        public void Run_Test()
        {
            // Pre-Conditions
            CreateFile();
            string strFolder = CreateFolder();
            CreateFile(CreateFolder(strFolder));
            CreateFile(strFolder);
            CreateFile(strFolder);
           
            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            CheckFolders(2, 0, 0);
            Assert.AreEqual(2, _fileManager.GetDirectoryList(_instance.RemoteFolder, ".+\\." + _instance.ArchivedExtension).Count(), "The count of uploaded file is wrong.");
        }

        [Test]
        public void Run_RemoteFolderIsNotSpecified_Test()
        {
            // Pre-Conditions
            _instance.RemoteFolder = null;
            CreateFile();
            string strFolder = CreateFolder();
            CreateFile(CreateFolder(strFolder));
            CreateFile(strFolder);
            CreateFile(strFolder);

            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            CheckFolders(0, 0, 2);
        }

        [Test]
        public void Run_UploadFolderWithDisabledArchiving_Test()
        {
            // Pre-Conditions
            _instance.Archive = false;
            CreateFile(CreateFolder());

            // Action
            ((IJob)_instance).Run(MockRepository.GenerateStub<IScheduler>(), _cancellationTokenSource.Token);

            // Post-Conditions
            CheckFolders(0, 1, 0);
        }
        #endregion

        #region Methods
        private void CreateFile(string root = null)
        {
            if (root == null)
            {
                root = _instance.SourceFolder;
            }
            using(StreamWriter writer = File.CreateText(Path.Combine(root, Guid.NewGuid().ToString("N"))))
            {
                writer.WriteLine(Guid.NewGuid());
            }
        }

        private string CreateFolder(string root = null)
        {
            if (root == null)
            {
                root = _instance.SourceFolder;
            }
            string strFolder = Path.Combine(root, Guid.NewGuid().ToString("N"));
            Directory.CreateDirectory(strFolder);
            return strFolder;
        }

        private void CheckFolders(int archiveItemCount, int errorItemCount, int uploadedItemCount)
        {
            CheckFolders(_instance, archiveItemCount, errorItemCount, uploadedItemCount);
        }

        private static void CheckFolders(UploadFile instance, int archiveItemCount, int errorItemCount, int uploadedFileCount)
        {
            if (instance.ArchiveFolder != null)
            {
                Assert.AreEqual(archiveItemCount, Directory.GetFileSystemEntries(instance.ArchiveFolder, "*.*").Length , "The archive files are wrong.");
            }
            if (instance.ErrorFolder != null)
            {
                Assert.AreEqual(errorItemCount, Directory.GetFileSystemEntries(instance.ErrorFolder, "*.*").Length, "The error files are wrong.");
            }
            if (instance.SourceFolder != null)
            {
                Assert.AreEqual(uploadedFileCount, Directory.GetFileSystemEntries(instance.SourceFolder, "*.*").Length, "The uploaded files are wrong.");
            }
        }
        #endregion

    }
}