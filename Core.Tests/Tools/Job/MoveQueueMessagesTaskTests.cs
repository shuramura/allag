﻿#region Using directives
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Allag.Core.Data.Queues;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Tools.Job
{
    [TestFixture]
    public class MoveQueueMessagesTaskTests
    {
        #region Variables
        private MockRepository _mocks;
        private IQueue _source;
        private IQueue _destination;
        private MoveQueueMessagesTask _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _mocks = new MockRepository();
            _source = _mocks.StrictMock<IQueue>();
            _destination = _mocks.StrictMock<IQueue>();

            _instance = new MoveQueueMessagesTask(_source, _destination, null);
        }

        [TearDown]
        public void CleanUp()
        {
            _mocks.BackToRecordAll();
            _source.Stub(x => x.Dispose());
            _destination.Stub(x => x.Dispose());
            _mocks.ReplayAll();
            _instance.Dispose();
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Post-Conditions
            Assert.AreEqual(1000, _instance.BatchSize, "The property 'BatchSize' is wrong.");
        }

        [Test]
        public void Ctor_NullSource_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => new MoveQueueMessagesTask(null, _destination));
        }

        [Test]
        public void Ctor_NullDestinations_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => new MoveQueueMessagesTask(_source, null));
        }

        [Test]
        public void Ctor_EmptyDestinations_Test()
        {
            // Action
            Assert.Throws<ArgumentException>(() => new MoveQueueMessagesTask(_source, null, null));
        }

        [Test]
        public void Dispose_Test()
        {
            // Pre-Conditions
            _source.Expect(x => x.Dispose());
            _destination.Expect(x => x.Dispose());
            _mocks.ReplayAll();

            // Action
            _instance.Dispose();

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        [Timeout(1000)]
        public void Run_CancellTask_Test()
        {
            using(CancellationTokenSource source = new CancellationTokenSource())
            {
                // Pre-Conditions
                _instance.BatchSize = int.MaxValue;
                Guid sourceQueueId = Guid.NewGuid();
                _source.Stub(x => x.Id).Return(sourceQueueId);
                _source.Expect(x => x.ReadAsync(Arg<CancellationToken>.Is.Equal(source.Token)))
                    .WhenCalled(invocation =>
                    {
                        MemoryStream stream = new MemoryStream();
                        stream.Write(new byte[] {1, 2, 3}, 0, 3);
                        invocation.ReturnValue = Task.FromResult(new Container<Stream>(Guid.NewGuid().ToString(), stream));
                    }).Return(null).Repeat.AtLeastOnce();

                Guid destQueueId = Guid.NewGuid();
                _destination.Stub(x => x.Id).Return(destQueueId);
                _destination.Expect(x => x.IsActiveAsync(source.Token)).Return(Task.FromResult(true)).Repeat.Once();
                _destination.Expect(x => x.IsActiveAsync(source.Token)).Return(Task.FromResult(false)).Repeat.AtLeastOnce();
                _destination.Expect(x => x.WriteAsync(Arg<Stream>.Is.NotNull, Arg<CancellationToken>.Is.Equal(default(CancellationToken)))).Return(Task.FromResult(Guid.NewGuid().ToString())).Repeat.AtLeastOnce();

                IScheduler scheduler = _mocks.StrictMock<IScheduler>();
                _mocks.ReplayAll();

                // Action
                Task<object> task = Task.Run(() => ((IJob)_instance).Run(scheduler, source.Token));

                // Post-Conditions
                Thread.Sleep(500);
                source.Cancel();
                MoveQueueResult result = (MoveQueueResult)task.Result;
                _mocks.VerifyAll();

                Assert.IsNotNull(result, "The result is wrong.");
                Assert.AreEqual(sourceQueueId, result.QueueId, "The property 'QueueId' is wrong.");
            }
        }

        [Test]
        [Timeout(1000)]
        public void Run_Test()
        {
            using(CancellationTokenSource source = new CancellationTokenSource())
            {
                // Pre-Conditions
                Guid sourceQueueId = Guid.NewGuid();
                _source.Stub(x => x.Id).Return(sourceQueueId);
                _source.Expect(x => x.ReadAsync(Arg<CancellationToken>.Is.Equal(source.Token)))
                    .WhenCalled(invocation =>
                    {
                        MemoryStream stream = new MemoryStream();
                        stream.Write(new byte[] {1, 2, 3}, 0, 3);
                        invocation.ReturnValue = Task.FromResult(new Container<Stream>(Guid.NewGuid().ToString(), stream));
                    }).Return(null).Repeat.AtLeastOnce();

                Guid destQueueId = Guid.NewGuid();
                _destination.Stub(x => x.Id).Return(destQueueId);
                _destination.Expect(x => x.IsActiveAsync(source.Token)).Return(Task.FromResult(true)).Repeat.AtLeastOnce();
                _destination.Expect(x => x.WriteAsync(Arg<Stream>.Is.NotNull, Arg<CancellationToken>.Is.Equal(default(CancellationToken)))).Return(Task.FromResult(Guid.NewGuid().ToString())).Repeat.Times(_instance.BatchSize);

                IScheduler scheduler = _mocks.StrictMock<IScheduler>();
                _mocks.ReplayAll();

                // Action
                Task<object> task = Task.Run(() => ((IJob)_instance).Run(scheduler, source.Token));

                // Post-Conditions
                MoveQueueResult result = (MoveQueueResult)task.Result;
                _mocks.VerifyAll();

                Assert.IsNotNull(result, "The result is wrong.");
                Assert.AreEqual(sourceQueueId, result.QueueId, "The property 'QueueId' is wrong.");
            }
        }
        #endregion
    }
}