﻿#region Using directives
using System;
using System.IO;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Tools.Job
{
    [TestFixture]
    public class ExecutionDataTests
    {
        #region Variables
        private IScheduler _scheduler;
        private IJob _job;
        private CancellationTokenSource _cancellationTokenSource;
        private ExecutionData _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _scheduler = MockRepository.GenerateStub<IScheduler>();
            _job = MockRepository.GenerateStub<IJob>();
            _cancellationTokenSource = new CancellationTokenSource();
            _instance = new ExecutionData(_scheduler, _job, _cancellationTokenSource.Token);
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Post-Conditions
            Assert.That(_instance.Scheduler == _scheduler, Is.True, "The property 'Scheduler' is wrong.");
            Assert.AreEqual(_job, _instance.Job, "The property 'Job' is wrong.");
            Assert.Greater(_instance.StartTime, DateTime.UtcNow.AddSeconds(-1), "The property 'StartTime' is wrong.");
            Assert.AreEqual(DateTime.MinValue, _instance.EndTime, "The property 'EndTime' is wrong.");
            Assert.IsNull(_instance.Exception, "The property 'Exception' is wrong.");
            Assert.AreEqual(0, _instance.Warnings.Count(), "The property 'Warnings' is wrong.");
            Assert.AreEqual(0, _instance.Files.Count(), "The property 'Files' is wrong.");
            Assert.AreEqual(_cancellationTokenSource.Token, _instance.CancellationToken, "The property 'CancellationToken' is wrong.");
        }

        [Test]
        public void Ctor_NullScheduler_Test()
        {
            // Action
            Assert.That(() => new ExecutionData(null, _job, _cancellationTokenSource.Token), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void Ctor_NullJob_Test()
        {
            // Action
            Assert.That(() => new ExecutionData(_scheduler, null, _cancellationTokenSource.Token), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void AddWarning_Test()
        {
            // Pre-Conditions
            const string expected = "Test 1 warning.";

            // Action
            string result = _instance.AddWarning("Test {0} warning.", 1);

            // Post-Conditions
            Assert.AreEqual(expected, result, "The result is wrong.");
            Assert.AreEqual(1, _instance.Warnings.Count(), "The warning count is wrong.");
            Assert.AreEqual(expected, _instance.Warnings.First().Item1, "The warning is wrong.");
            Assert.IsNull(_instance.Warnings.First().Item2, "The warning is wrong.");
        }

        [Test]
        public void AddWarning_WithException_Test()
        {
            // Pre-Conditions
            Exception exception = new Exception("Message");

            // Action
            string result = _instance.AddWarning(exception);

            // Post-Conditions
            Assert.AreEqual(exception.Message, result, "The result is wrong.");
            Assert.AreEqual(1, _instance.Warnings.Count(), "The warning count is wrong.");
            Assert.AreEqual(exception.Message, _instance.Warnings.First().Item1, "The warning is wrong.");
            Assert.AreEqual(exception, _instance.Warnings.First().Item2, "The warning is wrong.");
        }

        [Test]
        public void AddWarning_NullException_Test()
        {
            // Action
            Assert.That(() => _instance.AddWarning((Exception)null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void AddWarning_WrongMessage_Test([Values(null, "", "  ")] string message)
        {
            // Action
            Assert.That(() => _instance.AddWarning(message), Throws.InstanceOf<ArgumentNullOrEmptyException>());
        }

        [Test]
        public void AddFileTest()
        {
            // Pre-Conditions
            using(MemoryStream stream = new MemoryStream())
            {
                // Action
                _instance.AddFile("name", stream);

                // Post-Conditions
                Assert.AreEqual(1, _instance.Files.Count(), "The file count is wrong.");
            }
        }

        [Test]
        public void Finish_Test()
        {
            // Action
            _instance.Finish();

            // Post-Conditions
            Assert.Greater(_instance.EndTime, DateTime.UtcNow.AddSeconds(-1));
        }
        #endregion
    }
}