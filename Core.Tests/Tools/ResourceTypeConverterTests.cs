﻿#region Using directives
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Core.Tools
{
    [TestFixture]
    public class ResourceTypeConverterTests : BaseTypeConverterTests<ResourceTypeConverter, IResource>
    {
        #region Variables
        private MockRepository _mocks;
        #endregion

        #region Initialization
        [SetUp]
        public override void SetUp()
        {
            _mocks = new MockRepository();
            base.SetUp();
        }
        #endregion

        #region Overrides of BaseTypeConverterTests<ResourceTypeConverter,IResource>
        [Test]
        public override void ConvertFromStringTest()
        {
            // Pre-Conditions
            const string expected = "[{location:file},{test0:test1}]path";

            // Action
            IResource result = (IResource)Instance.ConvertFrom(null, null, expected);

            // Post-Conditions
            Assert.AreEqual(expected, result.ValueWithOptions);
        }

        [Test]
        public void ConvertFromDefaultLocationTest()
        {
            // Action
            object result = Instance.ConvertFrom(null, null, "value");

            // Post-Conditions
            Assert.IsInstanceOf<MemoryResource>(result);
        }

        [Test]
        public void ConvertFromMemoryLocationTest()
        {
            // Pre-Conditions
            const string str = "[{location:memory}]value";

            // Action
            object result = Instance.ConvertFrom(null, null, str);

            // Post-Conditions
            Assert.IsInstanceOf<MemoryResource>(result);
        }

        [Test]
        public void ConvertFromFileLocationTest()
        {
            // Pre-Conditions
            const string str = "[{location:file}]value";

            // Action
            object result = Instance.ConvertFrom(null, null, str);

            // Post-Conditions
            Assert.IsInstanceOf<FileResource>(result);
        }

        [Test]
        public void ConvertFromAssemblyLocationTest()
        {
            // Pre-Conditions
            const string str = "[{location:assembly}]value";

            // Action
            object result = Instance.ConvertFrom(null, null, str);

            // Post-Conditions
            Assert.IsInstanceOf<AssemblyResource>(result);
        }

        [Test]
        public override void ConvertToTypeStringTest()
        {
            // Pre-Conditions
            IResource resource = _mocks.StrictMock<IResource>();
            Expect.Call(resource.ValueWithOptions).Return("lalala");
            _mocks.ReplayAll();

            // Action
            Instance.ConvertTo(null, null, resource, typeof(string));

            // Post-Conditions
            _mocks.VerifyAll();
        }

        protected override IResource ValueOfType
        {
            get
            {
                IResource ret = _mocks.DynamicMock<IResource>();
                _mocks.Replay(ret);
                return ret;
            }
        }
        #endregion
    }
}