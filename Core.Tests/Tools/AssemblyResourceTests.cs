﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools
{
    [TestFixture]
    public class AssemblyResourceTests
    {
        #region Variables
        private AssemblyResource _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = CreateInstance("Allag.Core.TestData.Tools.AssemblyResource.txt");
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual("54321", _instance.ToString(), "The content is wrong.");
        }

        [Test]
        public void CtorSpecifiedAssemblyTest()
        {
            // Action
            AssemblyResource instance = CreateInstance("Allag.Core.TestData.Tools.AssemblyResource.txt", "Allag.Core.Tests.dll");

            // Post-Conditions
            Assert.AreEqual("54321", instance.ToString(), "The content is wrong.");
        }

        [Test]
        public void GetStreamDefaultTest()
        {
            // Post-Conditions
            using(Stream result = _instance.GetStream())
            {
                Assert.IsTrue(result.CanRead, "The property 'CanRead' is wrong.");
                Assert.IsFalse(result.CanWrite, "The property 'CanWrite' is wrong.");
            }
        }

        [Test]
        public void GetStreamReadOnlyTest()
        {
            // Post-Conditions
            using(Stream result = _instance.GetStream(true))
            {
                Assert.IsTrue(result.CanRead, "The property 'CanRead' is wrong.");
                Assert.IsFalse(result.CanWrite, "The property 'CanWrite' is wrong.");
            }
        }

        [Test]
        public void GetStreamWritableTest()
        {
            // Post-Conditions
            Assert.That(() => _instance.GetStream(false), Throws.InstanceOf<NotSupportedException>());
        }
        #endregion

        #region Methods
        private static AssemblyResource CreateInstance(string path, string assemblyName = null)
        {
            Dictionary<string, string> settings = new Dictionary<string, string> {{ResourceTypeConverter.LocationProperty, "assembly"}};
            if(assemblyName != null)
            {
                settings.Add(AssemblyResource.AssemblyProperty, assemblyName);
            }
            return new AssemblyResource(path, settings);
        }
        #endregion
    }
}