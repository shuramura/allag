﻿#region Using directives
using System;
using Allag.Core.Configuration;
using Allag.Core.Configuration.Reflection;
using Allag.Core.TestData.Configuration;
using Allag.Core.TestData.Tools;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools
{
    [TestFixture]
    public class ConfigurableTypeConverterTests
    {
        #region Variables
        private MockConfigurableTypeConverter _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new MockConfigurableTypeConverter();
        }
        #endregion

        #region Tests
        [Test]
        public void CanConvertFromNullTest()
        {
            // Action 
            bool result = _instance.CanConvertFrom(null, null);

            // Post-Conditions
            Assert.IsFalse(result);
        }

        [Test]
        public void CanConvertFromTypeStringTest()
        {
            // Action 
            bool result = _instance.CanConvertFrom(null, typeof(string));

            // Post-Conditions
            Assert.IsTrue(result);
        }

        [Test]
        public void CanConvertFromTypeObjectTest()
        {
            // Action 
            bool result = _instance.CanConvertFrom(null, typeof(ConfigurableClass));

            // Post-Conditions
            Assert.IsTrue(result);
        }

        [Test]
        public void CanConvertFromTypeWrongTest()
        {
            // Action 
            bool result = _instance.CanConvertFrom(null, typeof(int));

            // Post-Conditions
            Assert.IsFalse(result);
        }

        [Test]
        public void CanConvertToNullTest()
        {
            // Action 
            bool result = _instance.CanConvertTo(null, null);

            // Post-Conditions
            Assert.IsFalse(result);
        }

        [Test]
        public void CanConvertToTypeStringTest()
        {
            // Action 
            bool result = _instance.CanConvertTo(null, typeof(string));

            // Post-Conditions
            Assert.IsTrue(result);
        }

        [Test]
        public void CanConvertToTypeObjectTest()
        {
            // Action 
            bool result = _instance.CanConvertTo(null, typeof(ConfigurableClass));

            // Post-Conditions
            Assert.IsTrue(result);
        }

        [Test]
        public void CanConvertToTypeWrongTest()
        {
            // Action 
            bool result = _instance.CanConvertTo(null, typeof(int));

            // Post-Conditions
            Assert.IsFalse(result);
        }

        [Test]
        public void ConvertFromNoExistConfigurationTest()
        {
            // Action
            ConfigurableClass result = (ConfigurableClass) _instance.ConvertFrom(null, null, "lalala");

            // Post-Conditions
            Assert.AreEqual(new ObjectElement(), result.Configuration);
        }

        [Test]
        public void ConvertFromWrongTypeTest()
        {
            // Action
            Assert.That(() => _instance.ConvertFrom(null, null, 100), Throws.InstanceOf<NotSupportedException>());
        }

        [Test]
        public void ConvertFromTest()
        {
            // Pre-Conditions
            const string strName = "TestElement";

            // Action
            ConfigurableClass result = (ConfigurableClass) _instance.ConvertFrom(null, null, strName);

            // Post-Conditions
            Assert.AreEqual(CoreCfg.Root.GetSection<TestSection>().Items[strName], result.Configuration);
        }

        [Test]
        public void ConvertFromTypeObjectTest()
        {
            // Pre-Conditions
            ConfigurableClass expected = new ConfigurableClass();
            
            // Action
            ConfigurableClass result = (ConfigurableClass)_instance.ConvertFrom(null, null, expected);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ConvertToTypeStringTest()
        {
            // Pre-Conditions
            ConfigurableClass configurableClass = new ConfigurableClass {Configuration = new ObjectElement{Name = "lalala"}};

            // Action
            string result = (string)_instance.ConvertTo(null, null, configurableClass, typeof(string));

            // Post-Conditions
            Assert.AreEqual(configurableClass.Configuration.Name, result);
        }

        [Test]
        public void ConvertToWrongTypeTest()
        {
            // Action
            Assert.That(() => _instance.ConvertTo(null, null, 100, typeof(int)), Throws.InstanceOf<NotSupportedException>());
        }

        [Test]
        public void ConvertToTypeObjectTest()
        {
            // Pre-Conditions
            ConfigurableClass expected = new ConfigurableClass();

            // Action
            ConfigurableClass result = (ConfigurableClass)_instance.ConvertTo(null, null, expected, typeof(ConfigurableClass));

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }
        #endregion
    }
}