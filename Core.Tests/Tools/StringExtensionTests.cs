#region Using directives
using System;
using System.IO;
using Allag.Core.TestData.Tools;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools
{
    [TestFixture]
    public class StringExtensionTests
    {
        #region Tests
        [Test]
        public void ParseEnumWithNullValueTest()
        {
            // Action
            Assert.That(() => StringExtension.ParseEnum<TestEnum>(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void ParseEnumWithWrongValueTest()
        {
            // Action
            Assert.That(() => "lalala".ParseEnum<TestEnum>(), Throws.ArgumentException);
        }

        [Test]
        public void ParseEnumWithWrongValueWithoutExceptionTest()
        {
            // Action
            TestEnum result = "lalala".ParseEnum<TestEnum>(true, false);

            // Post-Conditions
            Assert.AreEqual(default(TestEnum), result);
        }

        [Test]
        public void ParseEnum_EmptyValue_Test([Values("", " ")] string value)
        {
            // Pre-Conditions 
            const TestEnum expected = TestEnum.Empty;

            // Action
            TestEnum result = value.ParseEnum<TestEnum>();

            //Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ParseEnumTest()
        {
            // Pre-Conditions 
            const string strValue = "One, Second, Three, ";
            const TestEnum expected = TestEnum.One | TestEnum.Two | TestEnum.Three | TestEnum.Empty;

            // Action
            TestEnum result = strValue.ParseEnum<TestEnum>();

            //Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ParseEnum_CustomDelimiter_Test()
        {
            // Pre-Conditions 
            const string strValue = "One  Second, Three";
            const TestEnum expected = TestEnum.One | TestEnum.Two | TestEnum.Three;

            // Action
            TestEnum result = strValue.ParseEnum<TestEnum>(delimiters: " ,");

            //Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ParseEnumIgnoreCaseTest()
        {
            // Pre-Conditions 
            const string strValue = "one, second, three";
            const TestEnum expected = TestEnum.One | TestEnum.Two | TestEnum.Three;

            // Action
            TestEnum result = strValue.ParseEnum<TestEnum>(true);

            //Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void EnumToStringTest()
        {
            // Pre-Conditions 
            const string expected = "One, Second, Three, ";
            const TestEnum value = TestEnum.Empty | TestEnum.One | TestEnum.Two | TestEnum.Three;

            // Action
            string result = value.EnumToString();

            //Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void EnumToStringWithoutXmlAttributeTest()
        {
            // Pre-Conditions 
            const string expected = "ReadWrite";
            const FileAccess value = FileAccess.Read | FileAccess.Write;

            // Action
            string result = value.EnumToString();

            //Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void EnumToStringNotEnumerationValueTest()
        {
            // Action
            Assert.That(() => true.EnumToString(), Throws.ArgumentException);
        }

        [Test]
        public void EnumToString_NullType_Test()
        {
            // Action
            Assert.That(() => StringExtension.EnumToString(null, this), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void JoinTest()
        {
            // Pre-Conditions
            string[] arr = new string[] {"1", "2", "3"};

            // Action
            string result = arr.Join(", ", "'", ">", x => x + '0');

            // Post-Conditions
            Assert.AreEqual("'10>, '20>, '30>", result);
        }

        [Test]
        public void JoinDefaultValueFunctionTest()
        {
            // Pre-Conditions
            string[] arr = new string[] { "1", "2", "3" };

            // Action
            string result = arr.Join(", ", "'", ">");

            // Post-Conditions
            Assert.AreEqual("'1>, '2>, '3>", result);
        }

        [Test]
        public void JoinDefaultCloseDecoraterTest()
        {
            // Pre-Conditions
            string[] arr = new string[] { "1", "2", "3" };

            // Action
            string result = arr.Join(", ", "'");

            // Post-Conditions
            Assert.AreEqual("'1', '2', '3'", result);
        }

        [Test]
        public void JoinNullOpenDecoraterTest()
        {
            // Pre-Conditions
            string[] arr = new string[] { "1", "2", "3" };

            // Action
            string result = arr.Join(", ", null, "'");

            // Post-Conditions
            Assert.AreEqual("'1', '2', '3'", result);
        }

        [Test]
        public void JoinDefaultOpenAndCloseDecorateresTest()
        {
            // Pre-Conditions
            string[] arr = new string[] { "1", "2", "3" };

            // Action
            string result = arr.Join(", ");

            // Post-Conditions
            Assert.AreEqual("1, 2, 3", result);
        }

        [Test]
        public void JoinDefaultTest()
        {
            // Pre-Conditions
            string[] arr = new string[] { "1", "2", "3" };

            // Action
            string result = arr.Join();

            // Post-Conditions
            Assert.AreEqual("123", result);
        }

        [Test]
        public void JoinNullSeparaterTest()
        {
            // Pre-Conditions
            string[] arr = new string[] { "1", "2", "3" };

            // Action
            string result = arr.Join(null, "'");

            // Post-Conditions
            Assert.AreEqual("'1''2''3'", result);
        }

        [Test]
        public void JoinDefaultNullArrayTest()
        {
            // Action
            string result = ((string[])null).Join();

            // Post-Conditions
            Assert.AreEqual(string.Empty, result);
        }
        #endregion
    }
}