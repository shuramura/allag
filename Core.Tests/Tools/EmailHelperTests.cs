﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Allag.Core.Configuration.Net;
using NUnit.Framework;
using SmtpClient = Allag.Core.Net.SmtpClient;
#endregion

namespace Allag.Core.Tools
{
    [TestFixture]
    public class EmailHelperTests
    {
        #region Constants
        private const string MAIL_FOLDER = @".\Emails";
        public const string EmailName = "LocalMail";
        private const string SMTP_NAME = "1";
        #endregion

        #region Variables
        private EmailHelper _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new EmailHelper(EmailName);

            ClearEmailFolder();
        }

        [TearDown]
        public void TearDown()
        {
            ClearEmailFolder();
        }
        #endregion

        #region Test
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual("LocalSmtp", _instance.SmtpClient.Configuration.Name, "The property 'SmtpServerName' is wrong.");
            Assert.AreEqual(string.Empty, _instance.Subject.ToString(), "The property 'Subject' is wrong.");
            Assert.AreEqual(string.Empty, _instance.Message.ToString(), "The property 'Message' is wrong.");
            Assert.AreEqual(true, _instance.IsBodyHtml, "The property 'IsBodyHtml' is wrong.");
            Assert.AreEqual(true, _instance.HandleExceptions, "The property 'HandleExceptions' is wrong.");
        }

        [Test]
        public void CtorNotExistEmailGroupTest()
        {
            // Action 
            EmailHelper instance = new EmailHelper("email0@email.com.au,email1@email.com.au");

            // Post-Conditions
            Assert.AreEqual(string.Empty, instance.SmtpClient.Configuration.Name, "The property 'SmtpServerName' is wrong.");
            Assert.AreEqual(string.Empty, instance.Subject.ToString(), "The property 'Subject' is wrong.");
            Assert.AreEqual(string.Empty, instance.Message.ToString(), "The property 'Message' is wrong.");
            Assert.AreEqual(false, instance.IsBodyHtml, "The property 'IsBodyHtml' is wrong.");
            Assert.AreEqual(true, instance.HandleExceptions, "The property 'HandleExceptions' is wrong.");
        }

        [Test]
        public void CtorWithDifferentSmtpClientTest()
        {
            // Action
            EmailHelper instance = new EmailHelper(EmailName, SMTP_NAME);

            // Post-Conditions
            Assert.AreEqual(SMTP_NAME, instance.SmtpClient.Configuration.Name, "The property 'SmtpServerName' is wrong.");
            Assert.AreEqual(string.Empty, instance.Subject.ToString(), "The property 'Subject' is wrong.");
            Assert.AreEqual(string.Empty, instance.Message.ToString(), "The property 'Message' is wrong.");
            Assert.IsTrue(instance.IsBodyHtml, "The property 'IsBodyHtml' is wrong.");
        }

        [Test]
        public void CtorNotExistConfigurationTest()
        {
            // Action 
            Assert.That(() => new EmailHelper((EmailElement)null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void SubjectSetTest()
        {
            // Pre-Conditions 
            const string expected = "lalala";

            // Action
            _instance.Subject = new MemoryResource(expected);

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Subject.ToString());
        }

        [Test]
        public void SubjectSetNullValueTest()
        {
            // Action
            Assert.That(() => _instance.Subject = null, Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void MessageSetTest()
        {
            // Pre-Conditions 
            const string expected = "lalala";

            // Action
            _instance.Message = new MemoryResource(expected);

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Message.ToString());
        }

        [Test]
        public void MessageSetNullValueTest()
        {
            // Action
            Assert.That(() => _instance.Message = null, Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void IsBodyHtmlSetTest()
        {
            // Pre-Conditions 
            bool expected = !_instance.IsBodyHtml;

            // Action
            _instance.IsBodyHtml = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.IsBodyHtml);
        }

        [Test]
        public void SmtpServerSetTest()
        {
            // Pre-Conditions 
            SmtpClient expected = new SmtpClient(SMTP_NAME);

            // Action
            _instance.SmtpClient = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.SmtpClient, "The property 'SmtpClient' is wrong.");
            Assert.IsTrue(_instance.IsBodyHtml, "The property 'IsBodyHtml' is wrong.");
            Assert.AreEqual(string.Empty, _instance.Subject.ToString(), "The property 'Subject' is wrong.");
            Assert.AreEqual(string.Empty, _instance.Message.ToString(), "The property 'Message' is wrong.");
        }

        [Test]
        public void SmtpClientSetNullValueTest()
        {
            // Action
            Assert.That(() => _instance.SmtpClient = null, Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void HandleExceptionsSetTest()
        {
            // Pre-Conditions 
            bool expected = !_instance.HandleExceptions;

            // Action
            _instance.HandleExceptions = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.HandleExceptions);
        }

        [Test]
        public void SendEmailDefaultlTest()
        {
            // Pre-Conditions 
            InitializeEmailHelper(_instance);

            // Action
            _instance.SendEmail();


            // Post-Conditions
            Email[] emails = GetEmails();
            Assert.AreEqual(1, emails.Length, "The email count is wrong.");
            Assert.AreEqual("email@test.com.au", emails[0].From, "The email from is wrong.");
            Assert.AreEqual("lalala@lala.com", emails[0].To, "The email to is wrong.");
            Assert.AreEqual(_instance.Subject.ToString(), emails[0].Subject, "The email subject is wrong.");
            Assert.AreEqual(MediaTypeNames.Text.Html, emails[0].ContentType, "The email content type is wrong.");
            Assert.AreEqual(_instance.Message.ToString(), emails[0].Body, "The email body is wrong.");
        }

        [Test]
        public void SendEmailCustomTest()
        {
            // Pre-Conditions 
            const string strSubject = "Custom Subject";
            const string strMessage = "Custom Message";

            InitializeEmailHelper(_instance);

            _instance.Subject = new MemoryResource(strSubject);
            _instance.Message = new MemoryResource(strMessage);
            _instance.Cc = new MemoryResource("cc@lala.com");
            _instance.Bcc = new MemoryResource("bcc@lala.com");

            // Action
            _instance.SendEmail();

            // Post-Conditions
            Email[] emails = GetEmails();
            Assert.AreEqual(1, emails.Length, "The email count is wrong.");
            Assert.AreEqual("email@test.com.au", emails[0].From, "The email from is wrong.");
            Assert.AreEqual("lalala@lala.com", emails[0].To, "The email to is wrong.");
            Assert.AreEqual("cc@lala.com", emails[0].Cc, "The email cc is wrong.");
            Assert.AreEqual(strSubject, emails[0].Subject, "The email subject is wrong.");
            Assert.AreEqual(MediaTypeNames.Text.Html, emails[0].ContentType, "The email content type is wrong.");
            Assert.AreEqual(strMessage, emails[0].Body, "The email body is wrong.");
        }

        [Test]
        public void SendEmailWithTemplateSubjectTest()
        {
            // Pre-Conditions 
            const string strSubject = "Custom Subject";
            const string strMessage = "Custom Message";

            InitializeEmailHelper(_instance);

            _instance.Subject = new MemoryResource("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" xmlns:msxsl=\"urn:schemas-microsoft-com:xslt\"><xsl:output method=\"text\" encoding=\"utf-8\"/><xsl:template match=\"/dateTime\"><xsl:text>Custom Subject </xsl:text><xsl:value-of select=\"substring-before(., 'T')\"/></xsl:template></xsl:stylesheet>") {Type = ResourceType.Xslt};
            _instance.Message = new MemoryResource(strMessage);

            // Action
            _instance.SendEmail(DateTime.Now);


            // Post-Conditions
            Email[] emails = GetEmails();
            Assert.AreEqual(1, emails.Length, "The email count is wrong.");
            Assert.AreEqual("email@test.com.au", emails[0].From, "The email from is wrong.");
            Assert.AreEqual("lalala@lala.com", emails[0].To, "The email to is wrong.");
            StringAssert.StartsWith(strSubject + " " + DateTime.Today.ToString("yyyy-MM-dd"), emails[0].Subject, "The email subject is wrong.");
            Assert.AreEqual(MediaTypeNames.Text.Html, emails[0].ContentType, "The email content type is wrong.");
            Assert.AreEqual(strMessage, emails[0].Body, "The email body is wrong.");
        }

        [Test]
        public void SendEmailHtmlBodyTest()
        {
            // Pre-Conditions 
            const string strSubject = "Custom Subject";
            const string strMessage = "<Body />";
            InitializeEmailHelper(_instance);

            _instance.Subject = new MemoryResource(strSubject);
            _instance.Message = new MemoryResource(strMessage);
            _instance.IsBodyHtml = true;

            // Action
            _instance.SendEmail();

            // Post-Conditions
            Email[] emails = GetEmails();
            Assert.AreEqual(1, emails.Length, "The email count is wrong.");
            Assert.AreEqual("email@test.com.au", emails[0].From, "The email from is wrong.");
            Assert.AreEqual("lalala@lala.com", emails[0].To, "The email to is wrong.");
            Assert.AreEqual(strSubject, emails[0].Subject, "The email subject is wrong.");
            Assert.AreEqual(MediaTypeNames.Text.Html, emails[0].ContentType, "The email content type is wrong.");
            Assert.AreEqual(strMessage, emails[0].Body, "The email body is wrong.");
        }

        [Test]
        public void SendEmailWithAttachmentTest()
        {
            // Pre-Conditions 
            const string strSubject = "Custom Subject";
            const string strMessage = "<Body />";

            InitializeEmailHelper(_instance);

            _instance.Subject = new MemoryResource(strSubject);
            _instance.Message = new MemoryResource(strMessage);
            _instance.IsBodyHtml = true;

            // Action
            _instance.SendEmail(null, MailPriority.Normal, new[] {@".\TestData\Tools\EmailAttacment.txt"});

            // Post-Conditions
            Email[] emails = GetEmails();
            Assert.AreEqual(1, emails.Length, "The email count is wrong.");
            Assert.AreEqual("email@test.com.au", emails[0].From, "The email from is wrong.");
            Assert.AreEqual("lalala@lala.com", emails[0].To, "The email to is wrong.");
            Assert.AreEqual(strSubject, emails[0].Subject, "The email subject is wrong.");
            Assert.AreEqual("multipart/mixed", emails[0].ContentType, "The email content type is wrong.");
        }

        [Test]
        public void SendEmailWithHandlingExceptionTest()
        {
            // Pre-Conditions 
            _instance.SmtpClient = new SmtpClient("WrongEmail");

            // Action
            _instance.SendEmail();

            // Post-Conditions
            Email[] emails = GetEmails();
            Assert.AreEqual(0, emails.Length, "The email count is wrong.");
        }

        [Test]
        public void SendEmailWithoutHandlingExceptionTest()
        {
            // Pre-Conditions 
            _instance.SmtpClient = new SmtpClient("WrongEmail");
            _instance.HandleExceptions = false;

            // Action
            Assert.That(() => _instance.SendEmail(), Throws.InstanceOf<FormatException>());
        }
        #endregion

        #region Methods
        public static void ClearEmailFolder()
        {
            Thread.Sleep(1000);
            if (Directory.Exists(MAIL_FOLDER))
            {
                Directory.Delete(MAIL_FOLDER, true);
            }
            Directory.CreateDirectory(MAIL_FOLDER);
        }

        public static Email[] GetEmails()
        {
            return (from file in Directory.GetFiles(MAIL_FOLDER, "*.eml") select new Email(file)).ToArray();
        }

        private static void InitializeEmailHelper(EmailHelper emailHelper)
        {
            emailHelper.Subject = new MemoryResource("Subject");
            emailHelper.Message = new MemoryResource("Message");
        }
        #endregion

        #region Classes
        public class Email
        {
            #region Variables
            private readonly Dictionary<string, string> _properties;
            #endregion

            #region Constructors
            public Email(string file)
            {
                _properties = new Dictionary<string, string>();
                using (TextReader reader = File.OpenText(file))
                {
                    string strTemp;
                    do
                    {
                        strTemp = reader.ReadLine();
                        if (strTemp != null)
                        {
                            string[] values = strTemp.Split(new char[] {':'});
                            if (values.Length > 1)
                            {
                                if (!_properties.ContainsKey(values[0]))
                                {
                                    _properties.Add(values[0], values[1]);
                                }
                            }
                            else
                            {
                                Body = reader.ReadToEnd().Trim().
                                              Replace("=3D", "=").Replace("=0D", "\r").
                                              Replace("=0A", "\n").Replace("=" + Environment.NewLine, "");
                                break;
                            }
                        }
                    } while (strTemp != null);
                }
            }
            #endregion

            #region Properties
            public string From
            {
                get { return GetValue("From"); }
            }

            public string To
            {
                get { return GetValue("To"); }
            }

            public string Cc
            {
                get { return GetValue("Cc"); }
            }

            public string Bcc
            {
                get { return GetValue("Bcc"); }
            }

            public string Subject
            {
                get { return GetValue("Subject"); }
            }

            public string ContentType
            {
                get { return GetValue("Content-Type").Split(new char[] {';'})[0].Trim(); }
            }

            public string Priority
            {
                get { return GetValue("Priority"); }
            }

            public string Importance
            {
                get { return GetValue("Importance"); }
            }

            public string Body { get; private set; }
            #endregion

            #region Methods
            private string GetValue(string name)
            {
                string ret;
                if (_properties.TryGetValue(name, out ret))
                {
                    ret = ret.Trim();
                }
                else
                {
                    ret = string.Empty;
                }
                return ret;
            }
            #endregion
        }
        #endregion
    }
}