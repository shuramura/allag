﻿#region Using directives
using Allag.Core.TestData.Tools;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools
{
    public class EnumTypeConverterTests : BaseTypeConverterTests<EnumTypeConverter<TestEnum>, TestEnum>
    {
        #region Overrides of BaseTypeConverterTests<EnumTypeConverter<TestEnum>,TestEnum>
        [Test]
        public override void ConvertFromStringTest()
        {
            // Pre-Conditions
            const TestEnum expected = TestEnum.Two;

            // Action 
            TestEnum result = (TestEnum) Instance.ConvertFrom(null, null, "Second");

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ConvertFromStringWrongCaseTest()
        {
            // Pre-Conditions
            const TestEnum expected = TestEnum.Two;

            // Action 
            TestEnum result = (TestEnum) Instance.ConvertFrom(null, null, "second");

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public override void ConvertToTypeStringTest()
        {
            // Pre-Conditions
            const string expected = "Second";

            // Action 
            string result = (string) Instance.ConvertTo(null, null, TestEnum.Two, typeof(string));

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        protected override TestEnum ValueOfType
        {
            get { return TestEnum.One; }
        }
        #endregion
    }
}