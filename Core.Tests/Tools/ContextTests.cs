﻿#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools
{
    [TestFixture]
    public class ContextTests
    {
        #region Variables
        private Guid _id;
        private Context<object> _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _id = Guid.NewGuid();
            _instance = new Context<object>(_id);
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Post-Conditions
            Assert.AreEqual(_id, _instance.Id, "The property 'Id' is wrong.");
        }

        [Test]
        public void Value_Set_Test()
        {
            // Pre-Conditions
            object expected = new object();

            // Action
            _instance.Value = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.Value, "The property 'Value' is wrong.");
        }

        [Test]
        public void Value_GetWithValueFactory_Test()
        {
            // Pre-Conditions
            Context<ContextTests> instance = new Context<ContextTests>(_id, id => this);
            
            // Action
            ContextTests result = instance.Value;

            // Post-Conditions
            Assert.AreEqual(this, result, "The result is wrong.");
        }
        #endregion
    }
}