﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools
{
    [TestFixture]
    public class EnumerableExtensionTests
    {
        #region Tests
        [Test]
        public void Each_Test()
        {
            // Pre-Conditions
            IEnumerable<int> data = new int[] {1, 2, 3};
            int nCount = 0;

            // Action
            EnumerableExtension.ForEach(data, i => Assert.AreEqual(++nCount, i));

            // Post-Conditions
            Assert.AreEqual(data.Count(), nCount);
        }

        [Test]
        public void Each_NullAction_Test()
        {
            // Action
            Assert.That(() => EnumerableExtension.ForEach(new int[] {1}, (Action<int>)null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void Each_NullEnumeration_Test()
        {
            // Pre-Conditions
            int nCount = 0;

            // Action
            EnumerableExtension.ForEach((int[])null, i =>
            {
                nCount++;
                Assert.Fail();
            });

            // Post-Conditions
            Assert.AreEqual(0, nCount);
        }

        [Test]
        public void Each_WithIndex_Test()
        {
            // Pre-Conditions
            int nCount = 0;
            int[] data = new int[] {0, 1, 2};

            // Action
            EnumerableExtension.ForEach(data, (index, item) =>
            {
                nCount++;
                Assert.AreEqual(data[index], item, "The item '{0}' is wrong.", index);
            });

            // Post-Conditions
            Assert.AreEqual(data.Length, nCount);
        }

        [Test]
        public void Each_WithIndexNullAction_Test()
        {
            // Action
            Assert.That(() => EnumerableExtension.ForEach(new int[] {1}, (Action<int, int>)null), Throws.InstanceOf<ArgumentNullException>());
        }
        #endregion
    }
}