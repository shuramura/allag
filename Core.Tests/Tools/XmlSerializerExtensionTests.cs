#region Using directives
using System;
using System.IO;
using System.Text;
using System.Xml.Schema;
using System.Xml.Serialization;
using Allag.Core.TestData;
using Allag.Core.TestData.Reflection;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools
{
    [TestFixture]
    public class XmlSerializerExtensionTests
    {
        #region Tests
        [Test]
        public void SerializeTest()
        {
            // Pre-Conditions
            Guid[] expected = new[] {Guid.NewGuid(), Guid.NewGuid()};
            bool isInvoked = false;

            // Action
            string result = XmlSerializerExtension.Serialize(expected,
                                                             x =>
                                                                 {
                                                                     Assert.IsNotNull(x, "The xml writer settings are wrong.");
                                                                     isInvoked = true;
                                                                 });

            // Post-Conditions
            Assert.IsTrue(isInvoked, "The xml writer settings is not invoked.");
            CheckSerializationResult(expected, result);
        }

        [Test]
        public void SerializeWithoutTypeSpecifyingTest()
        {
            // Pre-Conditions
            Guid[] expected = new[] {Guid.NewGuid(), Guid.NewGuid()};

            // Action
            string result = XmlSerializerExtension.Serialize(expected);

            // Post-Conditions
            CheckSerializationResult(expected, result);
        }

        [Test]
        public void SerializeWithoutTypeSpecifyingWithNullTest()
        {
            // Action
            Assert.That(() => XmlSerializerExtension.Serialize(null, new StringBuilder()), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void DeserializeTest()
        {
            // Pre-Conditions
            Type type = typeof(TestClass);

            TestClass expected = new TestClass {BaseString = "BaseString", String = string.Empty, Int = 10};
            TestClass.Child child = new TestClass.Child {String = "lalala"};
            expected.ChildInfo = new[] {child};

            StringBuilder stringBuilder = new StringBuilder();
            XmlSerializer xmlSerializer = new XmlSerializer(type);
            using(StringWriter writer = new StringWriter(stringBuilder))
            {
                xmlSerializer.Serialize(writer, expected);
            }
            bool isInvoked = false;

            // Action
            using(StringReader reader = new StringReader(stringBuilder.ToString()))
            {
                TestClass result = XmlSerializerExtension.Deserialize<TestClass>(reader,
                                                                                 x =>
                                                                                     {
                                                                                         Assert.IsNotNull(x, "The xml reader settings are wrong.");
                                                                                         isInvoked = true;
                                                                                     });

                // Post-Conditions
                Assert.IsTrue(isInvoked, "The xml reader settings initialization is not invoked.");
                Helper.Validate(expected, result, string.Empty);
            }
        }

        [Test]
        public void DeserializeNotWorkflowTest()
        {
            // Pre-Conditions
            Type type = typeof(Guid[]);
            Guid[] expected = new[] {Guid.NewGuid(), Guid.NewGuid()};

            StringBuilder stringBuilder = new StringBuilder();
            XmlSerializer xmlSerializer = new XmlSerializer(type);
            using(StringWriter writer = new StringWriter(stringBuilder))
            {
                xmlSerializer.Serialize(writer, expected);
            }

            using(StringReader reader = new StringReader(stringBuilder.ToString()))
            {
                // Action
                Guid[] result = (Guid[])XmlSerializerExtension.Deserialize(reader, type);

                // Post-Conditions
                Helper.Validate(expected, result, string.Empty);
            }
        }

        [Test]
        public void ValidateNullXmlTest()
        {
            // Action
            Assert.That(() => XmlSerializerExtension.Validate(null, typeof(TestClass)), Throws.ArgumentException);
        }

        [Test]
        public void ValidateEmptyXmlTest()
        {
            // Action
            Assert.That(() => XmlSerializerExtension.Validate(string.Empty, typeof(TestClass)), Throws.ArgumentException);
        }

        [Test]
        public void ValidateNullTypeTest()
        {
            // Action
            Assert.That(() => XmlSerializerExtension.Validate("lalala", null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void ValidateTest()
        {
            // Pre-Conditions
            Type itemType = typeof(TestClass);

            TestClass test = new TestClass {String = string.Empty};
            TestClass.Child child = new TestClass.Child {String = "lalala"};
            test.ChildInfo = new[] {child, new TestClass.Child()};
            string strXml = test.Serialize();

            // Action
            XmlValidationError result = XmlSerializerExtension.Validate(strXml, itemType);

            // Post-Conditions
            result = CheckValidity(result, "TestClass", 3);
            CheckValidity(result, "BaseString", 0);
            CheckValidity(result, "String", 0);
            result = CheckValidity(result, "Child[2]", 1);
            CheckValidity(result, "String", 0);
        }

        [Test]
        public void GetXmlSchemaForNotExistSchemaTest()
        {
            // Action
            XmlSchema result = XmlSerializerExtension.GetXmlSchema(typeof(int));

            // Post-Conditions
            Assert.IsNull(result);
        }

        [Test]
        public void GetXmlSchemaWithNullTypeTest()
        {
            // Action
            Assert.That(() => XmlSerializerExtension.GetXmlSchema(null), Throws.InstanceOf<ArgumentNullException>());
        }
        #endregion

        #region Methods
        private static XmlValidationError CheckValidity(XmlValidationError errors, string element, int childCount)
        {
            foreach (XmlValidationError item in errors.Children)
            {
                if (item.Element == element)
                {
                    if (childCount > -1)
                    {
                        Assert.AreEqual(childCount, item.Children.Count, "The element '{0}' has wrong count of children.", element);
                    }
                    return item;
                }
            }
            Assert.Fail("The element '{0}' of the element '{1}' is absent in the error list.", element, errors.Element);
            return null;
        }

        private static void CheckSerializationResult(object expected, string result)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(expected.GetType());
            using(StringReader reader = new StringReader(result))
            {
                Assert.AreEqual(expected, xmlSerializer.Deserialize(reader));
            }
        }
        #endregion
    }
}