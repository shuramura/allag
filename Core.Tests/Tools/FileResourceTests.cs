﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools
{
    [TestFixture]
    public class FileResourceTests
    {
        #region Constants
        private const string FOLDER = @".\TestData\Tools\";
        private const string PATH = FOLDER + "FileResource.txt";
        #endregion

        #region Variables
        private FileResource _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = CreateInstance(PATH);
        }

        [TearDown]
        public void CleanUp()
        {
            foreach (string file in Directory.GetFiles(FOLDER, "*.temp"))
            {
                File.Delete(file);
            }
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, PATH), _instance.Path, "The property 'Path' is wrong.");
            Assert.AreEqual("12345", _instance.ToString(), "The content is wrong.");
        }

        [Test]
        public void CtorWithFullPathTest()
        {
            // Pre-Conditions
            const string expected = @"C:\Test";

            // Action
            FileResource instance = CreateInstance(expected);

            // Post-Conditions
            Assert.AreEqual(expected, instance.Path, "The property 'Path' is wrong.");
        }

        [Test]
        public void GetStreamDefaultTest()
        {
            // Post-Conditions
            using (Stream result = _instance.GetStream())
            {
                Assert.IsTrue(result.CanRead, "The property 'CanRead' is wrong.");
                Assert.IsFalse(result.CanWrite, "The property 'CanWrite' is wrong.");
            }
        }

        [Test]
        public void GetStreamReadOnlyTest()
        {
            // Post-Conditions
            using (Stream result = _instance.GetStream(true))
            {
                Assert.IsTrue(result.CanRead, "The property 'CanRead' is wrong.");
                Assert.IsFalse(result.CanWrite, "The property 'CanWrite' is wrong.");
            }
        }

        [Test]
        public void GetStream_Writable_Test()
        {
            // Post-Conditions
            using (Stream result = _instance.GetStream(false))
            {
                Assert.IsTrue(result.CanRead, "The property 'CanRead' is wrong.");
                Assert.IsTrue(result.CanWrite, "The property 'CanWrite' is wrong.");
            }
        }

        [Test]
        public void GetStream_WritableFileDoesNotExist_Test()
        {
            // Pre-Conditions
            FileResource instance = CreateInstance(FOLDER + Guid.NewGuid() + ".temp");

            //Action
            using (Stream result = instance.GetStream(false))
            {
                // Post-Conditions
                Assert.IsTrue(result.CanRead, "The property 'CanRead' is wrong.");
                Assert.IsTrue(result.CanWrite, "The property 'CanWrite' is wrong.");
            }
        }
        #endregion

        #region Methods
        private static FileResource CreateInstance(string path)
        {
            return new FileResource(path, new Dictionary<string, string> { { ResourceTypeConverter.LocationProperty, "file" } });
        }
        #endregion
    }
}