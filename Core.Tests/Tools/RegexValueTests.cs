﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools
{
    [TestFixture]
    public class RegexValueTests
    {
        #region Constants
        private const RegexOptions DEFAULT_OPTIONS = RegexOptions.Compiled | RegexOptions.ExplicitCapture | RegexOptions.CultureInvariant | RegexOptions.Singleline | RegexOptions.IgnoreCase;
        #endregion

        #region Variables
        private RegexValue _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            _instance = new RegexValue();
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Post-Conditions
            Assert.IsNull(_instance.Template, "The property 'Template' is wrong.");
            Assert.AreEqual(DEFAULT_OPTIONS, _instance.Options, "The property 'Options' is wrong.");
            Assert.AreEqual(ReplaceMode.Native, _instance.ReplaceMode, "The property 'ReplaceMode' is wrong.");
        }

        [Test]
        public void Ctor_WithParameters_Test()
        {
            // Pre-Conditions
            const string strTemplate = ".+";
            const RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Multiline;

            // Action
            RegexValue instance = new RegexValue(strTemplate, options);

            // Post-Conditions
            Assert.AreEqual(strTemplate, instance.Template, "The property 'Template' is wrong.");
            Assert.AreEqual(options, instance.Options, "The property 'Options' is wrong.");
            Assert.AreEqual(ReplaceMode.Native, _instance.ReplaceMode, "The property 'ReplaceMode' is wrong.");
        }

        [Test]
        public void ReplaceMode_Set_Test()
        {
            // Pre-Conditions
            const ReplaceMode expected = ReplaceMode.Multiply;

            // Action
            _instance.ReplaceMode = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.ReplaceMode);
        }

        [Test]
        [Sequential]
        public void GetValue_StringConverter_Test([Values(@"(?<1>0)\w+(?<2>1)\w+(?<3>2)\w+")] string pattern,
            [Values("0asdfas1weqr2oiuy")] string data,
            [Values("012")] string expected)
        {
            // Pre-Conditions   
            _instance.Template = pattern;

            // Action
            string result = _instance.GetValue(data);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetValue_NullData_Test()
        {
            // Action
            Assert.That(() => _instance.GetValue(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void GetValue_NullConverter_Test()
        {
            // Action
            Assert.That(() => _instance.GetValue<int>(string.Empty, null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void GetValue_Test()
        {
            // Pre-Conditions   
            const string strData = "01234a5df";
            _instance.Template = @"(?<1>\d+)A(?<2>\d)\w+";

            // Action
            int result = _instance.GetValue(strData, x => int.Parse(string.Concat(x)));

            // Post-Conditions
            Assert.AreEqual(12345, result);
        }

        [Test]
        public void GetValues_Test()
        {
            // Pre-Conditions   
            const string strData = "01234a5df";
            _instance.Template = @"(?<1>\d+)\w";

            // Action
            IEnumerable<string> result = _instance.GetValues(strData);

            // Post-Conditions
            Assert.AreEqual(2, result.Count(), "The result count is wrong.");
            Assert.AreEqual("01234", result.First(), "The first result is wrong.");
            Assert.AreEqual("5", result.Skip(1).First(), "The second result is wrong.");
        }

        [Test]
        public void GetValues_WithConverter_Test()
        {
            // Pre-Conditions   
            const string strData = "01234a5df";
            _instance.Template = @"(?<1>\d+)\w";

            // Action
            IEnumerable<int> result = _instance.GetValues(strData, x => int.Parse(string.Concat(x)));

            // Post-Conditions
            Assert.AreEqual(2, result.Count(), "The result count is wrong.");
            Assert.AreEqual(1234, result.First(), "The first result is wrong.");
            Assert.AreEqual(5, result.Skip(1).First(), "The second result is wrong.");
        }

        [Test]
        public void GetValue_WithConverter_Test()
        {
            // Pre-Conditions   
            const string strData = "01234a5df";
            _instance.Template = @"(?<1>\d+)A(?<2>\d)\w+";

            // Action
            int result = _instance.GetValue(strData, x => int.Parse(string.Format("{0}1{1}", x)));

            // Post-Conditions
            Assert.AreEqual(123415, result);
        }

        [Test]
        public void GetValue_EmptyData_Test()
        {
            // Pre-Conditions   
            _instance.Template = @".+";

            // Action
            string result = _instance.GetValue(string.Empty);

            // Post-Conditions
            Assert.IsNull(result);
        }

        [Test]
        public void GetValue_DoesNotMatch_Test()
        {
            // Pre-Conditions   
            const string strData = "01234a5df";
            _instance.Template = @"^(?<1>\D+)A(?<2>\d)\w+";

            // Action
            string result = _instance.GetValue(strData);

            // Post-Conditions
            Assert.IsNull(result);
        }

        [Test]
        [Sequential]
        public void IsMatch_Test([Values(null, @"(?<1>\d+)A(?<2>\d)\w+", @"^\D+")] string template,
            [Values(true, true, false)] bool expected)
        {
            // Pre-Conditions
            const string strData = "01234a5df";
            _instance.Template = template;

            // Action
            bool result = _instance.IsMatch(strData);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void IsMatch_Test()
        {
            // Action
            Assert.That(() => _instance.IsMatch(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void Replacements_Set_Test()
        {
            // Pre-Conditions
            string[] expected = new[] {"1", "2", "3"};

            // Action
            _instance.Replacements = expected;

            // Post-Conditions
            CollectionAssert.AreEqual(expected, _instance.Replacements);
        }

        [Test]
        public void Replacements_Set_NullValue_Test()
        {
            // Action
            _instance.Replacements = null;

            // Post-Conditions
            CollectionAssert.AreEqual(new string[] {string.Empty}, _instance.Replacements);
        }

        [Test]
        public void Replacements_Set_EmptyArray_Test()
        {
            // Action
            _instance.Replacements = new string[0];

            // Post-Conditions
            CollectionAssert.AreEqual(new string[] {string.Empty}, _instance.Replacements);
        }

        [Test]
        [Sequential]
        public void Replace_ReplacModeIsNative_Test([Values(@"\d+", @"(?<1>\d+)", @"(?<1>\d{2})(?<2>\d{2})(?<3>\d)")] string template,
            [Values("100", "123", "12345")] string value,
            [Values("", "$1", "$1$3")] string replacement,
            [Values("", "123", "125")] string expected)
        {
            // Pre-Conditions
            _instance.ReplaceMode = ReplaceMode.Native;
            _instance.Template = template;
            _instance.Replacements = new[] {replacement};

            // Action
            string result = _instance.Replace(value);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        [Sequential]
        public void Replace_ReplacModeIsSimple_Test([Values(@"\d+", @"(?<1>\d+)", @"(?<1>\d)(?<2>\d)(?<3>\d)", @"asd(\d)(?<1>\d{2})fgh", @"sd(\d)(?<1>\d{2})fg", "<a>((?<1>[^<]{1,3})[^<]?)*<a>")] string template,
            [Values("100", "100", "123", "asd123fgh", "asd123fgh", "<a>123456<a>")] string value,
            [Values("", "**", "**--**", "asd1**fgh", "asd1**fgh", "<a>**4**<a>")] string expected)
        {
            // Pre-Conditions
            _instance.ReplaceMode = ReplaceMode.Simple;
            _instance.Template = template;
            _instance.Replacements = new[] {"**", "--"};

            // Action
            string result = _instance.Replace(value);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        [Sequential]
        public void Replace_ReplacModeIsMultiply_Test([Values(@"\d+", @"(?<1>\d+)", @"(?<1>\d{2})(?<2>\d{2})(?<3>\d)", @"asd\d(?<1>\d{2})fgh")] string template,
            [Values("100", "123", "12345", "asd123fgh")] string value,
            [Values("", "******", "****----**", "asd1****fgh")] string expected)
        {
            // Pre-Conditions
            _instance.ReplaceMode = ReplaceMode.Multiply;
            _instance.Template = template;
            _instance.Replacements = new[] {"**", "--"};

            // Action
            string result = _instance.Replace(value);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Operator_StringToRegexValue_Test([Values(null, ".+")] string str)
        {
            // Action
            RegexValue instance = str;

            // Post-Conditions
            Assert.AreEqual(str, instance.Template, "The property 'Template' is wrong.");
            Assert.AreEqual(DEFAULT_OPTIONS, instance.Options, "The property 'Options' is wrong.");
            Assert.AreEqual(ReplaceMode.Native, instance.ReplaceMode, "The property 'ReplaceMode' is wrong.");
        }

        [Test]
        public void Operator_RegexValueToString_Test([Values(null, ".+")] string template)
        {
            // Pre-Conditions
            RegexValue instance = new RegexValue(template);

            // Action
            string result = instance;

            // Post-Conditions
            Assert.AreEqual(template, result);
        }

        //[Test]
        //public void Replace_Test()
        //{
        //    // Pre-Conditions
        //    _instance.Template = "<a>((?<1>.{1,3}).?)*(?!<a>)<a>(?<2>.+)";
        //    _instance.Replacements = new[] { "**", "-" };

        //    // Action
        //    string result = _instance.Replace("<a>123456<a>asdf<a>7890<a>");

        //    // Post-Conditions
        //    Assert.AreEqual(string.Empty, result);
        //}
        #endregion
    }
}