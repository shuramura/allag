#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools
{
    [TestFixture]
    public class ArrayTypeConverterTests
    {
        #region Variables
        private ArrayTypeConverter _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new ArrayTypeConverter(typeof(int));
        }

        [TearDown]
        public void TearDown()
        {
        }
        #endregion

        #region Tests
        [Test]
        public void CanConvertFromArrayTest()
        {
            // Pre-Conditions 
            const bool expected = true;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.CanConvertFrom(null, typeof(string[])));
        }

        [Test]
        public void CanConvertFromOtherTest()
        {
            // Pre-Conditions 
            const bool expected = false;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.CanConvertFrom(null, typeof(byte)));
        }

        [Test]
        public void CanConvertFromStringTest()
        {
            // Pre-Conditions 
            const bool expected = true;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.CanConvertFrom(null, typeof(string)));
        }

        [Test]
        public void CanConvertToArrayTest()
        {
            // Pre-Conditions 
            const bool expected = true;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.CanConvertTo(null, typeof(string[])));
        }

        [Test]
        public void CanConvertToOtherTest()
        {
            // Pre-Conditions 
            const bool expected = false;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.CanConvertTo(null, typeof(byte)));
        }

        [Test]
        public void CanConvertToStringTest()
        {
            // Pre-Conditions 
            const bool expected = true;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.CanConvertTo(null, typeof(string)));
        }

        [Test]
        public void ConvertFromEmptyStringTest()
        {
            // Pre-Conditions 
            int[] expected = new int[0];

            // Action
            int[] result = (int[]) _instance.ConvertFrom(null, null, "");

            // Post-Conditions
            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        public void ConvertFromOtherTest()
        {
            // Action
            Assert.That(() => _instance.ConvertFrom(null, null, 0), Throws.InstanceOf<NotSupportedException>());
        }

        [Test]
        public void ConvertFromStringArrayTest()
        {
            // Pre-Conditions 
            int[] expected = new[] {1, 2, 3, 4, 5, 6};

            // Action
            int[] result = (int[]) _instance.ConvertFrom(null, null, new[] {"1", "2", "3", "4", "5", "6"});

            // Post-Conditions
            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        public void ConvertFromStringTest()
        {
            // Pre-Conditions 
            int[] expected = new[] {1, 2, 3, 4, 5, 6};

            // Action
            int[] result = (int[]) _instance.ConvertFrom(null, null, "1,2,3,4,5,6");

            // Post-Conditions
            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        public void ConvertToEmptyStringTest()
        {
            // Action
            string result = (string) _instance.ConvertTo(null, null, new int[0], typeof(string));

            // Post-Conditions
            Assert.IsEmpty(result);
        }

        [Test]
        public void ConvertToOtherTest()
        {
            // Action
            Assert.That(() => _instance.ConvertTo(null, null, new[] {1, 2, 3}, typeof(float)), Throws.InstanceOf<NotSupportedException>());
        }

        [Test]
        public void ConvertToStringArrayTest()
        {
            // Pre-Conditions 
            string[] expected = new[] {"1", "2", "3", "4", "5", "6"};

            // Action
            string[] result = (string[]) _instance.ConvertTo(null, null, new[] {1, 2, 3, 4, 5, 6}, typeof(string[]));

            // Post-Conditions
            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        public void ConvertToStringTest()
        {
            // Pre-Conditions 
            const string expected = "1,2,3,4,5,6";

            // Action
            string result = (string) _instance.ConvertTo(null, null, new[] {1, 2, 3, 4, 5, 6}, typeof(string));

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void CtorGenericTest()
        {
            // Action
            ArrayTypeConverter instance = new ArrayTypeConverter<int>();

            // Post-Conditions
            Assert.AreEqual(typeof(int), instance.ElementType, "The property 'ElementType' is wrong.");
        }

        [Test]
        public void CtorNullElementTypeTest()
        {
            // Action
            Assert.That(() => new ArrayTypeConverter(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(typeof(int), _instance.ElementType, "The property 'ElementType' is wrong.");
        }
        #endregion
    }
}