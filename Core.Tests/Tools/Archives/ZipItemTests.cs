﻿#region Using directives
using System;
using System.IO;
using System.Linq;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools.Archives
{
    [TestFixture]
    public class ZipItemTests
    {
        #region Constants
        private const string DEST_FOLDER = @".\TestData\Tools\Archives\";
        #endregion

        #region Variables
        private ZipFile _zipFile;
        private ZipItem _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _zipFile = new ZipFile(ZipFileTests.ZipFile, true);
            _instance = (ZipItem)_zipFile.FirstOrDefault();
        }   

        [TearDown]
        public void TearDown()
        {
            _zipFile.Dispose();

            foreach (string file in Directory.GetFiles(DEST_FOLDER, "*.txt"))
            {
                File.Delete(file);
            }
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_FileItem_Test()
        {
            // Post-Conditions
            Assert.AreEqual("file1.txt", _instance.FullName, "The property 'FullName' is wrong.");
            Assert.AreEqual("file1.txt", _instance.Name, "The property 'Name' is wrong.");
            Assert.IsFalse(_instance.IsFolder, "The property 'IsFolder' is wrong.");
            Assert.AreEqual(new DateTime(2011, 3, 11, 13, 24, 34), _instance.ModifiedOn, "The property 'ModifiedOn' is wrong.");
        }

        [Test]
        public void Ctor_SubFileItem_Test()
        {
            // Action
            ZipItem instance = _instance = (ZipItem)_zipFile.Skip(2).FirstOrDefault();

            // Post-Conditions
            Assert.AreEqual("Folder1/file3.txt", instance.FullName, "The property 'FullName' is wrong.");
            Assert.AreEqual("file3.txt", instance.Name, "The property 'Name' is wrong.");
            Assert.IsFalse(instance.IsFolder, "The property 'IsFolder' is wrong.");
            Assert.AreEqual(new DateTime(2011, 3, 11, 13, 24, 46), instance.ModifiedOn, "The property 'ModifiedOn' is wrong.");
        }

        [Test]
        public void Ctor_FolderItem_Test()
        {
            // Action
            ZipItem instance = _instance = (ZipItem)_zipFile.Skip(1).FirstOrDefault();

            // Post-Conditions
            Assert.AreEqual("Folder1", instance.FullName, "The property 'FullName' is wrong.");
            Assert.AreEqual("Folder1", instance.Name, "The property 'Name' is wrong.");
            Assert.IsTrue(instance.IsFolder, "The property 'IsFolder' is wrong.");
            Assert.AreEqual(new DateTime(2013, 7, 25, 15, 20, 42), instance.ModifiedOn, "The property 'ModifiedOn' is wrong.");
        }

        [Test]
        public void Ctor_SubFolderItem_Test()
        {
            // Action
            ZipItem instance = _instance = (ZipItem)_zipFile.Skip(3).FirstOrDefault();

            // Post-Conditions
            Assert.AreEqual("Folder1/Folder2", instance.FullName, "The property 'FullName' is wrong.");
            Assert.AreEqual("Folder2", instance.Name, "The property 'Name' is wrong.");
            Assert.IsTrue(instance.IsFolder, "The property 'IsFolder' is wrong.");
            Assert.AreEqual(new DateTime(2013, 7, 25, 15, 34, 16), instance.ModifiedOn, "The property 'ModifiedOn' is wrong.");
        }

        [Test]
        public void ExtractTest()
        {
            // Action
            string result = _instance.Extract(DEST_FOLDER);

            // Post-Conditions
            Assert.AreEqual(Path.Combine(DEST_FOLDER, _instance.FullName), result, "The result is wrong.");
            Assert.IsTrue(File.Exists(Path.Combine(DEST_FOLDER, _instance.FullName)), "The file does not exist.");
        }

        [Test]
        public void ExtractWithDifferentNameTest()
        {
            // Pre-Conditions
            const string strNewFileName = "lalala.txt";

            // Action
            _instance.Extract(DEST_FOLDER, strNewFileName);

            // Post-Conditions
            Assert.IsFalse(File.Exists(Path.Combine(DEST_FOLDER, _instance.FullName)), "The file is extrcated with original name.");
            Assert.IsTrue(File.Exists(Path.Combine(DEST_FOLDER, strNewFileName)), "The extracted file name is wrong.");
        }
        #endregion
    }
}