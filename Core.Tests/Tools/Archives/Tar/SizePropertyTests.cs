﻿#region Using directives
using System;
using System.IO;
using System.Linq;
using System.Text;
using Allag.Core.Reflection;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools.Archives.Tar
{
    [TestFixture]
    public class SizePropertyTests
    {
        #region Tests
        [Test]
        public void Ctor_NullSetter_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => new SizeProperty(null));
        }

        [Test]
        public void Write_Test()
        {
            // Pre-Conditions
            const long lSize = 100;
            byte[] container = new byte[137];
            const byte bFiller = 0x01;
            for (int i = 0; i < container.Length; i++)
            {
                container[i] = bFiller;
            }
            TarItem tarItem = new TarItem(new TarFile(), new MemoryStream(), "fileName").SetProperty(x => x.Size, lSize);
            SizeProperty instance = new SizeProperty((item, value) => {});

            // Action
            instance.Write(tarItem, container);

            // Post-Conditions
            Assert.IsTrue(container.Take(container.Length - 13).All(x => x == bFiller), "The container start is wrong.");
            Assert.AreEqual(container[container.Length - 1], bFiller, "The container end is wrong.");
            byte[] result = container.Skip(container.Length - 13).Take(12).ToArray();
            Assert.AreEqual(new byte[] { 48, 48, 48, 48, 48, 48, 48, 48, 49, 52, 52, 0 }, result, "The container is wrong.");
        }

        [Test]
        public void Write_GreaterThan8Gb_Test()
        {
            // Pre-Conditions
            const long lSize = 9000000000;
            byte[] container = new byte[137];
            const byte bFiller = 0x01;
            for (int i = 0; i < container.Length; i++)
            {
                container[i] = bFiller;
            }
            TarItem tarItem = new TarItem(new TarFile(), new MemoryStream(), "fileName").SetProperty(x => x.Size, lSize);
            SizeProperty instance = new SizeProperty((item, value) => { });

            // Action
            instance.Write(tarItem, container);

            // Post-Conditions
            Assert.IsTrue(container.Take(container.Length - 13).All(x => x == bFiller), "The container start is wrong.");
            Assert.AreEqual(container[container.Length - 1], bFiller, "The container end is wrong.");
            byte[] result = container.Skip(container.Length - 13).Take(12).ToArray();
            Assert.AreEqual(new byte[] { 128, 0, 0, 0, 0, 0, 0, 2, 24, 113, 26, 0 }, result, "The container is wrong.");
        }

        [Test]
        public void Read_Test()
        {
            // Pre-Conditions
            const long lSize = 100;
            byte[] container = new byte[137];
            const byte bFiller = 0x01;
            for (int i = 0; i < container.Length; i++)
            {
                container[i] = bFiller;
            }
            Array.Copy(new byte[] { 48, 48, 48, 48, 48, 48, 48, 48, 49, 52, 52, 0 }, 0, container, container.Length - 13, 12);
            long result = 0;
            TarItem tarItem = new TarItem(new TarFile(), new MemoryStream(), "fileName");
            SizeProperty instance = new SizeProperty((item, value) =>
            {
                result = value;
            });

            // Action
            instance.Read(tarItem, container);

            // Post-Conditions
            Assert.AreEqual(lSize, result);
        }

        [Test]
        public void Read_GreaterThan8Gb_Test()
        {
            // Pre-Conditions
            const long lSize = 9000000000;
            byte[] container = new byte[137];
            const byte bFiller = 0x01;
            for (int i = 0; i < container.Length; i++)
            {
                container[i] = bFiller;
            }
            Array.Copy(new byte[] { 128, 0, 0, 0, 0, 0, 0, 2, 24, 113, 26, 0 }, 0, container, container.Length - 13, 12);
            long result = 0;
            TarItem tarItem = new TarItem(new TarFile(), new MemoryStream(), "fileName");
            SizeProperty instance = new SizeProperty((item, value) =>
            {
                result = value;
            });

            // Action
            instance.Read(tarItem, container);

            // Post-Conditions
            Assert.AreEqual(lSize, result);
        }
        #endregion
    }
}