﻿#region Using directives
using System;
using System.IO;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools.Archives.Tar
{
    [TestFixture]
    public class PropertyTests
    {
        #region Tests
        [Test]
        public void Ctor_WrongPosition_Test()
        {
            // Action
            Assert.Throws<ArgumentOutOfRangeException>(() => new Property<int>(-1, 10, item => 1, (item, i) => { }, (value, toBase) => string.Empty, (value, toBase) => 2));
        }

        [Test]
        public void Ctor_WrongLength_Test()
        {
            // Action
            Assert.Throws<ArgumentOutOfRangeException>(() => new Property<int>(0, -1, item => 1, (item, i) => { }, (value, toBase) => string.Empty, (value, toBase) => 2));
        }

        [Test]
        public void Ctor_NullGetter_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => new Property<int>(0, 10, null, (item, i) => { }, (value, toBase) => string.Empty, (value, toBase) => 2));
        }

        [Test]
        public void Ctor_NullSetter_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => new Property<int>(0, 10, item => 1, null, (value, toBase) => string.Empty, (value, toBase) => 2));
        }

        [Test]
        public void Ctor_NullConvertTo_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => new Property<int>(0, 10, item => 1, (item, i) => { }, null, (value, toBase) => 2));
        }

        [Test]
        public void Ctor_NullConvertFrom_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => new Property<int>(0, 10, item => 1, (item, i) => { }, (value, toBase) => string.Empty, null));
        }

        [Test]
        public void Ctor_Test()
        {
            // Action
            Property<int> instance = new Property<int>(0, 10, item => 1, (item, i) => { }, (value, toBase) => string.Empty, (value, toBase) => 2);

            // Post-Conditions
            Assert.AreEqual('0', instance.Filler, "The property 'Filler' is wrong.");
            CollectionAssert.AreEqual(new char[] {' ', '\0'}, instance.TrimChars, "The property 'TrimChars' is wrong.");
            Assert.IsTrue(instance.AlignToRight, "The property 'AlignToRight' is wrong.");
        }

        [Test]
        public void Ctor_WithTrimChars_Test()
        {
            // Pre-Conditions
            char[] trimChars = new char[] {'1', '2'};

            // Action
            Property<int> instance = new Property<int>(0, 10, item => 1, (item, i) => { }, (value, toBase) => string.Empty, (value, toBase) => 2, trimChars);

            // Post-Conditions
            Assert.AreEqual('0', instance.Filler, "The property 'Filler' is wrong.");
            CollectionAssert.AreEqual(trimChars, instance.TrimChars, "The property 'TrimChars' is wrong.");
            Assert.IsTrue(instance.AlignToRight, "The property 'AlignToRight' is wrong.");
        }

        [Test]
        public void Write_NullItem_Test()
        {
            // Pre-Conditions
            Property<int> instance = new Property<int>(0, 10, item => 1, (item, i) => { }, (value, toBase) => string.Empty, (value, toBase) => 2);

            // Action
            Assert.Throws<ArgumentNullException>(() => instance.Write(null, new byte[10]));
        }

        [Test]
        public void Write_NullContainer_Test()
        {
            // Pre-Conditions
            Property<int> instance = new Property<int>(0, 10, item => 1, (item, i) => { }, (value, toBase) => string.Empty, (value, toBase) => 2);

            // Action
            Assert.Throws<ArgumentNullException>(() => instance.Write(new TarItem(new TarFile(), new MemoryStream(), "tar_file.tar"), null));
        }

        [Test]
        public void Write_WrongContainer_Test()
        {
            // Pre-Conditions
            Property<int> instance = new Property<int>(0, 10, item => 1, (item, i) => { }, (value, toBase) => string.Empty, (value, toBase) => 2);

            // Action
            Assert.Throws<ArgumentOutOfRangeException>(() => instance.Write(new TarItem(new TarFile(), new MemoryStream(), "tar_file.tar"), new byte[1]));
        }

        [Test]
        public void Write_SmallContainer_Test()
        {
            // Pre-Conditions
            const int nValue = 12;
            byte[] container = new byte[4];
            Property<int> instance = new Property<int>(0, 4, item => nValue,
                (item, i) => { },
                (value, toBase) =>
                {
                    Assert.AreEqual(nValue, value, "The value for converter to is wrong.");
                    return "12345";
                }, (value, toBase) => 2);

            // Action
            Assert.Throws<InvalidOperationException>(() => instance.Write(new TarItem(new TarFile(), new MemoryStream(), "tar_file.tar"), container));
        }

        [Test]
        public void Write_Test()
        {
            // Pre-Conditions
            const int nValue = 12;
            byte[] container = new byte[10];
            Property<int> instance = new Property<int>(0, 10, item => nValue,
                (item, i) => { },
                (value, toBase) =>
                {
                    Assert.AreEqual(nValue, value, "The value for converter to is wrong.");
                    return value.ToString();
                }, (value, toBase) => 2);

            // Action
            instance.Write(new TarItem(new TarFile(), new MemoryStream(), "tar_file.tar"), container);

            // Post-Conditions
            Assert.AreEqual(new byte[] {48, 48, 48, 48, 48, 48, 48, 49, 50, 0}, container);
        }

        [Test]
        public void Read_NullItem_Test()
        {
            // Pre-Conditions
            Property<int> instance = new Property<int>(0, 10, item => 1, (item, i) => { }, (value, toBase) => string.Empty, (value, toBase) => 2);

            // Action
            Assert.Throws<ArgumentNullException>(() => instance.Read(null, new byte[10]));
        }

        [Test]
        public void Read_NullContainer_Test()
        {
            // Pre-Conditions
            Property<int> instance = new Property<int>(0, 10, item => 1, (item, i) => { }, (value, toBase) => string.Empty, (value, toBase) => 2);

            // Action
            Assert.Throws<ArgumentNullException>(() => instance.Read(new TarItem(new TarFile(), new MemoryStream(), "tar_file.tar"), null));
        }

        [Test]
        public void Read_WrongContainer_Test()
        {
            // Pre-Conditions
            Property<int> instance = new Property<int>(0, 10, item => 1, (item, i) => { }, (value, toBase) => string.Empty, (value, toBase) => 2);

            // Action
            Assert.Throws<ArgumentOutOfRangeException>(() => instance.Read(new TarItem(new TarFile(), new MemoryStream(), "tar_file.tar"), new byte[1]));
        }

        [Test]
        public void Read_Test()
        {
            // Pre-Conditions
            const int expected = 12;
            int result = 0;
            byte[] container = new byte[] { 48, 48, 48, 48, 48, 48, 48, 49, 50, 0 };
            Property<int> instance = new Property<int>(0, 10, item => expected,
                (item, i) =>
                {
                    result = i;
                },
                (value, toBase) => value.ToString(), (value, toBase) =>
                {
                   Assert.AreEqual(expected.ToString(), value, "The value for converter to is wrong.");
                    return int.Parse(value);
                });

            // Action
            instance.Read(new TarItem(new TarFile(), new MemoryStream(), "tar_file.tar"), container);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }
        #endregion
    }
}