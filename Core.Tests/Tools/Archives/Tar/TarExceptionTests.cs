﻿#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools.Archives.Tar
{
    [TestFixture]
    public class TarExceptionTests
    {
        #region Tests
        [Test]
        public void Ctor_WithMessage_Test()
        {
            // Pre-Conditions
            const string strMessage = "message";

            // Action
            TarException instance = new TarException(strMessage);

            // Post-Conditions
            Assert.AreEqual(strMessage, instance.Message, "The property 'Message' is wrong.");
        }

        [Test]
        public void Ctor_WithInnerException_Test()
        {
            // Pre-Conditions
            const string strMessage = "message";
            Exception exception = new Exception();

            // Action
            TarException instance = new TarException(strMessage, exception);

            // Post-Conditions
            Assert.AreEqual(strMessage, instance.Message, "The property 'Message' is wrong.");
            Assert.AreEqual(exception, instance.InnerException, "The property 'InnerException' is wrong.");
        }
        #endregion
    }
}