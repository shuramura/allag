﻿#region Using directives
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools.Archives.Tar
{
    [TestFixture]
    public class PathPropertyTests
    {
        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Action
            PathProperty instance = new PathProperty(0, 20, item => item.Name, (item, value) => { });

            // Post-Conditions
            Assert.IsFalse(instance.AlignToRight, "The property 'AlignToRight' is wrong.");
        }

        [Test]
        public void Write_Test()
        {
            // Pre-Conditions
            const string strValue = @"1\2|3/4";
            byte[] container = new byte[20];
            PathProperty instance = new PathProperty(0, 20, item => strValue, (item, value) => { });

            // Action
            instance.Write(new TarItem(new TarFile(), new MemoryStream(), "fileName"), container);

            // Post-Conditions
            CollectionAssert.AreEqual(Encoding.ASCII.GetBytes("1/2/3/4").Concat(new byte[13]), container);
        }

        [Test]
        public void Read_Test()
        {
            // Pre-Conditions
            byte[] container = Encoding.ASCII.GetBytes(@"1\2|3/4").Concat(new byte[13]).ToArray();
            string result = null;
            PathProperty instance = new PathProperty(0, 20, item => string.Empty, (item, value) => { result = value; });

            // Action
            instance.Read(new TarItem(new TarFile(), new MemoryStream(), "fileName"), container);

            // Post-Conditions
            Assert.AreEqual(@"1\2\3\4", result);
        }
        #endregion
    }
}