﻿#region Using directives
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools.Archives.Tar
{
    [TestFixture]
    public class StringPropertyTests
    {
        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Action
            StringProperty instance = new StringProperty(0, 10, item => string.Empty, (item, value) => { });

            // Post-Conditions
            Assert.AreEqual(0, instance.Filler, "The property 'Filler' is wrong.");
        }
        #endregion
    }
}