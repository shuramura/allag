﻿#region Using directives
using System;
using System.Collections;
using System.IO;
using System.IO.Compression;
using System.Linq;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools.Archives
{
    [TestFixture]
    public class ZipFileTests
    {
        #region Constants
        private const string BASE_FOLDER = @".\TestData\Tools\Archives\";
        public const string ZipFile = BASE_FOLDER + "normal.zip";
        private const string NOT_ZIP_FILE = BASE_FOLDER + "NotZip.dat";
        #endregion

        #region Variables
        private ZipFile _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new ZipFile(ZipFile, true);
        }

        [TearDown]
        public void TearDown()
        {
            foreach (string file in Directory.GetFiles(BASE_FOLDER, "*.temp"))
            {
                File.Delete(file);
            }

            foreach (string folder in Directory.GetDirectories(BASE_FOLDER, "*.temp"))
            {
                Directory.Delete(folder, true);
            }
            _instance.Dispose();
            GC.WaitForPendingFinalizers();
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.IsTrue(_instance.IsReadOnly, "The property 'IsReadOnly' is wrong.");
            Assert.AreEqual(CompressionLevel.Optimal, _instance.CompressionLevel, "The property 'CompressionLevel' is wrong.");
            Assert.AreEqual(4, _instance.Count(), "The file count is wrong.");
        }

        [Test]
        public void CtorDefaultTest()
        {
            // Action
            ZipFile instance = new ZipFile();

            // Post-Conditions
            Assert.IsTrue(instance.IsReadOnly, "The property 'IsReadOnly' is wrong.");
            Assert.AreEqual(CompressionLevel.Optimal, instance.CompressionLevel, "The property 'CompressionLevel' is wrong.");
            Assert.That(() => instance.AddFile(ZipFile), Throws.InstanceOf<ObjectDisposedException>());
        }

        [Test]
        public void CtorNewFileTest()
        {
            // Pre-Conditions
            string strFileName = BASE_FOLDER + Guid.NewGuid() + ".temp";

            // Action
            using(ZipFile instance = new ZipFile(strFileName, false))
            {
                // Post-Conditions
                Assert.IsFalse(instance.IsReadOnly, "The property 'IsReadOnly' is wrong.");
                Assert.AreEqual(CompressionLevel.Optimal, instance.CompressionLevel, "The property 'CompressionLevel' is wrong.");
                Assert.IsTrue(File.Exists(strFileName), "The file does not exist.");
            }
        }

        [Test]
        public void CtorWithWrongParameterTest([Values("", null, "   ")] string file)
        {
            // Action
            Assert.Throws<ArgumentNullOrEmptyException>(() => new ZipFile(file, true));
        }

        [Test]
        public void CtorWithNullStreamTest()
        {
            // Action
            Assert.That(() => new ZipFile(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CtorNotZipStreamTest()
        {
            using(MemoryStream stream = new MemoryStream(new byte[] {0, 1, 2, 3, 4, 5}))
            {
                // Action
                Assert.That(() => new ZipFile(stream), Throws.InstanceOf<InvalidDataException>());
            }
        }

        [Test]
        public void CtorNotZipFileTest()
        {
            // Action
            Assert.That(() => new ZipFile(NOT_ZIP_FILE, true), Throws.InstanceOf<InvalidDataException>());
        }

        [Test]
        public void DisposeTest()
        {
            // Action
            _instance.Dispose();

            // Post-Conditions
            Assert.That(() => _instance.Count(), Throws.InstanceOf<ObjectDisposedException>());
        }

        [Test]
        public void GetEnumeratorTest()
        {
            // Action 
            IEnumerator result = ((IEnumerable)_instance).GetEnumerator();

            // Post-Conditions
            int i = 0;
            while (result.MoveNext())
            {
                Assert.AreEqual(((ZipItem)result.Current).FullName, _instance.ElementAt(i).FullName, "The element '{0}' is wrong.", i);
                i++;
            }

            Assert.AreEqual(4, i, "The count is wrong.");
        }

        [Test]
        public void CompressionLevelSetTest()
        {
            // Action
            _instance.CompressionLevel = CompressionLevel.Fastest;

            // Post-Conditions
            Assert.AreEqual(CompressionLevel.Fastest, _instance.CompressionLevel);
        }

        [Test]
        public void AddFileNewArchiveTest()
        {
            // Pre-Conditions
            string strFileName = BASE_FOLDER + Guid.NewGuid() + ".temp";
            using(ZipFile zipFile = new ZipFile(strFileName, false))
            {
                // Action
                zipFile.AddFile(ZipFile);
            }

            // Post-Conditions
            using(ZipFile zipFile = new ZipFile(strFileName, true))
            {
                Assert.AreEqual(1, zipFile.Count(), "The file count is wrong.");
                Assert.AreEqual(ZipFile.Replace("..\\", ""), zipFile.First().FullName, "The file name is wrong.");
            }
        }

        [Test]
        public void AddFileExistArchiveTest()
        {
            // Pre-Conditions
            const string strFileName = ZipFile + ".temp";
            File.Copy(ZipFile, strFileName);
            using(ZipFile zipFile = new ZipFile(strFileName, false))
            {
                // Action
                zipFile.AddFile(ZipFile);
            }

            // Post-Conditions
            using(IArchiveFile zipFile = _instance.Open(strFileName))
            {
                Assert.AreEqual(5, zipFile.Count(), "The file count is wrong.");
                Assert.AreEqual("file1.txt", zipFile.First().FullName, "The file name is wrong.");
            }
        }

        [Test, Sequential]
        public void AddFileWithTest([Values(null, "", "   ")] string fileName)
        {
            // Pre-Conditions
            string strFileName = BASE_FOLDER + Guid.NewGuid() + ".temp";
            using(ZipFile zipFile = new ZipFile(strFileName, false))
            {
                // Action
                Assert.That(() => zipFile.AddFile(fileName), Throws.ArgumentException);
            }
        }

        [Test]
        public void AddFileToStreamTest()
        {
            // Pre-Conditions
            using(MemoryStream stream = new MemoryStream())
            {
                using(ZipFile zipFile = new ZipFile(stream))
                {
                    // Action
                    zipFile.AddFile(ZipFile);
                }

                // Post-Conditions
                using(IArchiveFile zipFile = _instance.Open(stream))
                {
                    Assert.AreEqual(1, zipFile.Count(), "The file count is wrong.");
                    Assert.AreEqual(ZipFile.Replace("..\\", ""), zipFile.First().FullName, "The file name is wrong.");
                }
            }
        }

        [Test]
        public void AddFileToStreamAsArrayTest()
        {
            // Pre-Conditions
            using(MemoryStream stream = new MemoryStream())
            {
                using(ZipFile zipFile = new ZipFile(stream))
                {
                    using(MemoryStream data = new MemoryStream(new byte[] {1, 2, 3, 4, 5}))
                    {
                        // Action
                        zipFile.AddFile(ZipFile, data);
                    }
                }

                // Post-Conditions
                using(ZipFile zipFile = new ZipFile(stream))
                {
                    Assert.AreEqual(1, zipFile.Count(), "The file count is wrong.");
                    Assert.AreEqual(ZipFile.Replace("..\\", ""), zipFile.First().FullName, "The file name is wrong.");
                }
            }
        }

        [Test]
        public void AddFileToStreamNullDataTest()
        {
            // Pre-Conditions
            using(MemoryStream stream = new MemoryStream())
            {
                using(ZipFile zipFile = new ZipFile(stream))
                {
                    // Action
                    Assert.That(() => zipFile.AddFile(ZipFile, (Stream)null), Throws.InstanceOf<ArgumentNullException>());
                }
            }
        }

        [Test]
        public void AddFolderTest()
        {
            // Pre-Conditions
            string strFileName = BASE_FOLDER + Guid.NewGuid() + ".temp";
            string strArchiveFolder = Guid.NewGuid() + ".temp";
            string strFolder = BASE_FOLDER + strArchiveFolder;
            DirectoryInfo rootInfo = Directory.CreateDirectory(strFolder);
            DirectoryInfo info = rootInfo.CreateSubdirectory(Guid.NewGuid().ToString());
            File.Copy(ZipFile, Path.Combine(info.FullName, Path.GetFileName(ZipFile)));
            File.Copy(ZipFile, Path.Combine(rootInfo.FullName, Path.GetFileName(ZipFile)));

            using(ZipFile instance = new ZipFile(strFileName, false))
            {
                // Action
                instance.AddFolder(strFolder, SearchOption.AllDirectories);
            }

            // Post-Conditions
            using(ZipFile instance = new ZipFile(strFileName, false))
            {
                Assert.AreEqual(2, instance.Count());
            }
        }

        [Test]
        public void ExtractAll_NullDestinationFolder_Test()
        {
            // Action
            Assert.That(() => _instance.ExtractAll(null, item => { }), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void ExtractAll_Test()
        {
            // Pre-Conditions
            string strFolder = BASE_FOLDER + Guid.NewGuid() + ".temp";
            int nFileCount = 0;
            int nFolderCount = 0;

            // Action
            _instance.ExtractAll(strFolder, item =>
            {
                if (item.IsFolder)
                {
                    nFolderCount++;
                }
                else
                {
                    nFileCount++;
                }
            });

            // Post-Conditions
            Assert.AreEqual(2, Directory.GetFileSystemEntries(strFolder).Length, "The root folder is wrong.");
            Assert.AreEqual(2, nFolderCount, "The folder count is wrong.");
            Assert.AreEqual(2, nFileCount, "The file count is wrong.");
        }
        #endregion
    }
}