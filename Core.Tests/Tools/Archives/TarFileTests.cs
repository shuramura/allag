﻿#region Using directives
using System;
using System.Collections;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Allag.Core.Tools.Archives.Tar;
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools.Archives
{
    [TestFixture]
    public class TarFileTests
    {
        #region Constants
        private const string BASE_FOLDER = @".\TestData\Tools\Archives\";
        private const string ARCHIVE_FILE = BASE_FOLDER + "normal.tar";
        private const string NOT_ARCHIVE_FILE = BASE_FOLDER + "NotZip.dat";
        #endregion

        #region Variables
        private TarFile _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = (TarFile)new TarFile().Open(ARCHIVE_FILE, true);
        }

        [TearDown]
        public void TearDown()
        {
            foreach (string file in Directory.GetFiles(BASE_FOLDER, "*.temp"))
            {
                File.Delete(file);
            }

            foreach (string folder in Directory.GetDirectories(BASE_FOLDER, "*.temp"))
            {
                Directory.Delete(folder, true);
            }
            _instance.Dispose();
            GC.WaitForPendingFinalizers();
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_ReadOnly_Test()
        {
            // Post-Conditions
            Assert.IsTrue(_instance.IsReadOnly, "The property 'IsReadOnly' is wrong.");
            Assert.AreEqual(CompressionLevel.NoCompression, _instance.CompressionLevel, "The property 'CompressionLevel' is wrong.");
            Assert.AreEqual(4, _instance.Count(), "The file count is wrong.");
        }

        [Test]
        public void Ctor_Test()
        {
            // Action
            TarFile instance = new TarFile();

            // Post-Conditions
            Assert.IsTrue(instance.IsReadOnly, "The property 'IsReadOnly' is wrong.");
            Assert.AreEqual(CompressionLevel.NoCompression, instance.CompressionLevel, "The property 'CompressionLevel' is wrong.");
            Assert.Throws<ObjectDisposedException>(() => instance.AddFile(ARCHIVE_FILE));
        }

        [Test]
        public void Ctor_NewFile_Test()
        {
            // Pre-Conditions
            string strFileName = BASE_FOLDER + Guid.NewGuid() + ".temp";

            // Action
            using(TarFile instance = (TarFile)new TarFile().Open(strFileName, false))
            {
                // Post-Conditions
                Assert.IsFalse(instance.IsReadOnly, "The property 'IsReadOnly' is wrong.");
                Assert.AreEqual(CompressionLevel.NoCompression, instance.CompressionLevel, "The property 'CompressionLevel' is wrong.");
                Assert.IsTrue(File.Exists(strFileName), "The file does not exist.");
            }
        }

        [Test]
        public void Ctor_WithWrongParameter_Test([Values("", null, "   ")] string file)
        {
            // Action
            Assert.Throws<ArgumentNullOrEmptyException>(() => new TarFile().Open(file, true));
        }

        [Test]
        public void Ctor_WithNullStream_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => new TarFile().Open(null));
        }

        [Test]
        public void Ctor_NotTarStream_Test()
        {
            using(MemoryStream stream = new MemoryStream(new byte[] {0, 1, 2, 3, 4, 5}))
            {
                // Action
                Assert.Throws<TarException>(() => new TarFile().Open(stream));
            }
        }

        [Test]
        public void Ctor_NotTarFile_Test()
        {
            // Action
            Assert.Throws<TarException>(() => new TarFile().Open(NOT_ARCHIVE_FILE, true));
        }

        [Test]
        public void DisposeTest()
        {
            // Action
            _instance.Dispose();

            // Post-Conditions
            Assert.Throws<ObjectDisposedException>(() => _instance.Count());
        }

        [Test]
        public void GetEnumeratorTest()
        {
            // Action 
            IEnumerator result = ((IEnumerable)_instance).GetEnumerator();

            // Post-Conditions
            int i = 0;
            while (result.MoveNext())
            {
                Assert.AreEqual(((TarItem)result.Current).FullName, _instance.ElementAt(i).FullName, "The element '{0}' is wrong.", i);
                i++;
            }

            Assert.AreEqual(4, i, "The count is wrong.");
        }

        [Test]
        public void CompressionLevel_Set_Test()
        {
            // Action
            Assert.Throws<NotSupportedException>(() => _instance.CompressionLevel = CompressionLevel.Fastest);
        }

        [Test]
        public void AddFile_NewArchive_Test()
        {
            // Pre-Conditions
            string strFileName = BASE_FOLDER + Guid.NewGuid() + ".temp";
            using(IArchiveFile archiveFile = new TarFile().Open(strFileName, false))
            {
                // Action
                archiveFile.AddFile(ARCHIVE_FILE);
            }

            // Post-Conditions
            using(IArchiveFile archiveFile = new TarFile().Open(strFileName, true))
            {
                Assert.AreEqual(1, archiveFile.Count(), "The file count is wrong.");
                Assert.AreEqual(ARCHIVE_FILE, archiveFile.First().FullName, "The file name is wrong.");
            }
        }

        [Test]
        public void AddFile_FromStreamWronDestinationFile_Test([Values(null, "", "  ")]string fileName)
        {
            // Pre-Conditions
            const string strFileName = ARCHIVE_FILE + ".temp";
            File.Copy(ARCHIVE_FILE, strFileName);
            using (IArchiveFile archiveFile = new TarFile().Open(strFileName, false))
            {
                // Action
                Assert.Throws<ArgumentNullOrEmptyException>(() => archiveFile.AddFile(fileName, new MemoryStream()));
            }
        }

        [Test]
        public void AddFile_ExistArchive_Test()
        {
            // Pre-Conditions
            const string strFileName = ARCHIVE_FILE + ".temp";
            File.Copy(ARCHIVE_FILE, strFileName);
            using(IArchiveFile archiveFile = new TarFile().Open(strFileName, false))
            {
                // Action
                archiveFile.AddFile(ARCHIVE_FILE);
            }

            // Post-Conditions
            using(IArchiveFile archiveFile = _instance.Open(strFileName))
            {
                Assert.AreEqual(5, archiveFile.Count(), "The file count is wrong.");
                Assert.AreEqual("Folder1", archiveFile.First().FullName, "The file name is wrong.");
            }
        }

        [Test]
        public void AddFile_WrongFileName_Test([Values(null, "", "   ")] string fileName)
        {
            // Pre-Conditions
            string strFileName = BASE_FOLDER + Guid.NewGuid() + ".temp";
            using(IArchiveFile archiveFile = new TarFile().Open(strFileName, false))
            {
                // Action
                Assert.Throws<ArgumentNullOrEmptyException>(() => archiveFile.AddFile(fileName));
            }
        }

        [Test]
        public void AddFile_ToStream_Test()
        {
            // Pre-Conditions
            using(MemoryStream stream = new MemoryStream())
            {
                using(IArchiveFile archiveFile = new TarFile().Open(stream))
                {
                    // Action
                    archiveFile.AddFile(ARCHIVE_FILE);
                }

                // Post-Conditions
                stream.Seek(0, SeekOrigin.Begin);
                using(IArchiveFile archiveFile = _instance.Open(stream))
                {
                    Assert.AreEqual(1, archiveFile.Count(), "The file count is wrong.");
                    Assert.AreEqual(ARCHIVE_FILE, archiveFile.First().FullName, "The file name is wrong.");
                }
            }
        }

        [Test]
        public void AddFile_ToStreamAsArray_Test()
        {
            // Pre-Conditions
            using(MemoryStream stream = new MemoryStream())
            {
                using(IArchiveFile archiveFile = new TarFile().Open(stream))
                {
                    using(MemoryStream data = new MemoryStream(new byte[] {1, 2, 3, 4, 5}))
                    {
                        // Action
                        archiveFile.AddFile(ARCHIVE_FILE, data);
                    }
                }

                // Post-Conditions
                stream.Seek(0, SeekOrigin.Begin);
                using(IArchiveFile archiveFile = new TarFile().Open(stream))
                {
                    Assert.AreEqual(1, archiveFile.Count(), "The file count is wrong.");
                    Assert.AreEqual(ARCHIVE_FILE, archiveFile.First().FullName, "The file name is wrong.");
                }
            }
        }

        [Test]
        public void AddFileToStreamNullDataTest()
        {
            // Pre-Conditions
            using(MemoryStream stream = new MemoryStream())
            {
                using(IArchiveFile archiveFile = new TarFile().Open(stream))
                {
                    // Action
                    Assert.Throws<ArgumentNullException>(() => archiveFile.AddFile(ARCHIVE_FILE, (Stream)null));
                }
            }
        }

        [Test]
        public void AddFolder_Test()
        {
            // Pre-Conditions
            string strFileName = BASE_FOLDER + Guid.NewGuid() + ".temp";
            string strArchiveFolder = Guid.NewGuid() + ".temp";
            string strFolder = BASE_FOLDER + strArchiveFolder;
            DirectoryInfo rootInfo = Directory.CreateDirectory(strFolder);
            DirectoryInfo info = rootInfo.CreateSubdirectory(Guid.NewGuid().ToString());
            File.Copy(ARCHIVE_FILE, Path.Combine(info.FullName, Path.GetFileName(ARCHIVE_FILE)));
            File.Copy(ARCHIVE_FILE, Path.Combine(rootInfo.FullName, Path.GetFileName(ARCHIVE_FILE)));

            using(IArchiveFile archiveFile = new TarFile().Open(strFileName, false))
            {
                // Action
                archiveFile.AddFolder(strFolder, SearchOption.AllDirectories);
            }

            // Post-Conditions
            using(IArchiveFile archiveFile = new TarFile().Open(strFileName, false))
            {
                Assert.AreEqual(2, archiveFile.Count());
            }
        }

        [Test]
        public void ExtractAll_NullDestinationFolder_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => _instance.ExtractAll(null, item => { }));
        }

        [Test]
        public void ExtractAll_Test()
        {
            // Pre-Conditions
            string strFolder = BASE_FOLDER + Guid.NewGuid() + ".temp";
            int nFileCount = 0;
            int nFolderCount = 0;

            // Action
            _instance.ExtractAll(strFolder, item =>
            {
                if (item.IsFolder)
                {
                    nFolderCount++;
                }
                else
                {
                    nFileCount++;
                }
            });

            // Post-Conditions
            Assert.AreEqual(2, Directory.GetFileSystemEntries(strFolder).Length, "The root folder is wrong.");
            Assert.AreEqual(2, nFolderCount, "The folder count is wrong.");
            Assert.AreEqual(2, nFileCount, "The file count is wrong.");
        }
        #endregion
    }
}