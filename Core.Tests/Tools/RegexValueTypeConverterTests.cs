﻿#region Using directives
using NUnit.Framework;

#endregion

namespace Allag.Core.Tools
{
    [TestFixture]
    public class RegexValueTypeConverterTests : BaseTypeConverterTests<RegexValueTypeConverter, RegexValue>
    {
        #region Implementation of BaseTypeConverterTests<RegexValueTypeConverter, RegexValue>
        [Test]
        public override void ConvertFromStringTest()
        {
            // Pre-Conditions
            const string strTemplate = "template";
            RegexValue expected = new RegexValue(strTemplate);

            // Action
            RegexValue result = (RegexValue)Instance.ConvertFrom(null, null, strTemplate);

            // Post-Conditions
            Assert.AreEqual(expected.Template, result.Template, "The property 'Template' is wrong.");
        }

        [Test]
        public override void ConvertToTypeStringTest()
        {
            // Pre-Conditions
            const string expected = "template";
            RegexValue regexValue = new RegexValue(expected);

            // Action
            string result = (string)Instance.ConvertTo(null, null, regexValue, typeof(string));

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        protected override RegexValue ValueOfType
        {
            get { return new RegexValue("template"); }
        }
        #endregion
    }
}