﻿#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core
{
    [TestFixture]
    public class ArgumentNullOrEmptyExceptionTests
    {
        #region Constants
        private const string MESSAGE_FORMAT = "The argument cannot be null, empty or contain only white space.";
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Action
            ArgumentNullOrEmptyException instance = new ArgumentNullOrEmptyException();

            // Post-Conditions
            Assert.AreEqual(MESSAGE_FORMAT, instance.Message, "The property 'Message' is wrong.");
            Assert.IsNull(instance.ParamName, "The property 'ParamName' is wrong.");
            Assert.IsNull(instance.InnerException, "The property 'InnerException' is wrong.");
        }

        [Test]
        public void Ctor_WithParameter_Test()
        {
            // Pre-Conditions
            const string strParameterName = "parameter";

            // Action
            ArgumentNullOrEmptyException instance = new ArgumentNullOrEmptyException(strParameterName);

            // Post-Conditions
            StringAssert.StartsWith(MESSAGE_FORMAT, instance.Message, "The property 'Message' is wrong.");
            Assert.AreEqual(strParameterName, instance.ParamName, "The property 'ParamName' is wrong.");
            Assert.IsNull(instance.InnerException, "The property 'InnerException' is wrong.");
        }

        [Test]
        public void Ctor_WithMessageAndParameterName_Test()
        {
            // Pre-Conditions
            const string strMessage = "blablabla";
            const string strParameterName = "parameter";

            // Action
            ArgumentNullOrEmptyException instance = new ArgumentNullOrEmptyException(strMessage, strParameterName);

            // Post-Conditions
            StringAssert.StartsWith(MESSAGE_FORMAT + Environment.NewLine + strMessage, instance.Message, "The property 'Message' is wrong.");
            Assert.AreEqual(strParameterName, instance.ParamName, "The property 'ParamName' is wrong.");
            Assert.IsNull(instance.InnerException, "The property 'InnerException' is wrong.");
        }

        [Test]
        public void Ctor_WithMessageAndException_Test()
        {
            // Pre-Conditions
            const string strMessage = "blablabla";
            Exception exception = new Exception();

            // Action
            ArgumentNullOrEmptyException instance = new ArgumentNullOrEmptyException(strMessage, exception);

            // Post-Conditions
            StringAssert.StartsWith(MESSAGE_FORMAT + Environment.NewLine + strMessage, instance.Message, "The property 'Message' is wrong.");
            Assert.IsNull(instance.ParamName, "The property 'ParamName' is wrong.");
            Assert.AreEqual(exception, instance.InnerException, "The property 'InnerException' is wrong.");
        }

        [Test]
        public void Ctor_WithMessageParameterNameAndException_Test()
        {
            // Pre-Conditions
            const string strMessage = "blablabla";
            const string strParameterName = "parameter";
            Exception exception = new Exception();

            // Action
            ArgumentNullOrEmptyException instance = new ArgumentNullOrEmptyException(strMessage, strParameterName, exception);

            // Post-Conditions
            StringAssert.StartsWith(MESSAGE_FORMAT + Environment.NewLine + strMessage, instance.Message, "The property 'Message' is wrong.");
            Assert.AreEqual(strParameterName, instance.ParamName, "The property 'ParamName' is wrong.");
            Assert.AreEqual(exception, instance.InnerException, "The property 'InnerException' is wrong.");
        }
        #endregion
    }
}