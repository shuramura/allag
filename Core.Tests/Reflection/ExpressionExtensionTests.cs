#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Reflection;
using NUnit.Framework;

#endregion

namespace Allag.Core.Reflection
{
    [TestFixture]
    public class ExpressionExtensionTests
    {
        #region Initialization
        [SetUp]
        public void SetUp() {}

        [TearDown]
        public void TearDown() {}
        #endregion

        #region Tests
        [Test]
        public void HandleMethodExpressionTest()
        {
            // Pre-Conditions
            Expression<Action<string>> expression = x => x.IndexOf(' ');

            // Action
            string strMethodName = null;
            int nArgCount = 0;
            bool result = expression.Body.HandleMethodExpression(delegate(MethodInfo info, IList<Expression> args)
            {
                strMethodName = info.Name;
                nArgCount = args.Count;
            });

            // Post-Conditions
            Assert.IsTrue(result, "The result is wrong.");
            Assert.AreEqual("IndexOf", strMethodName, "The name of the method is wrong.");
            Assert.AreEqual(1, nArgCount, "The count of the arguments is wrong.");
        }

        [Test]
        public void HandleMethodExpressionWrongExpressionTypeTest()
        {
            // Pre-Conditions
            Expression<Action<string>> expression = x => x.IndexOf(' ');

            // Action
            bool isHandled = false;
            bool result = expression.HandleMethodExpression((info, args) => isHandled = true);

            // Post-Conditions
            Assert.IsFalse(result, "The result is wrong.");
            Assert.IsFalse(isHandled, "The handler is wrong.");
        }

        [Test]
        public void HandleMethodExpressionWithTypeTest()
        {
            // Pre-Conditions
            Expression<Action<string>> expression = x => x.IndexOf(' ');

            // Action
            string strMethodName = null;
            int nArgCount = 0;
            bool result =
                expression.Body.HandleMethodExpression<string>(delegate(MethodInfo info, IList<Expression> args)
                {
                    strMethodName = info.Name;
                    nArgCount = args.Count;
                });

            // Post-Conditions
            Assert.IsTrue(result, "The result is wrong.");
            Assert.AreEqual("IndexOf", strMethodName, "The name of the method is wrong.");
            Assert.AreEqual(1, nArgCount, "The count of the arguments is wrong.");
        }

        [Test]
        public void HandleMethodExpressionWithWrongTypeTest()
        {
            // Pre-Conditions
            Expression<Action<string>> expression = x => x.IndexOf(' ');
            bool isHandled = false;

            // Action
            Assert.That(() => expression.Body.HandleMethodExpression<int>((info, args) => isHandled = true), Throws.InvalidOperationException);
        }

        [Test]
        public void HandleMethodExpressionNullHandlerTest()
        {
            // Pre-Conditions
            Expression<Action<string>> expression = x => x.IndexOf(' ');

            // Action
            Assert.That(() => expression.Body.HandleMethodExpression<string>(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void HandleMethodExpressionBaseClassTest()
        {
            // Pre-Conditions
            Expression<Action<Parent>> expression = x => x.Method();

            // Action
            bool isHandled = false;
            bool result = expression.Body.HandleMethodExpression<Child>((info, args) => isHandled = true);

            // Post-Conditions
            Assert.IsTrue(result, "The result is wrong.");
            Assert.IsTrue(isHandled, "The handler is wrong.");
        }

        [Test]
        public void JoinExpressionNullExpressionsTest()
        {
            // Action
            Assert.That(() => ((IEnumerable<Expression<Func<bool>>>)null).JoinExpression(), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        [Sequential]
        public void JoinExpressionTest([Values(true, false)] bool joinByAnd, [Values(false, true)] bool expected)
        {
            // Pre-Conditions
            Expression<Func<bool>> trueFunc = () => true;
            Expression<Func<bool>> falseFunc = () => false;

            // Action
            bool result = new Expression<Func<bool>>[] {trueFunc, falseFunc}.JoinExpression(joinByAnd).Compile()();

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void AreEqual_True_Test()
        {
            // Pre-Conditions
            Expression<Func<bool>> func0 = () => true;
            Expression<Func<bool>> func1 = () => true;

            // Action
            bool result = ExpressionExtension.AreEqual(func0, func1);

            // Post-Conditions
            Assert.IsTrue(result);
        }

        [Test]
        public void AreEqual_False_Test()
        {
            // Pre-Conditions
            Expression<Func<bool>> func0 = () => true;
            Expression<Func<bool>> func1 = () => false;

            // Action
            bool result = ExpressionExtension.AreEqual(func0, func1);

            // Post-Conditions
            Assert.IsFalse(result);
        }

        [Test]
        public void AreEqual_BothExpressonNull_Test()
        {
            // Pre-Conditions
            Expression<Func<bool>> func0 = () => true;
            Expression<Func<bool>> func1 = () => false;

            // Action
            bool result = ExpressionExtension.AreEqual((Expression<Func<bool>>)null, null);

            // Post-Conditions
            Assert.IsTrue(result);
        }

        [Test]
        public void AreEqual_ExpressonNull_Test()
        {
            // Pre-Conditions
            Expression<Func<bool>> func = () => true;

            // Action
            bool result = ExpressionExtension.AreEqual(null, func);

            // Post-Conditions
            Assert.IsFalse(result);
        }

        [Test]
        public void GetMemberInfo_NullExpression_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => ExpressionExtension.GetMemberInfo(((Expression<Func<ExpressionExtensionTests, int>>)null)));
        }

        [Test]
        public void GetMemberInfo_Test()
        {
            // Pre-Conditions
            Expression<Func<ExpressionExtensionTests, int>> property = x => (byte)x.Test;

            // Action
            MemberInfo result = ExpressionExtension.GetMemberInfo(property);

            // Post-Conditions
            Assert.AreEqual(typeof(ExpressionExtensionTests).GetProperty("Test"), result);
        }

        [Test]
        public void GetMemberInfo_WrongExpression_Test()
        {
            // Pre-Conditions
            Expression<Func<ExpressionExtensionTests, string>> property = x => x.Test.ToString();

            // Action
            Assert.Throws<InvalidDataException>(() => ExpressionExtension.GetMemberInfo(property));
        }
        #endregion

        #region Properties
        public int Test { get; set; }
        #endregion

        #region Classes
        public class Parent
        {
            #region Methods
            public void Method() {}
            #endregion
        }

        public class Child : Parent {}
        #endregion
    }
}