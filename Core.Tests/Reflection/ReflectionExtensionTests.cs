#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Reflection;
using System.Xml.Serialization;
using Allag.Core.Data.Persistence;
using NUnit.Framework;

#endregion

namespace Allag.Core.Reflection
{
    [TestFixture]
    [XmlInclude(typeof(object))]
    [XmlInclude(typeof(object))]
    [XmlInclude(typeof(object))]
    public class ReflectionExtensionTests
    {
        #region Variables
        private readonly int _test;
        private readonly int _nTest;
        private static FileAttributes _staticTest;
        public int TestField;
        #endregion

        #region Constructors
        public ReflectionExtensionTests()
        {
            _test = 0;
        }

        public ReflectionExtensionTests(int test, int test1)
        {
            _test = test;
        }

        public ReflectionExtensionTests(int one, int two, int test = 10)
        {
            _test = 0;
        }
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp() {}

        [TearDown]
        public void TearDown() {}
        #endregion

        #region Tests
        [Test]
        public void HandleAttribute_ForAllAttributes_Test()
        {
            // Pre-Conditions
            int result = 0;

            // Action
            GetType().HandleAttribute<XmlIncludeAttribute>(delegate
            {
                result++;
                return true;
            });
            // Post-Conditions
            Assert.AreEqual(3, result);
        }

        [Test]
        public void HandleAttribute_ForOneAttribute_Test()
        {
            // Pre-Conditions
            int result = 0;

            // Action
            GetType().HandleAttribute<XmlIncludeAttribute>(delegate
            {
                result++;
                return false;
            });
            // Post-Conditions
            Assert.AreEqual(1, result);
        }

        [Test]
        public void HandleAttribute_ForNotExistAttribute_Test()
        {
            // Pre-Conditions
            int result = 0;

            // Action
            GetType().HandleAttribute<FlagsAttribute>(delegate
            {
                result++;
                return false;
            });
            // Post-Conditions
            Assert.AreEqual(0, result);
        }

        [Test]
        public void HandleMemeber_All_Test()
        {
            // Pre-Conditions
            int result = 0;

            // Action
            GetType().GetProperties().HandleMember(delegate
            {
                result++;
                return true;
            });

            // Post-Conditions
            Assert.AreEqual(5, result);
        }

        [Test]
        public void HandleMemeber_One_Test()
        {
            // Pre-Conditions
            int result = 0;

            // Action
            GetType().GetProperties().HandleMember(delegate
            {
                result++;
                return false;
            });
            // Post-Conditions
            Assert.AreEqual(1, result);
        }

        [Test]
        public void GetConstructorByParameterNames_NullType_Test()
        {
            // Action
            Assert.That(() => ReflectionExtension.GetConstructorByParameterNames(null, new[] {"test"}), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void GetConstructorByParameterNames_Test()
        {
            // Action
            ConstructorInfo result = GetType().GetConstructorByParameterNames(new[] {"test1", "test"});

            // Post-Conditions
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetConstructorByParameterNames_NotExist_Test()
        {
            // Action
            ConstructorInfo result = GetType().GetConstructorByParameterNames(new[] {"lala"});

            // Post-Conditions
            Assert.IsNull(result);
        }

        [Test]
        public void GetMethodByParameterNames_NullMethodList_Test()
        {
            // Action
            Assert.That(() => ReflectionExtension.GetMethodByParameterNames(null, new[] {"lala"}), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void GetMethodByParameterNames_NullParameterList_Test()
        {
            // Action
            Assert.That(() => GetType().GetMethods().GetMethodByParameterNames(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void GetMethodByParameterNames_WithDefaultParameters_Test()
        {
            // Action
            MethodBase result = GetType().GetConstructors().GetMethodByParameterNames(new[] {"one", "two"});

            // Post-Conditions
            Assert.IsNotNull(result);
        }

        [Test]
        public void SetProperty_NullObject_Test()
        {
            // Action
            Assert.That(() => ((ReflectionExtensionTests)null).SetProperty(x => x.Test2, FileAttributes.ReadOnly), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void SetProperty_Static_Test()
        {
            // Pre-Conditions
            const FileAttributes expected = FileAttributes.Archive;

            // Action
            ReflectionExtension.SetProperty(() => StaticTest2, expected);

            // Post-Conditions
            Assert.AreEqual(expected, StaticTest2);
        }

        [Test]
        public void SetProperty_SetterOnly_Test()
        {
            // Pre-Conditions
            const int expected = 10;

            // Action
            this.SetProperty(x => x.Test, expected);
           
            // Post-Conditions
            Assert.AreEqual(expected, Test);
        }

        [Test]
        public void SetProperty_SetterOnlyStatic_Test()
        {
            // Pre-Conditions
            const FileAttributes expected = FileAttributes.Archive;

            // Action
            ReflectionExtension.SetProperty(() => StaticTest, expected);

            // Post-Conditions
            Assert.AreEqual(expected, StaticTest);
        }

        [Test]
        public void SetProperty_SetterOnlyWrongBackFieldName_Test()
        {
            // Action
            Assert.Throws<InvalidOperationException>(() => this.SetProperty(x => x.Test1, 10));
        }

        [Test]
        public void SetProperty_NullExpression_Test()
        {
            // Action
            Assert.That(() => this.SetProperty(null, 10), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void SetProperty_WrongBody_Test()
        {
            // Action
            Assert.That(() => this.SetProperty(x => 100, 10), Throws.InvalidOperationException);
        }

        [Test]
        public void SetProperty_WrongObjectMember_Test()
        {
            // Action
            Assert.That(() => this.SetProperty(x => x._test, 10), Throws.InvalidOperationException);
        }

        [Test]
        public void SetPropertyTest()
        {
            // Pre-Conditions
            const FileAttributes expected = FileAttributes.ReadOnly | FileAttributes.Hidden | FileAttributes.Archive;

            // Action
            ReflectionExtensionTests result = this.SetProperty(x => (int)x.Test2, (int)expected);

            // Post-Conditions
            Assert.AreEqual(expected, Test2, "The property value is wrong.");
            Assert.AreEqual(this, result, "The result is wrong.");
        }

        [Test]
        public void CreateGetHandlerNullMemberTest()
        {
            // Action
            Assert.That(() => ((MemberInfo)null).CreateGetHandler<ReflectionExtensionTests>(), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CreateGetHandlerWrongMemberTest()
        {
            // Action
            Assert.That(() => GetType().GetMethod("CreateGetHandlerWrongMemberTest").CreateGetHandler<ReflectionExtensionTests>(), Throws.InvalidOperationException);
        }

        [Test]
        public void CreateGetHandlerPropertyTest()
        {
            // Pre-Conditions 
            const FileAttributes expected = FileAttributes.NotContentIndexed;
            Test2 = expected;

            // Action
            Func<object, object> result = GetType().GetProperty("Test2").CreateGetHandler<ReflectionExtensionTests>();

            // Post-Conditions
            Assert.AreEqual(expected, result(this));
        }

        [Test]
        public void CreateGetHandler_Expresiont_Test()
        {
            // Pre-Conditions 
            const FileAttributes expected = FileAttributes.NotContentIndexed;
            Test2 = expected;
            Expression<Func<ReflectionExtensionTests, FileAttributes>> exp = x => x.Test2;

            // Action
            Func<object, object> result = exp.CreateGetHandler<ReflectionExtensionTests, ReflectionExtensionTests, FileAttributes>();

            // Post-Conditions
            Assert.AreEqual(expected, result(this));
        }

        [Test]
        public void CreateGetHandlerFieldTest()
        {
            // Pre-Conditions 
            const int expected = 100;
            TestField = expected;

            // Action
            Func<object, object> result = GetType().GetField("TestField").CreateGetHandler<ReflectionExtensionTests>();

            // Post-Conditions
            Assert.AreEqual(expected, result(this));
        }

        [Test]
        public void CreateSetHandlerNullMemberTest()
        {
            // Action
            Assert.That(() => ((MemberInfo)null).CreateSetHandler<ReflectionExtensionTests>(), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CreateSetHandlerWrongMemberTest()
        {
            // Action
            Assert.That(() => GetType().GetMethod("CreateGetHandlerWrongMemberTest").CreateSetHandler<ReflectionExtensionTests>(), Throws.InvalidOperationException);
        }

        [Test]
        public void CreateSetHandler_Property_Test()
        {
            // Pre-Conditions 
            const FileAttributes expected = FileAttributes.NotContentIndexed;

            // Action
            Action<object, object> result = GetType().GetProperty("Test2").CreateSetHandler<ReflectionExtensionTests>();

            // Post-Conditions
            result(this, expected);
            Assert.AreEqual(expected, Test2);
        }

        [Test]
        public void CreateSetHandler_Expression_Test()
        {
            // Pre-Conditions 
            Expression<Func<ReflectionExtensionTests, FileAttributes>> exp = x => x.Test2;
            const FileAttributes expected = FileAttributes.NotContentIndexed;

            // Action
            Action<object, object> result = exp.CreateSetHandler<ReflectionExtensionTests, ReflectionExtensionTests, FileAttributes>();

            // Post-Conditions
            result(this, expected);
            Assert.AreEqual(expected, Test2);
        }

        [Test]
        public void CreateSetHandlerFieldTest()
        {
            // Pre-Conditions 
            const int expected = 100;

            // Action
            Action<object, object> result = GetType().GetField("TestField").CreateSetHandler<ReflectionExtensionTests>();

            // Post-Conditions
            result(this, expected);
            Assert.AreEqual(expected, TestField);
        }

        [Test]
        public void CreateEqualityAndHashCodeHandlers_Test()
        {
            // Pre-Conditions
            Func<TestClass, TestClass, bool> equalityHandler;
            Func<TestClass, int> hashCodeHandler;

            // Action
            ReflectionExtension.CreateEqualityAndHashCodeHandlers<TestClass, BusinessKeyAttribute>(out equalityHandler, out hashCodeHandler);

            // Post-Conditions
            TestClass one = new TestClass(10, 20) {FieldPubInt = 15, PropPubInt = 25, FieldInt = 11, PropInt = 22, Ints = new List<int?> {1, null, 3, 4}, String = "lalala"};
            TestClass two = new TestClass(10, 20) {FieldPubInt = 15, PropPubInt = 25, FieldInt = 12, PropInt = 23, Ints = new List<int?> {1, null, 3, 4}, String = "lalala"};

            Assert.IsTrue(equalityHandler(one, two), "The equality handler is wrong.");
            two = new TestClass(10, 25) {FieldPubInt = 15, PropPubInt = 25, FieldInt = 12, PropInt = 23};
            Assert.IsFalse(equalityHandler(one, two), "The equality handler is wrong.");
            Assert.AreEqual(-1473148946, hashCodeHandler(one), "The hash code handler is wrong.");
        }

        [Test]
        public void CreateEqualityAndHashCodeHandlers_WithNullValue_Test()
        {
            // Pre-Conditions
            Func<TestClass, TestClass, bool> equalityHandler;
            Func<TestClass, int> hashCodeHandler;

            // Action
            ReflectionExtension.CreateEqualityAndHashCodeHandlers<TestClass, BusinessKeyAttribute>(out equalityHandler, out hashCodeHandler);

            // Post-Conditions
            TestClass one = new TestClass(10, 20) {FieldPubInt = 15, PropPubInt = 25, FieldInt = 11, PropInt = 22};
            TestClass two = new TestClass(10, 20) {FieldPubInt = 15, PropPubInt = 25, FieldInt = 12, PropInt = 23};

            Assert.IsTrue(equalityHandler(one, two), "The equality handler is wrong.");
            two = new TestClass(10, 25) {FieldPubInt = 15, PropPubInt = 25, FieldInt = 12, PropInt = 23};
            Assert.IsFalse(equalityHandler(one, two), "The equality handler is wrong.");
            Assert.AreEqual(-596106466, hashCodeHandler(one), "The hash code handler is wrong.");
        }

        [Test]
        public void CreateEqualityAndHashCodeHandlers_WithoutAttribute_Test()
        {
            // Pre-Conditions
            Func<ReflectionExtensionTests, ReflectionExtensionTests, bool> equalityHandler;
            Func<ReflectionExtensionTests, int> hashCodeHandler;

            // Action
            ReflectionExtension.CreateEqualityAndHashCodeHandlers<ReflectionExtensionTests, BusinessKeyAttribute>(out equalityHandler, out hashCodeHandler);

            // Post-Conditions

            ReflectionExtensionTests one = new ReflectionExtensionTests();
            ReflectionExtensionTests two = new ReflectionExtensionTests();

            Assert.IsTrue(equalityHandler(one, one), "The equality handler is wrong.");
            Assert.IsFalse(equalityHandler(one, two), "The equality handler is wrong.");
            Assert.AreEqual(0, hashCodeHandler(one), "The hash code handler is wrong.");
            Assert.AreEqual(0, hashCodeHandler(null), "The hash code handler is wrong for null object.");
        }

        [Test]
        public void ToDictionary_NullObject_Test()
        {
            // Action
            IReadOnlyDictionary<string, object> result = ReflectionExtension.ToDictionary(null);

            // Post-Conditions
            Assert.AreEqual(0, result.Count, "The count is wrong.");
        }

        [Test]
        public void ToDictionary_Test()
        {
            // Pre-Conditions
            const string strKey = "test";
            const string strValue = "Test";
            object @obj = new {test = strValue};

            // Action
            IReadOnlyDictionary<string, object> result = ReflectionExtension.ToDictionary(@obj);

            // Post-Conditions
            Assert.AreEqual(1, result.Count, "The count is wrong.");
            Assert.IsTrue(result.ContainsKey(strKey), "The key is wrong.");
            Assert.AreEqual(strValue, result[strKey], "The value is wrong.");
        }

        [Test]
        public void ToDictionary_ListNonPublicField_Test()
        {
            // Pre-Conditions
            const string strKey = "FieldProtInt";
            const int nValue = 20;
            TestClass obj = new TestClass(nValue, 10);

            // Action
            IReadOnlyDictionary<string, object> result = ReflectionExtension.ToDictionary(@obj, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);

            // Post-Conditions
            Assert.AreEqual(1, result.Count, "The count is wrong.");
            Assert.IsTrue(result.ContainsKey(strKey), "The key is wrong.");
            Assert.AreEqual(nValue, result[strKey], "The value is wrong.");
        }

        [Test]
        public void ToDictionary_ListNonPublicProperty_Test()
        {
            // Pre-Conditions
            const string strKey = "PropProtInt";
            const int nValue = 20;
            TestClass obj = new TestClass(10, nValue);

            // Action
            IReadOnlyDictionary<string, object> result = ReflectionExtension.ToDictionary(@obj, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetProperty);

            // Post-Conditions
            Assert.AreEqual(1, result.Count, "The count is wrong.");
            Assert.IsTrue(result.ContainsKey(strKey), "The key is wrong.");
            Assert.AreEqual(nValue, result[strKey], "The value is wrong.");
        }
        #endregion

        #region Properties
        public int Test
        {
            get { return _test; }
        }

        public int Test1
        {
            get { return _nTest; }
        }

        public FileAttributes Test2 { get; private set; }
        #endregion

        #region Static properties
        public static FileAttributes StaticTest2 { get; private set; }
        public static FileAttributes StaticTest { get { return _staticTest; } }
        #endregion


        #region Classes
        public class TestClass
        {
            #region Constructors
            public TestClass(int fieldProtInt, int propProtInt)
            {
                FieldProtInt = fieldProtInt;
                PropProtInt = propProtInt;
            }
            #endregion

            #region Variables
            [BusinessKey]
            public int? FieldPubInt;

            [BusinessKey]
            protected int FieldProtInt;

            public int FieldInt;
            #endregion

            #region Properties
            [BusinessKey]
            public int PropPubInt { get; set; }

            [BusinessKey]
            protected int PropProtInt { get; set; }

            [BusinessKey]
            public List<int?> Ints { get; set; }

            [BusinessKey]
            public string String { get; set; }

            public int PropInt { get; set; }
            #endregion
        }
        #endregion
    }
}