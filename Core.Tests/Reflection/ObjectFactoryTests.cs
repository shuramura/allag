#region Using directives

using System;
using Allag.Core.Configuration.Reflection;
using Allag.Core.TestData.Reflection;
using NUnit.Framework;

#endregion

namespace Allag.Core.Reflection
{
    [TestFixture]
    public class ObjectFactoryTests : IObjectFactoryTests
    {
        #region Initialization
        [TearDown]
        public void TearDown()
        {
            ObjectFactory.Remove<IObjectFactoryTests>(this);
            ObjectFactory.Remove<IObjectFactoryTests>();
        }
        #endregion

        #region Tests
        [Test]
        public void CreateTest()
        {
            // Action
            ITest result = ObjectFactory.Create<ITest>(this);

            // Post-Conditions
            Assert.IsInstanceOf(typeof(TestClass), result);
        }

        [Test]
        public void CreateRealClassWhichNotInTheConfigurationTest()
        {
            // Action
            ObjectFactoryTests  result = ObjectFactory.Create<ObjectFactoryTests>(this);

            //Post-Conditions
            Assert.IsNotNull(result);
        }

        [Test]
        public void CreateByInterfaceWhichDoesNotExistTest()
        {
            // Action
            IDisposable result = ObjectFactory.Create<IDisposable>(this);

            // Post-Conditions
            Assert.IsNull(result);
        }

        [Test]
        public void CreateWithNullInterfaceTest()
        {
            // Action
            Assert.That(() => ObjectFactory.Create(null, GetType()), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void CreateWithNullOwnerTest()
        {
            // Action
            Assert.That(() => ObjectFactory.Create<ITest>((object)null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void GetTest()
        {
            // Pre-Conditions 
            ITest expected = ObjectFactory.Get<ITest>(this);

            // Action
            ITest result = ObjectFactory.Get<ITest>(this);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetCommonObjectForDifferentOwnerTest()
        {
            // Pre-Conditions 
            ITest expected = ObjectFactory.Get<ITest>("123456");

            // Action
            ITest result = ObjectFactory.Get<ITest>(new object());

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetForDifferentOwnersTest()
        {
            // Action
            ITest result0 = ObjectFactory.Get<ITest>(this);
            ITest result1 = ObjectFactory.Get<ITest>(typeof(ObjectElementTests));

            // Post-Conditions
            Assert.AreNotEqual(result0, result1);
        }

        [Test]
        public void GetWithNullOwnerTest()
        {
            // Action
            Assert.That(() => ObjectFactory.Get<ITest>((object)null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void GetWithNullInterfaceTest()
        {
            // Action
            Assert.That(() => ObjectFactory.Get(null, GetType()), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void AddTest()
        {
            // Action
            ObjectFactory.Add<IObjectFactoryTests, ObjectFactoryTests>(this);

            // Post-Conditions
            Assert.AreEqual(this, ObjectFactory.Get<IObjectFactoryTests>(this));
        }

        [Test]
        public void AddExistingTest()
        {
            // Pre-Conditions
            ObjectFactoryTests expected = new ObjectFactoryTests();
            ObjectFactory.Add<IObjectFactoryTests, ObjectFactoryTests>(this);

            // Action
            ObjectFactory.Add<IObjectFactoryTests, ObjectFactoryTests>(expected);

            // Post-Conditions
            Assert.AreEqual(expected, ObjectFactory.Get<IObjectFactoryTests>(this));
        }

        [Test]
        public void AddNullTest()
        {
            // Action
            ObjectFactory.Add<IObjectFactoryTests>(null);

            //Post-Conditions
            Assert.IsNull(ObjectFactory.Get<IObjectFactoryTests>(null));
        }

        [Test]
        public void ContainsTest()
        {
            // Pre-Conditions
            ObjectFactory.Add<IObjectFactoryTests>(() => null);

            // Action
            bool result = ObjectFactory.Contains<IObjectFactoryTests>();

            //Post-Conditions
            Assert.IsTrue(result);
        }

        [Test]
        public void RemoveTest()
        {
            // Pre-Conditions
            ObjectFactory.Add<IObjectFactoryTests>(null);

            // Action
            ObjectFactory.Remove<IObjectFactoryTests>();

            //Post-Conditions
            Assert.IsNull(ObjectFactory.Get<IObjectFactoryTests>(null));
        }
        #endregion
    }

    #region Interfaces
    public interface IObjectFactoryTests
    {

    }
    #endregion
}
