﻿#region Using directives

using System.Reflection;
using System.Runtime.InteropServices;

#endregion

[assembly: AssemblyCompanyAttribute("Alexander Lagun")]
[assembly: AssemblyCopyright("Copyright © Alexander Lagun 2008-2015")]
[assembly: AssemblyProduct("Allag")]
[assembly: AssemblyTrademark("")]

[assembly: AssemblyConfiguration("")]

[assembly: ComVisible(false)]

[assembly: AssemblyVersion("2.0.1506.0")]
[assembly: AssemblyFileVersion("2.0.15231.1618")]
