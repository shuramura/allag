﻿namespace Allag.Core.Configuration.Net
{
    public class SftpElement : ServerElement
    {
        #region Constructors
        public SftpElement()
            : base(22, 100000) {}
        #endregion

        #region Implementation of ServerElement
        public override string UriScheme
        {
            get { return "sftp"; }
        }
        #endregion

    }
}