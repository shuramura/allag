﻿#region Using directives
using System;
using Renci.SshNet.Sftp;

#endregion

namespace Allag.Core.Net
{
    public class SftpItem : IItem
    {
        #region Constructors
        public SftpItem(SftpFile file)
        {
            if(file == null)
            {
                throw new ArgumentNullException("file");
            }

            Original = file;

            Parent = file.FullName.Replace(file.Name, string.Empty);
            Type = file.IsRegularFile || file.Length > 0 ? ItemType.File : ItemType.Directory;
        }
        #endregion

        #region Implementation of IItem
        public string Path
        {
            get { return FtpClient.GetFtpPath(Parent, FullName); }
        }

        public string Parent { get; private set; }

        public ItemType Type { get; private set; }

        public string FullName { get { return Original.Name; } }

        public long Size { get { return Original.Length; } }

        public DateTime LastModified { get { return Original.LastWriteTime; } }
        #endregion

        #region Properties
        public SftpFile Original { get; private set; }
        #endregion

    }
}