﻿#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using Renci.SshNet.Common;
using Renci.SshNet.Sftp;

#endregion

namespace Allag.Core.Net
{
    public class SftpClient : IFileClient
    {
        #region Variables
        private string _strHost;
        #endregion

        #region Constructors
        public SftpClient(string host)
        {
            Host = host;
        }
        #endregion

        #region Implementation of IFileClient
        public IEnumerable<string> Download(string sourceFolder, string mask, Func<int, IItem, Stream, string> saveFileHndler, Func<IItem, bool> isDownloadItemHandler = null, Func<IItem, bool> isDeleteItemHandler = null)
        {
            using(LogBlock.New<SftpClient>(new {sourceFolder, mask}))
            {
                if (saveFileHndler == null)
                {
                    throw new ArgumentNullException("saveFileHndler");
                }

                using(Renci.SshNet.SftpClient client = GetSftpClient())
                {
                    int nIndex = 0;
                    foreach (SftpItem item in GetDirectoryList(client, sourceFolder, mask).Select(x => new SftpItem(x)))
                    {
                        if ((item.Original.IsRegularFile || item.Size > 0) && (isDownloadItemHandler == null || isDownloadItemHandler(item)))
                        {
                            using(MemoryStream stream = new MemoryStream())
                            {
                                client.DownloadFile(item.Original.FullName, stream);
                                stream.Seek(0, SeekOrigin.Begin);
                                string strFile = saveFileHndler(nIndex++, item, stream);
                                LogBlock.Logger.Info("The file '{0}' was saved. The original name is {1}.", strFile, item.FullName);

                                if (isDeleteItemHandler != null && isDeleteItemHandler(item))
                                {
                                    item.Original.Delete();
                                }
                                yield return strFile;
                            }
                        }
                    }
                }
            }
        }

        public void Upload(string destinationFolder, Func<int, Tuple<string, Stream>> getFileSource, Action<int, Stream> releaseResourse)
        {
            using(LogBlock.New<SftpClient>(new {destinationFolder}))
            {
                using(Renci.SshNet.SftpClient client = GetSftpClient())
                {
                    int nIndex = 0;
                    while (true)
                    {
                        Tuple<string, Stream> item = getFileSource(nIndex);
                        if (item != null)
                        {
                            try
                            {
                                item.Item2.Seek(0, SeekOrigin.Begin);
                                string strPath = FtpClient.GetFtpPath(destinationFolder, item.Item1);
                                client.UploadFile(item.Item2, strPath);
                                LogBlock.Logger.Info("The file '{0}' was uploaded to {1}.", item.Item1, strPath);
                            }
                            finally
                            {
                                if (releaseResourse != null)
                                {
                                    releaseResourse(nIndex, item.Item2);
                                }
                                nIndex++;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
        }

        public void Delete(string folder, string itemName)
        {
            using(LogBlock.New<SftpClient>(new {folder, fileName = itemName}))
            {
                using(Renci.SshNet.SftpClient client = GetSftpClient())
                {
                    string strPath = FtpClient.GetFtpPath(folder, itemName);
                    client.DeleteFile(strPath);
                    LogBlock.Logger.Info("The file '{0}' was deleted.", strPath);
                }
            }
        }

        public IEnumerable<IItem> GetDirectoryList(string folder, string mask)
        {
            using(LogBlock.New<SftpClient>(new {folder, mask}))
            {
                try
                {
                    using(Renci.SshNet.SftpClient client = GetSftpClient())
                    {
                        return GetDirectoryList(client, folder, mask).Select(x => new SftpItem(x));
                    }
                }
                catch (SshException exp)
                {
                    throw new InvalidDataException(string.Format("The folder '{0}' does not find.", folder), exp);
                }
            }
        }
        #endregion

        #region Properties
        public string Host
        {
            get { return _strHost; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentNullOrEmptyException("value");
                }
                _strHost = value;
            }
        }

        public int Port { get; set; }

        public int Timeout { get; set; }

        public NetworkCredential Credential { get; set; }
        #endregion

        #region Methods
        public Renci.SshNet.SftpClient GetSftpClient()
        {
            string strUserName = null;
            string strPassword = null;
            if (Credential != null)
            {
                strUserName = Credential.UserName;
                strPassword = Credential.Password;
            }
            Renci.SshNet.SftpClient ret = new Renci.SshNet.SftpClient(Host, Port, strUserName, strPassword)
            {
                OperationTimeout = new TimeSpan(0, 0, 0, 0, Timeout)
            };
            ret.Connect();
            return ret;
        }
        #endregion

        #region Static methods
        private static IEnumerable<SftpFile> GetDirectoryList(Renci.SshNet.SftpClient client, string folder, string mask)
        {
            if (string.IsNullOrEmpty(mask))
            {
                throw new ArgumentException("The parameter cannot be null or empty.", "mask");
            }

            Regex regex = new Regex(mask, RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.Singleline);
            IEnumerable<SftpFile> ret = client.ListDirectory(FtpClient.NormalizeFtpFolder(folder)).Where(x => x.Name != "." && x.Name != ".." && regex.IsMatch(x.Name));
            return ret;
        }
        #endregion
    }
}