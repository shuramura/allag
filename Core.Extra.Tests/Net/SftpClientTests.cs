﻿#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Net
{
    [TestFixture]
    [Ignore("The SFTP service is not installed.")]
    public class SftpClientTests : BaseClientTests<SftpClient>
    {
        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual("localhost", Instance.Host, "The property 'Host' is wrong.");
            Assert.AreEqual(22, Instance.Port, "The property 'Port' is wrong.");
        }

        //
        [Test]
        public void CtorAbsentParameterTest()
        {
            // Action
            Assert.That(() => new SftpClient("lalala"), Throws.InstanceOf<ArgumentNullException>());
        }
        #endregion

        #region Implementation of BaseClientTests<SftpClient>
        protected override SftpClient CreateInstance()
        {
            return new SftpClient("TestSftp");
        }
        #endregion
    }
}