﻿#region Using directives
using System;
using NUnit.Framework;

#endregion

namespace Allag.Core.Net
{
    [TestFixture]
    [Ignore("The SFTP service is not installed.")]
    public class SftpItemTests : BaseItemTests<SftpClient, SftpItem>
    {
        #region Tests
        [Test]
        public void CtorWithNullParameterTest()
        {
            // Action 
            Assert.That(() => new SftpItem(null), Throws.InstanceOf<ArgumentNullException>());
        }
        #endregion

        #region Implementation of BaseItemTests<SftpClient, SftpItem>
        protected override SftpClient CreateClientInstance()
        {
            return new SftpClient("TestSftp");
        }
        #endregion
    }
}