﻿#region Using directives
using Allag.Core.Data.Persistence;

#endregion

namespace Allag.Data.TestData.Repositories
{
    public interface IMockRepository : IRepository {}
}