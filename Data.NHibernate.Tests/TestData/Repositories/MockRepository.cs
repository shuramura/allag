﻿#region Using directives
using Allag.Core.Data.Persistence;
using Allag.Data.TestData.Entities;

#endregion

namespace Allag.Data.TestData.Repositories
{
    [Repository(Factory = typeof(IMockRepositoryFactory))]
    public class MockRepository<TEntity, TId> : Repository<IMockRepositoryFactory, int, TEntity, TId>
        where TEntity : Entity<IMockRepositoryFactory, TEntity, TId>, new()
    {
        #region Variables
        private Operations _operations;
        #endregion

        #region Constructors
        public MockRepository()
        {
            _operations = Operations.All;
        }

        public MockRepository(UnitOfWork unitOfWork) : this()
        {
            UnitOfWork = unitOfWork;
        }
        #endregion

        #region Overrides of BaseDao<TEntity,TId,IDaoFactory>
        public override Operations SupportedOperations
        {
            get
            {
                return _operations == base.SupportedOperations ? base.SupportedOperations : _operations;
            }
        }
        #endregion

        #region Methods
        public void SetSupportedOperations(Operations operations)
        {
            _operations = operations;
        }
        #endregion
    }

    [Repository(typeof(MockEntity))]
    public class MockRepository : MockRepository<MockEntity, int>, IMockRepository
    {
        #region Constructors
        public MockRepository() { }
        public MockRepository(UnitOfWork unitOfWork) : base(unitOfWork) { }
        #endregion
    }
}