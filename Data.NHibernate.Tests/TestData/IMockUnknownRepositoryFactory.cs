﻿#region Using directives
using Allag.Core.Data.Persistence;

#endregion

namespace Allag.Data.TestData
{
    public interface IMockUnknownRepositoryFactory : IRepositoryFactory<IMockRepositoryFactory, int> {}
}