﻿using Allag.Core.Data.Persistence;

namespace Allag.Data.TestData
{
    public interface IMockRepositoryFactory : IRepositoryFactory<IMockRepositoryFactory, int> { }
}