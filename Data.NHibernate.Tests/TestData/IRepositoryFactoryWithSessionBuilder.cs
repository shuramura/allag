﻿#region Using directives
using Allag.Core.Data.Persistence;
using Allag.Data.Management;

#endregion

namespace Allag.Data.TestData
{
    public interface IRepositoryFactoryWithSessionBuilder : IRepositoryFactory<IRepositoryFactoryWithSessionBuilder, int>, ISessionBuilder
    {
    }
}