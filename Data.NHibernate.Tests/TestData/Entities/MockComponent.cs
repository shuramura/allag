﻿namespace Allag.Data.TestData.Entities
{
    public class MockComponent
    {
        #region Properties
        public int Prop0 { get; set; }
        public int Prop1 { get; set; }
        public MockEntity Entity { get; set; }
        #endregion
    }
}