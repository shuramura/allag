﻿#region Using directives
using Allag.Core.Data.Persistence;

#endregion

namespace Allag.Data.TestData.Entities
{
    public class MockHasOneEntity : Entity<IMockRepositoryFactory, MockHasOneEntity, int>
    {
        #region Properties
        public virtual MockEntity Entity { get; set; }
        #endregion
    }
}