#region Using directives
using System.Collections.Generic;
using Allag.Core.Data.Persistence;

#endregion

namespace Allag.Data.TestData.Entities
{
    public class MockManyToManyEntity : Entity<IMockRepositoryFactory, MockManyToManyEntity, int>
    {
        #region Constructors
        public MockManyToManyEntity()
        {
            Parents = new List<MockEntityParent>();
        }
        #endregion

        #region Properties
        public virtual ICollection<MockEntityParent> Parents { get; protected set; }
        #endregion
    }
}