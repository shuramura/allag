#region Using directives

using System.Collections.Generic;
using Allag.Core.Data.Persistence;

#endregion

namespace Allag.Data.TestData.Entities
{
    public class MockEntityParent : Entity<IMockRepositoryFactory, MockEntityParent, int>
    {
        #region Constructors

        public MockEntityParent()
        {
            Children = new List<MockEntity>();
            ManyToMany = new List<MockManyToManyEntity>();
        }

        #endregion

        #region Properties
        public virtual int Number { get; set; }
        public virtual string String { get; set; }

        public virtual ICollection<MockEntity> Children { get; protected set; }

        public virtual ICollection<MockManyToManyEntity> ManyToMany { get; protected set; }
        #endregion
    }
}
