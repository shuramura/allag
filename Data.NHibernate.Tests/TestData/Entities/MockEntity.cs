#region Using directives
using Allag.Core.Data.Persistence;

#endregion

namespace Allag.Data.TestData.Entities
{
    public class MockEntity : Entity<IMockRepositoryFactory, MockEntity, int>
    {
        #region Properties
        [BusinessKey]
        public virtual int Number { get; set; }

        [BusinessKey]
        public virtual int? NullableNumber { get; set; }

        public virtual string String { get; set; }

        public virtual bool Boolean { get; set; }

        public virtual MockComponent Component { get; set; }

        public virtual MockEntityParent Parent { get; set; }

        public virtual MockHasOneEntity OneToOne { get; set; }
        #endregion
    }
}