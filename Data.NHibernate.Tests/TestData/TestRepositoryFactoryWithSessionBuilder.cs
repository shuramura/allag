﻿#region Using directives
using System;
using System.Xml;
using NHibernate;

#endregion

namespace Allag.Data.TestData
{
    public class TestRepositoryFactoryWithSessionBuilder : RepositoryFactory<IRepositoryFactoryWithSessionBuilder, int>, IRepositoryFactoryWithSessionBuilder
    {
        #region Implementation of ISessionBuilder
        public Tuple<NHibernate.Cfg.Configuration, ISessionFactory> BuildSessionFactory(XmlReader configuration)
        {
            NHibernate.Cfg.Configuration cfg = new NHibernate.Cfg.Configuration().Configure(configuration);
            return new Tuple<NHibernate.Cfg.Configuration, ISessionFactory>(cfg, cfg.BuildSessionFactory());
        }
        #endregion
    }
}