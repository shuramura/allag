﻿#region Using directives
using System;
using System.IO;
using System.Xml.Serialization;
using NHibernate;
using NUnit.Framework;

#endregion

namespace Allag.Data.Type
{
    [TestFixture]
    public class EnumStringTypeTests
    {
        #region Variables
        private EnumStringType<EnumStringEnum> _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new EnumStringType<EnumStringEnum>();
        }
        #endregion

        #region Tests
        [Test]
        public void GetValueTest()
        {
            // Pre-Conditions
            const string expected = "second";

            // Action
            object result = _instance.GetValue(EnumStringEnum.Two);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetValueInvalidEnumTypeTest()
        {
            // Action
            Assert.That(() =>_instance.GetValue(FileAccess.ReadWrite), Throws.ArgumentException);
        }

        [Test]
        public void GetInstanceTest()
        {
            // Pre-Conditions
            const EnumStringEnum expected = EnumStringEnum.Two;

            // Action
            object result = _instance.GetInstance("second");

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetInstanceInvalidValueTypeTest()
        {
            // Action
            Assert.That(() => _instance.GetInstance(this), Throws.InstanceOf<HibernateException>());
        }

        #endregion


        #region Enumerations
        public enum EnumStringEnum
        {
            One,
            [XmlEnum("second")]
            Two,
            Three
        }
        #endregion

    }
}