﻿#region Using directives
using System;
using System.Data;
using Allag.Core.Tools;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Data.Type
{
    [TestFixture]
    [Serializable]
    public class XmlTypeTests
    {
        #region Variables
        private MockRepository _mocks;
        private XmlType<Mock> _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _mocks = new MockRepository();
            _instance = new XmlType<Mock>();
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.AreEqual(1, _instance.SqlTypes.Length, "The property 'SqlTypes' length is wrong.");
            Assert.IsInstanceOf<XmlSqlType>(_instance.SqlTypes[0], "The property 'SqlTypes' is wrong.");
            Assert.AreEqual(typeof(Mock), _instance.ReturnedType, "The property 'ReturnedType' is wrong.");
            Assert.IsFalse(_instance.IsMutable, "The property 'IsMutable' is wrong.");
        }

        [Test]
        public void EqualsTest()
        {
            // Action 
            bool result = ((IUserType)_instance).Equals(_instance, this);

            // Post-Conditions
            Assert.IsFalse(result);
        }

        [Test]
        public void GetHashCodeTest()
        {
            // Action
            int result = _instance.GetHashCode(_instance);

            // Post-Conditions
            Assert.AreEqual(result, _instance.GetHashCode());
        }

        [Test]
        public void NullSafeGetTest()
        {
            // Pre-Conditions
            Mock mock = new Mock();

            const string strColumnName = "Column";
            const int nColumnIndex = 1;
            IDataReader dataReader = _mocks.StrictMock<IDataReader>();
            Expect.Call(dataReader.GetOrdinal(strColumnName)).Return(nColumnIndex);
            Expect.Call(dataReader.IsDBNull(nColumnIndex)).Return(false);
            Expect.Call(dataReader[nColumnIndex]).Return(mock.Serialize());
            _mocks.ReplayAll();

            // Action
            object result = _instance.NullSafeGet(dataReader, new string[] {strColumnName}, this);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.IsInstanceOf<Mock>(result);
        }

        [Test]
        public void NullSafeGetNullTest()
        {
            // Pre-Conditions
            const string strColumnName = "Column";
            const int nColumnIndex = 1;
            IDataReader dataReader = _mocks.StrictMock<IDataReader>();
            Expect.Call(dataReader.GetOrdinal(strColumnName)).Return(nColumnIndex);
            Expect.Call(dataReader.IsDBNull(nColumnIndex)).Return(true);
            _mocks.ReplayAll();

            // Action
            object result = _instance.NullSafeGet(dataReader, new string[] {strColumnName}, this);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.IsNull(result);
        }

        [Test]
        public void NullSafeSetTest()
        {
            // Pre-Conditions
            Mock mock = new Mock();
            const int nColumnIndex = 1;
            IDbDataParameter dbDataParameter = _mocks.DynamicMock<IDbDataParameter>();
            IDataParameterCollection collection = _mocks.StrictMock<IDataParameterCollection>();
            IDbCommand command = _mocks.StrictMock<IDbCommand>();
            Expect.Call(command.Parameters).Return(collection);
            Expect.Call(collection[nColumnIndex]).Return(dbDataParameter);
            dbDataParameter.Value = XmlSerializerExtension.Serialize(mock);
            _mocks.ReplayAll();

            // Action
            _instance.NullSafeSet(command, mock, nColumnIndex);

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void NullSafeSetNullTest()
        {
            // Pre-Conditions
            const int nColumnIndex = 1;
            IDataParameter dataParameter = _mocks.StrictMock<IDataParameter>();
            IDataParameterCollection collection = _mocks.StrictMock<IDataParameterCollection>();
            IDbCommand command = _mocks.StrictMock<IDbCommand>();
            Expect.Call(command.Parameters).Return(collection);
            Expect.Call(collection[nColumnIndex]).Return(dataParameter);
            dataParameter.Value = DBNull.Value;
            _mocks.ReplayAll();

            // Action
            _instance.NullSafeSet(command, null, nColumnIndex);

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void DeepCopyNotSerializableTest()
        {
            // Action
            Assert.That(() => _instance.DeepCopy(_mocks), Throws.ArgumentException);
        }

        [Test]
        public void DeepCopyTest()
        {
            // Pre-Conditions
            const string expected = "lalala";
            Mock mock = new Mock();
            mock.Prop0 = expected;

            // Action
            dynamic result = _instance.DeepCopy(mock);

            // Post-Conditions
            Assert.IsInstanceOf<Mock>(result, "The result is wrong.");
            Assert.AreEqual(expected, result.Prop0, "The result is wrong.");
        }

        [Test]
        public void Test()
        {
            // Pre-Conditions
            Mock expected = new Mock();

            // Action
            object result = _instance.Replace(expected, this, this);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void AssembleTest()
        {
            // Pre-Conditions
            const string expected = "lalala";
            Mock mock = new Mock();
            mock.Prop0 = expected;

            // Action
            dynamic result = _instance.Assemble(mock, this);

            // Post-Conditions
            Assert.IsInstanceOf<Mock>(result, "The result is wrong.");
            Assert.AreEqual(expected, result.Prop0, "The result is wrong.");
        }

        [Test]
        public void DisassembleTest()
        {
            // Pre-Conditions
            const string expected = "lalala";
            Mock mock = new Mock();
            mock.Prop0 = expected;

            // Action
            dynamic result = _instance.Disassemble(mock);

            // Post-Conditions
            Assert.IsInstanceOf<Mock>(result, "The result is wrong.");
            Assert.AreEqual(expected, result.Prop0, "The result is wrong.");
        }

        [Test]
        public void FromXMLStringTest()
        {
            // Pre-Conditions
            Mock expected = new Mock();
            expected.Prop0 = "lalala";

            // Action
            dynamic result = _instance.FromXMLString(XmlSerializerExtension.Serialize(expected));

            // Post-Conditions
            Assert.AreEqual(expected.Prop0, result.Prop0);
        }

        [Test]
        public void ToXMLStringTest()
        {
            // Pre-Conditions
            Mock mock = new Mock();
            mock.Prop0 = "lalala";
            string expected = XmlSerializerExtension.Serialize(mock);

            // Action
            string result = _instance.ToXMLString(mock);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ObjectToSQLStringNullValueTest()
        {
            // Action
            string result = _instance.ObjectToSQLString(null);

            // Post-Conditions
            Assert.IsNull(result);
        }

        [Test]
        public void ObjectToSQLStringTest()
        {
            // Pre-Conditions
            Mock mock = new Mock();
            mock.Prop0 = "lalala";
            string expected = "'" + XmlSerializerExtension.Serialize(mock) + "'";

            // Action
            string result = _instance.ObjectToSQLString(mock);

            // Post-Conditions
            Assert.AreEqual(expected, result);
        }
        #endregion

        #region Classes
        [Serializable]
        public class Mock
        {
            #region Properties
            public string Prop0 { get; set; }
            #endregion
        }
        #endregion
    }
}