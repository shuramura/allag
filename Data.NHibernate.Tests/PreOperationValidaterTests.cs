﻿#region Using directives
using Allag.Core.Data.Persistence;
using NHibernate.Event;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Data
{
    [TestFixture]
    public class PreOperationValidaterTests
    {
        #region Variables
        private MockRepository _mocks;
        private PreOperationValidater _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _mocks = new MockRepository();
            _instance = new PreOperationValidater();
        }
        #endregion

        #region Tests
        [Test]
        public void OnPreDeleteTest()
        {
            // Pre-Conditions
            IEntityPreValidater validater = _mocks.StrictMock<IEntityPreValidater>();
            validater.Validate(Operations.Delete);
            PreDeleteEvent @event = new PreDeleteEvent(validater, null, null, null, null);
            _mocks.ReplayAll();

            // Action
            _instance.OnPreDelete(@event);

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void OnPreInsertTest()
        {
            // Pre-Conditions
            IEntityPreValidater validater = _mocks.StrictMock<IEntityPreValidater>();
            validater.Validate(Operations.Add);
            PreInsertEvent @event = new PreInsertEvent(validater, null, null, null, null);
            _mocks.ReplayAll();

            // Action
            _instance.OnPreInsert(@event);

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void OnPreUpdateTest()
        {
            // Pre-Conditions
            IEntityPreValidater validater = _mocks.StrictMock<IEntityPreValidater>();
            validater.Validate(Operations.Update);
            PreUpdateEvent @event = new PreUpdateEvent(validater, null, null, null, null, null);
            _mocks.ReplayAll();

            // Action
            _instance.OnPreUpdate(@event);

            // Post-Conditions
            _mocks.VerifyAll();
        }
        #endregion
    }
}