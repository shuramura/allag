#region Using directives
using System;
using System.Linq.Expressions;
using NUnit.Framework;

#endregion

namespace Allag.Data
{
    [TestFixture]
    public class CriterionHelperTests
    {
        #region Tests
        [Test]
        public void CreateCriteriaNullSessionTest()
        {
            // Pre-Conditions 
            Expression<Func<System.Type, bool>> criteria = x => x.IsPublic;

            // Action
            Assert.That(() => ((Session)null).CreateCriteria(criteria), Throws.InstanceOf<ArgumentNullException>());
        }
        #endregion
    }
}