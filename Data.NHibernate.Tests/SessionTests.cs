﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using NHibernate;
using NHibernate.Engine;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Data
{
    [TestFixture]
    public class SessionTests
    {
        #region Variables
        private MockRepository _mocks;
        private ISessionFactory _sessionFactory;
        private ISession _session;
        private IStatelessSession _statelessSession;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _mocks = new MockRepository();
            _session = _mocks.StrictMock<ISession>();
            _statelessSession = _mocks.StrictMock<IStatelessSession>();

            _sessionFactory = _mocks.DynamicMock<ISessionFactory>();
            SetupResult.For(_sessionFactory.OpenSession()).Return(_session);
            SetupResult.For(_sessionFactory.OpenStatelessSession()).Return(_statelessSession);
            _mocks.Replay(_sessionFactory);
        }
        #endregion

        #region Tests
        [Test]
        public void CtorNullSessionFactoruTest()
        {
            // Action
            Assert.That(() => new Session(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void Ctor_Test()
        {
            //Pre-Conditions
            ISessionFactory sessionFactory = _mocks.DynamicMock<ISessionFactory>();
            IDbConnection connection = _mocks.DynamicMock<IDbConnection>();
            _session.Expect(x => x.Connection).Return(connection);
            _session.Expect(x => x.IsOpen).Return(true);
            _session.Expect(x => x.SessionFactory).Return(sessionFactory);
            _mocks.ReplayAll();

            // Action
            Session instance = new Session(_sessionFactory);

            // Post-Conditions
            Assert.IsTrue(instance.IsOpen, "The property 'IsOpen' is wrong.");
            Assert.AreEqual(sessionFactory, instance.SessionFactory, "The property 'SessionFactory' is wrong.");
            Assert.AreEqual(connection, instance.Connection, "The property 'Connection' is wrong.");
            _mocks.VerifyAll();
        }

        [Test]
        public void Ctor_StatelessSession_Test()
        {
            //Pre-Conditions
            IDbConnection connection = _mocks.DynamicMock<IDbConnection>();
            Expect.Call(_statelessSession.Connection).Return(connection);
            _mocks.ReplayAll();

            // Action
            Session instance = new Session(_sessionFactory, true);

            // Post-Conditions
            Assert.IsTrue(instance.IsOpen, "The property 'IsOpen' is wrong.");
            Assert.AreEqual(_sessionFactory, instance.SessionFactory);
            Assert.AreEqual(connection, instance.Connection, "The property 'Connection' is wrong.");
            _mocks.ReplayAll();
        }

        [Test]
        public void DisposeSessionTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory);

            _session.Dispose();
            _mocks.ReplayAll();

            // Action
            instance.Dispose();

            //Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void DisposeStatelessSessionTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory, true);

            _statelessSession.Dispose();
            _mocks.ReplayAll();

            // Action
            instance.Dispose();

            //Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void IsOpenAfterDisposingTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory);
            _session.Dispose();
            _mocks.ReplayAll();
            instance.Dispose();

            // Action
            Assert.Throws<ObjectDisposedException>(() => { bool result = instance.IsOpen; });
        }

        [Test]
        public void FlushSessionTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory);

            _session.Flush();
            _mocks.ReplayAll();

            // Action
            instance.Flush();

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void FlushStatelessSessionTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory, true);
            _mocks.ReplayAll();

            // Action
            instance.Flush();

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void ClearSessionTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory);

            _session.Clear();
            _mocks.ReplayAll();

            // Action
            instance.Clear();

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void ClearStatelessSessionTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory, true);
            _mocks.ReplayAll();

            // Action
            instance.Clear();

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void BeginTransactionSessionTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory);
            ITransaction expected = _mocks.DynamicMock<ITransaction>();

            Expect.Call(_session.BeginTransaction()).Return(expected);
            _mocks.ReplayAll();

            // Action
            ITransaction result = instance.BeginTransaction();

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void BeginTransactionStatelessSessionTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory, true);
            ITransaction expected = _mocks.DynamicMock<ITransaction>();

            Expect.Call(_statelessSession.BeginTransaction()).Return(expected);
            _mocks.ReplayAll();

            // Action
            ITransaction result = instance.BeginTransaction();

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetSessionByIdTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory);

            object id = new object();
            object expected = this;

            Expect.Call(_session.Get<SessionTests>(id)).Return(this);
            _mocks.ReplayAll();

            // Action
            object result = instance.Get<SessionTests>(id);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetStatelessSessionByIdTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory, true);

            object id = new object();
            object expected = this;

            Expect.Call(_statelessSession.Get<SessionTests>(id)).Return(this);
            _mocks.ReplayAll();

            // Action
            object result = instance.Get<SessionTests>(id);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void SaveSessionObjectTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory);

            object obj = new object();
            object expected = new object();

            Expect.Call(_session.Save(obj)).Return(expected);
            _mocks.ReplayAll();

            // Action
            object result = instance.Save(obj);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void SaveStatelessSessionObjectTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory, true);

            object obj = new object();
            object expected = new object();

            Expect.Call(_statelessSession.Insert(obj)).Return(expected);
            _mocks.ReplayAll();

            // Action
            object result = instance.Save(obj);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void UpdateSessionObjectTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory);

            object obj = new object();

            _session.Update(obj);
            _mocks.ReplayAll();

            // Action
            instance.Update(obj);

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void UpdateStatelessSessionObjectTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory, true);

            object obj = new object();
            _statelessSession.Update(obj);
            _mocks.ReplayAll();

            // Action
            instance.Update(obj);

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void DeleteSessionObjectTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory);

            object obj = new object();

            _session.Delete(obj);
            _mocks.ReplayAll();

            // Action
            instance.Delete(obj);

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void DeleteStatelessSessionObjectTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory, true);

            object obj = new object();
            _statelessSession.Delete(obj);
            _mocks.ReplayAll();

            // Action
            instance.Delete(obj);

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void CloseSessionObjectTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory);

            IDbConnection expected = _mocks.DynamicMock<IDbConnection>();

            Expect.Call(_session.Close()).Return(expected);
            _mocks.ReplayAll();

            // Action
            IDbConnection result = instance.Close();

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void CloseStatelessSessionObjectTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory, true);

            _statelessSession.Close();
            _mocks.ReplayAll();

            // Action
            IDbConnection result = instance.Close();

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.IsNull(result);
        }

        [Test]
        public void CreateCriteriaTypedSessionTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory);

            ICriteria expected = _mocks.DynamicMock<ICriteria>();

            Expect.Call(_session.CreateCriteria<SessionTests>()).Return(expected);
            _mocks.ReplayAll();

            // Action
            ICriteria result = instance.CreateCriteria<SessionTests>();

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void CreateCriteriaTypedStatelessSessionTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory, true);

            ICriteria expected = _mocks.DynamicMock<ICriteria>();

            Expect.Call(_statelessSession.CreateCriteria<SessionTests>()).Return(expected);
            _mocks.ReplayAll();

            // Action
            ICriteria result = instance.CreateCriteria<SessionTests>();

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void CreateQuerySessionTest()
        {
            // Pre-Conditions
            const string strQuery = "lalala";
            Session instance = new Session(_sessionFactory);

            IQuery expected = _mocks.DynamicMock<IQuery>();

            Expect.Call(_session.CreateQuery(strQuery)).Return(expected);
            _mocks.ReplayAll();

            // Action
            IQuery result = instance.CreateQuery(strQuery);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void CreateQueryStatelessSessionTest()
        {
            const string strQuery = "lalala";
            Session instance = new Session(_sessionFactory, true);

            IQuery expected = _mocks.DynamicMock<IQuery>();

            Expect.Call(_statelessSession.CreateQuery(strQuery)).Return(expected);
            _mocks.ReplayAll();

            // Action
            IQuery result = instance.CreateQuery(strQuery);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetNamedQuerySessionTest()
        {
            // Pre-Conditions
            const string strQueryName = "lalala";
            Session instance = new Session(_sessionFactory);

            IQuery expected = _mocks.DynamicMock<IQuery>();

            Expect.Call(_session.GetNamedQuery(strQueryName)).Return(expected);
            _mocks.ReplayAll();

            // Action
            IQuery result = instance.GetNamedQuery(strQueryName);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetNamedQueryStatelessSessionTest()
        {
            const string strQueryName = "lalala";
            Session instance = new Session(_sessionFactory, true);

            IQuery expected = _mocks.DynamicMock<IQuery>();

            Expect.Call(_statelessSession.GetNamedQuery(strQueryName)).Return(expected);
            _mocks.ReplayAll();

            // Action
            IQuery result = instance.GetNamedQuery(strQueryName);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void QueryOverSessionTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory);

            IQueryOver<SessionTests, SessionTests> expected = _mocks.DynamicMock<IQueryOver<SessionTests, SessionTests>>();

            Expect.Call(_session.QueryOver<SessionTests>()).Return(expected);
            _mocks.ReplayAll();

            // Action
            IQueryOver result = instance.QueryOver<SessionTests>();

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void QueryOverStatelessSessionTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory, true);

            IQueryOver<SessionTests, SessionTests> expected = _mocks.DynamicMock<IQueryOver<SessionTests, SessionTests>>();

            Expect.Call(_statelessSession.QueryOver<SessionTests>()).Return(expected);
            _mocks.ReplayAll();

            // Action
            IQueryOver result = instance.QueryOver<SessionTests>();

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void QueryOverWithAliasSessionTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory);

            IQueryOver<SessionTests, SessionTests> expected = _mocks.DynamicMock<IQueryOver<SessionTests, SessionTests>>();
            SessionTests testAllias = null;
            Expression<Func<SessionTests>> alias = () => testAllias;
            Expect.Call(_session.QueryOver(alias)).Return(expected);
            _mocks.ReplayAll();

            // Action
            IQueryOver result = instance.QueryOver(alias);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void QueryOverWithAliasStatelessSessionTest()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory, true);

            IQueryOver<SessionTests, SessionTests> expected = _mocks.DynamicMock<IQueryOver<SessionTests, SessionTests>>();
            SessionTests testAllias = null;
            Expression<Func<SessionTests>> alias = () => testAllias;
            Expect.Call(_statelessSession.QueryOver(alias)).Return(expected);
            _mocks.ReplayAll();

            // Action
            IQueryOver result = instance.QueryOver(alias);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Future_NullCriteria_Test([Values(false, true)] bool isLightSession)
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory, isLightSession);
            _mocks.ReplayAll();

            // Action
            Assert.Throws<ArgumentNullException>(() => instance.Future<SessionTests>(null));

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void Future_Test()
        {
            // Pre-Conditions
            SessionTests[] expected = new SessionTests[] {this};
            Session instance = new Session(_sessionFactory);
            ICriteria criteria = _mocks.StrictMock<ICriteria>();
            criteria.Expect(x => x.Future<SessionTests>()).Return(expected);
            _mocks.ReplayAll();

            // Action
            IEnumerable<SessionTests> result = instance.Future<SessionTests>(criteria);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Future_StatelessSession_Test()
        {
            // Pre-Conditions
            SessionTests[] expected = new SessionTests[] {this};
            Session instance = new Session(_sessionFactory, true);
            ICriteria criteria = _mocks.StrictMock<ICriteria>();
            criteria.Expect(x => x.List<SessionTests>()).Return(expected);
            _mocks.ReplayAll();

            // Action
            IEnumerable<SessionTests> result = instance.Future<SessionTests>(criteria);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void FutureValue_NullCriteria_Test([Values(false, true)] bool isLightSession)
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory, isLightSession);
            _mocks.ReplayAll();

            // Action
            Assert.Throws<ArgumentNullException>(() => instance.FutureValue<SessionTests>(null));

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void FutureValue_Test()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory);
            ICriteria criteria = _mocks.StrictMock<ICriteria>();
            IFutureValue<SessionTests> futureValue = _mocks.StrictMock<IFutureValue<SessionTests>>();
            futureValue.Stub(x => x.Value).Return(this);
            criteria.Expect(x => x.FutureValue<SessionTests>()).Return(futureValue);
            _mocks.ReplayAll();

            // Action
            Lazy<SessionTests> result = instance.FutureValue<SessionTests>(criteria);

            // Post-Conditions
            Assert.AreEqual(this, result.Value);
            _mocks.VerifyAll();
        }

        [Test]
        public void FutureValue_StatelessSession_Test()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory, true);
            ICriteria criteria = _mocks.StrictMock<ICriteria>();
            criteria.Expect(x => x.UniqueResult<SessionTests>()).Return(this);
            _mocks.ReplayAll();

            // Action
            Lazy<SessionTests> result = instance.FutureValue<SessionTests>(criteria);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(this, result.Value);
        }

        [Test]
        public void Get_ById_NullType_Test([Values(false, true)] bool isLightSession)
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory, isLightSession);

            // Action
            Assert.Throws<ArgumentNullException>(() => instance.Get(null, this));
        }

        [Test]
        public void Get_ById_Session_Test()
        {
            // Pre-Conditions
            object expected = new object();
            Session instance = new Session(_sessionFactory, false);
            _session.Expect(x => x.Get(GetType(), this)).Return(expected);
            _mocks.ReplayAll();

            // Action
            object result = instance.Get(GetType(), this);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Get_ById_StatelessSession_Test()
        {
            // Pre-Conditions
            object expected = new object();
            Session instance = new Session(_sessionFactory, true);
            _statelessSession.Expect(x => x.Get(GetType().FullName, this)).Return(expected);
            _mocks.ReplayAll();

            // Action
            object result = instance.Get(GetType(), this);

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void CreateCriteria_Session_Test()
        {
            // Pre-Conditions
            ICriteria expected = _mocks.DynamicMock<ICriteria>();
            Session instance = new Session(_sessionFactory, false);
            _session.Expect(x => x.CreateCriteria(GetType())).Return(expected);
            _mocks.ReplayAll();

            // Action
            object result = instance.CreateCriteria(GetType());

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void CreateCriteria_StatelessSession_Test()
        {
            // Pre-Conditions
            ICriteria expected = _mocks.DynamicMock<ICriteria>();
            Session instance = new Session(_sessionFactory, true);
            _statelessSession.Expect(x => x.CreateCriteria(GetType())).Return(expected);
            _mocks.ReplayAll();

            // Action
            object result = instance.CreateCriteria(GetType());

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetQueryable_Session_Test()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory, false);
            _session.Expect(x => x.GetSessionImplementation()).Return(_mocks.DynamicMock<ISessionImplementor>());
            _mocks.ReplayAll();

            // Action
            object result = instance.GetQueryable<SessionTests>();

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetQueryable_StatelessSession_Test()
        {
            // Pre-Conditions
            Session instance = new Session(_sessionFactory, true);
            _statelessSession.Expect(x => x.GetSessionImplementation()).Return(_mocks.DynamicMock<ISessionImplementor>());
            _mocks.ReplayAll();

            // Action
            object result = instance.GetQueryable<SessionTests>();

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.IsNotNull(result);
        }
        #endregion
    }
}