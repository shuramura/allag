﻿#region Using directives
using System;
using NHibernate;
using NUnit.Framework;
using Rhino.Mocks;

#endregion

namespace Allag.Data.Management
{
    [TestFixture]
    public class DbContextTests
    {
        #region Variables
        private MockRepository _mocks;
        private ISessionFactory _sessionFactory;
        private DbContext _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _mocks = new MockRepository();
            _sessionFactory = _mocks.StrictMock<ISessionFactory>();
            _mocks.ReplayAll();
            _instance = new DbContext(_sessionFactory);
        }
        #endregion

        #region Tests
        [Test]
        public void CtorNullSessionFactoryTest()
        {
            // Action 
            Assert.That(() => new DbContext(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void SessionGetNotOpenedSessionTest()
        {
            // Action 
            Assert.That(() => _instance.Session, Throws.InstanceOf<InvalidOperationException>());
        }

        [Test]
        public void OpenSessionOpenedSessionTest()
        {
            // Pre-Conditions
            _instance.OpenSession();

            // Action 
            Assert.That(() => _instance.OpenSession(), Throws.InstanceOf<InvalidOperationException>());
        }

        [Test]
        public void SessionGetTest()
        {
            // Pre-Conditions
            _mocks.BackToRecordAll();
            Expect.Call(_sessionFactory.OpenSession()).Return(_mocks.StrictMock<ISession>());
            _mocks.ReplayAll();
            _instance.OpenSession();

            // Action 
            Session result = _instance.Session;

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.IsNotNull(result);
        }

        [Test]
        public void SessionGetWithTransactionTest()
        {
            // Pre-Conditions
            _mocks.BackToRecordAll();
            ISession session = _mocks.StrictMock<ISession>();
            Expect.Call(session.BeginTransaction()).Return(_mocks.StrictMock<ITransaction>());
            Expect.Call(_sessionFactory.OpenSession()).Return(session);
            
            _mocks.ReplayAll();
            _instance.OpenSession();
            _instance.BeginTransaction();

            // Action 
            Session result = _instance.Session;

            // Post-Conditions
            _mocks.VerifyAll();
            Assert.IsNotNull(result);
        }

        [Test]
        public void BeginTransactionNotOpenedSessionTest()
        {
            // Action 
            Assert.That(() => _instance.BeginTransaction(), Throws.InvalidOperationException);
        }

        [Test]
        public void BeginTransactionWithoutGettingSessionTest()
        {
            // Pre-Conditions
            _instance.OpenSession();

            // Action 
            _instance.BeginTransaction();

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void BeginTransactionWithGettingSessionTest()
        {
            // Pre-Conditions
            _mocks.BackToRecordAll();
            ISession session = _mocks.StrictMock<ISession>();
            Expect.Call(session.BeginTransaction()).Return(_mocks.StrictMock<ITransaction>());
            Expect.Call(_sessionFactory.OpenSession()).Return(session);
            _mocks.ReplayAll();

            _instance.OpenSession();
            Session tempSession = _instance.Session;

            // Action 
            _instance.BeginTransaction();

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void CloseTransactionNotOpenedSessionTest()
        {
            // Action 
            Assert.That(() => _instance.CloseTransaction(), Throws.InstanceOf<InvalidOperationException>());
        }

        [Test]
        public void CloseTransactionWithoutOpenedTransactionTest()
        {
            // Pre-Conditions
            _instance.OpenSession();

            // Action 
            _instance.CloseTransaction();

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void CloseTransactionWithOpenedTransactionTest()
        {
            // Pre-Conditions
            _mocks.BackToRecordAll();
            ITransaction transaction = _mocks.StrictMock<ITransaction>();
            SetupResult.For(transaction.WasCommitted).Return(false);
            SetupResult.For(transaction.WasRolledBack).Return(false);
            transaction.Commit();
            transaction.Dispose();

            ISession session = _mocks.StrictMock<ISession>();
            Expect.Call(session.BeginTransaction()).Return(transaction);
            Expect.Call(_sessionFactory.OpenSession()).Return(session);
            _mocks.ReplayAll();

            _instance.OpenSession();
            Session tempSession = _instance.Session;
            _instance.BeginTransaction();

            // Action 
            _instance.CloseTransaction();

            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void CloseTransactionWithOpenedTransactionAndRollbackTest()
        {
            // Pre-Conditions
            _mocks.BackToRecordAll();
            ITransaction transaction = _mocks.StrictMock<ITransaction>();
            SetupResult.For(transaction.WasCommitted).Return(false);
            SetupResult.For(transaction.WasRolledBack).Return(false);
            transaction.Rollback();
            transaction.Dispose();
            
            ISession session = _mocks.StrictMock<ISession>();
            session.Expect(x => x.Clear());
            Expect.Call(session.BeginTransaction()).Return(transaction);
            Expect.Call(_sessionFactory.OpenSession()).Return(session);
            _mocks.ReplayAll();

            _instance.OpenSession();
            Session tempSession = _instance.Session;
            _instance.BeginTransaction();

            // Action 
            _instance.CloseTransaction(false);
            
            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void CloseTransactionWithOpenedTransactionAndExceptionTest()
        {
            // Pre-Conditions
            _mocks.BackToRecordAll();
            ITransaction transaction = _mocks.StrictMock<ITransaction>();
            SetupResult.For(transaction.WasCommitted).Return(false);
            SetupResult.For(transaction.WasRolledBack).Return(false);

            transaction.Expect(x => x.Commit()).Throw(new HibernateException());
            transaction.Rollback();
            transaction.Dispose();


            ISession session = _mocks.StrictMock<ISession>();
            session.Expect(x => x.Clear());
            Expect.Call(session.BeginTransaction()).Return(transaction);
            Expect.Call(_sessionFactory.OpenSession()).Return(session);
            _mocks.ReplayAll();

            _instance.OpenSession();
            Session tempSession = _instance.Session;
            _instance.BeginTransaction();

            // Action 
            Assert.That(() => _instance.CloseTransaction(), Throws.InstanceOf<HibernateException>());
           
            // Post-Conditions
            _mocks.VerifyAll();
        }

        [Test]
        public void CommitWithoutAnyQueryTest()
        {
            // Pre-Conditions
            _instance.OpenSession();

            // Action
            _instance.Commit();
        }

        [Test]
        public void CloseSessionWithOpenedTransactionAndExceptionTest()
        {
            // Pre-Conditions
            _mocks.BackToRecordAll();
            ITransaction transaction = _mocks.StrictMock<ITransaction>();
            SetupResult.For(transaction.WasCommitted).Return(false);
            SetupResult.For(transaction.WasRolledBack).Return(false);

            transaction.Expect(x => x.Commit()).Throw(new HibernateException());
           
            ISession session = _mocks.StrictMock<ISession>();
            SetupResult.For(session.IsOpen).Return(true);
            Expect.Call(session.BeginTransaction()).Return(transaction);
            Expect.Call(_sessionFactory.OpenSession()).Return(session);
            _mocks.ReplayAll();

            _instance.OpenSession();
            Session tempSession = _instance.Session;
            _instance.BeginTransaction();

            // Action 
            Assert.That(() => _instance.CloseSession(), Throws.InstanceOf<HibernateException>());

            // Post-Conditions
            _mocks.VerifyAll();
        }
        #endregion
    }
}
