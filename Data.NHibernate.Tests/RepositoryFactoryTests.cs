﻿#region Using directives
using System;
using Allag.Core.Data.Persistence;
using Allag.Data.TestData;
using Allag.Data.TestData.Entities;
using Allag.Data.TestData.Repositories;
using NUnit.Framework;
using MockRepository = Rhino.Mocks.MockRepository;

#endregion

namespace Allag.Data
{
    [TestFixture]
    public class RepositoryFactoryTests
    {
        #region Variables
        private RepositoryFactory<IMockRepositoryFactory, int> _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void Init()
        {
            ;
            _instance = new RepositoryFactory<IMockRepositoryFactory, int> {UnitOfWork = UnitOfWorkFactory.Create()};
        }

        [TearDown]
        public void CleanUp()
        {
            UnitOfWorkFactory.Close();
        }
        #endregion

        #region Tests
        [Test]
        public void UnitOfWork_Set_Test()
        {
            // Pre-Conditions
            UnitOfWork expected = new UnitOfWork();

            // Action
            _instance.UnitOfWork = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.UnitOfWork);
        }

        [Test]
        public void UnitOfWork_Set_NullValue_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => _instance.UnitOfWork = null);
        }

        [Test]
        public void UnitOfWork_Set_WrongType_Test()
        {
            // Action
            Assert.Throws<ArgumentException>(() => _instance.UnitOfWork = MockRepository.GenerateStub<IUnitOfWork>());
        }

        [Test]
        public void Get_NullEntityType_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => _instance.Get(null));
        }

        [Test]
        public void Get_Test()
        {
            // Action
            IMockRepository result = (IMockRepository)_instance.Get(typeof(MockEntity));

            // Post-Conditions
            Assert.IsNotNull(result);
        }

        [Test]
        public void Get_Typed_Test()
        {
            // Action
            IMockRepository result = (IMockRepository)((IRepositoryFactory)_instance).Get<MockEntity>();

            // Post-Conditions
            Assert.IsNotNull(result);
        }

        [Test]
        public void Get_Generic_Test()
        {
            // Action
            IRepository result = _instance.Get<MockManyToManyEntity>();

            // Post-Conditions
            Assert.IsInstanceOf<MockRepository<MockManyToManyEntity, int>>(result);
        }

        [Test]
        public void UnitOfWork_Get_Test()
        {
            // Action
            IUnitOfWork result = _instance.UnitOfWork;

            // Post-Conditions
            Assert.AreEqual(UnitOfWorkFactory.Current, result);
        }
        #endregion
    }
}