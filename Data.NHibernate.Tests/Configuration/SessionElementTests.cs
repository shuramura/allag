#region Using directives
using Allag.Core.Data.Persistence;
using NUnit.Framework;

#endregion

namespace Allag.Data.Configuration
{
    [TestFixture]
    public class SessionElementTests
    {
        #region Variables
        private SessionElement _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new SessionElement();
        }

        [TearDown]
        public void TearDown()
        {
        }
        #endregion

        #region Tests
        [Test]
        public void CtorTest()
        {
            // Post-Conditions
            Assert.IsNull(_instance.ConfigurationPath, "The property 'ConfigurationPath' is wrong.");
        }

        [Test]
        public void CtorRealTest()
        {
            // Pre-Conditions 
            SessionElement instance = DataNHibernateConfiguration.Sessions.Items["Allag.Data.TestData.IMockRepositoryFactory, Allag.Data.NHibernate.Tests"];

            // Post-Conditions
            Assert.IsNotNull(instance.ConfigurationPath, "The property 'ConfigurationPath' is wrong.");
        }
        #endregion
    }
}