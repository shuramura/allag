#region Using directives
using NUnit.Framework;

#endregion

namespace Allag.Data.Configuration
{
    [TestFixture]
    public class SessionsSectionTests
    {
        #region Tests
        [Test]
        public void GetSectionTest()
        {
            // Pre-Conditions
            SessionsSection instance = DataNHibernateConfiguration.Sessions;

            // Post-Conditions
            Assert.IsNotNull(instance, "The result is wrong.");
            Assert.AreEqual("sessions", instance.DefaultName, "The property 'DefaultName' is wrong.");
        }
        #endregion
    }
}