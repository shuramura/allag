#region Using directives

using NUnit.Framework;

#endregion

namespace Allag.Data.Configuration
{
    [TestFixture]
    public class DataNHibernateConfigurationTests
    {
        #region Tests
        [Test]
        public void CtorTest()
        {
			// Post-Conditions
            Assert.Greater(DataNHibernateConfiguration.Sessions.Items.Count, 0, "The property 'Sessions' is wrong.");
        }

        #endregion
    }
}
