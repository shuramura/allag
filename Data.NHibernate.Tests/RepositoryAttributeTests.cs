#region Using directives

using NUnit.Framework;

#endregion

namespace Allag.Data
{
    [TestFixture]
    public class RepositoryAttributeTests
    {
        #region Variables
        private RepositoryAttribute _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _instance = new RepositoryAttribute(typeof(TearDownAttribute));
        }

        [TearDown]
        public void TearDown() {}

        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
			// Post-Conditions
            Assert.AreEqual(typeof(TearDownAttribute), _instance.Entity, "The property 'Entity' is wrong.");
            Assert.IsNull(_instance.Factory, "The property 'Factory' is wrong.");
        }

        #endregion
    }
}
