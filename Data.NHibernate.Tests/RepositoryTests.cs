#region Using directives
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using Allag.Core.Data.Persistence;
using Allag.Core.Data.Persistence.Helper;
using Allag.Data.TestData.Repositories;
using Allag.Data.TestData.Entities;
using Microsoft.CSharp.RuntimeBinder;
using NUnit.Framework;

#endregion

namespace Allag.Data
{
    [TestFixture]
    public class RepositoryTests
    {
        #region Variables
        private MockRepository _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            SetUpTests.CreateDb();

            _instance = new MockRepository(SetUpTests.UnitOfWork);
        }

        [TearDown]
        public void TearDown()
        {
            SetUpTests.DeleteDb();
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Action
            MockRepository instance = new MockRepository();

            // Post-Conditions
            Assert.AreEqual(Operations.All, instance.SupportedOperations, "The property 'SupportedOperations' is wrong.");
            Assert.IsNull(instance.UnitOfWork, "The property 'UnitOfWork' is wrong.");
        }

        [Test]
        public void Ctor_WithUnitOfWork_Test()
        {
            // Post-Conditions
            Assert.AreEqual(Operations.All, _instance.SupportedOperations, "The property 'SupportedOperations' is wrong.");
            Assert.AreEqual(SetUpTests.UnitOfWork, _instance.UnitOfWork, "The property 'UnitOfWork' is wrong.");
            Assert.IsNotNull(_instance.RepositoryFactory, "The property 'RepositoryFactory' is wrong.");
        }

        [Test]
        public void UnitOfWork_Set_Test()
        {
            // Pre-Conditions
            UnitOfWork expected = new UnitOfWork();

            // Action
            _instance.UnitOfWork = expected;

            // Post-Conditions
            Assert.AreEqual(expected, _instance.UnitOfWork);
        }

        [Test]
        public void UnitOfWork_Set_NullValue_Test()
        {
            // Action
            Assert.Throws<ArgumentNullException>(() => _instance.UnitOfWork = null);
        }

        [Test]
        public void UnitOfWork_Set_WrongType_Test()
        {
            // Action
            Assert.Throws<ArgumentException>(() => _instance.UnitOfWork = Rhino.Mocks.MockRepository.GenerateStub<IUnitOfWork>());
        }

        [Test]
        public void Count_Test()
        {
            // Action
            long result = _instance.Count().Value;

            // Post-Conditions
            Assert.AreEqual(56, result);
        }

        [Test]
        public void Count_Conditional_Test()
        {
            // Action
            long result = _instance.Count(x => x.Number >= 2 && x.Number <= 4 && x.Boolean, null, null).Value;

            // Post-Conditions
            Assert.AreEqual(3, result);
        }

        [Test]
        public void Count_OperationInWithSubquery_Test()
        {
            // Action   
            long result = _instance.Count(x => !OpMethods.In<MockHasOneEntity>(x.Id, y => y.Entity.Distinct(true))).Value;

            // Post-Conditions
            Assert.AreEqual(28, result);
        }

        [Test]
        public void NotSupportedOperationTest([Values(Operations.Add, Operations.Update, Operations.Delete)] Operations operation)
        {
            // Pre-Conditions
            _instance.SetSupportedOperations(Operations.All ^ operation);
            Action<MockEntity> action = null;
            switch (operation)
            {
                case Operations.Add:
                    action = x => _instance.Add(x);
                    break;
                case Operations.Delete:
                    action = _instance.Delete;
                    break;
                case Operations.Update:
                    action = x => _instance.Update(x);
                    break;
                default:
                    throw new NotSupportedException();
            }

            // Action
            Assert.That(() => action(new MockEntity()), Throws.InvalidOperationException);
        }

        [Test]
        public void AddGetDeleteTest()
        {
            // Pre-Conditions 
            _instance.UnitOfWork.BeginTransaction();
            MockEntity mockEntity = new MockEntity {Number = 100};

            // Action
            MockEntity result = _instance.Add(mockEntity);

            // Post-Conditions
            Assert.AreNotEqual(0, result.Id, "The operation 'Add' is wrong.");

            // Pre-Conditions
            _instance.UnitOfWork.Commit();

            // Action
            result = _instance[result.Id];

            // Post-Conditions
            Assert.AreEqual(100, result.Number, "The operation 'Get' is wrong.");

            // Action
            _instance.Delete(_instance[result.Id]);
            _instance.UnitOfWork.Commit();

            // Post-Conditions
            Assert.IsNull(_instance[result.Id], "The operation 'Delete' is wrong.");
            _instance.UnitOfWork.CloseTransaction();
        }

        [Test]
        public void AddNullArgumentTest()
        {
            // Action
            Assert.That(() => _instance.Add(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void DeleteNullArgumentTest()
        {
            // Action
            Assert.That(() => _instance.Delete(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void GetNotExistTest()
        {
            // Action
            MockEntity result = _instance[int.MaxValue];

            // Post-Conditions
            Assert.IsNull(result);
        }

        [Test]
        public void Get_WithCustomCriteria_Test()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> func = x => x.Number == 908;
            LambdaExpression criteria = func;

            // Action
            MockEntity result = _instance.Get(criteria);

            // Post-Conditions
            Assert.AreEqual(56, result.Id);
        }

        [Test]
        public void This_GetUnTyped_Test()
        {
            // Action
            object result = ((IRepository)_instance)[56];

            // Post-Conditions
            Assert.IsNotNull(result);
        }

        [Test]
        public void Get_WithCustomCriteriaNotUniqueResult_Test()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> func = x => x.Number != 908;
            LambdaExpression criteria = func;

            // Action
            Assert.Throws<NonUniqueResultException>(() => _instance.Get(criteria));
        }

        [Test]
        public void Get_WithCustomCriteriaNotFound_Test()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> func = x => x.Number == 909;
            LambdaExpression criteria = func;

            // Action
            Assert.Throws<EntityNotFoundException<MockEntity>>(() => _instance.Get(criteria));
        }

        [Test]
        public void Get_WithCustomCriteriaAndProperties_Test()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, bool>> func = x => x.Number == 908 && x.Boolean.Distinct(true);
            LambdaExpression criteria = func;

            // Action
            var result = _instance.Get(criteria);

            // Post-Conditions
            int nId = 0;
            Assert.IsNotNull(result, "The result is wrong.");
            Assert.IsFalse(result.Boolean, "The property 'Boolean' is wrong.");
            Assert.Throws<RuntimeBinderException>(() => nId = result.Id);
        }

        [Test]
        public void GetNotExistWithThrowExceptionTest()
        {
            // Action
            Assert.That(() => _instance[int.MaxValue, true], Throws.InstanceOf<EntityNotFoundException<MockEntity, int>>());
        }

        [Test]
        public void UpdateNullArgumentTest()
        {
            // Action
            Assert.That(() =>_instance.Update(null), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void UpdateTest()
        {
            // Pre-Conditions 
            MockEntity mockEntity = _instance[1];
            mockEntity.Number = 101;

            // Action
            _instance.Update(mockEntity);

            // Post-Conditions
            _instance.UnitOfWork.Commit();
            Assert.AreEqual(101, _instance[mockEntity.Id].Number);

            mockEntity = _instance[1];
            mockEntity.Number = mockEntity.Id;
            _instance.Update(mockEntity);
            _instance.UnitOfWork.Commit();
        }

        [Test]
        public void UpdateWithRollbackTest()
        {
            // Pre-Conditions
            _instance.UnitOfWork.BeginTransaction();
            MockEntity mockEntity = _instance[2];
            mockEntity.Number = 101;

            // Action
            _instance.Update(mockEntity);
            _instance.UnitOfWork.CloseTransaction(false);

            // Post-Conditions
            SetUpTests.Session.Clear();
            Assert.AreEqual(1, _instance[mockEntity.Id].Number);
        }

        [Test]
        public void Find_Test()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find();

            // Post-Conditions
            Assert.AreEqual(MockEntityCount, result.Count());
        }

        [Test]
        public void Find_WithPaging_Test()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => 4.FirstResult() && 10.MaxResults());

            // Post-Conditions
            Assert.AreEqual(10, result.Count(), "The count is wrong.");
            Assert.AreEqual(5, result.First().Id, "The is is wrong.");
        }

        [Test]
        public void Find_JunctionAnd_Test()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Number == 2 && x.String == "2");

            // Post-Conditions
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void FindJunctionOrTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Number == 2 || x.String == "3");

            // Post-Conditions
            Assert.AreEqual(3, result.Count());
        }

        [Test]
        public void FindJunctionAndOrTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.String == "2" && x.Number == 2 || x.String == "3");

            // Post-Conditions
            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void FindOperationEqualNullTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.NullableNumber == null);

            // Post-Conditions
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void Find_OperationEqualWithProjection_Test()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Id == OpMethods.Max(x.Id));

            // Post-Conditions
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void FindOperationNotEqualTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Number != 3);

            // Post-Conditions
            Assert.AreEqual(MockEntityCount - 1, result.Count());
        }

        [Test]
        public void Find_OperationNotEqualWithProjection_Test()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Id != OpMethods.Max(x.Id));

            // Post-Conditions
            Assert.AreEqual(MockEntityCount - 1, result.Count());
        }

        [Test]
        public void FindOperationNotEqualNullTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.NullableNumber != null);

            // Post-Conditions
            Assert.AreEqual(MockEntityCount - 1, result.Count());
        }

        [Test]
        public void FindOperationGreaterThanOrEqualTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Number >= 2);

            // Post-Conditions
            Assert.AreEqual(MockEntityCount - 2, result.Count());
        }

        [Test]
        public void Find_OperationGreaterThanOrEqualWithProjection_Test()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Number >= OpMethods.Max(x.Number));

            // Post-Conditions
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void FindOperationGreaterThanTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Number > 2);

            // Post-Conditions
            Assert.AreEqual(MockEntityCount - 4, result.Count());
        }

        [Test]
        public void Find_OperationGreaterThanWithProjection_Test()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Number > OpMethods.Max(x.Number));

            // Post-Conditions
            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void FindOperationLessThanTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Number < 3);

            // Post-Conditions
            Assert.AreEqual(4, result.Count());
        }

        [Test]
        public void Find_OperationLessThanWithProjection_Test()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Number < OpMethods.Max(x.Number));

            // Post-Conditions
            Assert.AreEqual(MockEntityCount - 1, result.Count());
        }

        [Test]
        public void FindOperationLessThanWithNotTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => !(x.Number < 3));

            // Post-Conditions
            Assert.AreEqual(MockEntityCount - 4, result.Count());
        }

        [Test]
        public void FindOperationLessThanOrEqualTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Number <= 3);

            // Post-Conditions
            Assert.AreEqual(5, result.Count());
        }

        [Test]
        public void Find_OperationLessThanOrEqualWithProjection_Test()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Number <= OpMethods.Min(x.Number));

            // Post-Conditions
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void FindOperationLikeTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => OpMethods.Like(x.String, "3"));

            // Post-Conditions
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void Find_OperationIn_Test()
        {
            // Pre-Conditions
            IEnumerable<int> values = new int[] {3, 1, 3};
            values = values.Distinct();

            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => OpMethods.In(x.Number, values));

            // Post-Conditions
            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void FindOperationConstantTrueTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => 3 == 3);

            // Post-Conditions
            Assert.AreEqual(MockEntityCount, result.Count());
        }

        [Test]
        public void FindOperationConstantFalseTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => 3 != 3);

            // Post-Conditions
            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void Find_EmtpyCollection_Test()
        {
            // Pre-Conditions
            MockRepository<MockEntityParent, int> instance = new MockRepository<MockEntityParent, int>(SetUpTests.UnitOfWork);

            // Action
            IEnumerable<MockEntityParent> result = instance.Find(x => OpMethods.IsEmpty(x.Children));

            // Post-Conditions
            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void Find_NotEmtpyCollection_Test()
        {
            // Pre-Conditions
            MockRepository<MockEntityParent, int> instance = new MockRepository<MockEntityParent, int>(SetUpTests.UnitOfWork);

            // Action
            IEnumerable<MockEntityParent> result = instance.Find(x => !OpMethods.IsEmpty(x.Children));

            // Post-Conditions
            Assert.AreEqual(9, result.Count());
        }

        [Test]
        public void Find_OperationContains_Test()
        {
            // Pre-Conditions
            MockEntity entity = _instance[15];

            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => OpMethods.Contains(x.Parent.Children, entity));

            // Post-Conditions
            Assert.AreEqual(3, result.Count());
        }


        [Test]
        public void Find_OperationContainsWithNull_Test()
        {
            // Pre-Conditions
            MockEntity entity = _instance[15];

            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => OpMethods.Contains(x.Parent.Children, entity, null));

            // Post-Conditions
            Assert.AreEqual(14, result.Count());
        }

        [Test]
        public void Find_OperationContainsOnlyWithNull_Test()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => OpMethods.Contains<MockEntity, MockEntity>(x.Parent.Children));

            // Post-Conditions
            Assert.AreEqual(11, result.Count());
        }

        [Test]
        public void Find_OperationContainsById_Test()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => OpMethods.Contains(x.Parent.Children, 15));

            // Post-Conditions
            Assert.AreEqual(3, result.Count());
        }

        [TestCase(true, 11)]
        [TestCase(false, 2)]
        public void Find_OperationIf_Test(bool condition, int count)
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => OpMethods.If(x, condition, y => y.Parent == null, y => y.Parent.Id == 3));

            // Post-Conditions
            Assert.AreEqual(count, result.Count());
        }

        [TestCase(true, 56)]
        [TestCase(false, 2)]
        public void Find_OperationIfWithNullBranch_Test(bool condition, int count)
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => OpMethods.If(x, true, null, y => y.Parent.Id == 3));

            // Post-Conditions
            Assert.AreEqual(MockEntityCount, result.Count());
        }

        [Test]
        public void Find_OperationIfWrongConditionalType_Test()
        {
            // Action
            Assert.That(() => _instance.Find(x => OpMethods.If(x.Parent, true, y => y.Children.IsEmpty(), y => y.String == "")), Throws.InstanceOf<InvalidDataException>());
        }

        [Test]
        public void FindOperationComplexContainsTest()
        {
            // Pre-Conditions
            MockRepository<MockManyToManyEntity, int> manyToManyRepository = new MockRepository<MockManyToManyEntity, int>(SetUpTests.UnitOfWork);
            MockManyToManyEntity entity = manyToManyRepository[1];

            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => OpMethods.Contains(x.Parent.ManyToMany, entity));

            // Post-Conditions
            Assert.AreEqual(20, result.Count());
        }

        [Test]
        public void FindOperationContainsOneLevelTest()
        {
            // Pre-Conditions
            MockRepository<MockEntityParent, int> instance = new MockRepository<MockEntityParent, int>(SetUpTests.UnitOfWork);
            MockEntity entity = _instance[15];

            // Action
            IEnumerable<MockEntityParent> result = instance.Find(x => OpMethods.Contains(x.Children, entity));

            // Post-Conditions
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void Find_OperationContainsOneLevelWithSubquery_Test()
        {
            // Pre-Conditions
            MockRepository<MockEntityParent, int> instance = new MockRepository<MockEntityParent, int>(SetUpTests.UnitOfWork);

            // Action
            IEnumerable<MockEntityParent> result = instance.Find(x => OpMethods.Contains(x.Children, y => y.Id == 15));

            // Post-Conditions
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void Find_OperationComplexContainsWithSubquery_Test()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => OpMethods.Contains(x.Parent.ManyToMany, y => y.Id == 1));

            // Post-Conditions
            Assert.AreEqual(20, result.Count());
        }

        [Test]
        public void Find_OperationComplexContainsWithSubqueryFromAnotherEntity_Test()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => OpMethods.Contains<MockEntityParent>(x.Parent.ManyToMany, y => y.Number.Distinct(true) && y.Number == 1));

            // Post-Conditions
            Assert.AreEqual(20, result.Count());
        }

        [Test]
        public void FindComplexPropertyTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Parent.Number == 5 && x.Number < 503);

            // Post-Conditions
            Assert.AreEqual(3, result.Count(), "The count is wrong.");
            Assert.AreEqual(6, result.First().Parent.Id, "The parent is wrong.");
        }

        [Test]
        public void FindComplexPropertyMultiExpressionTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Parent.Number == 5, x => x.Number < 503);

            // Post-Conditions
            Assert.AreEqual(3, result.Count(), "The count is wrong.");
            Assert.AreEqual(6, result.First().Parent.Id, "The parent is wrong.");
        }

        [Test]
        public void FindGroupsTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => (x.Number > 500 && x.Number < 503) || (x.Number > 400 && x.Number < 402));

            // Post-Conditions
            Assert.AreEqual(3, result.Count());
        }

        [Test]
        public void FindGroupsWithNotTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => !((x.Number > 500 && x.Number < 503) || (x.Number > 400 && x.Number < 402)));

            // Post-Conditions
            Assert.AreEqual(MockEntityCount - 3, result.Count());
        }

        [Test]
        public void FindWithAccessToOtherMemberTest()
        {
            // Pre-Conditions
// ReSharper disable ConvertToConstant.Local
            bool bTemp = true;
// ReSharper restore ConvertToConstant.Local

            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => (!bTemp || (bTemp && x.Number == 503)));

            // Post-Conditions
            Assert.AreEqual(_instance.Find(x => x.Number == 503).Count(), result.Count());
        }

        [Test]
        public void FindOrderTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => OpMethods.Order(x.Number, false));

            // Post-Conditions
            Assert.AreEqual(908, result.First().Number);
        }

        [Test]
        public void Find_ByOneProjection_Test()
        {
            // Action
            var result = _instance.Find(x => new {x.Boolean});

            // Post-Conditions
            Assert.AreEqual(56, result.Count());
        }

        [Test]
        public void Find_ByOneProjectionAndGroupBy_Test()
        {
            // Action
            var result = _instance.Find(x => new {x.Boolean}.GroupBy());

            // Post-Conditions
            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void Find_ByOneProjectionAndDistinct_Test()
        {
            // Action
            var result = _instance.Find(x => new {x.Boolean}.Distinct());

            // Post-Conditions
            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void FindByProjectionTest()
        {
            // Action
            var result = _instance.Find(x => new {x.Boolean, x.Parent.Id}, x => x.Parent != null);

            // Post-Conditions
            Assert.AreEqual(45, result.Count());
        }

        [Test]
        public void Find_ByProjectionWithGroupBy_Test()
        {
            // Action
            var result = _instance.Find(x => new {x.Boolean, x.Parent.Id}.GroupBy(), x => x.Parent != null);

            // Post-Conditions
            Assert.AreEqual(9, result.Count());
        }

        [Test]
        public void Find_ByProjectionWrongOperation_Test()
        {
            // Action
            Assert.That(() => _instance.Find(x => new {Str = string.IsNullOrEmpty(x.String)}).ToArray(), Throws.InvalidOperationException);
        }

        [Test]
        public void FindByProjectionMinimumTest()
        {
            // Action
            var result = _instance.Find(x => new {Id = OpMethods.Min(x.Parent.Id)});

            // Post-Conditions
            Assert.AreEqual(2, result.First().Id);
        }

        [Test]
        public void FindByProjectionMinimumWithConditionsTest()
        {
            // Action
            var result = _instance.Find(x => new {Id = OpMethods.Min(x.Parent.Id)}, x => x.Parent.Id > 3);

            // Post-Conditions
            Assert.AreEqual(4, result.First().Id);
        }

        [Test]
        public void FindByProjectionMaximumTest()
        {
            // Action
            var result = _instance.Find(x => new {Id = OpMethods.Max(x.Parent.Id)});

            // Post-Conditions
            Assert.AreEqual(10, result.First().Id);
        }

        [Test]
        public void FindByProjectionMaximumWithConditionsTest()
        {
            // Action
            var result = _instance.Find(x => new {Id = OpMethods.Max(x.Parent.Id)}, x => x.Parent.Id < 9);

            // Post-Conditions
            Assert.AreEqual(8, result.First().Id);
        }

        [Test]
        public void FindByProjectionAverageTest()
        {
            // Action
            var result = _instance.Find(x => new {Id = OpMethods.Avg(x.Parent.Id)});

            // Post-Conditions
            Assert.AreEqual(7.333333333333333d, result.First().Id);
        }

        [Test]
        public void FindByProjectionAverageWithConditionsTest()
        {
            // Action
            var result = _instance.Find(x => new {Id = OpMethods.Avg(x.Parent.Id)}, x => x.Parent.Id < 9);

            // Post-Conditions
            Assert.AreEqual(6.0d, result.First().Id);
        }

        [Test]
        public void FindByProjectionSumTest()
        {
            // Action
            var result = _instance.Find(x => new {Id = OpMethods.Sum(x.Parent.Id)});

            // Post-Conditions
            Assert.AreEqual(330, result.First().Id);
        }

        [Test]
        public void FindByProjectionWithNullResultTest()
        {
            // Action
            var result = _instance.Find(x => new {Id = OpMethods.Sum(x.Parent.Id)}, x => x.Parent.Id > int.MaxValue);

            // Post-Conditions
            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void FindByProjectionSumWithConditionsTest()
        {
            // Action
            var result = _instance.Find(x => new {Id = OpMethods.Sum(x.Parent.Id)}, x => x.Parent.Id < 9);

            // Post-Conditions
            Assert.AreEqual(168, result.First().Id);
        }

        [Test]
        public void GetOneMoreThenOneResultTest()
        {
            // Action
            Assert.That(() => _instance[x => x.Number == 2], Throws.InstanceOf<NonUniqueResultException>());
        }

        [Test]
        public void GetOneTest()
        {
            // Action
            MockEntity result = _instance[x => x.Number == 3];

            // Post-Conditions
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetOneNotExistTest()
        {
            // Action
            MockEntity result = _instance[x => x.Number == int.MaxValue];

            // Post-Conditions
            Assert.IsNull(result);
        }

        [Test]
        public void ExistsTrueTest()
        {
            // Action
            bool result = _instance.Exists(x => x.Number == 2);

            // Post-Conditions
            Assert.IsTrue(result);
        }

        [Test]
        public void ExistsFalseTest()
        {
            // Action
            bool result = _instance.Exists(x => x.Number == int.MaxValue);

            // Post-Conditions
            Assert.IsFalse(result);
        }

        [Test]
        public void FindByComponentPropertyTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Component.Prop0 > 1 && x.Component.Prop0 < 4);

            // Post-Conditions
            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void FindByComponentEntityPropertyTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Component.Entity.Id == 5);

            // Post-Conditions
            Assert.AreEqual(4, result.Count());
        }

        [Test]
        public void FindWithMaxResultsTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Component.Entity.Id == 5 && OpMethods.MaxResults(2));

            // Post-Conditions
            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void FindWithFirstResultTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => x.Component.Entity.Id == 5 && OpMethods.FirstResult(3));

            // Post-Conditions
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void FindWithBetweenTest()
        {
            // Action
            IEnumerable<MockEntity> result = _instance.Find(x => OpMethods.Between(x.Number, 3, 5));

            // Post-Conditions
            Assert.AreEqual(3, result.Count());
        }

        [Test]
        public void Find_SingleAnonimousType_Test()
        {
            // Action
            var result = _instance.Find(
                x => new
                {
                    x.Number,
                    NullableProp = (int?)x.Component.Prop0,
                    ThirdLevel = x.OneToOne.Entity.Id,
                    x.Component.Prop0,
                    Props = x.Component
                },
                x => x.Id.In(2, 13) && x.OneToOne.Entity != null).ToArray();

            // Post-Conditions 
            Assert.AreEqual(2, result.Count(), "The result is wrong.");
            var item = result.First();
            Assert.AreEqual(1, item.Number, "The property 'Number' is wrong.");
            Assert.AreEqual(1, item.NullableProp, "The property 'NullableProp' is wrong.");
            Assert.AreEqual(1, item.Prop0, "The property 'Prop' is wrong.");
            Assert.AreEqual(item.Prop0, item.Props.Prop0, "The property 'Props.Prop0' is wrong.");
            Assert.AreEqual(100, item.Props.Prop1, "The property 'Props.Prop0' is wrong.");

            item = result.Skip(1).First();
            Assert.AreEqual(200, item.Number, "The property 'Number' is wrong.");
            Assert.IsNull(item.NullableProp, "The property 'NullableProp' is wrong.");
            Assert.AreEqual(0, item.Prop0, "The property 'Props.Prop0' is wrong.");
            Assert.IsNull(item.Props, "The property 'Props' is wrong.");
        }

        [Test]
        public void Find_AnonimousType_Test()
        {
            // Action
            var result = _instance.Find(x => new
            {
                Avg = OpMethods.Avg(x.Number),
                Sum = OpMethods.Sum(x.Number),
                MinNumber = OpMethods.Min(x.Number),
                MaxNumber = OpMethods.Max(x.Number),
                MinId = OpMethods.Min(x.Id)
            }).ToArray();

            // Post-Conditions 
            Assert.AreEqual(1, result.Count(), "The result is wrong.");
            var item = result.First();
            Assert.AreEqual(511.91071428571428d, item.Avg, "The property 'Avg' is wrong.");
            Assert.AreEqual(28667, item.Sum, "The property 'Sum' is wrong.");
        }

        [Test]
        public void Find_WithWrongPropertiesType_Test()
        {
            // Action
            Assert.That(() =>_instance.Find(x => this).ToArray(), Throws.InstanceOf<InvalidDataException>());
        }

        [Test]
        public void Find_WithNullProperties_Test()
        {
            // Pre-Conditions
            Expression<Func<MockEntity, object[]>> properties = null;

            // Action
            Assert.That(() => _instance.Find(properties).ToArray(), Throws.InstanceOf<ArgumentNullException>());
        }

        [Test]
        public void Find_ArrayType_Test()
        {
            // Action
            var result = _instance.Find(x => new object[]
            {
                OpMethods.Avg(x.Number),
                OpMethods.Max(x.Number),
            }).ToArray();

            // Post-Conditions 
            Assert.AreEqual(1, result.Count(), "The result is wrong.");
            var item = result.First();
            Assert.AreEqual(511.91071428571428d, item[0], "The index 0 is wrong.");
            Assert.AreEqual(908, item[1], "The property 1 is wrong.");
        }

        [Test]
        public void Find_TypeWithMemberInitialization_Test()
        {
            // Pre-Conditions
            MockEntity mockEntity = _instance[13];

            // Action
            var result = _instance.Find(x => new Item(x.Id)
            {
                One = x.Boolean,
                Two = x.Number,
                List = {x.String, x.Parent.String}
            }, x => x.Parent != null).ToArray();

            // Post-Conditions 
            Assert.AreEqual(45, result.Count(), "The result is wrong.");
            var item = result.Skip(1).First();
            Assert.AreEqual(mockEntity.Id, item.Id, "The property 'Id' is wrong.");
            Assert.AreEqual(mockEntity.Boolean, item.One, "The property 'One' is wrong.");
            Assert.AreEqual(mockEntity.Number, item.Two, "The property 'Two' is wrong.");
            Assert.AreEqual(mockEntity.String, item.List[0], "The property 'List[0]' is wrong.");
            Assert.AreEqual(mockEntity.Parent.String, item.List[1], "The property 'List[1]' is wrong.");
        }

        [Test]
        public void Find_MixPropertiesWithAggregationMethods_Test()
        {
            // Action
            Assert.That(() => _instance.Find(x => new {x.Number, Sum = OpMethods.Sum(x.Number)}).ToArray(), Throws.InstanceOf<InvalidDataException>());
        }

        [Test]
        public void Find_MixPropertiesWithAggregationMethodsAndGroupBy_Test()
        {
            // Action
            var result = _instance.Find(x => new {x.Boolean, Sum = OpMethods.Sum(x.Number)}.GroupBy());

            // Post-Conditions
            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void Find_ByProjectionCount_Test()
        {
            // Action
            var result = _instance.Find(x => new {x.Boolean, Count = OpMethods.RowCount(x.Number)}.GroupBy());

            // Post-Conditions
            Assert.AreEqual(2, result.Count(), "The count is wrong.");
            Assert.AreEqual(46, result.First().Count, "The first row count is wrong.");
        }

        [Test]
        public void Find_WithCustomCriteriaAndOneProjection_Test()
        {
            // Action
            Expression<Func<MockEntity, bool>> func = x => OpMethods.Avg(x.Number) == 0f;
            LambdaExpression criteria = func;
            dynamic[] result = _instance.Find(criteria).ToArray();

            // Post-Conditions 
            Assert.AreEqual(1, result.Length, "The result is wrong.");
            var item = result.First();
            Assert.AreEqual(511.91071428571428d, item.NumberAverage, "The property 'NumberAverage' is wrong.");
        }

        [Test]
        public void Find_WithCustomCriteriaAndMoreThanOneProjection_Test()
        {
            // Action
            Expression<Func<MockEntity, bool>> func = x => OpMethods.Avg(x.Parent.Id) == 0 && OpMethods.Max(x.Number) == 0;
            LambdaExpression criteria = func;
            var result = _instance.Find(criteria).ToArray();

            // Post-Conditions 
            Assert.AreEqual(1, result.Count(), "The result is wrong.");
            var item = result.First();
            Assert.AreEqual(7.333333333333333d, item.ParentIdAverage, "The property 'ParentIdAverage' is wrong.");
            Assert.AreEqual(908, item.NumberMaximum, "The property 'NumberMaximum' is wrong.");
        }

        [Test]
        public void Find_WithCustomCriteria_Test()
        {
            // Action
            Expression<Func<MockEntity, bool>> func = x => x.Number == 908;
            LambdaExpression criteria = func;
            var result = _instance.Find(criteria).ToArray();

            // Post-Conditions 
            Assert.AreEqual(1, result.Count(), "The result is wrong.");
            var item = result.First();
            Assert.IsInstanceOf<MockEntity>(item, "The item is wrong.");
            Assert.AreEqual(908, item.Number, "The property 'Number' is wrong.");
        }

        [Test]
        public void Find_OperationInWithSubquery_Test()
        {
            // Action   
            var result = _instance.Find(x => !OpMethods.In<MockHasOneEntity>(x.Id, y => y.Entity.Id.Distinct(true))).ToArray();

            // Post-Conditions
            Assert.AreEqual(28, result.Length);
        }
        #endregion

        #region Properties
        public static int MockEntityCount
        {
            get { return 56; }
        }
        #endregion

        #region Classes
        public class Item
        {
            #region Constructors
            public Item(int id)
            {
                Id = id;
                List = new List<string>();
            }
            #endregion

            #region Properties
            public int Id { get; private set; }
            public bool One { get; set; }
            public int Two { get; set; }

            public IList<string> List { get; private set; }
            #endregion
        }
        #endregion
    }
}