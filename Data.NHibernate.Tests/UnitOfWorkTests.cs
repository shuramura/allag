﻿#region Using directives
using System;
using Allag.Core.Data.Persistence;
using Allag.Core.Reflection;
using Allag.Data.Management;
using Allag.Data.TestData;
using Allag.Data.TestData.Entities;
using Allag.Data.TestData.Repositories;
using NUnit.Framework;
using MockRepository = Rhino.Mocks.MockRepository;

#endregion

namespace Allag.Data
{
    [TestFixture]
    public class UnitOfWorkTests
    {
        #region Variables
        private MockRepository _mocks;
        private UnitOfWork _instance;
        #endregion

        #region Initialization
        [SetUp]
        public void SetUp()
        {
            _mocks = new MockRepository();
            SetUpTests.CreateDb();
            _instance = UnitOfWorkFactory.Current as UnitOfWork;
        }

        [TearDown]
        public void TearDown()
        {
            SetUpTests.DeleteDb();
        }
        #endregion

        #region Tests
        [Test]
        public void Ctor_Test()
        {
            // Post-Conditions
            Assert.IsNotNull(_instance, "The property 'Instance' is wrong.");
            Assert.IsNotNull(UnitOfWork.GetConfiguration<IMockRepositoryFactory>(), "The property 'Configurations' is wrong.");
        }

        [Test]
        public void Get_Test()
        {
            // Action 
            IMockRepositoryFactory result = _instance.Get<IMockRepositoryFactory>();

            // Post-Conditions
            Assert.IsNotNull(result);
        }

        [Test]
        public void Get_UnknownFactoryType_Test()
        {
            // Action 
            IMockUnknownRepositoryFactory result = _instance.Get<IMockUnknownRepositoryFactory>();

            // Post-Conditions
            Assert.IsNull(result);
        }

        [Test]
        public void This_ByType_Test()
        {
            // Action 
            object result = _instance[typeof(IMockRepositoryFactory)];

            // Post-Conditions
            Assert.IsInstanceOf<IMockRepositoryFactory>(result, "The result is wrong.");
        }

        [Test]
        public void This_ByTypeNullType_Test()
        {
            // Pre-Conditions
            object result;

            // Action 
            Assert.Throws<ArgumentNullException>(() => result = _instance[null]);
        }

        [Test]
        public void This_ByTypeWrongType_Test()
        {
            // Action 
            object result = _instance[GetType()];

            // Post-Conditions
            Assert.IsNull(result);
        }

        [Test]
        public void GetContext_Test()
        {
            // Action
            DbContext resutl = _instance.GetContext<IMockRepositoryFactory>();

            // Post-Conditions
            Assert.IsNotNull(resutl);
        }

        [Test]
        public void ThisNotOpenedSessionTest()
        {
            // Pre-Conditions
            _instance.CloseSession();

            // Action
            Assert.That(() => _instance.GetContext<IMockRepositoryFactory>(), Throws.InvalidOperationException);
        }

        [Test]
        public void RollbackTest()
        {
            // Pre-Conditions 
            MockRepository<MockEntity, int> repository = _mocks.PartialMock<MockRepository<MockEntity, int>>(_instance);
            _mocks.Replay(repository);

            _instance.BeginTransaction();

            MockEntity entity = repository.Add(new MockEntity());

            // Action
            _instance.Rollback();

            // Post-Conditions
            Assert.IsNull(repository[entity.Id]);
        }

        [Test]
        public void OpenSessionTransactionalTest()
        {
            // Pre-Conditions 
            _instance.CloseSession();
            _instance = (UnitOfWork)UnitOfWorkFactory.Create(false, true);
            MockRepository<MockEntity, int> repository = _mocks.PartialMock<MockRepository<MockEntity, int>>(_instance);
            _mocks.Replay(repository);

            // Action
            MockEntity entity = repository.Add(new MockEntity());

            // Post-Conditions
            _instance.CloseTransaction(false);
            Assert.IsNull(repository[entity.Id]);
            _instance.CloseSession();
        }

        [Test]
        public void ModifyEntityAfterClosingTransaction_Test()
        {
            // Pre-Conditions 
            _instance.CloseSession();
            _instance = (UnitOfWork)UnitOfWorkFactory.Create(false, true);
            MockRepository<MockEntity, int> repository = _mocks.PartialMock<MockRepository<MockEntity, int>>(_instance);
            _mocks.Replay(repository);

            MockEntity entity = repository.Add(new MockEntity());
            _instance.CloseTransaction();
            _instance.BeginTransaction();
            entity.Boolean = true;
            _instance.CloseTransaction();

            // Action
            MockEntity result = repository[entity.Id];

            // Post-Conditions

            Assert.IsTrue(result.Boolean, "The property 'Boolean' is wrong.");
            _instance.CloseSession();
        }

        [Test]
        public void CloseSessionWithErrorTest()
        {
            // Pre-Conditions 
            IUnitOfWork result = null;

            _instance.CloseSession();
            _instance = (UnitOfWork)UnitOfWorkFactory.Create(false, true);

            MockRepository<MockEntity, int> repository = _mocks.PartialMock<MockRepository<MockEntity, int>>(_instance);
            _mocks.Replay(repository);
            MockEntity entity = repository[1];
            entity.Parent = new MockEntityParent().SetProperty(x => x.Id, int.MaxValue);

            // Action
            try
            {
                _instance.CloseSession();
            }
            catch (Exception)
            {
                _instance.Rollback();
                _instance.CloseSession();
                result = UnitOfWorkFactory.Create();
            }

            // Post-Conditions
            Assert.IsNotNull(result);
        }

        [Test]
        public void Dispose_WithError_Test()
        {
            // Pre-Conditions 
            IUnitOfWork result = null;

            _instance.CloseSession();
            _instance = (UnitOfWork)UnitOfWorkFactory.Create(false, true);

            MockRepository<MockEntity, int> repository = _mocks.PartialMock<MockRepository<MockEntity, int>>(_instance);
            _mocks.Replay(repository);
            MockEntity entity = repository[1];
            entity.Parent = new MockEntityParent().SetProperty(x => x.Id, int.MaxValue);

            // Action
            _instance.Dispose();

            // Post-Conditions
            result = UnitOfWorkFactory.Create();
            Assert.IsNotNull(result);
        }

        [Test]
        public void CommitTest()
        {
            // Pre-Conditions 
            MockRepository<MockEntity, int> repository = _mocks.PartialMock<MockRepository<MockEntity, int>>(_instance);
            _mocks.Replay(repository);

            _instance.BeginTransaction();
            MockEntity entity = repository.Add(new MockEntity());

            // Action
            _instance.Commit();

            // Post-Conditions
            Assert.IsNotNull(repository[entity.Id]);
        }

        [Test]
        public void CloseTransactionTest()
        {
            // Pre-Conditions 
            MockRepository<MockEntity, int> repository = _mocks.PartialMock<MockRepository<MockEntity, int>>(_instance);
            _mocks.Replay(repository);

            _instance.BeginTransaction();
            MockEntity entity = repository.Add(new MockEntity());

            // Action
            _instance.CloseTransaction();

            // Post-Conditions
            Assert.IsNotNull(repository[entity.Id]);
        }

        [Test]
        public void CloseTransactionRollbackTest()
        {
            // Pre-Conditions 
            MockRepository<MockEntity, int> repository = _mocks.PartialMock<MockRepository<MockEntity, int>>(_instance);
            _mocks.Replay(repository);

            _instance.BeginTransaction();
            MockEntity entity = repository.Add(new MockEntity());

            // Action
            _instance.CloseTransaction(false);

            // Post-Conditions
            Assert.IsNull(repository[entity.Id]);
        }

        [Test]
        public void ToString_Test()
        {
            // Action
            string result = _instance.ToString();

            // Post-Conditions
            Assert.AreEqual(string.Format("{{Id: {0}}}", _instance.Id), result);
        }
        #endregion
    }
}