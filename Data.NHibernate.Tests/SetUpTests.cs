#region Using directives
using System;
using System.Globalization;
using System.IO;
using Allag.Core;
using Allag.Core.Data.Persistence;
using Allag.Data.TestData;
using Allag.Data.TestData.Repositories;
using Allag.Data.TestData.Entities;
using NHibernate.Tool.hbm2ddl;
using NUnit.Framework;

#endregion

namespace Allag.Data
{
    [SetUpFixture]
    public class SetUpTests
    {
        #region Initialization
        [OneTimeSetUp]
        public void Init()
        {
            Application.Root = typeof(RepositoryFactory);
        }
        #endregion

        #region Static properties
        public static UnitOfWork UnitOfWork
        {
            get { return ((UnitOfWork)UnitOfWorkFactory.Current); }
        }

        public static Session Session
        {
            get { return UnitOfWork.GetContext<IMockRepositoryFactory>().Session; }
        }
        #endregion


        #region Methods
        public static void CreateDb()
        {
            UnitOfWorkFactory.Create();
            new SchemaExport(UnitOfWork.GetConfiguration<IMockRepositoryFactory>()).Execute(true, true, false, Session.Connection, null);
            FillData();
        }

        public static void DeleteDb()
        {
            if (UnitOfWork != null)
            {
                UnitOfWork.Dispose();    
            }
            
            File.Delete("testdb.db");
        }

        private static void FillData<TRepository, TEntity, TId>(int count, Func<int, TEntity> hndCreateEntity, Func<TEntity[]> hndAction = null)
            where TEntity : Entity<IMockRepositoryFactory, TEntity, TId>, new()
            where TRepository : MockRepository<TEntity, TId>
        {
            if (hndCreateEntity == null)
            {
                throw new ArgumentNullException("hndCreateEntity");
            }

            TRepository repository = (TRepository)Activator.CreateInstance(typeof(TRepository), UnitOfWork);
            for (int i = 0; i < count; i++)
            {
                TEntity entity = hndCreateEntity(i);
                if (entity != null)
                {
                    repository.Add(entity);
                }
            }
            if (hndAction != null)
            {
                foreach (TEntity entity in hndAction())
                {
                    repository.Add(entity);
                }
            }
        }

        private static void FillData()
        {
            UnitOfWork.BeginTransaction();
            FillData<MockRepository<MockEntity, int>, MockEntity, int>(10,
                i => new MockEntity
                {
                    Number = i, NullableNumber = i, String = i.ToString(CultureInfo.InvariantCulture),
                    Boolean = true, Component = new MockComponent {Prop0 = i, Prop1 = i * 100}
                },
                () => new[] {new MockEntity {Number = 2}});

            FillData<MockRepository<MockManyToManyEntity, int>, MockManyToManyEntity, int>(2,
                i => new MockManyToManyEntity());

            MockRepository<MockManyToManyEntity, int> manyToManyRepository = new MockRepository<MockManyToManyEntity, int>(UnitOfWork);
            FillData<MockRepository<MockEntityParent, int>, MockEntityParent, int>(10,
                index =>
                {
                    MockEntityParent parent = new MockEntityParent
                    {
                        Number = index,
                        String = "String" + index
                    };

                    FillData<MockRepository<MockEntity, int>, MockEntity, int>(index,
                        i =>
                        {
                            int n = 100 * index + i;
                            MockEntity child = new MockEntity {Parent = parent, Number = n, NullableNumber = n, String = "String" + n};
                            parent.Children.Add(child);
                            return child;
                        });

                    MockManyToManyEntity manyToManyEntity = manyToManyRepository[index % 2 + 1];
                    parent.ManyToMany.Add(manyToManyEntity);
                    return parent;
                },
                () => new MockEntityParent[] {new MockEntityParent {Number = 11}});

            MockRepository<MockEntity, int> mockRepository = new MockRepository<MockEntity, int>(UnitOfWork);
            MockEntity entity = mockRepository[5, true];
            for (int i = 1; i < 5; i++)
            {
                mockRepository[i].Component.Entity = entity;
            }

            foreach (MockEntity item in mockRepository.Items)
            {
                if (item.Id % 2 == 0)
                {
                    FillData<MockRepository<MockHasOneEntity, int>, MockHasOneEntity, int>(1, index => new MockHasOneEntity
                    {
                        Entity = item
                    });
                }
            }

            UnitOfWork.CloseTransaction();
        }
        #endregion
    }
}